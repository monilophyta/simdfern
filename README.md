
# TODO

## Error handling

* define appropriate error classes in C++ with error handover

## Preprocessing

* Color support
* Data Augmentation (mirror)

## Image

* more generic CImagePointer for pointer of types different to int32_t and float32_t
  * Extension of vi32_t::gather to handle other types as well
  * Use Case: Extraction of labels from label image

## Box Evaluation

* add a box evaluation class (similar to box feature evaluation) for evaluation of a list of boxes without explicit feature calculation
  * required for random fern evaluation after LDA / SVM

## Visualization

* ebl tree visualization

## Line Search

* currenlty only the middle between two candidates is considered.
  However there might be a better optimum due the regularization. Choose this Optimimum!