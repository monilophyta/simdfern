/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <utility>


namespace tree
{

template <typename eval_T, typename T>
inline constexpr std::pair<T,T>
golden_section_search( eval_T func, T f_left, T f_right, T f_tol = T(1e-5) );

} // namespace tree
