/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <simd.hpp>
#include "tree_node_index.hpp"
#include "tree_enums.hpp"


namespace tree
{


struct CTreeSplitStatisticsBase
{

protected:

    smd::matrix_vi32_t m_CorrelationCounts_ai32;   // [ #Leaf x #Label ]
    smd::array_vi32_t             m_Counts_ai32;   // [ #Leaf ]

public:

    CTreeSplitStatisticsBase( int32_t f_numLabels_i32, int32_t f_numNodes_i32 );
    virtual ~CTreeSplitStatisticsBase() {}

    inline int32_t n_nodes() const { return m_Counts_ai32.elem().size(); }
    inline int32_t n_leafs() const { return n_nodes(); }
    inline int32_t n_labels() const { return m_CorrelationCounts_ai32.elem().n_cols(); }

    virtual void initialize_zero();

protected:
    
    void initialize_from( const CTreeSplitStatisticsBase& other );

    const int32_t*
    initialize_from( const nodeIdx_t* nodeIter, const nodeIdx_t* const nodeIterEnd,
                     const int32_t* labelIter,
                     const int32_t* weightIter );

public:

    /*!
      \brief

      \param f_nodeIdx_i32
      \param f_labelIdx_i32
      \param f_weight

      \return Old count m_CorrelationCounts_ai32[f_nodeIdx_i32,f_labelIdx_i32] BEFORE modification (increase_counts)
    */
    int32_t increase_counts( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 );
    
    /*!
      \brief

      \param f_nodeIdx_i32
      \param f_labelIdx_i32
      \param f_weight

      \return Old count m_CorrelationCounts_ai32[f_nodeIdx_i32,f_labelIdx_i32] BEFORE modification (decrease_counts)
    */
    int32_t decrease_counts( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 );


    virtual bool assert_correctness() const;
};



} // namespace tree

