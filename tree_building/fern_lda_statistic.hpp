/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <simd.hpp>
#include "tree_node_index.hpp"

#include <vector>


namespace tree
{


// -------------------------------------------------------------------------------------------------------


struct CMultiNodeFirstPassStatistics
{
protected:

    smd::matrix_i32_t              m_weightSums_ai32;        // [ N x L ]

private:

    std::vector<smd::array_vf32_t> m_features_means_vaf32;   // [ N*L x [ F ] ]

    int32_t                        m_numFeatures_i32;

    std::vector<bool>              m_nodeRelevance_ab;       // [ N ]

public:

    CMultiNodeFirstPassStatistics( int32_t f_numLabels_i32, int32_t f_numNodes_i32, int32_t f_numFeatures_i32 );

    inline int32_t num_nodes() const { return m_nodeRelevance_ab.size(); }
    inline int32_t num_labels() const { return m_weightSums_ai32.n_cols(); }
    inline int32_t num_features() const { return m_numFeatures_i32; }
    inline bool node_is_relevant( int32_t nIdx ) const { return m_nodeRelevance_ab[nIdx]; }

    void process_first_pass( const nodeIdx_t* nodeIter, const nodeIdx_t* const nodeIterEnd,
                             const int32_t* labelIter,
                             const int32_t* weightIter,
                             smd::bundle_iterator_cf32_t featureIter );

    int32_t get_inter_covariance( smd::matrix_view_f32_t&  f_interCov_r ) const;
    int32_t get_inter_covariance( smd::matrix_view_vf32_t& f_interCov_r ) const;

private:

    static
    void update_mean( int32_t                             f_weight_i32,
                      int32_t&                            f_weightSum_ri32,
                      const smd::column_iterator_cf32_t&  f_featureIter,
                      smd::array_view_f32_t               f_mean_stat_af32 );

    void update_node_relevances();

    int32_t get_inter_co_moment( smd::matrix_view_vf32_t& f_interCov_r ) const;

protected:

    const smd::array_view_vf32_t& first_pass_mean( int32_t f_flatIdx_i32 ) const;

    const smd::array_view_vf32_t& 
    first_pass_mean( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32 ) const;

    const std::vector<smd::array_vf32_t>& first_pass_means() const { return m_features_means_vaf32; }

    // ----------------------------------------------------------------------

    smd::array_view_vf32_t& 
    node_class_mean( int32_t                         f_nodeIdx_i32,
                     int32_t                         f_labelIdx_i32,
                     std::vector<smd::array_vf32_t>& f_meanStorage ) const;

    const smd::array_view_vf32_t& 
    node_class_mean( int32_t                                f_nodeIdx_i32,
                     int32_t                                f_labelIdx_i32,
                     const std::vector<smd::array_vf32_t>&  f_meanStorage ) const;

    // ----------------------------------------------------------------------
    // Helpers
    inline int32_t flat_nl_index(
            int32_t f_nodeIdx_i32,
            int32_t f_labelIdx_i32 ) const { return ((num_labels() * f_nodeIdx_i32) + f_labelIdx_i32); }

    static 
    void update_co_moment( int32_t                        f_weight_i32,
                           int32_t&                       f_weightSum_ri32,
                           const smd::array_view_cvf32_t& f_val_af32,
                           smd::array_view_vf32_t&        f_mean_stat_af32,
                           smd::matrix_view_vf32_t&       f_co_stat_af32 );


    static
    void co_moment_to_covariance( int32_t f_norm_weight_i32,
            const smd::matrix_view_cf32_t& f_co_moment_af32,
                   smd::matrix_view_f32_t  f_cov_arf32 );
};


// -------------------------------------------------------------------------------------------------------


struct CMultiNodeClassStatistics : public CMultiNodeFirstPassStatistics
{

private:

    int32_t                          m_weightSum_i32;

    std::vector<smd::array_vf32_t>   m_second_pass_means_vaf32;   // [ N*L x [ F ] ]

    smd::matrix_vf32_t               m_intra_co_moment_vaf32;   // [ F x F ]

public:

    CMultiNodeClassStatistics( int32_t f_numLabels_i32, int32_t f_numNodes_i32, int32_t f_numFeatures_i32 );
    
    
    void process( const nodeIdx_t* nodeIter, const nodeIdx_t* const nodeIterEnd,
                  const int32_t* labelIter,
                  const int32_t* weightIter,
                  smd::bundle_iterator_cf32_t featureIter );


    int32_t get_intra_covariance( smd::matrix_view_f32_t& f_intraCov_r ) const;

private:

    void process_second_pass( const nodeIdx_t* nodeIter, const nodeIdx_t* const nodeIterEnd,
                              const int32_t* labelIter,
                              const int32_t* weightIter,
                              smd::bundle_iterator_cf32_t featureIter );


    inline smd::array_view_vf32_t& 
    second_pass_mean( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32 ) { 
        return m_second_pass_means_vaf32[ flat_nl_index(f_nodeIdx_i32, f_labelIdx_i32) ]; }
};



} // namespace tree

