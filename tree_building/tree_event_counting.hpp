/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <atomic>


namespace tree
{


struct EventStat
{
    typedef unsigned long long counter_t;

    // tree classification counters
    counter_t m_treeClassificationNumAligned_u;
    counter_t m_treeClassificationNumUnaligned_u;

    // fern_sample_projection counters
    counter_t m_sampleProjectionNumAligned_u;
    counter_t m_sampleProjectionNumUnaligned_u;

    // aligned vs non-aligned line search evaluation counters
    counter_t m_lineSearchEvalNumAligned_u;
    counter_t m_lineSearchEvalNumUnaligned_u;
};


// Singleton for event counts
struct EventCounter
{
    typedef unsigned long long counter_t;

private:

    friend EventCounter& evtcnt();

    // tree classification counters
    std::atomic<counter_t> m_treeClassificationNumAligned_u;
    std::atomic<counter_t> m_treeClassificationNumUnaligned_u;

    // fern_sample_projection counters
    std::atomic<counter_t> m_sampleProjectionNumAligned_u;
    std::atomic<counter_t> m_sampleProjectionNumUnaligned_u;

    // aligned vs non-aligned line search evaluation counters
    std::atomic<counter_t> m_lineSearchEvalNumAligned_u;
    std::atomic<counter_t> m_lineSearchEvalNumUnaligned_u;

    EventCounter();

public:

    operator EventStat() const;

    // tree classification counters
    inline counter_t increment_tree_classification_num_aligned( counter_t l_cnt_u = 1u );
    inline counter_t tree_classification_num_aligned() const;

    inline counter_t increment_tree_classification_num_unaligned( counter_t l_cnt_u = 1u );
    inline counter_t tree_classification_num_unaligned() const;


    // fern_sample_projection counters
    inline counter_t increment_sample_projection_num_aligned( counter_t l_cnt_u = 1u );
    inline counter_t sample_projection_num_aligned() const;

    inline counter_t increment_sample_projection_num_unaligned( counter_t l_cnt_u = 1u );
    inline counter_t sample_projection_num_unaligned() const;


    // aligned vs non-aligned line search evaluation counters
    inline counter_t increment_line_search_eval_num_aligned( counter_t l_cnt_u = 1u );
    inline counter_t line_search_eval_num_aligned() const;

    inline counter_t increment_line_search_eval_num_unaligned( counter_t l_cnt_u = 1u );
    inline counter_t line_search_eval_num_unaligned() const;
};

// singleton getter
EventCounter& evtcnt();


} // namespace tree

