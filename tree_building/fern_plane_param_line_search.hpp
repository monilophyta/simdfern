/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <limits>
#include <type_traits>
#include <utility>

#include "tree_plane_param_optimization_base.hpp"
#include "fern_weight_regularisation.hpp"
#include "tree_split_statistics_gini.hpp"
#include "tree_split_statistics_entropy.hpp"


namespace tree
{


struct limboSplit_t
{
    float32_t   m_prmVal_f32;

    inline limboSplit_t() { initialize(); }

    void initialize();
    void set( float32_t f_prmVal_f32 );
    inline float32_t get() const { return m_prmVal_f32; }
};


//-------------------------------------------------------------------------------------------


template <typename penalizer_T, typename split_stat_T>
struct CPlaneParamLineSearch : public CBasePlaneParamOptimizer< split_stat_T, CPlaneParamLineSearch< penalizer_T, split_stat_T> >
{   

public:

    typedef CPlaneParamLineSearch<penalizer_T,split_stat_T>   this_t;
    typedef CBasePlaneParamOptimizer< split_stat_T, this_t >  base_t;

    using typename base_t::rating_t;

    struct optimal_t
    {
        float32_t  m_prmVal_f32;

        rating_t   m_splitRating;
        float32_t  m_prmValPenalty_f32;


        inline optimal_t() { initialize(); }

        inline rating_t splitRating() const { return m_splitRating; }
        inline float32_t prmValPenalty() const { return m_prmValPenalty_f32; };

        inline float32_t optimRating() const { return (static_cast<float32_t>(splitRating()) + prmValPenalty()); }
    
        void initialize();
    };


private:

    using typename base_t::split_stat_t;


    optimal_t     m_optimal;
    limboSplit_t  m_limboSplit;

    penalizer_T   m_prmValPenalizer;

public:

    //-----------------------------------------------------------------------------------

    CPlaneParamLineSearch( int32_t f_numLabels_i32, 
                           int32_t f_numNodes_i32,
                const penalizer_T& f_prmValPenalizer = penalizer_T() );

    inline static constexpr size_t n_nodes() { return 1; }

    const int32_t*
    initialize_from( const nodeIdx_t* nodeIter, const nodeIdx_t* const nodeIterEnd,
                     const int32_t* labelIter, 
                     const int32_t* weightIter );
    
    //-----------------------------------------------------------------------------------

    void process_invalid_feature_sample( const int32_t, nodeIdx_t, const int32_t );


    EFeatureContrib start_feature_processing( const float32_t f_feature_f32,
                                              const int32_t   f_labelIdx_i32,
                                              const nodeIdx_t f_nodeIdx,
                                              const int32_t   f_sampleWeight_i32 );


    EFeatureContrib process_next_feature_sample( const float32_t f_prmVal_f32,
                                                 const int32_t   f_labelIdx_i32,
                                                 const nodeIdx_t f_nodeIdx,
                                                 const int32_t   f_sampleWeight_i32 );

    EFeatureContrib finish_feature_processing();

    //-----------------------------------------------------------------------------------

    inline const optimal_t& getOptimal() const { return m_optimal; }
    
    //-----------------------------------------------------------------------------------

    using base_t::process_sorted_feature;
    using base_t::process_unsorted_feature;

private:

    using base_t::leftStat;
    using base_t::rightStat;

    inline EFeatureContrib eval_param_value_candidate( float32_t f_centralVal_f32 );
    inline void flip_tree_sides( const int32_t   f_labelIdx_i32,
                                       nodeIdx_t f_nodeIdx, 
                                 const int32_t   f_sampleWeight_i32 );
};


//-------------------------------------------------------------------------------------------------------------------
// Explicit instantiation

#define INSTANTIATE_WITH_PENALIZER( PENAL_TYPE ) \
    extern template struct CBasePlaneParamOptimizer< CTreeSplitEntropyStatistics, CPlaneParamLineSearch< PENAL_TYPE, CTreeSplitEntropyStatistics > >; \
    extern template struct CBasePlaneParamOptimizer< CTreeSplitGiniStatistics,    CPlaneParamLineSearch< PENAL_TYPE, CTreeSplitGiniStatistics > >; \
    extern template struct CPlaneParamLineSearch< PENAL_TYPE, CTreeSplitEntropyStatistics >; \
    extern template struct CPlaneParamLineSearch< PENAL_TYPE, CTreeSplitGiniStatistics >;


INSTANTIATE_WITH_PENALIZER( CNoPenalizer );
INSTANTIATE_WITH_PENALIZER( CPenalizer );
INSTANTIATE_WITH_PENALIZER( CNormalizedL1Penalizer );
//INSTANTIATE_WITH_PENALIZER( CBaseVectPenalizer<float32_t> );
//INSTANTIATE_WITH_PENALIZER( CBaseVectPenalizer<smd::vf32_t> );
INSTANTIATE_WITH_PENALIZER( CVectPenalizer<float32_t> );
INSTANTIATE_WITH_PENALIZER( CVectPenalizer<smd::vf32_t> );
INSTANTIATE_WITH_PENALIZER( CNormalizedL1VectPenalizer<float32_t> );
INSTANTIATE_WITH_PENALIZER( CNormalizedL1VectPenalizer<smd::vf32_t> );

#undef INSTANTIATE_WITH_PENALIZER

} // namespace tree
