/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "tree_base_types.hpp"


namespace tree
{

/// factor the smallest possible treshold for the left-right comparison 
/// in golden section search is scaled with
static const int32_t GOLD_SECTION_SEARCH_TOL_FACTOR = 128;


}  // namespace tree

