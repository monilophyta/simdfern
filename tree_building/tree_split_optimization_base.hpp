/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <simd.hpp>
#include <cpp_tools.hpp>

#include "tree_node_index.hpp"
#include "tree_plane_param_optimization_base.hpp"


namespace tree
{

//-------------------------------------------------------------------------------------------


template <typename split_stat_T, typename derived_T>
struct CBaseSplitOptimizer  : public CBasePlaneParamOptimizer<split_stat_T,derived_T>
{   
private:

    typedef CBasePlaneParamOptimizer<split_stat_T,derived_T>                        base_t;

    /// Currently processed feature
    int32_t m_featureIdx_i32;

public:

    using typename base_t::split_stat_t;

    //-----------------------------------------------------------------------------------

    CBaseSplitOptimizer( int32_t f_numLabels_i32, int32_t f_numNodes_i32 );


    void initialize_feature_run(            int32_t  f_nextFeatureIdx_i32,
                                 const split_stat_t& f_previousLayerStat );

protected:

    inline int32_t featureIdx() const { return m_featureIdx_i32; }

};


//-------------------------------------------------------------------------------------------


} // namespace tree

