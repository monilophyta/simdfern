/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "tree_cfg.hpp"



namespace tree
{

template <typename idx_T, typename bool_T, typename derived_T>
inline constexpr auto
CNodeIdxBase<idx_T,bool_T,derived_T>::cast_from( const idx_t* ptr ) -> const derived_t*
{
    return reinterpret_cast<const derived_t*>( ptr );
}


template <typename idx_T, typename bool_T, typename derived_T>
inline constexpr auto
CNodeIdxBase<idx_T,bool_T,derived_T>::cast_from( idx_t* ptr ) -> derived_t*
{
    return reinterpret_cast<derived_t*>( ptr );
}


template <typename idx_T, typename bool_T, typename derived_T>
inline constexpr auto
CNodeIdxBase<idx_T,bool_T,derived_T>::cast_from( const idx_t& ptr ) -> const derived_t&
{
    return reinterpret_cast<const derived_t&>( ptr );
}


template <typename idx_T, typename bool_T, typename derived_T>
inline constexpr auto
CNodeIdxBase<idx_T,bool_T,derived_T>::cast_from( idx_t& ptr ) -> derived_t&
{
    return reinterpret_cast<derived_t&>( ptr );
}

//--------------- tree traversal functions  ----------------------------------------------------

template <typename idx_T, typename bool_T, typename derived_T>
inline auto
CNodeIdxBase<idx_T,bool_T,derived_T>::move_downwards() const -> derived_t
{  
    return derived_t( nodeact::move_downwards( m_value ) );
}

template <typename idx_T, typename bool_T, typename derived_T>
inline auto
CNodeIdxBase<idx_T,bool_T,derived_T>::move_downwards( bool_T l_decision_b ) const -> derived_t
{  
    return derived_t( nodeact::make_node_decision( l_decision_b,
                                   nodeact::move_downwards( m_value ) ) );
}


template <typename idx_T, typename bool_T, typename derived_T>
inline auto
CNodeIdxBase<idx_T,bool_T,derived_T>::pop_decision() -> bool_t
{  
    bool_t is_left_b = nodeact::is_left( m_value, 0 );
    m_value = nodeact::move_upwards( m_value );
    return is_left_b;
}


template <typename idx_T, typename bool_T, typename derived_T>
inline auto
CNodeIdxBase<idx_T,bool_T,derived_T>::finalize() const -> derived_t
{
    return derived_t( nodeact::finalize_node( m_value ) );
}



}  // namespace tree
