/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include <simd.hpp>
#include "tree_debug.hpp"
#include "tree_node_index.hpp"
#include "tree_classification.hpp"

#include "tree_node_index.inl"
#include "tree_event_counting.inl"



using namespace tree;


CIndexedTreeLayer::CIndexedTreeLayer()
    : m_featureIdx_ai32()
    , m_splitVals_af32()
{}



// -----------------------------------------------------------------------------------


template <typename RANDOM_ACCESS_T>
inline nodeIdx_t
CIndexedTreeLayer::classify_single_random_access(
    nodeIdx_t f_nodeIdx, 
    const RANDOM_ACCESS_T& f_featureVals_af32  ) const  // [ F ]
{
    if (f_nodeIdx.isFinal())
        return f_nodeIdx.move_downwards();
    
    const int32_t l_featureIdx_i32 = m_featureIdx_ai32[ f_nodeIdx.idx() ];

    if ( l_featureIdx_i32 < 0 )
        return f_nodeIdx.finalize().move_downwards();


    const bool l_comparison_b = 
        nodeact::feature_comparison( f_featureVals_af32[ l_featureIdx_i32 ],
                                       m_splitVals_af32[ f_nodeIdx.idx() ] );
    
    return f_nodeIdx.move_downwards( l_comparison_b );
}


// --------------------------------------------------


nodeIdx_t 
CIndexedTreeLayer::classify_single_data( 
    nodeIdx_t f_nodeIdx,
    const smd::array_view_cf32_t& f_featureVals_af32  ) const // [ F ]
{
    return classify_single_random_access( f_nodeIdx, f_featureVals_af32 );
}


nodeIdx_t 
CIndexedTreeLayer::classify_single_data( 
    nodeIdx_t f_nodeIdx,
    const smd::column_iterator_cf32_t& f_featureVals_af32  ) const  // [ F ]
{
    return classify_single_random_access( f_nodeIdx, f_featureVals_af32 );
}


// -----------------------------------------------------------------------------------


template <typename FEATURE_ACCESS_FUNC_T>
inline nodeIdx_vt
CIndexedTreeLayer::classify_multiple_random_access(
    const nodeIdx_vt&     f_nodeIdx, 
    FEATURE_ACCESS_FUNC_T f_af32_featureAccessFunc ) const
{
    const smd::vi32_t l_nodeIdx_vi32 = f_nodeIdx.idx();

    /// Select Feature Indices - Set invalid feature indices (closed nodes) to zero
    smd::vi32_t l_selectedFeatureIdx_vi32 = smd::gather( m_featureIdx_ai32, l_nodeIdx_vi32 ); // [ n ]

    /// Update classification request validity
    const smd::vb32_t l_validFeatureMask_vb32 = l_selectedFeatureIdx_vi32 >= 0;   // [ n ]

    // Set Node indices to negative values for invalid features
    const nodeIdx_vt l_nodeIdx( smd::blend(  l_validFeatureMask_vb32, 
                                             f_nodeIdx.m_value,      // Valid Tree nodes let keep data node indices
                                            -l_nodeIdx_vi32 ) );    // Invalid Tree nodes cause finalized data node indices

    // set feature index to zero if invalid
    l_selectedFeatureIdx_vi32 = std::max( l_selectedFeatureIdx_vi32, smd::vi32_t(0) );

    /// Select features from feature Accessor
    const smd::vf32_t l_selectedFeatures_vf32 = f_af32_featureAccessFunc( l_selectedFeatureIdx_vi32 ); // [ n ]
    
    /// Select splitting values
    const smd::vf32_t l_selectedSplitVals_vf32 = smd::gather( m_splitVals_af32, l_nodeIdx_vi32 ); // [ n ]

    /// compare values => invalid feature always become false
    const smd::vb32_t l_comparison_b32 = l_validFeatureMask_vb32 && 
                              nodeact::feature_comparison( l_selectedFeatures_vf32, l_selectedSplitVals_vf32 ); // [ n ]

    return l_nodeIdx.move_downwards( l_comparison_b32 );
}


nodeIdx_vt 
CIndexedTreeLayer::classify_multiple_data(
    const nodeIdx_vt&  f_nodeIdx_vi32,                              // [ n ]
    const smd::column_iterator_cvf32_t& f_featureVals_avf32 ) const  // [ F x n(=4/8/16) ]
{
    auto l_af32_featureAccessFunc = [&f_featureVals_avf32]( const smd::vi32_t& f_selectedFeatureIdx_vi32 ) -> smd::vf32_t
    {
        return smd::gather( f_featureVals_avf32, f_selectedFeatureIdx_vi32 );
    };

    return classify_multiple_random_access( f_nodeIdx_vi32, l_af32_featureAccessFunc );
}


nodeIdx_vt 
CIndexedTreeLayer::classify_multiple_data(
    const nodeIdx_vt&  f_nodeIdx_vi32,                             // [ n ]
    const smd::bundle_iterator_cf32_t& f_featureVals_af32 ) const  // [ F x n+... ]
{
    auto l_af32_featureAccessFunc = [&f_featureVals_af32]( const smd::vi32_t& f_selectedFeatureIdx_vi32 ) -> smd::vf32_t
    {
        return smd::gather( f_featureVals_af32, f_selectedFeatureIdx_vi32 );
    };
    
    return classify_multiple_random_access( f_nodeIdx_vi32, l_af32_featureAccessFunc );
}


// -------------------------------------------------------------------------------------------------------------


nodeIdx_vt 
CIndexedTreeLayer::classify_multiple_data(
    const smd::vi32_t& f_dataIdx_vi32,               // [ n ]
    const  nodeIdx_vt& f_nodeIdx_vi32,                // [ n ]
    const smd::matrix_view_cf32_t& f_featureMatrix_pmf32 ) const // [ N x F ]
{
    auto l_af32_featureAccessFunc = [&]( const smd::vi32_t& f_selectedFeatureIdx_vi32 ) -> smd::vf32_t
    {
        return smd::gather( f_featureMatrix_pmf32, f_dataIdx_vi32, f_selectedFeatureIdx_vi32 );
    };

    return classify_multiple_random_access( f_nodeIdx_vi32, l_af32_featureAccessFunc );
}


// -------------------------------------------------------------------------------------------------------------


void
CIndexedTreeLayer::classify_data_sequence( 
    const nodeIdx_t*              f_srcNodeIter,
    const nodeIdx_t*              f_srcNodeIterEnd,
    smd::bundle_iterator_cf32_t   f_dataColIter,
    nodeIdx_t*                    f_trgNodeIter ) const
{
    const auto l_numItems = std::distance( f_srcNodeIter, f_srcNodeIterEnd );
    
    const smd::v32_array_aligner l_arrayAligner( f_srcNodeIter, l_numItems );

    if ( (false == l_arrayAligner.has_aligned_memory() ) ||
         (false == l_arrayAligner.is_alignable_with( f_trgNodeIter )) )
    {   // node idx arrays are not alignable
        
        const nodeIdx_t* l_trgNodeIterEnd = generic_classify_data_sequence( f_srcNodeIter,
                                                                            f_srcNodeIterEnd,
                                                                            f_dataColIter,
                                                                            f_trgNodeIter );
        
        tree_assert( l_trgNodeIterEnd == std::next( f_trgNodeIter, l_numItems ),
                         "Error in iterator" );
        
        // update counter
        evtcnt().increment_tree_classification_num_unaligned( l_numItems );
    }
    else
    {   // node idx arrays should have some aligned memory

        // processing part before aligned memory
        if (l_arrayAligner.has_pre_memory())
        {
            const nodeIdx_t* l_trgNodeIterEnd = generic_classify_data_sequence( 
                l_arrayAligner.pre_begin( f_srcNodeIter ),
                l_arrayAligner.pre_end( f_srcNodeIter ),
                l_arrayAligner.pre_begin( f_dataColIter ),
                l_arrayAligner.pre_begin( f_trgNodeIter ) );
            
            tree_assert( l_trgNodeIterEnd == l_arrayAligner.pre_end( f_trgNodeIter ),
                         "Error in iterator" );
        }

        // processing aligned part
        {
            // We would not be here otherwise
            tree_assert( l_arrayAligner.has_aligned_memory(), "Inconsistent memory aligner" )
            
            static_assert( true == cppt::is_iterator<smd::bundle_iterator_cf32_t>::value );
            
            const nodeIdx_vt* l_trgNodeIterEnd = generic_classify_data_sequence( 
                nodeIdx_vt::cast_from( l_arrayAligner.aligned_begin_cast( nodeIdx_t::cast_to_idx( f_srcNodeIter ) ) ),
                nodeIdx_vt::cast_from(   l_arrayAligner.aligned_end_cast( nodeIdx_t::cast_to_idx( f_srcNodeIter ) ) ),
                l_arrayAligner.aligned_begin( f_dataColIter ),
                nodeIdx_vt::cast_from( l_arrayAligner.aligned_begin_cast( nodeIdx_t::cast_to_idx( f_trgNodeIter ) ) ) );
            
            tree_assert( l_trgNodeIterEnd == nodeIdx_vt::cast_from( l_arrayAligner.aligned_end_cast( nodeIdx_t::cast_to_idx( f_trgNodeIter ) ) ),
                         "Error in aligned memory iterator" );
        }

        tree_assert( f_srcNodeIterEnd == l_arrayAligner.post_end( f_srcNodeIter ), "Error in iterator" );

        // processing part after aligned memory
        if (l_arrayAligner.has_post_memory())
        {
            const nodeIdx_t* l_trgNodeIterEnd = generic_classify_data_sequence( 
                l_arrayAligner.post_begin( f_srcNodeIter ),
                l_arrayAligner.post_end( f_srcNodeIter ),
                l_arrayAligner.post_begin( f_dataColIter ),
                l_arrayAligner.post_begin( f_trgNodeIter ) );
            
            tree_assert( l_trgNodeIterEnd == l_arrayAligner.post_end( f_trgNodeIter ),
                         "Error in iterator" );
        }

        // update counter
        evtcnt().increment_tree_classification_num_aligned( l_arrayAligner.num_aligned() );
        evtcnt().increment_tree_classification_num_unaligned( l_arrayAligner.num_unaligned() );
    }
}



inline nodeIdx_t*
CIndexedTreeLayer::generic_classify_data_sequence( const nodeIdx_t*             f_srcNodeIter,
                                                   const nodeIdx_t*             f_srcNodeIterEnd,
                                                   smd::bundle_iterator_cf32_t  f_dataColIter,
                                                   nodeIdx_t*                   f_trgNodeIter ) const
{
    for (; f_srcNodeIter != f_srcNodeIterEnd;
            ++f_srcNodeIter, ++f_dataColIter, ++f_trgNodeIter )
    {
        *f_trgNodeIter = classify_single_data( *f_srcNodeIter, *f_dataColIter );
    }

    return f_trgNodeIter;
}



inline nodeIdx_vt*
CIndexedTreeLayer::generic_classify_data_sequence( const nodeIdx_vt*             f_srcNodeIter,
                                                   const nodeIdx_vt*             f_srcNodeIterEnd,
                                                   smd::bundle_iterator_cf32_t   f_dataColIter,
                                                   nodeIdx_vt*                   f_trgNodeIter ) const
{
    for (; f_srcNodeIter != f_srcNodeIterEnd;
            ++f_srcNodeIter,  ++f_trgNodeIter, f_dataColIter += nodeIdx_vt::idx_t::num_items() )
    {
        *f_trgNodeIter = classify_multiple_data( *f_srcNodeIter, f_dataColIter );
    }

    return f_trgNodeIter;
}
