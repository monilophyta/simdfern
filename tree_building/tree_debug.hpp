/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <simd.hpp>
#include <cpp_tools.hpp>





/// Assert that EXPRESSION evaluates to true, otherwise raise AssertionFailureException with associated MESSAGE 
/// (which may use C++ stream-style message formatting)
#define tree_assert(EXPRESSION, MESSAGE) cpp_assert(EXPRESSION, MESSAGE)

#define tree_input_check(EXPRESSION) tree_assert(EXPRESSION, "Bad Input")

#define tree_bounds_check(EXPRESSION) tree_assert(EXPRESSION, "Out of bounds access")
#define tree_dims_check(EXPRESSION) tree_assert(EXPRESSION, "Array/Matrix dimensions do not match")


#ifndef NO_EXPENSIVE_DEBUG
    #define tree_expensive_assert(EXPRESSION, MESSAGE) tree_assert(EXPRESSION, MESSAGE)
#else
    #define tree_expensive_assert(EXPRESSION, MESSAGE)
#endif




/// TODO
#ifndef IGNORE_TODO
    #define TREE_TODO( msg ) TODO_MESSAGE( msg )
#else
    #define TREE_TODO( msg )
#endif

