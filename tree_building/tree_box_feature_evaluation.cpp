/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "tree_box_feature_evaluation_base.hpp"
#include "tree_box_feature_evaluation_base.inl"
#include "tree_box_feature_evaluation.hpp"

using namespace img;


namespace tree
{


//-------------------------------------------------------------------------------------------------------------------------


template <typename IMG_T, typename FEATURE_FUNC, typename PARAM_T>
auto
CBoxFeatureEvaluatorI32< IMG_T, FEATURE_FUNC, PARAM_T >::evaluate_borderless_boxpair(
        const image_coord_i32_t& f_ankerIdx, 
        int32_t  f_featureIdx ) const -> feature_t
{
    image_bounds_check( static_cast<size_t>(f_featureIdx) < m_boxPairs.size() );
    image_bounds_check( m_imageFrame.convert_to_integral_image_frame().is_inside( f_ankerIdx ) );

    const int32_t l_ankerOffset = m_imageFrame.anker_offset( f_ankerIdx );

    return evaluate_borderless_boxpair( m_boxPairs[ f_featureIdx ], l_ankerOffset );
}


template <typename IMG_T, typename FEATURE_FUNC, typename PARAM_T>
auto
CBoxFeatureEvaluatorI32< IMG_T, FEATURE_FUNC, PARAM_T >::evaluate_borderchecked_boxpair(
        const image_coord_i32_t& f_ankerIdx, 
        int32_t  f_featureIdx ) const -> feature_t
{
    image_bounds_check( static_cast<size_t>(f_featureIdx) < m_boxPairs.size() );
    image_bounds_check( m_imageFrame.convert_to_integral_image_frame().is_inside( f_ankerIdx ) );

    const int32_t l_ankerOffset = m_imageFrame.anker_offset( f_ankerIdx );
    
    return evaluate_borderchecked_boxpair( f_ankerIdx, m_boxPairs[ f_featureIdx ], l_ankerOffset );
}


template <typename IMG_T, typename FEATURE_FUNC, typename PARAM_T>
auto
CBoxFeatureEvaluatorI32< IMG_T, FEATURE_FUNC, PARAM_T >::evaluate_borderchecked_boxpair(
        const image_coord_i32_t& f_ankerIdx, 
        const         boxpair_t& f_boxPair,
        int32_t    f_ankerOffset ) const -> feature_t
{
    image_bounds_check( m_imageFrame.convert_to_integral_image_frame().is_inside( f_ankerIdx ) );
    
    const bool l_isInside_b = m_imageFrame.is_feature_inside( f_boxPair.extent, f_ankerIdx );

    if ( l_isInside_b )
    {
        // Box is inside image
        return evaluate_borderless_boxpair( f_boxPair, f_ankerOffset );
    }
    else
    {
        // Box is not inside image
        return feature_value<feature_t>::invalid();
    }
}


//-------------------------------------------------------------------------------------------------------------------------

static inline 
img::intbox_vi32_t blend_int_box( const smd::vb32_t f_mask, const intbox_vi32_t& f_intbox )
{
    static const smd::vi32_t l_zero_vi32( 0 );

    /// fade between box and empty
    return intbox_vi32_t( smd::blend( f_mask, f_intbox.m_LeftUp,    l_zero_vi32 ),
                          smd::blend( f_mask, f_intbox.m_RightDown, l_zero_vi32 ),
                          smd::blend( f_mask, f_intbox.m_LeftDown,  l_zero_vi32 ),
                          smd::blend( f_mask, f_intbox.m_RightUp,   l_zero_vi32 ) );
}


template <typename IMG_T, typename FEATURE_FUNC, typename PARAM_T>
auto
CBoxFeatureEvaluatorV32< IMG_T, FEATURE_FUNC, PARAM_T >::evaluate_borderchecked_boxpair(
        const image_coord_i32_t& f_ankerIdx, 
        const         boxpair_t& f_boxPair,
        int32_t  f_ankerOffset ) const -> feature_t
{
    image_bounds_check( smd::all( m_imageFrame.convert_to_integral_image_frame().is_inside( f_ankerIdx ) ) );

    const smd::vb32_t l_isInside_b32 = m_imageFrame.is_feature_inside( f_boxPair.extent, f_ankerIdx );
    
    
    /// check which boxes are inside and which are not
    if ( true == smd::any( l_isInside_b32 ) )
    {   /// some features are valid

        if ( true == smd::all( l_isInside_b32 ) )
        {   // all are valid
            return evaluate_borderless_boxpair( f_boxPair, f_ankerOffset );
        }
        else
        {   // some are invalid

            const intbox_vi32_t l_firstBox  = blend_int_box( l_isInside_b32, f_boxPair.first );
            const intbox_vi32_t l_secondBox = blend_int_box( l_isInside_b32, f_boxPair.second );

            const feature_t l_firstFeature  =  l_firstBox.evaluate( m_imagePointer, static_cast<smd::vi32_t>( f_ankerOffset ) );
            const feature_t l_secondFeature = l_secondBox.evaluate( m_imagePointer, static_cast<smd::vi32_t>( f_ankerOffset ) );


            feature_func_t feature_func;
            
            const feature_t l_featureVal = feature_func( l_firstFeature, l_secondFeature, f_boxPair.param );

            return smd::blend( l_isInside_b32, l_featureVal, feature_value<feature_t>::invalid() );
        }
    }
    else 
    {
        return feature_value<feature_t>::invalid();
    }

}

//-------------------------------------------------------------------------------------------------------------------------

// explicit instantiation
template struct CBoxFeatureEvaluatorI32<   int32_t, boxratio_eval_func_t, dummy_param_t<0>>;
template struct CBoxFeatureEvaluatorI32< float32_t, boxratio_eval_func_t, dummy_param_t<0>>;
template struct CBoxFeatureEvaluatorI32<   int32_t,  boxdiff_eval_func_t,     float32_t>;
template struct CBoxFeatureEvaluatorI32< float32_t,  boxdiff_eval_func_t,     float32_t>;

// explicit instantiation
template struct CBoxFeatureEvaluatorV32<   int32_t, boxratio_eval_func_vt, dummy_param_t<sizeof(smd::vf32_t)>>;
template struct CBoxFeatureEvaluatorV32< float32_t, boxratio_eval_func_vt, dummy_param_t<sizeof(smd::vf32_t)>>;
template struct CBoxFeatureEvaluatorV32<   int32_t,  boxdiff_eval_func_vt, const smd::vf32_t&>;
template struct CBoxFeatureEvaluatorV32< float32_t,  boxdiff_eval_func_vt, const smd::vf32_t&>;


} // namespace tree
