/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "tree_debug.hpp"
#include "fern_binary_conversion.hpp"
#include "tree_split_statistics_base.hpp"

#include <limits>


namespace tree
{


template <typename split_stat_T>
CMultiClassFern2BinaryConverter<split_stat_T>::CMultiClassFern2BinaryConverter( 
        int32_t f_numLabels_i32,
        int32_t f_numLeafs_i32,
        uint8_t f_layerBitIdx_u8 )
    : m_leftStat( f_numLabels_i32, f_numLeafs_i32 >> 1 )
    , m_rightStat( f_numLabels_i32, f_numLeafs_i32 >> 1 )
    , m_layerBitMask_i32( int32_t(1) << int32_t(f_layerBitIdx_u8) )
    , m_lowerBitMask_i32( m_layerBitMask_i32 - 1 )
    , m_upperBitMask_i32( std::numeric_limits<int32_t>::max() ^ (m_layerBitMask_i32 | m_lowerBitMask_i32) )
{
    tree_assert( 0 <= m_lowerBitMask_i32, "Error in bitmask creation" );
    tree_assert( m_lowerBitMask_i32 < m_layerBitMask_i32, "Error in bitmask creation" );
    tree_assert( m_layerBitMask_i32 < m_upperBitMask_i32, "Error in bitmask creation" );

    tree_assert( 0 == (m_layerBitMask_i32 & m_lowerBitMask_i32), "Error in bitmask creation" );
    tree_assert( 0 == (m_layerBitMask_i32 & m_upperBitMask_i32), "Error in bitmask creation" );
    tree_assert( 0 == (m_lowerBitMask_i32 & m_upperBitMask_i32), "Error in bitmask creation" );
}


template <typename split_stat_T>
const int32_t*
CMultiClassFern2BinaryConverter<split_stat_T>::initialize_statistics( 
        const int32_t* leafIter, const int32_t* const leafIterEnd,
        const int32_t* labelIter,
        const int32_t* weightIter )
{
    /// For sake of performance: first initialize base statistics
    CTreeSplitStatisticsBase l_leftStat(  m_leftStat.n_labels(),  m_leftStat.n_nodes() );
    l_leftStat.initialize_zero();
    CTreeSplitStatisticsBase l_rightStat( m_rightStat.n_labels(), m_rightStat.n_nodes() );
    l_rightStat.initialize_zero();

    /// processing input
    for ( ; leafIter != leafIterEnd;
            ++leafIter, ++labelIter, ++weightIter )
    {
        const int32_t l_leafIdx_i32 = *leafIter;

        tree_input_check( 0 <= *leafIter );
        tree_input_check( 0 <= *labelIter );
        tree_input_check( 0 <= *weightIter );


        /// leaf index after exclusion of binary mapped layer index
        const int32_t l_exclNodeIdx_i32 = exclude_layer_idx_from( l_leafIdx_i32 );


        if ( is_left( l_leafIdx_i32 ) )
        {
            /// Left data assignment
            l_leftStat.increase_counts( l_exclNodeIdx_i32, *labelIter, *weightIter );
        }
        else
        {
            /// Right data assignment
            l_rightStat.increase_counts( l_exclNodeIdx_i32, *labelIter, *weightIter );
        }
    }

    // initialize real statistics
    m_leftStat.initialize_from( l_leftStat );
    m_rightStat.initialize_from( l_rightStat );
    
    return labelIter;
}


static inline constexpr EPlaneSide flip_plane_side( EPlaneSide f_side_e )
{
    return ( (f_side_e == EPlaneSide::LEFT) ? EPlaneSide::RIGHT : EPlaneSide::LEFT );
}



template <typename split_stat_T>
auto CMultiClassFern2BinaryConverter<split_stat_T>::map_sample( 
    int32_t f_leafIdx_i32,
    int32_t f_labelIdx_i32,
    int32_t f_weight_i32 ) const -> binary_mapping_t
{
    /// leaf index after exclusion of binary mapped layer index
    const int32_t l_exclNodeIdx_i32 = exclude_layer_idx_from( f_leafIdx_i32 );

    EPlaneSide l_layerSide_e = EPlaneSide::LEFT;
    rating_t l_changedSideRating(0);

    /// run mapping
    if ( is_left( f_leafIdx_i32 ) )
    {
        /// sample is asigned to left side

        /// Initial layer side
        l_layerSide_e = EPlaneSide::LEFT;

        /// rating if changing side from left to right
        l_changedSideRating = 
            m_leftStat.calc_decrease_rating( l_exclNodeIdx_i32, f_labelIdx_i32, f_weight_i32 ) +
            m_rightStat.calc_increase_rating( l_exclNodeIdx_i32, f_labelIdx_i32, f_weight_i32 );
    }
    else
    {
        /// sample is asigned to right side

        /// Initial layer side
        l_layerSide_e = EPlaneSide::RIGHT;

        /// sample is asigned to right side
        l_changedSideRating = 
            m_leftStat.calc_increase_rating( l_exclNodeIdx_i32, f_labelIdx_i32, f_weight_i32 ) +
            m_rightStat.calc_decrease_rating( l_exclNodeIdx_i32, f_labelIdx_i32, f_weight_i32 );
    }

    /// initialization with default (no side change)
    binary_mapping_t l_bm = { l_layerSide_e, 
                              std::abs( l_changedSideRating ) };

    /// check if changing the layer side brings an improvemnet ( smaller is better )
    if ( 0 > l_changedSideRating )
    {
        /// yes, changing side brings an improvement
        l_bm.m_planeSide    = flip_plane_side( l_layerSide_e );
        tree_assert( l_bm.m_planeSide != l_layerSide_e, "Invalid plane side mapping" );
    }

    tree_assert( (l_bm.m_planeSide == EPlaneSide::LEFT) || (l_bm.m_planeSide == EPlaneSide::RIGHT), 
                 "Invalid enum mapping" );
    tree_assert( l_bm.m_weightRating >= 0, "Invalid weight rating" );

    return l_bm;
}


template <typename split_stat_T>
inline int32_t 
CMultiClassFern2BinaryConverter<split_stat_T>::exclude_layer_idx_from( int32_t f_leafIdx_i32 ) const
{
    const int32_t l_exclNodeIdx_i32 = 
        ( ( f_leafIdx_i32 & m_upperBitMask_i32 ) >> 1 ) | ( f_leafIdx_i32 & m_lowerBitMask_i32 );
    
    tree_assert( (        f_leafIdx_i32 & m_lowerBitMask_i32 ) ==
                    ( l_exclNodeIdx_i32 & m_lowerBitMask_i32 ), "Error in layer bit removal" );
    tree_assert( (              f_leafIdx_i32 & m_upperBitMask_i32 ) ==
                   ( (l_exclNodeIdx_i32 << 1) & m_upperBitMask_i32 ), "Error in layer bit removal" );
    tree_assert( l_exclNodeIdx_i32 <= f_leafIdx_i32, "Error in layer bit removal" );
    tree_assert( l_exclNodeIdx_i32 < m_leftStat.n_leafs(), "Error in layer bit removal" );
    
    return l_exclNodeIdx_i32;
}


template <typename split_stat_T>
inline bool CMultiClassFern2BinaryConverter<split_stat_T>::is_left( int32_t f_leafIdx_i32 ) const
{
    return (0 != ( f_leafIdx_i32 & m_layerBitMask_i32 ));
}


//-------------------------------------------------------------------------------------------


// instantiation in cpp
template class CMultiClassFern2BinaryConverter< CTreeSplitEntropyStatistics >;
template class CMultiClassFern2BinaryConverter< CTreeSplitGiniStatistics >;

//-------------------------------------------------------------------------------------------




} // namespace tree
