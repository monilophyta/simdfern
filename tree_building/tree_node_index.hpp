/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <type_traits>
#include "tree_cfg.hpp"
#include "tree_math.hpp"
#include "tree_node_actions.hpp"



namespace tree
{

template <typename idx_T, typename bool_T, typename derived_T>
struct CNodeIdxBase
{
private:

    typedef CNodeIdxBase<idx_T,bool_T,derived_T>            this_t;


public:


    typedef typename std::remove_const<
                typename std::remove_reference<idx_T>::type>::type   idx_t;
    typedef typename std::remove_const<
                typename std::remove_reference<bool_T>::type>::type  bool_t;
    typedef derived_T                                                derived_t;

    idx_t m_value;

    static inline constexpr const derived_t* cast_from( const idx_t* ptr );
    static inline constexpr derived_t* cast_from( idx_t* ptr );
    static inline constexpr const derived_t& cast_from( const idx_t& ptr );
    static inline constexpr derived_t& cast_from( idx_t& ptr );

    static inline constexpr idx_t* cast_to_idx( this_t* ptr ) {return &(ptr->m_value); };
    static inline constexpr const idx_t* cast_to_idx( const this_t* ptr ) {return &(ptr->m_value); };


    inline
    CNodeIdxBase() : m_value(0)
    {}

    explicit inline 
    CNodeIdxBase( idx_T f_value ) : m_value( f_value )
    {}


    //inline explicit operator idx_t() const { return this->idx(); }
    inline idx_t idx() const { return std::abs( m_value ); };

    //--- tree traversal functions  -------------------------------- 
    
    inline derived_t move_downwards() const;
    inline derived_t move_downwards( bool_T l_decision_b ) const;

    /*!
      \brief removes lowest bit (last plane decision) and returns it

      \return true: if the lowest (removed) bit was set
    */
    inline bool_t pop_decision();

    inline derived_t finalize() const;


    inline constexpr 
    bool_t
    isFinal() const { return nodeact::isFinalNode( m_value ); }

};


// --------------------------------------------------------------------------------

struct CNodeIdx 
    : public CNodeIdxBase<int32_t,bool,CNodeIdx>
{
private:

    typedef CNodeIdxBase<int32_t,bool,CNodeIdx>   base_t;


public:

    using base_t::CNodeIdxBase;

};

static_assert( sizeof(CNodeIdx) == sizeof(int32_t) );

// --------------------------------------------------------------------------------


struct CNodeIdxVect
    : public CNodeIdxBase< const smd::vi32_t&, 
                           const smd::vb32_t&,
                           CNodeIdxVect>
{
private:
    typedef CNodeIdxBase< const smd::vi32_t&, 
                          const smd::vb32_t&,
                          CNodeIdxVect>          base_t;

public:

    using base_t::CNodeIdxBase;

    
    inline operator int32_t*()             { return m_value.begin(); }
    inline operator const int32_t*() const { return m_value.cbegin(); }
};

static_assert( sizeof(CNodeIdxVect) == sizeof(smd::vi32_t) );

typedef CNodeIdx      nodeIdx_t;
typedef CNodeIdxVect  nodeIdx_vt;


}  // namespace tree
