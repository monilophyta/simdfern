/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <limits>
#include "tree_split_optimization_base.hpp"

#include "tree_split_statistics_gini.hpp"
#include "tree_split_statistics_entropy.hpp"


namespace tree
{


//-------------------------------------------------------------------------------------------

template <typename split_stat_T>
struct CFernFeatureSplitOptimizer;

typedef CFernFeatureSplitOptimizer< CTreeSplitGiniStatistics >        fern_split_gini_optimizer_t;
typedef CFernFeatureSplitOptimizer< CTreeSplitEntropyStatistics >  fern_split_entropy_optimizer_t;


//-------------------------------------------------------------------------------------------


template <typename split_stat_T>
struct CFernFeatureSplitOptimizer : public CBaseSplitOptimizer< split_stat_T, CFernFeatureSplitOptimizer<split_stat_T> >
{   

private:

    typedef CFernFeatureSplitOptimizer<split_stat_T>               this_t;
    typedef CBaseSplitOptimizer< split_stat_T, this_t >            base_t;

    using typename base_t::rating_t;
    using typename base_t::split_stat_t;


    struct optimal_t;
    struct limboSplit_t;


    optimal_t            m_optimal;
    limboSplit_t         m_limboSplit;

public:

    //-----------------------------------------------------------------------------------

    CFernFeatureSplitOptimizer( int32_t f_numLabels_i32, int32_t f_numNodes_i32 );
    
    // reset memory about previous optimal splits
    void reset_optimal_split();

    inline static constexpr size_t n_nodes() { return 1; }

    void initialize_feature_run(            int32_t  f_nextFeatureIdx_i32,
                                 const split_stat_t& f_previousLayerStat );
    
    //-----------------------------------------------------------------------------------

    void process_invalid_feature_sample( const int32_t   f_labelIdx_i32,
                                         const nodeIdx_t f_nodeIdx,
                                         const int32_t   f_weight_i32 );

    inline
    EFeatureContrib start_feature_processing( const float32_t f_feature_f32,
                                              const int32_t   f_labelIdx_i32,
                                              const nodeIdx_t f_nodeIdx, 
                                              const int32_t   f_weight_i32 )
    {
        return process_next_feature_sample( f_feature_f32, f_labelIdx_i32, f_nodeIdx, f_weight_i32 );
    }


    EFeatureContrib process_next_feature_sample( const float32_t f_feature_f32,
                                                 const int32_t   f_labelIdx_i32,
                                                 const nodeIdx_t f_nodeIdx, 
                                                 const int32_t   f_weight_i32 );

    inline
    EFeatureContrib finish_feature_processing() { return EFeatureContrib::NO_IMPROVEMENT; }

    //-----------------------------------------------------------------------------------

    inline const optimal_t& getOptimalFeatureSplit() const { return m_optimal; }
    
    //-----------------------------------------------------------------------------------

    using base_t::process_sorted_feature;
    using base_t::process_unsorted_feature;

private:

    using base_t::leftStat;
    using base_t::rightStat;
    using base_t::featureIdx;

    inline EFeatureContrib update_optimal_split( rating_t f_nodeRating, float32_t f_featureVal_f32 );


    struct optimal_t
    {
        rating_t        m_nodeRating;
        int32_t         m_featureIdx_i32;
        float32_t       m_featureSplit_f32;

        EFeatureContrib m_nextSplitImprovement_e;

        inline optimal_t();

        inline rating_t nodeRating() { return m_nodeRating; }
    };


    struct limboSplit_t
    {
        float32_t   m_featureVal_f32;

        inline limboSplit_t() { initialize(); }

        inline void initialize() { m_featureVal_f32 = -std::numeric_limits<float32_t>::max(); }
        inline void set( float32_t f_featureVal_f32 ) { m_featureVal_f32 = f_featureVal_f32; }
        inline float32_t get() { return m_featureVal_f32; }
    };
};


//-------------------------------------------------------------------------------------------

// Announcing explicit instantiation in cpp

extern template struct CBasePlaneParamOptimizer< CTreeSplitEntropyStatistics, fern_split_entropy_optimizer_t >;
extern template struct CBasePlaneParamOptimizer< CTreeSplitGiniStatistics, fern_split_gini_optimizer_t >;

extern template struct CBaseSplitOptimizer< CTreeSplitEntropyStatistics, fern_split_entropy_optimizer_t >;
extern template struct CBaseSplitOptimizer< CTreeSplitGiniStatistics, fern_split_gini_optimizer_t >;

extern template struct CFernFeatureSplitOptimizer< CTreeSplitEntropyStatistics >;
extern template struct CFernFeatureSplitOptimizer< CTreeSplitGiniStatistics >;

//-------------------------------------------------------------------------------------------


} // namespace tree
