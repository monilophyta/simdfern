/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <iterator>
#include <cmath>
#include <algorithm>
#include <numeric>
#include "simd.hpp"



namespace tree
{





#ifdef ENABLE_UNUSED

template <typename ITER_T,
          typename ENTROPY_T = std::conditional< 
                                   std::is_integral< std::iterator_traits<ITER_T>::value_type >::value,
                                   float32_t,
                                   smd::vf32_t>::type>
inline float32_t
unormed_weighted_entropy( const ITER_T& begin, const ITER_T& end )
{
    typedef typename std::iterator_traits<ITER_T>::value_type elem_t;
    
    // Type containing (first) total sum of elements (second) sum of x*logx
    typedef std::pair<elem_t,ENTROPY_T>  accu_t;

    auto kernel = []( const accu_t& lastv, const elem_t& newv) -> accu_t 
    {
        return accu_t( lastv.first + newv,
                       lastv.second + ( 
                           ENTROPY_T(newv) * std::log( std::max( ENTROPY_T(newv), 
                                                                 std::numeric_limits<ENTROPY_T>::min() ) ) ));
    };

    // build statistics
    const accu_t accu = std::accumulate( begin, end, accu_t( elem_t(0), ENTROPY_T(0) ), kernel );

    // summarized counts
    const float32_t l_counts_f32 = static_cast<float32_t>( smd::sum( accu.first ) );
    
    // Build entripy
    float32_t l_entropy_f32 = static_cast<float32_t>( smd::sum( accu.second ) );
    l_entropy_f32 += l_sumlog_f32 * std::log( std::max( l_counts_f32,
                                                        std::numeric_limits<float32_t>::min() ) );

    return l_entropy_f32;
}

#endif // ENABLE_UNUSED

} // namespace tree
