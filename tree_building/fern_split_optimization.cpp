/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "fern_split_optimization.hpp"

#include "tree_plane_param_optimization_base.hpp"
#include "tree_plane_param_optimization_base.inl"
#include "tree_split_optimization_base.hpp"
#include "tree_split_optimization_base.inl"

#include "tree_box_feature.hpp"
#include "tree_enums.hpp"



namespace tree 
{


//---------------------------------------------------------------------------------------------

template <typename split_stat_T>
CFernFeatureSplitOptimizer<split_stat_T>::optimal_t::optimal_t()
    : m_nodeRating(  std::numeric_limits<rating_t>::max() )
    , m_featureIdx_i32(   evalue(ELeafCode::UNDEFINED_SPLIT_INDEX) )
    , m_featureSplit_f32( std::numeric_limits<float32_t>::signaling_NaN() )
    , m_nextSplitImprovement_e( EFeatureContrib::NO_IMPROVEMENT )
{}

//---------------------------------------------------------------------------------------------


template <typename split_stat_T>
CFernFeatureSplitOptimizer<split_stat_T>::CFernFeatureSplitOptimizer ( 
        int32_t f_numLabels_i32, 
        int32_t f_numNodes_i32 )

    : base_t( f_numLabels_i32, f_numNodes_i32 )
    , m_optimal()
    , m_limboSplit()
{
    // nothing to do
}


template <typename split_stat_T>
void
CFernFeatureSplitOptimizer<split_stat_T>::reset_optimal_split()
{
    m_optimal = optimal_t();
    m_limboSplit = limboSplit_t();
}


template <typename split_stat_T>
void
CFernFeatureSplitOptimizer<split_stat_T>::initialize_feature_run( 
                   int32_t  f_nextFeatureIdx_i32,
        const split_stat_t& f_previousLayerStat )
{
    base_t::initialize_feature_run( f_nextFeatureIdx_i32, f_previousLayerStat );
    
    m_limboSplit.initialize();
}


//----------------------------------------------------------------------------------------------


template <typename split_stat_T>
void
CFernFeatureSplitOptimizer<split_stat_T>::process_invalid_feature_sample(
            const int32_t   f_labelIdx_i32,
            const nodeIdx_t f_nodeIdx,
            const int32_t   f_weight_i32 )
{
    /// check if node has already been finished
    if ( false == f_nodeIdx.isFinal() )
    {
        /// update left and right statistics
        int32_t l_halfWeight_i32 = f_weight_i32 / 2;
        
        leftStat().increase_counts(  f_nodeIdx.idx(), f_labelIdx_i32, l_halfWeight_i32 );
        rightStat().decrease_counts( f_nodeIdx.idx(), f_labelIdx_i32, l_halfWeight_i32 );
    }
}


template <typename split_stat_T>
EFeatureContrib
CFernFeatureSplitOptimizer<split_stat_T>::process_next_feature_sample( 
            const float32_t f_feature_f32,
            const int32_t   f_labelIdx_i32,
            const nodeIdx_t f_nodeIdx,
            const int32_t   f_weight_i32 )
{
    EFeatureContrib l_improvement = EFeatureContrib::NO_IMPROVEMENT;

    /// verify if split between limbo feature and current feature is possible
    const bool l_splitPossible_b = 
        nodeact::feature_comparison( nextafterf( m_limboSplit.get(), MAX_FLOAT32 ), f_feature_f32 );
    
    if ( true == l_splitPossible_b )
    {
        const rating_t l_nodeRating = leftStat().total_rating() + rightStat().total_rating();
        
        /// compare entropies
        if ( l_nodeRating < m_optimal.nodeRating() )
        {
            // there is an improvement
            l_improvement = update_optimal_split( l_nodeRating, f_feature_f32 );
        }
    }

    /// move data sample from right to left side - update left and right statistics
    leftStat().increase_counts(  f_nodeIdx.idx(), f_labelIdx_i32, f_weight_i32 );
    rightStat().decrease_counts( f_nodeIdx.idx(), f_labelIdx_i32, f_weight_i32 );

    
    /// set new limbo split
    m_limboSplit.set( f_feature_f32  );

    return l_improvement;
}


//----------------------------------------------------------------------------------------------


template <typename split_stat_T>
EFeatureContrib
CFernFeatureSplitOptimizer<split_stat_T>::update_optimal_split( rating_t f_nodeRating, float32_t f_featureVal_f32 )
{
    tree_assert( f_nodeRating < m_optimal.m_nodeRating, "Nonoptimal update" );

    float32_t l_new_featureSplit_f32 =   (0.5f * f_featureVal_f32) 
                                       + (0.5f * m_limboSplit.m_featureVal_f32);
    
    tree_assert(     (m_optimal.m_featureIdx_i32 != featureIdx()) 
                  || (m_optimal.m_featureSplit_f32 < l_new_featureSplit_f32),
                 "Inconsistent split change" )

    m_optimal.m_featureSplit_f32 = l_new_featureSplit_f32;
    m_optimal.m_nodeRating = f_nodeRating;
    m_optimal.m_featureIdx_i32 = featureIdx();

    // ensuring that first returned improvement is from initialization
    const EFeatureContrib l_improvement = m_optimal.m_nextSplitImprovement_e;
    m_optimal.m_nextSplitImprovement_e = EFeatureContrib::IMPROVEMENT;

    return l_improvement;
}

//----------------------------------------------------------------------------------------------

// instantiation in cpp

template struct CBasePlaneParamOptimizer< CTreeSplitEntropyStatistics, fern_split_entropy_optimizer_t >;
template struct CBasePlaneParamOptimizer< CTreeSplitGiniStatistics, fern_split_gini_optimizer_t >;

template struct CBaseSplitOptimizer< CTreeSplitEntropyStatistics, fern_split_entropy_optimizer_t >;
template struct CBaseSplitOptimizer< CTreeSplitGiniStatistics, fern_split_gini_optimizer_t >;

template struct CFernFeatureSplitOptimizer< CTreeSplitEntropyStatistics >;
template struct CFernFeatureSplitOptimizer< CTreeSplitGiniStatistics >;

//-------------------------------------------------------------------------------------------


} // namespace tree
