/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "tree_box_feature_evaluation_base.hpp"
#include <algorithm>


namespace tree
{


template <typename BASE_T, typename DERIVED_T>
CBoxFeatureEvaluatorBase< BASE_T, DERIVED_T >::CBoxFeatureEvaluatorBase(
        const img::image_frame_t&              f_imageFrame,
        const img::CImagePointer<const img_t>& f_imagePointer,
        size_t                                 f_numFeatures )
    : m_imageFrame( f_imageFrame )
    , m_imagePointer( f_imagePointer )
    , m_boxPairs()
    , m_allOverExtent()
{
    m_boxPairs.reserve( f_numFeatures );
}



template <typename BASE_T, typename DERIVED_T>
template <typename BT> 
inline void
CBoxFeatureEvaluatorBase< BASE_T, DERIVED_T >::emplace_boxpair( 
        const img::CBox<BT>& f_first,
        const img::CBox<BT>& f_second,
        feature_param_constr_t f_param )
{
    m_boxPairs.emplace_back( m_imageFrame.offset_free_intbox( f_first ),
                             m_imageFrame.offset_free_intbox( f_second ),
                             feature_extent_t::create_feature_extent( f_first, f_second ),
                             f_param );
    
    m_allOverExtent.union_update( m_boxPairs.back().extent );
}


template <typename BASE_T, typename DERIVED_T>
template <typename iter_T>
inline iter_T
CBoxFeatureEvaluatorBase< BASE_T, DERIVED_T >::evaluate_location(
        const img::image_coord_i32_t& f_ankerIdx, 
        iter_T f_storeIter )  const
{
    static_assert( is_correct_iter<iter_T, feature_t>::value, "iterator value type does not match feature type"  );
    
    const bool l_isDefinitlyInside_b = m_imageFrame.is_feature_inside( m_allOverExtent, f_ankerIdx );

    if (true == l_isDefinitlyInside_b)
    {
        return derived().evaluate_borderless_location( f_ankerIdx, f_storeIter );
    }
    else
    {
        return derived().evaluate_borderchecked_location( f_ankerIdx, f_storeIter );
    }
}



template <typename BASE_T, typename DERIVED_T>
template <typename iter_T>
inline iter_T
CBoxFeatureEvaluatorBase< BASE_T, DERIVED_T >::evaluate_locations(
        const img::image_coord_i32_t* f_begin,
        const img::image_coord_i32_t* f_end,
        iter_T f_storeIter ) const
{
    static_assert( is_correct_iter<iter_T, feature_t>::value, "iterator value type does not match feature type"  );
    
    for ( auto it  = f_begin; 
               it != f_end;
               ++it )
    {
        f_storeIter = derived().evaluate_location( *it, f_storeIter );
    }
    return f_storeIter;
}




template <typename BASE_T, typename DERIVED_T>
template <typename iter_T>
inline iter_T
CBoxFeatureEvaluatorBase< BASE_T, DERIVED_T >::evaluate_borderless_location(
        const img::image_coord_i32_t& f_ankerIdx,
        iter_T f_storeIter ) const
{
    static_assert( is_correct_iter<iter_T, feature_t>::value, "iterator value type does not match feature type"  );
    
    image_bounds_check( m_imageFrame.convert_to_integral_image_frame().is_inside( f_ankerIdx ) );

    const int32_t l_ankerOffset = m_imageFrame.anker_offset( f_ankerIdx );

    auto locationKernel = [=] ( const boxpair_t& f_boxPair )
    {
        return derived().evaluate_borderless_boxpair( f_boxPair, l_ankerOffset );
    };

    return std::transform( m_boxPairs.cbegin(), m_boxPairs.cend(),
                           f_storeIter,
                           locationKernel );
}


template <typename BASE_T, typename DERIVED_T>
template <typename iter_T>
inline iter_T
CBoxFeatureEvaluatorBase< BASE_T, DERIVED_T >::evaluate_borderchecked_location(
        const img::image_coord_i32_t& f_ankerIdx, 
        iter_T f_storeIter ) const
{
    static_assert( is_correct_iter<iter_T, feature_t>::value, "iterator value type does not match feature type"  );

    image_bounds_check( m_imageFrame.convert_to_integral_image_frame().is_inside( f_ankerIdx ) );
    
    const int32_t l_ankerOffset = m_imageFrame.anker_offset( f_ankerIdx );

    auto locationKernel = [=,&f_ankerIdx] ( const boxpair_t& f_boxPair )
    { 
        return derived().evaluate_borderchecked_boxpair( f_ankerIdx, f_boxPair, l_ankerOffset );
    };

    return std::transform( m_boxPairs.cbegin(), m_boxPairs.cend(),
                           f_storeIter,
                           locationKernel );
}

// ---------------------------------------------------------------------------------------------


template <typename BASE_T, typename DERIVED_T>
auto
CBoxFeatureEvaluatorBase< BASE_T, DERIVED_T >::evaluate_borderless_boxpair( 
        const boxpair_t& f_boxPair,
                 int32_t f_ankerOffset ) const -> feature_t
{
    feature_func_t feature_func;
    
    typedef typename intbox_t::idx_t   ankerOffset_t;
    
    const auto  l_firstIntegral =  f_boxPair.first.evaluate( m_imagePointer, static_cast<ankerOffset_t>( f_ankerOffset ) );
    const auto l_secondIntegral = f_boxPair.second.evaluate( m_imagePointer, static_cast<ankerOffset_t>( f_ankerOffset ) );

    feature_t l_feature = feature_func( l_firstIntegral, l_secondIntegral, f_boxPair.param );

    return l_feature;
}



} // namespace tree

