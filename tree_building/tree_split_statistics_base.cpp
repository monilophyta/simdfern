/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */



#include "tree_split_statistics_base.hpp"
#include "tree_debug.hpp"
#include <algorithm>


namespace tree
{



CTreeSplitStatisticsBase::CTreeSplitStatisticsBase( 
    int32_t f_numLabels_i32, 
    int32_t f_numNodes_i32 )

    : m_CorrelationCounts_ai32( f_numNodes_i32, f_numLabels_i32 )  // [ #Leaf x #Label ]
    , m_Counts_ai32( f_numNodes_i32 )                              // [ #Leaf ]
{}


void CTreeSplitStatisticsBase::initialize_zero()
{
    m_CorrelationCounts_ai32.fill( smd::vi32_t( int32_t(0) ) );
    m_Counts_ai32.fill( smd::vi32_t( int32_t(0) ) );
}


void CTreeSplitStatisticsBase::initialize_from( const CTreeSplitStatisticsBase& other )
{
    tree_dims_check( m_CorrelationCounts_ai32.elem().size() == other.m_CorrelationCounts_ai32.elem().size() );
    tree_dims_check(            m_Counts_ai32.elem().size() == other.m_Counts_ai32.elem().size() );
    
    m_CorrelationCounts_ai32.deep_copy_from( other.m_CorrelationCounts_ai32 );
    m_Counts_ai32.deep_copy_from( other.m_Counts_ai32 );
}


const int32_t* CTreeSplitStatisticsBase::initialize_from(
            const nodeIdx_t* nodeIter, const nodeIdx_t* const nodeIterEnd,
            const   int32_t* labelIter,
            const   int32_t* weightIter )
{
    tree_assert( 0 <= nodeIter->idx(), "invalid node indices" );
    initialize_zero();

    for ( ; nodeIter != nodeIterEnd;
            ++nodeIter, ++labelIter, ++weightIter )
    {
        tree_input_check( 0 <= *weightIter );

        
        m_CorrelationCounts_ai32.elem( nodeIter->idx(), *labelIter ) += *weightIter;
                   m_Counts_ai32.elem( nodeIter->idx() )             += *weightIter;
    }
    
    return labelIter;
}


int32_t
CTreeSplitStatisticsBase::increase_counts( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 )
{
    tree_input_check( 0 <= f_weight_i32 );
    tree_assert( 0 <= f_nodeIdx_i32, "invalid node indices" );
    
    /// store old count of correlation that is going to be modified afterwards
    const int32_t l_oldCorrCount_i32 = m_CorrelationCounts_ai32.elem( f_nodeIdx_i32, f_labelIdx_i32 );

    /// increase counts (usually on left side)
    m_CorrelationCounts_ai32.elem( f_nodeIdx_i32, f_labelIdx_i32 ) += f_weight_i32;
               m_Counts_ai32.elem( f_nodeIdx_i32 )                 += f_weight_i32;

    /// return old correlation count before modification
    return l_oldCorrCount_i32;
}


int32_t
CTreeSplitStatisticsBase::decrease_counts( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 )
{
    tree_input_check( 0 <= f_weight_i32 );
    tree_assert( 0 <= f_nodeIdx_i32, "invalid node indices" );
    
    /// store old count of correlation that is going to be modified afterwards
    const int32_t l_oldCorrCount_i32 = m_CorrelationCounts_ai32.elem( f_nodeIdx_i32, f_labelIdx_i32 );

    /// decrease counts (usually on right side)
    m_CorrelationCounts_ai32.elem( f_nodeIdx_i32, f_labelIdx_i32 ) -= f_weight_i32;
               m_Counts_ai32.elem( f_nodeIdx_i32 )                 -= f_weight_i32;
    
    tree_assert( 0 <= m_CorrelationCounts_ai32.elem( f_nodeIdx_i32, f_labelIdx_i32 ), 
                 "Inconsistent data counts in node" );
    tree_assert( 0 <= m_Counts_ai32.elem( f_nodeIdx_i32 ), "Inconsistent data counts in tree node" );
    
    /// return old correlation count before modification
    return l_oldCorrCount_i32;
}



bool CTreeSplitStatisticsBase::assert_correctness() const
{
    bool check = true;
    int32_t total_sum = 0;

    for ( int32_t leafIdx = 0; leafIdx < n_leafs(); ++leafIdx )
    {
        const int32_t csum = smd::sum( m_CorrelationCounts_ai32.row( leafIdx ) );

        tree_assert( csum >= 0, "inconsistent counts" );
        tree_assert( csum == m_Counts_ai32.elem( leafIdx ), "inconsistent counts" );
        check = check && ( csum == m_Counts_ai32.elem( leafIdx ) );

        total_sum += csum;
    }

    tree_assert( total_sum == smd::sum( m_Counts_ai32 ), "inconsistent counts" );

    return check;
}


} // namespace tree
