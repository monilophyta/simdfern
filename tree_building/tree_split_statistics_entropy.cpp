/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */



#include "tree_split_statistics_entropy.hpp"
#include "tree_debug.hpp"
#include <algorithm>


namespace tree
{



CTreeSplitEntropyStatistics::CTreeSplitEntropyStatistics( 
    int32_t f_numLabels_i32, 
    int32_t f_numNodes_i32 )

    : base_t( f_numLabels_i32, f_numNodes_i32 )
    , m_XlogX_af32( f_numNodes_i32, f_numLabels_i32 )              // [ #Leaf x #Label ]
    , m_unormedEntropies_af32( f_numNodes_i32 )
{}


void CTreeSplitEntropyStatistics::initialize_zero()
{
    base_t::initialize_zero();
    m_XlogX_af32.fill( smd::vf32_t( float32_t(0) ) );
    m_unormedEntropies_af32.fill( smd::vf32_t( float32_t(0) ) );

    tree_assert( assert_correctness(), "inconsistent statistics" );
}


void CTreeSplitEntropyStatistics::initialize_from( const CTreeSplitEntropyStatistics& other )
{
    base_t::initialize_from( other );

    tree_dims_check( m_XlogX_af32.elem().size() == other.m_XlogX_af32.elem().size() );

    m_XlogX_af32.deep_copy_from( other.m_XlogX_af32 );
    m_unormedEntropies_af32.deep_copy_from( other.m_unormedEntropies_af32 );

    tree_assert( assert_correctness(), "inconsistent statistics" );
}


void CTreeSplitEntropyStatistics::initialize_from( const CTreeSplitStatisticsBase& other )
{
    base_t::initialize_from( other );

    recalculate_entropy_stats();
    
    tree_assert( assert_correctness(), "inconsistent statistics" );
}


const int32_t* 
CTreeSplitEntropyStatistics::initialize_from(
        const nodeIdx_t* nodeIter, const nodeIdx_t* const nodeIterEnd,
        const   int32_t* labelIter,
        const   int32_t* weightIter )
{
    labelIter = base_t::initialize_from( nodeIter, nodeIterEnd, labelIter, weightIter );

    recalculate_entropy_stats();
    
    tree_assert( assert_correctness(), "inconsistent statistics" );
    
    return labelIter;
}


void
CTreeSplitEntropyStatistics::increase_counts( int32_t f_nodeIdx, int32_t f_labelIdx_i32, int32_t f_weight_i32 )
{
    base_t::increase_counts( f_nodeIdx, f_labelIdx_i32, f_weight_i32 );
    update_entropy_stats( f_nodeIdx, f_labelIdx_i32 );
}


void
CTreeSplitEntropyStatistics::decrease_counts( int32_t f_nodeIdx, int32_t f_labelIdx_i32, int32_t f_weight_i32 )
{
    base_t::decrease_counts( f_nodeIdx, f_labelIdx_i32, f_weight_i32 );
    update_entropy_stats( f_nodeIdx, f_labelIdx_i32 );
}



float32_t
CTreeSplitEntropyStatistics::total_rating() const
{
    return smd::sum( m_unormedEntropies_af32 );
}



bool CTreeSplitEntropyStatistics::assert_correctness() const
{
    bool check = base_t::assert_correctness();

    for ( int32_t leafIdx = 0; leafIdx < n_leafs(); ++leafIdx )
    {
        auto leafRow = m_XlogX_af32.row( leafIdx );
        for ( auto it = leafRow.elem().cbegin(); it != leafRow.elem().cend(); ++it )
        {
            tree_assert( std::isfinite(*it), "inconsistent xlogx" );
            check = check && std::isfinite(*it);
        }
    }

    return check;
}


//-------------------------------------------------------------------------------------------------------------



inline float32_t
CTreeSplitEntropyStatistics::calculate_unormed_node_entropy( int32_t f_nodeIdx_i32 ) const
{
    const float32_t l_maxEntropy_f32 = nuk::safe_XlogX( static_cast<float32_t>( m_Counts_ai32.elem( f_nodeIdx_i32 ) ) );
    const float32_t l_Entropy_f32    = smd::sum( m_XlogX_af32.row( f_nodeIdx_i32 ) );
    
    tree_assert( 0.f <= l_Entropy_f32, "Inconsistent entropy" );
    tree_assert( (l_Entropy_f32 - l_maxEntropy_f32) <= (1e-4f * std::abs(l_maxEntropy_f32)),   // Comparison with tolerance
                  "Inconsistent entropy" );

    return std::max( 0.f, l_maxEntropy_f32 - l_Entropy_f32 );
}



inline void
CTreeSplitEntropyStatistics::update_entropy_stats( int32_t f_nodeIdx, int32_t f_labelIdx_i32 )
{
    /// calculate left and right SumXLogX
    m_XlogX_af32.elem( f_nodeIdx, f_labelIdx_i32 ) = 
        nuk::safe_XlogX( static_cast<float32_t>( m_CorrelationCounts_ai32.elem( f_nodeIdx, f_labelIdx_i32 ) ) );
    
    /// calculate node specific entropy
    m_unormedEntropies_af32.elem( f_nodeIdx ) = calculate_unormed_node_entropy( f_nodeIdx );
}



void 
CTreeSplitEntropyStatistics::recalculate_entropy_stats()
{
    /// calculate XlogX statistics
    std::transform( m_CorrelationCounts_ai32.cbegin(), m_CorrelationCounts_ai32.cend(),
                    m_XlogX_af32.begin(),
                    nuk::safe_XlogX<smd::vf32_t> );
    
    /// calculate node wise entropies
    auto l_unormedEntropyIter = m_unormedEntropies_af32.elem().begin();
    for ( int32_t l_nodeIdx_i32 = 0; 
          l_nodeIdx_i32 < n_nodes(); 
          ++l_nodeIdx_i32, ++l_unormedEntropyIter )
    {
        tree_assert( l_unormedEntropyIter < m_unormedEntropies_af32.elem().end(), "Inconsistent array sizes" );

        *l_unormedEntropyIter = calculate_unormed_node_entropy( l_nodeIdx_i32 );
    }
}


//-------------------------------------------------------------------------------------------------------------

float32_t
CTreeSplitEntropyStatistics::calc_increase_rating( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 ) const
{
    return calc_update_rating( f_nodeIdx_i32, f_labelIdx_i32, f_weight_i32 );
}


float32_t
CTreeSplitEntropyStatistics::calc_decrease_rating( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 ) const
{
    return calc_update_rating( f_nodeIdx_i32, f_labelIdx_i32, -f_weight_i32 );
}


inline float32_t
CTreeSplitEntropyStatistics::calc_update_rating( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 ) const
{
    /// Calculate diff in Max-Entropy part
    const int32_t l_oldCounts_i32 = m_Counts_ai32.elem( f_nodeIdx_i32 );
    const int32_t l_newCounts_i32 = l_oldCounts_i32 + f_weight_i32;
    tree_assert( 0.f <= l_newCounts_i32, "Inconsistent entropy" );
    
    const float32_t l_diffMaxEntropy_f32 =
                        nuk::safe_XlogX( static_cast<float32_t>( l_newCounts_i32 ) ) -    //  newMaxEntropy -
                        nuk::safe_XlogX( static_cast<float32_t>( l_oldCounts_i32 ) );     //  oldMaxEntropy
    
    /// Calculate diff in Entropy part
    const int32_t l_oldCorrCount_i32 = m_CorrelationCounts_ai32.elem( f_nodeIdx_i32, f_labelIdx_i32 );
    const int32_t l_newCorrCount_i32 = l_oldCorrCount_i32 + f_weight_i32;
    tree_assert( 0.f <= l_newCorrCount_i32, "Inconsistent entropy" );

    const float32_t l_diffEntropy_f32 = 
                        nuk::safe_XlogX( static_cast<float32_t>( l_newCorrCount_i32 ) ) -  // newEntropy
                        nuk::safe_XlogX( static_cast<float32_t>( l_oldCorrCount_i32 ) );   // oldEntropy

    /// final difference in rating
    return (l_diffMaxEntropy_f32 - l_diffEntropy_f32);
}


//-------------------------------------------------------------------------------------------------------------



} // namespace tree
