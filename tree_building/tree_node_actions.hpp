/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "tree_base_types.hpp"
#include "simd.hpp"


namespace tree
{
namespace nodeact
{

//--------- NODE STATE INDICATOR ---------------------------------------------------

inline constexpr
bool
isFinalNode( int32_t f_nodeIdx_i32 ) { return (f_nodeIdx_i32 < 0); }

inline
smd::vb32_t
isFinalNode( const smd::vi32_t& f_nodeIdx_vi32 ) 
{ 
    return ( f_nodeIdx_vi32 < smd::vi32_t(0) );
}


inline
bool
is_bit_set( int32_t f_nodeIdx_i32, uint8_t l_bitIdx_u8 )
{
    return ( 0 != ( std::abs(f_nodeIdx_i32) & (int32_t(1) << int32_t(l_bitIdx_u8)) ) );
}

inline bool is_left( int32_t f_nodeIdx_i32, uint8_t l_bitIdx_u8 )
{
    return is_bit_set( f_nodeIdx_i32, l_bitIdx_u8 );
}



inline
smd::vb32_t
is_bit_set( const smd::vi32_t& f_nodeIdx_vi32, uint8_t l_bitIdx_u8 )
{
    return ( 0 != ( std::abs(f_nodeIdx_vi32) & (int32_t(1) << int32_t(l_bitIdx_u8)) ) );
}

inline smd::vb32_t is_left( const smd::vi32_t& f_nodeIdx_vi32, uint8_t l_bitIdx_u8 )
{
    return is_bit_set( f_nodeIdx_vi32, l_bitIdx_u8 );
}

//----------------------------------------------------------------------------------
//--------- TREE TRAVERSAL FUNCTIONS -----------------------------------------------
//----------------------------------------------------------------------------------

inline
int32_t
finalize_node( int32_t f_nodeIdx_i32 )
{
    return (-std::abs( f_nodeIdx_i32 ));
}

//----------------------------------------------------------------------------------

inline constexpr 
int32_t
move_downwards( int32_t f_nodeIdx_i32 ) { return (f_nodeIdx_i32 << 1); }

inline
smd::vi32_t
move_downwards( const smd::vi32_t& f_nodeIdx_vi32 )
{
    return ( f_nodeIdx_vi32 << smd::vi32_t(1) );
}

//----------------------------------------------------------------------------------

inline constexpr 
int32_t
move_upwards( int32_t f_nodeIdx_i32 ) { return (f_nodeIdx_i32 >> 1); }


inline
smd::vi32_t
move_upwards( const smd::vi32_t& f_nodeIdx_vi32 )
{
    return ( f_nodeIdx_vi32 >> smd::vi32_t(1) );
}


//----------------------------------------------------------------------------------

inline constexpr 
bool
feature_comparison( float32_t f_featureVal_f32, float32_t f_featureSplit_f32 )
{
    return ( f_featureVal_f32 < f_featureSplit_f32 );
}

inline
smd::vb32_t
feature_comparison( const smd::vf32_t& f_featureVal_vf32, const smd::vf32_t& f_featureSplit_vf32 )
{
    return ( f_featureVal_vf32 < f_featureSplit_vf32 );
}


//----------------------------------------------------------------------------------

inline constexpr 
int32_t
make_node_decision( bool f_comparisonRes_b, int32_t f_nodeIdx_i32 )
{
    //  static_cast<int32_t>( f_comparisonRes_b ) == 0: // stay right
    //  static_cast<int32_t>( f_comparisonRes_b ) == 1: // go left

    return ( f_nodeIdx_i32 + smd::copyminus( static_cast<int32_t>( f_comparisonRes_b ), f_nodeIdx_i32 ) );
}


inline 
smd::vi32_t
make_node_decision( const smd::vb32_t& f_comparisonRes_vb, const smd::vi32_t& f_nodeIdx_vi32 )
{
    return smd::blend( f_comparisonRes_vb, 
                       f_nodeIdx_vi32 + smd::copyminus( smd::vi32_t(1), f_nodeIdx_vi32 ),  // go left
                       f_nodeIdx_vi32 );                                                  // stay right
}

//----------------------------------------------------------------------------------

} // nodeact
} // namespace tree

