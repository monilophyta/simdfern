/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <iterator>
#include <algorithm>

#include <simd.hpp>
#include "tree_debug.hpp"
#include "tree_base_types.hpp"

#include "fern_sample_projection.hpp"
#include "tree_event_counting.inl"


namespace tree
{


//--------------------------------------------------------------------------------------------------------
// ------ CBaseSampleProjection Projection implementation ------------------------------------------------
//--------------------------------------------------------------------------------------------------------


template <typename Derived>
CBaseSampleProjection<Derived>::CBaseSampleProjection( float32_t* f_projIter_f32, float32_t* f_projIterEnd_f32 )
    : m_projIter_f32( f_projIter_f32 )
    , m_projIterEnd_f32( f_projIterEnd_f32 )
    , m_arrayAligner( f_projIter_f32, this->num_data() )
{}



template <typename Derived>
void CBaseSampleProjection<Derived>::map( const float32_t*        f_weightIter_pf32,
                                          const float32_t* const  f_weightIterEnd_pf32,
                                          const float32_t* const* f_featureRowIter_ppf32 )
{
    if ( false == this->has_aligned_memory() )
    {
        derived().map_unaligned( f_weightIter_pf32, f_weightIterEnd_pf32, f_featureRowIter_ppf32 );

        // update counter
        evtcnt().increment_sample_projection_num_unaligned( this->num_data() * 
                                                            std::distance( f_weightIter_pf32, f_weightIterEnd_pf32 ) );
    }
    else
    {
        derived().map_aligned( f_weightIter_pf32, f_weightIterEnd_pf32, f_featureRowIter_ppf32 );
    }
}


template <typename Derived>
inline
void CBaseSampleProjection<Derived>::map_aligned( const float32_t*        f_weightIter_pf32,
                                                  const float32_t* const  f_weightIterEnd_pf32,
                                                  const float32_t* const* f_featureRowIter_ppf32 )
{
    tree_assert( has_aligned_memory(), "invalid alignment" );
    tree_assert( 0 <= std::distance( f_weightIter_pf32, f_weightIterEnd_pf32 ), "Invalid iterators" );
    
    for (; f_weightIter_pf32 != f_weightIterEnd_pf32;
            ++f_weightIter_pf32, ++f_featureRowIter_ppf32 )
    {
        const float32_t  l_weight_f32      = *f_weightIter_pf32;
        const float32_t* l_featureRow_pf32 = *f_featureRowIter_ppf32;
        
        if ( derived().is_alignable_with( l_featureRow_pf32 ) )
        { // improved speed due to (partially) aligned memory

            // processing part before aligned memory
            if (m_arrayAligner.has_pre_memory())
            {
                const float32_t* pre_feature_end = derived().map_prealigned_features( l_featureRow_pf32, l_weight_f32 );
                tree_assert( pre_feature_end == m_arrayAligner.pre_end( l_featureRow_pf32 ), "Error in memory alignment" );
            }

            // processing part with aligned memory
            tree_assert( derived().has_aligned_memory(), "When we are here, this should always be true" );
            {
                const smd::vf32_t* aligned_feature_end = derived().map_aligned_features( l_featureRow_pf32, l_weight_f32 );
                tree_assert( aligned_feature_end == m_arrayAligner.aligned_end_cast( l_featureRow_pf32 ), "Error in memory alignment" );
            }

            // processing part after aligned memory
            if (m_arrayAligner.has_post_memory())
            {
                const float32_t* post_feature_end = derived().map_postaligned_feature( l_featureRow_pf32, l_weight_f32 );
                tree_assert( post_feature_end == m_arrayAligner.post_end( l_featureRow_pf32 ), "Error in memory alignment" );
            }

            // update counter
            evtcnt().increment_sample_projection_num_aligned( m_arrayAligner.num_aligned() );
            evtcnt().increment_sample_projection_num_unaligned( m_arrayAligner.num_unaligned() );
        }
        else
        { // procesing without aligned memory
            derived().map_unaligned_feature( l_featureRow_pf32, l_weight_f32 );

            // update counter
            evtcnt().increment_sample_projection_num_unaligned( this->num_data() );
        }
    }
}


template <typename Derived>
inline
void CBaseSampleProjection<Derived>::map_unaligned( const float32_t*        f_weightIter_pf32,
                                                    const float32_t* const  f_weightIterEnd_pf32,
                                                    const float32_t* const* f_featureRowIter_ppf32 )
{
    tree_assert( 0 <= std::distance( f_weightIter_pf32, f_weightIterEnd_pf32 ), "Invalid iterators" );
    
    for (; f_weightIter_pf32 != f_weightIterEnd_pf32;
           ++f_weightIter_pf32, ++f_featureRowIter_ppf32 )
    {
        derived().map_unaligned_feature( *f_featureRowIter_ppf32, *f_weightIter_pf32 );
    }
}


//--------------------------------------------------------------------------------------------------------
// ------ CSimpleProjection Projection implementation ----------------------------------------------------
//--------------------------------------------------------------------------------------------------------

/// Explicit Instantiation
template class CBaseSampleProjection<CSimpleProjection>;



template <typename FT>
inline const FT*
CSimpleProjection::map_feature( const FT*  f_sampleIter,
                                FT         f_weight,
                                FT*        f_projIter,
                                FT* const  f_projIterEnd )
{
    tree_assert( 0 <= std::distance( f_projIter, f_projIterEnd ), "Invalid iterators" );
    
    // loop over samples
    for (; f_projIter != f_projIterEnd;
           ++f_sampleIter, ++f_projIter )
    {
        *f_projIter += f_weight * (*f_sampleIter);
    }

    return f_sampleIter;
}


const float32_t*
CSimpleProjection::map_prealigned_features( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 )
{
    return derived().map_feature( m_arrayAligner.pre_begin( f_featureRow_pf32 ),
                                  f_weight_f32,
                                  m_arrayAligner.pre_begin( m_projIter_f32 ),
                                  m_arrayAligner.pre_end( m_projIter_f32 ) );
}


const smd::vf32_t*
CSimpleProjection::map_aligned_features( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 )
{
    return derived().map_feature( m_arrayAligner.aligned_begin_cast( f_featureRow_pf32 ),
                                  smd::vf32_t( f_weight_f32 ),
                                  m_arrayAligner.aligned_begin_cast( m_projIter_f32),
                                  m_arrayAligner.aligned_end_cast( m_projIter_f32 ) );
}


const float32_t*
CSimpleProjection::map_postaligned_feature( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 )
{
    return derived().map_feature( m_arrayAligner.post_begin( f_featureRow_pf32 ),
                                 f_weight_f32,
                                 m_arrayAligner.post_begin( m_projIter_f32 ),
                                 m_arrayAligner.post_end( m_projIter_f32 ) );
}


const float32_t* 
CSimpleProjection::map_unaligned_feature( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 )
{
    return derived().map_feature( f_featureRow_pf32, f_weight_f32, m_projIter_f32, m_projIterEnd_f32 );
}


//--------------------------------------------------------------------------------------------------------
// ------ CBaseCompensated Projection implementation -----------------------------------------------------
//--------------------------------------------------------------------------------------------------------


template <typename Derived>
CBaseCompensatedProjection<Derived>::CBaseCompensatedProjection( float32_t* f_projIter_f32, float32_t* f_projIterEnd_f32 )
    : base_t( f_projIter_f32, f_projIterEnd_f32 )
      /// array for error compensation + potential alignment overlap
    , m_compPtr_pf32( allocator_t().allocate( this->num_data() + smd::vf32_t::size() - 1u ) )
      /// align array of error compensators with output (f_projIter_f32)
    , m_comp_pf32( smd::equalize_alignemnt( this->m_projIter_f32, m_compPtr_pf32.get() ) )
{
    tree_assert( static_cast<size_t>(std::distance( m_compPtr_pf32.get(), m_comp_pf32 )) < smd::vf32_t::size(), "Error in memory alignment" );

    /// initialize compensator with zeros
    std::fill_n( m_comp_pf32, this->num_data(), 0.f );
}


template <typename Derived>
CBaseCompensatedProjection<Derived>::~CBaseCompensatedProjection()
{
    m_comp_pf32 = nullptr;
    /// smart pointer takes car of himself
}


//--------------------------------------------------------------------------------------------------------
// ------ CFirstOrderCompensatedProjection Projection implementation -------------------------------------
//--------------------------------------------------------------------------------------------------------


template <typename Derived>
const float32_t*
CFirstOrderCompensatedProjection<Derived>::map_prealigned_features( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 )
{
    return derived().map_feature( this->m_arrayAligner.pre_begin( f_featureRow_pf32 ),
                                  f_weight_f32,
                                  this->m_arrayAligner.pre_begin( this->m_projIter_f32 ),
                                  this->m_arrayAligner.pre_end( this->m_projIter_f32 ),
                                  this->m_arrayAligner.pre_begin( this->m_comp_pf32 ) );
}

template <typename Derived>
const smd::vf32_t*
CFirstOrderCompensatedProjection<Derived>::map_aligned_features( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 )
{
    return derived().map_feature( this->m_arrayAligner.aligned_begin_cast( f_featureRow_pf32 ),
                                  smd::vf32_t( f_weight_f32 ),
                                  this->m_arrayAligner.aligned_begin_cast( this->m_projIter_f32),
                                  this->m_arrayAligner.aligned_end_cast( this->m_projIter_f32 ),
                                  this->m_arrayAligner.aligned_begin_cast( this->m_comp_pf32 ) );
}

template <typename Derived>
const float32_t*
CFirstOrderCompensatedProjection<Derived>::map_postaligned_feature( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 )
{
    return derived().map_feature( this->m_arrayAligner.post_begin( f_featureRow_pf32 ),
                                 f_weight_f32,
                                 this->m_arrayAligner.post_begin( this->m_projIter_f32 ),
                                 this->m_arrayAligner.post_end( this->m_projIter_f32 ),
                                 this->m_arrayAligner.post_begin( this->m_comp_pf32 ) );
}

template <typename Derived>
const float32_t* 
CFirstOrderCompensatedProjection<Derived>::map_unaligned_feature( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 )
{
    return derived().map_feature( f_featureRow_pf32, f_weight_f32, this->m_projIter_f32, this->m_projIterEnd_f32, this->m_comp_pf32 );
}

//--------------------------------------------------------------------------------------------------------
// ------ Kahan implementation ---------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------

/// Explicit Instantiation
template class CBaseSampleProjection<CKahanSampleProjection>;
template class CBaseCompensatedProjection<CKahanSampleProjection>;


template <typename FT>
inline const FT*
CKahanSampleProjection::map_feature( const FT*  f_sampleIter,
                                     FT         f_weight,
                                     FT*        f_projIter,
                                     FT* const  f_projIterEnd,
                                     FT*        f_compIter )
{
    tree_assert( 0 <= std::distance( f_projIter, f_projIterEnd ), "Invalid iterators" );
    
    // loop over samples
    for (; f_projIter != f_projIterEnd;
           ++f_sampleIter, ++f_projIter, ++f_compIter )
    {
        smd::kahan_sum_step( *f_projIter, f_weight, *f_sampleIter, *f_compIter );
    }

    return f_sampleIter;
}

//--------------------------------------------------------------------------------------------------------
// ------ Neumaier implementation ------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------

/// Explicit Instantiation
template class CBaseSampleProjection<CNeumaierSampleProjection>;
template class CBaseCompensatedProjection<CNeumaierSampleProjection>;


void CNeumaierSampleProjection::map_aligned( const float32_t*        f_weightIter_pf32,
                                             const float32_t* const  f_weightIterEnd_pf32,
                                             const float32_t* const* f_featureRowIter_ppf32 )
{
    base_t::map_aligned( f_weightIter_pf32, f_weightIterEnd_pf32, f_featureRowIter_ppf32 );

    // processing part before aligned memory
    if (this->m_arrayAligner.has_pre_memory())
    {
        /// apply compensator
        const float32_t* pre_proj_end = 
            std::transform( this->m_arrayAligner.pre_begin( this->m_projIter_f32 ),
                                this->m_arrayAligner.pre_end( this->m_projIter_f32 ),
                            this->m_arrayAligner.pre_begin( this->m_comp_pf32 ),
                            this->m_arrayAligner.pre_begin( this->m_projIter_f32 ), 
                            std::plus<float32_t>() );
        
        tree_assert( pre_proj_end == this->m_arrayAligner.pre_end( this->m_projIter_f32 ), "Error in memory alignment" );
    }

    // processing part with aligned memory
    tree_assert(this->has_aligned_memory(), "When we are here, this should always be true" );
    {
        /// apply compensator
        const smd::vf32_t* align_proj_end = 
            std::transform( this->m_arrayAligner.aligned_begin_cast( this->m_projIter_f32 ),
                                this->m_arrayAligner.aligned_end_cast( this->m_projIter_f32 ),
                            this->m_arrayAligner.aligned_begin_cast( this->m_comp_pf32 ),
                            this->m_arrayAligner.aligned_begin_cast( this->m_projIter_f32 ), 
                            std::plus<smd::vf32_t>() );
        
        tree_assert( align_proj_end == this->m_arrayAligner.aligned_end_cast( this->m_projIter_f32 ), "Error in memory alignment" );
    }

    // processing part after aligned memory
    if (this->m_arrayAligner.has_post_memory())
    {
        /// apply compensator
        const float32_t* post_proj_end = 
            std::transform( this->m_arrayAligner.post_begin( this->m_projIter_f32 ),
                                this->m_arrayAligner.post_end( this->m_projIter_f32 ),
                            this->m_arrayAligner.post_begin( this->m_comp_pf32 ),
                            this->m_arrayAligner.post_begin( this->m_projIter_f32 ), 
                            std::plus<float32_t>() );
        
        tree_assert( post_proj_end == this->m_arrayAligner.post_end( this->m_projIter_f32 ), "Error in memory alignment" );
    }
    
    /// invalidate compensator
    std::fill_n( this->m_comp_pf32, this->num_data(), 0.f );
}


void CNeumaierSampleProjection::map_unaligned( const float32_t*        f_weightIter_pf32,
                                               const float32_t* const  f_weightIterEnd_pf32,
                                               const float32_t* const* f_featureRowIter_ppf32 )
{
    base_t::map_unaligned( f_weightIter_pf32, f_weightIterEnd_pf32, f_featureRowIter_ppf32 );
    
    /// apply compensator
    std::transform( this->m_projIter_f32, this->m_projIterEnd_f32,
                    this->m_comp_pf32,
                    this->m_projIter_f32, 
                    std::plus<float32_t>() );
    
    /// invalidate compensator
    std::fill_n( this->m_comp_pf32, this->num_data(), 0.f );
}



template <typename FT>
inline const FT*
CNeumaierSampleProjection::map_feature( const FT*  f_sampleIter,
                                        FT         f_weight,
                                        FT*        f_projIter,
                                        FT* const  f_projIterEnd,
                                        FT*        f_compIter )
{
    tree_assert( 0 <= std::distance( f_projIter, f_projIterEnd ), "Invalid iterators" );
    
    // loop over samples
    for (; f_projIter != f_projIterEnd;
           ++f_sampleIter, ++f_projIter, ++f_compIter )
    {
        smd::neumaier_sum_step( *f_projIter, f_weight, *f_sampleIter, *f_compIter );
    }

    return f_sampleIter;
}

//--------------------------------------------------------------------------------------------------------
// ------ Klein implementation ---------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------

namespace
{
template <class InputIterator1, class InputIterator2, class InputIterator3, class OutputIterator, class TernayOperator>
  OutputIterator transform3 ( InputIterator1 first1, InputIterator1 last1,
                              InputIterator2 first2,
                              InputIterator3 first3,
                              OutputIterator result, TernayOperator op)
{
    while (first1 != last1)
    {
        *result = op(*first1,*first2,*first3);
        ++result; ++first1; ++first2; ++first3;
    }
    return result;
}
template <class T>
struct plus3 {
    T operator() (const T& x, const T& y, const T& z) const {return (x+y+z);}
};
} // anonymouse namespace



/// Explicit Instantiation
template class CBaseSampleProjection<CKleinSampleProjection>;
template class CBaseCompensatedProjection<CKleinSampleProjection>;



CKleinSampleProjection::CKleinSampleProjection( float32_t* f_projIter_f32, float32_t* f_projIterEnd_f32 )
    : base_t( f_projIter_f32, f_projIterEnd_f32 )
      /// array for second order error compensation + potential alignment overlap
    , m_comp2Ptr_pf32( allocator_t().allocate( this->num_data() + smd::vf32_t::size() - 1u ) )
      /// align array of second order error compensators with output (f_projIter_f32)
    , m_comp2_pf32( smd::equalize_alignemnt( this->m_projIter_f32, m_comp2Ptr_pf32.get() ) )
{
    tree_assert( static_cast<size_t>(std::distance( m_comp2Ptr_pf32.get(), m_comp2_pf32 )) < smd::vf32_t::size(), "Error in memory alignment" );

    /// initialize compensator with zeros
    std::fill_n( m_comp2_pf32, this->num_data(), 0.f );
}


CKleinSampleProjection::~CKleinSampleProjection()
{
    m_comp2_pf32 = nullptr;
    /// smart pointer takes car of himself
}



void CKleinSampleProjection::map_aligned( const float32_t*        f_weightIter_pf32,
                                          const float32_t* const  f_weightIterEnd_pf32,
                                          const float32_t* const* f_featureRowIter_ppf32 )
{
    base_t::map_aligned( f_weightIter_pf32, f_weightIterEnd_pf32, f_featureRowIter_ppf32 );

    // processing part before aligned memory
    if (this->m_arrayAligner.has_pre_memory())
    {
        /// apply compensator
        const float32_t* pre_proj_end = 
            transform3( this->m_arrayAligner.pre_begin( this->m_projIter_f32 ),
                            this->m_arrayAligner.pre_end( this->m_projIter_f32 ),
                        this->m_arrayAligner.pre_begin( this->m_comp_pf32 ),
                        this->m_arrayAligner.pre_begin( this->m_comp2_pf32 ),
                        this->m_arrayAligner.pre_begin( this->m_projIter_f32 ), 
                        plus3<float32_t>() );
        
        tree_assert( pre_proj_end == this->m_arrayAligner.pre_end( this->m_projIter_f32 ), "Error in memory alignment" );
    }

    // processing part with aligned memory
    tree_assert(this->has_aligned_memory(), "When we are here, this should always be true" );
    {
        /// apply compensator
        const smd::vf32_t* align_proj_end = 
            transform3( this->m_arrayAligner.aligned_begin_cast( this->m_projIter_f32 ),
                            this->m_arrayAligner.aligned_end_cast( this->m_projIter_f32 ),
                        this->m_arrayAligner.aligned_begin_cast( this->m_comp_pf32 ),
                        this->m_arrayAligner.aligned_begin_cast( this->m_comp2_pf32 ),
                        this->m_arrayAligner.aligned_begin_cast( this->m_projIter_f32 ), 
                        plus3<smd::vf32_t>() );
        
        tree_assert( align_proj_end == this->m_arrayAligner.aligned_end_cast( this->m_projIter_f32 ), "Error in memory alignment" );
    }

    // processing part after aligned memory
    if (this->m_arrayAligner.has_post_memory())
    {
        /// apply compensator
        const float32_t* post_proj_end = 
            transform3( this->m_arrayAligner.post_begin( this->m_projIter_f32 ),
                            this->m_arrayAligner.post_end( this->m_projIter_f32 ),
                        this->m_arrayAligner.post_begin( this->m_comp_pf32 ),
                        this->m_arrayAligner.post_begin( this->m_comp2_pf32 ),
                        this->m_arrayAligner.post_begin( this->m_projIter_f32 ), 
                        plus3<float32_t>() );
        
        tree_assert( post_proj_end == this->m_arrayAligner.post_end( this->m_projIter_f32 ), "Error in memory alignment" );
    }
    
    /// invalidate compensators
    std::fill_n( this->m_comp_pf32, this->num_data(), 0.f );
    std::fill_n( this->m_comp2_pf32, this->num_data(), 0.f );
}



void CKleinSampleProjection::map_unaligned( const float32_t*        f_weightIter_pf32,
                                            const float32_t* const  f_weightIterEnd_pf32,
                                            const float32_t* const* f_featureRowIter_ppf32 )
{
    base_t::map_unaligned( f_weightIter_pf32, f_weightIterEnd_pf32, f_featureRowIter_ppf32 );
    
    /// apply compensator
    std::transform( this->m_projIter_f32, this->m_projIterEnd_f32,
                    this->m_comp_pf32,
                    this->m_projIter_f32, 
                    std::plus<float32_t>() );
    
    /// invalidate compensator
    std::fill_n( this->m_comp_pf32, this->num_data(), 0.f );
    std::fill_n( this->m_comp2_pf32, this->num_data(), 0.f );
}


const float32_t*
CKleinSampleProjection::map_prealigned_features( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 )
{
    return derived().map_feature( this->m_arrayAligner.pre_begin( f_featureRow_pf32 ),
                                  f_weight_f32,
                                  this->m_arrayAligner.pre_begin( this->m_projIter_f32 ),
                                  this->m_arrayAligner.pre_end( this->m_projIter_f32 ),
                                  this->m_arrayAligner.pre_begin( this->m_comp_pf32 ),
                                  this->m_arrayAligner.pre_begin( this->m_comp2_pf32 ) );
}

const smd::vf32_t*
CKleinSampleProjection::map_aligned_features( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 )
{
    return derived().map_feature( this->m_arrayAligner.aligned_begin_cast( f_featureRow_pf32 ),
                                  smd::vf32_t( f_weight_f32 ),
                                  this->m_arrayAligner.aligned_begin_cast( this->m_projIter_f32),
                                  this->m_arrayAligner.aligned_end_cast( this->m_projIter_f32 ),
                                  this->m_arrayAligner.aligned_begin_cast( this->m_comp_pf32 ),
                                  this->m_arrayAligner.aligned_begin_cast( this->m_comp2_pf32 ) );
}

const float32_t*
CKleinSampleProjection::map_postaligned_feature( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 )
{
    return derived().map_feature( this->m_arrayAligner.post_begin( f_featureRow_pf32 ),
                                 f_weight_f32,
                                 this->m_arrayAligner.post_begin( this->m_projIter_f32 ),
                                 this->m_arrayAligner.post_end( this->m_projIter_f32 ),
                                 this->m_arrayAligner.post_begin( this->m_comp_pf32 ),
                                 this->m_arrayAligner.post_begin( this->m_comp2_pf32 ) );
}

const float32_t* 
CKleinSampleProjection::map_unaligned_feature( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 )
{
    return derived().map_feature( f_featureRow_pf32,
                                  f_weight_f32,
                                  this->m_projIter_f32,
                                  this->m_projIterEnd_f32,
                                  this->m_comp_pf32,
                                  this->m_comp2_pf32 );
}


template <typename FT>
inline const FT*
CKleinSampleProjection::map_feature( const FT*  f_sampleIter,
                                     FT         f_weight,
                                     FT*        f_projIter,
                                     FT* const  f_projIterEnd,
                                     FT*        f_comp1Iter,
                                     FT*        f_comp2Iter )
{
    tree_assert( 0 <= std::distance( f_projIter, f_projIterEnd ), "Invalid iterators" );
    
    // loop over samples
    for (; f_projIter != f_projIterEnd;
           ++f_sampleIter, ++f_projIter, ++f_comp1Iter, ++f_comp2Iter )
    {
        smd::klein_sum_step( *f_projIter, f_weight, *f_sampleIter, *f_comp1Iter, *f_comp2Iter );
    }

    return f_sampleIter;
}


} // namespace tree
