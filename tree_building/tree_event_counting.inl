/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "tree_event_counting.hpp"


namespace tree
{

// ------------------------------------------------------------------------------------------------------
// tree classification counters

inline auto
EventCounter::increment_tree_classification_num_aligned( counter_t l_cnt_u ) -> counter_t
{
    return m_treeClassificationNumAligned_u.fetch_add( l_cnt_u, std::memory_order_relaxed );
}


inline auto 
EventCounter::tree_classification_num_aligned() const -> counter_t
{
    return m_treeClassificationNumAligned_u.load(std::memory_order_relaxed);
}


inline auto
EventCounter::increment_tree_classification_num_unaligned( counter_t l_cnt_u ) -> counter_t
{
    return m_treeClassificationNumUnaligned_u.fetch_add( l_cnt_u, std::memory_order_relaxed );
}


inline auto 
EventCounter::tree_classification_num_unaligned() const -> counter_t
{
    return m_treeClassificationNumUnaligned_u.load(std::memory_order_relaxed);
}


// ------------------------------------------------------------------------------------------------------
// fern_sample_projection counters

inline auto
EventCounter::increment_sample_projection_num_aligned( counter_t l_cnt_u ) -> counter_t
{
    return m_sampleProjectionNumAligned_u.fetch_add( l_cnt_u, std::memory_order_relaxed );
}


inline auto 
EventCounter::sample_projection_num_aligned() const -> counter_t
{
    return m_sampleProjectionNumAligned_u.load(std::memory_order_relaxed);
}


inline auto
EventCounter::increment_sample_projection_num_unaligned( counter_t l_cnt_u ) -> counter_t
{
    return m_sampleProjectionNumUnaligned_u.fetch_add( l_cnt_u, std::memory_order_relaxed );
}


inline auto 
EventCounter::sample_projection_num_unaligned() const -> counter_t
{
    return m_sampleProjectionNumUnaligned_u.load(std::memory_order_relaxed);
}

// ------------------------------------------------------------------------------------------------------
// aligned vs non-aligned line search evaluation counters


inline auto 
EventCounter::increment_line_search_eval_num_aligned( counter_t l_cnt_u ) -> counter_t
{
    return m_lineSearchEvalNumAligned_u.fetch_add( l_cnt_u, std::memory_order_relaxed );
}

inline auto 
EventCounter::line_search_eval_num_aligned() const -> counter_t
{
    return m_lineSearchEvalNumAligned_u.load( std::memory_order_relaxed );
}

inline auto 
EventCounter::increment_line_search_eval_num_unaligned( counter_t l_cnt_u ) -> counter_t
{
    return m_lineSearchEvalNumUnaligned_u.fetch_add( l_cnt_u, std::memory_order_relaxed );
}

inline auto 
EventCounter::line_search_eval_num_unaligned() const -> counter_t
{
    return m_lineSearchEvalNumUnaligned_u.load( std::memory_order_relaxed );
}


} // namespace tree

