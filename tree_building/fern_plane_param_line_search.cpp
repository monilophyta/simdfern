/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "fern_plane_param_line_search.hpp"

#include "tree_plane_param_optimization_base.hpp"
#include "tree_plane_param_optimization_base.inl"

#include "tree_split_statistics_entropy.hpp"
#include "tree_split_statistics_gini.hpp"

#include "fern_weight_regularisation.hpp"
#include "fern_weight_regularisation.inl"

#include "tree_enums.hpp"

#include "tree_node_index.hpp"
#include "tree_node_index.inl"

#include "tree_debug.hpp"



namespace tree 
{

// Explicit instantiation
// template struct CBaseVectPenalizer<float32_t>;
// template struct CBaseVectPenalizer<smd::vf32_t>;
// template struct CVectPenalizer<float32_t>;
// template struct CVectPenalizer<smd::vf32_t>;
// template struct CNormalizedL1VectPenalizer<float32_t>;
// template struct CNormalizedL1VectPenalizer<smd::vf32_t>;


static inline constexpr float32_t inf_val() { return std::numeric_limits<float32_t>::infinity(); }

//---------------------------------------------------------------------------------------------

template <typename penalizer_T, typename split_stat_T>
void
CPlaneParamLineSearch<penalizer_T,split_stat_T>::optimal_t::initialize()
{
    m_prmVal_f32        = -inf_val();
    m_splitRating       = std::numeric_limits<rating_t>::max();
    m_prmValPenalty_f32 = std::numeric_limits<float32_t>::max();
}


//---------------------------------------------------------------------------------------------


template <typename penalizer_T, typename split_stat_T>
CPlaneParamLineSearch<penalizer_T,split_stat_T>::CPlaneParamLineSearch ( 
                       int32_t f_numLabels_i32, 
                       int32_t f_numNodes_i32,
            const penalizer_T& f_prmValPenalizer )

    : base_t( f_numLabels_i32, f_numNodes_i32 )
    , m_optimal()
    , m_limboSplit()
    , m_prmValPenalizer( f_prmValPenalizer )
{
    // nothing to do
}


template <typename penalizer_T, typename split_stat_T>
const int32_t*
CPlaneParamLineSearch<penalizer_T,split_stat_T>::initialize_from( 
            const nodeIdx_t* nodeIter, const nodeIdx_t* const nodeIterEnd,
            const int32_t* labelIter, 
            const int32_t* weightIter )
{
    {
        // cast left and right statistics to base types => allow counting upwards without expensive statistic updates
        CTreeSplitStatisticsBase& l_leftStat  = static_cast<CTreeSplitStatisticsBase&>( leftStat() );
        CTreeSplitStatisticsBase& l_rightStat = static_cast<CTreeSplitStatisticsBase&>( rightStat() );

        // m_leftStat and m_rightStat might become invalid here
        l_leftStat.initialize_zero();
        l_rightStat.initialize_zero();


        for ( ; nodeIter != nodeIterEnd;
                ++nodeIter, ++labelIter, ++weightIter )
        {
            tree_input_check( 0 <= *weightIter );

            nodeIdx_t l_nodeIdx = *nodeIter;
            
            if ( false == l_nodeIdx.isFinal() )   // finalized nodes are ignored
            {
                const bool l_isleft_b = l_nodeIdx.pop_decision();

                if (l_isleft_b)
                {
                    l_leftStat.increase_counts( l_nodeIdx.idx(), *labelIter, *weightIter );
                }
                else
                {
                    l_rightStat.increase_counts( l_nodeIdx.idx(), *labelIter, *weightIter );
                }
            }
        }
    }

    /// repair left and right node statistics
    leftStat().repair_statistics();
    rightStat().repair_statistics();

    tree_assert( leftStat().assert_correctness(), "Inconsistent left statistic" );
    tree_assert( rightStat().assert_correctness(), "Inconsistent right statistic" );

    /// initialize state variables
    m_optimal.initialize();
    m_limboSplit.initialize();

    return labelIter;
}



//----------------------------------------------------------------------------------------------


template <typename penalizer_T, typename split_stat_T>
void
CPlaneParamLineSearch<penalizer_T,split_stat_T>::process_invalid_feature_sample(
            const int32_t, nodeIdx_t, const int32_t )
{
    /// do not do anything with marginal samples
}


template <typename penalizer_T, typename split_stat_T>
EFeatureContrib
CPlaneParamLineSearch<penalizer_T,split_stat_T>::start_feature_processing( 
            const float32_t f_prmVal_f32,
            const int32_t   f_labelIdx_i32,
            const nodeIdx_t f_nodeIdx,
            const int32_t   f_sampleWeight_i32 )
{
    tree_assert( true  == is_feature_valid( f_prmVal_f32 ), "this feature should be valid" );
    tree_assert( (false == is_feature_valid( m_limboSplit.get() )) && 
                 ( 0 > m_limboSplit.get() ), "limbo should be invalid (negative infinity)" );
    tree_assert( false == f_nodeIdx.isFinal(), "node is supposed to be active" );

    float32_t l_centralVal_f32 = 0.f;

    if (f_prmVal_f32 <= 0)
    {
        // left boundary
        l_centralVal_f32 = f_prmVal_f32 - std::max( std::abs(0.1f * f_prmVal_f32), 
                                                          std::numeric_limits<float32_t>::epsilon() );
    }
    // else  // set split to zero since everything else is right of zero
    
    /// evaluate valuecandicate
    const EFeatureContrib l_improvement = eval_param_value_candidate( l_centralVal_f32 );
    
    /// flip tree side of node
    flip_tree_sides( f_labelIdx_i32, f_nodeIdx, f_sampleWeight_i32 );

    /// set new limbo split
    m_limboSplit.set( f_prmVal_f32 );

    return l_improvement;
}


template <typename penalizer_T, typename split_stat_T>
EFeatureContrib
CPlaneParamLineSearch<penalizer_T,split_stat_T>::process_next_feature_sample( 
            const float32_t f_prmVal_f32,
            const int32_t   f_labelIdx_i32,
            const nodeIdx_t f_nodeIdx,
            const int32_t   f_sampleWeight_i32 )
{
    tree_assert( true == is_feature_valid( f_prmVal_f32 ), "this feature should be valid" );
    tree_assert( true == is_feature_valid( m_limboSplit.get() ), "limbo should be valid" );
    tree_assert( m_limboSplit.get() <= f_prmVal_f32, "inconsistent feature order" );
    tree_assert( false == f_nodeIdx.isFinal(), "node is supposed to be active" );

    EFeatureContrib l_improvement = EFeatureContrib::NO_IMPROVEMENT;
    
    /// verify if split between limbo feature and current feature is possible
    const bool l_splitPossible_b = 
        nodeact::feature_comparison( nextafterf( m_limboSplit.get(), MAX_FLOAT32 ), f_prmVal_f32 );
    
    if (l_splitPossible_b)
    {   
        // somewhere in the middle
        const float32_t l_centralVal_f32 = (0.5f * f_prmVal_f32) + (0.5f * m_limboSplit.get());

        l_improvement = eval_param_value_candidate( l_centralVal_f32 );
    }
    
    /// flip tree side of node
    flip_tree_sides( f_labelIdx_i32, f_nodeIdx, f_sampleWeight_i32 );
    
    /// set new limbo split
    m_limboSplit.set( f_prmVal_f32 );

    return l_improvement;
}


template <typename penalizer_T, typename split_stat_T>
EFeatureContrib
CPlaneParamLineSearch<penalizer_T,split_stat_T>::finish_feature_processing()
{
    EFeatureContrib l_improvement = EFeatureContrib::NO_IMPROVEMENT;
    
    if (is_feature_valid( m_limboSplit.get()))
    {
        float32_t l_centralVal_f32 = 0.f;

        if (m_limboSplit.get() >= 0)
        {
            // right boundary
            l_centralVal_f32 = m_limboSplit.get() + std::max( std::abs(0.1f * m_limboSplit.get()), 
                                                                std::numeric_limits<float32_t>::epsilon() );
        }
        // else  // set split to zero since everything else is left of zero

        l_improvement = eval_param_value_candidate( l_centralVal_f32 );
    }
    
    return l_improvement;
}


//----------------------------------------------------------------------------------------------


template <typename penalizer_T, typename split_stat_T>
inline EFeatureContrib
CPlaneParamLineSearch<penalizer_T,split_stat_T>::eval_param_value_candidate( float32_t f_centralVal_f32 )
{
    tree_assert( is_feature_valid( f_centralVal_f32 ), "Inconsistent split change" );

    /// split rating
    const rating_t l_splitRating = leftStat().total_rating() + rightStat().total_rating();

    if (l_splitRating >= m_optimal.optimRating())
        return EFeatureContrib::NO_IMPROVEMENT;

    /// regularisation of feature weight
    const float32_t l_centralVal_f32 = m_prmValPenalizer.calc_penalty( f_centralVal_f32 );
    
    /// optimization criterion
    const float32_t l_optimRating_f32 = static_cast<float32_t>(l_splitRating) + l_centralVal_f32;

    // compare optimization criterions
    if ( l_optimRating_f32 < m_optimal.optimRating() )
    {
        m_optimal.m_splitRating = l_splitRating;
        m_optimal.m_prmVal_f32 = f_centralVal_f32;
        m_optimal.m_prmValPenalty_f32 = l_centralVal_f32;

        return EFeatureContrib::IMPROVEMENT;
    }

    return EFeatureContrib::NO_IMPROVEMENT;
}



template <typename penalizer_T, typename split_stat_T>
inline void
CPlaneParamLineSearch<penalizer_T,split_stat_T>::flip_tree_sides( const int32_t   f_labelIdx_i32,
                                                                        nodeIdx_t f_nodeIdx, 
                                                                  const int32_t   f_sampleWeight_i32 )
{
    const bool l_isLeft_b = f_nodeIdx.pop_decision();
    if (true == l_isLeft_b)
    {
        /// move data sample from left to right side - update left and right statistics
        leftStat().decrease_counts(  f_nodeIdx.idx(), f_labelIdx_i32, f_sampleWeight_i32 );
        rightStat().increase_counts( f_nodeIdx.idx(), f_labelIdx_i32, f_sampleWeight_i32 );
    }
    else
    {
        /// move data sample from right to left side - update left and right statistics
        leftStat().increase_counts(  f_nodeIdx.idx(), f_labelIdx_i32, f_sampleWeight_i32 );
        rightStat().decrease_counts( f_nodeIdx.idx(), f_labelIdx_i32, f_sampleWeight_i32 );
    }
}

//-------------------------------------------------------------------------------------------


void 
limboSplit_t::initialize()
{
    m_prmVal_f32 = -inf_val();
}

void
limboSplit_t::set( float32_t f_prmVal_f32 )
{
    m_prmVal_f32 = f_prmVal_f32;
}

//-------------------------------------------------------------------------------------------

#define INSTANTIATE_WITH_PENALIZER( PENAL_TYPE ) \
    template struct CBasePlaneParamOptimizer< CTreeSplitEntropyStatistics, CPlaneParamLineSearch< PENAL_TYPE, CTreeSplitEntropyStatistics > >; \
    template struct CBasePlaneParamOptimizer< CTreeSplitGiniStatistics,    CPlaneParamLineSearch< PENAL_TYPE, CTreeSplitGiniStatistics > >; \
    template struct CPlaneParamLineSearch< PENAL_TYPE, CTreeSplitEntropyStatistics >; \
    template struct CPlaneParamLineSearch< PENAL_TYPE, CTreeSplitGiniStatistics >;


INSTANTIATE_WITH_PENALIZER( CNoPenalizer );
INSTANTIATE_WITH_PENALIZER( CPenalizer );
INSTANTIATE_WITH_PENALIZER( CNormalizedL1Penalizer );
//INSTANTIATE_WITH_PENALIZER( CBaseVectPenalizer<float32_t> );
//INSTANTIATE_WITH_PENALIZER( CBaseVectPenalizer<smd::vf32_t> );
INSTANTIATE_WITH_PENALIZER( CVectPenalizer<float32_t> );
INSTANTIATE_WITH_PENALIZER( CVectPenalizer<smd::vf32_t> );
INSTANTIATE_WITH_PENALIZER( CNormalizedL1VectPenalizer<float32_t> );
INSTANTIATE_WITH_PENALIZER( CNormalizedL1VectPenalizer<smd::vf32_t> );


#undef INSTANTIATE_WITH_PENALIZER

} // namespace tree
