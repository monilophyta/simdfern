/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include <limits>
#include <utility>
#include <new>
#include <memory>
#include <numeric>
#include "tree_split_optimization.hpp"
#include "tree_box_feature.hpp"
#include "tree_enums.hpp"
#include "tree_debug.hpp"
//#include "tree.hpp"

#include "tree_plane_param_optimization_base.hpp"
#include "tree_plane_param_optimization_base.inl"
#include "tree_split_optimization_base.hpp"
#include "tree_split_optimization_base.inl"


namespace tree 
{


//---------------------------------------------------------------------------------------------

template <typename rating_T>
CNodeState<rating_T>::optimal_t::optimal_t()
    : m_nodeRating(  std::numeric_limits<rating_t>::max() )
    , m_featureIdx_i32(   evalue(ELeafCode::UNDEFINED_SPLIT_INDEX) )
    , m_featureSplit_f32( std::numeric_limits<float32_t>::signaling_NaN() )
    , m_nextSplitImprovement_e( EFeatureContrib::NO_IMPROVEMENT )
{}


template <typename rating_T>
CNodeState<rating_T>::limboSplit_t::limboSplit_t()
    : m_featureVal_f32( std::numeric_limits<float32_t>::signaling_NaN() )
{}



template <typename rating_T>
CNodeState<rating_T>::CNodeState()
    : m_optimal()
    , m_limboSplit()
{}


template <typename rating_T>
void CNodeState<rating_T>::deactivate()
{
    m_optimal.m_nodeRating  = -std::numeric_limits<rating_T>::max();
    m_optimal.m_featureIdx_i32   = evalue( ELeafCode::NODE_UNUSED );
    m_optimal.m_featureSplit_f32 = std::numeric_limits<float32_t>::signaling_NaN();
    
    m_limboSplit = limboSplit_t();
}


template <typename rating_T>
bool CNodeState<rating_T>::is_active() const
{
    return ( evalue( ELeafCode::NODE_UNUSED ) != m_optimal.m_featureIdx_i32 );
}

//---------------------------------------------------------------------------------------------



template <typename rating_T>
void
CNodeState<rating_T>::set_rating( rating_t f_nodeRating )
{
    if ( f_nodeRating < m_optimal.m_nodeRating ) // the smaller the better
    {
        m_optimal = optimal_t();
        m_optimal.m_nodeRating = f_nodeRating;
    }
}


template <typename rating_T>
EFeatureContrib 
CNodeState<rating_T>::update_optimal_split( int32_t f_featureIdx_i32, rating_t f_nodeRating, float32_t f_featureVal_f32 )
{
    tree_assert( is_active(), "updating split on inactive node" );
    tree_assert( f_nodeRating < m_optimal.m_nodeRating, "Nonoptimal update" );
    
    float32_t l_new_featureSplit_f32 =   (0.5f * f_featureVal_f32) 
                                       + (0.5f * m_limboSplit.m_featureVal_f32);
    
    tree_assert(     (m_optimal.m_featureIdx_i32 != f_featureIdx_i32) 
                  || (m_optimal.m_featureSplit_f32 < l_new_featureSplit_f32),
                 "Inconsistent split change" )

    m_optimal.m_featureSplit_f32 = l_new_featureSplit_f32;
    m_optimal.m_nodeRating = f_nodeRating;
    m_optimal.m_featureIdx_i32 = f_featureIdx_i32;

    // ensuring that first returned improvement is from initialization
    const EFeatureContrib l_improvement = m_optimal.m_nextSplitImprovement_e;
    m_optimal.m_nextSplitImprovement_e = EFeatureContrib::IMPROVEMENT;

    return l_improvement;
}



template <typename rating_T>
void CNodeState<rating_T>::initialize_limbo_split()
{
    m_limboSplit.m_featureVal_f32 = -std::numeric_limits<float32_t>::max();
}


template <typename rating_T>
void CNodeState<rating_T>::set_limbo_split( float32_t f_featureVal_f32 )
{
    tree_assert( is_active(), "updating split on inactive node" );
    m_limboSplit.m_featureVal_f32 = f_featureVal_f32;
}


template <typename rating_T>
float32_t CNodeState<rating_T>::get_limbo_split() const
{
    tree_assert( is_active(), "requesting limbo split on inactive node" );
    return m_limboSplit.m_featureVal_f32;
}

//----------------------------------------------------------------------------------------------

template <typename split_stat_T>
CFeatureSplitOptimizer<split_stat_T>::CFeatureSplitOptimizer ( 
        int32_t f_numLabels_i32, 
        int32_t f_numNodes_i32 )

    : base_t( f_numLabels_i32, f_numNodes_i32 )
    , m_nodeStates( f_numNodes_i32 )   // [ #Leaf ]
{
    // nothing to do
}

template <typename split_stat_T>
void 
CFeatureSplitOptimizer<split_stat_T>::initialize_node_ratings(
        const split_stat_t& l_rightStat )
{
    tree_assert( static_cast<size_t>(l_rightStat.n_nodes()) == m_nodeStates.size(), 
                 "Inconsistent array lengths" );
    
    int32_t l_nodeIdx = 0;
    
    for ( node_state_t& l_NState : m_nodeStates )
    {
        l_NState.set_rating( l_rightStat.node_rating( l_nodeIdx ) );
        ++l_nodeIdx;
    }
}

//----------------------------------------------------------------------------------------------

template <typename split_stat_T>
void CFeatureSplitOptimizer<split_stat_T>::initialize_feature_run(            int32_t  f_nextFeatureIdx_i32,
                                                                   const split_stat_t& f_previousLayerStat )
{
    base_t::initialize_feature_run( f_nextFeatureIdx_i32, f_previousLayerStat );

    // Initialize limbo states
    for ( node_state_t& l_NState : m_nodeStates )
    {
        l_NState.initialize_limbo_split();
    }
}



template <typename split_stat_T>
void
CFeatureSplitOptimizer<split_stat_T>::process_invalid_feature_sample(
            const int32_t   f_labelIdx_i32,
            const nodeIdx_t f_nodeIdx,
            const int32_t   f_weight_i32 )
{
    /// Get current state of toched node
    node_state_t& l_NState = m_nodeStates[ f_nodeIdx.idx() ];
    
    /// nothing to be done for inactive nodes
    if (true == l_NState.is_active())
    {
        /// check if node has already been finished
        if ( false == f_nodeIdx.isFinal() )
        {
            /// update left and right statistics
            int32_t l_halfWeight_i32 = f_weight_i32 / 2;
            
            leftStat().increase_counts(  f_nodeIdx.idx(), f_labelIdx_i32, l_halfWeight_i32 );
            rightStat().decrease_counts( f_nodeIdx.idx(), f_labelIdx_i32, l_halfWeight_i32 );
        }
    }
}


template <typename split_stat_T>
EFeatureContrib
CFeatureSplitOptimizer<split_stat_T>::process_next_feature_sample( 
            const float32_t f_feature_f32,
            const int32_t   f_labelIdx_i32,
            const nodeIdx_t f_nodeIdx,
            const int32_t   f_weight_i32 )
{
    /// Get current state of toched node
    node_state_t& l_NState = m_nodeStates[ f_nodeIdx.idx() ];
    
    // check if node has already been finished
    if ( false == l_NState.is_active() )
    {
        return EFeatureContrib::NO_IMPROVEMENT;
    }

    EFeatureContrib l_improvement = EFeatureContrib::NO_IMPROVEMENT;

    /// verify if split between limbo feature and current feature is possible
    const bool l_splitPossible_b = 
        nodeact::feature_comparison( nextafterf( l_NState.get_limbo_split(), MAX_FLOAT32 ), f_feature_f32 );
    
    if ( true == l_splitPossible_b )
    {
        const rating_t l_nodeRating = 
            leftStat().node_rating( f_nodeIdx.idx() ) + rightStat().node_rating( f_nodeIdx.idx() );
        
        /// compare entropies
        if ( l_nodeRating < l_NState.optimal().m_nodeRating )
        {
            // there is an improvement
            l_improvement = l_NState.update_optimal_split( featureIdx(), l_nodeRating, f_feature_f32 );
        }
    }

    /// move data sample from right to left side - update left and right statistics
    leftStat().increase_counts(  f_nodeIdx.idx(), f_labelIdx_i32, f_weight_i32 );
    rightStat().decrease_counts( f_nodeIdx.idx(), f_labelIdx_i32, f_weight_i32 );

    /// set new limbo split
    l_NState.set_limbo_split( f_feature_f32 );

    return l_improvement;
}


//----------------------------------------------------------------------------------------------


template <typename split_stat_T>
void 
CFeatureSplitOptimizer<split_stat_T>::getOptimalFeatureSplits( int32_t*&    featureIdxIter,
                                                               float32_t*&  splitValsIter,
                                                               rating_t*&   featureRatingIter ) const
{
    for ( auto nstateIter = m_nodeStates.cbegin();
               nstateIter != m_nodeStates.cend();
          ++nstateIter, 
          ++featureIdxIter, ++splitValsIter, ++featureRatingIter )
    {
        *featureIdxIter     = nstateIter->optimal().m_featureIdx_i32;
        *splitValsIter      = nstateIter->optimal().m_featureSplit_f32;
        *featureRatingIter  = nstateIter->optimal().m_nodeRating;
    }
}


//----------------------------------------------------------------------------------------------

// instantiation in cpp

template struct CBasePlaneParamOptimizer< CTreeSplitEntropyStatistics, split_entropy_optimizer_t >;
template struct CBasePlaneParamOptimizer< CTreeSplitGiniStatistics, split_gini_optimizer_t >;

template struct CBaseSplitOptimizer< CTreeSplitEntropyStatistics, split_entropy_optimizer_t >;
template struct CBaseSplitOptimizer< CTreeSplitGiniStatistics, split_gini_optimizer_t >;

template struct CFeatureSplitOptimizer< CTreeSplitEntropyStatistics >;
template struct CFeatureSplitOptimizer< CTreeSplitGiniStatistics >;

//-------------------------------------------------------------------------------------------


} // namespace tree

