/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <limits>
#include <iterator>
#include "tree_plane_param_optimization_base.hpp"
#include "tree_enums.hpp"
#include "tree_box_feature.hpp"
#include "tree_debug.hpp"
//#include "tree.hpp"



namespace tree 
{
//-------------------------------------------------------------------------------------------

namespace
{

constexpr float32_t MIN_FLOAT32 = std::numeric_limits<float32_t>::min();
constexpr float32_t MAX_FLOAT32 = std::numeric_limits<float32_t>::max();

} // anonymous namespace

//----------------------------------------------------------------------------------------------

template <typename split_stat_T, typename derived_T>
CBasePlaneParamOptimizer<split_stat_T,derived_T>::CBasePlaneParamOptimizer ( 
        int32_t f_numLabels_i32, 
        int32_t f_numNodes_i32 )

    : m_leftStat( f_numLabels_i32, f_numNodes_i32 )
    , m_rightStat( f_numLabels_i32, f_numNodes_i32 )
{
    // nothing to do
}


//----------------------------------------------------------------------------------------------

template <typename split_stat_T, typename derived_T>
int32_t
CBasePlaneParamOptimizer<split_stat_T,derived_T>::process_sorted_feature(
            const float32_t*  featureIter,
            const float32_t*  featureIterEnd,
            const int32_t*    labelIdxIter,
            const nodeIdx_t*  nodeIdxIter,
            const int32_t*    weightIter )
{
    {
        const float32_t* featureIterStore = featureIter;
        /// Process all invalid feature at the beginning of the feature list
        featureIter = process_invalid_feature_range( featureIter, featureIterEnd,
                                                     labelIdxIter,
                                                     nodeIdxIter,
                                                     weightIter );

        tree_assert( (featureIterEnd == featureIter) || is_feature_valid( *featureIter ), 
                     "this feature should be valid" );

        /// progress label and index iterator
        const std::ptrdiff_t l_progressStep = std::distance( featureIterStore, featureIter );
        
        std::advance( labelIdxIter, l_progressStep );
        std::advance(  nodeIdxIter, l_progressStep );
        std::advance(   weightIter, l_progressStep );
    }

    /// Process all invalid feature at the end of the feature list
    {
        const std::ptrdiff_t l_endStep = std::distance( featureIter, featureIterEnd );
        
        auto featureIterEndR = process_invalid_feature_range( 
                                    std::make_reverse_iterator( featureIterEnd ),
                                    std::make_reverse_iterator( featureIter ),
                                    std::make_reverse_iterator( std::next( labelIdxIter, l_endStep ) ),
                                    std::make_reverse_iterator( std::next(  nodeIdxIter, l_endStep ) ),
                                    std::make_reverse_iterator( std::next(   weightIter, l_endStep ) ) );
        
        tree_assert( (featureIterEndR.base() == featureIterEnd) || is_feature_invalid( *(featureIterEndR.base()) ),
                     "Wrong understanding of reverse iterator" );
        
        featureIterEnd = featureIterEndR.base();
    }
    
    /// Process all valid features
    tree_expensive_assert( m_leftStat.assert_correctness(), "Inconsistent left statistik" );
    tree_expensive_assert( m_rightStat.assert_correctness(), "Inconsistent right statistik" );

    int32_t l_numImprovements_i32 = 0;

    /// process first valid feature sample
    while ( (featureIterEnd != featureIter) && (true == nodeIdxIter->isFinal()) )
    {
        ++featureIter;
        ++labelIdxIter;
        ++nodeIdxIter; 
        ++weightIter;
    }

    if (featureIterEnd != featureIter)
    {
        tree_assert( is_feature_valid( *featureIter ), "this feature should be valid" );
        tree_assert( false == nodeIdxIter->isFinal(), "this sample should not have been closed" );

        l_numImprovements_i32 += 
                    static_cast<int32_t>( derived().start_feature_processing( 
                                    *featureIter,
                                    *labelIdxIter,
                                    *nodeIdxIter,
                                    *weightIter ) );

        tree_expensive_assert( m_leftStat.assert_correctness(), "Inconsistent left statistik" );
        tree_expensive_assert( m_rightStat.assert_correctness(), "Inconsistent right statistik" );

        // progress index
        ++featureIter;
        ++labelIdxIter;
        ++nodeIdxIter; 
        ++weightIter;
    }

    /// process next feature samples
    for (; featureIterEnd != featureIter; 
          ++featureIter,
          ++labelIdxIter, ++nodeIdxIter, ++weightIter )
    {
        tree_assert( is_feature_valid( *featureIter ), "this feature should be valid" );
        
        nodeIdx_t l_nodeIdx = *nodeIdxIter;

        if (false == l_nodeIdx.isFinal())
        {
            l_numImprovements_i32 +=
                    static_cast<int32_t>( derived().process_next_feature_sample( 
                                    *featureIter,
                                    *labelIdxIter,
                                    l_nodeIdx,
                                    *weightIter ) );

            tree_expensive_assert( m_leftStat.assert_correctness(), "Inconsistent left statistik" );
            tree_expensive_assert( m_rightStat.assert_correctness(), "Inconsistent right statistik" );
        }
    }

    // finish feature prossing
    l_numImprovements_i32 += static_cast<int32_t>( derived().finish_feature_processing() );

    return l_numImprovements_i32;
}



template <typename split_stat_T, typename derived_T>
int32_t
CBasePlaneParamOptimizer<split_stat_T,derived_T>::process_unsorted_feature(
                const float32_t* const f_featureArray,
                const float32_t* const f_featureEnd,
                const int32_t*   const f_labelIdxArray,
                const nodeIdx_t* const f_nodeIdxArray,
                const int32_t*   const f_weightArray )
{
    std::unique_ptr<int32_t[]> l_srtIdx_ptr;
    int32_t* l_srtIdxEnd = order_unsorted_features( l_srtIdx_ptr, f_featureArray, f_featureEnd );

    if (nullptr == l_srtIdxEnd) 
        return evalue( EECode::FAILED_MEMORY_ALLOCATION );

    /// Process all invalid feature at the beginning of the feature list
    int32_t* idxIter = process_invalid_feature_range( l_srtIdx_ptr.get(), l_srtIdxEnd, 
                                                      f_featureArray,
                                                      f_labelIdxArray,
                                                      f_nodeIdxArray,
                                                      f_weightArray );

    /// Process all invalid feature at the end of the feature list
    {
        auto l_srtIdxEndR = process_invalid_feature_range( 
                                        std::make_reverse_iterator( l_srtIdxEnd ),
                                        std::make_reverse_iterator( idxIter ),
                                        f_featureArray,
                                        f_labelIdxArray,
                                        f_nodeIdxArray,
                                        f_weightArray );

        tree_assert( (l_srtIdxEndR.base() == l_srtIdxEnd) || 
                     is_feature_invalid( f_featureArray[ *(l_srtIdxEndR.base()) ] ),
                     "Wrong understanding of reverse iterator" );
        
        l_srtIdxEnd = l_srtIdxEndR.base();
    }
    
    /// Process all valid features
    tree_expensive_assert( m_leftStat.assert_correctness(), "Inconsistent left statistik" );
    tree_expensive_assert( m_rightStat.assert_correctness(), "Inconsistent right statistik" );

    int32_t l_numImprovements_i32 = 0;

    /// process first valid feature sample
    while ( (l_srtIdxEnd != idxIter) && (true == f_nodeIdxArray[ *idxIter ].isFinal()) ) ++idxIter;

    if (l_srtIdxEnd != idxIter)
    {
        const int32_t idx = *idxIter;
        
        tree_assert( is_feature_valid( f_featureArray[ idx ] ), "this feature should be valid" );
        tree_assert( false == f_nodeIdxArray[ idx ].isFinal(), "this sample should not have been closed" );

        l_numImprovements_i32 += 
                    static_cast<int32_t>( derived().start_feature_processing( 
                                    f_featureArray[ idx ],
                                    f_labelIdxArray[ idx ],
                                    f_nodeIdxArray[ idx ],
                                    f_weightArray[ idx ] ) );

        tree_expensive_assert( m_leftStat.assert_correctness(), "Inconsistent left statistik" );
        tree_expensive_assert( m_rightStat.assert_correctness(), "Inconsistent right statistik" );

        // progress index
        ++idxIter;
    }
    
    /// process next feature samples
    for (; l_srtIdxEnd != idxIter; ++idxIter )
    {
        const int32_t idx = *idxIter;

        tree_assert( is_feature_valid( f_featureArray[ idx ] ), "this feature should be valid" );

        nodeIdx_t l_nodeIdx = f_nodeIdxArray[ idx ];

        if (false == l_nodeIdx.isFinal())
        {
            l_numImprovements_i32 += 
                    static_cast<int32_t>( derived().process_next_feature_sample( 
                                    f_featureArray[ idx ],
                                    f_labelIdxArray[ idx ],
                                    l_nodeIdx,
                                    f_weightArray[ idx ] ) );

            tree_expensive_assert( m_leftStat.assert_correctness(), "Inconsistent left statistik" );
            tree_expensive_assert( m_rightStat.assert_correctness(), "Inconsistent right statistik" );
        }
    }

    // finish feature prossing
    l_numImprovements_i32 += static_cast<int32_t>( derived().finish_feature_processing() );

    return l_numImprovements_i32;
}


//----------------------------------------------------------------------------------------------


template <typename split_stat_T, typename derived_T>
template <typename FITER, typename LITER, typename NITER, typename WITER>
inline FITER 
CBasePlaneParamOptimizer<split_stat_T,derived_T>::process_invalid_feature_range(
                    FITER  featureIter,
              const FITER  featureIterEnd,
                    LITER  labelIdxIter,
                    NITER  nodeIdxIter,
                    WITER  weightIter )
{
    for (; (featureIterEnd != featureIter) && ( is_feature_invalid( *featureIter ) );
           ++featureIter, ++labelIdxIter, ++nodeIdxIter, ++weightIter )
    {
        derived().process_invalid_feature_sample( *labelIdxIter, *nodeIdxIter, *weightIter );
    }
    return featureIter;
}


template <typename split_stat_T, typename derived_T>
template <typename SITER>
inline SITER 
CBasePlaneParamOptimizer<split_stat_T,derived_T>::process_invalid_feature_range(
                    SITER   idxIter,
              const SITER   idxIterEnd,
     const float32_t* const featureArray,
     const int32_t*   const labelIdxArray,
     const nodeIdx_t* const nodeIdxArray,
     const int32_t*   const weightArray )
{
    for(; (idxIter != idxIterEnd) && ( is_feature_invalid( featureArray[*idxIter] ) );
           ++idxIter )
    {
        derived().process_invalid_feature_sample( labelIdxArray[ *idxIter ],
                                                  nodeIdxArray[ *idxIter ],
                                                  weightArray[ *idxIter ] );
    }
    return idxIter;
}



} // namespace tree

