/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "tree_golden_section_search.hpp"
#include <tuple>
#include <utility>
#include <algorithm>
#include <cassert>

#include "tree_base_types.hpp"


namespace tree
{

template <typename T=float32_t>
static inline constexpr T invphi() { 
    return (T(0.5) * (std::sqrt(T(5)) - T(1)));
}

template <typename T=float32_t>
static inline constexpr T invphi2() { 
    return (T(0.5) * (T(3) - std::sqrt(T(5))));
}


template <typename eval_T, typename T>
inline constexpr std::pair<T,T>
golden_section_search( eval_T func, T f_left, T f_right, T f_tol )
{
    /// bring left and right in correct order
    std::tie(f_left, f_right) = std::minmax(f_left, f_right);
    
    T l_range = f_right - f_left;

    /// check if target is already reached
    if (l_range <= f_tol)
        return std::make_pair(f_left,f_right);

    T l_XmiddleLeft  = f_left + invphi2<T>() * l_range;
    T l_XmiddleRight = f_left + invphi<T>() * l_range;

    T l_YmiddleLeft  = func(l_XmiddleLeft);
    T l_YmiddleRight = func(l_XmiddleRight);
    
    do
    {
        if (l_YmiddleLeft < l_YmiddleRight)
        {
            f_right        = l_XmiddleRight;

            l_XmiddleRight = l_XmiddleLeft;
            l_YmiddleRight = l_YmiddleLeft;
            
            l_range        = f_right - f_left;

            l_XmiddleLeft  = f_left + invphi2<T>() * l_range;
            l_YmiddleLeft  = func(l_XmiddleLeft);
        }
        else
        {
            f_left         = l_XmiddleLeft;

            l_XmiddleLeft  = l_XmiddleRight;
            l_YmiddleLeft  = l_YmiddleRight;

            l_range        = f_right - f_left;

            l_XmiddleRight = f_left + invphi<T>() * l_range;
            l_YmiddleRight = func(l_XmiddleRight);
        }
    } while ((l_range > f_tol) && (l_XmiddleLeft < l_XmiddleRight));

    if (l_YmiddleLeft < l_YmiddleRight)
        return std::make_pair(f_left,l_XmiddleRight);
    else
        return std::make_pair(l_XmiddleLeft,f_right);
}


} // namespace tree
