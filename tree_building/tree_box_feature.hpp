/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <simd.hpp>
#include <limits>
#include "tree_base_types.hpp"


namespace tree
{


template < typename T >
struct CBoxIntRatio
{
    typedef T                                                     param_t;
    typedef typename std::remove_const<
                typename std::remove_reference<T>::type>::type    ret_t;


    template <typename DUMMY_T>
    inline 
    ret_t operator()( param_t x, param_t y, DUMMY_T ) const
    {
        ret_t l_val = x / std::max( y, std::numeric_limits<ret_t>::epsilon() );
        return l_val;
    }
};


//--------------------------------------------------------------------------------------------------------------


template < typename T >
struct CBoxIntDifference
{
    typedef T                                                     param_t;
    typedef typename std::remove_const<
                typename std::remove_reference<T>::type>::type    ret_t;

    inline 
    ret_t operator()( param_t x, param_t y, param_t lambda ) const
    {
        return ( x - (lambda * y) );
    }
};


//--------------------------------------------------------------------------------------------------------------

template < typename feature_T >
struct feature_value
{
    static_assert(  smd::is_simd_vf32<feature_T>::value ||
                   (std::is_floating_point<feature_T>::value && std::is_same<feature_T,float32_t>::value) );
    
    static inline constexpr 
    feature_T invalid()
    {
        return std::numeric_limits<feature_T>::signaling_NaN();
    }

    static inline constexpr
    typename smd::select<feature_T, bool>::type
    is_invalid( const feature_T& f_val ) { return ( !std::isfinite( f_val ) ); }

    static inline constexpr
    typename smd::select<feature_T, bool>::type
    is_valid( const feature_T& f_val ) { return std::isfinite( f_val ); }
};


template <typename T>
constexpr inline decltype( feature_value<T>::is_invalid( T() ) )
is_feature_invalid( const T& f_val  ) { return feature_value<T>::is_invalid( f_val ); }


template <typename T>
constexpr inline decltype( feature_value<T>::is_valid( T() ) )
is_feature_valid( const T& f_val  ) { return feature_value<T>::is_valid( f_val ); }


//--------------------------------------------------------------------------------------------------------------


} // namespace tree

