/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "tree_split_statistics_base.hpp"


namespace tree
{


struct CTreeSplitGiniStatistics : public CTreeSplitStatisticsBase
{

private:

    typedef CTreeSplitStatisticsBase      base_t;

    smd::array_vi32_t                     m_sumAbsDiff_ai32;    // [ #Leaf ]
    int32_t                               m_totalUnormedGini_i32;

public:

    CTreeSplitGiniStatistics( int32_t f_numLabels_i32, int32_t f_numNodes_i32 );

    virtual void initialize_zero() override;  // will be called from base class
    void initialize_from( const CTreeSplitGiniStatistics& other );
    void initialize_from( const CTreeSplitStatisticsBase& other );

    const int32_t*
    initialize_from( const nodeIdx_t* nodeIter, const nodeIdx_t* const nodeIterEnd,
                     const int32_t* labelIter, 
                     const int32_t* weightIter );


    void increase_counts( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 );
    void decrease_counts( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 );


    int32_t calc_increase_rating( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 ) const;
    int32_t calc_decrease_rating( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 ) const;

    int32_t unormed_gini_coefficient( int32_t f_nodeIdx_i32 ) const;
    

    /*!
      \brief TODO

      \param f_nodeIdx_i32 TODO

      \return node_rating <= 0, smaller values are better
    */
    inline int32_t node_rating( int32_t f_nodeIdx_i32 ) const { return -unormed_gini_coefficient( f_nodeIdx_i32 ); }

    inline int32_t total_rating() const { return -m_totalUnormedGini_i32; }

    inline void repair_statistics() { recalculate_ginis(); }

    bool assert_correctness() const override;

private:

    inline void update_gini_stats( const int32_t f_nodeIdx_i32, 
                                   const int32_t f_labelIdx_i32,
                                   const int32_t f_oldCorrCount_i32 );
    
    inline int32_t recalculate_sumAbsDiff( int32_t f_leafIdx_i32 ) const;
    void recalculate_ginis();

        
    inline int32_t calc_update_rating( const int32_t f_nodeIdx_i32, 
                                       const int32_t f_labelIdx_i32,
                                       const int32_t f_weight_i32 ) const;

};



} // namespace tree

