/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <type_traits>
#include "simd.hpp"


namespace tree
{



#ifdef ENABLE_UNUSED

/**
 * Calculates unormalized but weighted entropy:
 * \f{equation}{
 *  - \sum_{l=1}^L N^{\text{left}}_l \log_2 N^{\text{left}}_l - \sum_{l=1}^L N^{\text{right}}_l \log_2  N^{\text{right}}_l 
 *  - {N^{\text{left}}} \log_2 N^{\text{left}} - N^{\text{right}} \log_2 N^{\text{right}}  
 * \f}
 * 
 * @param begin and end of iterators
 * @return entropy
 */
template <typename ITER_T,
          typename ENTROPY_T = std::conditional< 
                                   std::is_integral< std::iterator_traits<ITER_T>::value_type >::value,
                                   float32_t,
                                   smd::vf32_t>::type>
inline float32_t
unormed_weighted_entropy( const ITER_T& begin, const ITER_T& end );

#endif // ENABLE_UNUSED

} // namespace tree


#include "tree_math.inl"

