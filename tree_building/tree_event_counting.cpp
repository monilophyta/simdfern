/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include "tree_event_counting.hpp"

namespace tree
{


EventCounter::EventCounter()
    : m_treeClassificationNumAligned_u(0)
    , m_treeClassificationNumUnaligned_u(0)

    , m_sampleProjectionNumAligned_u(0)
    , m_sampleProjectionNumUnaligned_u(0)

    , m_lineSearchEvalNumAligned_u(0)
    , m_lineSearchEvalNumUnaligned_u(0)
{}


EventCounter::operator EventStat() const
{
    EventStat ret = { m_treeClassificationNumAligned_u.load(std::memory_order_relaxed),
                      m_treeClassificationNumUnaligned_u.load(std::memory_order_relaxed),
                      
                      m_sampleProjectionNumAligned_u.load(std::memory_order_relaxed),
                      m_sampleProjectionNumUnaligned_u.load(std::memory_order_relaxed),
                      
                      m_lineSearchEvalNumAligned_u.load(std::memory_order_relaxed),
                      m_lineSearchEvalNumUnaligned_u.load(std::memory_order_relaxed) };
    return ret;
}



// singleton getter
EventCounter& evtcnt()
{
    static EventCounter l_event_counter;
    return l_event_counter;
}

} // namespace tree

