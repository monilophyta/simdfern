/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "tree_split_statistics_gini.hpp"
#include "tree_debug.hpp"
#include <algorithm>


namespace tree
{

//--------------------------------------------------------------------------------------------------------------


CTreeSplitGiniStatistics::CTreeSplitGiniStatistics( int32_t f_numLabels_i32, int32_t f_numNodes_i32 )
    : base_t( f_numLabels_i32, f_numNodes_i32 )
    , m_sumAbsDiff_ai32( f_numNodes_i32 )
    , m_totalUnormedGini_i32( -std::numeric_limits<int32_t>::max() )  // fill with invalid value
{}


void CTreeSplitGiniStatistics::initialize_zero()
{
    base_t::initialize_zero();
    m_sumAbsDiff_ai32.fill( smd::vi32_t( float32_t(0) ) );

    // initialize with equal distribution (worst possible value)
    m_totalUnormedGini_i32 = 0;

    tree_assert( assert_correctness(), "inconsistent statistics" );
}


void CTreeSplitGiniStatistics::initialize_from( const CTreeSplitGiniStatistics& other )
{
    base_t::initialize_from( other );
    
    tree_dims_check( m_sumAbsDiff_ai32.elem().size() == other.m_sumAbsDiff_ai32.elem().size() );

    m_sumAbsDiff_ai32.deep_copy_from( other.m_sumAbsDiff_ai32 );
    m_totalUnormedGini_i32 = other.m_totalUnormedGini_i32;

    tree_assert( assert_correctness(), "inconsistent statistics" );
}


void CTreeSplitGiniStatistics::initialize_from( const CTreeSplitStatisticsBase& other )
{
    base_t::initialize_from( other );

    // calculate sumAbsDiff and total Gini statistic from the scratch 
    recalculate_ginis();

    tree_assert( assert_correctness(), "inconsistent statistics" );
}



const int32_t* CTreeSplitGiniStatistics::initialize_from(
            const nodeIdx_t* nodeIter,
            const nodeIdx_t* const nodeIterEnd,
            const   int32_t* labelIter,
            const   int32_t* weightIter )
{
    labelIter = base_t::initialize_from( nodeIter, nodeIterEnd, labelIter, weightIter );
    
    // calculate sumAbsDiff and total Gini statistic from the scratch 
    recalculate_ginis();

    tree_assert( assert_correctness(), "inconsistent statistics" );

    return labelIter;
}


void
CTreeSplitGiniStatistics::increase_counts( int32_t f_nodeIdx, int32_t f_labelIdx_i32, int32_t f_weight_i32 )
{
    int32_t l_oldCorrCount_i32 = base_t::increase_counts( f_nodeIdx, f_labelIdx_i32, f_weight_i32 );

    update_gini_stats( f_nodeIdx, f_labelIdx_i32, l_oldCorrCount_i32 );
}


void
CTreeSplitGiniStatistics::decrease_counts( int32_t f_nodeIdx, int32_t f_labelIdx_i32, int32_t f_weight_i32 )
{
    int32_t l_oldCorrCount_i32 = base_t::decrease_counts( f_nodeIdx, f_labelIdx_i32, f_weight_i32 );
    
    update_gini_stats( f_nodeIdx, f_labelIdx_i32, l_oldCorrCount_i32 );
}



//--------------------------------------------------------------------------------------------------------------

int32_t CTreeSplitGiniStatistics::unormed_gini_coefficient( int32_t f_nodeIdx_i32 ) const
{
    const int32_t l_unormed_gini_coefficient = m_sumAbsDiff_ai32.elem( f_nodeIdx_i32 );
    
    tree_assert( 0 <= l_unormed_gini_coefficient, "Inconsistent sumAbsDiff count" );
    
    tree_assert( (n_labels() * m_Counts_ai32.elem( f_nodeIdx_i32 )) >= l_unormed_gini_coefficient, 
                  "Inconsistens sumAbsDiff count" );

    return l_unormed_gini_coefficient;
}


//--------------------------------------------------------------------------------------------------------------


bool CTreeSplitGiniStatistics::assert_correctness() const
{
    bool check = base_t::assert_correctness();

    for ( int32_t leafIdx = 0; check && (leafIdx < n_leafs()); ++leafIdx )
    {
        const int32_t l_sumAbsDiff = recalculate_sumAbsDiff( leafIdx );

        /// check with recalculation
        tree_assert( l_sumAbsDiff == m_sumAbsDiff_ai32.elem( leafIdx ), "inconsistent gini statistic" );
        check = check && ( l_sumAbsDiff == m_sumAbsDiff_ai32.elem( leafIdx ) );

        /// check for valid gini index
        const int32_t l_unormedGiniMax = m_Counts_ai32.elem( leafIdx ) * (n_labels() - 1); // Why -1 ?? Gini index on wikipedia shows without
        
        tree_assert( l_unormedGiniMax >= m_sumAbsDiff_ai32.elem( leafIdx ), "inconsistent gini statistic" );
        check = check && ( l_unormedGiniMax >= m_sumAbsDiff_ai32.elem( leafIdx ) );
    }

    /// check total unormed gini statistic correctnes
    tree_assert( m_totalUnormedGini_i32 == smd::sum( m_sumAbsDiff_ai32 ), "inconsistent total gini statistic" );
    check = check && ( m_totalUnormedGini_i32 == smd::sum( m_sumAbsDiff_ai32 ) );

    /// check for valid total gini index
    const int32_t l_totalUnormedGiniMax = smd::sum( m_Counts_ai32 ) * (n_labels() - 1); // Why -1 ?? Gini index on wikipedia shows without

    tree_assert( l_totalUnormedGiniMax >= m_totalUnormedGini_i32, "inconsistent total gini statistic" );
    check = check && ( l_totalUnormedGiniMax >= m_totalUnormedGini_i32 );

    return check;
}

//--------------------------------------------------------------------------------------------------------------




inline void
CTreeSplitGiniStatistics::update_gini_stats( const int32_t f_nodeIdx_i32, 
                                             const int32_t f_labelIdx_i32,
                                             const int32_t f_oldCorrCount_i32 )
{
    const smd::array_view_i32_t l_countsView_ai32  = m_CorrelationCounts_ai32.elem().row( f_nodeIdx_i32 );

    /// Reference to unormed gini (sum abs diff) statistic for modification
    int32_t& l_sumAbsDiff = m_sumAbsDiff_ai32.elem( f_nodeIdx_i32 );

    /// Store old unormed gini (sum abs diff) statistic
    const int32_t l_oldSumAbsDiff = l_sumAbsDiff;

    /// new correlation count
    const int32_t l_newCorrCount_i32 = l_countsView_ai32[ f_labelIdx_i32 ];

    /// update all abs-diffs containing statistic of m_CorrelationCounts_ai32.elem().row( f_nodeIdx_i32, f_labelIdx_i32 )
    {
        for (int32_t idx = 0; idx < f_labelIdx_i32; ++idx )
        {
            l_sumAbsDiff +=  std::abs( l_countsView_ai32[ idx ] - l_newCorrCount_i32 )   // add new abs diff
                           - std::abs( l_countsView_ai32[ idx ] - f_oldCorrCount_i32 );  // remove old abs diff
            tree_assert( 0 <= l_sumAbsDiff, "statistic shall allays be =>0" );
        }

        for (int32_t idx = (f_labelIdx_i32 + 1); idx < n_labels(); ++idx )
        {
            l_sumAbsDiff +=  std::abs( l_countsView_ai32[ idx ] - l_newCorrCount_i32 )   // add new abs diff
                           - std::abs( l_countsView_ai32[ idx ] - f_oldCorrCount_i32 );  // remove old abs diff
            tree_assert( 0 <= l_sumAbsDiff, "statistic shall allays be =>0" );
        }
    }

    tree_assert( l_sumAbsDiff ==  m_sumAbsDiff_ai32.elem( f_nodeIdx_i32 ), "Incorrect reference" );

    // update total unormed gini index
    m_totalUnormedGini_i32 += l_sumAbsDiff - l_oldSumAbsDiff;
}

//-------------------------------------------------------------------------------------------------------------


int32_t
CTreeSplitGiniStatistics::calc_increase_rating( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 ) const
{
    return calc_update_rating( f_nodeIdx_i32, f_labelIdx_i32, f_weight_i32 );
}


int32_t
CTreeSplitGiniStatistics::calc_decrease_rating( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 ) const
{
    return calc_update_rating( f_nodeIdx_i32, f_labelIdx_i32, -f_weight_i32 );
}


//-------------------------------------------------------------------------------------------------------------


inline int32_t
CTreeSplitGiniStatistics::calc_update_rating( const int32_t f_nodeIdx_i32, 
                                              const int32_t f_labelIdx_i32,
                                              const int32_t f_weight_i32 ) const
{
    const smd::array_view_ci32_t l_countsView_ai32 = m_CorrelationCounts_ai32.elem().row( f_nodeIdx_i32 );
    
    /// correlation count before update
    const int32_t l_oldCorrCount_i32 = l_countsView_ai32[ f_labelIdx_i32 ];
    /// correlation count after update - negative weight for decrease, positive weight for increase
    const int32_t l_newCorrCount_i32 = l_oldCorrCount_i32 + f_weight_i32;

    /// Unormed note rating (sum of absolute differences) before update
    const int32_t l_oldSumAbsDiff = m_sumAbsDiff_ai32.elem( f_nodeIdx_i32 );
    /// Unormed note rating (sum of absolute differences) for updating
    int32_t       l_newSumAbsDiff = l_oldSumAbsDiff;
    
    // TODO: Combine this with update_gini_stats

    /// calcuate update rating
    {
        for (int32_t idx = 0; idx < f_labelIdx_i32; ++idx )
        {
            l_newSumAbsDiff +=  std::abs( l_countsView_ai32[ idx ] - l_newCorrCount_i32 )   // add new abs diff
                              - std::abs( l_countsView_ai32[ idx ] - l_oldCorrCount_i32 );  // remove old abs diff
            tree_assert( 0 <= l_newSumAbsDiff, "statistic shall allays be =>0" );
        }

        for (int32_t idx = (f_labelIdx_i32 + 1); idx < n_labels(); ++idx )
        {
            l_newSumAbsDiff +=  std::abs( l_countsView_ai32[ idx ] - l_newCorrCount_i32 )   // add new abs diff
                              - std::abs( l_countsView_ai32[ idx ] - l_oldCorrCount_i32 );  // remove old abs diff
            tree_assert( 0 <= l_newSumAbsDiff, "statistic shall allays be =>0" );
        }
    }

    // final unormalized rating update (reversed diff due to minus one multiplication)
    const int32_t l_rating_update_i32 = l_oldSumAbsDiff - l_newSumAbsDiff;
    return l_rating_update_i32;
}



//-------------------------------------------------------------------------------------------------------------


int32_t 
CTreeSplitGiniStatistics::recalculate_sumAbsDiff( int32_t f_leafIdx_i32 ) const
{
    const smd::array_view_ci32_t l_countsView_ai32  = m_CorrelationCounts_ai32.elem().row( f_leafIdx_i32 );
    
    int32_t l_sumAbsDiff = 0;

    for ( int32_t j = 1; j < n_labels(); ++j )
    {
        for ( int32_t k = 0; k < j; ++k )
        {
            l_sumAbsDiff += std::abs( l_countsView_ai32[j] - l_countsView_ai32[k] );
        }
    }

    return l_sumAbsDiff;
}


void CTreeSplitGiniStatistics::recalculate_ginis()
{
    /// initialize total gini stat
    int32_t l_totalUnormedGini_i32 = 0;
    

    auto sbdIter = m_sumAbsDiff_ai32.elem().begin();

    for (int32_t leafIdx = 0;
                 sbdIter != m_sumAbsDiff_ai32.elem().end();
                 ++leafIdx, ++sbdIter )
    {
        /// recalculate unormed gini index of node leafIdx
        int32_t l_nodeGini_i32 = recalculate_sumAbsDiff( leafIdx );
        
        /// store node specific unormed gini index
        *sbdIter = l_nodeGini_i32;

        /// add for total unormed gini
        l_totalUnormedGini_i32 += l_nodeGini_i32;
    }

    /// store total unormed gini statistic
    m_totalUnormedGini_i32 = l_totalUnormedGini_i32;
}

//------------------------------------------------------------------------------------------------------------



} // namespace tree