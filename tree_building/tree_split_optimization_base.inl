/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "tree_split_optimization_base.hpp"
#include "tree_enums.hpp"
//#include "tree_debug.hpp"
//#include "tree.hpp"



namespace tree 
{


//----------------------------------------------------------------------------------------------

template <typename split_stat_T, typename derived_T>
CBaseSplitOptimizer<split_stat_T,derived_T>::CBaseSplitOptimizer ( 
        int32_t f_numLabels_i32, 
        int32_t f_numNodes_i32 )

    : base_t( f_numLabels_i32, f_numNodes_i32 )
    , m_featureIdx_i32( evalue( ELeafCode::UNDEFINED_FEATURE ) )
{
    // nothing to do
}



template <typename split_stat_T, typename derived_T>
void
CBaseSplitOptimizer<split_stat_T,derived_T>::initialize_feature_run( 
                   int32_t  f_nextFeatureIdx_i32,
        const split_stat_t& f_previousLayerStat )
{
    this->leftStat().initialize_zero();

    /// right size is initialized like previous layer
    this->rightStat().initialize_from( f_previousLayerStat );

    /// index of processed feature
    m_featureIdx_i32 = f_nextFeatureIdx_i32;
}


} // namespace tree

