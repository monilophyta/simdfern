/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <simd.hpp>
#include "tree_node_index.hpp"


namespace tree
{


struct CIndexedTreeLayer
{
    smd::array_view_ci32_t m_featureIdx_ai32;   // [ K ]
    smd::array_view_cf32_t m_splitVals_af32;    // [ K ]


    CIndexedTreeLayer();


    inline CIndexedTreeLayer( const smd::array_view_ci32_t& f_featureIdx_ai32,
                              const smd::array_view_cf32_t& f_splitVals_af32 )
        : m_featureIdx_ai32( f_featureIdx_ai32 )
        , m_splitVals_af32( f_splitVals_af32 )
    {}

    
    nodeIdx_t classify_single_data( 
        nodeIdx_t f_nodeIdx,
        const smd::array_view_cf32_t& f_featureVals_af32  ) const;  // [ F ]


    nodeIdx_t classify_single_data( 
        nodeIdx_t f_nodeIdx,
        const smd::column_iterator_cf32_t& f_featureVals_af32  ) const;  // [ F ]

    
    nodeIdx_vt classify_multiple_data(
        const nodeIdx_vt&  f_nodeIdx_vi32,                              // [ n ]
        const smd::column_iterator_cvf32_t& f_featureVals_avf32 ) const; // [ F x n(=4/8/16) ]


    nodeIdx_vt classify_multiple_data(
        const nodeIdx_vt&  f_nodeIdx_vi32,                               // [ n ]
        const smd::bundle_iterator_cf32_t& f_featureVals_af32 ) const; // [ F x n+... ]


    nodeIdx_vt classify_multiple_data(
        const smd::vi32_t& f_dataIdx_vi32,               // [ n ]
        const nodeIdx_vt&  f_nodeIdx_vi32,                // [ n ]
        const smd::matrix_view_cf32_t& f_featureMatrix_pmf32 ) const; // [ N x F ]

    // --------------------------------------------------------------------------------------------------------

    void classify_data_sequence( 
        const nodeIdx_t*              f_srcNodeIdx_p,
        const nodeIdx_t*              f_srcNodeIdxEnd_p,
        smd::bundle_iterator_cf32_t   f_dataColIter,
        nodeIdx_t*                    f_trgNodeIdx_p ) const;


    // -----------------------------------------------------------------------------


private:

    /*!
      \brief

      \param f_nodeIdx
      \param f_featureVals_af32

      \return Propagated (according to feature comparison ) node index. 
              For either "f_nodeIdx < 0" or "featureIdx[nodeidx] < 0"  "-(abs(f_nodeIdx) << 1)" is returned
    */
    template <typename RANDOM_ACCESS_T>
    inline nodeIdx_t
    classify_single_random_access( nodeIdx_t f_nodeIdx, 
                                   const RANDOM_ACCESS_T& f_featureVals_af32  ) const;  // [ F ]
    
    /*!
      \brief

      \param f_nodeIdx_vi32
      \param f_af32_featureAccessFunc 
        functor f_af32_featureAccessFunc( vi32_idx ) returns features for corresponding indices

      \return Propagated (according to feature comparison ) node indices. 
              For either "f_nodeIdx_vi32 < 0" or "featureIdx[nodeidx] < 0"  "-(abs(f_nodeIdx_vi32) << 1)" is returned
    */
    template <typename FEATURE_ACCESS_FUNC_T>
    inline nodeIdx_vt
    classify_multiple_random_access( const nodeIdx_vt&           f_nodeIdx_vi32,                     // [ n ]
                                           FEATURE_ACCESS_FUNC_T f_af32_featureAccessFunc  ) const;  // [ F x n(=4/8/16)... ]


    /*!
      \brief function to process a sequene of nodes in unaligned memory
    */
    inline nodeIdx_t*
    generic_classify_data_sequence( const nodeIdx_t*             f_srcNodeIter,
                                    const nodeIdx_t*             f_srcNodeIterEnd,
                                    smd::bundle_iterator_cf32_t  f_dataColIter,
                                    nodeIdx_t*                   f_trgNodeIter ) const;

    /*!
      \brief function to process a sequene of nodes in aligned memory
    */
    inline nodeIdx_vt*
    generic_classify_data_sequence( const nodeIdx_vt*             f_srcNodeIter,
                                    const nodeIdx_vt*             f_srcNodeIterEnd,
                                    smd::bundle_iterator_cf32_t   f_dataColIter,
                                    nodeIdx_vt*                   f_trgNodeIter ) const;
};



} // namespace tree
