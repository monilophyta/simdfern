/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "fern_lda_statistic.hpp"
#include "tree_debug.hpp"

#include <algorithm>
#include <functional>



namespace tree
{

// ---------------------------------------------------------------------------------------------------

CMultiNodeFirstPassStatistics::CMultiNodeFirstPassStatistics( 
            int32_t f_numLabels_i32, 
            int32_t f_numNodes_i32, 
            int32_t f_numFeatures_i32 )
     : m_weightSums_ai32( f_numNodes_i32, f_numLabels_i32 )
     , m_features_means_vaf32()
     , m_numFeatures_i32(f_numFeatures_i32)
     , m_nodeRelevance_ab( f_numNodes_i32, false )
{
    m_features_means_vaf32.reserve( f_numNodes_i32 * f_numLabels_i32 );
}


void CMultiNodeFirstPassStatistics::process_first_pass(
        const nodeIdx_t* nodeIter, const nodeIdx_t* const nodeIterEnd,
        const int32_t* labelIter,
        const int32_t* weightIter,
        smd::bundle_iterator_cf32_t featureIter )
{
    // initialze weights with zeros
    m_weightSums_ai32.set_zero();
    
    /// calculate mean values
    for( ; nodeIter != nodeIterEnd;
           ++nodeIter, ++labelIter, ++weightIter, ++featureIter )
    {
        if ( false == nodeIter->isFinal() )
        {
            const int32_t l_nodeIdx_i32 = nodeIter->idx();

            update_mean( *weightIter,
                         m_weightSums_ai32( l_nodeIdx_i32, *labelIter ),
                         *featureIter,
                         node_class_mean( l_nodeIdx_i32, *labelIter, m_features_means_vaf32 ).elem() );
        }
    }

    /// determ relevant nodes
    update_node_relevances();
}


int32_t
CMultiNodeFirstPassStatistics::get_inter_co_moment( smd::matrix_view_vf32_t& f_inter_co_moment_r ) const
{
    tree_input_check( num_features() == static_cast<int32_t>(f_inter_co_moment_r.elem().n_rows()) );
    tree_input_check( num_features() == static_cast<int32_t>(f_inter_co_moment_r.elem().n_cols()) );
    
    smd::array_vf32_t l_globalMeans_vf32( num_features() );
    tree_assert( f_inter_co_moment_r.n_cols() == l_globalMeans_vf32.size(), "Inconsistent vector sizes" );

    /// weight counter
    int32_t l_globalWeightSum_i32 = 0;
    
    /// initialize statistics with zeros
    f_inter_co_moment_r.set_zero();

    /// loop over all nodes
    for ( int32_t nIdx = 0; nIdx < num_nodes(); ++nIdx )
    {
        if ( false == m_nodeRelevance_ab[nIdx] )
            continue;  // node irrelevant
        
        /// initialize global means statistics with zeros
        l_globalMeans_vf32.set_zero();

        /// node specifc weight accumulator initialized with zero
        int32_t l_nodeWeightSum_i32 = 0;

        /// shortcut for node wise label weights
        const smd::array_view_ci32_t l_weightArray_ai32 = m_weightSums_ai32.row( nIdx );

        /// loop over labels
        for ( int32_t lIdx = 0; lIdx < num_labels(); ++lIdx )
        {
            const int32_t l_nlWeight_i32 = l_weightArray_ai32[ lIdx ];

            if (0 < l_nlWeight_i32)
            {// relevant label
            
                const int32_t l_vectorIdx_i32 = flat_nl_index( nIdx, lIdx );
                tree_assert( l_vectorIdx_i32 < static_cast<int32_t>( m_features_means_vaf32.size() ),
                             "Inconsistent Mean Container" );

                const smd::array_view_vf32_t& l_nlMean_r = m_features_means_vaf32[ l_vectorIdx_i32 ];
                tree_assert( num_features() == static_cast<int32_t>(l_nlMean_r.elem().size()),
                             "Inconsistent Mean Container" );
                
                /// update statistics
                update_co_moment( l_nlWeight_i32, l_nodeWeightSum_i32,
                                  l_nlMean_r,
                                  l_globalMeans_vf32,
                                  f_inter_co_moment_r );
            }
        }

        /// update global weight accumulator
        l_globalWeightSum_i32 += l_nodeWeightSum_i32;
    }

    return l_globalWeightSum_i32;
}


int32_t
CMultiNodeFirstPassStatistics::get_inter_covariance( smd::matrix_view_vf32_t& f_interCov_r ) const
{
    /// get inter covariance moment
    const int32_t l_globalWeightSum_i32 = get_inter_co_moment( f_interCov_r );

    /// copy and normalize lower triangle to upper triangle 
    co_moment_to_covariance( l_globalWeightSum_i32, f_interCov_r.elem(), f_interCov_r.elem() );

    return l_globalWeightSum_i32;
}


int32_t
CMultiNodeFirstPassStatistics::get_inter_covariance( smd::matrix_view_f32_t& f_interCov_r ) const
{
    /// temporary memory storing inter covariance moments
    smd::matrix_vf32_t l_inter_co_moment_vaf32( f_interCov_r.n_rows(), f_interCov_r.n_cols() );
    
    /// get inter covariance moment
    const int32_t l_globalWeightSum_i32 = get_inter_co_moment( l_inter_co_moment_vaf32 );

    /// copy and normalize lower triangle to upper triangle 
    co_moment_to_covariance( l_globalWeightSum_i32, l_inter_co_moment_vaf32.elem(), f_interCov_r );

    return l_globalWeightSum_i32;
}


void CMultiNodeFirstPassStatistics::update_mean(
        int32_t                            f_weight_i32,
        int32_t&                           f_weightSum_ri32,
        const smd::column_iterator_cf32_t& f_featureIter,
        smd::array_view_f32_t              f_mean_stat_af32 )
{
    /// Update Weight Sums
    f_weightSum_ri32 += f_weight_i32;

    /// next sample weight
    const float32_t l_weight_f32       = static_cast<float32_t>( f_weight_i32 );
    const float32_t l_scaledWeight_f32 = l_weight_f32 / static_cast<float32_t>( f_weightSum_ri32 );

    // --- Online update mean  ------
    
    /// kernel: x - xmean_{n-1}
    auto l_kernel = [=]( float32_t mean, float32_t feature ) -> float32_t { 
        return ( mean + (l_scaledWeight_f32 * (feature - mean)) );
    };

    /// apply kernel on features
    std::transform( f_mean_stat_af32.cbegin(), f_mean_stat_af32.cend(),
                    f_featureIter,
                    f_mean_stat_af32.begin(),   // Output
                    l_kernel );
}


void CMultiNodeFirstPassStatistics::update_node_relevances()
{
    for ( int32_t nIdx=0; nIdx < num_nodes(); ++nIdx )
    {
        int32_t l_labelCounter = 0;

        const auto iterEnd = m_weightSums_ai32.cend( nIdx );
        
        for (auto iter = m_weightSums_ai32.cbegin( nIdx ); 
                  (iter != iterEnd) && (l_labelCounter < 2); ++iter )
        {
            if (0 < *iter) ++l_labelCounter;
        }
        
        // Only nodes with more than two used labels are relevant
        if (1 < l_labelCounter)
            m_nodeRelevance_ab[nIdx] = true;
    }
}


const smd::array_view_vf32_t& 
CMultiNodeFirstPassStatistics::first_pass_mean( int32_t f_flatIdx_i32 ) const
{
    tree_assert( f_flatIdx_i32 < static_cast<int32_t>(m_features_means_vaf32.size()),
                 "Inconsistent array index" );
    return m_features_means_vaf32[f_flatIdx_i32];
}


const smd::array_view_vf32_t& 
CMultiNodeFirstPassStatistics::first_pass_mean( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32 ) const
{
    return node_class_mean( f_nodeIdx_i32, f_labelIdx_i32, m_features_means_vaf32 );
}


smd::array_view_vf32_t&
CMultiNodeFirstPassStatistics::node_class_mean( 
                                int32_t f_nodeIdx_i32,
                                int32_t f_labelIdx_i32,
        std::vector<smd::array_vf32_t>& f_meanStorage ) const
{
    const int32_t l_vectorIdx_i32 = flat_nl_index( f_nodeIdx_i32, f_labelIdx_i32 );

    tree_assert( l_vectorIdx_i32 < (num_labels() * num_nodes()), "Inconsistent array index" );

    /// check if vector has correct length
    if ( static_cast<int32_t>( f_meanStorage.size()) <= l_vectorIdx_i32 )
    {
        f_meanStorage.resize( l_vectorIdx_i32 );
        f_meanStorage.emplace_back( num_features() );
        f_meanStorage.back().set_zero();
    }
    else
    {
        /// check if mean vector has already been initialized
        if ( 0 == static_cast<int32_t>(f_meanStorage[l_vectorIdx_i32].size()) )
        {
            /// Initialize via move construction
            f_meanStorage[l_vectorIdx_i32] = smd::array_vf32_t( num_features() );

            /// set all entries to zeros
            f_meanStorage[l_vectorIdx_i32].set_zero();
        }
    }

    tree_assert( static_cast<int32_t>( f_meanStorage[l_vectorIdx_i32].elem().size() ) == num_features(), "Unexpected container size" );        

    return f_meanStorage[l_vectorIdx_i32];
}



const smd::array_view_vf32_t&
CMultiNodeFirstPassStatistics::node_class_mean( 
                                  int32_t f_nodeIdx_i32,
                                  int32_t f_labelIdx_i32,
    const std::vector<smd::array_vf32_t>& f_meanStorage ) const
{
    const int32_t l_vectorIdx_i32 = flat_nl_index( f_nodeIdx_i32, f_labelIdx_i32 );

    tree_assert( l_vectorIdx_i32 < (num_labels() * num_nodes()), "Inconsistent array index" );
    tree_bounds_check( l_vectorIdx_i32 < static_cast<int32_t>(f_meanStorage.size()) );
    tree_assert( static_cast<int32_t>(f_meanStorage[l_vectorIdx_i32].elem().size()) == num_features(),
                 "Accessing invalid mean value" );       

    return f_meanStorage[l_vectorIdx_i32];
}



void CMultiNodeFirstPassStatistics::update_co_moment( int32_t                        f_weight_i32,
                                                      int32_t&                       f_weightSum_ri32,
                                                      const smd::array_view_cvf32_t& f_val_af32,
                                                      smd::array_view_vf32_t&        f_mean_stat_af32,
                                                      smd::matrix_view_vf32_t&       f_co_stat_af32 )
{
    // https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Online :
    //
    // def online_weighted_covariance(data1, data2, data3):
    //     meanx = meany = 0
    //     wsum = 0
    //     C = 0
    //     for x, y, w in zip(data1, data2, data3):
    //         wsum += w
    //         dx = x - meanx
    //         meanx += (w / wsum) * dx
    //         meany += (w / wsum) * (y - meany)
    //         C += w * dx * (y - meany)
    //
    //     population_covar = C / wsum
    
    
    /// update weight statistic
    f_weightSum_ri32 += f_weight_i32;

    /// next sample weight
    const float32_t l_weight_f32 = static_cast<float32_t>(f_weight_i32);
    const float32_t l_scaledWeight_f32 = l_weight_f32 / static_cast<float32_t>( f_weightSum_ri32 );
    
    const int32_t l_num_elem_features = f_mean_stat_af32.elem().size();    // elementary array view

    // Online update mean and variance
    for ( int32_t rIdx = 0; rIdx < l_num_elem_features; ++rIdx )
    {
        /// x - xmean_{n-1}
        const float32_t l_rowNm1Diff_f32 = f_val_af32.elem(rIdx) - f_mean_stat_af32.elem(rIdx);

        /// update mean moment:  mean_n += (w/wsum) * (x - mean_{n-1})
        f_mean_stat_af32.elem(rIdx) += l_scaledWeight_f32 * l_rowNm1Diff_f32;

        /// vectorized w * (x - xmean_{n-1})
        const smd::vf32_t l_weightedRowNm1Diff_vf32( l_weight_f32 * l_rowNm1Diff_f32 );

        /// vectorized column size
        const int32_t l_diagVectColSize_i32 = rIdx / smd::vf32_t::size();
        tree_assert( l_diagVectColSize_i32 < static_cast<int32_t>( f_mean_stat_af32.elem().size() ), 
                     "Invalid vectorized column size" );

        /// process other off diagonal elements 
        for ( int32_t cIdx = 0; cIdx <= l_diagVectColSize_i32; ++cIdx )
        {
            /// y - ymean_n
            const smd::vf32_t l_colNDiff_vf32 = f_val_af32[cIdx] - f_mean_stat_af32[cIdx];

            /// update co_moment: C += w * (x - xmean_{n-1}) * (y - ymean_n)
            f_co_stat_af32( rIdx, cIdx ) += l_weightedRowNm1Diff_vf32 * l_colNDiff_vf32;
        }
    }
}


void
CMultiNodeFirstPassStatistics::co_moment_to_covariance(
                              int32_t  f_norm_weight_i32,
        const smd::matrix_view_cf32_t& f_co_moment_af32,
               smd::matrix_view_f32_t  f_cov_arf32 )
{
    const size_t l_num_features_i32 = f_co_moment_af32.n_cols();
    tree_dims_check( l_num_features_i32 == f_co_moment_af32.n_rows() );
    tree_dims_check( l_num_features_i32 == f_cov_arf32.n_cols() );
    
    const float32_t l_scale_f32 = 1.f / static_cast<float32_t>( f_norm_weight_i32 );
    
    /// Copy intra covariance
    for ( size_t rIdx = 0; rIdx < l_num_features_i32; ++rIdx )
    {
        /// process other off diagonal elements 
        for ( size_t cIdx = 0; cIdx < rIdx; ++cIdx )
        {
            /// set off diagonal elements
            f_cov_arf32( rIdx, cIdx ) = f_cov_arf32( cIdx, rIdx ) = l_scale_f32 * f_co_moment_af32( rIdx, cIdx );
        }

        /// set diagonal
        f_cov_arf32( rIdx, rIdx ) = l_scale_f32 * f_co_moment_af32( rIdx, rIdx );
    }
}


// --------------------------------------------------------------------------------------------------------------------


CMultiNodeClassStatistics::CMultiNodeClassStatistics( 
            int32_t f_numLabels_i32, 
            int32_t f_numNodes_i32, 
            int32_t f_numFeatures_i32 )
     : CMultiNodeFirstPassStatistics( f_numLabels_i32, f_numNodes_i32, f_numFeatures_i32 )
     , m_weightSum_i32( 0 )
     , m_second_pass_means_vaf32()
     , m_intra_co_moment_vaf32( f_numFeatures_i32, f_numFeatures_i32 )
{
    //m_second_pass_means_vaf32.reserve( f_numNodes_i32 * f_numLabels_i32 );
}



void CMultiNodeClassStatistics::process(
        const nodeIdx_t* nodeIter, const nodeIdx_t* const nodeIterEnd,
        const int32_t* labelIter,
        const int32_t* weightIter,
        smd::bundle_iterator_cf32_t featureIter )
{
    /// Run first pass
    process_first_pass( nodeIter, nodeIterEnd,
                        labelIter,
                        weightIter,
                        featureIter );
    
    /// Run second pass
    process_second_pass( nodeIter, nodeIterEnd,
                         labelIter,
                         weightIter,
                         featureIter );
}


void CMultiNodeClassStatistics::process_second_pass(
        const nodeIdx_t* nodeIter, const nodeIdx_t* const nodeIterEnd,
        const int32_t* labelIter,
        const int32_t* weightIter,
        smd::bundle_iterator_cf32_t featureIter )
{
    /// Initialize statistics
    m_weightSum_i32 = 0;
    m_weightSums_ai32.set_zero();
    m_intra_co_moment_vaf32.set_zero();
    
    /// Initialize second pass mean values
    m_second_pass_means_vaf32.resize( first_pass_means().size() );
    for ( int32_t nIdx = 0; nIdx < num_nodes(); ++nIdx )
    {
        if ( false == node_is_relevant( nIdx ) )
            continue;
        
        for ( int32_t lIdx = 0, l_flatIdx_i32 = flat_nl_index( nIdx, 0 );
                 (l_flatIdx_i32 < static_cast<int32_t>(m_second_pass_means_vaf32.size())) 
              && (lIdx < num_labels()); 
              l_flatIdx_i32 = flat_nl_index( nIdx, ++lIdx ) )
        {
            if (first_pass_mean(l_flatIdx_i32).size() > 0)
            {
                m_second_pass_means_vaf32[l_flatIdx_i32] = smd::array_vf32_t( num_features() );
                m_second_pass_means_vaf32[l_flatIdx_i32].set_zero();
            }
        }
    }

    /// Run second pass
    {
        /// storage to place centralized feature vector
        smd::array_vf32_t l_centralized_feature( num_features() );

        /// loop over all data
        for( ; nodeIter != nodeIterEnd;
            ++nodeIter, ++labelIter, ++weightIter, ++featureIter )
        {
            const int32_t l_nodeIdx_i32 = nodeIter->idx();
            
            if ( (true == nodeIter->isFinal()) || (false == node_is_relevant( l_nodeIdx_i32 ) ) )
                continue;

            /// centralize feature vector
            {
                tree_assert( featureIter->size() == first_pass_mean( l_nodeIdx_i32, *labelIter ).elem().size(),
                             "Inconsistent array size" );
                
                const auto l_centralized_feature_end = 
                std::transform( featureIter->begin(), featureIter->end(),
                                first_pass_mean( l_nodeIdx_i32, *labelIter ).elem().cbegin(),
                                l_centralized_feature.elem().begin(),
                                std::minus<float32_t>() );
                
                tree_assert( l_centralized_feature_end == l_centralized_feature.elem().cend(),
                             "Inconsistent iterator ending" );
            }

            /// update intra covariance statistics
            m_weightSum_i32 += *weightIter;    // Update Weight Sums

            // update co moments
            update_co_moment( *weightIter,
                              m_weightSums_ai32( l_nodeIdx_i32, *labelIter ),
                              l_centralized_feature,
                              second_pass_mean( l_nodeIdx_i32, *labelIter ),   /// get responsible feature mean storage
                              m_intra_co_moment_vaf32 );
        }
    }
}



int32_t CMultiNodeClassStatistics::get_intra_covariance( smd::matrix_view_f32_t& f_intraCov_r ) const
{
    tree_assert( smd::sum( m_weightSums_ai32 ) == m_weightSum_i32, "Inconsistent weights" );
    tree_dims_check( num_features() == static_cast<int32_t>( f_intraCov_r.n_rows() ) );
    tree_dims_check( num_features() == static_cast<int32_t>( f_intraCov_r.n_cols() ) );

    co_moment_to_covariance( m_weightSum_i32, m_intra_co_moment_vaf32.elem(), f_intraCov_r );

    return m_weightSum_i32;
}


} // namesapce tree
