/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <cstdint>


namespace tree
{


// -----------------------------------------------------------------------------------------


enum class EECode : int32_t
{
    FAILED_MEMORY_ALLOCATION = -111111
};

inline constexpr int32_t evalue( EECode f_e ) { return static_cast<int32_t>(f_e); }


// -----------------------------------------------------------------------------------------


enum class ELeafCode : int32_t
{
    // split val codes
    UNDEFINED_SPLIT_INDEX    = -1,
    NODE_UNUSED              = -2,

    // node idx code
    LEAF_FINISHED            = -3,
    
    // internal code
    UNDEFINED_FEATURE        = -4,

};

inline constexpr int32_t evalue( ELeafCode f_e ) { return static_cast<int32_t>(f_e); }
inline constexpr bool is_valid_leaf_code( int32_t f_c ) { return (0 <= f_c); }
inline constexpr bool is_valid_leaf_code( ELeafCode f_e ) { return is_valid_leaf_code( static_cast<int32_t>(f_e) ); }

// -----------------------------------------------------------------------------------------


}  // namespace tree

