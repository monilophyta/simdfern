/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <simd.hpp>
#include "tree_base_types.hpp"



namespace tree
{



template <typename Derived>
class CBaseSampleProjection
{
protected:

    Derived& derived() { return static_cast<Derived&>(*this); }
    const Derived& derived() const { return static_cast<const Derived&>(*this); }

protected:


    float32_t* m_projIter_f32;
    float32_t* m_projIterEnd_f32;

    smd::v32_array_aligner m_arrayAligner;

    size_t num_data() const { return std::distance( m_projIter_f32, m_projIterEnd_f32 ); }

    inline bool has_aligned_memory() const { return m_arrayAligner.has_aligned_memory(); }
    inline bool is_alignable_with( const float32_t* f_featureRow_pf32 ) const { return m_arrayAligner.is_alignable_with(f_featureRow_pf32); }

public:

    /*!
        \brief project features of many sample onto a projection axis using projection weights

        \param f_projIter pointer to the start of an array of projection dimensions. the projection items have to initialized!
        \param f_projIterEnd pointer to the end of an array of projection dimensions.
    */
    CBaseSampleProjection( float32_t* f_projIter_f32, float32_t* f_projIterEnd_f32 );

    /*!
        \brief project features of many sample onto a projection axis using projection weights

        \param f_weightIter pointer to the start of an array of projection weights
        \param f_weightIterEnd pointer to the end of an array of projection weights
        \param f_featureRowIter pointer to an array of pointer to arrays of feature rows
    */
    void map( const float32_t*        f_weightIter_pf32,
              const float32_t* const  f_weightIterEnd_pf32,
              const float32_t* const* f_featureRowIter_ppf32 );

protected:

    inline 
    void map_aligned( const float32_t*        f_weightIter_pf32,
                      const float32_t* const  f_weightIterEnd_pf32,
                      const float32_t* const* f_featureRowIter_ppf32 );

    inline 
    void map_unaligned( const float32_t*        f_weightIter_pf32,
                        const float32_t* const  f_weightIterEnd_pf32,
                        const float32_t* const* f_featureRowIter_ppf32 );
};

//--------------------------------------------------------------------------------------------------------

class CSimpleProjection : public CBaseSampleProjection<CSimpleProjection>
{
    friend class CBaseSampleProjection<CSimpleProjection>;
    typedef CBaseSampleProjection<CSimpleProjection> base_t;

public:

    // constructor is inherited
    using base_t::base_t;

private:

    const float32_t*   map_prealigned_features( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 );
    const smd::vf32_t* map_aligned_features( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 );
    const float32_t*   map_postaligned_feature( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 );
    
    const float32_t* map_unaligned_feature( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 );


    template <typename FT>
    static inline const FT* map_feature( const FT*  f_sampleIter,
                                         FT         f_weight,
                                         FT*        f_projIter,
                                         FT* const  f_projIterEnd );
};

//--------------------------------------------------------------------------------------------------------

template <typename Derived>
class CBaseCompensatedProjection : public CBaseSampleProjection<Derived>
{
    typedef CBaseSampleProjection<Derived> base_t;

protected:

    typedef smd::simple_allocator<float32_t> allocator_t;
    typedef smd::deleter<allocator_t>        deleter_t;

public:

    /*!
        \brief project features of many sample onto a projection axis using projection weights

        \param f_projIter pointer to the start of an array of projection dimensions. the projection items have to initialized!
        \param f_projIterEnd pointer to the end of an array of projection dimensions.
    */
    CBaseCompensatedProjection( float32_t* f_projIter_f32, float32_t* f_projIterEnd_f32 );
    ~CBaseCompensatedProjection();

private:
    std::unique_ptr<float32_t[],deleter_t> m_compPtr_pf32;

protected:
    float32_t*                             m_comp_pf32;
};

//--------------------------------------------------------------------------------------------------------

template <typename Derived>
class CFirstOrderCompensatedProjection : public CBaseCompensatedProjection<Derived>
{
    friend class CBaseSampleProjection<Derived>;
    typedef CBaseCompensatedProjection<Derived> base_t;

protected:

    using base_t::derived;

public:

    using base_t::base_t;

protected:

    const float32_t*   map_prealigned_features( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 );
    const smd::vf32_t* map_aligned_features( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 );
    const float32_t*   map_postaligned_feature( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 );
    
    const float32_t* map_unaligned_feature( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 );
};

//--------------------------------------------------------------------------------------------------------


class CKahanSampleProjection : public CFirstOrderCompensatedProjection<CKahanSampleProjection>
{
    friend class CBaseSampleProjection<CKahanSampleProjection>;
    friend class CFirstOrderCompensatedProjection<CKahanSampleProjection>;

    typedef CFirstOrderCompensatedProjection<CKahanSampleProjection> base_t;

public:

    // constructor is inherited
    using base_t::base_t;

private:

    template <typename FT>
    static inline const FT* map_feature( const FT*  f_sampleIter,
                                         FT         f_weight,
                                         FT*        f_projIter,
                                         FT* const  f_projIterEnd,
                                         FT*        f_compIter );
};


//--------------------------------------------------------------------------------------------------------

class CNeumaierSampleProjection : public CFirstOrderCompensatedProjection<CNeumaierSampleProjection>
{
    friend class CBaseSampleProjection<CNeumaierSampleProjection>;
    friend class CFirstOrderCompensatedProjection<CNeumaierSampleProjection>;

    typedef CFirstOrderCompensatedProjection<CNeumaierSampleProjection> base_t;

public:

    // constructor is inherited
    using base_t::base_t;

private:


    void map_aligned( const float32_t*        f_weightIter_pf32,
                      const float32_t* const  f_weightIterEnd_pf32,
                      const float32_t* const* f_featureRowIter_ppf32 );


    void map_unaligned( const float32_t*        f_weightIter_pf32,
                        const float32_t* const  f_weightIterEnd_pf32,
                        const float32_t* const* f_featureRowIter_ppf32 );


    template <typename FT>
    static inline const FT* map_feature( const FT*  f_sampleIter,
                                         FT         f_weight,
                                         FT*        f_projIter,
                                         FT* const  f_projIterEnd,
                                         FT*        f_compIter );
};

//--------------------------------------------------------------------------------------------------------


class CKleinSampleProjection : public CBaseCompensatedProjection<CKleinSampleProjection>
{
    friend class CBaseSampleProjection<CKleinSampleProjection>;

    typedef CBaseCompensatedProjection<CKleinSampleProjection> base_t;

    using typename base_t::allocator_t;
    using typename base_t::deleter_t;

public:

    /*!
        \brief project features of many sample onto a projection axis using projection weights

        \param f_projIter pointer to the start of an array of projection dimensions. the projection items have to initialized!
        \param f_projIterEnd pointer to the end of an array of projection dimensions.
    */
    CKleinSampleProjection( float32_t* f_projIter_f32, float32_t* f_projIterEnd_f32 );
    ~CKleinSampleProjection();

private:

    std::unique_ptr<float32_t[],deleter_t> m_comp2Ptr_pf32;

protected:

    float32_t*                             m_comp2_pf32;

private:

    void map_aligned( const float32_t*        f_weightIter_pf32,
                      const float32_t* const  f_weightIterEnd_pf32,
                      const float32_t* const* f_featureRowIter_ppf32 );


    void map_unaligned( const float32_t*        f_weightIter_pf32,
                        const float32_t* const  f_weightIterEnd_pf32,
                        const float32_t* const* f_featureRowIter_ppf32 );


    const float32_t*   map_prealigned_features( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 );
    const smd::vf32_t* map_aligned_features( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 );
    const float32_t*   map_postaligned_feature( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 );
    
    const float32_t* map_unaligned_feature( const float32_t* f_featureRow_pf32, float32_t f_weight_f32 );


    template <typename FT>
    static inline const FT* map_feature( const FT*  f_sampleIter,
                                         FT         f_weight,
                                         FT*        f_projIter,
                                         FT* const  f_projIterEnd,
                                         FT*        f_comp1Iter,
                                         FT*        f_comp2Iter );
};



} // namespace tree
