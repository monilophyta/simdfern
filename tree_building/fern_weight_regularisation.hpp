/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <type_traits>
#include <simd.hpp>
#include "tree_base_types.hpp"


namespace tree
{

//-------------------------------------------------------------------------------------------

/**
 * Dummy penalizer for NO regularization
 */
struct CNoPenalizer
{  
    inline constexpr float32_t calc_penalty( float32_t ) const { return 0.f; }
};
static_assert( true == std::is_standard_layout<CNoPenalizer>::value );

//-------------------------------------------------------------------------------------------

/**
 * Simple regularization by fading between L1 and L2 norm
 * \f{equation}{
 *  - R(x) = (\text{l1Ratio} * \left| x \right|) + (\text{1-l1Ratio} * \left| x \right|^2)
 * \f}
 */
struct CPenalizer
{
    float32_t m_regulStrength_f32;
    float32_t m_l1Ratio_f32;

    inline
    CPenalizer( float32_t f_regulStrength_f32 = 0.f,
                float32_t f_l1Ratio_f32       = 1.f );
    
    inline float32_t calc_penalty( float32_t f_value_f32 ) const;
};
static_assert( true == std::is_standard_layout<CPenalizer>::value );

//-------------------------------------------------------------------------------------------

template <typename MT>
struct CBaseVectPenalizer
{
    float32_t m_regulStrength_f32;

    const MT* m_offset_pf32;
    const MT* m_offsetEnd_pf32;
    const MT* m_scale_pf32;

    inline
    CBaseVectPenalizer( float32_t f_regulStrength_f32 = 0.f,
                        const MT* f_offset_pvf32      = nullptr,
                        const MT* f_offsetEnd_pvf32   = nullptr,
                        const MT* f_scale_pvf32       = nullptr );

    inline size_t num_features() const { return (smd::num_items(MT()) * std::distance(m_offset_pf32, m_offsetEnd_pf32)); }
    inline std::pair<float32_t,float32_t> calc_l1l2( float32_t f_value_f32 ) const;
};


//-------------------------------------------------------------------------------------------


/**
 * Mapping regularization with fading between L1 and L2 norm
 * \f{equation}{
 *  - R(x) = (\text{l1Ratio} * \left| \text{scale} * x + \text{offset} \right|) + (\text{1-l1Ratio} * \left| \text{scale} * x + \text{offset} \right|^2)
 * \f}
 */
template <typename MT>
struct CVectPenalizer
{
    CBaseVectPenalizer<MT> m_base;
    float32_t m_l1Ratio_f32;

    inline
    CVectPenalizer( float32_t f_regulStrength_f32 = 0.f,
                    float32_t f_l1Ratio_f32       = 1.f,
                    const MT* f_offset_pvf32      = nullptr,
                    const MT* f_offsetEnd_pvf32   = nullptr,
                    const MT* f_scale_pvf32       = nullptr );

    inline float32_t calc_penalty( float32_t f_value_f32 ) const;

    inline size_t num_features() const { return m_base.num_features(); }

    inline operator const CBaseVectPenalizer<MT>&() const { return m_base; }
    inline operator CBaseVectPenalizer<MT>&() { return m_base; }
};


//-------------------------------------------------------------------------------------------

/**
 * Normalized Regularization
 * \f{equation}{
 *  - R(x) = (\left| x \right|_1 + \text{l1Offset}) / \sqrt( x*x + \text{sqrL2Offset})
 * \f}
 */
struct CNormalizedL1Penalizer
{
    float32_t m_regulStrength_f32;
    float32_t m_l1Offset_f32;
    float32_t m_sqrL2Offset_f32;

    inline
    CNormalizedL1Penalizer( float32_t f_regulStrength_f32 = 0.f,
                            float32_t f_l1Offset_f32      = 0.f,
                            float32_t f_sqrL2Offset_f32   = 1.f );

    inline float32_t calc_penalty( float32_t f_value_f32 ) const;
};
static_assert( true == std::is_standard_layout<CNormalizedL1Penalizer>::value );


//-------------------------------------------------------------------------------------------

/**
 * Normalized Regularization
 * \f{equation}{
 *  - R(x) = (\left| \text{scale} * x + \text{offset} \right|_1) / (\left| \text{scale} * x + \text{offset} \right|_2)
 * \f}
 */
template <typename MT>
struct CNormalizedL1VectPenalizer : public CBaseVectPenalizer<MT>
{
    // inherit constructor
    using CBaseVectPenalizer<MT>::CBaseVectPenalizer;

    inline float32_t calc_penalty( float32_t f_value_f32 ) const;
};

//-------------------------------------------------------------------------------------------

} // namespace tree
