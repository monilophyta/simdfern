/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <numeric>
#include <iterator>
#include "tree_plane_param_optimization_base.hpp"
#include "tree_enums.hpp"
#include "tree_debug.hpp"
//#include "tree.hpp"



namespace tree 
{

int32_t*
order_unsorted_features( std::unique_ptr<int32_t[]>& f_srtIdx_ptr,
                         const float32_t* const f_featureArray,
                         const float32_t* const f_featureEnd )
{
    const size_t num_data = std::distance( f_featureArray, f_featureEnd );
    
    /// Smart Pointer for index array
    f_srtIdx_ptr.reset( new (std::nothrow) int32_t[ num_data ] );

    /// Handling problem of no memory allocation
    if ( nullptr == f_srtIdx_ptr.get() )
    {
        return nullptr;
    }

    /// iterator end of index array
    int32_t* const l_srtIdxEnd = std::next( f_srtIdx_ptr.get(), num_data );
    
    /// Initialize with range 0...num_data
    std::iota( f_srtIdx_ptr.get(), l_srtIdxEnd, 0 );

    /// argsort of feature array
    {
        // more complicate comparison kernel also copes with NaN values
        // These value are accumlated at the end of the sequence
        // Comparisons using >= or <= resultet in segmentation faults
        auto comp = [=] ( int32_t idx1, int32_t idx2) -> bool { 
            return ( (f_featureArray[idx1] < f_featureArray[idx2]) || 
                     (std::isnan(f_featureArray[idx2]) && (!std::isnan(f_featureArray[idx1])) ) ); };
        
        std::sort( f_srtIdx_ptr.get(), l_srtIdxEnd, comp );
        TREE_TODO( "Performance improvment: Implement sorting via bucket-wise skew-heap sorting" );
    }

    return l_srtIdxEnd;
}



} // namespace tree

