/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "fern_weight_regularisation.hpp"
#include "tree_debug.hpp"


namespace tree
{

//-------------------------------------------------------------------------------------------

CPenalizer::CPenalizer( float32_t f_regulStrength_f32,
                        float32_t f_l1Ratio_f32 )
    : m_regulStrength_f32( f_regulStrength_f32 )
    , m_l1Ratio_f32( f_l1Ratio_f32 )
{
    tree_input_check( 0.f <= f_regulStrength_f32 );

    tree_input_check( 0.f <= f_l1Ratio_f32 );
    tree_input_check( 1.f >= f_l1Ratio_f32 );
}


inline float32_t
CPenalizer::calc_penalty( float32_t f_value_f32 ) const
{
    return ( m_regulStrength_f32 * ((m_l1Ratio_f32 * std::abs(f_value_f32)) + 
                                    ((1.f - m_l1Ratio_f32) * f_value_f32 * f_value_f32)) );
}

//-------------------------------------------------------------------------------------------

template <typename MT>
CBaseVectPenalizer<MT>::CBaseVectPenalizer( float32_t f_regulStrength_f32,
                                            const MT* f_offset_pf32,
                                            const MT* f_offsetEnd_pf32,
                                            const MT* f_scale_pf32 )
    : m_regulStrength_f32( f_regulStrength_f32 )
    , m_offset_pf32( f_offset_pf32 )
    , m_offsetEnd_pf32( f_offsetEnd_pf32 )
    , m_scale_pf32( f_scale_pf32 )
{
    static_assert( true == std::is_standard_layout<CBaseVectPenalizer<MT>>::value );

    tree_input_check( 0.f <= f_regulStrength_f32 );
    tree_input_check( f_offset_pf32 <= f_offsetEnd_pf32 );
    tree_input_check( bool(f_offsetEnd_pf32 == nullptr) == bool( f_scale_pf32 == nullptr ) );
}
static_assert( sizeof(CBaseVectPenalizer<float32_t>) == sizeof(CBaseVectPenalizer<smd::vf32_t>) );



template <typename MT>
inline std::pair<float32_t,float32_t>
CBaseVectPenalizer<MT>::calc_l1l2( float32_t f_value_f32 ) const
{
    const MT l_value_f32( f_value_f32 );

    MT l_l1sum(0);
    MT l_l2sum(0);
    {
        // compensators
        MT l_l1comp(0);
        MT l_l2comp(0);

        /// linear mapping of value
        for (const MT *offsetIter = m_offset_pf32,
                       *scaleIter = m_scale_pf32; 
                offsetIter != m_offsetEnd_pf32;
            ++offsetIter, ++scaleIter )
        {
            /// x = v + tau * gradient
            const MT l_map = (l_value_f32 * (*scaleIter)) + (*offsetIter);

            /// += |x|
            smd::kahan_sum_step( l_l1sum, std::abs( l_map ), l_l1comp );
            
            /// += x*x
            smd::kahan_sum_step( l_l2sum, l_map, l_map, l_l2comp );
        }
    }
    return std::make_pair( smd::sum( l_l1sum ), smd::sum( l_l2sum ) );
}

// Explicit instantiation
//template struct CBaseVectPenalizer<float32_t>;
//template struct CBaseVectPenalizer<smd::vf32_t>;

//-------------------------------------------------------------------------------------------


template <typename MT>
CVectPenalizer<MT>::CVectPenalizer( float32_t f_regulStrength_f32,
                                    float32_t f_l1Ratio_f32,
                                    const MT* f_offset_pf32,
                                    const MT* f_offsetEnd_pf32,
                                    const MT* f_scale_pf32 )
    : m_base( f_regulStrength_f32, f_offset_pf32, f_offsetEnd_pf32, f_scale_pf32 )
    , m_l1Ratio_f32( f_l1Ratio_f32 )
{
    static_assert( true == std::is_standard_layout<CVectPenalizer<MT>>::value );
    //static_assert( sizeof(CVectPenalizer<MT>) == (sizeof(CBaseVectPenalizer<MT>) + sizeof(float32_t)) );

    tree_input_check( 0.f <= f_l1Ratio_f32 );
    tree_input_check( 1.f >= f_l1Ratio_f32 );
}
static_assert( sizeof(CVectPenalizer<float32_t>) == sizeof(CVectPenalizer<smd::vf32_t>) );



template <typename MT>
inline float32_t
CVectPenalizer<MT>::calc_penalty( float32_t f_value_f32 ) const
{
    const auto l_l1l2sum = m_base.calc_l1l2( f_value_f32 );

    const float32_t l_l1value_f32 = m_l1Ratio_f32 * l_l1l2sum.first;
    const float32_t l_l2value_f32 = (1.f - m_l1Ratio_f32) * l_l1l2sum.second;

    return (m_base.m_regulStrength_f32 * (l_l1value_f32 + l_l2value_f32));
}

// Explicit instantiation
//template struct CVectPenalizer<float32_t>;
//template struct CVectPenalizer<smd::vf32_t>;

//-------------------------------------------------------------------------------------------

inline
CNormalizedL1Penalizer::CNormalizedL1Penalizer( float32_t f_regulStrength_f32,
                                                float32_t f_l1Offset_f32,
                                                float32_t f_sqrL2Offset_f32 )
    : m_regulStrength_f32( f_regulStrength_f32 )
    , m_l1Offset_f32( f_l1Offset_f32 )
    , m_sqrL2Offset_f32( f_sqrL2Offset_f32 )
{
    tree_input_check( 0.f <= f_regulStrength_f32 );
    tree_input_check( 0.f <= f_l1Offset_f32 );
    tree_input_check( 0.f <= f_sqrL2Offset_f32 );
}


inline float32_t 
CNormalizedL1Penalizer::calc_penalty( float32_t f_value_f32 ) const
{
    const float32_t l_l1norm = std::abs( f_value_f32 ) + m_l1Offset_f32;
    const float32_t l_l2norm = std::sqrt( (f_value_f32 * f_value_f32) + m_sqrL2Offset_f32 );

    return m_regulStrength_f32 * (l_l1norm / std::max(l_l2norm, std::numeric_limits<float32_t>::epsilon()));
}

//-------------------------------------------------------------------------------------------


template <typename MT>
inline float32_t
CNormalizedL1VectPenalizer<MT>::calc_penalty( float32_t f_value_f32 ) const
{
    static_assert( true == std::is_standard_layout<CNormalizedL1VectPenalizer<MT>>::value );   
    
    const auto l_l1l2sum = this->calc_l1l2( f_value_f32 );
    
    const float32_t l_regulTerm_f32 = (l_l1l2sum.second > std::numeric_limits<float32_t>::min())
                                         ? (l_l1l2sum.first / std::sqrt(l_l1l2sum.second))
                                         : 0.f;

    return (this->m_regulStrength_f32 * l_regulTerm_f32);
}
static_assert( sizeof(CNormalizedL1VectPenalizer<float32_t>) == sizeof(CNormalizedL1VectPenalizer<smd::vf32_t>) );


// Explicit instantiation
//template struct CNormalizedL1VectPenalizer<float32_t>;
//template struct CNormalizedL1VectPenalizer<smd::vf32_t>;


} // namespace tree

