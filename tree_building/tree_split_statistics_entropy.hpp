/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "tree_split_statistics_base.hpp"


namespace tree
{


struct CTreeSplitEntropyStatistics : public CTreeSplitStatisticsBase
{

private:

    typedef CTreeSplitStatisticsBase      base_t;

    smd::matrix_vf32_t                    m_XlogX_af32;              // [ #Leaf x #Label ]
    smd::array_vf32_t                     m_unormedEntropies_af32;   // [ #Leaf ]

public:

    CTreeSplitEntropyStatistics( int32_t f_numLabels_i32, int32_t f_numNodes_i32 );

    virtual void initialize_zero() override;  // will be called from base class
    void initialize_from( const CTreeSplitEntropyStatistics& other );
    void initialize_from( const CTreeSplitStatisticsBase& other );

    const int32_t*
    initialize_from( const nodeIdx_t* nodeIter, const nodeIdx_t* const nodeIterEnd,
                     const int32_t* labelIter,
                     const int32_t* weightIter );

    void increase_counts( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 );
    void decrease_counts( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 );

    float32_t calc_increase_rating( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 ) const;
    float32_t calc_decrease_rating( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 ) const;

    /*!
      \brief TODO

      \param f_nodeIdx_i32 TODO

      \return node_rating rating >= 0; smaller values are better
    */
    inline float32_t node_rating( int32_t f_nodeIdx_i32 ) const { return m_unormedEntropies_af32.elem( f_nodeIdx_i32 ); }

    float32_t total_rating() const;

    inline void repair_statistics() { recalculate_entropy_stats(); }

    virtual bool assert_correctness() const override;

private:

    inline float32_t calculate_unormed_node_entropy( int32_t f_nodeIdx_i32 ) const;

    inline void update_entropy_stats( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32 );

    void recalculate_entropy_stats();

    inline float32_t calc_update_rating( int32_t f_nodeIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 ) const;
};


} // namespace tree
