/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <type_traits>
#include <simd.hpp>
#include <image.hpp>
#include "tree_box_feature_evaluation_base.hpp"
#include "tree_box_feature.hpp"
#include "tree_base_types.hpp"



namespace tree
{


//-----------------------------------------------------------------------------------------------------------------------


template <typename IMG_T, typename FEATURE_FUNC, typename PARAM_T>
struct CBoxSingleEvalParamTypes
        : public CBoxEvalParamTypes< IMG_T,FEATURE_FUNC,PARAM_T >
{
    typedef img::intbox_i32_t                        intbox_t;
    typedef img::feature_extent_i32_t                feature_extent_t;

    typedef float32_t                                feature_t;
};



template <typename IMG_T, typename FEATURE_FUNC, typename PARAM_T>
struct CBoxMultipleEvalParamTypes
        : public CBoxEvalParamTypes< IMG_T,FEATURE_FUNC,PARAM_T >
{
    typedef img::intbox_vi32_t                       intbox_t;
    typedef img::feature_extent_vi32_t               feature_extent_t;

    typedef smd::vf32_t                              feature_t;
};



//-----------------------------------------------------------------------------------------------------------------------


template <typename IMG_T, typename FEATURE_FUNC, typename PARAM_T>
struct CBoxFeatureEvaluatorI32
    : public CBoxFeatureEvaluatorBase< 
            CBoxSingleEvalParamTypes< IMG_T, FEATURE_FUNC, PARAM_T >,
            CBoxFeatureEvaluatorI32< IMG_T, FEATURE_FUNC, PARAM_T > >
{
    
    typedef CBoxFeatureEvaluatorI32< IMG_T, FEATURE_FUNC, PARAM_T >    this_t;
    
    typedef CBoxFeatureEvaluatorBase< 
            CBoxSingleEvalParamTypes< IMG_T, FEATURE_FUNC, PARAM_T >,
            this_t>                                                    base_t;

    using typename base_t::img_t;
    using typename base_t::boxpair_t;
    using typename base_t::feature_t;
    using typename base_t::feature_func_t;

    using base_t::m_imageFrame;
    using base_t::m_imagePointer;
    using base_t::m_boxPairs;
    using base_t::m_allOverExtent;

    using base_t::evaluate_borderless_boxpair;

    using base_t::base_t;


    feature_t evaluate_borderless_boxpair( const img::image_coord_i32_t& f_ankerIdx, 
                                                                int32_t  f_featureIdx ) const;
    
    feature_t evaluate_borderchecked_boxpair( const img::image_coord_i32_t& f_ankerIdx, 
                                                                   int32_t  f_featureIdx ) const;


    feature_t evaluate_borderchecked_boxpair( const img::image_coord_i32_t& f_ankerIdx, 
                                              const              boxpair_t& f_boxPair,
                                                                   int32_t  f_ankerOffset ) const;

};



//-----------------------------------------------------------------------------------------------------------------------



template <typename IMG_T, typename FEATURE_FUNC, typename PARAM_T>
struct CBoxFeatureEvaluatorV32
    : public CBoxFeatureEvaluatorBase< 
            CBoxMultipleEvalParamTypes< IMG_T, FEATURE_FUNC, PARAM_T >,
            CBoxFeatureEvaluatorV32< IMG_T, FEATURE_FUNC, PARAM_T > >
{
    typedef CBoxFeatureEvaluatorV32< IMG_T, FEATURE_FUNC, PARAM_T >       this_t;
    typedef CBoxFeatureEvaluatorBase< 
            CBoxMultipleEvalParamTypes< IMG_T, FEATURE_FUNC, PARAM_T >,
            this_t >                                                      base_t;
    
    using typename base_t::img_t;
    using typename base_t::boxpair_t;
    using typename base_t::feature_t;
    using typename base_t::feature_func_t;
    
    using base_t::m_imageFrame;
    using base_t::m_imagePointer;
    using base_t::m_boxPairs;
    using base_t::m_allOverExtent;

    using base_t::evaluate_borderless_boxpair;

    using base_t::base_t;

    
    feature_t evaluate_borderchecked_boxpair( const img::image_coord_i32_t& f_ankerIdx, 
                                              const              boxpair_t& f_boxPair,
                                                                   int32_t  f_ankerOffset ) const;
};



//-----------------------------------------------------------------------------------------------------------------------


template <typename IMG_T, typename FEATURE_FUNC, typename PARAM_T>
struct CBoxFeatureEvaluatorI32;

template <typename IMG_T, typename FEATURE_FUNC, typename PARAM_T>
struct CBoxFeatureEvaluatorV32;


typedef CBoxIntRatio<float32_t>                  boxratio_eval_func_t;
typedef CBoxIntRatio<const smd::vf32_t&>        boxratio_eval_func_vt;

typedef CBoxIntDifference<float32_t>              boxdiff_eval_func_t;
typedef CBoxIntDifference<const smd::vf32_t&>    boxdiff_eval_func_vt;


//-----------------------------------------------------------------------------------------------------------------------

enum class EUseSIMD : bool
{
    USE_ELEM_FLOAT32 = false,
    USE_SIMD_FLOAT32 = true
};



//-----------------------------------------------------------------------------------------------------------------------



template< typename T, EUseSIMD USE_SIMD >
using boxratio_eval_t = typename
    std::conditional< USE_SIMD == EUseSIMD::USE_SIMD_FLOAT32,
                      CBoxFeatureEvaluatorV32< typename std::remove_cv<T>::type, boxratio_eval_func_vt, dummy_param_t<sizeof(smd::vf32_t)> >,
                      CBoxFeatureEvaluatorI32< typename std::remove_cv<T>::type, boxratio_eval_func_t,  dummy_param_t<0>>
                    >::type;


typedef boxratio_eval_t<   int32_t, EUseSIMD::USE_ELEM_FLOAT32 > boxratio_eval_i32_t;
typedef boxratio_eval_t< float32_t, EUseSIMD::USE_ELEM_FLOAT32 > boxratio_eval_f32_t;

typedef boxratio_eval_t<   int32_t, EUseSIMD::USE_SIMD_FLOAT32 > boxratio_eval_vi32_t;
typedef boxratio_eval_t< float32_t, EUseSIMD::USE_SIMD_FLOAT32 > boxratio_eval_vf32_t;


// The following static asserts ensure correct alignment with the vector structure m_boxPairs

static_assert( 0 == ( sizeof(boxratio_eval_vi32_t::intbox_t)         % sizeof(smd::vi32_t) ) );
static_assert( 0 == ( sizeof(boxratio_eval_vi32_t::feature_extent_t) % sizeof(smd::vi32_t) ) );
static_assert( 0 == ( sizeof(boxratio_eval_vi32_t::feature_param_t)  % sizeof(smd::vf32_t) ) );
static_assert( 0 == ( sizeof(boxratio_eval_vi32_t::boxpair_t)        % sizeof(smd::vi32_t) ) );


static_assert( 0 == ( sizeof(boxratio_eval_vf32_t::intbox_t)         % sizeof(smd::vi32_t) ) );
static_assert( 0 == ( sizeof(boxratio_eval_vf32_t::feature_extent_t) % sizeof(smd::vi32_t) ) );
static_assert( 0 == ( sizeof(boxratio_eval_vf32_t::feature_param_t)  % sizeof(smd::vf32_t) ) );
static_assert( 0 == ( sizeof(boxratio_eval_vf32_t::boxpair_t)        % sizeof(smd::vi32_t) ) );

//-----------------------------------------------------------------------------------------------------------------------



template< typename T, EUseSIMD USE_SIMD >
using boxdiff_eval_t = typename
    std::conditional< USE_SIMD == EUseSIMD::USE_SIMD_FLOAT32,
                      CBoxFeatureEvaluatorV32< typename std::remove_cv<T>::type, boxdiff_eval_func_vt, const smd::vf32_t&>,
                      CBoxFeatureEvaluatorI32< typename std::remove_cv<T>::type, boxdiff_eval_func_t,          float32_t>
                    >::type;

typedef boxdiff_eval_t<   int32_t, EUseSIMD::USE_ELEM_FLOAT32 >  boxdiff_eval_i32_t;
typedef boxdiff_eval_t< float32_t, EUseSIMD::USE_ELEM_FLOAT32 >  boxdiff_eval_f32_t;

typedef boxdiff_eval_t<   int32_t, EUseSIMD::USE_SIMD_FLOAT32 >  boxdiff_eval_vi32_t;
typedef boxdiff_eval_t< float32_t, EUseSIMD::USE_SIMD_FLOAT32 >  boxdiff_eval_vf32_t;


// The following static asserts ensure correct alignment with the vector structure m_boxPairs

static_assert( 0 == ( sizeof(boxdiff_eval_vi32_t::intbox_t)         % sizeof(smd::vi32_t) ) );
static_assert( 0 == ( sizeof(boxdiff_eval_vi32_t::feature_extent_t) % sizeof(smd::vi32_t) ) );
static_assert( 0 == ( sizeof(boxdiff_eval_vi32_t::feature_param_t)  % sizeof(smd::vf32_t) ) );
static_assert( 0 == ( sizeof(boxdiff_eval_vi32_t::boxpair_t)        % sizeof(smd::vi32_t) ) );


static_assert( 0 == ( sizeof(boxdiff_eval_vf32_t::intbox_t)         % sizeof(smd::vi32_t) ) );
static_assert( 0 == ( sizeof(boxdiff_eval_vf32_t::feature_extent_t) % sizeof(smd::vi32_t) ) );
static_assert( 0 == ( sizeof(boxdiff_eval_vf32_t::feature_param_t)  % sizeof(smd::vf32_t) ) );
static_assert( 0 == ( sizeof(boxdiff_eval_vf32_t::boxpair_t)        % sizeof(smd::vi32_t) ) );

//-----------------------------------------------------------------------------------------------------------------------


// explicit instantiation
extern template struct CBoxFeatureEvaluatorI32<   int32_t,  boxdiff_eval_func_t,     float32_t>;
extern template struct CBoxFeatureEvaluatorI32< float32_t,  boxdiff_eval_func_t,     float32_t>;
extern template struct CBoxFeatureEvaluatorV32<   int32_t,  boxdiff_eval_func_vt, const smd::vf32_t&>;
extern template struct CBoxFeatureEvaluatorV32< float32_t,  boxdiff_eval_func_vt, const smd::vf32_t&>;


//-----------------------------------------------------------------------------------------------------------------------


} // namespace tree

