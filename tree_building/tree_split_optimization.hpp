/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>
#include "tree_split_optimization_base.hpp"

#include "tree_split_statistics_entropy.hpp"
#include "tree_split_statistics_gini.hpp"


namespace tree
{


//-------------------------------------------------------------------------------------------

template <typename split_stat_T>
struct CFeatureSplitOptimizer;

typedef CFeatureSplitOptimizer< CTreeSplitGiniStatistics >        split_gini_optimizer_t;
typedef CFeatureSplitOptimizer< CTreeSplitEntropyStatistics >  split_entropy_optimizer_t;


//-------------------------------------------------------------------------------------------


template <typename rating_T>
struct CNodeState
{
private:

    typedef rating_T rating_t;

    struct optimal_t
    {
        rating_t        m_nodeRating;
        int32_t         m_featureIdx_i32;
        float32_t       m_featureSplit_f32;

        EFeatureContrib m_nextSplitImprovement_e;

        optimal_t();
    };


    struct limboSplit_t
    {
        float32_t   m_featureVal_f32;

        limboSplit_t();
    };

    optimal_t            m_optimal;
    limboSplit_t         m_limboSplit;
    
    /// structure padding
    POWER_OF_TWO_PADDING( sizeof(optimal_t) + sizeof(limboSplit_t) );

public:

    CNodeState();

    inline const optimal_t& optimal() const { return m_optimal; }
    inline const limboSplit_t& limbo() const { return m_limboSplit; }


    void deactivate();
    bool is_active() const;

    void set_rating( rating_t f_nodeRating );

    EFeatureContrib update_optimal_split( int32_t f_featureIdx_i32, rating_t f_nodeRating, float32_t f_featureVal_f32 );
    
    void      initialize_limbo_split();
    void      set_limbo_split( float32_t f_featureVal_f32 );
    float32_t get_limbo_split() const;
};


//-------------------------------------------------------------------------------------------


template <typename split_stat_T>
struct CFeatureSplitOptimizer : public CBaseSplitOptimizer< split_stat_T, CFeatureSplitOptimizer<split_stat_T> >
{   

private:

    typedef CFeatureSplitOptimizer<split_stat_T>               this_t;
    typedef CBaseSplitOptimizer< split_stat_T, this_t >        base_t;

    using typename base_t::rating_t;
    using typename base_t::split_stat_t;

    typedef CNodeState<rating_t>                         node_state_t;
    static_assert( true == cppt::isPowerOfTwo( sizeof(node_state_t) ) ); // c++ performance optimization

    /// Storage of "Optimal Split Results" for every node
    std::vector<node_state_t> m_nodeStates;        // [ #Leaf ]


public:

    //-----------------------------------------------------------------------------------

    CFeatureSplitOptimizer( int32_t f_numLabels_i32, int32_t f_numNodes_i32 );

    void initialize_node_ratings( const split_stat_t& m_rightStat );

    inline size_t n_nodes() const { return m_nodeStates.size(); }


    //-----------------------------------------------------------------------------------

    void initialize_feature_run( int32_t  f_nextFeatureIdx_i32,
                                 const split_stat_t& f_previousLayerStat );

    //-----------------------------------------------------------------------------------


    void process_invalid_feature_sample( const int32_t   f_labelIdx_i32,
                                         const nodeIdx_t f_nodeIdx,
                                         const int32_t   f_weight_i32 );

    inline
    EFeatureContrib start_feature_processing( const float32_t f_feature_f32,
                                              const int32_t   f_labelIdx_i32,
                                              const nodeIdx_t f_nodeIdx, 
                                              const int32_t   f_weight_i32 )
    {
        return process_next_feature_sample( f_feature_f32, f_labelIdx_i32, f_nodeIdx, f_weight_i32 );
    }

    EFeatureContrib process_next_feature_sample( const float32_t f_feature_f32,
                                                 const int32_t   f_labelIdx_i32,
                                                 const nodeIdx_t f_nodeIdx, 
                                                 const int32_t   f_weight_i32 );

    inline
    EFeatureContrib finish_feature_processing() { return EFeatureContrib::NO_IMPROVEMENT; }

    //-----------------------------------------------------------------------------------

    void getOptimalFeatureSplits( int32_t*&    featureIdxIter,
                                  float32_t*&  splitValsIter,
                                  rating_t*&   featureRatingIter ) const;
    
    //-----------------------------------------------------------------------------------

    using base_t::process_sorted_feature;
    using base_t::process_unsorted_feature;

private:

    using base_t::leftStat;
    using base_t::rightStat;
    using base_t::featureIdx;
};


//-------------------------------------------------------------------------------------------

// Announcing explicit instantiation in cpp

extern template struct CBasePlaneParamOptimizer< CTreeSplitEntropyStatistics, split_entropy_optimizer_t >;
extern template struct CBasePlaneParamOptimizer< CTreeSplitGiniStatistics, split_gini_optimizer_t >;

extern template struct CBaseSplitOptimizer< CTreeSplitEntropyStatistics, split_entropy_optimizer_t >;
extern template struct CBaseSplitOptimizer< CTreeSplitGiniStatistics, split_gini_optimizer_t >;

extern template struct CFeatureSplitOptimizer< CTreeSplitEntropyStatistics >;
extern template struct CFeatureSplitOptimizer< CTreeSplitGiniStatistics >;

//-------------------------------------------------------------------------------------------


} // namespace tree
