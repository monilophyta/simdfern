/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "tree_base_types.hpp"
#include "tree_debug.hpp"

#include "tree_event_counting.hpp"
#include "tree_event_counting.inl"

#include "tree_math.hpp"
#include "tree_math.inl"

#include "tree_cfg.hpp"

#include "tree_node_index.hpp"
#include "tree_node_index.inl"


#include "tree_enums.hpp"


#include "tree_box_feature_evaluation_base.hpp"
#include "tree_box_feature_evaluation_base.inl"
#include "tree_box_feature_evaluation.hpp"

#include "tree_split_statistics_entropy.hpp"
#include "tree_split_statistics_gini.hpp"

#include "tree_split_optimization.hpp"
#include "fern_split_optimization.hpp"

#include "tree_golden_section_search.hpp"
#include "tree_golden_section_search.inl"

#include "fern_weight_regularisation.hpp"
#include "fern_plane_param_line_search.hpp"

#include "fern_lda_statistic.hpp"
#include "fern_sample_projection.hpp"
#include "fern_binary_conversion.hpp"

#include "tree_classification.hpp"

