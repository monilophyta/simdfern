/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <simd.hpp>
#include <cpp_tools.hpp>

#include "tree_split_statistics_entropy.hpp"
#include "tree_split_statistics_gini.hpp"



namespace tree
{


enum class EPlaneSide : int32_t
{
    LEFT  = -1,
    RIGHT =  1
};



template <typename split_stat_T>
class CMultiClassFern2BinaryConverter
{

    typedef split_stat_T   split_stat_t;

private:

    /// Statistics of all LEFT tree branches
    split_stat_t m_leftStat;

    /// Statistics of all RIGHT tree branches 
    split_stat_t m_rightStat;


    /// Mask of considered layer bit in optimization
    int32_t m_layerBitMask_i32;

    /// Mask of all bits below layer bit in optimization
    int32_t m_lowerBitMask_i32;

    /// Mask of all bits above layer bit in optimization
    int32_t m_upperBitMask_i32;

public:

    //-----------------------------------------------------------------------------------

    CMultiClassFern2BinaryConverter( int32_t f_numLabels_i32, int32_t f_numLeafs_i32, uint8_t f_layerBitIdx_u8 );


    const int32_t*
    initialize_statistics( const int32_t* leafIter, const int32_t* const leafIterEnd,
                           const int32_t* labelIter,
                           const int32_t* weightIter );


    typedef decltype( ((split_stat_t*)nullptr)->node_rating( int32_t() ) )   rating_t;

    struct binary_mapping_t
    {
        EPlaneSide  m_planeSide;
        rating_t    m_weightRating;
    };

    binary_mapping_t map_sample( int32_t f_leafIdx_i32, int32_t f_labelIdx_i32, int32_t f_weight_i32 ) const;


private:

    inline int32_t exclude_layer_idx_from( int32_t f_leafIdx_i32 ) const;
    inline bool is_left( int32_t f_leafIdx_i32 ) const;
    inline bool is_right( int32_t f_leafIdx_i32 ) const { return (!is_left( f_leafIdx_i32 )); }

};

//-------------------------------------------------------------------------------------------

// Announcing explicit instantiation in cpp

extern template class CMultiClassFern2BinaryConverter< CTreeSplitEntropyStatistics >;
extern template class CMultiClassFern2BinaryConverter< CTreeSplitGiniStatistics >;


typedef CMultiClassFern2BinaryConverter< CTreeSplitEntropyStatistics >   multicl_to_binary_entropy_conv_t;
typedef CMultiClassFern2BinaryConverter< CTreeSplitGiniStatistics >      multicl_to_binary_gini_conv_t;

//-------------------------------------------------------------------------------------------


} // namespace tree

