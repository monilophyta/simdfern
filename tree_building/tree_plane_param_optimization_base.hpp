/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <simd.hpp>
#include <cpp_tools.hpp>

#include "tree_node_index.hpp"


namespace tree
{


enum class EFeatureContrib : int32_t
{
    NO_IMPROVEMENT = 0,
    IMPROVEMENT = 1
};


//-------------------------------------------------------------------------------------------


template <typename split_stat_T, typename derived_T>
struct CBasePlaneParamOptimizer
{   
    typedef split_stat_T                                                      split_stat_t;
    typedef decltype( ((split_stat_t*)nullptr)->node_rating( int32_t() ) )        rating_t;

private:

    typedef derived_T                                                            derived_t;

    /// Statistics of all LEFT tree branches
    split_stat_t m_leftStat;


    /// Statistics of all RIGHT tree branches 
    split_stat_t m_rightStat;


public:

    //-----------------------------------------------------------------------------------

    CBasePlaneParamOptimizer( int32_t f_numLabels_i32, int32_t f_numNodes_i32 );


    //-----------------------------------------------------------------------------------

    int32_t process_sorted_feature( const float32_t*  featureIter,
                                    const float32_t*  featureIterEnd,
                                    const int32_t*    labelIdxIter,
                                    const nodeIdx_t*  nodeIdxIter,
                                    const int32_t*    weightIter );

    int32_t process_unsorted_feature( const float32_t* const f_featureArray,
                                      const float32_t* const f_featureEnd,
                                      const int32_t*   const f_labelIdxArray,
                                      const nodeIdx_t* const f_nodeIdxArray,
                                      const int32_t*   const f_weightArray );


protected:

    inline split_stat_t& leftStat() { return m_leftStat; }
    inline split_stat_t& rightStat() { return m_rightStat; }

    //-----------------------------------------------------------------------------------

    inline derived_t& derived() { return static_cast<derived_t&>(*this); }
    inline const derived_t& derived() const { return static_cast<const derived_t&>(*this); }

private:

    //-----------------------------------------------------------------------------------

    template <typename FITER, typename LITER, typename NITER, typename WITER>
    inline FITER process_invalid_feature_range(       FITER  featureIter,
                                                const FITER  featureIterEnd,
                                                      LITER  labelIdxIter,
                                                      NITER  nodeIdxIter,
                                                      WITER  weightIter );
    

    template <typename SITER>
    inline SITER process_invalid_feature_range(       SITER   idxIter,
                                                const SITER   idxIterEnd,
                                                const float32_t* const featureArray,
                                                const int32_t*   const labelIdxArray,
                                                const nodeIdx_t* const nodeIdxArray,
                                                const int32_t*   const weightArray );
};


// ---------------------------------------------------------------------------------------------
// ----------------- Common code factorized out ------------------------------------------------
// ---------------------------------------------------------------------------------------------

int32_t*
order_unsorted_features( std::unique_ptr<int32_t[]>& f_srtIdx_ptr,
                         const float32_t* const f_featureArray,
                         const float32_t* const f_featureEnd );

} // namespace tree

