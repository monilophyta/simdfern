/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>
#include <limits>
#include <type_traits>
#include <iterator>
#include <simd.hpp>
#include <image.hpp>
#include "tree_base_types.hpp"



namespace tree
{

template <int MEM_SIZE>
struct dummy_param_t
{
    inline dummy_param_t() {}
    
    template <typename AUX_T>
    inline dummy_param_t( AUX_T ) {}

    char dumm_array[MEM_SIZE];
};

static_assert( 1 >= sizeof( dummy_param_t<0> ) );


//-------------------------------------------------------------------------------------------------------------


template <typename IMG_T, typename FEATURE_FUNC, typename PARAM_T>
struct CBoxEvalParamTypes
{
    typedef IMG_T                                                          img_t;
    typedef FEATURE_FUNC                                          feature_func_t;

    typedef PARAM_T                                       feature_param_constr_t;
    typedef typename std::remove_const<
            typename std::remove_reference<PARAM_T>::type>::type feature_param_t;
};


//-------------------------------------------------------------------------------------------------------------

template< typename test_T, typename value_T >
using is_correct_iter = 
    std::is_same< typename std::iterator_traits< test_T >::value_type,
                  value_T >;



template <typename BASE_T, typename DERIVED_T>
class CBoxFeatureEvaluatorBase : public BASE_T
{
private:

    typedef CBoxFeatureEvaluatorBase<DERIVED_T,BASE_T> this_t;
    typedef BASE_T                                     base_t;

public:

    using typename base_t::img_t;
    using typename base_t::feature_param_t;
    using typename base_t::feature_param_constr_t;
    using typename base_t::feature_func_t;
    using typename base_t::intbox_t;
    using typename base_t::feature_extent_t;
    using typename base_t::feature_t;


    struct CBoxPair
    {
        intbox_t           first;
        intbox_t           second;
        feature_extent_t   extent;
        feature_param_t    param;

        inline
        CBoxPair( const intbox_t&         f_first,
                  const intbox_t&         f_second,
                  const feature_extent_t& f_extent,
                  feature_param_constr_t  f_param )
            : first( f_first )
            , second( f_second )
            , extent( f_extent )
            , param( f_param )
        {}
    };

    typedef CBoxPair                                                       boxpair_t;
    typedef smd::aligned_allocator<boxpair_t,sizeof(feature_t)>  boxpair_allocator_t;
    typedef std::vector<boxpair_t,boxpair_allocator_t>              boxpair_vector_t;

protected:

    img::image_frame_t                 m_imageFrame;
    img::CImagePointer<const img_t>    m_imagePointer;
    boxpair_vector_t                   m_boxPairs;
    img::feature_extent_i32_t          m_allOverExtent;

public:

    inline CBoxFeatureEvaluatorBase()
        : m_imageFrame()
        , m_imagePointer()
        , m_boxPairs()
        , m_allOverExtent()
    {}

    CBoxFeatureEvaluatorBase( const img::image_frame_t&              f_imageFrame,
                              const img::CImagePointer<const img_t>& f_imagePointer,
                              size_t                                 f_numFeatures );

    //--------------------------------------------------------------------------------------------------------------------------
    // Main Interface

    /** Add box pair to list of features (box pairs)
     */
    template <typename BT> 
    inline
    void emplace_boxpair( const img::CBox<BT>& f_first, const img::CBox<BT>& f_second,
                          feature_param_constr_t f_param = feature_param_t( std::numeric_limits<float32_t>::signaling_NaN() ) );
    

    template <typename iter_T>
    inline iter_T
    evaluate_location( const img::image_coord_i32_t& f_ankerIdx, iter_T f_storeIter ) const;

    template <typename iter_T>
    inline iter_T
    evaluate_locations( const img::image_coord_i32_t* f_begin, const img::image_coord_i32_t* f_end,
                        iter_T f_storeIter ) const;

    
    template <typename iter_T>
    inline iter_T
    evaluate_borderless_location(    const img::image_coord_i32_t& f_ankerIdx, iter_T f_storeIter ) const;
    
    template <typename iter_T>
    inline iter_T
    evaluate_borderchecked_location( const img::image_coord_i32_t& f_ankerIdx, iter_T f_storeIter ) const;

    //--------------------------------------------------------------------------------------------------------------------------
    // Main Implementation
    feature_t evaluate_borderless_boxpair( const boxpair_t& f_boxPair, int32_t f_ankerOffset ) const;
    
    inline
    feature_t evaluate_borderchecked_boxpair( const img::image_coord_i32_t&, 
                                              const              boxpair_t&,
                                                                   int32_t ) const
    {
        throw std::runtime_error( "Unimplemented (non-overloaded) evaluate_borderchecked_boxpair" );
    }
    
    inline
    feature_t evaluate_borderchecked_boxpair( const img::image_coord_i32_t& f_ankerIdx, const boxpair_t& f_boxPair ) const
    {
        return derived().evaluate_borderchecked_boxpair( f_ankerIdx, f_boxPair, m_imageFrame.anker_offset( f_ankerIdx ) );
    }

private:

    inline DERIVED_T& derived() { return static_cast<DERIVED_T&>(*this); }
    inline const DERIVED_T& derived() const { return static_cast<const DERIVED_T&>(*this); }
};




} // namespace tree

