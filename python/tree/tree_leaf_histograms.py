# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = [ "create_leaf_histograms",
            "BaseLeafHistograms", "FullLeafHistograms", "PrunedLeafHistograms", "SparsePrunedLeafHistograms"]

import typing as tp
from abc import ABC, abstractmethod
import numpy as np

from ..clmath import Crosstab

# -------------------------------------------------------------------------------------------------------

def create_leaf_histograms( leaf_idx : np.ndarray,
                           label_idx : np.ndarray, 
                        data_weights : tp.Optional[np.ndarray] = None, 
                          num_layers : tp.Optional[int] = None,
                          num_labels : tp.Optional[int] = None ):
        """
        Parameters
        ----------
        node_idx : int32 ndarray [ len=N ]
        label_idx : int32 array: 0 <= i < 31, len < 31 [ len=N ]
        data_weights : int32 ndarray [ len=N ] or None
        
        Returns
        -------
        int32 matrix [ num_leafs x num_labels ]
            coocurence matrix of labels and leaf indices
        """
        assert leaf_idx.size == label_idx.size
        assert np.issubdtype( leaf_idx.dtype, np.integer )
        assert np.issubdtype( label_idx.dtype, np.integer )
        assert np.all( label_idx >= 0 )
        assert np.all( leaf_idx >= 0 )
        assert (data_weights is None) or ( data_weights.size == label_idx.size )

        if num_layers is None:
            num_layers = int( np.ceil( np.log2( leaf_idx.max() + 1 ) ) )
        
        if num_labels is None:
            num_labels = label_idx.max() + 1
        
        num_leafs = 2**num_layers

        flat_hist_size = int(num_leafs) * int(num_labels)
        assert flat_hist_size <= np.iinfo(np.int32).max, "exceeding numpy.int32 range"
        
        # flat data indices within histogram
        flat_data_idx = (leaf_idx * num_labels) + label_idx  # [ N ]

        # flat histogram
        flat_histograms = np.bincount( flat_data_idx, weights=data_weights, minlength=flat_hist_size )
        flat_histograms = np.round( flat_histograms ).astype(np.int32)

        # rebuild full hitogramm
        histograms = flat_histograms.reshape( (num_leafs, num_labels ) )      # [ nN x nL ]
        assert (data_weights is None) or (histograms.sum(dtype=np.int64) == data_weights.sum(dtype=np.int64))
        assert (data_weights is None) or ( np.all( [ histograms[:,lidx].sum() == data_weights[label_idx == lidx].sum() for lidx in range(num_labels) ] ) )
        assert (data_weights is not None) or (histograms.sum(dtype=np.int64) == label_idx.size)

        return histograms


# -------------------------------------------------------------------------------------------------------


class BaseLeafHistograms( ABC ):

    @property
    @abstractmethod
    def num_leafs(self):
        return None
    
    @property
    @abstractmethod
    def num_labels(self):
        return None
    
    @property
    def num_layers(self):
        return int( np.ceil( np.log2( self.num_leafs ) ) )

    @abstractmethod
    def __call__( self, idx ):
        return None


    @staticmethod
    def _expected_confusion_matrix( histograms ):
        """
        Parameters
        ----------
        histograms : int32 matrix [ num_leafs x num_labels ]
            coocurence matrix of labels and leaf indices
        
        Returns
        -------
        int32 matrix [ num_labels x num_labels ]
            confusion_matrix, rows represent the true labels,
            columns represent the assigned labels
        """
        num_leafs, num_classes = histograms.shape
        confusion_matrix = np.zeros( (num_classes,num_classes), dtype=histograms.dtype )
        max_label = histograms.argmax( axis=1 )   # [ num_leafs ]

        for leaf_idx, label in enumerate( max_label ):
            confusion_matrix[:,label] += histograms[leaf_idx,:]

        return confusion_matrix


    @staticmethod
    def _expected_precision( histograms ):
        """
        Parameters
        ----------
        histograms : int32 matrix [ num_leafs x num_labels ]
            coocurence matrix of labels and leaf indices
        
        Returns
        -------
        precision: float32 >=0, <=1
        """
        #num_leafs, num_classes = histograms.shape
        
        correct_count = histograms.max( axis=1 ).sum()
        total_count = histograms.sum()

        return (float(correct_count) / float(total_count))


# -------------------------------------------------------------------------------------------------------


class FullLeafHistograms( BaseLeafHistograms ):
    
    histograms = None     # [ num_leafs x num_labels ]
    class_freqs = None    # [ num_labels ]

    num_data = None


    @property
    def num_leafs(self):
        return self.histograms.shape[0]
    
    @property
    def num_labels(self):
        return self.histograms.shape[1]


    def __init__(self, histograms, class_freqs=None ):
        """
        Parameters
        ----------
        histograms : int32 matrix [ num_leafs x num_labels ]
            coocurence matrix of labels and leaf indices
        """
        assert np.issubdtype( histograms.dtype, np.integer )
        self.histograms = histograms

        if class_freqs is None:
            class_freqs = histograms.sum( axis=0 )
        assert np.issubdtype( class_freqs.dtype, np.integer )
        self.class_freqs = class_freqs        # [ num_labels ]

        self.num_data = int(class_freqs.sum())


    @classmethod
    def build_from_indices( cls, leaf_idx : np.ndarray,
                                label_idx : np.ndarray, 
                             data_weights : tp.Optional[np.ndarray] = None, 
                               num_layers : tp.Optional[int] = None,
                               num_labels : tp.Optional[int] = None ):
        """
        Parameters
        ----------
        leaf_idx : int32 ndarray [ len=N ]
        label_idx : int32 array: 0 <= i < 31, len < 31 [ len=N ]
        data_weights : int32 ndarray [ len=N ] or None
        
        Returns
        -------
        int32 matrix [ num_leafs x num_labels ]
            coocurence matrix of labels and leaf indices
        """

        histograms = create_leaf_histograms( leaf_idx, label_idx, data_weights, num_layers, num_labels )
        return cls(histograms)


    def __call__( self, idx ):
        """
        Parameters
        ----------
        idx : int32 array [ N ]
            leaf indices
        
        Returns
        -------
        label histograms for leafs
        """
        hists = self.histograms.take(idx, axis=0)
        return hists
    

    def majority_mapping(self, mark_unknown : bool = True ):
        """
        Parameters
        ----------
        mark_unknown : bool 
            marke unknown class (all class bin are equal) with -1 
        
        Returns
        -------
        majority vote for every histogram bin
        """
        majority_map = self.histograms.argmax(axis=1)  # [ num_leafs ]

        if bool(mark_unknown):
            min_val = self.histograms.min(axis=1)  # [ num_leafs ]
            unknown_mask = min_val == self.histograms[np.arange(self.num_leafs),majority_map]
            assert np.all( unknown_mask == (min_val == self.histograms.max(axis=1)) )
            majority_map[unknown_mask] = -1

        return majority_map


    def expected_confusion_matrix(self):
        """
        Returns
        -------
        int32 matrix [ num_labels x num_labels ]
            confusion_matrix, rows represent the true labels,
            columns represent the assigned labels
        """
        return self._expected_confusion_matrix( self.histograms )


    def expected_precision(self):
        """
        Returns
        -------
        precision: float32 >=0, <=1
        """
        return self._expected_precision( self.histograms )

    # ---------------------------------------------------------------------------------------------------
    # --- Pruning ---------------------------------------------------------------------------------------
    # ---------------------------------------------------------------------------------------------------

    def prune( self, same_label_pruning : bool = True,
                     reduced_significance_pruning : bool = False,
                     preserve_empty_leafs : bool = True ):
        
        # get thinned out histogram list
        leaf_map = self.calc_thinnedout_leaf_map( same_label_pruning = same_label_pruning,
                                                  reduced_significance_pruning = reduced_significance_pruning )
        assert leaf_map.size == self.num_leafs
        assert np.all( leaf_map >= 0 )
        assert np.all( leaf_map < self.num_leafs )
        assert np.all( leaf_map <= np.arange( self.num_leafs, dtype=leaf_map.dtype ) )

        pruned_leaf_indices, pruned_leaf_sizes = np.unique( leaf_map, return_counts=True )
        if preserve_empty_leafs:
            empty_leaf_mask = np.any( self.histograms > 0, axis=1 )
            preserve_empty_leafs = preserve_empty_leafs and np.any( empty_leaf_mask )
        
        if pruned_leaf_indices.size == self.num_leafs:
            # No sparse possible
            return self
        
        num_pruned_leafs = pruned_leaf_indices.size

        if preserve_empty_leafs:
            num_pruned_leafs += 1

        # create pruned histograms
        pruned_histograms = np.empty( (num_pruned_leafs, self.num_labels), dtype=self.histograms.dtype )

        if __debug__:
            if np.issubdtype( pruned_histograms.dtype, np.integer ):
                pruned_histograms.fill( np.iinfo( pruned_histograms.dtype ).max )
            else:
                pruned_histograms.fill( np.nan )

        for pidx, (lidx, lcount) in enumerate( zip( pruned_leaf_indices, pruned_leaf_sizes ) ):
            
            leaf_range = slice( lidx, lidx+lcount )
            assert np.all( leaf_map[leaf_range] == lidx )

            np.add.reduce( self.histograms[leaf_range,:], axis=0, out=pruned_histograms[pidx,:] )
        
        # create new leaf_map
        n_leaf_hist_map = np.arange( pruned_leaf_indices.size, dtype=np.int32 ).repeat( pruned_leaf_sizes )

        if preserve_empty_leafs:
            # one extra histogram for empty leafs
            pruned_histograms[-1,:] = 0
            n_leaf_hist_map[empty_leaf_mask] = pruned_histograms.shape[0] - 1

        # check that total number of counts is still correct
        assert self.histograms.sum(dtype=np.int64) == pruned_histograms.sum(dtype=np.int64)

        return PrunedLeafHistograms( n_leaf_hist_map, pruned_histograms )



    def calc_thinnedout_leaf_map(self, same_label_pruning : bool, reduced_significance_pruning : bool ):
        """
        Thinns out leafs nodes using histogram error probabiliy
        
        Returns
        -------
        leaf_map : int32 array
            with  leaf_map[n] beeing the new leaf index
            of former leaf n
        """
        # starting with every leaf pointing to its own histogram
        leaf_map = np.arange( self.num_leafs, dtype=np.int32 )

        if not (same_label_pruning or reduced_significance_pruning):
            assert False, "Invalid Parameters"
            return leaf_map

        is_leaf = np.ones( leaf_map.shape, dtype=np.bool )
        
        logprob = lambda p: np.log( np.maximum( p, np.finfo(p.dtype).tiny ) )

        # process bottom layer first
        hists = self.histograms                 # [ num_leafs x num_labels ]

        # size of lowest layer if tree
        children_sizes = hists.sum( axis=1 )
        
        if reduced_significance_pruning:
            children_err_prob = self.histogram_error_probability( hists )
        
        # majority label on leaf layer
        if same_label_pruning:
            children_empty = children_sizes == 0
            children_majority_label = hists.argmax( axis=1 )
        

        for lIdx in range( 1, self.num_layers ):
            
            is_to_prune = False

            # calculate parent histograms
            hists = hists[::2,:] + hists[1::2,:]

            ## check for pruning candidates
            # is majority label significance improved
            if reduced_significance_pruning:
                assert is_leaf.size == children_err_prob.size

                # logarithmic error likelihood of layer lIdx
                parent_err_prob = self.histogram_error_probability( hists )  # the smaller the better
                
                # size of current layer size
                layer_node_sizes = hists.sum( axis=1 )

                # weight of left and right branches
                weight_norm = np.reciprocal( np.maximum( layer_node_sizes, np.finfo(np.float32).tiny, dtype=np.float32 ) )
                left_weight  = weight_norm * children_sizes[::2].astype(np.float32)
                right_weight = weight_norm * children_sizes[1::2].astype(np.float32)

                # log multiplied parent error likelihood - weighted average
                sum_children_err_prob = (  left_weight * children_err_prob[::2] ) + \
                                        ( right_weight * children_err_prob[1::2] )
                
                is_to_prune = sum_children_err_prob >= parent_err_prob

                # preparation for processing of next higher layer
                children_err_prob = parent_err_prob   # we are climbing upwards => parents become children
                children_sizes    = layer_node_sizes

            # majority label on leaf layer
            if same_label_pruning:
                layer_majority_label = hists.argmax( axis=1 )
                same_label = np.logical_and( layer_majority_label == children_majority_label[::2],
                                             layer_majority_label == children_majority_label[1::2] )
                same_label = np.logical_or( same_label, children_empty[::2], out=same_label )
                same_label = np.logical_or( same_label, children_empty[1::2], out=same_label )
                
                is_to_prune = np.logical_or( is_to_prune, same_label )
                
                # preparation for processing of next higher layer
                children_majority_label = layer_majority_label
                children_empty          = np.all( hists == 0, axis=1 )
            
            # node must be a leaf (is_leaf & error probability decreases for parent )
            is_leaf = np.logical_and( is_leaf[::2], is_leaf[1::2] )

            # pruning idx
            is_to_prune = np.logical_and( is_leaf, is_to_prune )
            prune_indices = np.flatnonzero( is_to_prune )

            # prune subtrees by mapping them to one histogram index
            for pIdx in prune_indices:
                # subtree is always identified by the index of its most left (smallest) leaf (index)
                leaf_idx_start = pIdx << lIdx
                assert leaf_map[leaf_idx_start] == leaf_idx_start   

                leaf_idx_slice = slice( leaf_idx_start, pIdx+1 << lIdx )
                leaf_map[leaf_idx_slice] = leaf_idx_start
            
            # every pruned subtree becomes a leaf
            is_leaf = is_to_prune

            # check if further pruning is not possible anymore
            if prune_indices.size < 2:
                assert is_leaf.sum() < 2
                break

        return leaf_map


    def histogram_error_probability( self, hists ):
        """
        Parameters
        ----------
        histograms : int32 matrix [ num_nodes x num_labels ]
            coocurence matrix of labels and leaf indices
        
        Returns
        -------
        right side cumulative distribution function
        the smaller this value, the better
        """

        cr = Crosstab.from_histograms( self.class_freqs, hists, num_data=self.num_data )

        return cr.hypergeom_type_one_error_prob()
    

# -------------------------------------------------------------------------------------------------------


class PrunedLeafHistograms( BaseLeafHistograms ):

    leaf_to_hist_map = None  # [ num_leafs ]
    histograms = None        # [ num_pruned_leafs x num_labels ]

    @property
    def num_leafs(self):
        return self.leaf_to_hist_map.size

    @property
    def num_pruned_leafs(self):
        return self.histograms.shape[0]

    @property
    def num_labels(self):
        return self.histograms.shape[1]
    

    def __init__(self, leaf_to_hist_map, histograms ):
        assert histograms.shape[0] <= leaf_to_hist_map.size
        #assert np.all( np.arange(leaf_to_hist_map.size, dtype=np.int32) >= leaf_to_hist_map )

        self.leaf_to_hist_map = leaf_to_hist_map
        self.histograms = histograms


    def __call__( self, idx ):
        """
        Parameters
        ----------
        idx : int32 matrix [ N ]
            leaf indices
        
        Returns
        -------
        label histograms for leafs
        """
        hist_idx = self.map_node_idx( idx )
        hists = self.histograms.take( hist_idx, axis=0 )    
        return hists


    def majority_mapping(self, mark_unknown : bool = True ):
        """
        Parameters
        ----------
        mark_unknown : bool
            marke unknown class (all class bin are equal) with -1 
        
        Returns
        -------
        majority vote for every histogram bin
        """
        majority_map = self.histograms.argmax(axis=1)  # [ num_pruned_leafs ]

        if bool(mark_unknown):
            min_val = self.histograms.min(axis=1)  # [ num_pruned_leafs ]
            unknown_mask = min_val == self.histograms[np.arange(self.num_pruned_leafs),majority_map]
            assert np.all( unknown_mask == (min_val == self.histograms.max(axis=1)) )
            majority_map[unknown_mask] = -1

        return majority_map[self.leaf_to_hist_map]  # [] num_leafs ]



    def map_node_idx( self, idx ):
        """
        Parameters
        ----------
        idx : int32 matrix [ N ]
            leaf indices
        
        Returns
        -------
        histogram index
        """
        hist_idx = self.leaf_to_hist_map[idx]
        return hist_idx



    def expected_confusion_matrix(self):
        """
        Returns
        -------
        int32 matrix [ num_labels x num_labels ]
            confusion_matrix, rows represent the true labels,
            columns represent the assigned labels
        """
        return self._expected_confusion_matrix( self.histograms )


    def expected_precision(self):
        """
        Returns
        -------
        precision: float32 >=0, <=1
        """
        return self._expected_precision( self.histograms )

# -------------------------------------------------------------------------------------------------------


class SparsePrunedLeafHistograms( BaseLeafHistograms ):
    
    label_idx    = None   # [ num_pruned_leafs ]
    label_counts = None   # [ num_pruned_leafs ]
    rest_counts = None    # [ num_pruned_leafs ]
    
    leaf_to_hist_map = None  # [ num_leafs ]

    num_labels = None    # [ None ]


    @property
    def num_leafs(self):
        return self.leaf_to_hist_map.size

    @property
    def num_pruned_leafs(self):
        return self.label_counts.shape[0]

    @property
    def num_labels(self):
        return self.label_idx.max() + 1


    def __init__(self, label_idx, label_counts, rest_counts ):
        pass
   

    def __call__( self, idx ):
        """
        Parameters
        ----------
        idx : int32 matrix [ N ]
            leaf indices
        
        Returns
        -------
        label_idx: majority label index
        label_counts: histogram strength of majority label
        rest_counts: accumulated strength of remaining classes
        """
        pruned_idx  = self.leaf_to_hist_map[idx]

        label_counts = self.label_counts[pruned_idx]
        label_idx   = self.label_idx[pruned_idx]
        rest_counts = self.rest_counts[pruned_idx]
        
        return (label_idx, label_counts, rest_counts)


    def majority_mapping(self, mark_unknown : bool = True ):
        """
        Parameters
        ----------
        mark_unknown : bool
            marke unknown class (all class bin are equal) with -1 
        
        Returns
        -------
        majority vote for every histogram bin
        """

        if bool(mark_unknown):
            unkown_mask = ((self.num_labels-1) * self.label_counts) <= self.rest_counts   # [ num_pruned_leafs ]
            label_idx = np.where( unkown_mask, self.label_idx.dtype(-1), self.label_idx ) # [ num_pruned_leafs ]
        else:
            label_idx = self.label_idx
        
        majority_map = label_idx[self.leaf_to_hist_map]   # [ num_leafs ]
        return majority_map
    

    def expected_precision(self):
        """
        Returns
        -------
        precision: float32 >=0, <=1
        """
        label_counts = self.label_counts.sum()
        total_counts = self.rest_counts.sum() + label_counts
        return (float(label_counts) / float(total_counts))
