# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


# Helpfull links:
# https://en.wikipedia.org/wiki/Newick_format


__all__ = ["TreeAnalyser"]


import math as m
import numpy as np

from ..clmath import Crosstab
from .tree_navigation import AbstractTreeNavigator




class TreeAnalyser( AbstractTreeNavigator ):
    """
    Attributes
    ----------
    node_supports : integer array [ N ]
        number of data samples passing this node
    node_weights : integer array [ N ]
        weight sum of data samples passing this node
    node_support_hists : integer matrix [ N x L ]
        histogram of sample class specific sample counts passing this node
    node_weight_hists : integer matrix [ N x L ]
        histogram of sample class specific weight sums passing this node
    """

    node_supports = None  # int32 array [ N ]
    node_weights = None   # int32 array [ N ]
    
    node_support_hists = None  # int32 array [ N x L ]
    node_weight_hists = None   # int32 array [ N x L ]


    @property
    def num_nodes(self):
        return self.node_supports.size


    @property
    def num_labels(self):
        return self.node_support_hists.shape[1]


    def __init__(self, nodeIdx, labels, data_weights=None ):
        """
        Parameters
        ----------
        nodeIdx : int32 array [ N ]
            Node index of sample n,
            >=0 for active nodes, <0 for finalized nodes
        labels : int32 array [ N ]
        data_weights : int32 array [ N ] or None
            in case of None weight 1 for each sample is assumed
        """
        assert isinstance( nodeIdx, np.ndarray )
        assert np.issubdtype( nodeIdx.dtype, np.integer )

        abs_nodeIdx = np.absolute( nodeIdx )

        # determ number of used bits
        num_leafs = abs_nodeIdx.max() + 1
        num_bits = int( m.ceil( np.log2( float(num_leafs) ) ) )
        assert num_leafs <= 2**num_bits
        num_nodes = 2**(num_bits+1) - 1

        # initialize object members
        self.node_supports = np.zeros( num_nodes, dtype=np.int32 )

        # determ number of labels
        num_labels = labels.max() + 1

        # initialize node histogram
        self.node_support_hists = np.zeros( (num_nodes,num_labels), dtype=np.int32 )

        if data_weights is None:
            self.node_weight_hists = None
            self.node_weights = None
            data_weights = DummyWeights()
        else:
            self.node_weight_hists = np.zeros( (num_nodes,num_labels), dtype=np.int32 )
            self.node_weights = np.zeros( num_nodes, dtype=np.int32 )

        # build subtree
        self.build_subtree( 0, num_bits-1, abs_nodeIdx, labels, data_weights )

        if self.node_weight_hists is None:
            self.node_weight_hists = self.node_support_hists
            self.node_weights = self.node_supports

        # For debugging
        if __debug__:

            node_idx = np.arange( self.num_nodes, dtype=np.int32 )
            
            parent_idx  = self.parent_idx( node_idx )
            parent_mask = parent_idx >= 0
            
            left_children_idx  = self.left_child_idx( nodeIdx )
            right_children_idx = self.right_child_idx( nodeIdx )
            left_mask  = left_children_idx < self.num_nodes
            right_mask = right_children_idx < self.num_nodes

            assert np.all( self.node_supports[parent_idx[parent_mask]] >= self.node_supports[node_idx[parent_mask]] )
            assert np.all( self.node_weights[parent_idx[parent_mask]] >= self.node_weights[node_idx[parent_mask]] )
            
            assert np.all( self.node_support_hists[parent_idx[parent_mask],:] >= self.node_support_hists[node_idx[parent_mask],:] )
            assert np.all( self.node_weight_hists[parent_idx[parent_mask],:] >= self.node_weight_hists[node_idx[parent_mask],:] )   


    def build_subtree( self, array_idx, bit_idx, abs_nodeIdx, labels, data_weights=None ):
        """
        Parameters
        ----------
        array_idx : int < labels.max() + 1
        bit_idx : int >= 0
        abs_nodeIdx : int32 array [ nN ]
        labels : int32 array [ nN ]
        data_weights : int32 array [ nN ] or None
        """

        if data_weights is None:
            data_weights = DummyWeights()

        # size of current subtree
        subtree_size = abs_nodeIdx.size

        # Stop recursion if
        if subtree_size == 0:
            return ( int(0), int(0), int(0) )
            
        # set subtree size
        self.node_supports[ array_idx ] = subtree_size

        use_data_weights = not isinstance( data_weights, DummyWeights )


        if bit_idx < 0:

            subtree_support_hist = self.node_support_hists[array_idx,:] = \
                    np.bincount( labels, minlength=self.num_labels ).astype( labels.dtype )
            assert( np.issubdtype( subtree_support_hist.dtype, np.integer ))
            
            if use_data_weights:
                subtree_weight_hist = self.node_weight_hists[array_idx,:] = \
                        np.bincount( labels, data_weights, minlength=self.num_labels ).astype( data_weights.dtype )
                assert np.issubdtype( subtree_weight_hist.dtype, np.integer )
                
                self.node_weights[ array_idx ] = subtree_weight = subtree_weight_hist.sum()
            else:
                subtree_weight_hist = subtree_weight = None


        else:
            assert bit_idx >= 0

            # bit mask lower sub_trees
            lower_bit_mask = (1 << bit_idx) - 1

            # indices of child nodes
            left_array_idx = (2*array_idx) + 1
            right_array_idx = left_array_idx + 1

            # build left sub tree
            left_mask = abs_nodeIdx < (1 << bit_idx)
            left_weight, left_support_hist, left_weight_hist = \
                self.build_subtree( left_array_idx, bit_idx-1, \
                                    np.bitwise_and( lower_bit_mask, abs_nodeIdx.compress( left_mask ) ), \
                                    labels.compress( left_mask ), \
                                    data_weights.compress( left_mask ) )
            
            # build right sub tree
            right_mask = np.logical_not( left_mask, out=left_mask )  # reuse memory
            right_weight, right_support_hist, right_weight_hist = \
                self.build_subtree( right_array_idx, bit_idx-1, \
                                    np.bitwise_and( lower_bit_mask, abs_nodeIdx.compress( right_mask ) ), \
                                    labels.compress( right_mask ), \
                                    data_weights.compress( right_mask ) )

            if use_data_weights:
                # set subtree weight
                subtree_weight = left_weight + right_weight
                self.node_weights[ array_idx ] = subtree_weight

                subtree_weight_hist  = np.add( left_weight_hist, right_weight_hist, out=self.node_weight_hists[array_idx,:] )
            else:
                subtree_weight = None
                subtree_weight_hist = None

            # set subtree histogramms
            subtree_support_hist = np.add( left_support_hist, right_support_hist, out=self.node_support_hists[array_idx,:] )

        
        return ( subtree_weight, subtree_support_hist, subtree_weight_hist )

    #-----------------------------------------------------------------------------------------------------------------------


    def histogram_error_probability( self, node_idx=None, use_sample_counts=False ):
        """
        Parameters
        ----------
        node_idx : None or int32 array
            indices of selected nodes for calculation of null hypothesis likelihood
        use_sample_counts : bool
            use node support histograms instead of node weight histograms

        Returns
        -------
        right side cumulative distribution function for each (selected) node
            the smaller this value, the better
        """

        if use_sample_counts is True:
            histograms = self.node_support_hists
        else:
            histograms = self.node_weight_hists
        
        # frequency of class members over all nodes (equals root histogram)
        class_freqs = histograms[self.root_idx,:]
        num_data = class_freqs.sum()
        
        if node_idx is not None:
            # select specific nodes
            histograms = histograms[node_idx,:]   # [ M x L ]
        
        cr = Crosstab.from_histograms( class_freqs, histograms, num_data=num_data )
        return cr.hypergeom_type_one_error_prob()

    #-----------------------------------------------------------------------------------------------------------------------

    
    def collapse_unbranched_paths( self ):
        
        # indices of all nodes except the root
        node_idx = np.arange( 1, self.num_nodes, dtype=np.int32)

        # parent indices of all node indices
        parent_idx = self.parent_idx( node_idx )
        assert np.all( parent_idx >= 0 )

        # count number of children for all parents
        unique_parent_idx = np.unique( parent_idx )

        # indices of children of considered parents
        left_children_idx  = self.left_child_idx( unique_parent_idx )
        right_children_idx = self.right_child_idx( unique_parent_idx )

        # indices of valid points
        is_valid = self.node_supports > 0

        # numner of nodes till depth
        num_nodes = (2**(self.tree_depth+1)) - 1

        # number nodes till parent depth
        num_inner_nodes = ( 2**self.tree_depth ) - 1

        while True:

            num_children = np.zeros( num_nodes, dtype=np.int8 )
            num_children[ unique_parent_idx ] += is_valid[ left_children_idx ]
            num_children[ unique_parent_idx ] += is_valid[ right_children_idx ]

            
            remove_mask = np.logical_and( num_children[ node_idx ] == 0, 
                                          num_children[ parent_idx ] == 1 )
                                        
            remove_idx = node_idx[remove_mask]
            assert np.all( remove_idx > 0 )

            # stop loop condition: no changes to is_valid
            if not np.any( is_valid[remove_idx] ):
                break

            is_valid[ remove_idx ] = False
            assert is_valid[0]

            valid_node_idx = np.flatnonzero( is_valid )

            if __debug__:
                vset = frozenset( list(valid_node_idx) )
                valid_parents = self.parent_idx( valid_node_idx )
                # if a child is valid, its parent should also be valid
                check = np.array( [ (v<0) or ( v in vset) for v in valid_parents ] )
                assert np.all( check )


        return valid_node_idx


########################################################################

class DummyWeights(object):

    def compress(self, *args, **argv):
        return self


###############################################################################################


if __name__ == '__main__':
    pass



