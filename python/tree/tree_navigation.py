# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


# Helpfull links:
# https://www.geeksforgeeks.org/array-representation-of-binary-heap/


__all__ = ["AbstractTreeNavigator", "TreeNavigator"]


from abc import ABC, abstractmethod
import math as m
import numpy as np



class AbstractTreeNavigator( ABC ):

    @property
    @abstractmethod
    def num_nodes(self):
        pass
    

    @property
    def node_indices(self):
        return np.arange( self.num_nodes, dtype=np.int32 )

    @property
    def num_leafs(self):
        return ((self.num_nodes + 1) >> 1)
    
    @property
    def num_bits(self):
        return int( m.ceil( np.log2( float( self.num_leafs ) ) ) )

    @property
    def tree_depth(self):
        return int( m.floor( np.log2( self.num_nodes ) ) )
    
    @property
    def root_idx(self):
        return 0

    @property
    def leaf_indices(self):
        lidx = np.arange( self.num_nodes - self.num_leafs, self.num_nodes, dtype=np.int32 )
        assert np.all(self.left_child_idx( lidx ) >= self.num_nodes)
        assert np.all(self.right_child_idx( lidx ) > self.num_nodes)
        return lidx


    @staticmethod
    def parent_idx( node_idx ):
        """
        Parameters
        ----------
        node_idx : int or int32 array

        Returns
        -------
        parent_idx : int or int32 array
            index of parent node
        """
        p_idx = (node_idx - 1) // 2

        return p_idx


    @staticmethod
    def left_child_idx( node_idx ):
        """
        Parameters
        ----------
        node_idx : int or int32 array

        Returns
        -------
        int or int32 array
            index of left child/children
        """
        left_idx = (node_idx*2) + 1
        assert np.all( AbstractTreeNavigator.parent_idx( left_idx ) == node_idx )

        return left_idx


    @staticmethod
    def right_child_idx( node_idx ):
        """
        Parameters
        ----------
        node_idx : int or int32 array

        Returns
        -------
        int or int32 array
            index of right child/children
        """
        right_idx = (node_idx*2) + 2
        assert np.all( AbstractTreeNavigator.parent_idx( right_idx ) == node_idx )
        
        return right_idx
    

    @staticmethod
    def node_depth( node_idx ):
        """
        Parameters
        ----------
        node_idx : int or int32 array

        Returns
        -------
        int or int32 array
            depth of nodes
        """
        return np.floor( np.log2( node_idx + 1, dtype=np.float32 ) ).astype(np.int32)


# ---------------------------------------------------------------------------------------------------

class TreeNavigator( AbstractTreeNavigator ):

    __num_nodes = None

    @property
    def num_nodes(self):
        return self.__num_nodes

    def __init__(self, num_nodes ):
        self.__num_nodes = num_nodes


    @classmethod
    def from_max_leaf_idx( cls, leaf_idx ):
        num_leafs = leaf_idx + 1
        num_bits = int( m.ceil( np.log2( float(num_leafs) ) ) )
        assert num_leafs <= 2**num_bits
        num_nodes = 2**(num_bits+1) - 1

        return cls( num_nodes )

