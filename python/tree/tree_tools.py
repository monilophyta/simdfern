
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["norm_depthwise", "generate_newick_sequence"]


import numpy as np
from .tree_navigation import TreeNavigator


def norm_depthwise( node_idx, weights ):
    
    assert np.issubdtype( node_idx.dtype, np.integer )
    assert node_idx.size == weights.size
    
    depths = TreeNavigator.node_depth( node_idx )
    assert np.all( depths <= TreeNavigator.from_max_leaf_idx( node_idx.max() ).tree_depth )

    weights = weights.astype(np.float32)
    normed_weights = np.zeros_like( weights )
    normed_weights[depths==0] = 1

    for d in range( 1, depths.max()+1 ):
        didx = np.flatnonzero( depths == d )

        scale = weights[didx].sum()
        normed_weights[didx] = weights[didx] * (1. / scale)
    
    return normed_weights



def generate_newick_sequence( num_nodes, array_idx=0, accu_seq=list() ):
    
    if array_idx < num_nodes:
        # indices of child nodes
        left_array_idx = (2*array_idx) + 1
        right_array_idx = left_array_idx + 1

        accu_seq.append( '(' )
        generate_newick_sequence( num_nodes, left_array_idx, accu_seq )
        accu_seq.append( ',' )
        generate_newick_sequence( num_nodes, right_array_idx, accu_seq )
        accu_seq.append( ')%d' % array_idx )
    else:
        accu_seq.append( "%d" % array_idx )
    
    return accu_seq
