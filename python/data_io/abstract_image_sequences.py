# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = [ "AbstractDataImageSequence", "AbstractLabelImageSequence" ]

from abc import ABC, abstractmethod



class AbstractDataImageSequence( ABC ):
    
    @property
    @abstractmethod
    def height(self):
        return None

    @property
    @abstractmethod
    def width(self):
        return None

    @property
    @abstractmethod
    def num_channels(self):
        return None

    @abstractmethod
    def __iter__(self):
        return iter(list())

    @abstractmethod
    def __len__(self):
        return 0

    @abstractmethod
    def __getitem__(self, idx):
        return None




class AbstractLabelImageSequence( ABC ):
    
    @property
    @abstractmethod
    def height(self):
        return None

    @property
    @abstractmethod
    def width(self):
        return None

    @property
    @abstractmethod
    def num_channels(self):
        return None

    @abstractmethod
    def __iter__(self):
        return iter(list())

    @abstractmethod
    def __len__(self):
        return 0

    @abstractmethod
    def __getitem__(self, idx):
        return None
