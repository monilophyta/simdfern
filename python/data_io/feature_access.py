# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["AbstractBoxFeatures", "BoxFeatures", "RecombinedBoxFeatures" ]


from abc import abstractmethod
import collections
import numpy as np

from .memory_advise import madvise_hugepage, madvise_sequential, madvise_willneed, madvise_donotneed
from ..simd import aligned_empty


# ------------------------------------------------------------------------------------------------


class AbstractBoxFeatures( collections.abc.Sequence ):


    box_integrals = None   # [ B x N ]
    is_memmap     : bool = False

    @property
    def num_data(self):
        return self.box_integrals.shape[1]
    

    @property
    def num_boxes(self):
        return self.box_integrals.shape[0]
    

    @property
    @abstractmethod
    def num_features(self):
        return 0


    @abstractmethod
    def get_box_tuple(self,idx):
        return (None, None)


    def __init__(self, box_integrals : np.ndarray ):
        assert box_integrals.ndim == 2
        self.box_integrals = box_integrals

        self.is_memmap = bool(hasattr( box_integrals, 'filename' ))

        if self.is_memmap is True:
            x = self.box_integrals
            while (x.base is not None) and isinstance(x.base, np.ndarray):
                x = x.base
            madvise_hugepage( x )
            madvise_sequential( x )
    

    def __get_box_tuple(self,idx):
        b1, b2 = self.get_box_tuple(idx)  # subclass method
        assert b1.ndim == b2.ndim

        if self.is_memmap is True:
            if b1.ndim == 1:
                madvise_willneed( b1 )
                madvise_willneed( b2 )
            elif b1.ndim == 2:
                for bidx in range( b1.shape[0] ):
                    madvise_willneed( b1[bidx,:] )
                    madvise_willneed( b2[bidx,:] )
        
        return (b1,b2)
    

    @staticmethod
    def __donotneed_memmap( data : np.ndarray ):
        if data.ndim == 1:
            madvise_donotneed( data )
        elif data.ndim == 2:
            for bidx in range( data.shape[0] ):
                madvise_donotneed( data[bidx,:] )
    

    @staticmethod
    def __create_feature_array( shape : tuple, use_memmap : bool ):
        feature = aligned_empty( shape, dtype=np.float32, do_extend=True, use_memmap=use_memmap )[:,:shape[-1]]

        if __debug__:
            feature.fill(np.nan)

        if use_memmap is True:
            madvise_hugepage( feature )
            madvise_sequential( feature )
            madvise_willneed( feature )
        
        return feature
    

    def ratio_feature(self, idx, use_memmap : bool = False ):
        use_memmap = bool( use_memmap )
        b1, b2 = self.__get_box_tuple(idx)
        assert b1.shape == b2.shape
        assert b1.shape[-1] == self.num_data

        # output array
        feature = self.__create_feature_array( b2.shape, use_memmap=use_memmap )
        divisor = np.maximum( b2, np.finfo(np.float32).eps, dtype=np.float32, out=feature )
        feature = np.divide( b1, divisor, dtype=np.float32, out=feature )
        
        if self.is_memmap is True:
            self.__donotneed_memmap( b1 )
            self.__donotneed_memmap( b2 )

        return feature
    

    def difference_feature(self, idx : np.ndarray, 
                                 Lambdas : np.ndarray,
                                 use_memmap : bool = False ):
        use_memmap = bool( use_memmap )
        b1, b2 = self.__get_box_tuple(idx)
        assert b1.shape == b2.shape             # [ f x N ]
        assert b1.shape[-1] == self.num_data
        assert np.all( np.isfinite(Lambdas) )

        if Lambdas.ndim < b2.ndim:
            assert Lambdas.ndim == 1
            assert Lambdas.size == idx.size  
            Lambdas = Lambdas[:,np.newaxis]     # [ f x 1 ]

        # output array
        feature = self.__create_feature_array( b2.shape, use_memmap=use_memmap )
        scaled_y = np.multiply( Lambdas, b2, dtype=np.float32, out=feature )
        feature = np.subtract( b1, scaled_y, dtype=np.float32, out=feature )

        if self.is_memmap is True:
            self.__donotneed_memmap( b1 )
            self.__donotneed_memmap( b2 )
        
        return feature
    
    
    def __getitem__(self, idx):
        return self.ratio_feature(idx)


    def __len__(self):
        return self.num_features

        
    def __iter__(self):
        return ( self[idx] for idx in range(len(self)) )


# ------------------------------------------------------------------------------------------------


class BoxFeatures( AbstractBoxFeatures ):

    @property
    def num_features( self ):
        return (self.num_boxes // 2)


    def get_box_tuple( self, idx ):
        idx = idx << 1
        return (self.box_integrals[idx,:], self.box_integrals[idx+1,:])


    def __init__(self, box_integrals : np.ndarray ):
        assert (box_integrals.shape[0] % 2) == 0
        super( BoxFeatures, self ).__init__( box_integrals )


# ------------------------------------------------------------------------------------------------


class RecombinedBoxFeatures( AbstractBoxFeatures ):

    box_combinations = None   # [ F x 2 ]


    @property
    def num_features( self ):
        return self.box_combinations.shape[0]


    def get_box_tuple( self, idx ):
        idx = self.box_combinations[idx,:].reshape((-1,2))
        idx1 = idx[:,0]
        idx2 = idx[:,1]
        return (self.box_integrals[idx1,:], self.box_integrals[idx2,:])


    def __init__(self, box_integrals : np.ndarray, box_combinations : np.ndarray ):
        assert box_combinations.ndim == 2
        assert box_combinations.shape[1] == 2
        assert np.all(box_combinations >= 0)
        assert box_combinations.max() < box_integrals.shape[0]
        assert np.issubdtype( box_combinations.dtype, np.integer )
        
        super( RecombinedBoxFeatures, self ).__init__( box_integrals )
        self.box_combinations = box_combinations
