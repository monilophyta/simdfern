# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["madvise_normal", "madvise_random", "madvise_sequential", \
           "madvise_willneed", "madvise_donotneed", \
           "madvise_hugepage" ]


import ctypes as ct
import traceback
from enum import IntEnum
import warnings
import os
import sys
import numpy as np


# -------------------------------------------------------------------------------


class MADVICE_VALUES(IntEnum):
    MADV_NORMAL      = 0   # No special treatment
    MADV_RANDOM      = 1   # Expect random page references.
    MADV_SEQUENTIAL  = 2   # Expect sequential page references
    MADV_WILLNEED    = 3   # will need these pages
    MADV_DONTNEED    = 4   # don't need these pages 
    
    MADV_HUGEPAGE    = 14  # Worth backing with hugepages 

# -------------------------------------------------------------------------------


def madvise_normal( data: np.ndarray ):
    posix_madvise( data, MADVICE_VALUES.MADV_NORMAL )


def madvise_random( data: np.ndarray ):
    posix_madvise( data, MADVICE_VALUES.MADV_RANDOM )


def madvise_sequential( data: np.ndarray ):
    posix_madvise( data, MADVICE_VALUES.MADV_SEQUENTIAL )


def madvise_willneed( data: np.ndarray ):
    posix_madvise( data, MADVICE_VALUES.MADV_WILLNEED )


def madvise_donotneed( data: np.ndarray ):
    posix_madvise( data, MADVICE_VALUES.MADV_DONTNEED )


def madvise_hugepage( data: np.ndarray ):
    linux_madvise( data, MADVICE_VALUES.MADV_HUGEPAGE )


# -------------------------------------------------------------------------------


__linux_madvise = lambda *args, **argv: 0
__posix_madvise = lambda *args, **argv: 0


def __set_madvise():
    global __linux_madvise
    global __posix_madvisee

    if os.name == 'posix':

        if sys.platform.startswith('linux'):
            __linux_madvise = ct.CDLL("libc.so.6").madvise
            __linux_madvise.argtypes = [ ct.c_void_p, ct.c_size_t, ct.c_int ]
            __linux_madvise.restype = ct.c_int
            print("madvise(linux) loaded")
            __posix_madvise = __linux_madvise
        else:
            __posix_madvise = ct.CDLL("libc.so.6").posix_madvise
            __posix_madvise.argtypes = [ ct.c_void_p, ct.c_size_t, ct.c_int ]
            __posix_madvise.restype = ct.c_int
            print("posix_madvise loaded")
    elif os.name == "nt":
        pass
    else:
        print("unknown os: no memory advises used")

__set_madvise()

# -----------------------------------------------------------------------------------


def posix_madvise( data: np.ndarray, flag : int ):
    assert data.flags.contiguous or data.flags.f_contiguous
    #print( "posix_madvise: ", flag)

    data_size = data.nbytes
    res = __posix_madvise( data.ctypes.data, data_size, flag )
    if res != 0:
        warnings.warn( "posix_madvise failed with return value=%d and flag=%d" % (res, flag) )
        traceback.print_stack()


def linux_madvise( data: np.ndarray, flag : int ):
    assert data.flags.contiguous or data.flags.f_contiguous
    #print( "linux_madvise: ", flag)

    data_size = data.nbytes
    res = __linux_madvise( data.ctypes.data, data_size, flag )
    if res != 0:
        warnings.warn( "madvise(linux) failed with return value=%d and flag=%d" % (res, flag) )
        traceback.print_stack()



# -----------------------------------------------------------------------------------
