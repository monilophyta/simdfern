# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["memmap_write"]


from typing import Any, Iterator
from collections.abc import Iterable
import os
import platform
import numpy as np
from ..data_sampling import ImageFeatureSampler, calc_number_of_feature_boxes



#---------------------------------------------------------------------------------------


def memmap_write( memmap_file       : Any, 
                  num_data          : int,
                  feature_sampler   : ImageFeatureSampler,
                  position_iterator : Iterator[tuple],
                  label_dtype       : type = np.int32 )  ->  dict:

    sysname = platform.system()

    if sysname == "Linux":
        return LinuxMemmapWriter( num_data ).write( memmap_file,
                                                    feature_sampler,
                                                    position_iterator,
                                                    label_dtype )
    elif sysname == "Windows":
        return memmap_write_generic( memmap_file,
                                     num_data,
                                     feature_sampler,
                                     position_iterator,
                                     label_dtype )
    else:
        assert False, "Unsupported system"
    

# #---------------------------------------------------------------------------------------


class LinuxMemmapWriter( object ):

    num_data    = None

    num_boxes   = None
    buffers     = None    # writing buffers for every row
    buffer_size = None
    fpos        = None    # row positions in memmap file
    dtype       = None


    def __init__(self, num_data ):
        self.num_data = int( num_data )
        assert self.num_data > 0
    

    def init( self, num_boxes, dtype ):
        self.num_boxes = num_boxes
        self.dtype = dtype

        # writing buffers for every row
        self.buffers = tuple( ( list() for i in range(num_boxes) ) )
        self.buffer_size = 0

        # row positions in memmap file
        self.fpos = np.arange( 0, self.num_data * num_boxes, self.num_data )
        self.fpos *= dtype.itemsize


    def buffer_data( self, data, n_num_data ):

        row_size = n_num_data * self.dtype.itemsize

        for fidx, fbuf in enumerate( self.buffers ):

            row_data = data[:,fidx].tobytes(order='C')
            #print( type(row_data), len(row_data) )
            assert len(row_data) == row_size
            fbuf.append( row_data )
        
        # size of new row buffer
        self.buffer_size += row_size
        return self.buffer_size
    

    def write_buffer( self, fout, buffers, buffer_size ):
        
        # write data to disk
        for fidx, (fbuf,rowp) in enumerate( zip( buffers, self.fpos ) ):
            fdata = bytes().join(fbuf)

            if buffer_size is None:
                buffer_size = len(fdata)

            assert len(fdata) == buffer_size
            fbuf.clear()

            fout.seek( rowp )
            fout.write( fdata )
        
        self.fpos += buffer_size

        return buffer_size


    def write( self,
               memmap_file       : str, 
               feature_sampler   : ImageFeatureSampler,
               position_iterator : Iterator[tuple],
               label_dtype       : type = np.int32,
               row_buf_size      : int = 16384 )  ->  dict:
        
        assert isinstance( memmap_file, str )
        assert isinstance( feature_sampler, ImageFeatureSampler )
        assert isinstance( position_iterator, Iterable )
        
        label_list     = list()
        weight_list    = list()
        img_idx_list   = list()
        positions_list = list()

        # create file of not existing
        flags = os.O_CREAT
        # create new file, if existent
        flags = flags | os.O_TRUNC
        # open for writing
        flags = flags | os.O_WRONLY
        #flags = flags | os.O_RDWR
        # no buffering
        #flags = flags | os.O_DIRECT
        # synchronized writing
        flags = flags | os.O_DSYNC


        fout_descriptor = os.open( memmap_file, flags=flags )
        with os.fdopen( fout_descriptor, "w+b", buffering=0 ) as fout:
            didx = 0

            for img_idx, positions, labels, data_weights in position_iterator:
                assert isinstance( img_idx, int )
                assert positions.size == labels.size
                assert data_weights.size == labels.size

                n_num_data = labels.size
                if n_num_data == 0:
                    continue

                data = feature_sampler( img_idx, positions )   # [ nN x F ]
                print( data.shape, data_weights.sum() )
                assert data.shape[0] == n_num_data
                assert data.shape[1] >= calc_number_of_feature_boxes( feature_sampler.num_features )

                if self.dtype is None:
                    # on first seen data sample
                    num_boxes = data.shape[1]
                    self.init( num_boxes, data.dtype )
                
                buffer_size = self.buffer_data( data, n_num_data )
                didx += n_num_data

                # check if writing is required
                do_write = buffer_size >= row_buf_size    # buffer full
                if do_write:
                    self.write_buffer( fout, self.buffers, self.buffer_size )
                    self.buffer_size = 0

                # annotation
                label_list.append( labels )
                weight_list.append( data_weights )
                img_idx_list.append( np.array( [img_idx], dtype=np.int32 ).repeat( n_num_data ) )
                positions_list.append( np.stack( (positions.row_idx, positions.col_idx), axis=-1 )  )
            # endfor

            if self.buffer_size > 0:
                self.write_buffer( fout, self.buffers, self.buffer_size )
                self.buffer_size = 0
            assert self.buffer_size == 0

            # Fill rests of arry with invalid data
            if didx < self.num_data:
                num_invalid_data = self.num_data - didx
                invalid_data = np.empty( num_invalid_data, dtype=self.dtype )

                if np.issubdtype( self.dtype, np.integer ):
                    invalid_data.fill( np.iinfo( self.dtype ).max )
                elif np.issubdtype( self.dtype, np.floating ):
                    invalid_data.fill( np.nan )

                invalid_row_data = invalid_data.tobytes(order='C')
                assert (len(invalid_row_data) % invalid_data.size) == 0
                print( "Writing %d inalid columns (%d bytes)" % (num_invalid_data, len(invalid_row_data)) )

                # Write to disk
                ibuffer = [ [invalid_row_data] for idx in range(num_boxes) ]
                self.write_buffer( fout, ibuffer, len(invalid_row_data) )
                didx += num_invalid_data
            
            assert didx == self.num_data
            assert np.all( 0 == (self.fpos % (self.num_data * self.dtype.itemsize)) )
            assert np.all( self.fpos[1:] > self.fpos[:-1] )

        #os.close( fout_descriptor )
        fout_descriptor = None

        # create annotation
        annotation = { 'labels'             : np.concatenate( label_list, axis=0 ).astype(label_dtype),
                       'data_weights'       : np.concatenate( weight_list, axis=0 ),
                       'img_idx'            : np.concatenate( img_idx_list, axis=0 ),
                       'positions'          : np.concatenate( positions_list, axis=0 ),
                       'boxes'              : feature_sampler.boxes,
                       'box_scaling'        : feature_sampler.box_scaling,
                       'receptive_field'    : feature_sampler.receptive_field,
                       'channel_indices'    : feature_sampler.channel_indices,
                       'channel_box_counts' : feature_sampler.channel_box_counts,
                       'memmap_file'        : memmap_file,
                       'memmap_dtype'       : self.dtype,
                       'row_size'           : self.num_data }
        
        return annotation

#---------------------------------------------------------------------------------------------------


def memmap_write_generic( memmap_file       : str, 
                          num_data          : int,
                          feature_sampler   : ImageFeatureSampler,
                          position_iterator : Iterator[tuple],
                          label_dtype       : type = np.int32 )  ->  dict:
    
    assert isinstance( memmap_file, str ) or hasattr( memmap_file, 'write' )
    assert isinstance( feature_sampler, ImageFeatureSampler )
    assert isinstance( position_iterator, Iterable )

    num_data = int( num_data )
    assert num_data > 0

    label_list     = list()
    weight_list    = list()
    img_idx_list   = list()
    positions_list = list()

    didx = 0
    features = None

    for img_idx, positions, labels, data_weights in position_iterator:
        assert isinstance( img_idx, int )
        assert positions.size == labels.size
        assert positions.size == data_weights.size
        assert np.all( data_weights >= 1 )

        n_num_data = labels.size
        data = feature_sampler( img_idx, positions )   # [ nN x F ]
        print( data.shape, data_weights.sum() )
        assert data.shape[0] == n_num_data
        assert data.shape[1] >= calc_number_of_feature_boxes( feature_sampler.num_features )

        if features is None:
            # open memmap when dtype is known
            num_boxes = data.shape[1]
            features = np.memmap( memmap_file, dtype=data.dtype, mode='w+', shape=(num_boxes, num_data) )

        data = np.require( data.transpose(), requirements=['C_CONTIGUOUS'] )  # [ F x nN ]
        features[:,didx:(didx+n_num_data)] = data
        didx += n_num_data

        # annotation
        label_list.append( labels )
        weight_list.append( data_weights )
        img_idx_list.append( np.array( [img_idx], dtype=np.int32 ).repeat( n_num_data ) )
        positions_list.append( np.stack( (positions.row_idx, positions.col_idx), axis=-1 )  )
    
    assert didx <= num_data

    if didx < num_data:
        if np.issubdtype( features.dtype, np.integer ):
            invalid_val = np.iinfo( features.dtype ).min
        elif np.issubdtype( features.dtype, np.floating ):
            invalid_val = np.nan
        
        # fill rest with invalid values
        features[:,didx:] = invalid_val

    # create annotation
    annotation = { 'labels'             : np.concatenate( label_list, axis=0 ).astype(label_dtype),
                   'data_weights'       : np.concatenate( weight_list, axis=0 ),
                   'img_idx'            : np.concatenate( img_idx_list, axis=0 ),
                   'positions'          : np.concatenate( positions_list, axis=0 ),
                   'boxes'              : feature_sampler.boxes,
                   'box_scaling'        : feature_sampler.box_scaling,
                   'receptive_field'    : feature_sampler.receptive_field,
                   'channel_indices'    : feature_sampler.channel_indices,
                   'channel_box_counts' : feature_sampler.channel_box_counts,
                   'memmap_file'        : memmap_file,
                   'memmap_dtype'       : features.dtype,
                   'row_size'           : num_data }
    
    # close memap
    del features
    
    return annotation




