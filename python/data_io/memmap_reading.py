# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["open_memmap"]


import numpy as np
from ..image import ImageBoxes
from .memory_advise import madvise_donotneed


def open_memmap(      fname : str, \
                 annotation : dict ):
    """
    Opens memmap file for reading
    """

    assert isinstance( annotation, dict )
    assert 'labels' in annotation
    #assert 'boxes' in annotation
    assert isinstance( annotation["labels"], np.ndarray )
    assert isinstance( annotation["boxes"], ImageBoxes )
    #assert annotation["channel_box_counts"].sum() == annotation["boxes"].size

    # real number of data
    num_data  = annotation["labels"].size

    # roll stride
    row_stride = annotation["row_size"]
    assert num_data <= row_stride

    # number of boxes
    num_boxes = annotation["boxes"].size

    # open memmap
    memmap_shape = (num_boxes, row_stride)
    memmap_dtype = annotation["memmap_dtype"]
    print( "memmap_shape", memmap_shape, num_data )
    memmap = np.memmap( fname, dtype=memmap_dtype, mode='r', shape=memmap_shape )

    if num_data < row_stride:

        # check for invalid values in overlap
        if __debug__:
            if np.issubdtype( memmap_dtype, np.integer ):
                assert np.all( memmap[:,num_data:] == np.iinfo( memmap_dtype ).min )
            elif np.issubdtype( memmap_dtype, np.floating ):
                assert np.all( np.isnan( memmap[:,num_data:] ) )
            print( "overlap proofed to be invalid" )

        madvise_donotneed( memmap )
        memmap = memmap[:,:num_data]
    else:
        madvise_donotneed( memmap )

    return memmap
