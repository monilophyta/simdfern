# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["LogisticFernRegressionAlgo"]


#import sys
import typing as tp
from collections import Sequence
import numpy as np

from ..ctypes_py import WeightedFernLayer
from ..ctypes_py import FeatureRows
from .fern_logistic_regression_evaluation import LogisticRegressionEvaluator
from .fern_logistic_regression_minimization_context import MinimizationContext



class LogisticFernRegressionAlgo( object ):

    default_dtype  = np.float32

    # scipy minimize optimization method
    optim_method   : tp.Optional[str] = None

    # optimization tolerance for convergence definition
    optim_tol      : tp.Optional[float] = None

    # optimization tolerance for convergence definition
    optim_gtol     : float = 1e-2

    # strength of regularisation
    l2weight       : float = 1.0

    dtype          = None

    __logistic_regression_evaluator : tp.Optional[LogisticRegressionEvaluator] = None

    # -------------------------------------------------------------------------------


    def __init__(self, dtype = default_dtype ):
        self.dtype = np.dtype( dtype )
        self.__logistic_regression_evaluator = None
    

    @staticmethod
    def get_fern_layer( min_context : MinimizationContext ) -> WeightedFernLayer:

        assert isinstance( min_context, MinimizationContext )
        params = min_context.x
        weights = params[:-1]
        offset = float(params[-1])

        # normalization of weights 
        param_norm = 1 / np.linalg.norm( weights )
        weights = weights * param_norm
        offset  = offset * param_norm
        
        # create fern layer
        fern_layer = WeightedFernLayer.create( weights.astype(np.float32), -float(offset) )
        return fern_layer


    def get_optimization_context( self, layer : WeightedFernLayer,
                                 num_features : tp.Optional[int] = None ) -> MinimizationContext:
        assert (num_features is None) or (num_features >= layer.num_features)
        
        if num_features is None:
            num_features = layer.num_features
        
        regression_layer = np.zeros( num_features + 1, dtype=self.dtype )
        regression_layer[:layer.num_features] = layer.feature_weights
        regression_layer[-1]                  = layer.feature_split

        min_context = MinimizationContext( x0 = regression_layer )

        if self.optim_method is not None:
            min_context.optim_method = self.optim_method
        
        if self.optim_tol is not None:
            min_context.optim_tol = self.optim_tol
        
        min_context.optim_gtol = self.optim_gtol

        return min_context


    def init_logreg_layer_evaluator( self, feature_rows : FeatureRows,
                                         label_idx      : np.ndarray,
                                         leaf_idx       : np.ndarray,
                                         sample_weights : tp.Optional[np.ndarray] = None,
                                         num_labels     : tp.Optional[int] = None,
                                         num_leafs      : tp.Optional[int] = None,
                                         eps            : float = 0.5 ) -> tp.NoReturn:
        
        assert isinstance( label_idx, np.ndarray )
        assert np.issubdtype( label_idx.dtype, np.int32 )
        assert isinstance( leaf_idx, np.ndarray )
        assert np.issubdtype( leaf_idx.dtype, np.int32 )
        assert leaf_idx.size == label_idx.size

        if num_labels is None:
            num_labels = label_idx.max() + 1
        else:
            assert num_labels > 0

        if num_leafs is None:
            num_leafs = leaf_idx.max() + 1
        else:
            assert num_leafs > 0

        if sample_weights is not None:
            assert isinstance( sample_weights, np.ndarray )

        self.__logistic_regression_evaluator = \
            LogisticRegressionEvaluator( feature_rows = feature_rows,
                                            label_idx = label_idx,
                                             leaf_idx = leaf_idx,
                                       sample_weights = sample_weights,
                                           num_labels = num_labels,
                                            num_leafs = num_leafs,
                                                scale = 1.0,
                                                  eps = eps,
                                        eval_gradient = False,
                                        eval_jacobian = False,
                                       max_cache_size = 4,
                                                dtype = self.dtype )


    def optimize_layer( self, min_context : MinimizationContext,
                              logreg_eval : LogisticRegressionEvaluator,
                                num_iters : int = 500,
                                 callback : tp.Optional[tp.Callable] = None ):

        num_data = logreg_eval.num_data
        
        # data normalization -> required to get a comparable scale for l2weight
        assert self.l2weight > 0
        data_norm = 1 / ( float(num_data) * self.l2weight )

        # evaluations:
        func_eval = lambda x: (data_norm * logreg_eval.eval(x)) + logreg_eval.l2norm_eval(x)
        jac_eval  = lambda x: (data_norm * logreg_eval.gradient(x)) + logreg_eval.l2norm_gradient(x)
        hess_eval = lambda x: (data_norm * logreg_eval.hessian(x))  + logreg_eval.l2norm_hessian(x)

        # run one optimization step
        min_context.minimize( func = func_eval,
                               jac = jac_eval,
                              hess = hess_eval,
                     max_num_iters = int(num_iters),
                          callback = callback )

