# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["FernPartitionOptimizer", "LineSearchResult"]


import itertools as it
import typing as tp
#import math
import numpy as np

from ..ctypes_py import WeightedFernLayer
from ..ctypes_py import FeatureRows, create_feature_rows
from ..ctypes_py import project_features, FernSplitOptimizer, ESummationAlgo
from ..ctypes_py import lsearch_fern_param, line_search_result_t
from ..ctypes_py import ParamPenalizer, EPenalizerType
from .. import simd

from ..clmath import PowellsMethod

from .fern_logistic_regression_evaluation import LogisticRegressionEvaluator
from .fern_logistic_regression_algo import LogisticFernRegressionAlgo
#from .fern_logistic_regression_minimization_context import MinimizationContext



class FernPartOptimResult(object):
    """
    Class for the collection of fern part optimization results

    Attributes
    ----------
    rating : int (gini) or float (entropy)
        The final obtained rating without regularisation term
    fern_layer : instance of WeightedFernLayer [ K ]
        Resulting new fern layer after optimization with applied dir_scale.
        Feature weights are renormalized
    regul_strength : float
        Applied strength of regularisation
    **other_params:
        further attributes might be added which are optimization specific
    """

    rating                 : tp.Union[int,float]
    fern_layer             : WeightedFernLayer
    regul_strength         : float
    num_invalid_candidates : int   # number of invalid (infinite) candidates during line search optimization

    @property
    def penalty(self) -> float:
        return float(self.regul_strength * self.fern_layer.centralized_l1norm)

    @property
    def regularized_rating(self) -> float:
        return (float(self.rating) + self.penalty)

    def __init__(self, rating : tp.Union[int,float],
                   fern_layer : WeightedFernLayer,
               regul_strength : float,
       num_invalid_candidates : int = 0,
               **other_params ):
        
        self.rating = rating
        self.fern_layer = fern_layer
        self.regul_strength = float(regul_strength)
        self.num_invalid_candidates = int(num_invalid_candidates)

        for key,value in other_params.items():
            setattr(self,key,value)



class LineSearchResult(FernPartOptimResult):
    """
    Class for the collection of line search optimization results

    Attributes
    ----------
    rating : int (gini) or float (entropy)
        The final obtained rating without regularisation term
    fern_layer : instance of WeightedFernLayer [ K ]
        Resulting new fern layer after optimization with applied dir_scale.
        Feature weights are renormalized
    regul_strength : float
        Applied strength of regularisation
    num_optim_steps : int
        Number of steps, the optimization criterion got increased
    num_invalid_candidates : int
        number of invalid (infinite) candidates during line search optimization
    **other_params:
        further attributes might be added which are optimization specific
    """

    num_optim_steps        : int

    def __init__(self, rating : tp.Union[int,float],
                   fern_layer : WeightedFernLayer,
              num_optim_steps : int,
               regul_strength : float,
       num_invalid_candidates : int = 0,
               **other_params ):
        super().__init__(rating, fern_layer, regul_strength, num_invalid_candidates, **other_params )
        self.num_optim_steps = int(num_optim_steps)



class FernPartitionOptimizer( object ):
    """
    Attributes
    ----------
    objective : str
        Either "gini" or "entropy"
    feature_rows : instance of FeatureRows [ K x W ]
    label_idx : int32 ndarray [ N ]
        Labels of each sample
    sample_weights : int32 ndarray [ N ]
        Weight of each sample
    num_labels : int or None
        Number of labels
    sum_sample_weights : int
        Sum of all sample weights
    regul_strength : float
        Strength (weight) of regularization
    """

    # optimization
    objective        : str = "gini"   # split optimizer currently only supports gini
    get_rating       : tp.Callable[[line_search_result_t],float]
    get_total_rating : tp.Callable[[line_search_result_t],float]

    rand_state     : np.random.RandomState

    feature_rows   : FeatureRows

    label_idx      : np.ndarray
    sample_weights : np.ndarray

    num_labels : int
    sum_sample_weights : int

    # regularisation
    regul_strength : float

    node_idx  : np.ndarray
    num_nodes : int

    # -----------------------------------------------
    # Derived attributes
    
    @property
    def num_features(self) -> int:
        return self.feature_rows.num_features

    @property
    def num_data(self) -> int:
        return self.label_idx.size

    @property
    def leaf_idx_base(self) -> np.ndarray:
        leaf_idx = np.left_shift( self.node_idx, 1 )
        assert np.all( (leaf_idx % 2) == 0 )
        return leaf_idx


    # -----------------------------------------------
    # Gradient calculation parameters
    
    grd_weight_scale  : float
    grd_eps           : float
    logreg_dtype      : np.dtype

    # -----------------------------------------------
    # internal properties

    weight_optimization_order : tp.Optional[tp.List[int]] = None

    # instance of split optimization
    __split_optimizer : tp.Optional[FernSplitOptimizer]

    @property
    def split_optimizer(self) -> FernSplitOptimizer:
        if self.__split_optimizer is None:
            self.__split_optimizer = FernSplitOptimizer( node_idx = self.node_idx,
                                                        label_idx = self.label_idx,
                                                     data_weights = self.sample_weights,
                                                        num_nodes = self.num_nodes,
                                                       num_labels = self.num_labels )
        return self.__split_optimizer

    # gradient evaluator
    __logreg_eval : tp.Optional[LogisticRegressionEvaluator]

    @property
    def logreg_eval(self) -> LogisticRegressionEvaluator:
        if self.__logreg_eval is None:
            self.__logreg_eval = LogisticRegressionEvaluator( feature_rows = self.feature_rows,
                                                                 label_idx = self.label_idx,
                                                                  leaf_idx = self.node_idx,
                                                            sample_weights = self.sample_weights,
                                                                num_labels = self.num_labels,
                                                                 num_leafs = self.num_nodes,
                                                                     scale = self.grd_weight_scale,
                                                                       eps = self.grd_eps,
                                                             eval_gradient = False,
                                                             eval_jacobian = False,
                                                                     dtype = self.logreg_dtype,
                                                            max_cache_size = 2 )
        return self.__logreg_eval

    # Powells method
    __powells_algo : tp.Optional[PowellsMethod]

    @property
    def powells_algo(self) -> PowellsMethod:
        if self.__powells_algo is None:
            self.__powells_algo = PowellsMethod( xinit = np.zeros( self.num_features+1, dtype=np.float32 ),
                                            rand_state = self.rand_state,
                                     rand_dir_set_init = True )
        return self.__powells_algo

    # -----------------------------------------------

    def __init__(self, feature_rows        : FeatureRows,
                       label_idx           : np.ndarray,
                       node_idx            : np.ndarray,
                       sample_weights      : tp.Optional[np.ndarray] = None,
                       num_labels          : tp.Optional[int] = None,
                       num_leafs           : tp.Optional[int] = None,
                       regul_strength      : float = 1.0,
                       grd_weight_scale    : float = 1.0,
                       grd_eps             : float = 0.1,
                       logreg_dtype        : np.dtype = np.float32,
                       rand_state          : tp.Optional[tp.Union[int,np.random.RandomState]] = None ):
        
        assert self.objective == 'gini'
        assert isinstance( feature_rows, FeatureRows )
        assert len(feature_rows[0]) == label_idx.size
        assert label_idx.size == node_idx.size
        assert (sample_weights is None) or ( (sample_weights.size == label_idx.size) and np.issubdtype( sample_weights.dtype, np.int32 ) )

        assert np.issubdtype( node_idx.dtype, np.int32 )
        assert np.issubdtype( label_idx.dtype, np.int32 )

        if num_labels is None:
            num_labels = label_idx.max() + 1
        assert num_labels > label_idx.max()

        if num_leafs is None:
            num_leafs = node_idx.max() + 1
        assert num_leafs > node_idx.max()

        # Initialize random state 
        if isinstance( rand_state, np.random.RandomState ):
            self.rand_state = rand_state
        else:
            self.rand_state = np.random.RandomState(seed=rand_state)

        self.num_labels      = int(num_labels)
        self.num_nodes       = int(num_leafs)
        self.feature_rows    = feature_rows
        self.label_idx       = label_idx
        
        # shift up leaf indices
        self.node_idx = node_idx

        if sample_weights is None:
            sample_weights = np.ones( label_idx.size, dtype=np.int32 )
            sum_sample_weights = int(label_idx.size)
        else:
            sum_sample_weights = int(sample_weights.sum(dtype=np.int64))
        self.sample_weights = sample_weights
        self.sum_sample_weights = sum_sample_weights

        # set gradient calculation parameters
        self.grd_weight_scale = grd_weight_scale
        self.grd_eps = grd_eps
        self.logreg_dtype = logreg_dtype

        # set strength of regularization
        self.regul_strength = regul_strength
        
        self.__split_optimizer = None
        self.__logreg_eval = None
        self.__powells_algo = None

        # set rating selector
        if self.objective == 'gini':
            self.get_rating = lambda optim_res: optim_res.nodeGiniRating
            self.get_total_rating = lambda optim_res: float(optim_res.nodeGiniRating) + optim_res.prmValPenalty
        elif self.objective == 'entropy':
            self.get_rating = lambda optim_res: optim_res.nodeEntropy
            self.get_total_rating = lambda optim_res: optim_res.nodeEntropy + optim_res.prmValPenalty
        else:
            assert False
    
    # -------------------------------------------------------------------------------------------------------------------------

    
    def optimize_split(self, fern_layer : WeightedFernLayer) -> tp.Optional[LineSearchResult]:
        """
        Parameters
        ----------
        fern_layer : instance of WeightedFernLayer
            Fern layer who's split parameter is optimized

        Returns instance of LineSearchResult
        """
        assert isinstance( fern_layer, WeightedFernLayer )
        assert fern_layer.num_features == self.feature_rows.num_features
        
        # project features
        projections = project_features( fern_layer.feature_weights, self.feature_rows )  # [ N ]

        # reset split optimizer from last optimization
        self.split_optimizer.reset_optimal_split()

        # process projection
        num_optim_steps = self.split_optimizer.process_feature( projections )
        assert num_optim_steps > 0
        
        
        optimal_split, rating = self.split_optimizer.get_optimal_split()
        assert optimal_split.feature_idx == self.split_optimizer.last_feature_idx, "Invalid feature processed"
        
        n_fern_layer = WeightedFernLayer.create( feature_weights = fern_layer.feature_weights,
                                                    feature_split = optimal_split.feature_split )

        return LineSearchResult( rating = rating, 
                        num_optim_steps = num_optim_steps,
                             fern_layer = n_fern_layer,
                         regul_strength = self.regul_strength )
        

    def next_weight_optimization_index(self, rand_state : np.random.RandomState ):
        assert isinstance( rand_state, np.random.RandomState )

        if self.weight_optimization_order is None:
            self.weight_optimization_order = list( rand_state.permutation( self.num_features ) )
        elif len(self.weight_optimization_order) == 1:
            # sample new list without last weight in front
            while True:
                n_weight_optimization_order = rand_state.permutation( self.num_features )
                if n_weight_optimization_order[0] != self.weight_optimization_order[0]:
                    self.weight_optimization_order.extend( list(n_weight_optimization_order) )
                    break
        
        assert len(self.weight_optimization_order) > 1
        return self.weight_optimization_order.pop(0)


    def optimize_weight(self, fern_layer : WeightedFernLayer,
                                    cidx : int ) -> LineSearchResult:
        """
        Parameters
        ----------
        fern_layer : instance of WeightedFernLayer
            Fern layer with start parameters.
            Only one weight is going to be optimized.
        cidx : indx
            The weight of the coordinate who's weight is to be optimized

        Performs a line search along the weight of one coordinate

        Returns instance of LineSearchResult with members:
        --------------------------------------------------
        regular parameters of LineSearchResul and
        
        weight_penalty : float
            regularization penalty of weight
        co_scale : float
            weight
        num_optim_steps : int
            Number of steps, the optimization criterion got increased
        """
        assert isinstance( fern_layer, WeightedFernLayer )
        assert fern_layer.num_features == self.feature_rows.num_features

        # projection of all features except feature_rows[cidx]
        init_leaf_idx, param_candidates, num_invalid = self.calc_lsearch_candidates( 
                                                           factor = self.feature_rows[cidx],
                                                           offset = self.calc_coordinate_marginal( fern_layer=fern_layer, cidx=cidx ) )
        if init_leaf_idx is None:
            assert param_candidates is None
            return None

        # param penalizer for weight regularization
        penalizer = self.create_single_weight_penalizer( cidx=cidx, fern_layer=fern_layer )

        # run the actual optimization
        res = lsearch_fern_param( param_candidates = param_candidates,
                                          node_idx = init_leaf_idx,
                                         label_idx = self.label_idx,
                                      data_weights = self.sample_weights,
                                         num_nodes = self.num_nodes << 1,
                                        num_labels = self.num_labels,
                                         objective = self.objective,
                                         penalizer = penalizer )
        
        # create new layer as result of optimization
        n_feature_weights = fern_layer.feature_weights.copy()
        n_feature_weights[cidx] = res.prmVal
        n_layer = self.create_fern_partition( feature_weights = n_feature_weights,
                                                feature_split = fern_layer.feature_split )
        
        # compose return
        return LineSearchResult( rating = self.get_rating(res), # get rating (without regularisation),, 
                        num_optim_steps = res.num_improvements,
                             fern_layer = n_layer,
                         regul_strength = self.regul_strength,
                         weight_penalty = res.prmValPenalty,
                               co_scale = res.prmVal,
                 num_invalid_candidates = num_invalid )


    def optimize_direction(self, fern_layer : WeightedFernLayer,
                         use_regul_gradient : bool = True,
                             optimize_split : bool = False,
                         weight_lsearch_dir : tp.Optional[np.ndarray] = None,
                          split_lsearch_dir : tp.Optional[float] = None ) -> LineSearchResult:
        """
        Parameters
        ----------
        fern_layer : instance of WeightedFernLayer
            Fern layer with start parameters serving as offset point in line search
        use_regul_gradient : bool
            If True (default) the search direction is combined with the gradient of the regularisation
        optimize_split : bool
            If False (default) no optimization on split is performed
        weight_lsearch_dir : float32 ndarray [ K ] (optional)
            Direction in fern layer weight space along which line search is performed.
            A good direction might be the gradient at point of fern_layer
            If no weight_lsearch_dir is provided, the gradient of logisitic regression for maximization of mutual information is used.
        split_lsearch_dir : float (optional)
            Only used when 
            Search direction for split parameters
            A good direction might be the gradient at point of fern_layer
            If no weight_lsearch_dir is provided, the gradient of logisitic regression for maximization of mutual information is used.

        Performs a line search along $o + x * g$ with 
            $o$ = [ fern_layer.feaure_weights, -fern_layer.feature_split ]
            $g$ = [ weight_lsearch_dir, -split_lsearch_dir ]
            $x$ = dir_scale (output)

        In case 'optimize_direction' calculates its own search directions on the basis of logisitic regression gradient


        Returns instance of LineSearchResult with members:
        --------------------------------------------------
        regular parameters of LineSearchResul and

        dir_scale : float
            Result of optimization
        dir_scale_penalty : float
            Regularisation term
        lsearch_dir : float32 ndarray [ K ]
            Actual search direction in line search. Should be the input search direction
            of combination if the input and the regularisation gradient
        num_optim_steps : int
            Number of steps, the optimization criterion got increased
        """
        assert isinstance( fern_layer, WeightedFernLayer )
        assert fern_layer.num_features == self.feature_rows.num_features
        optimize_split = bool(optimize_split)


        # set line search direction if none has been provided
        if (weight_lsearch_dir is None) or (optimize_split and (split_lsearch_dir is None)):
            params = np.concatenate( [ fern_layer.feature_weights, 
                                     [-float(fern_layer.feature_split) ] ], axis=0 ).astype(self.logreg_dtype)
            gradient = self.logreg_eval.gradient( params )

            if not optimize_split:
                gradient[-1] = 0

            if weight_lsearch_dir is None:
                weight_lsearch_dir = gradient[:-1].astype(np.float32)
            
        if split_lsearch_dir is None:
            if optimize_split:
                split_lsearch_dir = float(gradient[-1])   # TODO: check if this has to be plus or minus (plot indicated plus)
            else:
                split_lsearch_dir = 0.

        # --------------------------------------------------------------------------------------------
        
        # Add derivative of regularization to gradient (search direction)
        if bool(use_regul_gradient):
            weight_lsearch_dir = weight_lsearch_dir + self.calc_penalizer_gradient( fern_layer )

        # --------------------------------------------------------------------------------------------

        # run line search
        res, num_invalid = self.__do_line_search( feature_weights = fern_layer.feature_weights,
                                                    feature_split = fern_layer.feature_split,
                                               weight_lsearch_dir = weight_lsearch_dir,
                                                split_lsearch_dir = split_lsearch_dir )
        
        # create new layer as result of optimization
        n_layer = self.create_fern_partition( feature_weights = (res.prmVal * weight_lsearch_dir) + fern_layer.feature_weights,
                                              feature_split = (res.prmVal * split_lsearch_dir)  + fern_layer.feature_split )

        # compose return
        return LineSearchResult( rating = self.get_rating(res), # get rating (without regularisation)
                        num_optim_steps = res.num_improvements,
                             fern_layer = n_layer,
                         regul_strength = self.regul_strength,
                      dir_scale_penalty = res.prmValPenalty,
                              dir_scale = res.prmVal,
                     weight_lsearch_dir = weight_lsearch_dir,
                      split_lsearch_dir = split_lsearch_dir,
                 num_invalid_candidates = num_invalid )


    def optimize_logistic_regression(self, fern_layer : WeightedFernLayer,
                                        max_num_iters : int,
                                             l2weight : float,
                                           optim_gtol : float,
                                            optim_tol : tp.Optional[float] = None,
                                         optim_method : tp.Optional[str] = None ):

        assert np.issubdtype( self.logreg_dtype, np.floating )
        assert l2weight > 0
        assert optim_method in {None, 'BFGS', 'Newton-CG', 'trust-ncg', 'trust-krylov', 'trust-exact'}
        assert optim_tol is None or (optim_tol > 0)
        assert optim_gtol > 0

        # create logistic regression alogrithm 
        logreg_algo = LogisticFernRegressionAlgo( dtype = self.logreg_dtype )
        if optim_method is not None:
            logreg_algo.optim_method = optim_method

        if optim_tol is not None:
            logreg_algo.optim_tol = optim_tol  # Tolerance for termination

        logreg_algo.optim_gtol = optim_gtol # gradient norm must be less than gtol before successful termination. Relevant for BFGS, trust-exact
        logreg_algo.l2weight = l2weight  # regularization weight for l2norm on weights

        # obtain quadratic programming context
        min_context = logreg_algo.get_optimization_context( fern_layer, num_features=self.num_features )

        # run the optimization
        logreg_algo.optimize_layer( min_context = min_context, 
                                    logreg_eval = self.logreg_eval,
                                      num_iters = max_num_iters )

        # optimization result
        ret = dict( fern_layer = logreg_algo.get_fern_layer( min_context ),
                    is_converged = min_context.is_converged,
                    optim_method = min_context.optim_method,
                    iter_count = min_context.iter_count,
                    status_message = min_context.status_message )
        return ret


    # ------------------------------------------------------------------------------------------------------------------
    
    def powells_method(self, fern_layer : WeightedFernLayer,
                          max_num_iters : int,
                             optim_dtol : float,
            enforce_reorthogonalization : bool = False ) -> FernPartOptimResult:
        """
        Fern partition optimization using powells method for minimization without derivatives

        Parameters
        ----------
        fern_layer : instance of WeightedFernLayer
            layer for parameter initialization
        max_num_iters : int
            maximum number of iterations
        optim_dtol : float
            threshold for comparison of subsequent direction estimates. If difference falls below
            this threshold the optimization terminates
        enforce_reorthogonalization : bool
            performns a re-orthogonalization of the direction set before running powells method
        
        Returns
        -------
        instance of  FernPartOptimResult
            containing all relevant results of parameter optimization
        """
        
        max_num_iters = int(max_num_iters)
        optim_dtol = float(optim_dtol)

        # X0 initializer
        x0 = np.concatenate( [ fern_layer.feature_weights, 
                               [fern_layer.feature_split] ], axis=0 ).astype(np.float32)

        # update powell optimizer with currently best known param configuration
        # this also performs a re-orthogonalization of the direction set
        self.powells_algo.reset_xpos( x0, reothogonalize = enforce_reorthogonalization )
        
        # line search function
        def lsearch_func(xpos, xdir) -> tp.Tuple[float,line_search_result_t]:
            res = self.__do_line_search( feature_weights = xpos[:-1],
                                           feature_split = float(xpos[-1]),
                                      weight_lsearch_dir = xdir[:-1],
                                       split_lsearch_dir = float(xdir[-1]) )
            if res is None:
                # we encountered problems with the projection and might better stay on the same value
                return (0., None)
            
            res, num_invalid = res
            return (res.prmVal, (res,num_invalid))

        # initialize total rating
        last_total_rating = np.finfo(np.float32).max

        # optimization loop
        for iter_idx in it.count(1):
            optim_dir, optim_res = self.powells_algo.optimization_step(lsearch_func)

            if optim_res is None:
                # in some very bad data situation "optim_res" might be None
                raise NotImplementedError("Unhandled case of 'optim_res == None'")
            optim_res, num_invalid_candidates = optim_res

            optim_dir_norm = np.linalg.norm( optim_dir )

            n_total_rating = self.get_total_rating( optim_res )
            #print( iter_idx, n_total_rating )
            if last_total_rating <= n_total_rating:
                break
            last_total_rating = n_total_rating

            if optim_dir_norm < optim_dtol:
                break

            if iter_idx == max_num_iters:
                break
        
        # create new layer as result of optimization
        fern_layer = self.create_fern_partition( feature_weights = self.powells_algo.xpos[:-1],
                                                   feature_split = float(self.powells_algo.xpos[-1]) )
        
        # compose return
        return FernPartOptimResult( rating = self.get_rating(optim_res), # get rating (without regularisation),
                                fern_layer = fern_layer,
                            regul_strength = self.regul_strength,
                          last_dir_penalty = optim_res.prmValPenalty,
                             last_dir_norm = optim_dir_norm,
                                 num_iters = iter_idx,
                        weight_lsearch_dir = optim_dir[:-1],
                         split_lsearch_dir = float(optim_dir[-1]),
                    num_invalid_candidates = num_invalid_candidates )

    # ------------------------------------------------------------------------------------------------------------------
    
    def create_single_weight_penalizer(self, fern_layer : WeightedFernLayer,
                                                   cidx : int ) -> ParamPenalizer:
        """creates penalizer for optimization of a single weight"""
        
        l1offset = float( np.absolute( fern_layer.feature_weights[:cidx], dtype=np.float64 ).sum() ) + \
                   float( np.absolute( fern_layer.feature_weights[cidx+1:], dtype=np.float64 ).sum() )
        sqrl2Offset = float( np.square( fern_layer.feature_weights[:cidx], dtype=np.float64 ).sum()) + \
                      float( np.square( fern_layer.feature_weights[cidx+1:], dtype=np.float64 ).sum() )

        return ParamPenalizer.create_single_regularization( 
                                    regul_type = EPenalizerType.CENTRALIZED_L1, 
                                regul_strength = self.regul_strength,
                                  num_features = self.num_features,
                                      l1offset = l1offset,
                                   sqrl2Offset = sqrl2Offset,
                                       l1ratio = 1.0 )
    
    
    def create_vector_penalizer(self, offset_weights : np.ndarray,
                                      scaled_weights : np.ndarray ) -> ParamPenalizer:
        """creates penalizer for optimization along a line search direction"""
        assert isinstance( offset_weights, np.ndarray )
        assert isinstance( scaled_weights, np.ndarray )
        assert offset_weights.size == scaled_weights.size
        
        do_align = simd.default_simd_vector32_size() <= (offset_weights.size * 2)

        return ParamPenalizer.create_vector_regularization( 
                                    regul_type = EPenalizerType.CENTRALIZED_L1, 
                                regul_strength = self.regul_strength,
                                        offset = offset_weights,
                                         scale = scaled_weights,
                                      do_align = do_align )

    # -------------------------------------------------------------------------------------------------

    def calc_coordinate_marginal(self, fern_layer : WeightedFernLayer,
                                             cidx : int ) -> np.ndarray:
        """For single weight optimization: calculates projection on a single weight axis"""
        feature_mask = np.ones( self.num_features, dtype=np.bool )  # [ F ]
        feature_mask[cidx] = False

        # marginal feature selection
        m_feature_rows = create_feature_rows( \
                                num_data = self.num_data,
                            feature_rows = [ fr for (fr,fm) in zip( self.feature_rows, feature_mask ) if fm ],
                feature_row_pointer_list = [ frp for (frp,fm) in zip( self.feature_rows.feature_row_pointer_list, feature_mask ) if fm ] )
        
        # project margin features
        m_projection = project_features( fern_layer.feature_weights[feature_mask], m_feature_rows )  # [ N ]

        return np.subtract( fern_layer.feature_split, m_projection, out=m_projection )


    def calc_penalizer_gradient( self, fern_layer : WeightedFernLayer ) -> np.ndarray:
        """Gradient of normalized L1 regularization term"""
        return (self.regul_strength * fern_layer.centralized_l1norm_gradient)


    # -------------------------------------------------------------------------------------------------

    def calc_plane_normal_projection( self, feature_weights : np.ndarray,
                                              feature_split : float ) -> np.ndarray:
        """For line search optimization: projects all data samples along a plane normal"""
        assert isinstance( feature_weights, np.ndarray )
        assert isinstance( feature_split, float )
        assert np.issubdtype( feature_weights.dtype, np.float32 )
        assert feature_weights.size == self.num_features

        projection = project_features( feature_weights, self.feature_rows, ESummationAlgo.NEUMAIER ) # [ N ]
        return np.subtract( feature_split, projection, out=projection )


    def calc_lsearch_projection(self, weight_lsearch_dir : np.ndarray,
                                       split_lsearch_dir : float ) -> np.ndarray:
        """For line search optimization: projects all data samples along the line search direction"""
        assert isinstance( weight_lsearch_dir, np.ndarray )
        assert isinstance( split_lsearch_dir, float )
        assert np.issubdtype( weight_lsearch_dir.dtype, np.float32 )
        assert weight_lsearch_dir.size == self.num_features

        projection = project_features( weight_lsearch_dir, self.feature_rows, ESummationAlgo.NEUMAIER )

        if split_lsearch_dir != 0:
            projection = np.subtract( projection, split_lsearch_dir, out=projection )
        
        return projection


    def __do_line_search(self, feature_weights : np.ndarray,
                                 feature_split : float,
                            weight_lsearch_dir : np.ndarray,
                             split_lsearch_dir : float ) -> tp.Tuple[line_search_result_t,int]:
        # Project all samples onto the plane normal
        plane_normal_projection = self.calc_plane_normal_projection( feature_weights = feature_weights,
                                                                       feature_split = feature_split )   # [ N ]
        
        # Project all samples onto the line search direction
        lsearch_projection = self.calc_lsearch_projection( weight_lsearch_dir = weight_lsearch_dir,
                                                            split_lsearch_dir = split_lsearch_dir )  # [ N ]

        # calculate candate for search direction scaling
        init_leaf_idx, scale_candidates, num_invalid = self.calc_lsearch_candidates( 
                                                           factor = lsearch_projection,
                                                           offset = plane_normal_projection )  # [ N ]
        if init_leaf_idx is None:
            assert scale_candidates is None
            return None
        del lsearch_projection
        del plane_normal_projection

        # param penalizer for weight regularization
        penalizer = self.create_vector_penalizer( offset_weights = feature_weights, 
                                                  scaled_weights = weight_lsearch_dir )

        # run the actual optimization
        res = lsearch_fern_param( param_candidates = scale_candidates,
                                          node_idx = init_leaf_idx,
                                         label_idx = self.label_idx,
                                      data_weights = self.sample_weights,
                                         num_nodes = self.num_nodes << 1,
                                        num_labels = self.num_labels,
                                         objective = self.objective,
                                         penalizer = penalizer )
        assert isinstance( res, line_search_result_t )

        return (res, num_invalid)


    # -------------------------------------------------------------------------------------------------

    def calc_lsearch_candidates( self, factor, offset ):

        with np.errstate(divide='ignore',invalid='ignore'):
            candidates = np.divide( offset, factor )  # [ N ]

        # mask if finite values
        finite_mask = np.isfinite( candidates )

        if np.any( finite_mask ):
            # margin position
            min_candidate = candidates[finite_mask].min()
            num_invalid = finite_mask.size - finite_mask.sum()
        else:
            # No optimization possible
            return (None, None, finite_mask.size)
        # for the sake of performance
        bool_out = finite_mask
        del finite_mask

        # classify according to min_candidate; NaN <=> 0/0 also goes to the right side (idx==0)
        with np.errstate(invalid='ignore'):
            init_leaf_idx = np.less_equal( min_candidate, candidates, out=bool_out ).astype(np.int32)
        assert np.all( init_leaf_idx[np.isnan(candidates)] == 0 )

        # flip sides of samples with negative feature
        init_leaf_idx = np.bitwise_xor( init_leaf_idx,
                                        np.less(factor, 0, out=bool_out).astype(np.int32),
                                        out = init_leaf_idx )
        
        # add other fern levels
        init_leaf_idx += self.leaf_idx_base
        assert np.all( init_leaf_idx < (self.num_nodes << 1) )

        return (init_leaf_idx, candidates, num_invalid)

    # -------------------------------------------------------------------------------------------------

    @staticmethod
    def create_fern_partition( feature_weights : np.ndarray, 
                                 feature_split : float ) -> WeightedFernLayer:
        """normalizes a weights and creates a new fern_partition"""
        
        assert isinstance( feature_weights, np.ndarray )
        feature_split = float(feature_split)

        # normalize weight vector to length one
        norm = 1. / np.linalg.norm( feature_weights )
        feature_weights *= norm
        feature_split *= norm

        return WeightedFernLayer.create( feature_weights=feature_weights, feature_split=feature_split )

