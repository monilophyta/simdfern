# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["SLSQPContext"]


import typing as tp
import numpy as np
import scipy
from scipy.optimize._slsqp import slsqp



class SLSQPContextException( Exception ):
    def __init__(self, msg):
        super(SLSQPContextException,self).__init__(msg)


class SLSQPContext(object):

    x  : np.ndarray

    @property
    def num_independent_variables(self) -> int:
        return int(self.x.size)
    
    num_equal_constr : int

    # Workspace for SLSQP
    w  : np.ndarray
    jw : np.ndarray

    # internal SLSQP variables (used in scipy version >1.2.0)
    internal : list

    # dummy bounds parameters
    xl : np.ndarray
    xu : np.ndarray

    mode    : int
    majiter : int

    # values updated during iterations
    fx     : float             # [ 1 ]                      function evaluation 
    c_eq   : np.ndarray        # [ num_equal_constr ]       constraints evaluations
    g      : np.ndarray        # [ n + 1 ]                  gradient evaluation
    a_eq   : np.ndarray        # [ num_equal_constr x n+1 ] gradients of constraints

    # evaluation counters
    num_function_evals : int
    num_gradient_evals : int

    # convergence indicator
    is_converged : bool
    iter_count : int



    def __init__(self, x0, num_eq_constr : int, max_iter : int = 1 << 15 ):
        self.x = np.asfarray(x0).flatten()

        self.num_equal_constr = int(num_eq_constr)
        assert self.num_equal_constr >= 0
        
        # Set the parameters that SLSQP will need
        # meq, mieq: number of equality and inequality constraints
        meq  = num_eq_constr
        mieq = 0
        
        # m = The total number of constraints
        m = meq + mieq
        
        # n = The number of independent variables
        n = self.num_independent_variables
        
        # Define the workspaces for SLSQP
        n1 = n + 1
        mineq = m - meq + n1 + n1
        len_w = (3*n1+m)*(n1+1)+(n1-meq+1)*(mineq+2) + 2*mineq+(n1+mineq)*(n1-meq) \
                 + 2*meq + n1 + ((n+1)*n)//2 + 2*m + 3*n + 3*n1 + 1
        len_jw = mineq
        self.w  = np.zeros(len_w)
        self.jw = np.zeros(len_jw)

        # no bound restrictions
        self.xl = np.empty( n, dtype=float )
        self.xl.fill(np.nan)
        self.xu = np.empty( n, dtype=float )
        self.xu.fill(np.nan)

        # Initialize the iteration counter and the mode value
        self.reinitialize( max_iter = max_iter )

        # initialize values updated during iterations
        self.fx   = np.inf
        self.g    = np.zeros( n+1, float )
        self.c_eq = np.zeros( num_eq_constr, float )
        self.a_eq = np.zeros( (max(num_eq_constr,1), n+1), float )

        if numeric_scipy_version() > 10200:
            # scipy.version > 1.2.0
            self.internal = init_slsqp_internal()
        else:
            self.internal = list()

    # -----------------------------------------------------------------------------

    def reinitialize(self, max_iter : int = 1 << 15 ):
        # Initialize the iteration counter and the mode value
        self.mode = 0
        self.majiter = int(max_iter)

        # empty workspaces
        self.w.fill(0)
        self.jw.fill(0)

        self.num_function_evals = 0
        self.num_gradient_evals = 0

        self.is_converged = False
        self.iter_count = 0


    # -----------------------------------------------------------------------------

    def next_iteration( self, func, jac, 
                                    args : tuple = (),
                        constraint_funcs : tuple = (),
                          constraint_jac : tuple = (),
                                    ftol : float = 1.0E-6,
                           max_num_iters : int = 1,
                          throw_on_error : bool = True ):

        assert len(constraint_funcs) == self.num_equal_constr
        assert len(constraint_jac) == self.num_equal_constr
        assert self.is_converged is False

        num_iters = int(max_num_iters)
        assert num_iters > 0

        constraint_funcs = constraint_funcs[:self.num_equal_constr]
        constraint_jac   = constraint_jac[:self.num_equal_constr]

        if self.mode != 0:
            majiter_prev = self.majiter
        else:
            majiter_prev = 0
        
        majiter = np.array( self.majiter, int )

        while num_iters > 0:

            if self.mode in {0,1}:  # objective and constraint evaluation requird

                # Compute objective function
                self.fx = float( func(self.x) )
                
                self.num_function_evals += 1
                
                 # Compute the constraints
                if self.num_equal_constr > 0:
                    self.c_eq[:] = [ float(cfuncs(self.x, *args)) for cfuncs in constraint_funcs  ]

            if self.mode in {0,-1}:  # gradient evaluation required

                # Compute the derivatives of the objective function
                # For some reason SLSQP wants g dimensioned to n+1
                self.g[:-1] = jac(self.x)
                self.g[-1]  = 0

                self.num_gradient_evals +=1

                # Compute the normals of the constraints
                if self.num_equal_constr > 0:
                    for cidx, cjac in enumerate(constraint_jac):
                        self.a_eq[cidx,:-1] = cjac(self.x,*args)
                    self.a_eq[:,-1] = 0
                else:  # no equality constraint
                    self.a_eq.fill(0)  # [ 1 x n+1 ]
                
            mode = np.array( self.mode, int )
            acc = np.array(ftol, float)
            
            # Call SLSQP
            slsqp( self.num_equal_constr,   # total number of constraints
                   self.num_equal_constr,   # number of equal constraints
                   self.x,                  # function parameter for further evaluations
                   self.xl, self.xu,        # bounds
                   self.fx,                 # function evaluation 
                   self.c_eq,               # constraints evaluations
                   self.g,                  # gradient evaluation
                   self.a_eq,               # gradients of constraints
                   acc,                     # tolerance
                   majiter,                 # max iteration iter and iteration counter
                   mode,                    # mode in optimization iteration
                   self.w,                  # Workspape
                   self.jw,                 # Workspace
                   *list(self.internal) )

            self.mode = int(mode)
            self.majiter = int(majiter)

            if self.mode == 0:
                # Optimization terminated successfully 
                self.is_converged = True
                return False
            elif abs(self.mode) != 1:
                if bool(throw_on_error) is True:
                    raise SLSQPContextException(self.exit_modes[self.mode])

            if self.majiter > majiter_prev:
                # A major iteration finished
                num_iters -= 1
                self.iter_count += 1
            else:
                assert self.mode in {1,-1}

        return True


    # -----------------------------------------------------------------------------

    exit_modes = {-1: "Gradient evaluation required (g & a)",
                   0: "Optimization terminated successfully.",
                   1: "Function evaluation required (f & c)",
                   2: "More equality constraints than independent variables",
                   3: "More than 3*n iterations in LSQ subproblem",
                   4: "Inequality constraints incompatible",
                   5: "Singular matrix E in LSQ subproblem",
                   6: "Singular matrix C in LSQ subproblem",
                   7: "Rank-deficient equality constraint subproblem HFTI",
                   8: "Positive directional derivative for linesearch",
                   9: "Iteration limit exceeded"}


# -----------------------------------------------------------------------------

def numeric_scipy_version():
    return sum( [ int(x) * 100**p for (p,x) in enumerate(scipy.version.full_version.split(".")[::-1] ) ] )


def init_slsqp_internal():
    """take slsqp interface change due to bugfix into consideration: 
       https://github.com/scipy/scipy/commit/0e7bd3b789c4e4c8ce5517e4982ef6d354771f97
    """
    
    alpha = np.array(0, float)
    f0 = np.array(0, float)
    gs = np.array(0, float)
    h1 = np.array(0, float)
    h2 = np.array(0, float)
    h3 = np.array(0, float)
    h4 = np.array(0, float)
    t = np.array(0, float)
    t0 = np.array(0, float)
    tol = np.array(0, float)
    iexact = np.array(0, int)
    incons = np.array(0, int)
    ireset = np.array(0, int)
    itermx = np.array(0, int)
    line = np.array(0, int)
    n1 = np.array(0, int)
    n2 = np.array(0, int)
    n3 = np.array(0, int)
    
    return [ alpha, f0, gs, h1, h2, h3, h4, t, t0, tol,
             iexact, incons, ireset, itermx, line, 
             n1, n2, n3 ]
