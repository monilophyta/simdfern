# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["MinimizationContext"]

# -------------------------------------------------------------------------------------------------

import logging
import atexit

import typing as tp
import numpy as np
import scipy
from scipy.optimize import minimize as scipy_minimize
from scipy.optimize import OptimizeResult

# -------------------------------------------------------------------------------------------------
# global counting

GLOBAL_COUNTER_nfev = 0   # Number of evaluations of the objective functions
GLOBAL_COUNTER_njev = 0   # Number of evaluations of the Jacobian functions
GLOBAL_COUNTER_nhev = 0   # Number of evaluations of the Hessian functions
GLOBAL_COUNTER_nit  = 0   # Number of iterations performed by the optimizer.

# -------------------------------------------------------------------------------------------------
# logging

# create logger
def dump_count_stats():

    logger = logging.getLogger('logreg.minctxt')

    if __debug__:
        logger.setLevel(logging.INFO)
    else:
        logger.setLevel(logging.WARNING)

    logger.info( "GLOBAL_COUNTER_nit=%d", GLOBAL_COUNTER_nit  )
    logger.info( "GLOBAL_COUNTER_nfev=%d", GLOBAL_COUNTER_nfev )
    logger.info( "GLOBAL_COUNTER_njev=%d", GLOBAL_COUNTER_njev )
    logger.info( "GLOBAL_COUNTER_nhev=%d", GLOBAL_COUNTER_nhev )

atexit.register(dump_count_stats)

# -------------------------------------------------------------------------------------------------

class MinimizationContext(object):

    x0        : np.ndarray
    optim_res : tp.Optional[OptimizeResult] = None

    # scipy minimize optimization method
    optim_method   : str

    # optimization tolerance for convergence definition
    optim_tol      : tp.Optional[float] = None

    # optimization tolerance for convergence definition
    optim_gtol     : float = 1e-2


    @property
    def num_independent_variables(self) -> int:
        return int(self.x0.size)

    @property
    def is_converged(self) -> bool:
        if self.optim_res is None:
            return False
        return self.optim_res.success

    @property
    def iter_count(self) -> int:
        if self.optim_res is None:
            return 0
        return self.optim_res.nit
    

    @property
    def status_message(self) -> str:
        if self.optim_res is None:
            return "no optimization has run"
        return "%d (%s)" % (self.optim_res.status, self.optim_res.message)


    @property
    def x(self) -> np.ndarray:
        if self.optim_res is not None:
            return self.optim_res.x
        else:
            return self.x0


    def __init__(self, x0 ):
        self.x0 = np.asfarray(x0).flatten()
        assert np.all( np.isfinite(self.x0) )

        if numeric_scipy_version() >= 10000:
            self.optim_method = "trust-exact"
        else:
            self.optim_method = "Newton-CG"

        # Initialize the iteration counter and the mode value
        self.reinitialize()

    # -----------------------------------------------------------------------------

    def reinitialize(self):
        self.optim_res = None

    # -----------------------------------------------------------------------------

    def minimize( self, func : tp.Callable,
                         jac : tp.Callable,
                        hess : tp.Callable,
                        args : tuple = (),
               max_num_iters : int = 100,
                    callback : tp.Optional[tp.Callable] = None ):

        optim_options = { "disp": False,
                          "maxiter": max_num_iters }
        
        if self.optim_method == "Newton-CG":
            pass
            #optim_options["xtol"] = 1.e-4
        elif self.optim_method in {"trust-exact","BFGS", "trust-ncg"}:
            optim_options["gtol"] = self.optim_gtol

            if self.optim_method == {"trust-exact", "trust-ncg"}:
                optim_options["max_trust_radius"] = 100
            elif self.optim_method == "BFGS":
                hess = None
        
        optim_res = scipy_minimize( fun = func,
                                     x0 = self.x,
                                   args = args,
                                 method = self.optim_method,
                                    jac = jac,
                                   hess = hess,
                                    tol = self.optim_tol,
                               callback = callback,
                                options = optim_options )
        
        self.optim_res = optim_res

        # update global counter
        global GLOBAL_COUNTER_nfev
        global GLOBAL_COUNTER_njev
        global GLOBAL_COUNTER_nhev
        global GLOBAL_COUNTER_nit
        GLOBAL_COUNTER_nfev += optim_res.nfev
        GLOBAL_COUNTER_njev += optim_res.njev
        if hasattr(optim_res, 'nhev'):
            GLOBAL_COUNTER_nhev +=optim_res .nhev
        GLOBAL_COUNTER_nit  += optim_res.nit


# -----------------------------------------------------------------------------

def numeric_scipy_version():
    return sum( [ int(x) * 100**p for (p,x) in enumerate(scipy.version.full_version.split(".")[::-1] ) ] )
