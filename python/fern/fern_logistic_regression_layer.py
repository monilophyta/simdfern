# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["LogisticRegressionFernLayer"]


import typing as tp
from collections import Sequence
import numpy as np

from scipy.special import expit

from ..ctypes_py import WeightedFernLayer, project_features



class LogisticRegressionFernLayer( object ):

    scale  : float      = None
    __x    : np.ndarray = None


    @property
    def num_params(self):
        return int(self.__x.size)

    @property
    def num_weights(self):
        return self.num_params

    @property
    def num_features(self):
        return (self.num_params - 1)

    @property
    def params(self):
        return self.__x

    @property
    def weights(self):
        return self.__x
    
    @property
    def feature_weights(self):
        return self.__x[:-1]   # excluding offset


    @property
    def offset(self):
        return float(self.__x[-1])
    

    @property
    def dtype(self):
        return self.__x.dtype

    @property
    def constraint(self):
        sqr_norm = np.dot( self.feature_weights, self.feature_weights )
        return (sqr_norm - 1)

    @property
    def constraint_gradient( self ):
        return np.concatenate( ( 2 * self.feature_weights, [0] ), axis=0 )


    def __init__( self, params : np.ndarray, scale : float = 1.0 ):
        assert isinstance( params, np.ndarray )
        assert np.issubdtype( params.dtype, np.floating )
        assert scale > 0

        self.scale = float( scale )
        self.__x = params
    

    @classmethod
    def from_weighted_fern_layer( cls, layer : WeightedFernLayer, dtype, scale : float = 1.0 ):
        assert isinstance( layer, WeightedFernLayer )

        params = np.concatenate( [ layer.feature_weights.astype(dtype), 
                                    [-float(layer.feature_split) ] ], axis=0 )
        return cls( params.astype( dtype ), scale )


    def eval( self, feature_rows : Sequence ):
        assert isinstance( feature_rows, Sequence )
        assert len( feature_rows )  == self.num_features

        # project features
        faccum = project_features( self.feature_weights.astype(self.dtype), feature_rows )

        # add offset
        faccum += self.offset

        # scale
        if self.scale != 1:
            faccum *= self.scale

        # evaluate logistic function
        sigmoid_eval = expit( faccum, out=faccum )   # [ num_data ]
        return sigmoid_eval


    def eval_gradient( self, feature_rows : Sequence, 
                             sigmoid_eval : tp.Optional[np.ndarray] = None ):
        assert isinstance( feature_rows, Sequence )
        num_data = feature_rows[0].size

        if sigmoid_eval is None:
            sigmoid_eval = self.eval( feature_rows )
        assert isinstance( sigmoid_eval, np.ndarray )
        assert sigmoid_eval.ndim == 1
        assert sigmoid_eval.size == num_data
        
        gradient = np.empty( (self.num_weights, num_data), dtype=sigmoid_eval.dtype )
        if __debug__:
            gradient.fill( np.nan )

        # first fill with offset
        aux = np.multiply( sigmoid_eval, 1 - sigmoid_eval, out=gradient[-1,:] )

        # scale
        if self.scale != 1:
            aux *= self.scale
        
        # regular gradient
        for fidx in range(self.num_features):
            np.multiply( aux, feature_rows[fidx], out=gradient[fidx,:] )
        
        assert np.all( np.isfinite( gradient ) )
        return gradient  # [ num_weights x num_data ]
    

    def eval_jacobian( self, feature_rows    : Sequence, 
                             sigmoid_eval    : tp.Optional[np.ndarray] = None,
                             jacobian_coords : tp.Optional[np.ndarray] = None ):
        
        assert isinstance( feature_rows, Sequence )
        num_data = feature_rows[0].size

        if sigmoid_eval is None:
            sigmoid_eval = self.eval( feature_rows )
        assert isinstance( sigmoid_eval, np.ndarray )
        assert sigmoid_eval.ndim == 1
        assert sigmoid_eval.size == num_data

        # number of distinct items in triangular jacobian - including offset
        num_jacob_items = (self.num_weights * (self.num_weights + 1)) // 2

        if jacobian_coords is None:
            jacobian_coords = np.triu_indices( self.num_features+1 )
            jacobian_coords = np.stack( jacobian_coords, axis=0 )
        assert isinstance( jacobian_coords, np.ndarray )
        assert jacobian_coords.ndim == 2
        assert jacobian_coords.shape[0] == 2
        assert jacobian_coords.shape[1] == num_jacob_items

        jacobian = np.empty( (num_jacob_items, num_data), dtype=sigmoid_eval.dtype )   # [ num_jacob x num_data ]
        
        if __debug__:
            jacobian.fill( np.nan )
        
        for idx, (ridx,cidx) in enumerate( zip( jacobian_coords[0,:], jacobian_coords[1,:] ) ):
            assert max(ridx, cidx) < self.num_weights

            if ridx < self.num_features:
                X = feature_rows[ridx]
            else:
                X = 1
            
            if cidx < self.num_features:
                Y = feature_rows[cidx]
            else:
                Y = 1
            
            if (X is 1) and (Y is 1):
                jacobian[idx,:].fill( 1 )
            else:
                np.multiply( X, Y, out=jacobian[idx,:] )

        aux = 1 - (3*sigmoid_eval) + (2*sigmoid_eval*sigmoid_eval)    # [ num_data ]
        aux *= sigmoid_eval                                           # [ num_data ] * [ num_data ]
        
        # scale
        if self.scale != 1:
            aux *= (self.scale * self.scale)

        jacobian *= aux[np.newaxis,:]                                 # [ num_jacob x num_data ] * [ 1 x num_data ]

        assert np.all( np.isfinite( jacobian ) )

        return (jacobian, jacobian_coords)  # [ num_triu x num_data ]
