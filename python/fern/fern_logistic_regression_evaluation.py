# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["LogisticRegressionEvaluator"]

import logging
import atexit

from collections import OrderedDict, Sequence
import typing as tp
import numpy as np

from .fern_logistic_regression_layer import LogisticRegressionFernLayer
from .fern_logistic_regression_counts import LogisticRegressionStatCounter
from .fern_logistic_regression_optimization import EntropyOptimization


# -------------------------------------------------------------------------------------------------
# global counting

# initialize cache statistics
GLOBAL_COUNTER_evaluation_cache_mismatch = 0
GLOBAL_COUNTER_gradient_cache_mismatch = 0
GLOBAL_COUNTER_jacobian_cache_mismatch = 0

# -------------------------------------------------------------------------------------------------
# logging

# create logger
def dump_count_stats():

    logger = logging.getLogger('logreg.eval')

    if __debug__:
        logger.setLevel(logging.INFO)
    else:
        logger.setLevel(logging.WARNING)

    logger.info( "GLOBAL_COUNTER_evaluation_cache_mismatch=%d", GLOBAL_COUNTER_evaluation_cache_mismatch  )
    logger.info( "GLOBAL_COUNTER_gradient_cache_mismatch=%d", GLOBAL_COUNTER_gradient_cache_mismatch )
    logger.info( "GLOBAL_COUNTER_jacobian_cache_mismatch=%d", GLOBAL_COUNTER_jacobian_cache_mismatch )

atexit.register(dump_count_stats)

# -------------------------------------------------------------------------------------------------


class LogisticRegressionEvaluator(object):


    default_dtype  = np.float32

    # pseudo count in entropy statistics
    eps            : float

    dtype          : np.dtype

    feature_rows   : Sequence

    stat_counter   : LogisticRegressionStatCounter

    scale          : float

    do_evaluate_gradient : bool
    do_evaluate_jacobian : bool

    evaluation_cache : OrderedDict
    max_cache_size   : int


    @property
    def num_labels(self) -> int:
        return self.stat_counter.num_labels

    @property
    def num_leafs(self) -> int:
        return self.stat_counter.num_leafs

    @property
    def num_data(self) -> int:
        return self.stat_counter.flat_count_coords.size


    @property
    def num_features(self) -> int:
        return len(self.feature_rows)
    
    @property
    def num_weights(self) -> int:
        return (self.num_features + 1)


    def __init__(self, feature_rows : Sequence,
                          label_idx : np.ndarray,
                           leaf_idx : np.ndarray,
                     sample_weights : tp.Optional[np.ndarray] = None,
                         num_labels : int = None,
                          num_leafs : int = None,
                              scale : float = 1.0,
                                eps : float = 1e-4,
                      eval_gradient : bool = True,
                      eval_jacobian : bool = False,
                     max_cache_size : int = 10,
                              dtype = default_dtype ):
        
        assert isinstance( label_idx, np.ndarray )
        assert np.issubdtype( label_idx.dtype, np.int32 )
        assert np.all( label_idx >= 0)

        if num_labels is None:
            num_labels = int(label_idx.max() + 1)
        else:
            num_labels = int( num_labels )
            assert np.all( label_idx < num_labels )

        assert isinstance( leaf_idx, np.ndarray )
        assert np.issubdtype( leaf_idx.dtype, np.int32 )
        assert np.all( leaf_idx >= 0 )

        if num_leafs is None:
            num_leafs = int(leaf_idx.max() + 1)
        else:
            num_leafs = int( num_leafs )
            assert np.all( leaf_idx < num_leafs )

        assert label_idx.size == leaf_idx.size

        assert isinstance( feature_rows, Sequence )
        assert np.all( leaf_idx.size == np.array( [ feature.size for feature in feature_rows ] ) )
        
        self.dtype = np.dtype( dtype )

        if sample_weights is not None:
            assert isinstance( sample_weights, np.ndarray )
            assert sample_weights.size == leaf_idx.size

        # initializer statistic counter
        self.stat_counter = LogisticRegressionStatCounter( label_idx = label_idx,
                                                            leaf_idx = leaf_idx,
                                                      sample_weights = sample_weights,
                                                          num_labels = num_labels,
                                                           num_leafs = num_leafs )

        self.feature_rows = feature_rows

        assert scale >= 1
        self.scale = float(scale)

        assert eps > 0
        self.eps = float(eps)

        self.evaluation_cache = OrderedDict()
        assert max_cache_size >= 1
        self.max_cache_size = max( 1, int(max_cache_size) )

        self.do_evaluate_gradient = bool( eval_gradient )
        self.do_evaluate_jacobian = bool( eval_jacobian )


    def eval( self, params : np.ndarray ) -> float:
        assert isinstance( params, np.ndarray )
        assert params.size == self.num_weights

        eval_tuple = self.full_evaluation( params, True, self.do_evaluate_gradient, self.do_evaluate_jacobian )
        return eval_tuple[0]

    
    def gradient( self, params : np.ndarray ) -> np.ndarray:
        assert isinstance( params, np.ndarray )
        assert params.size == self.num_weights

        eval_tuple = self.full_evaluation( params, False, True, self.do_evaluate_jacobian )
        return eval_tuple[1]


    def hessian( self, params : np.ndarray ) -> np.ndarray:
        assert isinstance( params, np.ndarray )
        assert params.size == self.num_weights

        eval_tuple = self.full_evaluation( params, False, self.do_evaluate_gradient, True )

        hessian = np.zeros( (self.num_features+1, self.num_features+1), dtype=self.dtype )
        jacob_coords = eval_tuple[3]
        hessian[jacob_coords[0,:], jacob_coords[1,:]] = eval_tuple[2]
        hessian[jacob_coords[1,:], jacob_coords[0,:]] = eval_tuple[2]
        
        return hessian
    

    def l2norm_eval( self, params : np.ndarray ) -> float:
        assert isinstance( params, np.ndarray )
        assert params.size == self.num_weights

        params = params[:-1]
        sqr_norm = np.dot( params, params )
        return sqr_norm


    def l2norm_gradient( self, params : np.ndarray ) -> np.ndarray:
        assert isinstance( params, np.ndarray )
        assert params.size == self.num_weights
        return np.concatenate( ( 2 * params[:-1], [0] ), axis=0 )


    def l2norm_hessian( self, params : np.ndarray )-> np.ndarray:
        hess = np.empty( (self.num_weights,), dtype=self.dtype )
        
        if __debug__:
            hess.fill(np.nan)

        hess[:-1] = 2
        hess[-1] = 0

        assert np.all( np.isfinite(hess) )
        return np.diag( hess )


    def constraint_eval( self, params : np.ndarray ) -> float:
        return (self.l2norm_eval(params) - 1)
    

    def constraint_gradient( self, params : np.ndarray ) -> np.ndarray:
        return self.l2norm_gradient( params )
    

    def constraint_hessian( self, params : np.ndarray )-> np.ndarray:
        return self.l2norm_hessian(params)
    

    def do_cache( self, params : np.ndarray, eval_tuple : tuple ):
        key = params.tobytes()

        if len(self.evaluation_cache) == self.max_cache_size:
            self.evaluation_cache.popitem( last=False )
        self.evaluation_cache[ key ] = eval_tuple
    

    def get_cached( self, params : np.ndarray):
        key = params.tobytes()
        return self.evaluation_cache.get(key, 4 * (None,) )


    def full_evaluation(self, params : np.ndarray,
                           calc_eval : bool,
                       calc_gradient : bool,
                       calc_jacobian : bool ) -> tuple:
        
        calc_eval     = bool( calc_eval )
        calc_gradient = bool( calc_gradient )
        calc_jacobian = bool( calc_jacobian )

        # create logistic regression layer
        layer = LogisticRegressionFernLayer( params=params.astype(self.dtype), scale=self.scale )

        # check cache for precomputed values
        stat_dict, optim_val, d_optim_val, dd_optim_val = self.get_cached( params )

        if stat_dict is None:
            # calculate evaluation relevant statistics
            stat_dict = self.calc_base_statistics( layer )
        
        global GLOBAL_COUNTER_evaluation_cache_mismatch
        global GLOBAL_COUNTER_gradient_cache_mismatch
        global GLOBAL_COUNTER_jacobian_cache_mismatch

        if "grd_N_left_ml" not in stat_dict:
            if (calc_gradient or calc_jacobian) is True:
                GLOBAL_COUNTER_gradient_cache_mismatch += 1
                stat_dict.update( self.calc_gradient_statistics( layer, stat_dict["layer_eval"] ) )
        
        if (calc_jacobian is True) and ("jacob_N_left_ml" not in stat_dict):
            GLOBAL_COUNTER_jacobian_cache_mismatch += 1
            stat_dict.update( self.calc_jacobian_statistics( layer, stat_dict["layer_eval"] ) )
            
        # instance for entropy and gradient evaluation
        entropy_optimization = EntropyOptimization( eps=self.eps, **stat_dict )

        # get optimization value
        if (calc_eval is True) and (optim_val is None):
            GLOBAL_COUNTER_evaluation_cache_mismatch += 1
            optim_val = entropy_optimization.calc_value()
        

        # get derivatives, if required        
        calc_derivatives = (calc_gradient is True) and ( d_optim_val is None) or \
                           (calc_jacobian is True) and (dd_optim_val is None)

        if calc_derivatives is True:
            d_optim_val, dd_optim_val = entropy_optimization.calc_derivatives( **stat_dict )

        # cache result
        self.do_cache( params, (stat_dict, optim_val, d_optim_val, dd_optim_val) )

        # return results
        if not (calc_gradient or calc_jacobian):
            return (optim_val,)
        
        if calc_jacobian is False:
            return (optim_val, d_optim_val)
        
        return (optim_val, d_optim_val, dd_optim_val, stat_dict["jacob_coords"])



    def calc_base_statistics( self, layer : LogisticRegressionFernLayer ) -> dict:
        # evaluate layer
        layer_eval = layer.eval( self.feature_rows )  # [ num_data ]
        assert layer_eval.size == self.num_data

        # left and right side evaluation statistic
        N_left_ml  = self.stat_counter.count1d( layer_eval )   # [ num_leafs x num_labels ]
        N_right_ml = self.stat_counter.count1d( 1 - layer_eval )   # [ num_leafs x num_labels ]
        assert N_left_ml.ndim == 2
        assert N_left_ml.shape[0] == self.num_leafs
        assert N_left_ml.shape[1] == self.num_labels
        assert N_left_ml.shape == N_right_ml.shape

        stat_dict = { "layer_eval" : layer_eval, \
                       "N_left_ml" : N_left_ml, \
                      "N_right_ml" : N_right_ml }
        
        return stat_dict
    

    def calc_gradient_statistics( self, layer : LogisticRegressionFernLayer,
                                   layer_eval : np.ndarray ) -> dict:
        
        # evaluate gradient
        gradient_eval = layer.eval_gradient( self.feature_rows, layer_eval ) # [ num_features+1 x num_data ]
        assert gradient_eval.shape[0] == (self.num_features+1)
        assert gradient_eval.shape[1] == self.num_data

        # left side gradient statistic
        grd_N_left_ml = self.stat_counter.count2d( gradient_eval )    # [ num_features+1 x num_leafs x num_labels ]
        assert grd_N_left_ml.ndim == 3
        assert grd_N_left_ml.shape[0] == (self.num_features+1)
        assert grd_N_left_ml.shape[1] == self.num_leafs
        assert grd_N_left_ml.shape[2] == self.num_labels
        #grd_N_left_m = grd_N_left_ml.sum(axis=-1)     # [ num_features+1 x num_leafs ]
        
        return { "grd_N_left_ml" : grd_N_left_ml }


    def calc_jacobian_statistics( self, layer : LogisticRegressionFernLayer,
                                   layer_eval : np.ndarray ) -> dict:
        
        # evaluate jacobian
        jacob_eval, jacob_coords = layer.eval_jacobian( self.feature_rows, layer_eval ) # [ ((num_features+1) * (num_features+2))/2 x num_data ]
        assert jacob_eval.ndim == 2
        assert jacob_eval.shape[0] == ((self.num_features+1) * (self.num_features+2)) // 2
        assert jacob_eval.shape[1] == self.num_data

        # left side jacobien statistic
        jacob_N_left_ml = self.stat_counter.count2d( jacob_eval )    # [ (num_features+1 * num_features+2)/2 x num_leafs x num_labels ]
        assert jacob_N_left_ml.ndim == 3
        assert jacob_N_left_ml.shape[0] == ((self.num_features+1) * (self.num_features+2)) // 2
        assert jacob_N_left_ml.shape[1] == self.num_leafs
        assert jacob_N_left_ml.shape[2] == self.num_labels

        return { "jacob_N_left_ml" : jacob_N_left_ml, \
                    "jacob_coords" : jacob_coords  }


    def approximate_derivatives( self, params : np.ndarray, delta : tp.Optional[float] = None ) -> tuple:
        """
        Mainly a tool for debugging
        """
        if delta is None:
            delta = np.power( np.finfo( self.dtype ).resolution, 1./4 )
            #delta = 0.000001
            #delta = np.finfo( np.float64 ).resolution

        local_eval = self.full_evaluation( params, True, False, False )[0]

        one_side_steps = np.empty( (2, self.num_weights), dtype=self.dtype )
        two_side_steps = np.empty( (2, self.num_weights, self.num_weights), dtype=self.dtype )

        if __debug__:
            one_side_steps.fill( np.nan )
            two_side_steps.fill( np.nan )

        for didx, perturbation in enumerate( (-1 * delta, delta) ):
            for widx in range( self.num_weights ):
                
                wparams = params.copy()
                wparams[widx] += perturbation
                one_side_steps[didx,widx] = self.full_evaluation( wparams, True, False, False )[0]

                for vidx in range(widx+1):
                    vparams = wparams.copy()
                    vparams[vidx] += perturbation
                    two_side_steps[didx,widx,vidx] = self.full_evaluation( vparams, True, False, False )[0]
                    two_side_steps[didx,vidx,widx] = two_side_steps[didx,widx,vidx]
        
        approx_gradient = (one_side_steps[1,:] - one_side_steps[0,:]) * (0.5 / delta)
        
        aux = one_side_steps.sum(axis=0)
        approx_hessian = two_side_steps.sum(axis=0) - np.add.outer( aux, aux )
        approx_hessian += 2*local_eval
        approx_hessian *= 0.5 * np.power( delta, -2 )

        assert np.all( np.isfinite( approx_hessian ) )
        assert np.all( np.isfinite( approx_gradient ) )

        return (approx_gradient, approx_hessian)


