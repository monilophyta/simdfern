# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["evaluate_layer_contributions"]


import numpy as np
from .fern_layer_reordering import remove_layer

from ..clmath import mutual_counts



def evaluate_layer_contributions( labels, nodeIdx, data_weights=None, num_labels=None, num_layers=None, measure="gini" ):
    """
    Parameters
    ----------
    labels : int32 ndarray [ len=N ]
    nodeIdx : int32 ndarray [ len=N ]
    data_weights : None or int32 ndarray [ len=N ]
    num_labels : int or None
    num_layers : int or None
    measure : "gini" or "mutual"

    Returns
    -------
    float32 array [ len=num_layers ]
        ret[i] is measure(full_tree) - measure( tree - layer i )
    """

    assert isinstance( labels, np.ndarray )
    assert np.issubdtype( labels.dtype, np.integer )
    assert isinstance( nodeIdx, np.ndarray )
    assert np.issubdtype( nodeIdx.dtype, np.integer )
    
    assert labels.size == nodeIdx.size
    assert (data_weights is None) or (isinstance( data_weights, np.ndarray) )
    assert (data_weights is None) or (np.issubdtype( data_weights.dtype, np.integer) )
    assert (data_weights is None) or (data_weights.size == labels.size)

    if num_labels is None:
        num_labels = labels.max() + 1
    else:
        assert num_labels >= (labels.max() + 1)
    
    abs_nodeIdx = np.absolute( nodeIdx )

    if num_layers is None:
        num_layers = int( np.ceil( np.log2( abs_nodeIdx.max() + 1) ) )
    
    assert abs_nodeIdx.max() < (2**num_layers)
    
    # preparing measure
    pre_measure = lambda nidx, num_N: mutual_counts( nidx, labels, weights=data_weights, num_X=num_N, num_Y = num_labels )

    if measure.lower() == "gini":
        measure = lambda nidx, num_L: pre_measure(nidx, 1 << num_L ).gini_index( axis=1, do_norm=False )
    elif measure.lower() == "mutual":
        measure = lambda nidx, num_L: pre_measure(nidx, 1 << num_L ).mutual_information()
    else:
        assert( False )

    # measure full tree
    full_tree_measure = measure( nodeIdx, num_layers )

    # loop over all layers
    layer_measures = [ measure( remove_layer( abs_nodeIdx, lidx, num_layers=num_layers ), num_layers-1 ) \
                       for lidx in range(num_layers) ]
    layer_measures = np.array( layer_measures )

    assert np.all( layer_measures >= 0 )
    assert np.all( layer_measures <= full_tree_measure )

    # return differences
    return (full_tree_measure - layer_measures)
