# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["FernLeafHistograms"]

from collections import deque
import numpy as np

from ..tree import FullLeafHistograms
from .fern_layer_reordering import remove_layer, select_layers
from ..clmath import mutual_counts


class FernLeafHistograms( FullLeafHistograms ):


    def __init__(self, *args, **kwargs ):
        super().__init__( *args, **kwargs )



    def optimize_layer_order( self, smallest_contribution_first: bool ):
        """
        Attempts to provide optimal layer order for pruning
        """
        # mask of non empty (relevant) leafs
        leaf_idx = np.flatnonzero( self.histograms.max(axis=1) > 0 ).astype( np.int32 )  # [ nR <= num_leafs ]

        # Most likely label for each leaf node
        leaf_labels = np.argmax( self.histograms, axis=1 )                               # [ num_leafs ]
        leaf_labels = leaf_labels[ leaf_idx ]                                            # [ nR <= num_leafs ]

        assert leaf_idx.size == leaf_labels.size

        # list of original layer indices
        orig_layer_idx = deque( range( self.num_layers ) )                               # [ num_layers ]

        # list of new layer indices
        n_layer_idx = list()

        # counting layers backwards
        for nLayers in range( self.num_layers, 1, -1 ):
            
            # Generate ferns with one removed layer
            reduced_fern_idx = [ remove_layer( leaf_idx, lidx, nLayers ) for lidx in range( nLayers ) ]     # num_layers x [ num_leafs ]

            # Evaluate ferns with removed layer
            fern_evals = [ mutual_counts( rIdx, leaf_labels, num_X=2**(nLayers-1), num_Y=self.num_labels ).mutual_information() for rIdx in reduced_fern_idx ]     # num_layers x [ num_leafs ]
            fern_evals = np.array( fern_evals, dtype=np.float32 )

            if smallest_contribution_first is True:
                # Fern idx with lowest mutual information contribution (highest actual mutual information)
                sel_mutual_contrib_idx = fern_evals.argmin()
            else:
                # Fern idx with largest mutual information contribution (lowest actual mutual information)
                sel_mutual_contrib_idx = fern_evals.argmax()

            # commit layer indices with lowest contribution (max mutual inf when removed)
            n_layer_idx.append( orig_layer_idx[sel_mutual_contrib_idx] )
            del orig_layer_idx[sel_mutual_contrib_idx]
            
            # apply leaf indices for next loop
            leaf_idx = reduced_fern_idx[ sel_mutual_contrib_idx ]
        
        # one layer should be left
        assert len(orig_layer_idx) == 1
        assert np.all(leaf_idx <= 1)

        # final (new) order of layers
        n_layer_idx.append( orig_layer_idx[0] )
        assert len(frozenset(n_layer_idx)) == self.num_layers
        n_layer_idx = np.array( n_layer_idx, dtype=np.int32 )
        
        return n_layer_idx



    def reorder_histograms( self, layer_idx ):
        """
        Parameters
        ----------
        layer_idx : int32 array [ len = num_layers ]
        
        Returns
        -------
        new instance of FernLeafHistograms
        """

        assert layer_idx.size == self.num_layers
        assert (layer_idx.max()+1) == self.num_layers
        
        leaf_idx = np.arange( self.num_leafs, dtype=np.int32 )      # [ num_leafs ]
        
        # reorder leaf indices 
        n_leaf_idx = select_layers( leaf_idx, layer_idx, num_layers = self.num_layers ) # [ num_leafs ]
        assert np.unique( n_leaf_idx ).size == np.unique( leaf_idx ).size

        # leaf index remapping
        leaf_idx_remap = np.empty( self.num_leafs, dtype=np.int32 )

        if __debug__:
            leaf_idx_remap.fill( np.iinfo( leaf_idx_remap.dtype ).max )

        leaf_idx_remap[n_leaf_idx] = leaf_idx

        # remap histogram
        n_histograms = self.histograms.take( leaf_idx_remap, axis=0 )  # [ num_leafs x num_labels ]

        return FernLeafHistograms( n_histograms )











