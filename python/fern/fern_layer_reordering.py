# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["swap_layers", "select_layers", "remove_layer", \
           "select_bits", "swap_bits", "remove_bit" ]


import numpy as np
import warnings

# definitions due to bugs in some numpy version
# these values are set in handle_buggy_numpy
left_bit_shift  = None
right_bit_shift = None
bitwise_and     = None
bitwise_or      = None
bitwise_not     = None



def swap_layers( nodeIdx, lidx1, lidx2, num_layers ):
    """
    Parameters
    ----------
    nodeIdx : int32 ndarray [ len=N ]
    lidx1 :  int, 0<=, <31
    lidx2 :  int, 0<=, <31
    num_layers: int32

    Returns
    -------
    int32 array [ len=N ]
        layer bits lidx1 and lidx2 are swapped

    
    Relation bit index (bidx) and layer index (lidx):
    bidx = num_layers - lidx
    """
    assert np.issubdtype( nodeIdx.dtype, np.int32 )
    abs_node_idx = np.absolute( nodeIdx )
    assert num_layers >= int( np.ceil( np.log2(abs_node_idx.max() + 1) ) )
    
    max_layer = int(num_layers) - 1
    bidx1 = max_layer - lidx1
    bidx2 = max_layer - lidx2

    n_nodeIdx = swap_bits( abs_node_idx, bidx1, bidx2 )
    n_nodeIdx[ np.signbit( nodeIdx ) ] *= np.int32(-1)

    return n_nodeIdx


def select_layers( nodeIdx, layer_indices, num_layers : int ):
    """
    Parameters
    ----------
    nodeIdx : int32 ndarray [ len=N ]
    layer_indices : int array: 0 <= i < 31, len < 31
    num_layers: int
    
    Returns
    -------
    int32 array [ len=N ]
        where layer_bits are reordered according to layer_indices

    Relation bit index (bidx) and layer index (lidx):
    bidx = num_layers - lidx
    """
    assert np.issubdtype( nodeIdx.dtype, np.int32 )
    abs_node_idx = np.absolute( nodeIdx )

    #if num_layers is None:
    #    num_layers = int( np.ceil( np.log2( abs_node_idx.max() ) ) )
    assert num_layers >= int( np.ceil( np.log2(abs_node_idx.max() + 1) ) )

    max_layer = int(num_layers) - 1
    
    bit_indices = max_layer - np.asarray( layer_indices[::-1], dtype=np.int32 )

    n_nodeIdx = select_bits( abs_node_idx, bit_indices )
    n_nodeIdx[ np.signbit( nodeIdx ) ] *= np.int32(-1)
    
    assert np.issubdtype( n_nodeIdx.dtype, np.int32 )
    return n_nodeIdx



def remove_layer( nodeIdx, lidx, num_layers ):
    """
    Parameters
    ----------
    nodeIdx : int32 ndarray [ len=N ]
    lidx : int, 0<=, <31
    num_layers: int32

    Returns
    -------
    int32 array [ len=N ]
        layer bits lidx1 and lidx2 are swapped

    
    Relation bit index (bidx) and layer index (lidx):
    bidx = num_layers - lidx
    """
    assert np.issubdtype( nodeIdx.dtype, np.int32 )
    
    abs_node_idx = np.absolute( nodeIdx )

    #if num_layers is None:
    #    num_layers = int( np.ceil( np.log2(abs_node_idx.max() + 1) ) )
    #else:
    assert num_layers >= int( np.ceil( np.log2(abs_node_idx.max() + 1) ) )
    
    max_layer = int(num_layers) - 1
    bidx = max_layer - lidx

    n_nodeIdx = remove_bit( abs_node_idx, bidx )
    n_nodeIdx[ np.signbit( nodeIdx ) ] *= np.int32(-1)
    
    assert np.issubdtype( n_nodeIdx.dtype, np.int32 )
    return n_nodeIdx


# ---------------------------------------------------------------------------------

def swap_bits( nodeIdx, bidx1, bidx2 ):
    """
    Parameters
    ----------
    nodeIdx : int32 ndarray [ len=N ]
    bidx1 : bidx1 : int, 0<=, <31
    bidx2 : bidx2 : int, 0<=, <31
    
    Returns
    -------
    int32 array [ len=N ]
        where the bits bidx1 and bidx2 are swapped
    """
    assert np.issubdtype( nodeIdx.dtype, np.int32 )
    assert nodeIdx.min() >= 0
    assert isinstance( bidx1, int )
    assert isinstance( bidx2, int )
    assert (bidx1 >= 0) and (bidx1 < 31)
    assert (bidx2 >= 0) and (bidx2 < 31)

    if bidx1 == bidx2:
        return nodeIdx

    mask1 = np.int32( 1 << bidx1 )
    mask2 = np.int32( 1 << bidx2 )

    # template around the two bits
    mask_neg = bitwise_not( bitwise_or( mask1, mask2 ) )    

    # prepare shift functions
    shift_1to2_val = bidx2 - bidx1
    shift_2to1_val = bidx1 - bidx2
    assert (shift_1to2_val * shift_2to1_val) < 0

    if shift_1to2_val < 0:
        shift_1to2 = lambda x: right_bit_shift( x, -shift_1to2_val ).astype(np.int32)
        shift_2to1 = lambda x: left_bit_shift(  x,  shift_2to1_val ).astype(np.int32)
    else:
        shift_1to2 = lambda x: left_bit_shift(  x,  shift_1to2_val ).astype(np.int32)
        shift_2to1 = lambda x: right_bit_shift( x, -shift_2to1_val ).astype(np.int32)

    # get the swapped bits
    swapped_bits12 = shift_1to2( bitwise_and( nodeIdx, mask1 ) )
    swapped_bits21 = shift_2to1( bitwise_and( nodeIdx, mask2 ) )
    
    swapped_bits = bitwise_or( swapped_bits12, swapped_bits21 )

    # reintegration of bits
    n_nodeIdx = bitwise_and( nodeIdx, mask_neg, out=swapped_bits21 )    # bidx1 and bidx2 are deleted
    n_nodeIdx = bitwise_or( n_nodeIdx, swapped_bits, out=swapped_bits12 )

    assert np.issubdtype( n_nodeIdx.dtype, nodeIdx.dtype )
    return n_nodeIdx

# ---------------------------------------------------------------------------------


def select_bits( nodeIdx, bit_indices ):
    """
    Parameters
    ----------
    nodeIdx : int32 ndarray [ len=N ]
    bit_indices : int array: 0 <= i < 31, len < 31
    
    Returns
    -------
    int32 array [ len=N ]
        where bits are reordered according to bit_indices
    """
    assert np.issubdtype( nodeIdx.dtype, np.int32 )
    assert nodeIdx.min() >= 0
    assert len( bit_indices ) < 31
    assert np.all( np.asarray( bit_indices ) >= 0 )
    assert np.all( np.asarray( bit_indices ) < 31 )

    if len(bit_indices) == 0:
        return nodeIdx
    
    mask0 = np.int32( 1 )

    # initialize with last layer index
    n_nodeIdx = bitwise_and( right_bit_shift( nodeIdx, bit_indices[-1], dtype=np.int32 ), mask0 )

    down_shifts = np.asarray( bit_indices[-2::-1], dtype=np.int32 )  # reverse order, skipping last index

    for ds in down_shifts:
        n_nodeIdx = left_bit_shift( n_nodeIdx, 1 )  # shift everything up

        # prepare bit for next layer
        if ds > 0:
            bit = bitwise_and( right_bit_shift( nodeIdx, ds ), mask0 )  # (x >> d) & 1
        else:
            bit = bitwise_and( nodeIdx, mask0 )  # (x >> d) & 1
        
        # add next bit
        n_nodeIdx = bitwise_or( n_nodeIdx, bit )
    
    assert np.issubdtype( n_nodeIdx.dtype, nodeIdx.dtype )
    return n_nodeIdx

# ---------------------------------------------------------------------------------



def remove_bit( nodeIdx, bidx ):
    """
    Parameters
    ----------
    nodeIdx : int32 ndarray [ len=N ]
    bidx : int, 0<=, <31
    
    Returns
    -------
    int32 array [ len=N ]
        where the bit bidx has been removed
    """

    assert np.issubdtype( nodeIdx.dtype, np.int32 )
    assert nodeIdx.min() >= 0
    assert isinstance( bidx, (int, np.integer) )
    assert (bidx >= 0) and (bidx < 31)

    if bidx == 0:
        res = right_bit_shift( nodeIdx, 1 )
        assert np.issubdtype( res.dtype, nodeIdx.dtype )
        return res

    lower_mask = np.int32( (1 << bidx) - 1 )
    full_mask = np.int32( (1 << 31) - 1 )
    up_mask = full_mask - np.int32( (1 << (bidx+1)) - 1 )
    assert( (up_mask + lower_mask + (1 << bidx)) == full_mask )

    # take upper bits
    up_bits = bitwise_and( nodeIdx, up_mask )  

    # shift down
    up_bits = right_bit_shift( up_bits, 1 )

    # take lower bits
    lower_bits = bitwise_and( nodeIdx, lower_mask ) 

    # combine lower and upper bits
    res = bitwise_or( lower_bits, up_bits )
    
    assert np.issubdtype( res.dtype, nodeIdx.dtype )
    return res


# ----------------------------------------------------------------------------------

def handle_buggy_numpy():
    """Workaround buggy numpy version
        Bug: after using some bitwise function dtype still has name 
             "int32" but the type does not match int32 anymore"""
    
    global left_bit_shift
    global right_bit_shift
    global bitwise_and
    global bitwise_or
    global bitwise_not
    
    test_array = np.arange( 16, dtype=np.int32 )

    check = np.left_shift( test_array, np.int32(1) )
    if np.issubdtype( check.dtype, np.int32 ):
        left_bit_shift = np.left_shift
    else:
        warnings.warn("buggy numpy.left_shift")
        left_bit_shift = lambda *args, **argv: np.left_shift(*args, **argv).astype(np.int32)

    check = np.right_shift( test_array, np.int32(1) )
    if np.issubdtype( check.dtype, np.int32 ):
        right_bit_shift = np.right_shift
    else:
        warnings.warn("buggy numpy.right_shift")
        right_bit_shift = lambda *args, **argv: np.right_shift(*args, **argv).astype(np.int32)

    check = np.bitwise_and( test_array, np.int32(1) )
    if np.issubdtype( check.dtype, np.int32 ):
        bitwise_and = np.bitwise_and
    else:
        warnings.warn("buggy numpy.bitwise_and")
        bitwise_and = lambda *args, **argv: np.bitwise_and(*args, **argv).astype(np.int32)

    check = np.bitwise_or( test_array, np.int32(1) )
    if np.issubdtype( check.dtype, np.int32 ):
        bitwise_or = np.bitwise_or
    else:
        warnings.warn("buggy numpy.bitwise_or")
        bitwise_or = lambda *args, **argv: np.bitwise_or(*args, **argv).astype(np.int32)

    check = np.bitwise_not( test_array )
    if np.issubdtype( check.dtype, np.int32 ):
        bitwise_not = np.bitwise_not
    else:
        warnings.warn("buggy numpy.bitwise_not")
        bitwise_not = lambda *args, **argv: np.bitwise_not(*args, **argv).astype(np.int32)

handle_buggy_numpy()
