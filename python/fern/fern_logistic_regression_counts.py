# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



__all__ = ["LogisticRegressionStatCounter"]


import typing as tp
import numpy as np



class LogisticRegressionStatCounter( object ):

    sample_weights    : tp.Optional[np.ndarray]
    
    num_labels        : int
    num_leafs         : int

    flat_count_coords : np.ndarray # [ N ]


    @property
    def num_data(self) -> int:
        return self.flat_count_coords.size


    def __init__(self, label_idx      : np.ndarray,
                       leaf_idx       : np.ndarray,
                       sample_weights : tp.Optional[np.ndarray] = None,
                       num_labels     : int = None,
                       num_leafs      : int = None ):
        
        assert isinstance( label_idx, np.ndarray )
        assert np.issubdtype( label_idx.dtype, np.int32 )
        assert isinstance( leaf_idx, np.ndarray )
        assert np.issubdtype( leaf_idx.dtype, np.int32 )
        assert label_idx.size == leaf_idx.size

        if num_labels is None:
            num_labels = label_idx.max() + 1
        else:
            assert num_labels > 0
        
        if num_leafs is None:
            num_leafs = leaf_idx.max() + 1
        else:
            assert num_leafs > 0

        if sample_weights is not None:
            assert isinstance( sample_weights, np.ndarray )
            assert sample_weights.size == leaf_idx.size
        self.sample_weights = sample_weights
        
        self.num_labels = int(num_labels)
        self.num_leafs  = int(num_leafs)
        self.flat_count_coords = np.ravel_multi_index( (leaf_idx,label_idx), (self.num_leafs, self.num_labels) )  # [ N ]


    def __simple_count( self, weights : np.ndarray ) -> np.ndarray:
        dtype = weights.dtype
        minlength = self.num_leafs * self.num_labels

        if self.sample_weights is not None:
            weights = np.multiply( self.sample_weights, weights, dtype=dtype )

        counts = np.bincount( self.flat_count_coords, weights=weights, minlength=minlength )  # [minlength]
        return counts.astype( dtype )


    def count1d( self, weights : np.ndarray ) -> np.ndarray:
        assert isinstance( weights, np.ndarray )
        assert weights.ndim == 1
        assert weights.size == self.num_data

        counts = self.__simple_count( weights )
        counts = counts.reshape( (self.num_leafs, self.num_labels) )
        return counts


    def count2d( self, weights : np.ndarray ) -> np.ndarray:
        assert isinstance( weights, np.ndarray )
        assert weights.ndim == 2
        assert weights.shape[1] == self.num_data

        n_dims = weights.shape[0]
        counts = np.stack( [ self.__simple_count( weights[didx,:] ) for didx in range(n_dims) ], axis=0 )  # [ F x num_leaf x num_labels ]
        counts = counts.reshape( (n_dims, self.num_leafs, self.num_labels) )
        return counts


    def count3d( self, weights : np.ndarray ) -> np.ndarray:
        assert isinstance( weights, np.ndarray )
        assert weights.ndim == 3
        assert weights.shape[2] == self.num_data

        ndim1 = weights.shape[0]
        ndim2 = weights.shape[1]

        counts = self.count2d( weights.reshape( ( ndim1*ndim2, self.num_data ) ) )
        counts = counts.reshape( (ndim1, ndim2, self.num_leafs, self.num_labels) )
        return counts

