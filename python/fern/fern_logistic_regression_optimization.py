# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = [ "EntropyOptimization"]


import typing as tp
import numpy as np



class EntropyOptimization( object ):

    eps       = None

    N_left_m  = None    # [ num_leafs ]
    N_right_m = None    # [ num_leafs ]

    N_left_ml  = None    # [ num_leafs x num_labels ]
    N_right_ml = None    # [ num_leafs x num_labels ]

    N_left_m_eps   = None   # [ num_leafs ]
    N_left_ml_eps  = None   # [ num_leafs ]
    N_right_m_eps  = None   # [ num_leafs ]
    N_right_ml_eps = None   # [ num_leafs ]

    log_N_left_ml = None    # [ num_leafs x num_labels ]
    log_N_left_m  = None    # [ num_leafs ]

    log_N_right_ml = None   # [ num_leafs x num_labels ]
    log_N_right_m  = None   # [ num_leafs ]


    @property
    def dtype(self):
        return self.N_left_m.dtype
    

    @property
    def num_leafs(self):
        return self.N_left_m.size
    
    @property
    def num_labels(self):
        return self.N_left_ml.shape[1]


    def __init__( self,   N_left_ml : np.ndarray, 
                         N_right_ml : np.ndarray,
                                eps : float = 0.5,
                        **other_args ):
        """
        Parameters
        ----------
        N_left_ml       : float ndarray [ num_leafs x num_labels ]
        N_right_ml      : float ndarray [ num_leafs x num_labels ]
        eps             : float
            pseudo count for numarical safty and gradient sedation
        **other_args    : ignored parameters
        Returns
        -------
        optim_val    : float
        """
        assert N_left_ml.shape == N_right_ml.shape
        assert N_left_ml.ndim == 2
        assert N_right_ml.ndim == 2
        assert eps >= 0

        self.eps = eps

        self.N_left_m   = N_left_ml.sum( axis=1 )     # [ num_leafs ]
        self.N_right_m  = N_right_ml.sum( axis=1 )    # [ num_leafs ]
        self.N_left_ml  = N_left_ml                   # [ num_leafs x num_labels ]
        self.N_right_ml = N_right_ml                  # [ num_leafs x num_labels ]

        if eps > 0:
            self.N_left_m_eps   = self.N_left_m   + self.eps   # [ num_leafs ]
            self.N_left_ml_eps  = self.N_left_ml  + self.eps   # [ num_leafs ]
            self.N_right_m_eps  = self.N_right_m  + self.eps   # [ num_leafs ]
            self.N_right_ml_eps = self.N_right_ml + self.eps   # [ num_leafs ]
        else:
            self.N_left_m_eps   = self.N_left_m
            self.N_left_ml_eps  = self.N_left_ml
            self.N_right_m_eps  = self.N_right_m
            self.N_right_ml_eps = self.N_right_ml

        self.log_N_left_ml = np.log( self.N_left_ml_eps )     # [ num_leafs x num_labels ]
        self.log_N_left_m  = np.log( self.N_left_m_eps )      # [ num_leafs ]
        assert np.all( np.isfinite(self.log_N_left_ml) )
        assert np.all( np.isfinite(self.log_N_left_m) )

        self.log_N_right_ml = np.log( self.N_right_ml_eps )   # [ num_leafs x num_labels ]
        self.log_N_right_m  = np.log( self.N_right_m_eps )    # [ num_leafs ]
        assert np.all( np.isfinite(self.log_N_right_ml) )
        assert np.all( np.isfinite(self.log_N_right_m) )


    def calc_value(self):
        """
        Returns
        -------
        optim_val    : float
        """
        max_unormed_left_entropy  = float( np.dot( self.N_left_m, self.log_N_left_m ) )
        max_unormed_right_entropy = float( np.dot( self.N_right_m, self.log_N_right_m ) )
        
        unormed_left_entropy   = float( np.tensordot( self.N_left_ml, self.log_N_left_ml, axes=2 ) )
        unormed_right_entropy  = float( np.tensordot( self.N_right_ml, self.log_N_right_ml, axes=2 ) )

        assert (unormed_left_entropy - max_unormed_left_entropy) < 1e-3, "diff = %e" % (max_unormed_left_entropy - unormed_left_entropy)
        assert (unormed_right_entropy - max_unormed_right_entropy) < 1e-3, "diff = %e" % (max_unormed_right_entropy - unormed_right_entropy)

        optim_val = max_unormed_left_entropy  - unormed_left_entropy + \
                    max_unormed_right_entropy - unormed_right_entropy
        
        return float(optim_val)


    def calc_derivatives(self,   grd_N_left_ml : np.ndarray,
                               jacob_N_left_ml : tp.Optional[np.ndarray] = None,
                               jacob_coords    : tp.Optional[np.ndarray] = None,
                               **other_args ):
        """
        Parameters
        ----------
        grd_N_left_ml   : float ndarray [ num_dims x num_leafs x num_labels ]
        jacob_N_left_ml : float ndarray [ num_dims*(num_dims+1)//2 x num_leafs x num_labels ]
        jacob_coords    : int ndarray [ 2 x num_dims*(num_dims+1)//2 ]
        **other_args    : ignored parameters

        Returns
        -------
        d_optim_val   : float ndarray [ num_dims ]
        dd_optim_val  : float ndarray [ num_dims*(num_dims+1)//2 ]
        """
        
        grd_N_left_m = grd_N_left_ml.sum( axis=2 )   # [ num_dims x num_leafs ]

        grd_factor_m, grd_factor_ml = self.calc_derivate_prerequisites()

        # calc gradient
        d_optim_val = self.calc_gradient( grd_factor_m, grd_factor_ml, grd_N_left_m, grd_N_left_ml )

        # check for hessian calculation
        return_hessian = (jacob_N_left_ml is not None) and (jacob_coords is not None)

        if not bool(return_hessian):
            return (d_optim_val, None)

        # calc jacobian
        dd_optim_val = self.calc_jacobian( grd_factor_m,    grd_factor_ml,
                                           grd_N_left_m,    grd_N_left_ml,
                                           jacob_N_left_ml, jacob_coords )

        return (d_optim_val, dd_optim_val)

    

    def calc_derivate_prerequisites( self ):

        # ---- derivative relevant pre-calculations ----------------------

        #grd_N_left_m  = grd_N_left_ml.sum( axis=2 )       # [ num_dims x num_leafs ]
        grd_factor_m  = self.log_N_left_m  - self.log_N_right_m     # [ num_leafs ]
        grd_factor_ml = self.log_N_left_ml - self.log_N_right_ml    # [ num_leafs x num_labels ]
        
        if self.eps > 0:
            # these parts are greater than 0 if eps > 0 
            grd_factor_m  += (self.N_left_m  / self.N_left_m_eps)  - (self.N_right_m  / self.N_right_m_eps)
            grd_factor_ml += (self.N_left_ml / self.N_left_ml_eps) - (self.N_right_ml / self.N_right_ml_eps)
        
        return (grd_factor_m, grd_factor_ml)



    def calc_gradient( self, grd_factor_m, grd_factor_ml, 
                             grd_N_left_m, grd_N_left_ml ):
        """
        Parameters
        ----------
        grd_factor_m  : float ndarray [ num_leafs ]
        grd_factor_ml : float ndarray [ num_leafs x num_labels ]
        grd_N_left_m  : float ndarray [ num_dims x num_leafs ]
        grd_N_left_ml : float ndarray [ num_dims x num_leafs x num_labels ]

        Returns
        -------
        d_optim_val  : float ndarray [ num_dims ]
        """
        assert grd_factor_m.shape  == (self.num_leafs,)
        assert grd_factor_ml.shape == (self.num_leafs, self.num_labels)
        assert grd_N_left_m.shape[1:]  == (self.num_leafs,)
        assert grd_N_left_ml.shape[1:] == (self.num_leafs, self.num_labels)
        assert grd_N_left_m.shape[0] == grd_N_left_ml.shape[0]

        # [ num_dims x num_leafs ] x [ num_leafs ]
        d_optim_val  = np.dot( grd_N_left_m, grd_factor_m )                  # [ num_dims ]

        # [ num_dims x num_leafs x num_labels ] x [ num_leafs x num_labels ]
        d_optim_val -= np.tensordot( grd_N_left_ml, grd_factor_ml, axes=2 )  # [ num_dims ]
        
        return d_optim_val



    def calc_jacobian_prerequisites( self ):
        eps2 = 2 * self.eps
        dd_factor_m = (eps2 + self.N_left_m)  * np.power(self.N_left_m_eps,  -2) + \
                      (eps2 + self.N_right_m) * np.power(self.N_right_m_eps, -2)        # [ num_leafs ]
        
        dd_factor_ml = (eps2 + self.N_left_ml)  * np.power(self.N_left_ml_eps,  -2) + \
                       (eps2 + self.N_right_ml) * np.power(self.N_right_ml_eps, -2)     # [ num_leafs x num_labels ]
        return (dd_factor_m, dd_factor_ml)



    def calc_jacobian( self, grd_factor_m    : np.ndarray,
                             grd_factor_ml   : np.ndarray,
                             grd_N_left_m    : np.ndarray,
                             grd_N_left_ml   : np.ndarray,
                             jacob_N_left_ml : np.ndarray,
                             jacob_coords    : np.ndarray ):
        """
        Parameters
        ----------
        grd_factor_m    : float ndarray [ num_leafs ]
        grd_factor_ml   : float ndarray [ num_leafs x num_labels ]
        grd_N_left_m    : float ndarray [ num_dims x num_leafs ]
        grd_N_left_ml   : float ndarray [ num_dims x num_leafs x num_labels ]
        jacob_N_left_ml : float ndarray [ num_dims*(num_dims+1)//2 x num_leafs x num_labels ]
        jacob_coords    :   int ndarray [ 2 x num_dims*(num_dims+1)//2 ]

        Returns
        -------
        dd_optim_val  : float ndarray [ num_dims*(num_dims+1)//2 ]
        """
        assert grd_factor_m.shape  == (self.num_leafs,)
        assert grd_factor_ml.shape == (self.num_leafs, self.num_labels)
        assert grd_N_left_m.shape[1:]  == (self.num_leafs,)
        assert grd_N_left_ml.shape[1:] == (self.num_leafs, self.num_labels)
        assert grd_N_left_m.shape[0] == grd_N_left_ml.shape[0]
        assert jacob_N_left_ml.shape[1:] == (self.num_leafs, self.num_labels)
        assert jacob_coords.shape[0] == 2
        assert jacob_coords.shape[1] == jacob_N_left_ml.shape[0]

        jacob_N_left_m = jacob_N_left_ml.sum( axis=2 )   # [ num_jacob x num_leafs ]

        # [ num_jacob x num_leafs ] x [ num_leafs ]  => [ num_jacob ]
        dd_optim_val  = np.dot(       jacob_N_left_m, grd_factor_m )           # [ num_jacob ]
        # [ num_jacob x num_leafs x num_labels ] x [ num_leafs x num_labels ]  => [ num_jacob ]
        dd_optim_val -= np.tensordot( jacob_N_left_ml, grd_factor_ml, axes=2 ) # [ num_jacob ]
        del jacob_N_left_m


        dd_factor_m, dd_factor_ml = self.calc_jacobian_prerequisites()  # [ num_leafs ], [ num_leafs x num_labels ]

        # [ num_dims x num_leafs ] .* [ 1 x num_leafs ] => [ num_dims x num_leafs ]
        dd_factor_m = grd_N_left_m * dd_factor_m[np.newaxis,:]     # [ num_dims x num_leafs ]

        # [ num_dims x num_leafs x num_labels ] .*  [ num_leafs x num_labels ]
        dd_factor_ml = grd_N_left_ml * dd_factor_ml[np.newaxis,:,:]   # [ num_dims x num_leafs x num_labels ]

        # cross product of gradient
        jacob_zip = list( zip( jacob_coords[0,:], jacob_coords[1,:]  ) )
        
        dd_optim_val += np.array( [ np.dot( dd_factor_m[i,:], grd_N_left_m[j,:] ) for (i,j) in jacob_zip ], 
                                  dtype=self.dtype )
        dd_optim_val -= np.array( [ np.tensordot( dd_factor_ml[i,:,:], grd_N_left_ml[j,:,:], axes=2 ) for (i,j) in jacob_zip ], 
                                    dtype=self.dtype )

        return (dd_optim_val)
