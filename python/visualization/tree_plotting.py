# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["TreePlotter"]


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
from matplotlib.path import Path
from matplotlib.collections import PathCollection, PatchCollection, LineCollection





class TreePlotter( object ):

    node_idx      = None
    node_weights  = None
    edge_weights  = None
    edge_weights2 = None

    node_hists    = None

    layer_idx     = None


    @property
    def num_nodes(self):
        return self.node_idx.size


    @property
    def parent_idx( self ):
        """
        Returns
        -------
        parent_idx : int32 array
            index of parent node
        """
        return ((self.node_idx - 1) // 2)


    def __init__(self, node_idx, 
                       node_weights = None,
                       edge_weights = None,
                       edge_weights2 = None,
                       node_hists = None ):
        
        self.node_idx = node_idx

        if node_hists is not None:
            assert node_hists.shape[0] == node_idx.size
            assert node_weights is None
            node_weights = node_hists.sum( axis=1 )  # [ N x C ]

            node_norms = np.reciprocal( np.maximum( node_weights.astype(np.float32), \
                                                    np.finfo( np.float32 ).eps ) )
            node_hists = node_hists.astype(np.float32) * node_norms[:,np.newaxis]
            self.node_hists = node_hists
        
        if node_weights is None:
            node_weights = np.ones( node_idx.size, dtype=np.float32 )
        else:
            assert node_weights.size == node_idx.size

        if edge_weights is not None:
            edge_weights = np.asarray( edge_weights, dtype=np.float32 )
            assert edge_weights.size == node_idx.size
            self.edge_weights = edge_weights * ( 1 / edge_weights.max() )

        if edge_weights2 is not None:
            assert edge_weights2.size == node_idx.size
            self.edge_weights2 = edge_weights2 * ( 1 / edge_weights2.max() )

        assert node_weights.size == node_idx.size
        self.node_weights = np.asarray( node_weights, dtype=np.float32 )

        # layer indices
        self.layer_idx = np.log2( node_idx.astype(np.float32) + 1.5 ).astype(np.int32)


    # -----------------------------------------------------------------------------------------------


    def calc_inlayer_position(self):
        # position offset
        inlayer_pos_offset = 1.5 * np.power( 2., self.layer_idx, dtype=np.float32 ) - 1.5

        # inlayer position
        inlayer_position = self.node_idx.astype(np.float32) - inlayer_pos_offset

        return inlayer_position


    def calc_hierarchical_tree_node_positions( self, horizontal_tree, flip_tree ):
        """
        Parameters
        ----------
        
        horizontal_tree : 
        
        flip_tree : 

        Returns
        -------
        X : float32 array
        Y : float32 array
        """

        # position within layer
        inlayer_position = self.calc_inlayer_position()

        # in layer scaling
        max_layer_idx = self.layer_idx.max()
        layer_scale = np.power( 2, max_layer_idx - self.layer_idx, dtype=np.float32 )

        # X,Y positions
        X = (layer_scale * inlayer_position).astype( np.float32 )
        Y = self.layer_idx.copy().astype( np.float32 )
        
        if flip_tree is True:
            X *= -1
        
        Y *= -0.5 * (X.max() - X.min()) / (Y.max() - Y.min())
        if horizontal_tree is True:
            X,Y = Y,X
            X *= -1
    
        return (X,Y)
    

    def calc_circle_tree_node_positions( self, node_radii, angular_offset=0 ):
        """
        Parameters
        ----------
        
        angular_offset : 

        Returns
        -------
        X : float32 array
        Y : float32 array
        """

        # position within layer
        inlayer_position = self.calc_inlayer_position()

        # pi scaling
        inlayer_scaling = np.reciprocal( np.power( 2, self.layer_idx - 1, dtype=np.float32 ) )

        # radial position on cirlcle
        node_angles = inlayer_position * inlayer_scaling
        assert (np.absolute( node_angles ) < 1.1).all()
        node_angles *= np.pi

        if angular_offset != 0:
            node_angles += angular_offset * np.pi / 180

        # determ radii of layers
        max_node_radii = np.array( [ node_radii[self.layer_idx == l].max() for l in range(self.layer_idx.max()+1) ], dtype=np.float32 )
        if max_node_radii.size > 0:
            max_node_radii[0] *= 1.618
        layer_radii = np.add.accumulate( max_node_radii[:-1] + max_node_radii[1:] )
        layer_radii = np.concatenate( ([0],layer_radii), axis=0 )

        # X,Y positions
        X = layer_radii[self.layer_idx] * np.sin( node_angles )
        Y = layer_radii[self.layer_idx] * np.cos( node_angles )

        return (X,Y)

    
    # -----------------------------------------------------------------------------------------------


    def calc_hierarchical_weight_scale( self ):
        
        for lidx in range( self.layer_idx.max(), 0, -1 ):
            
            last_layer_mask = self.layer_idx == lidx
            max_weight = self.node_weights[ last_layer_mask ].max()

            if max_weight > 0:
                break
        
        # c * w = pi * 0.5^2 => c = pi * 0.25 / w
        #weight_scale =  (np.pi * 0.25) / max_weight
        weight_scale =  0.25 / max_weight

        return weight_scale


    def calc_circular_weight_scale( self ):
        
        max_weight = self.node_weights.max()
        #weight_scale =  (np.pi * 0.25) / max_weight
        weight_scale =  0.25 / max_weight

        return weight_scale


    def layerwise_weight_normalization( self, weights ):

        n_weights = np.empty( weights.shape, dtype=np.float32 )
        
        if __debug__:
            n_weights.fill( np.nan )

        for lidx in range( self.layer_idx.max() + 1 ):
            inlayer_idx = np.flatnonzero( self.layer_idx == lidx )
            
            norm = 1 / max( weights[inlayer_idx].sum(), np.finfo( np.float32 ).eps )
            n_weights[inlayer_idx] = norm * weights[inlayer_idx]
        
        assert np.all( np.isfinite(n_weights) )

        return n_weights


    # -----------------------------------------------------------------------------------------------

    @staticmethod
    def create_pie_slice_collection( start_angles, end_angles, sizes, positions, **kwargs ):
        
        assert start_angles.size == end_angles.size
        assert sizes.size == start_angles.size
        assert positions.shape[0] == start_angles.size
        assert positions.shape[1] == 2

        valid_idx = np.flatnonzero( start_angles != end_angles )
        if valid_idx.size < sizes.size:
            start_angles = start_angles.take( valid_idx )
            end_angles   = end_angles.take( valid_idx )
            sizes        = sizes.take( valid_idx )
            positions    = positions.take( valid_idx, axis=0 )


        wedges = [ Path.wedge( sa, ea ) for sa,ea in zip( start_angles, end_angles ) ]
        wedges = [ Path( (w.vertices * s) + positions[idx,:], w.codes ) for idx,(w,s) in enumerate( zip( wedges, sizes ) ) ]

        pcollection = PathCollection( wedges, **kwargs )
        return pcollection


    def create_node_pies( self, sizes, positions, cmap, **kwargs ):
        assert positions.shape[0] == self.num_nodes
        assert positions.shape[1] == 2
        assert sizes.size == self.num_nodes

        num_classes = self.node_hists.shape[1]
        valid_node_idx = np.flatnonzero( sizes > 0 )
        num_nodes = valid_node_idx.size

        end_angles = 360 * np.add.accumulate( self.node_hists[valid_node_idx,:], axis=1 ) # [ Nv x C ]
        
        start_angles = np.zeros( (num_nodes,1), dtype=end_angles.dtype )
        start_angles = np.concatenate( (start_angles, end_angles[:,:-1] ), axis=1 )
        sizes        = sizes.take( valid_node_idx )
        positions    = positions.take( valid_node_idx, axis=0 )


        colors = cmap(np.linspace( 0, 1, num_classes) )
        collections = [ self.create_pie_slice_collection( start_angles[:,c],
                                                            end_angles[:,c],
                                                                      sizes,
                                                                  positions, 
                                                        facecolor=colors[c],
                                                                   zorder=2,
                                                                  **kwargs ) \
                        for c in range( num_classes ) ]
        
        return collections


    def create_node_circles( self, sizes, positions, **kwargs ):
        assert positions.shape[0] == self.num_nodes
        assert positions.shape[1] == 2
        assert sizes.size == self.num_nodes

        valid_node_idx = np.flatnonzero( sizes > 0 )
        num_nodes = valid_node_idx.size

        patches = list( [ mpatches.Circle( positions[nidx,:], \
                                           sizes[nidx], \
                                           edgecolor=None ) \
                          for nidx in self.node_idx if sizes[nidx] > 0 ] )

        pcollection = PatchCollection(patches, **kwargs )
        return pcollection


    def create_edges( self, edge_width, positions, edge_colors, alpha ):
        tree_edges = list()

        # mapping node indices to position maps
        node_to_pos_map = dict( zip( self.node_idx, range( self.node_idx.size ) ) )

        valid_idx = np.flatnonzero( edge_width > 0 )

        for nidx,pidx in zip( self.node_idx[valid_idx], self.parent_idx[valid_idx] ):
            
            if nidx == 0:
                continue
            
            nidx = node_to_pos_map[nidx]
            pidx = node_to_pos_map[pidx]

            xdata = ( positions[nidx,0], positions[pidx,0] )
            ydata = ( positions[nidx,1], positions[pidx,1] )
            line = mlines.Line2D( xdata, ydata, color=edge_colors[nidx], zorder=1, linewidth=edge_width[nidx], alpha=alpha )
            tree_edges.append( line )
        
        return tree_edges


    @staticmethod
    def label(xy, text):
        y = xy[1] - 0.15  # shift y-value for label so that it's below the artist
        plt.text(xy[0], y, text, ha="center", family='sans-serif', size=14)



    def plot(self, ax = None,
                   color="gray",
                   node_color=None, edge_color=None,
                   node_cmap=plt.cm.nipy_spectral, edge_cmap=plt.cm.viridis,
                   plot_horizontal = False, flip=False,
                   circular_tree = False,
                   angular_offset = 90,
                   layerwise_edge_weight_normalization = True,
                   layerwise_edge_weight2_normalization = True,
                   linewidth=3,
                   alpha = 1.0 ):
        """
        Parameters
        ----------
        
        color : string or RGB
            default color value for nodes and edges
        node_color : string or RGB
            default color for nodes, eguals "color" parameter if not set
        edge_color : string or RGB
            default color of edges, equals "color" parameter if not set
        node_cmap : matplotlib.pyplot.cm.<colormap>
            color map used for node histograms
        edge_cmap : matplotlib.pyplot.cm.<colormap>
            color map used for edge_weights2
        plot_horizontal : bool (default False)
            plots hierarchical tree horizontally, ignored for circular tree
        flip : bool (default False)
            inverts node order
        circular_tree : bool (default False)
            if True a circular tree is plotted
        angular_offset : 0 <= x <= 360
            angular offset in circular plot
        layerwise_edge_weight_normalization : bool (default True)
            normalize edge_weight within each tree layer
        layerwise_edge_weight2_normalization : bool (default True)
            normalize edge_weight2 within each tree layer
        linewidth : float >= 0
            default line width, used if no edge weights are provided 
        alpha : 0 <= x <= 1:
            transpareny of entire plot

        Returns
        -------
        ax : used matplotlib axes
        """

        if ax is None:
            fig, ax = plt.subplots()
        
        if node_color is None:
            node_color = color
        
        if edge_color is None:
            edge_color = color
        
        ## calc node weights scaling
        if circular_tree is True:
            weight_scale = self.calc_circular_weight_scale()
        else:
            weight_scale = self.calc_hierarchical_weight_scale()

        # radii of node circles
        # w = pi * r**2
        #circle_radii = np.sqrt( weight_scale * self.node_weights / np.pi )
        circle_radii = 0.95 * np.sqrt( weight_scale * self.node_weights )

        ## get node postions
        if circular_tree is True:
            X, Y = self.calc_circle_tree_node_positions( circle_radii, angular_offset )
        else:
            X, Y = self.calc_hierarchical_tree_node_positions( plot_horizontal, flip )

        node_coords = np.stack( (X,Y), axis=-1 )  # [ N x 2 ]

        ## calc edge weights => edge_width
        if self.edge_weights is not None:
            edge_weights = self.edge_weights

            if layerwise_edge_weight_normalization:
                edge_weights = self.layerwise_weight_normalization( edge_weights )
        else:
            edge_weights = np.ones( self.num_nodes, dtype=np.float32 )
        
        edge_width = linewidth * edge_weights * ( self.node_weights > 0 ).astype( np.float32 )


        # calc edge weight2 => edge color
        if self.edge_weights2 is not None:
            edge_weights2 = self.edge_weights2

            if layerwise_edge_weight2_normalization:
                edge_weights2 = self.layerwise_weight_normalization( edge_weights2 )
            
            edge_colors = edge_cmap( edge_weights2 )
        else:
            edge_colors = edge_weights.size * [ edge_color ]

        ## plot edges
        tree_edges = self.create_edges( edge_width, node_coords, edge_colors=edge_colors, alpha=alpha )
        for edge in tree_edges:
            ax.add_line( edge )

        ## plot nodes
        if self.node_hists is not None:
            pcollections = self.create_node_pies( circle_radii, node_coords, cmap=node_cmap, linewidth=0, alpha=alpha )
            for collection in pcollections:
                ax.add_collection( collection )
        else:
            pcollection = self.create_node_circles( circle_radii, node_coords, color=node_color, alpha=alpha )
            ax.add_collection( pcollection )

        ## Format axes
        border = 1.1 * circle_radii.max()
        ax.set_xlim( X.min() - border, X.max() + border )
        ax.set_ylim( Y.min() - border, Y.max() + border )
        ax.set_aspect('equal')

        return ax

#------------------------------------------------------------------------------------


# test plotting
def test_plotting():
    fig, ax = plt.subplots()

    node_idx = np.arange(25)
    tplotter = TreePlotter( node_idx )

    tplotter.plot( ax )
    plt.show()


if __name__ == '__main__':
    test_plotting()

