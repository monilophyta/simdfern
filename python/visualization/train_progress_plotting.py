# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

# ----------------------------------------------------------------------------------------------------

__all__ = ["plot_training_progress", "TrainProgressPlotter"]

# ----------------------------------------------------------------------------------------------------

import typing as tp
import numpy as np

from ..clmath import hist_counts
from ..training import ObserverNotificationSource

import matplotlib.pyplot as plt
import matplotlib.collections as collections
import matplotlib.colors as mcolors
from matplotlib.ticker import FuncFormatter,FixedLocator

# ----------------------------------------------------------------------------------------------------


def plot_training_progress( obs, train_labels, test_labels, 
                            train_weights = None, test_weights = None,
                            fig_width = 12, figure=None ):
    
    plotter = TrainProgressPlotter( train_labels = train_labels,
                                      num_layers = obs["num_layers"],
                                  measure_source = obs["measure_source"],
                                      time_ticks = obs["time_ticks"],
                                   train_weights = train_weights,
                                      train_gini = obs["train_gini_index"],
                                    train_mutual = obs["train_mutual_inf"],
                                     test_labels = test_labels,
                                    test_weights = test_weights,
                                       test_gini = obs["test_gini_index"],
                                     test_mutual = obs["test_mutual_inf"] )
    
    if figure is None:
        fig = plt.figure( figsize = (fig_width, fig_width*9/16) )
    else:
        fig = plt.figure( figure.number )
        
    plt.tight_layout()
    plt.grid()
    ax = plt.gca()
    plotter.plot( ax )
    return (fig, ax)


# ----------------------------------------------------------------------------------------------------

class TrainProgressPlotter(object):

    colors = list(mcolors.TABLEAU_COLORS.values())

    num_layers     : np.ndarray
    measure_source : np.ndarray
    time_ticks     : np.ndarray

    xpos           : np.ndarray

    train_gini     : tp.Optional[np.ndarray]
    train_mutual   : tp.Optional[np.ndarray]

    test_gini    : tp.Optional[np.ndarray]
    test_mutual  : tp.Optional[np.ndarray]


    max_train_mutual_inf : float
    max_test_mutual_inf  : tp.Optional[float]


    @property
    def size(self) -> int:
        return int(self.measure_source.size)


    def __init__(self, train_labels : np.ndarray,
                         num_layers : np.ndarray,
                     measure_source : np.ndarray,
                         time_ticks : np.ndarray,
                      train_weights : tp.Optional[np.ndarray] = None,
                         train_gini : tp.Optional[np.ndarray] = None,
                       train_mutual : tp.Optional[np.ndarray] = None,
                       
                        test_labels : tp.Optional[np.ndarray] = None,
                       test_weights : tp.Optional[np.ndarray] = None,
                          test_gini : tp.Optional[np.ndarray] = None,
                        test_mutual : tp.Optional[np.ndarray] = None ):
        
        self.measure_source = measure_source

        assert (train_weights is None) or (train_weights.size == train_labels.size)
        assert (train_gini is None) or (train_gini.size == self.size)
        assert (train_mutual is None) or (train_mutual.size == self.size)
        assert (test_weights is None) or (test_weights.size == test_labels.size)
        assert (test_gini is None) or (test_gini.size == self.size)
        assert (test_mutual is None) or (test_mutual.size == self.size)

        #self.train_labels   = train_labels
        self.num_layers     = num_layers
        self.time_ticks     = time_ticks
        self.train_gini     = train_gini
        self.train_mutual   = train_mutual
        
        self.test_labels  = test_labels
        self.test_gini    = test_gini
        self.test_mutual  = test_mutual

        # x-positions
        self.xpos = np.arange( self.size ).astype(np.float32)

        # maximum entropy values
        self.max_train_mutual_inf = hist_counts( train_labels, weights = train_weights ).entropy( do_norm=True, use_log2=True )
        if test_labels is not None:
            self.max_test_mutual_inf = hist_counts( self.test_labels, weights = test_weights ).entropy( do_norm=True, use_log2=True )


    def plot(self, ax = None ):
        if ax is None:
            fig, ax = plt.subplots()
        else:
            pass
            #plt.sca(ax)
        
        # main plotting axis
        mainax = ax

        # time axis
        timax = mainax.twinx()

        # plot optimization measures on test data
        if self.test_mutual is not None:
            y_data = self.test_mutual / self.max_test_mutual_inf
            mainax.plot( self.xpos, y_data, "+:r", label="mutual inf. (testing)" )
            measure_y = y_data
        
        if self.test_gini is not None:
            mainax.plot( self.xpos, self.test_gini, "x-r", label="gini (testing)" )
            measure_y = self.test_gini

        # plot optimization measures on training data
        if self.train_mutual is not None:
            y_data = self.train_mutual / self.max_train_mutual_inf
            mainax.plot( self.xpos, y_data, "+:b", label="mutual inf. (training)" )
            measure_y = y_data
        
        if self.train_gini is not None:
            mainax.plot( self.xpos, self.train_gini, "-b", label="gini (training)" )
            measure_y = self.train_gini


        # plot measure source
        for midx,msrc in enumerate(ObserverNotificationSource):
            
            src_idx = np.flatnonzero( self.measure_source == msrc.value )
            if src_idx.size > 0:
                mainax.plot( self.xpos[src_idx], measure_y[src_idx], "s", color=self.colors[midx], label=msrc.name )

        mainax.set_xlabel("improvement steps")
        mainax.set_ylabel("mutual inf. & gini index")
        
        formatter = None
        if self.train_mutual is not None:
            if self.test_mutual is not None:
                formatter = lambda val,pos: "muinf: (%0.1f;%0.1f)\ngini/relative: %.1f" % (val*self.max_train_mutual_inf,
                                                                                           val*self.max_test_mutual_inf, val)
            else:
                formatter = lambda val,pos: "muinf: (%0.1f)\ngini/relative: %.1f" % (val*self.max_train_mutual_inf, val)
        elif self.test_mutual is not None:
            formatter = lambda val,pos: "muinf: (%0.1f)\ngini/relative: %.1f" % (val*self.max_test_mutual_inf, val)
        
        if formatter is not None:
            mainax.yaxis.set_major_formatter( FuncFormatter( formatter ) )
        
        mainax.legend()

        # time consumption
        timax.plot( self.xpos, self.time_ticks - self.time_ticks.min(), ":k", label="time ticks" )
        timax.yaxis.set_major_formatter( FuncFormatter( lambda val,pos: self.timedelta2string( val ) ) )

        #mainlocs = mainax.yaxis.get_ticklocs()

        mainlim = mainax.get_ylim()
        timelim = timax.get_ylim()
        tickerfunf = lambda x : timelim[0] + (x - mainlim[0]) / (mainlim[1]-mainlim[0]) * (timelim[1]-timelim[0])
        timeticks = tickerfunf(mainax.get_yticks())

        timax.yaxis.set_major_locator( FixedLocator(timeticks) )
        timax.set_ylabel("training time")
        #plt.sca(mainax)

        self.plot_num_layers( mainax )


    @staticmethod
    def timedelta2string( seconds ):

        minuteL = 60
        hourL = minuteL * 60
        dayL = hourL * 24

        days = int(seconds) // dayL
        seconds -= days * dayL

        hours = int(seconds) // hourL
        seconds -= hours * hourL

        minutes = int(seconds) // minuteL
        seconds -= minutes * minuteL

        return "%dd:%02dh\n%02dm:%02ds" % (days,hours,minutes,int(seconds))


    def plot_num_layers( self, ax ):

        gray_mask = 0 == (self.num_layers % 2)
        #gray_mask = np.logical_not( gray_mask )
        gray_mask = gray_mask.repeat(2)
        gray_mask[1:] = np.logical_or(gray_mask[1:], gray_mask[:-1])

        n_xpos = self.xpos.repeat(2)
        n_xpos[1:] = 0.5 * (n_xpos[:-1] + n_xpos[1:])
        n_xpos[0] += n_xpos[1] - n_xpos[2]
        n_xpos[-1] += n_xpos[-2] - n_xpos[-3]

        collection = collections.BrokenBarHCollection.span_where( n_xpos, 
                                                                  ymin = 0, ymax = 1,
                                                                  where = gray_mask,
                                                                  facecolor = 'gray',
                                                                  alpha = 0.3 )
        ax.add_collection(collection, autolim=False)
        #ax.plot( self.xpos, self.num_layers.astype(np.float) / self.num_layers.max(), 'x-m' )

        ## get centers of areas
        nLayers = np.arange(1,self.num_layers.max()+1 )
        area_centers = np.array( [ self.xpos[self.num_layers == l].mean() for l in nLayers ], dtype=np.float )

        # According to 
        # https://matplotlib.org/3.1.1/gallery/subplots_axes_and_figures/secondary_axis.html#sphx-glr-gallery-subplots-axes-and-figures-secondary-axis-py
        # https://stackoverflow.com/questions/19884335/matplotlib-top-bottom-ticks-different
        axT = ax.twiny()
        axT.set_xlim( ax.get_xlim() )
        axT.set_xticks( area_centers, minor=False )
        axT.set_xticklabels([ str(l) for l in nLayers ] )
        axT.tick_params(axis='x', direction="in", width = 4 )
        axT.set_xlabel("number of layers")
        
