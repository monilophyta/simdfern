# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

# --------------------------------------------------------------------------------------

__all__ = ["LevelWeightLineSearch"]

# --------------------------------------------------------------------------------------


import typing as tp
import math
import numpy as np

from ..ctypes_py import WeightedFernLayer
from ..ctypes_py import FeatureRows #, create_feature_rows
from ..ctypes_py import project_features, ESummationAlgo #, FernSplitOptimizer
from ..ctypes_py import lsearch_fern_param, line_search_result_t
from ..ctypes_py import ParamPenalizer, EPenalizerType
from ..clmath import normalize_gini
from .. import simd

# --------------------------------------------------------------------------------------


class LevelWeightLineSearch(object):
    """
    Attributes
    ----------
    feature_rows : instance of FeatureRows [ K x W ]
    label_idx : int32 ndarray [ N ]
        Labels of each sample
    sample_weights : int32 ndarray [ N ]
        Weight of each sample
    num_labels : int or None
        Number of labels
    sum_sample_weights : int
        Sum of all sample weights
    objective : str
        Either "gini" or "entropy"
    regul_strength : float
        Strength (weight) of regularization
    """
    feature_rows   : FeatureRows

    label_idx      : np.ndarray
    sample_weights : np.ndarray

    num_labels : int
    sum_sample_weights : int

    # optimization
    objective : str = "gini"   # split optimizer currently only supports gini

    # regularisation
    regul_strength : float


    @property
    def num_features(self) -> int:
        return self.feature_rows.num_features

    @property
    def num_data(self) -> int:
        return self.label_idx.size

    # -------------------------------------------------------------------------------------------------


    def __init__(self, feature_rows    : FeatureRows,
                       label_idx       : np.ndarray,
                       sample_weights  : tp.Optional[np.ndarray] = None,
                       num_labels      : tp.Optional[int] = None,
                       regul_ratio     : float = 0.01 ):
        """
        Parameters
        ----------
        feature_rows : instance of FeatureRows [ K x W ]
        label_idx : int32 ndarray [ N ]
            Labels of each sample
        sample_weights : int32 ndarray [ N ]
            Weight of each sample
        num_labels : int or None
            Number of labels
        regul_ratio : float
            Used for calculation of regulation strength by:
                regul_strength = regul_ratio * max(abs(optimization))
        """
        assert self.objective == 'gini'

        assert isinstance( feature_rows, FeatureRows )
        assert len(feature_rows[0]) == label_idx.size
        assert (sample_weights is None) or ( (sample_weights.size == label_idx.size) and np.issubdtype( sample_weights.dtype, np.int32 ) )

        assert np.issubdtype( label_idx.dtype, np.int32 )

        if num_labels is None:
            num_labels = label_idx.max() + 1
        assert num_labels > label_idx.max()

        self.num_labels      = int(num_labels)
        self.feature_rows    = feature_rows
        self.label_idx       = label_idx

        if sample_weights is None:
            sample_weights = np.ones( label_idx.size, dtype=np.int32 )
            sum_sample_weights = int(label_idx.size)
        else:
            sum_sample_weights = int(sample_weights.sum(dtype=np.int64))
        self.sample_weights = sample_weights
        self.sum_sample_weights = sum_sample_weights

        # initialize optimization
        self.set_regularization( regul_ratio = regul_ratio )


    def set_regularization(self, regul_ratio : float):
        assert regul_ratio >= 0
        
        if self.objective == "gini":
            max_abs_optimum = self.sum_sample_weights * (self.num_labels-1)
            assert normalize_gini( max_abs_optimum, self.sum_sample_weights, self.num_labels ) == 1.0
        elif self.objective == "entropy":
            max_abs_optimum = float( self.sum_sample_weights ) * math.log( float(self.num_labels) )
        else:
            assert( False )

        self.regul_strength = max_abs_optimum * float(regul_ratio)
    
    # -------------------------------------------------------------------------------------------------

    def optimize_along_direction(self, init_fern_layer    : WeightedFernLayer,
                                       weight_lsearch_dir : np.ndarray,
                                       split_lsearch_dir  : float,
                                       leaf_idx           : np.ndarray,
                                       num_leafs          : tp.Optional[int] = None,
                              use_regularization_gradient : bool = True ):
        """
        Parameters
        ----------
        init_fern_layer : instance of WeightedFernLayer
            Fern layer with start parameters serving as offset point in line search
        weight_lsearch_dir : float32 ndarray [ K ]
            Direction in fern layer weight space along which line search is performed.
            A good direction might be the gradient at point of init_fern_layer
        split_lsearch_dir : float
            Search direction for split parameters
        leaf_idx : float32 ndarray [ N ]
            Leaf indices of data after init_fern_layer has been applied
        num_leafs : int or None
            Number of leafs in leaf_idx. Should be something like 2**num_partitions
        use_regularization_gradient : bool
            If True (default) the search direction is combined with the gradient of the regularisation

        Performs a line search along $o + x * g$ with 
            $o$ = [ init_fern_layer.feaure_weights, -init_fern_layer.feature_split ]
            $g$ = [ weight_lsearch_dir, -split_lsearch_dir ]
            $x$ = dir_scale (output)
        

        Returns Dictionary with Items
        -----------------------------
        dir_scale : float
            Result of optimization
        rating : int (gini) or float (entropy)
            The final obtained rating without regularisation therm
        dir_scale_penalty : float
            Regularisation term
        fern_layer : instance of WeightedFernLayer [ K ]
            Resulting new fern layer after optimization with applied dir_scale.
            Feature weights are renormalized
        regul_strength : float
            Applied strength of regularisation
        lsearch_dir : float32 ndarray [ K ]
            Actual search direction in line search. Should be the input search direction
            of combination if the input and the regularisation gradient
        num_optim_steps : int
            Number of steps, the optimization criterion got increased
        """
        assert isinstance( init_fern_layer, WeightedFernLayer )
        assert init_fern_layer.num_features == self.feature_rows.num_features
        assert np.issubdtype( leaf_idx.dtype, np.int32 )

        if num_leafs is None:
            num_leafs = leaf_idx.max() + 1
        assert num_leafs > leaf_idx.max()
        num_nodes = int(num_leafs)
        
        # shift up leaf indices
        node_idx = np.left_shift( leaf_idx, 1 )
        assert np.all( (node_idx % 2) == 0 )

        # --------------------------------------------------------------------------------------------
        
        # Add derivative of regularization to gradient (search direction)
        if bool(use_regularization_gradient):
            weight_lsearch_dir = weight_lsearch_dir + self.calc_penalizer_gradient( init_fern_layer )

        # --------------------------------------------------------------------------------------------

        # Project all samples onto the plane normal
        plane_normal_projection = self.calc_plane_normal_projection( init_fern_layer )   # [ N ]
        
        # Project all samples onto the line search direction
        lsearch_projection = self.calc_lsearch_projection( weight_lsearch_dir, split_lsearch_dir )  # [ N ]

        # calculate candate for search direction scaling
        with np.errstate(divide='ignore',invalid='ignore'):
            scale_candidates = np.divide( plane_normal_projection,
                                          lsearch_projection,
                                          out=plane_normal_projection )  # [ N ]
        del plane_normal_projection

        # mask if finite values
        finite_mask = np.isfinite( scale_candidates )

        if np.any( finite_mask ):
            # margin position
            min_weight = scale_candidates[finite_mask].min()
        else:
            # No optimization possible
            return None
        
        # for the sake of performance
        bool_out = finite_mask
        del finite_mask

        # classify according to min_weight; NaN <=> 0/0 also goes to the left side
        init_leaf_idx = np.logical_or( min_weight <= scale_candidates,
                                       np.isnan( scale_candidates, out=bool_out ),
                                       out=bool_out ).astype(np.int32)

        # flip sides of samples with negative search direction scaling
        init_leaf_idx = np.bitwise_xor( init_leaf_idx,
                                        np.less(lsearch_projection, 0, out=bool_out).astype(np.int32),
                                        out = init_leaf_idx )
        
        # add other fern levels
        init_leaf_idx += node_idx
        assert np.all( init_leaf_idx < (num_nodes << 1) )

        # param penalizer for weight regularization
        penalizer = self.create_penalizer( offset_weights = init_fern_layer.feature_weights, 
                                           scaled_weights = weight_lsearch_dir )

        # run the actual optimization
        res = lsearch_fern_param( param_candidates = scale_candidates,
                                          node_idx = init_leaf_idx,
                                         label_idx = self.label_idx,
                                      data_weights = self.sample_weights,
                                         num_nodes = num_nodes << 1,
                                        num_labels = self.num_labels,
                                         objective = self.objective,
                                         penalizer = penalizer )
        assert isinstance( res, line_search_result_t )
        
        # create new layer as result of optimization
        n_layer = self.create_fern_layer( init_fern_layer = init_fern_layer,
                                            weight_change = res.prmVal * weight_lsearch_dir,
                                             split_change = res.prmVal * split_lsearch_dir )

        # optimization
        ret = dict( dir_scale_penalty = res.prmValPenalty,
                      num_optim_steps = res.num_improvements,
                           fern_layer = n_layer,
                       regul_strength = self.regul_strength,
                            dir_scale = res.prmVal,
                          lsearch_dir = weight_lsearch_dir )
        
        if self.objective == 'gini':
            ret["rating"] = res.nodeGiniRating
        elif self.objective == 'entropy':
            ret["rating"] = res.nodeEntropy
        else:
            assert False
        return ret

    # -------------------------------------------------------------------------------------------------

    def create_penalizer(self, offset_weights : np.ndarray,
                               scaled_weights : np.ndarray ):
        assert isinstance( offset_weights, np.ndarray )
        assert isinstance( scaled_weights, np.ndarray )
        assert offset_weights.size == scaled_weights.size
        
        do_align = simd.default_simd_vector32_size() <= (offset_weights.size * 2)

        return ParamPenalizer.create_vector_regularization( 
                                    regul_type = EPenalizerType.CENTRALIZED_L1, 
                                regul_strength = self.regul_strength,
                                        offset = offset_weights,
                                         scale = scaled_weights,
                                      do_align = do_align )
        
    # -------------------------------------------------------------------------------------------------

    def calc_penalizer_gradient( self, fern_layer : WeightedFernLayer ):
        """Gradient of normalized L1 regularization term"""
        return (self.regul_strength * fern_layer.centralized_l1norm_gradient)


    # -------------------------------------------------------------------------------------------------

    def calc_plane_normal_projection( self, fern_layer : WeightedFernLayer ):
        assert isinstance( fern_layer, WeightedFernLayer )
        assert fern_layer.num_features == self.num_features

        projection = fern_layer.project_features( self.feature_rows ) # [ N ]
        return np.subtract( fern_layer.feature_split, projection, out=projection )


    def calc_lsearch_projection(self, weight_lsearch_dir : np.ndarray,
                                       split_lsearch_dir : float ):
        assert isinstance( weight_lsearch_dir, np.ndarray )
        assert isinstance( split_lsearch_dir, float )
        assert np.issubdtype( weight_lsearch_dir.dtype, np.float32 )
        assert weight_lsearch_dir.size == self.num_features

        projection = project_features( weight_lsearch_dir, self.feature_rows, ESummationAlgo.NEUMAIER )

        if split_lsearch_dir != 0:
            projection = np.subtract( projection, split_lsearch_dir, out=projection )
        
        return projection


    def create_fern_layer(self, init_fern_layer : WeightedFernLayer,
                                  weight_change : np.ndarray,
                                   split_change : float ):
        
        assert isinstance( init_fern_layer, WeightedFernLayer )
        assert init_fern_layer.num_features == self.num_features
        assert isinstance( weight_change, np.ndarray )
        assert isinstance( split_change, float )
        assert np.issubdtype( weight_change.dtype, np.float32 )
        assert weight_change.size == self.num_features

        n_feature_weights = init_fern_layer.feature_weights + weight_change
        n_feature_split = init_fern_layer.feature_split + float(split_change)

        # normalize weight vector to length one
        norm = 1. / np.linalg.norm( n_feature_weights )
        n_feature_weights *= norm
        n_feature_split *= norm

        return WeightedFernLayer.create( feature_weights=n_feature_weights, feature_split=n_feature_split )
