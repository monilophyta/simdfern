# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

# --------------------------------------------------------------------------------------

__all__ = ["LevelWeightCoordinateDescent"]

# --------------------------------------------------------------------------------------


import typing as tp
import math
import numpy as np

from ..ctypes_py import WeightedFernLayer
from ..ctypes_py import FeatureRows, create_feature_rows
from ..ctypes_py import project_features, FernSplitOptimizer
from ..ctypes_py import lsearch_fern_param, line_search_result_t
from ..ctypes_py import ParamPenalizer, EPenalizerType

# --------------------------------------------------------------------------------------


class LevelWeightCoordinateDescent(object):

    invalid_weight : float = np.power( np.finfo( np.float32 ).max, 1./3, dtype=np.float32 )

    # instance of split optimization
    split_optimizer : FernSplitOptimizer

    feature_weights : np.ndarray
    feature_split   : float

    feature_rows   : FeatureRows

    label_idx      : np.ndarray
    node_idx       : np.ndarray
    sample_weights : np.ndarray

    num_labels : int
    num_nodes  : int

    sum_sample_weights : int

    # optimization
    objective : str = "gini"   # split optimizer currently only supports gini

    # regularisation
    regul_strength : float


    @property
    def num_features(self) -> int:
        return self.feature_rows.num_features

    @property
    def num_data(self) -> int:
        return self.label_idx.size


    def get_layer(self) -> WeightedFernLayer:
        return WeightedFernLayer.create( self.feature_weights, self.feature_split )
    


    def __init__(self, init_fern_layer : WeightedFernLayer,
                       feature_rows    : FeatureRows,
                       label_idx       : np.ndarray,
                       leaf_idx        : np.ndarray,
                       sample_weights  : tp.Optional[np.ndarray] = None,
                       num_labels      : tp.Optional[int] = None,
                       num_leafs       : tp.Optional[int] = None,
                       regul_ratio     : float = 0.01 ):
        
        assert self.objective == 'gini'
        assert isinstance( init_fern_layer, WeightedFernLayer )
        assert isinstance( feature_rows, FeatureRows )
        assert init_fern_layer.num_features == len(feature_rows)
        assert len(feature_rows[0]) == label_idx.size
        assert label_idx.size == leaf_idx.size
        assert (sample_weights is None) or ( (sample_weights.size == label_idx.size) and np.issubdtype( sample_weights.dtype, np.int32 ) )

        assert np.issubdtype( leaf_idx.dtype, np.int32 )
        assert np.issubdtype( label_idx.dtype, np.int32 )

        if num_labels is None:
            num_labels = label_idx.max() + 1
        assert num_labels > label_idx.max()

        if num_leafs is None:
            num_leafs = leaf_idx.max() + 1
        assert num_leafs > leaf_idx.max()

        self.feature_weights = init_fern_layer.feature_weights.copy()
        self.feature_split   = init_fern_layer.feature_split
        self.num_labels      = int(num_labels)
        self.num_nodes       = int(num_leafs)
        self.feature_rows    = feature_rows
        self.label_idx       = label_idx
        
        # shift up leaf indices
        self.node_idx        = np.left_shift( leaf_idx, 1 )
        assert np.all( (self.node_idx % 2) == 0 )

        if sample_weights is None:
            sample_weights = np.ones( label_idx.size, dtype=np.int32 )
            sum_sample_weights = int(label_idx.size)
        else:
            sum_sample_weights = int(sample_weights.sum(dtype=np.int64))
        self.sample_weights = sample_weights
        self.sum_sample_weights = sum_sample_weights

        # initialize optimization
        self.set_regularization( regul_ratio = regul_ratio  )
        
        # initialize split optimizer
        self.split_optimizer = FernSplitOptimizer( node_idx = leaf_idx,
                                                  label_idx = self.label_idx,
                                               data_weights = self.sample_weights,
                                                  num_nodes = int(num_leafs),
                                                 num_labels = self.num_labels )


    def set_regularization(self, regul_ratio : float ):
        assert regul_ratio >= 0

        if self.objective == "gini":
            max_abs_optimum = self.sum_sample_weights * self.num_labels
        elif self.objective == "entropy":
            max_abs_optimum = float( self.sum_sample_weights ) * math.log( float(self.num_labels) )
        else:
            assert( False ) 
        self.regul_strength = max_abs_optimum * float(regul_ratio)


    def optimize_weight(self, cidx ):

        # projection of all features except feature_rows[cidx]
        with np.errstate(divide='ignore',invalid='ignore'):
            param_candidates = np.divide( self.get_coordinate_marginal( cidx ), self.feature_rows[cidx] )  # [ N ]

        # mask if finite values
        finite_mask = np.isfinite( param_candidates )

        if np.any( finite_mask ):
            # margin position
            min_weight = param_candidates[finite_mask].min()
        else:
            # No optimization possible
            return None

        # classify according to min_weight; NaN <=> 0/0 also goes to the left side
        init_leaf_idx = np.logical_or( min_weight <= param_candidates,
                                       np.isnan( param_candidates ) ).astype(np.int32)

        # flip sides of samples with negative feature
        init_leaf_idx = np.bitwise_xor( init_leaf_idx,
                                        (self.feature_rows[cidx] < 0).astype(np.int32),
                                        out = init_leaf_idx )
        
        # add other fern levels
        init_leaf_idx += self.node_idx
        assert np.all( init_leaf_idx < (self.num_nodes << 1) )

        # run the actual optimization
        res = lsearch_fern_param( param_candidates = param_candidates,
                                          node_idx = init_leaf_idx,
                                         label_idx = self.label_idx,
                                      data_weights = self.sample_weights,
                                         num_nodes = self.num_nodes << 1,
                                        num_labels = self.num_labels,
                                         objective = self.objective,
                                         penalizer = self.create_penalizer(cidx) )
        
        self.feature_weights[cidx] = res.prmVal
        self.normalize_weights()

        ret = dict( prm_val_penalty = res.prmValPenalty,
                    num_optim_steps = res.num_improvements,
                           co_scale = res.prmVal,
                     regul_strength = self.regul_strength )
        if self.objective == 'gini':
            ret["rating"] = res.nodeGiniRating
        elif self.objective == 'entropy':
            ret["rating"] = res.nodeEntropy
        else:
            assert False
        return ret

    # -------------------------------------------------------------------------------------------------

    def create_penalizer(self, cidx : int ):
        
        l1offset = float(np.absolute(self.feature_weights[:cidx]).sum()) + float(np.absolute(self.feature_weights[cidx+1:]).sum())
        sqrl2Offset = float(np.square(self.feature_weights[:cidx]).sum()) + float(np.square(self.feature_weights[cidx+1:]).sum())

        return ParamPenalizer.create_single_regularization( 
                                    regul_type = EPenalizerType.CENTRALIZED_L1, 
                                regul_strength = self.regul_strength,
                                  num_features = self.num_features,
                                      l1offset = l1offset,
                                   sqrl2Offset = sqrl2Offset,
                                       l1ratio = 1.0 )
    
    # -------------------------------------------------------------------------------------------------

    def optimize_split(self):
        # project features
        projections = project_features( self.feature_weights, self.feature_rows )  # [ N ]

        # process projection
        num_optim_steps = self.split_optimizer.process_feature( projections )
        
        # check if improvement was possible
        if num_optim_steps > 0:
            optimal_split, rating = self.split_optimizer.get_optimal_split()
            assert optimal_split.feature_idx == self.split_optimizer.last_feature_idx, "Invalid feature processed"
            self.feature_split = optimal_split.feature_split

            return dict(rating=rating, num_optim_steps=num_optim_steps)
        
        return None

    # -------------------------------------------------------------------------------------------------

    def get_coordinate_marginal(self, cidx : int):

        feature_mask = np.ones( self.num_features, dtype=np.bool )  # [ F ]
        feature_mask[cidx] = False

        # marginal feature selection
        m_feature_rows = create_feature_rows( \
                                num_data = self.num_data,
                            feature_rows = [ fr for (fr,fm) in zip( self.feature_rows, feature_mask ) if fm ],
                feature_row_pointer_list = [ frp for (frp,fm) in zip( self.feature_rows.feature_row_pointer_list, feature_mask ) if fm ] )
        
        # project margin features
        m_projection = project_features( self.feature_weights[feature_mask], m_feature_rows )  # [ N ]

        return np.subtract( self.feature_split, m_projection, out=m_projection )


    def normalize_weights(self):
        norm = 1. / np.linalg.norm( self.feature_weights )
        self.feature_weights *= norm
        self.feature_split *= norm
