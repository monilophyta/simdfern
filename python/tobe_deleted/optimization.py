# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["modify_hessian", "calculate_newton_update"]


import numpy as np
import scipy



def modify_hessian( hessian, stop_precission=1e-6 ):
    """
    Hessian Modification according to 
    Wright, Stephen, and Jorge Nocedal. "Numerical optimization." Springer Science (1999), page 145
    """
    num_dims = hessian.shape[0]

    # initialize modification relative to upper bound of largest eigenvalue
    hessian_frobenius = np.linalg.norm( hessian, ord="fro" )

    if hessian_frobenius < np.sqrt( np.finfo(hessian.dtype).eps ):
        raise np.linalg.LinAlgError( "Hessian (almost) zero" )

    max_mod = 0.5 * hessian_frobenius
    min_mod = 0.0

    # find upper bound
    while True:

        hessian_mod = hessian + np.diag( max_mod.repeat( num_dims ) )

        try:
            hessian_L = np.linalg.cholesky( hessian_mod )
        except np.linalg.LinAlgError:
            min_mod = max_mod
            max_mod *= 2
        else:
            break
    
    # braketing until sufficient small upper and lower bound for modifcation are found
    while ((max_mod - min_mod) / max_mod) > stop_precission:

        cur_mod = 0.5 * (max_mod + min_mod)
        hessian_mod = hessian + np.diag( cur_mod.repeat( num_dims ) )

        try:
            hessian_L = np.linalg.cholesky( hessian_mod )
        except np.linalg.LinAlgError:
            # reset lower bound
            min_mod = cur_mod
        else:
            # reset upper bound
            max_mod = cur_mod
    
    return (hessian_L, max_mod)



def calculate_newton_update(     gradient : np.ndarray, 
                                  hessian : np.ndarray,
                                step_size : float = 1.0,
                              limit_step_on_gradient_size : bool = False  ):
    
    assert gradient.size == hessian.shape[0]
    assert gradient.size == hessian.shape[1]
    assert np.all( hessian == hessian.transpose() )
    step_size = float( step_size )
    limit_step_on_gradient_size = bool(limit_step_on_gradient_size)


    hessian_min_diag = hessian.diagonal().min()

    if hessian_min_diag <= 0:
        # a hessian modification is definitly required
        hessian_L, hessian_modification = modify_hessian( hessian )
    else:
        hessian_modification = 0
        try:
            hessian_L = np.linalg.cholesky( hessian )
        except np.linalg.LinAlgError:
            # unfortunatedly a hessian modification is required
            hessian_L, hessian_modification = modify_hessian( hessian )
    
    assert np.all( hessian_L == np.tril( hessian_L ) )

    ## Solve H^-1 * grad according to
    ##  https://en.wikipedia.org/w/index.php?title=Cholesky_decomposition&oldid=950716724#Applications

    # forward substitution
    grad_y = scipy.linalg.solve_triangular( hessian_L, gradient, lower=True, trans='N', check_finite=True )  # [ num_weights ]

    # backward substitution
    delta_x = scipy.linalg.solve_triangular( hessian_L, grad_y, lower=True, trans='T', check_finite=True )  # [ num_weights ]

    if limit_step_on_gradient_size is True:
        # Limit update length on scaled size of gradient
        delta_x_norm = np.linalg.norm( delta_x )
        gradient_norm = np.linalg.norm( gradient )
        max_delta_scale = step_size * gradient_norm

        if delta_x_norm > gradient_norm:
            delta_x *= max_delta_scale / max( delta_x_norm, 1e-8 )
    
    elif hessian_modification > 0:
        # apply step size scale on update in cases where hessian has been modified
        delta_x *= step_size
    
    return (delta_x, hessian_modification)


