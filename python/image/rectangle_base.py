# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--




__all__ = [ "RectangleBase" ]

from collections import namedtuple
import numpy as np



RectangleTuple = namedtuple('RectangleTuple',
                           [ 'up',          # inside
                             'left',        # inside
                             'down',        # outside
                             'right' ] )


class RectangleBase( RectangleTuple ):

    @property
    def height(self):
        return (self.down - self.up)
    
    @property
    def width(self):
        return (self.right - self.left)

    @property
    def area(self):
        return (self.width * self.height)
