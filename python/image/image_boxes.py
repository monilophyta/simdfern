# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



__all__ = [ "ImageBoxes" ]


from collections.abc import Sequence
import numpy as np

from ..simd import as_aligned_array, is_array_fully_aligned
from .rectangle_base import RectangleBase
from .rectangle import Rectangle



class ImageBoxes( RectangleBase ):

    @property
    def size(self):
        return self.up.size


    def __new__(typ, up, left, down, right ):
        obj = RectangleBase.__new__(typ, \
            np.require( up, np.int32, [ 'C_CONTIGUOUS' ] ), \
            np.require( left, np.int32, [ 'C_CONTIGUOUS' ] ), \
            np.require( down, np.int32, [ 'C_CONTIGUOUS' ] ), \
            np.require( right, np.int32, [ 'C_CONTIGUOUS' ] ) )
        return obj
    

    def __init__(self, *args, **argv ):
        assert( self.check_size() )
        assert np.all( self.up <= self.down )
        assert np.all( self.left <= self.right )


    def check_size(self):
        return (     (self.size == self.left.size)
                 and (self.size == self.down.size)
                 and (self.size == self.right.size) )
    

    @property
    def aligned( self ):
        aligned_boxes = ImageBoxes( as_aligned_array( self.up, do_extend=True, overlap_value=0 ), \
                                    as_aligned_array( self.left, do_extend=True, overlap_value=0 ), \
                                    as_aligned_array( self.down, do_extend=True, overlap_value=0 ), \
                                    as_aligned_array( self.right, do_extend=True, overlap_value=0 ) )
        
        assert( aligned_boxes.size >= self.size )
        assert( is_array_fully_aligned( aligned_boxes.up ) )     # should have been tested already
        assert( is_array_fully_aligned( aligned_boxes.left ) )    # should have been tested already
        assert( is_array_fully_aligned( aligned_boxes.down ) )    # should have been tested already
        assert( is_array_fully_aligned( aligned_boxes.right ) )    # should have been tested already

        return aligned_boxes


    @property
    def union(self):
        up    = self.up.min()
        left  = self.left.min()
        down  = self.down.max()
        right = self.right.max()

        return Rectangle( up=up, left=left, down=down, right=right )
    

    def select(self, idx ):
        return ImageBoxes( up=self.up[idx], left=self.left[idx], down=self.down[idx], right=self.right[idx] )
