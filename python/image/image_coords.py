# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



from collections import namedtuple
import numpy as np
import sys

__all__ = [ "ImageCoords" ]


little_endian_order = sys.byteorder == 'little'
if little_endian_order == False:
    assert( sys.byteorder == 'big' )



ImageCoordsBase = namedtuple('ImageCoordsBase', [ 'data' ] )


class ImageCoords( ImageCoordsBase ):

    @property
    def size(self):
        return self.data.shape[0]
    

    @property
    def row_idx(self):
        if little_endian_order == True:
            return self.data[:,1]
        else:
            return self.data[:,0]

    @property
    def col_idx(self):
        if little_endian_order == True:
            return self.data[:,0]
        else:
            return self.data[:,1]


    def __new__(typ, rowIdx, colIdx ):
        obj = ImageCoordsBase.__new__(typ, typ.concat_idx( rowIdx, colIdx )  )
        return obj
    
    def __init__(self, *args, **argv):
        assert( self.data.shape[1] == 2 )
    

    @staticmethod
    def concat_idx( rowIdx, colIdx ):

        rowIdx = np.require( rowIdx, np.int32 )
        colIdx = np.require( colIdx, np.int32 )

        if little_endian_order == True:
            img_coords = np.stack( ( colIdx.ravel(), rowIdx.ravel() ), axis=1 )
        else:
            img_coords = np.stack( ( rowIdx.ravel(), colIdx.ravel() ), axis=1 )
        
        return np.require( img_coords, np.int32, [ 'C_CONTIGUOUS' ] )
    

    @classmethod
    def from_image_shape( cls, shape ):
        colIdx, rowIdx = np.meshgrid( np.arange( 0, shape[1], dtype=np.int32 ),
                                      np.arange( 0, shape[0], dtype=np.int32 ) )
        return cls( rowIdx.ravel(), colIdx.ravel() )

