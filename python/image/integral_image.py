# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--





import warnings
import numpy as np

__all__ = [ "compute_integral_image" ]



def compute_integral_image( img, int_dtype=None ):
    """
    Computes integral an integral image for integer and floating images
    Performs a rudimentary integer overflow check
    
    Parameters
    ----------
    img : integer or float numpy image
        shape is either [ H x W ] or [ H x W x C ]
    int_dtype : (optional9 dtype of integral image
        default is np.int32 for integer images and np.float32 for floating images
    
    Returns
    -------
    int_img : numpy array 
        shape dependent on input either [ H+1 x W+1 ] or [ H+1 x W+1 x C ]
    """
    if int_dtype is None:
        if np.issubdtype( img.dtype, np.floating ):
            int_dtype = np.dtype( np.float32 )
        elif np.issubdtype( img.dtype, np.integer ):
            int_dtype = np.dtype( np.int32 )
    else:
        int_dtype = np.dtype( int_dtype )
    
    if np.issubdtype( int_dtype, np.integer ) and np.issubdtype( img.dtype, np.floating ):
        warnings.warn( "integral image computation for %r image with integral image dtype %r" % (img.dtype,int_dtype) )

    # final shape of output
    intimg_shape = np.array( img.shape )
    intimg_shape[:2] += 1
    intimg_shape = tuple(intimg_shape)

    # intermediatly reshaping image
    img = img.reshape( img.shape[:2] + (-1,) )

    # add up vertically
    aux = np.add.accumulate( img, axis=0, dtype=int_dtype )

    # Precheck for overflow candidates
    do_overflow_check = False
    if np.issubdtype( int_dtype, np.integer ):
        max_aux = int( aux[-1,:,:].max() )
        if (max_aux * int(img.shape[1])) > int(np.iinfo( int_dtype ).max):
            warnings.warn( "Detected integer overflow candidate in integral image computation for image with shape %r and dtype %r which might exceeds MAX_INT=%d" % (img.shape, int_dtype, np.iinfo( int_dtype ).max) )
            do_overflow_check = True


    intimg = np.empty( intimg_shape[:2] + (img.shape[2],), dtype=int_dtype )

    if __debug__:
        intimg.fill( np.iinfo( intimg.dtype ).max )

    intimg[:,0,:] = 0
    intimg[0,:,:] = 0

    np.add.accumulate( aux, axis=1, out=intimg[1:,1:,:] )
    
    if do_overflow_check:
        # run overflow check over entire image
        if np.any( intimg[:-1,:-1,:] > intimg[1:,1:,:] ) or \
           np.any( intimg[1: ,:-1,:] > intimg[1:,1:,:] ) or \
           np.any( intimg[:-1,1: ,:] > intimg[1:,1:,:] ):
           message = "integer overflow dectected for image with shape %r and dtype %r" % (img.shape, int_dtype)
           assert False, message
           warnings.warn( message )

    intimg = intimg.reshape( intimg_shape )

    return intimg

