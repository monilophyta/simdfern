# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--




__all__ = ["Rectangle"]


import numpy as np
from .rectangle_base import RectangleBase
from .image_coords import ImageCoords



class Rectangle( RectangleBase ):

    def __new__(typ, up, left, down, right ):
        obj = RectangleBase.__new__(typ, int(up), int(left), int(down), int(right) )
        return obj


    def __init__(self, up, left, down, right ):
        super( Rectangle, self ).__init__()
        assert( up <= down )
        assert( left <= right )
    
    def union( self, other ):
        assert isinstance( other, Rectangle )
        return Rectangle( up    = min( self.up,    other.up ), \
                          left  = min( self.left,  other.left ), \
                          down  = max( self.down,  other.down ), \
                          right = max( self.right, other.right ) )


    def is_inside( self, other ):
        if isinstance( other, Rectangle ):
            check = (self.up >= other.up) and \
                    (self.left >= other.left ) and \
                    (self.down <= other.down ) and \
                    (self.right <= other.right)
        elif isinstance( other, ImageCoords ):
            check = (self.up <= other.row_idx ) & \
                    (self.left <= other.col_idx ) & \
                    (self.down > other.row_idx ) & \
                    (self.right > other.col_idx )
        else:
            # assuming array
            check = (self.up <= other[:,0]) & \
                    (self.left <= other[:,1] ) & \
                    (self.down > other[:,0] ) & \
                    (self.right > other[:,1] )
        
        return check
    

    def sub_image( self, img ):
        rslice = slice( self.up, self.down )
        cslice = slice( self.left, self.right )

        if img.ndim == 3:
            return img[rslice,cslice,:]
        elif img.ndim == 2:
            return img[rslice,cslice]
        else:
            assert(False)


    def compute_integral( self, integral_image ):
        if integral_image.ndim == 2:
            integral_image = integral_image[:,:,np.newaxis]
        
        assert( integral_image.ndim == 3 )

        up_left    = integral_image[self.up,self.left,:]  # inside image rect - outside int_image rect
        
        up_right   = integral_image[self.up,  self.right,:] # outside image rect - outside int_image rect
        down_left  = integral_image[self.down,self.left,:]  # outside image rect - outside int_image rect

        down_right = integral_image[self.down,self.right,:]  # outside image rect - inside int_image rect

        # checks for correct integral image
        assert( np.all( up_left <= up_right) )
        assert( np.all( up_right <= down_right) )
        assert( np.all( up_left <= down_left) )
        assert( np.all( down_left <= down_right) )

        intval = ( ( down_right - down_left ) + up_left ) - up_right

        return intval


