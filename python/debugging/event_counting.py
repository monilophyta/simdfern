# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["evtcnt"]


import logging
import atexit
from threading import Lock


class EventCounts(object):

    __count_lock : Lock
    __event_dict : dict

    @classmethod
    def get_instance(cls):
        try:
            it = cls.__it__
        except AttributeError:           
            it = cls.__it__ = cls()
        return it
    

    def __init__(self):
        try:
            if EventCounts.__it__ is not None:
                raise RuntimeError( "Second instantiation of singleton")
        except AttributeError:
            pass
        
        self.__count_lock = Lock()
        self.__event_dict = dict()
    

    def increment( self, evt_key : str, count : int = 1 ) -> int:
        with self.__count_lock:
            val = self.__event_dict[evt_key] = int(count) + self.__event_dict.get(evt_key,0)
        return val
    
    def get_counts(self):
        return list(self.__event_dict.items())

# ----------------------------------

def evtcnt(*args,**kwargs):
    return EventCounts.get_instance().increment(*args,**kwargs)

# ----------------------------------

# create logger
logger = logging.getLogger('py.counts')

if __debug__:
    logger.setLevel(logging.INFO)
else:
    logger.setLevel(logging.WARN)

def dump_count_stats(ec):
    for field_name, field_val in ec.get_counts():
        logger.info("%s=%d", field_name, field_val)

atexit.register(dump_count_stats, EventCounts.get_instance())


