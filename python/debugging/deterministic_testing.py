# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



__all__ = [ "DeterministicIOChecker" ]

import logging
import hashlib
import struct
import os
import os.path
import enum
import pickle
import atexit
import sys

import typing as tp
import numpy as np

# --------------------------------------------------------------------------------------

ENVIRONMENT_VAR_NAME="RF_DETERM_CHECK"


# --------------------------------------------------------------------------------------

# create logger
logger = logging.getLogger('debugging.determcheck')

if __debug__:
    logger.setLevel(logging.INFO)
else:
    logger.setLevel(logging.WARN)

#logger.addHandler( logging.StreamHandler(sys.stdout) )

# --------------------------------------------------------------------------------------


class Indexer( object ):

    def __init__(self):
        self.__hash = hashlib.md5()

    def update(self, *args):
        
        for x in args:
            if isinstance(x,int):
                bval = struct.pack("q",x)
            elif isinstance(x,float):
                bval = struct.pack("d",x)
            elif isinstance(x,str):
                bval = x.encode()
            elif isinstance(x, np.ndarray):
                bval = x.tobytes()
                assert len(bval) == (x.size * x.itemsize)
            elif isinstance( x, (bytes, bytearray) ):
                bval = x
            else:
                assert False, "Unknown how to hash type %r" % type(x)
                bval = repr(x).encode()
        
            self.__hash.update(bval)
        return self
    
    def digest(self):
        return self.__hash.digest()

# --------------------------------------------------------------------------------------

@enum.unique
class DIOCheckMode( enum.Flag ):
    """
    Enum values:
    ------------
    RECORD
        Do record input/output hash values and store for later validation.
    STORE_AT_EXIT
        Store loaded/recorded hash values/pairs when program exists.
    LOAD_ON_INIT
        Load existing hash values/pairs when on checker initializes
    CHECK_REOCCURENCE
        Verify that only previously recorded (bit-identical) input hash values occur (hash check)
    CHECK_DETERMINISM
        Verify that previously recorded (bit-identical) input always resulst in the same bit-identical output (hash check)
    """
    RECORD            = enum.auto()
    STORE_AT_EXIT     = enum.auto()
    LOAD_ON_INIT      = enum.auto()
    CHECK_REOCCURENCE = enum.auto()
    CHECK_DETERMINISM = enum.auto()



def default_DIOCheckMode() -> DIOCheckMode:

    dio_check_env_var = None

    if ENVIRONMENT_VAR_NAME in os.environ:
        dio_check_env_var = os.environ[ENVIRONMENT_VAR_NAME].upper()
        
        logger.info( "Using environment variable %r=%r", ENVIRONMENT_VAR_NAME, dio_check_env_var )

        if dio_check_env_var == "RECORD":
            check_mode = DIOCheckMode.RECORD | DIOCheckMode.STORE_AT_EXIT
        elif dio_check_env_var == "RECORD_AND_VERIFY":
            check_mode = DIOCheckMode.RECORD | DIOCheckMode.CHECK_DETERMINISM | DIOCheckMode.STORE_AT_EXIT
        elif dio_check_env_var == "LOAD_RECORD_AND_VERIFY":
            check_mode = DIOCheckMode.LOAD_ON_INIT | DIOCheckMode.RECORD | DIOCheckMode.CHECK_DETERMINISM | DIOCheckMode.STORE_AT_EXIT
        elif dio_check_env_var == "VERIFY_IO":
            check_mode = DIOCheckMode.LOAD_ON_INIT | DIOCheckMode.CHECK_REOCCURENCE | DIOCheckMode.CHECK_DETERMINISM
        elif dio_check_env_var == "VERIFY_INPUT":
            check_mode = DIOCheckMode.LOAD_ON_INIT | DIOCheckMode.CHECK_REOCCURENCE
        elif dio_check_env_var == "VERIFY_OUTPUT":
            check_mode = DIOCheckMode.LOAD_ON_INIT | DIOCheckMode.CHECK_DETERMINISM
        else:
            raise ValueError( "Invalid content in environment variable %s = '%s'" % (ENVIRONMENT_VAR_NAME,os.environ[ENVIRONMENT_VAR_NAME]) )
    else:
        check_mode = DIOCheckMode.RECORD | DIOCheckMode.CHECK_DETERMINISM

    return check_mode


# -------------------------------------------------------------------------------------------------------

def store_io_hash( iodict, fname ):
    with open(fname, "wb") as fout:
        pickle.dump( iodict, fout )
        logger.info( "Hash data base with %d hashes dumped in %s", len(iodict), fname )


# -------------------------------------------------------------------------------------------------------

class DeterministicIOChecker( object ):

    __iodict   : dict
    __mode     : DIOCheckMode
    __do_raise : bool


    def __init__(self, mode : tp.Optional[DIOCheckMode] = None, 
                      fname : tp.Optional[str] = None,
            raise_on_failed : bool = False ):

        if mode is None:
            mode = default_DIOCheckMode()
        assert isinstance( mode, DIOCheckMode )

        if (mode & DIOCheckMode.LOAD_ON_INIT) and os.path.isfile( fname ):
            assert fname is not None
            with open(fname, "rb") as fin:
                self.__iodict = pickle.load( fin )
                logger.info( "Hash loaded from %s with size %d", fname, len(self.__iodict) )
        else:
            self.__iodict = dict()
        
        if mode & DIOCheckMode.STORE_AT_EXIT:
            if fname is None:
                raise ValueError( "Missing filename for writing hash IO")
            
            fname = os.path.abspath(fname)
            
            if os.path.isfile(fname):
                if not os.access(fname, os.W_OK):
                    raise ValueError( "File %r not writeable" % fname )
            else:
                pdir = os.path.dirname(fname)
                if (not os.path.isdir( pdir )) or (not os.access(pdir, os.W_OK)):
                    raise ValueError( "Path %r not writable" % fname )

            atexit.register( store_io_hash, iodict=self.__iodict, fname=fname )

        self.__mode = mode
        self.__do_raise = bool(raise_on_failed)
    

    def signal_error(self, assertion):
        if self.__do_raise:
            raise assertion
        return False
    

    def __call__(self, inp : tp.Sequence,
                      outp : tp.Optional[tp.Sequence] = None ):
        
        if self.__mode & (DIOCheckMode.CHECK_REOCCURENCE | DIOCheckMode.RECORD | DIOCheckMode.CHECK_DETERMINISM ):
            inp_hash = self.hash_args( *inp )

            if (self.__mode & DIOCheckMode.CHECK_REOCCURENCE) and (inp_hash not in self.__iodict):
                return self.signal_error( NoReoccurenceError( inp_hash ) )

            if ((self.__mode & DIOCheckMode.RECORD) and (outp is not None)) or (self.__mode & DIOCheckMode.CHECK_DETERMINISM):
                outp_hash = self.hash_args( *outp )

                if (self.__mode & DIOCheckMode.CHECK_DETERMINISM) and (self.__iodict.get(inp_hash,outp_hash) != outp_hash):
                    return self.signal_error( NoDeterminismError(inp_hash, outp_hash, self.__iodict[inp_hash]) )
            else:
                outp_hash = None

            if self.__mode & DIOCheckMode.RECORD:
                self.__iodict[inp_hash] = outp_hash
        
        return True


    def hash_args(self, *args):
        hval = Indexer().update(*args).digest()
        return hval

    def hash_input(self, *args):
        hval = self.hash_args( *args )
        self.__iodict[hval] = None
    

    def hash_io(self, inp : tp.Sequence,
                     outp : tp.Sequence ):
        ikey = self.hash_args(*inp)
        okey = self.hash_args(*outp)
        self.__iodict[ikey] = okey
    

# -------------------------------------------------------------------------------------------------------


class NoReoccurenceError( AssertionError ):
    inp_hash : str
    def __init__(self, inp_hash : str ):
        super().__init__( "Hash '%s' not yet recorded" % inp_hash )
        self.inp_hash = inp_hash

class NoDeterminismError( AssertionError ):
    inp_hash : str
    outp_hash_observed : str
    outp_hash_stored : str

    def __init__(self, inp_hash : str, 
                       outp_hash_observed : str,
                       outp_hash_stored : str ):
        super().__init__( "Output '%s' for input '%s' does not match with output hash '%s'" % (outp_hash_observed,  inp_hash, outp_hash_stored ) )
        self.inp_hash = inp_hash  
        self.outp_hash_observed = outp_hash_observed
        self.outp_hash_stored = outp_hash_stored

