# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["TestCtypesInformationMeasures"]


import unittest
import numpy as np
from ..ctypes_py import calc_unormed_gini_index, calc_unormed_conditional_entropy
from ..clmath import mutual_counts


class TestCtypesInformationMeasures( unittest.TestCase ):

    rand_seed     = 69

    num_data      = 132437
    num_labels    = 17
    num_layers    = 7
    max_weight    = 123


    leaf_idx      = None
    label_idx     = None
    data_weight   = None


    @property
    def num_leafs(self):
        return (1 << self.num_layers)


    def __init__(self, *args, **kwargs):
        super(TestCtypesInformationMeasures, self).__init__(*args, **kwargs)
        self.sample_data()


    def test_calc_unormed_gini_index(self):
        
        # python variant of unormed gini index calculation
        mcounts = mutual_counts( self.leaf_idx, self.label_idx, weights=self.data_weight, num_X=self.num_leafs, num_Y=self.num_labels )
        #unormed_gini = mcounts.gini_index( axis=1, do_norm=False )
        py_unormed_node_gini = mcounts.unormed_node_gini_index( axis=1 )
        
        # c++ variant of unormed gini index calculation
        ct_unormed_node_gini = calc_unormed_gini_index( \
                            leaf_idx    = self.leaf_idx, 
                            label_idx   = self.label_idx, 
                            data_weight = self.data_weight,
                            num_leafs   = self.num_leafs,
                            num_labels  = self.num_labels )

        num_equal = (py_unormed_node_gini == ct_unormed_node_gini).sum()
        self.assertTrue( num_equal, py_unormed_node_gini.size )
    
    
    def test_calc_unormed_conditional_entropy(self):
        
        # python variant of conditional entropy calculation
        mcounts = mutual_counts( self.leaf_idx, self.label_idx, weights=self.data_weight, num_X=self.num_leafs, num_Y=self.num_labels )
        py_unormed_condentropy = mcounts.conditional_entropy( axis=1, do_norm=False, use_log2=False )
        
        # c++ variant of conditional entropy calculation
        ct_unormed_condentropy = calc_unormed_conditional_entropy( \
                            leaf_idx    = self.leaf_idx, 
                            label_idx   = self.label_idx, 
                            data_weight = self.data_weight,
                            num_leafs   = self.num_leafs,
                            num_labels  = self.num_labels )

        self.assertEqual( ct_unormed_condentropy.size, self.num_leafs )
        self.assertTrue( np.isclose( py_unormed_condentropy, ct_unormed_condentropy.sum() ) )


    def sample_data( self ):
        rand_state = np.random.RandomState( self.rand_seed )

        self.leaf_idx    = rand_state.randint( 0, self.num_leafs, self.num_data, dtype=np.int32 )
        self.label_idx   = rand_state.randint( 0, self.num_labels, self.num_data, dtype=np.int32 )
        self.data_weight = rand_state.randint( 0, self.max_weight, self.num_data, dtype=np.int32 )
        #self.data_weight = np.ones_like( self.leaf_idx )
