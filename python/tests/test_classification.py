# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



import unittest
#import sys
#sys.path.append('../')

from ..ctypes_py.tree_classification_layer import IndexedTreeLayer, FeatureRows, create_feature_rows
from ..simd import as_aligned_array
import numpy as np


class TestIndexTreeLayer( unittest.TestCase ):

    #img_size = (640,480)
    #img_dtype = np.int32

    num_data      = 1023  # = N
    num_simd_data = 1024  # = Ns

    num_tree_nodes = 4  # = T
    num_features   = 7  # = F


    featureIdx  : np.ndarray
    splitVals   : np.ndarray

    features     : np.ndarray
    feature_rows : FeatureRows
    nodeIdx      : np.ndarray

    simd_features     : np.ndarray
    simd_feature_rows : FeatureRows
    simd_nodeIdx      : np.ndarray



    def __init__(self, *args, **kwargs):
        super(TestIndexTreeLayer, self).__init__(*args, **kwargs)
        self.sample_data()



    def test_classification(self):

        # create index tree layer
        idxtreeLayer = IndexedTreeLayer( self.featureIdx, self.splitVals )

        # classify indices
        trg_nodeIdx = idxtreeLayer.classify( self.nodeIdx, self.feature_rows )

        # python based index classification
        py_trg_nodeIdx = self.pythonic_classification( self.features, self.nodeIdx )
        
        # check equalness
        self.assertTrue( np.all(py_trg_nodeIdx == trg_nodeIdx) )
    

    def test_simd_classification(self):

        # create index tree layer
        idxtreeLayer = IndexedTreeLayer( self.featureIdx, self.splitVals )

        # classify indices
        trg_nodeIdx = idxtreeLayer.classify( self.simd_nodeIdx, self.simd_feature_rows )

        # python based index classification
        py_trg_nodeIdx = self.pythonic_classification( self.simd_features, self.simd_nodeIdx )
        
        # check equalness
        self.assertTrue( np.all(py_trg_nodeIdx == trg_nodeIdx) )



    def sample_data(self):
        # create feature indices
        self.featureIdx = np.random.randint(0, self.num_features-1, self.num_tree_nodes, dtype=np.int32)  # [ T ]

        # create split values [ 0 ... num_features-1 ]
        self.splitVals = np.arange( self.num_features, dtype=np.float32 ).take( self.featureIdx )    # [ T ]

        # sample features
        features = np.random.randn( self.num_features, self.num_data ).astype( dtype=np.float32 ) # [ F x N ]
        features += np.arange( self.num_features, dtype=np.float32 )[:,np.newaxis]
        self.features = features

        # convert to feature rows
        feature_rows = create_feature_rows()
        feature_rows.extend( [ features[i,:] for i in range(self.num_features) ] )
        self.feature_rows = feature_rows

        # sample Node indices
        self.nodeIdx = np.random.randint( 0, self.num_tree_nodes-1, self.num_data, dtype=np.int32 ) # [ N ]

        # sample simd features
        simd_features = np.random.randn( self.num_features, self.num_simd_data ).astype( dtype=np.float32 ) # [ F x Ns ]
        self.simd_features = simd_features

        # convert to feature rows
        simd_feature_rows = create_feature_rows()
        simd_feature_rows.extend( [ np.require( simd_features[i,:], np.float32, ["C_CONTIGUOUS", "ALIGNED"] ) for i in range(self.num_features) ] )
        self.simd_feature_rows = simd_feature_rows
        
        # sample simd indices
        self.simd_nodeIdx  = np.random.randint( 0, self.num_tree_nodes-1, self.num_simd_data, dtype=np.int32 ) # [ Ns ]
        self.simd_nodeIdx = as_aligned_array( self.simd_nodeIdx.astype( np.int32 ) )


    def pythonic_classification( self, features : np.ndarray,
                                        nodeIdx : np.ndarray ):
        # features: [ F x N(s) ]
        # nodeIdx: [ N(s) ]

        assert isinstance( features, np.ndarray )
        assert isinstance( nodeIdx, np.ndarray )
        assert( np.all(nodeIdx >= 0) )
        assert( np.all(nodeIdx < self.num_tree_nodes) )

        splitVals = self.splitVals.take( nodeIdx )  #  [ N ]
        featureIdx = self.featureIdx.take( nodeIdx )  #  [ N ]

        N = nodeIdx.size

        features = features[ (featureIdx,np.arange(N)) ] # N
        assert( features.size == N )

        idx_addon = (features < splitVals).astype(np.int32)
        assert( np.all(idx_addon >= 0) )
        assert( np.all(idx_addon <= 1) )

        nodeIdx = (nodeIdx << 1) + idx_addon

        return nodeIdx




if __name__ == '__main__':
    unittest.main()
