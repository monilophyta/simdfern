# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



import unittest
#import sys
#sys.path.append('../')

from ..image import compute_integral_image
import numpy as np



class TestIntegralImage( unittest.TestCase ):

    img_size = (640,480)
    img_dtype = np.int32


    def test_integral_image(self):
        
        # create random image
        img = np.random.randint( 0, 1023, size=self.img_size, dtype=self.img_dtype )

        # compute integral image
        intimg = compute_integral_image( img )

        # compute original image out of integral image
        leftUp    = intimg[:-1,:-1]
        rightDown = intimg[1:,1:]
        rightUp   = intimg[:-1,1:]
        leftDown  = intimg[1:,:-1]
        
        img_check = rightDown - rightUp + leftUp - leftDown

        # check equalness
        self.assertTrue( np.all(img == img_check) )



if __name__ == '__main__':
    unittest.main()

