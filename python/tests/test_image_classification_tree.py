# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



import unittest
#import sys
#sys.path.append('../')

from ..ctypes_py import IndexedTreeLayer
from ..training import TreeTrainer, TreeTrainingContext

from ..data_sampling import ImageSequenceSubsetSampler, ReceptiveFieldSampler, sample_data_weights
from ..ctypes_py import BoxFeatureEvaluator
from ..image import compute_integral_image
from ..classification import ClassificationTree, ImageClassificationTree


import numpy as np
from skimage.transform import resize


# create dummy logger
import logging
dummy_logger = logging.getLogger( "test_digits_training" )
dummy_logger.setLevel( logging.WARNING )



class ImageClassificationTreeTest( unittest.TestCase ):

    rand_seed = 49
    rand_state = None

    # Parameters
    image_shape = (1024, 768)

    num_channels = 3
    num_layers = 3

    image_label_shape = (3,2)
    num_labels = image_label_shape[0] * image_label_shape[1]

    max_rfield_height = 31
    max_rfield_width = 21

    num_train_data = 500
    num_features = 100
    min_box_size = 2
    max_box_size = 7

    ##################################################################################

    # Generated Data
    rand_state = None
    label_image = None
    channel_integrals = None

    # selected positions for training
    train_positions = None  #  [ N ]
    train_labels    = None  # [ N ]
    train_weights   = None  # [ N ]

    # sampled boxes for feature generation
    feature_boxes = None     # [ nF ]
    feature_channels = None  # [ nF ]

    # features from sampled box
    train_features  = None      # [ nF x N ]

    # Results after training
    trained_layers = None
    trained_nodeIdx = None
    used_feature_indices = None

    ##################################################################################

    def __init__(self, *args, **argv ):
        super().__init__( *args, **argv )

        # build up data
        self.create_test_data()
        self.sample_train_data_positions()
        self.sample_train_data_features()
        self.train_tree()


    ##################################################################################

    def test_ratio_classification(self):
        ## prepare input
        selected_feature_boxes = [ b.select( self.used_feature_indices ) for b in self.feature_boxes ]
        selected_feature_channels = self.feature_channels.take( self.used_feature_indices )

        selected_channels        = list()
        selected_box_evaluator   = list()
        selected_feature_mapping = np.empty_like( self.used_feature_indices )
        selected_feature_mapping.fill( np.iinfo( selected_feature_mapping.dtype ).min )  # for debugging

        cur_num_features = 0

        for c_idx in range( self.num_channels ):
            f_idx = np.flatnonzero(selected_feature_channels == c_idx)

            if f_idx.size == 0:
                continue
            
            selected_channels.append( self.channel_integrals[c_idx] )

            box_eval = BoxFeatureEvaluator( selected_feature_boxes[0].select( f_idx ), \
                                            selected_feature_boxes[1].select( f_idx ) )
            
            selected_box_evaluator.append( box_eval )

            selected_feature_mapping[f_idx] = np.arange( cur_num_features, cur_num_features + f_idx.size, dtype=np.int32 )
            
            cur_num_features += f_idx.size  # for next loop

        # check valid feature mapping
        self.assertTrue( np.all(selected_feature_mapping[f_idx] >= 0) )
        
        ## run classification
        image_classifier = ImageClassificationTree( self.trained_layers, selected_box_evaluator, selected_feature_mapping )
        node_idx_image = image_classifier.classify( selected_channels )
        #print( "Eval: ", node_idx_image.shape, np.unique(node_idx_image.ravel()) )

        # Extract node idx for training data
        node_idx_classification = node_idx_image[self.train_positions.row_idx, self.train_positions.col_idx]

        ## Final Test
        self.assertTrue( np.all(self.trained_nodeIdx == node_idx_classification) )


    ##################################################################################

    def test_diff_classification(self):

        ## restructure layers for box features
        box_diff_tree, feature_map_idx, box_lambda = ClassificationTree( self.trained_layers ).convert_ratio_to_diff()
        
        ## prepare input
        used_feature_indices = self.used_feature_indices[feature_map_idx]
        
        selected_feature_boxes = [ b.select( used_feature_indices ) for b in self.feature_boxes ]
        selected_feature_channels = self.feature_channels.take( used_feature_indices )

        selected_channels        = list()
        selected_box_evaluator   = list()
        selected_feature_mapping = np.empty_like( used_feature_indices )
        selected_feature_mapping.fill( np.iinfo( selected_feature_mapping.dtype ).min )  # for debugging
        
        cur_num_features = 0
        for c_idx in range( self.num_channels ):
            f_idx = np.flatnonzero(selected_feature_channels == c_idx)

            if f_idx.size == 0:
                continue
            
            selected_channels.append( self.channel_integrals[c_idx] )

            box_eval = BoxFeatureEvaluator( selected_feature_boxes[0].select( f_idx ), \
                                            selected_feature_boxes[1].select( f_idx ), \
                                            box_lambda.take( f_idx ) )
            
            selected_box_evaluator.append( box_eval )

            selected_feature_mapping[f_idx] = np.arange( cur_num_features, cur_num_features + f_idx.size, dtype=np.int32 )
            
            cur_num_features += f_idx.size  # for next loop


        # check valid feature mapping
        self.assertTrue( np.all(selected_feature_mapping[f_idx] >= 0) )

        ## run classification
        image_classifier = ImageClassificationTree( box_diff_tree.tree_layers, selected_box_evaluator, selected_feature_mapping )
        node_idx_image = image_classifier.classify( selected_channels )

        # Extract node idx for training data
        node_idx_classification = node_idx_image[self.train_positions.row_idx, self.train_positions.col_idx]

        ## Final Test
        self.assertTrue( np.all(self.trained_nodeIdx == node_idx_classification) )


    ##################################################################################


    def create_test_data( self ):
        self.rand_state = np.random.RandomState( self.rand_seed )

        # generate label image
        uscale = self.num_channels
        dscale = 1. / uscale

        small_label_image = np.arange( self.num_labels, dtype=np.float ) * dscale
        small_label_image = small_label_image.reshape( self.image_label_shape )

        self.label_image = np.round( uscale * resize( small_label_image, self.image_shape ) ).astype(np.int32)

        # generate data images
        uscale = self.num_channels + 1
        dscale = 1. / uscale

        small_channels = [ (1+self.rand_state.permutation( self.num_labels )).reshape( self.image_label_shape ) for i in range(self.num_labels) ]
        channels = [ resize( c.astype(np.float) * dscale, self.image_shape ) for c in small_channels  ]
        image_channels = [ np.round( (c * uscale) + np.absolute( self.rand_state.normal( 0.0, 0.175, size=c.shape ) ) ).astype(np.int32) for c in channels ]
        
        # This conditions is required to prevent special case hadling in box feature calculations
        assert( np.all( [ np.all(c > np.finfo(np.float32).eps) for c in image_channels ] ) )

        self.channel_integrals = [ compute_integral_image( x ) for x in image_channels ]


    def sample_train_data_positions( self ):
        # label frequency image
        xv,yv = np.meshgrid( np.arange(self.image_shape[0]), np.arange(self.image_shape[1]), sparse=False, indexing='ij' )
        label_frequency_image = np.zeros(self.image_shape + (self.num_labels,), dtype=np.int32 )
        label_frequency_image[xv.ravel(),yv.ravel(),self.label_image.ravel()] = 1

        # sample receptive field
        rfield_sampler   = ReceptiveFieldSampler( self.max_rfield_height, self.max_rfield_width )
        self.receptive_field = rfield_sampler.draw_uniform_sample( self.rand_state )

        frame_section = self.receptive_field.frame_section( self.image_shape[0], self.image_shape[1] )

        # Data Postion sampler
        position_sampler = ImageSequenceSubsetSampler( label_frequency_image )
        assert( position_sampler.num_classes == self.num_labels )
        equal_class_distribution = np.array( [1. / position_sampler.num_classes], dtype=np.float32 ).repeat( position_sampler.num_classes )
        sequential_pixel_selector = position_sampler.create_frame_data_sampler( frame_section,
                                                                                self.num_train_data,
                                                                                class_weights = equal_class_distribution,  # default is natural class distribution
                                                                                weight_frequency_fading = 1.0,
                                                                                rand_state=self.rand_state )
        
        # sample image coordinates
        positions, labels, selected_weights = sequential_pixel_selector.sample_data( self.label_image )
        self.train_positions = positions
        self.train_labels = labels
        self.train_weights = selected_weights


    def sample_train_data_features(self):

        num_features_per_channel = self.rand_state.multinomial( self.num_features, self.num_channels*[1. / self.num_channels] )  # [ nC ]
        
        # channel index of each feature
        channel_indices = np.arange( self.num_channels, dtype=np.int32 ).repeat( num_features_per_channel )  # [ nF ]

        # sampling boxes
        box1 = self.receptive_field.draw_uniform_box_samples( self.num_features, self.min_box_size, self.max_box_size, self.rand_state )
        box2 = self.receptive_field.draw_uniform_box_samples( self.num_features, self.min_box_size, self.max_box_size, self.rand_state )

        # looping of features
        feature_values = list()
        channel_end = np.add.accumulate( num_features_per_channel )
        channel_start = np.concatenate( ([0], channel_end[:-1]), axis=0 )

        for idx, (s,e) in enumerate( zip( channel_start, channel_end ) ):
            if s >= e:
                continue
            
            box_eval = BoxFeatureEvaluator( box1.select( slice(s,e) ), box2.select( slice(s,e) ) )
            feature_vals = box_eval.process( self.channel_integrals[idx], self.train_positions )  # [ N x dc ]

            feature_values.append( feature_vals.transpose() )  # [ dc x N ]

        feature_values = np.concatenate( feature_values, axis=0 )   # [ nF x N  ]

        self.train_features = np.require( feature_values, np.float32, [ 'C_CONTIGUOUS' ] )
        self.feature_boxes = (box1, box2)
        self.feature_channels = channel_indices

        assert( self.feature_channels.size == self.num_features )
        assert( self.train_features.shape[1] == self.train_labels.size )
        assert( self.feature_boxes[0].size == self.num_features )
        assert( self.train_features.shape[0] == self.num_features )




    def train_tree( self ):

        data_weights = sample_data_weights( self.train_labels, \
                                            eq_distr_fading = 0, \
                                            rands=self.rand_state )
        
        # Instantiate Tree Trainer
        trainer = TreeTrainer( self.train_labels, data_weights, logger=dummy_logger )

        # training context
        train_context = TreeTrainingContext( self.train_features )

        node_ratings = trainer.train_tree( train_context, self.num_layers )

        # get used features indixes
        used_feature_indices = np.unique( np.concatenate( [ l.feature_idx for l in train_context.layers ], axis=0 ) )
        self.used_feature_indices = used_feature_indices[used_feature_indices >= 0]
        #print("used_feature_indices ", self.used_feature_indices)

        # remapping feature indices
        index_map = np.empty( self.used_feature_indices.max()+3, dtype=np.int32 )
        index_map.fill( np.iinfo(np.int32).max )  # for debugging
        index_map[self.used_feature_indices] = np.arange( self.used_feature_indices.size, dtype=np.int32 )
        index_map[-1] = -1
        index_map[-2] = -2
        
        self.trained_layers = [ l.remap_feature_indices( index_map ) for l in train_context.layers ]
        self.trained_nodeIdx = train_context.leaf_idx
