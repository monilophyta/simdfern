# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

import unittest

import numpy as np

from .test_training_test_base import DebugFernTrainingObserver, DebugTreeTrainingObserver, DebugCombinedTrainingObserver
from .test_digits_training_base import DigitsTrainingBase
from .performance_tracking import TestPerformanceTracker

from ..parameter_registration import default_tree_training_params, default_fern_training_params, default_combined_training_params

from ..training import TreeTrainer, FernTrainer
from ..training import TreeTrainingContext, FernTrainingContext
from ..training import CombinedTrainer, CombinedTrainingContext
from ..training import ONS

from ..clmath import normalize_entropy, normalize_gini, MultiNodeClassStatistics


# create dummy logger
import logging
dummy_logger = logging.getLogger( "test_digits_training" )
dummy_logger.setLevel( logging.WARNING )

# ------------------------------------------------------------------------------------------

class DigitsTrainingTest( DigitsTrainingBase, unittest.TestCase ):

    num_tree_layers = 5
    num_fern_layers = 3
    num_combined_layers = 3

    pef_tracker : TestPerformanceTracker

    # ------------------------------------------------------------------------------------------

    def __init__(self, *args, **argv ):
        DigitsTrainingBase.__init__(self)
        unittest.TestCase.__init__( self, *args, **argv )
        self.pef_tracker = TestPerformanceTracker.get_instance("digits_training_pef.pickle")


    # ------------------------------------------------------------------------------------------------------------
    # ******** Tree Training *************************************************************************************
    # ------------------------------------------------------------------------------------------------------------

    def test_tree_training(self):
        
        tree_features = self.raw_data
        #tree_features = self.ratio_features

        trainer = TreeTrainer( self.labels, self.data_weights, logger=dummy_logger )

        # Debugging Observer
        observer = DebugTreeTrainingObserver( self )

        # Training Context
        train_context = TreeTrainingContext( tree_features, observer )

        # test call
        trainer.train_tree( train_context, self.num_tree_layers )

        # get history of rating
        node_ratings = train_context.observer.ratings

        self.assertEqual( train_context.num_layers, self.num_tree_layers )
        self.assertEqual( len(node_ratings), self.num_tree_layers )
        self.assertTrue( np.all( np.absolute( train_context.leaf_idx ) < 2**(self.num_tree_layers) ) )

        # check that classification is consistent after training
        self.assertTrue( trainer.verify_classification( train_context, do_throw=False ) )

        # -----------------------------------------------------------------

        mutual_inf_measure = np.array( observer.mutual_inf_measure, dtype=np.float32 )
        #print( "Tree - mutual information: ", mutual_inf_measure )
        self.assertTrue( np.all( mutual_inf_measure[:-1] <= mutual_inf_measure[1:] ) )
        self.assertGreaterEqual( mutual_inf_measure[-1], 2.607 )
        
        gini_measure = np.array( observer.gini_measure, dtype=np.float32 )
        #print( "Tree - gini_measure: ", gini_measure )
        self.assertTrue( np.all( gini_measure[:-1] <= gini_measure[1:] ) )
        self.assertGreaterEqual( gini_measure[-1], 0.947 )
        
        # -----------------------------------------------------------------
        
        for lidx in range(1,self.num_tree_layers):
            # check for rating improvements
            self.assertTrue( np.all( node_ratings[lidx-1] >= (node_ratings[lidx][::2] + node_ratings[lidx][1::2]) ) )

        for lidx, ratings in enumerate( node_ratings ):
            # checking correctness of rating
            rating_sum = ratings.sum()

            assert rating_sum <= 0  # using gini measure only
            normed_rating = normalize_gini( -rating_sum, self.data_magnitude, self.num_labels )
            self.assertAlmostEqual( normed_rating, gini_measure[lidx], 6 )
    
    
    # ------------------------------------------------------------------------------------------------------------
    # ******** Fern Training *************************************************************************************
    # ------------------------------------------------------------------------------------------------------------

    def test_fern_training_with_subsequent_optimization_tc1p1(self):
        """Test Case 1.1"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso" % self.num_fern_layers )

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "N" )

        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso_TC1.1" % self.num_fern_layers )


    def test_fern_training_with_subsequent_optimization_tc1p2(self):
        """Test Case 1.2"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso" % self.num_fern_layers )

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "LN" )

        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso_TC1.2" % self.num_fern_layers )


    def test_fern_training_with_subsequent_optimization_tc1p3(self):
        """Test Case 1.3"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso" % self.num_fern_layers )

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "NL" )
        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso_TC1.3" % self.num_fern_layers )


    def test_fern_training_with_subsequent_optimization_tc1p4(self):
        """Test Case 1.4"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso" % self.num_fern_layers )

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "LNL" )
        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso_TC1.4" % self.num_fern_layers )


    def test_fern_training_with_subsequent_optimization_tc2(self):
        """Test Case 2"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso" % self.num_fern_layers )

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "L" )
        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso_TC2" % self.num_fern_layers )


    def test_fern_training_with_subsequent_optimization_tc3(self):
        """Test Case 3"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso" % self.num_fern_layers )

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "[WS]*" )
        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso_TC3" % self.num_fern_layers )


    def test_fern_training_with_subsequent_optimization_tc4p1(self):
        """Test Case 4.1"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso" % self.num_fern_layers )

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "[WS]*N" )

        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso_TC4.1" % self.num_fern_layers )


    def test_fern_training_with_subsequent_optimization_tc4p2(self):
        """Test Case 4.2"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso" % self.num_fern_layers )

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "N[WS]*" )

        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso_TC4.2" % self.num_fern_layers )


    def test_fern_training_with_subsequent_optimization_tc4p3(self):
        """Test Case 4.3"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso" % self.num_fern_layers )

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "[WS]*N[WS]*" )

        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso_TC4.3" % self.num_fern_layers )
    
    
    def test_fern_training_with_subsequent_optimization_tc5(self):
        """Test Case 5"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso" % self.num_fern_layers )

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "GS" )

        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso_TC5" % self.num_fern_layers )


    def test_fern_training_with_subsequent_optimization_tc6(self):
        """Test Case 6"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso" % self.num_fern_layers )

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "AS" )

        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso_TC6" % self.num_fern_layers )


    def test_fern_training_with_subsequent_optimization_tc7p1(self):
        """Test Case 7.1"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso" % self.num_fern_layers )

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "PS" )

        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso_TC7" % self.num_fern_layers )


    def test_fern_training_with_subsequent_optimization_tc7p2(self):
        """Test Case 7.2"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso" % self.num_fern_layers )

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "PSNPS" )

        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context, tag="fern%d_sso_TC7p2" % self.num_fern_layers )


    # ----------------------------------------------------------------------------------------------------------------
    # 
    # ----------------------------------------------------------------------------------------------------------------

    def test_fern_training_with_parallel_optimization_tc1p1(self):
        """Test Case 1.1"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "N" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_po_TC1.1" % self.num_fern_layers )
    

    def test_fern_training_with_parallel_optimization_tc1p2(self):
        """Test Case 1.2"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "LN" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_po_TC1.2" % self.num_fern_layers )


    def test_fern_training_with_parallel_optimization_tc1p3(self):
        """Test Case 1.3"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "NL" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_po_TC1.3" % self.num_fern_layers )


    def test_fern_training_with_parallel_optimization_tc1p4(self):
        """Test Case 1.4"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "LNL" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_po_TC1.4" % self.num_fern_layers )


    def test_fern_training_with_parallel_optimization_tc2(self):
        """Test Case 2"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "L" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_po_TC2" % self.num_fern_layers )


    def test_fern_training_with_parallel_optimization_tc3(self):
        """Test Case 3"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "[WS]*" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_po_TC3" % self.num_fern_layers )


    def test_fern_training_with_parallel_optimization_tc4p1(self):
        """Test Case 4.1"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "[WS]*N" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_po_TC4.1" % self.num_fern_layers )


    def test_fern_training_with_parallel_optimization_tc4p2(self):
        """Test Case 4.2"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "N[WS]*" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_po_TC4.2" % self.num_fern_layers )


    def test_fern_training_with_parallel_optimization_tc4p3(self):
        """Test Case 4.3"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "[WS]*N[WS]*" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_po_TC4.3" % self.num_fern_layers )


    def test_fern_training_with_parallel_optimization_tc5(self):
        """Test Case 5"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "GS" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_po_TC5" % self.num_fern_layers )


    def test_fern_training_with_parallel_optimization_tc6(self):
        """Test Case 6"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "AS" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_po_TC6" % self.num_fern_layers )


    def test_fern_training_with_parallel_optimization_tc7p1(self):
        """Test Case 7"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "PS" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_po_TC7" % self.num_fern_layers )

    def test_fern_training_with_parallel_optimization_tc7p2(self):
        """Test Case 7"""
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "PSNPS" )
        self.evaluate_fern( trainer, train_context, tag="fern%d_po_TC7p2" % self.num_fern_layers )


    def run_fern_training(self, layer_optimization_steps : str ):

        fern_features = self.raw_data

        # configure training parameters
        parameter_registry = default_fern_training_params.copy()
        parameter_registry.set_param( 'logistic_regression_algo.optim_method', None )
        parameter_registry.set_param( 'logistic_regression_algo.max_num_iters', 5 )  # very slow otherwise

        # creater trainer instance
        trainer = FernTrainer( self.labels, self.data_weights, logger=dummy_logger, default_params=parameter_registry )

        # Debugging Observer
        observer = DebugFernTrainingObserver( self )
        #observer = None

        # Training Context
        train_context = FernTrainingContext( observer )
        train_context.feature_rows = fern_features

        # Training Context
        node_ratings = trainer.train_fern( train_context, 
                                           self.num_fern_layers,
                                           layer_optimization_steps = layer_optimization_steps )
        
        #print( "FernTrainingContext", train_context.observer.calls )

        self.assertEqual( len(node_ratings), self.num_fern_layers )
        self.assertTrue( np.all( node_ratings[:-1] >= node_ratings[1:] ) )

        return (trainer, train_context)



    def evaluate_fern(self, trainer, train_context, tag : str ):
        observer = train_context.observer

        # -----------------------------------------------------------------

        self.assertEqual( train_context.num_layers, self.num_fern_layers )
        self.assertTrue( np.all( train_context.leaf_idx < 2**(self.num_fern_layers) ) )

        # check that classification is consistent after training
        self.assertTrue( trainer.verify_classification( train_context, do_throw=False ) )

        # -----------------------------------------------------------------

        ratings = observer.ratings
        total_ratings = observer.total_ratings
        has_improvements = np.logical_or( total_ratings[:-1] >= total_ratings[1:], ratings[:-1] >= ratings[1:] )
        self.assertTrue( np.all( has_improvements ) )

        gini_measure = np.array( observer.gini_measure, dtype=np.float32 )
        #self.assertTrue( np.all( gini_measure[:-1] <= gini_measure[1:] ) )

        mutual_inf_measure = np.array( observer.mutual_inf_measure, dtype=np.float32 )

        # ----------------------------------------------------------------------------------
        # check rating (unormaled negative gini indices)
        normed_ratings = normalize_gini( -1 * np.array( observer.ratings, dtype=np.int32 ), self.data_magnitude, self.num_labels )
        self.assertTrue( np.all( np.isclose( normed_ratings, gini_measure ) ) )
        
        # ----------------------------------------------------------------------------------
        # store results in pef tracker
        self.pef_tracker.add( tag, gini = gini_measure[-1], mutual = mutual_inf_measure[-1] )



    # ------------------------------------------------------------------------------------------------------------
    # ******** Combined Training *********************************************************************************
    # ------------------------------------------------------------------------------------------------------------

    def run_combined_training(self, retrain_weakest_layer : bool,
                               layer_contribution_measure : str,
                                 layer_optimization_steps : str ):

        # configure training parameters
        combined_trainer_params = default_combined_training_params.copy()
        combined_trainer_params["train_initial_fern.layer_optimization_steps"] = layer_optimization_steps
        combined_trainer_params["remove_weakest_layer.layer_contribution_measure"] = layer_contribution_measure

        fern_trainer_params = default_fern_training_params.copy()
        fern_trainer_params["optimize_fern.layer_optimization_steps"] = layer_optimization_steps
        fern_trainer_params['logistic_regression_algo.max_num_iters'] = 5 # very slow otherwise

        trainer = CombinedTrainer( self.labels, self.data_weights, logger=dummy_logger, 
                                  combined_trainer_params = combined_trainer_params,
                                  fern_trainer_params = fern_trainer_params )

        # tree features and fern feature getter
        tree_feature_rows = self.raw_data
        fern_feature_getter = lambda idx, split: (tree_feature_rows[idx,:].transpose() - split).transpose()

        observer = DebugCombinedTrainingObserver( self )

        # Training Context
        orig_train_context = CombinedTrainingContext( tree_feature_rows = tree_feature_rows,
                                                    fern_feature_getter = fern_feature_getter,
                                                               observer = observer )

        # -----------------------------------------------------------------

        # test call
        train_context = trainer.train_fern( train_context = orig_train_context, 
                                          num_fern_layers = self.num_combined_layers,
                                    max_num_fern_features = orig_train_context.num_tree_features,
                                max_num_feature_extension = 1,
                                     num_init_tree_layers = 2,
                                    retrain_weakest_layer = retrain_weakest_layer )
        
        # -----------------------------------------------------------------

        self.assertEqual( train_context.num_layers, self.num_combined_layers )
        self.assertTrue( np.all( train_context.leaf_idx < 2**(self.num_combined_layers) ) )

        # check that classification is consistent after training
        self.assertTrue( trainer.fern_trainer.verify_classification( train_context, do_throw=False ) )

        # verify correct rating
        normed_ratings = normalize_gini( -1 * np.array( observer.ratings ), self.data_magnitude, self.num_labels )
        self.assertTrue( np.all( np.isclose( normed_ratings, observer.gini_measure ) ) )

        # extract combined optimization mask
        coptim_set = frozenset( [ ONS.INITIALLY_TRAINED_FERN, 
                                  ONS.IMPROVED_FERN ] )
        
        coptim_mask = [ (ms in coptim_set) for ms in train_context.observer.measure_source ]
        coptim_mask = np.array( coptim_mask, dtype=np.bool )
        
        # check consistency in rating development
        ratings = np.array( observer.ratings )[coptim_mask]
        self.assertTrue( np.all( ratings[:-1] >= ratings[1:] ) )

        return train_context


    def test_combined_training_tc1(self):
        tag="combined%d_TC1" % self.num_combined_layers
        
        train_context = self.run_combined_training( retrain_weakest_layer = True,
                                               layer_contribution_measure = "gini",
                                                 layer_optimization_steps = "N" )

        #print( "tc1: best gini index: ", train_context.observer.gini_measure[-1] )
        #print( "tc1: best mutual information: ", train_context.observer.mutual_inf_measure[-1])
        
        # store results in pef tracker
        self.pef_tracker.add( tag, gini = train_context.observer.gini_measure[-1], mutual = train_context.observer.mutual_inf_measure[-1] )


    def test_combined_training_tc2(self):
        tag="combined%d_TC2" % self.num_combined_layers

        train_context = self.run_combined_training( retrain_weakest_layer = False,
                                               layer_contribution_measure = "mutual",
                                                 layer_optimization_steps = "N" )

        #print( "tc2: best gini index: ", train_context.observer.gini_measure[-1] )
        #print( "tc2: best mutual information: ", train_context.observer.mutual_inf_measure[-1])
        
        # store results in pef tracker
        self.pef_tracker.add( tag, gini = train_context.observer.gini_measure[-1], mutual = train_context.observer.mutual_inf_measure[-1] )


    def test_combined_training_tc3(self):
        tag="combined%d_TC3" % self.num_combined_layers

        train_context = self.run_combined_training( retrain_weakest_layer = True,
                                               layer_contribution_measure = "gini",
                                                 layer_optimization_steps = "LNL" )

        #print( "tc3: best gini index: ", train_context.observer.gini_measure[-1] )
        #print( "tc3: best mutual information: ", train_context.observer.mutual_inf_measure[-1])
        
        # store results in pef tracker
        self.pef_tracker.add( tag, gini = train_context.observer.gini_measure[-1], mutual = train_context.observer.mutual_inf_measure[-1] )

# ------------------------------------------------------------------------------------------


if __name__ == '__main__':
    #unittest.main()
    digits = DigitsTrainingTest()

    # import cProfile
    # pr = cProfile.Profile()
    # pr.enable()
    # pr.run("digits.test_combined_training_tc1()")
    # pr.run("digits.test_combined_training_tc2()")
    # pr.run("digits.test_combined_training_tc3()")
    # pr.disable()
    # pr.dump_stats("test_digits_training.prof")

    #digits.test_combined_training_tc1()
    digits.test_combined_training_tc2()
    #digits.test_combined_training_tc3()


