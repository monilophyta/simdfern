# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



import unittest
#import sys
#sys.path.append('../')

from ..image import Rectangle
from ..data_sampling import ImageSequenceSubsetSampler
import numpy as np



class TestImageSequenceSubsetSampling( unittest.TestCase ):

    img_size = (640,480)
    img_dtype = np.int16

    randseed = 49
    randstate = None

    sample_num = 21 * 12

    num_classes = 21
    label_images = 8*[None]

    class_frequencies = None


    def __init__(self, *args, **kwargs):
        super(TestImageSequenceSubsetSampling, self).__init__(*args, **kwargs)
        self.randstate = np.random.RandomState( self.randseed )
        self.sample_label_images()
        self.calculate_frequencies()


    def test_uniform_pixelwise_data_sampling(self):
        position_sampler = ImageSequenceSubsetSampler( self.class_frequencies.astype( np.uint16 ) )

        pixel_sampler = position_sampler.create_frame_data_sampler( \
                                Rectangle( 0,0, self.img_size[0], self.img_size[1] ), \
                                sample_num = self.sample_num, \
                                class_weights = np.ones(self.num_classes, dtype=np.float32) / self.num_classes, \
                                weight_frequency_fading = 1, \
                                rand_state = self.randstate )
        
        # running test for pixel_sampler
        class_histogram = np.zeros( self.num_classes, dtype=np.int )
        expected_class_sizes = int( round( self.sample_num / float(self.num_classes) ) )

        for limg in self.label_images:
            positions, labels, selected_weights = pixel_sampler.sample_data( limg )
            self.assertEqual( (labels == limg[ positions.row_idx, positions.col_idx ]).sum(), labels.size )

            class_histogram += np.bincount( labels, weights=selected_weights, minlength=self.num_classes ).astype( class_histogram.dtype )
        
        self.assertEqual( self.num_classes, ( class_histogram == expected_class_sizes ).sum() )
    

    def test_natural_pixelwise_data_sampling(self):
        position_sampler = ImageSequenceSubsetSampler( self.class_frequencies.astype( np.uint16 ) )

        sub_window_frame = Rectangle( 40,43, self.img_size[0]- 5, self.img_size[1]-7 )

        pixel_sampler = position_sampler.create_frame_data_sampler( \
                                sub_window_frame, \
                                sample_num = self.sample_num, \
                                class_weights = None, \
                                weight_frequency_fading = 0, \
                                rand_state = self.randstate )
        
        # running test for pixel_sampler
        class_histogram = np.zeros( self.num_classes, dtype=np.int )

        for limg in self.label_images:
            positions, labels, selected_weights = pixel_sampler.sample_data( limg )
            self.assertEqual( (labels == limg[ positions.row_idx, positions.col_idx ]).sum(), labels.size )
            self.assertTrue( np.all( sub_window_frame.is_inside( positions ) ) )

            class_histogram += np.bincount( labels, weights=selected_weights, minlength=self.num_classes ).astype( class_histogram.dtype )

        self.assertTrue( np.all( class_histogram > 0 ) )


    def sample_label_images( self ):
        for i in range( len( self.label_images ) ):
            self.label_images[i] = self.randstate.randint( i, self.num_classes, \
                                                           size=self.img_size, \
                                                           dtype=self.img_dtype )
        
    def calculate_frequencies(self):
        self.class_frequencies = np.zeros( self.img_size + (self.num_classes,), dtype=self.img_dtype )
        
        for limg in self.label_images:
            for label in range( self.num_classes ):
                rpos, cpos = np.nonzero( limg == label )
                self.class_frequencies[rpos,cpos,label] += 1
                


if __name__ == '__main__':
    unittest.main()

