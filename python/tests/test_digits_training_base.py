# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--




__all__ = [ "DigitsTrainingBase" ]


import numpy as np
import functools

from sklearn.datasets import load_digits


# ------------------------------------------------------------------------------------------

class DigitsTrainingBase( object ):

    raw_data    = None    # [ 64 x N ]
    img_data    = None    # [ N x 8 x 8 ]  - int32
    labels      = None    # [ N ]

    feature_names  = None
    label_names = None

    data_weights = None
    
    # ------------------------------------------------------------------------------------------

    @property
    def num_data(self) -> int:
        assert self.data_magnitude == self.labels.size
        return self.labels.size


    @property
    @functools.lru_cache(maxsize=None)
    def data_magnitude(self) -> int:
        assert self.labels.size == self.data_weights.size
        return int(self.data_weights.sum(dtype=int))

    @property
    @functools.lru_cache(maxsize=None)
    def num_labels(self):
        return np.unique(self.labels).size
    
    @property
    def num_dims(self):
        return self.raw_data.shape[0]
    
    # ------------------------------------------------------------------------------------------


    def __init__(self):
        self.load_digits_features()


    def load_digits_features(self):
        digits = load_digits()

        # raw digits data
        raw_data = digits.data.transpose().astype(np.float32)   # [ 64 x N ]
        F, N = raw_data.shape
        
        # nan to detect out of bounds errors
        nan_extra = np.empty( (F, 123), dtype=np.float32 )
        nan_extra.fill( np.nan )

        raw_data = np.require( np.concatenate( (raw_data, nan_extra), axis=1 ), np.float32, ["C_CONTIGUOUS"] )
        self.raw_data = raw_data[:,:N]
        
        self.img_data = digits.images.astype(np.int32)               # [ N x 8 x 8 ]

        self.labels = digits.target.astype(np.int32)                 # [ N ]

        self.feature_names = [ "pixel_%02d" % i for i in range(self.raw_data.shape[0]) ]
        self.label_names = digits.target_names

        self.data_weights = np.ones_like( self.labels )
