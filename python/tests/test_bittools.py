# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


import unittest

import numpy as np
from ..fern import swap_layers, select_layers, remove_layer
from ..fern import select_bits, swap_bits, remove_bit


class TestLayerAlterations( unittest.TestCase ):

    num_layers = 17


    def test_basic_formulas(self):
        
        num_layers = np.arange( self.num_layers, dtype=np.int32 )
        num_nodes = self.get_num_nodes( num_layers )

        self.assertTrue( np.all( num_layers == self.get_num_layers( num_nodes ) ) )


    def test_swap_layers( self ):

        node_idx = self.get_node_idx( self.num_layers )

        for l1 in range( self.num_layers ):
            for l2 in range( l1 ):
                
                # test for meaningful results
                n1_node_idx = swap_layers( node_idx, l1, l2, num_layers=self.num_layers )
                self.assertTrue( np.all( n1_node_idx < self.get_num_nodes( self.num_layers ) ) )
                self.assertEqual( np.unique( n1_node_idx ).size, node_idx.size )

                # swapping indices can be altered
                n2_node_idx = swap_layers( node_idx, l2, l1, num_layers=self.num_layers )
                self.assertTrue( np.all( n1_node_idx == n2_node_idx ) )
                
                # test for corect swapping
                r_node_idx = swap_layers( n1_node_idx, l1, l2, num_layers=self.num_layers )
                self.assertTrue( np.all( node_idx == r_node_idx ) )


    def test_remove_layer( self ):
        
        for num_layers in range( 2, self.num_layers ):
            num_bits = np.arange( num_layers, dtype=np.int32 )  # [ L -1 ]
            node_idx = (2**num_bits).astype(np.int32)                   # [ L - 1 ]

            # removing lowest bit shoud be like a shift
            self.assertTrue( np.all( np.right_shift( node_idx, 1 ) == 
                                     remove_layer( node_idx, num_layers-1, num_layers=num_layers ) ) )

            # removing lowest bit shoud be like a shift
            self.assertTrue( np.all( np.right_shift( node_idx-1, 1 ) == 
                                     remove_layer( node_idx-1, num_layers-2, num_layers=num_layers-1 ) ) )

            # mask for lower n-1 layers
            lm1_mask = np.int32( (1 << (num_layers-1)) - 1 )

            # removing highest bit (first layer) check with bitmask
            self.assertTrue( np.all( np.bitwise_and( node_idx, lm1_mask ) == remove_layer( node_idx, 0, num_layers=num_layers ) ) )

            # mask for lower n-2 layers
            lm2_mask = np.int32( (1 << (num_layers-2)) - 1 )

            # removing highest bit (first layer) check with bitmask
            self.assertTrue( np.all( np.bitwise_and( node_idx-1, lm2_mask ) == remove_layer( node_idx-1, 0, num_layers=num_layers-1 ) ) )


    def test_select_layers_transfer( self ):
        
        node_idx = self.get_node_idx( self.num_layers )               # [ L - 1 ]
        
        # array for removing layers
        c_node_idx = node_idx.copy()

        # array for accumulating layers
        n_node_idx = np.zeros_like( node_idx )

        # alter layer indices
        for num_layers in range( self.num_layers, 0, -1 ):
            s_idx = select_layers( c_node_idx, np.array( [0], dtype=np.int32 ), num_layers = num_layers )
            c_node_idx = remove_layer( c_node_idx, 0, num_layers = num_layers )

            # shift up indices and bring cut out layer in at the lower bit 
            n_node_idx = np.left_shift( n_node_idx, 1 )
            n_node_idx = np.bitwise_or( n_node_idx, s_idx )
        
        self.assertTrue( np.all( c_node_idx == 0 ) )
        self.assertTrue( np.all( node_idx == n_node_idx) )  # all layers should have been transfered


    def test_select_layer_identical( self ):
        
        node_idx = self.get_node_idx( self.num_layers )               # [ L - 1 ]
        assert np.issubdtype( node_idx.dtype, np.int32 )
        
        sel_idx = np.arange( self.num_layers - 1, -1, -1, dtype=np.int32 )
        assert np.issubdtype( sel_idx.dtype, np.int32 )

        # bring layer indices in opposite order
        inverse_node_idx = select_layers( node_idx, sel_idx, num_layers = self.num_layers )
        self.assertTrue( np.issubdtype( inverse_node_idx.dtype, np.int32 ) )
        self.assertTrue( np.any( node_idx != inverse_node_idx ) )

        # reverse order again for checkin
        c_node_idx = select_layers( inverse_node_idx, sel_idx, num_layers = self.num_layers )
        self.assertTrue( np.issubdtype( c_node_idx.dtype, np.int32 ) )
        self.assertTrue( np.all( node_idx == c_node_idx ) )
    
    # -----------------------------------------------------------------------------------------------


    def test_swap_bits( self ):

        node_idx = self.get_node_idx( self.num_layers )
        assert np.issubdtype( node_idx.dtype, np.int32 )

        for l1 in range( self.num_layers ):
            for l2 in range( l1 ):
                
                # test for meaningful results
                n1_node_idx = swap_bits( node_idx, l1, l2 )
                self.assertTrue( np.issubdtype( n1_node_idx.dtype, np.int32 ) )
                self.assertTrue( np.all( n1_node_idx < self.get_num_nodes( self.num_layers ) ) )
                self.assertEqual( np.unique( n1_node_idx ).size, node_idx.size )

                # swapping indices can be altered
                n2_node_idx = swap_bits( node_idx, l2, l1 )
                self.assertTrue( np.issubdtype( n2_node_idx.dtype, np.int32 ) )
                self.assertTrue( np.all( n1_node_idx == n2_node_idx ) )
                
                # test for corect swapping
                r_node_idx = swap_bits( n1_node_idx, l1, l2 )
                self.assertTrue( np.issubdtype( r_node_idx.dtype, np.int32 ) )
                self.assertTrue( np.all( node_idx == r_node_idx ) )
                


    def test_remove_bit( self ):
        
        num_bits = np.arange( self.num_layers, dtype=np.int32 )  # [ L -1 ]
        node_idx = (2**num_bits).astype(np.int32)                   # [ L - 1 ]

        # removing lowest bit shoud be like a shift
        self.assertTrue( np.all( np.right_shift( node_idx, 1 ) == remove_bit( node_idx, 0 ) ) )

        # removing lowest bit shoud be like a shift
        self.assertTrue( np.all( np.right_shift( node_idx-1, 1 ) == remove_bit( node_idx-1, 0 ) ) )

        # removing lowest bit shoud be like a shift
        self.assertTrue( np.all( node_idx == remove_bit( node_idx, self.num_layers ) ) )

        # removing lowest bit shoud be like a shift
        self.assertTrue( np.all( node_idx-1 == remove_bit( node_idx-1, self.num_layers ) ) )


    def test_select_bits( self ):
        
        num_bits = np.arange( self.num_layers, dtype=np.int32 )  # [ L -1 ]
        node_idx = (2**num_bits).astype(np.int32)                   # [ L - 1 ]

        for bidx in range(self.num_layers):
            mask = 1 << bidx

            check1 = np.bitwise_and( mask, node_idx ) >> bidx
            self.assertTrue( np.all( check1 == select_bits( node_idx, [bidx] ) ) )

            check1 = np.bitwise_and( mask, node_idx - 1 ) >> bidx
            self.assertTrue( np.all( check1 == select_bits( node_idx - 1, [bidx] ) ) )



    @classmethod
    def get_node_idx( cls, num_layers ):
        return np.arange( cls.get_num_nodes( num_layers ), dtype=np.int32 )


    @staticmethod
    def get_num_nodes( num_layers ):
        return np.require( 2**num_layers, dtype=np.int32 )

    @staticmethod
    def get_num_layers( num_nodes ):
        return np.require( np.ceil( np.log2( num_nodes ) ), dtype=np.int32 )
