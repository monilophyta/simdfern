# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = [ "DebugTreeTrainingObserver", "DebugFernTrainingObserver", "DebugCombinedTrainingObserver" ]



#import numpy as np

from ..training import CountingTreeTrainingObserver
from ..training import CountingFernTrainingObserver
from ..training import CountingCombinedTrainingObserver



class DebugTreeTrainingObserver( CountingTreeTrainingObserver ):

    test_inst = None

    layer_improvement = 0  # True if layer in processing has improvement


    def __init__(self, test_inst ):
        super().__init__( record_gini_index = True, 
                  record_mutual_information = True )
        self.test_inst = test_inst


    def notify_start_layer_training( self, trainer, train_context ):
        super().notify_start_layer_training( trainer, train_context )
        self.layer_improvement = 0   # Initial Value


    def notify_processed_feature( self, trainer, train_context, optimizer, num_improvements, feature_idx ):
        super().notify_processed_feature( trainer, train_context, optimizer, num_improvements, feature_idx )

        if num_improvements > 0:
            self.layer_improvement += num_improvements


    def notify_layer_classification( self, trainer, train_context, feature_priotizer ):
        super().notify_layer_classification( trainer, train_context, feature_priotizer )

        if (self.layer_improvement > 0) and (len(self.measure_source) > 1):
            improved_measure =    (self.mutual_inf_measure[-1] > self.mutual_inf_measure[-2]) \
                               or (self.gini_measure[-1] >  self.gini_measure[-2])
            
            self.test_inst.assertTrue( improved_measure )


# -----------------------------------------------------------------------------------------------------------

class DebugFernTrainingObserver( CountingFernTrainingObserver ):

    test_inst = None

    def __init__(self, test_inst ):
        super().__init__( record_gini_index = True, 
                  record_mutual_information = True )
        self.test_inst = test_inst

# -----------------------------------------------------------------------------------------------------------

class DebugCombinedTrainingObserver( CountingCombinedTrainingObserver ):

    test_inst = None

    def __init__(self, test_inst ):
        super().__init__( record_gini_index = True, 
                  record_mutual_information = True )
        self.test_inst = test_inst


