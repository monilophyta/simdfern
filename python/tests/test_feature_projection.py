# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

import unittest

import numpy as np

from ..ctypes_py import create_feature_rows, FeatureRows
from ..ctypes_py import project_features, ESummationAlgo

from .test_common import prime_numbers


# ------------------------------------------------------------------------------------------------

class FeatureProjectionTest( unittest.TestCase ):

    num_data     : int = 45
    num_features : int = 7
    feature_rows : np.ndarray   # [ num_features x num_data ]

    #seed : int = 69

    # --------------------------------------------------------------

    def __init__(self, *args, **argv ):
        super().__init__( *args, **argv )
        feature_rows = prime_numbers[:(self.num_data*self.num_features)]
        self.feature_rows = feature_rows.reshape( (self.num_features, self.num_data) )


    def test_projection(self):
        prime_idx = self.feature_rows.size + np.arange( self.num_features, dtype=np.int )
        feature_weights_i32 = prime_numbers.take(prime_idx)

        feature_rows_f32 = self.feature_rows.astype(np.float32)    # [ F x N ]
        feature_weights_f32 = feature_weights_i32.astype(np.float32)   # [ F ]

        for num_features in range(1, self.num_features+1):
            for ridx in range(self.num_features - num_features + 1):
                rselect = slice(ridx,ridx+num_features)

                for num_data in range(1,self.num_data+1):
                    for nidx in range(self.num_data - num_data + 1):
                        dselect = slice(nidx,nidx+num_data)
                        feature_rows = create_feature_rows(0)
                        feature_rows.extend( list(feature_rows_f32[rselect,dselect]) )

                        for fidx in range(self.num_features - num_features + 1):
                            fselect = slice(fidx,fidx+num_features)
                            feature_weights = feature_weights_f32[fselect]

                            #print( feature_rows_f32[rselect,dselect].shape, feature_weights.shape )

                            # check call
                            c_proj_data = np.dot( feature_weights_i32[fselect], self.feature_rows[rselect,dselect] )

                            # ensure precise float range
                            self.assertLess( c_proj_data.max(), 2**24 )

                            for sum_algo in ESummationAlgo:
                                # test call
                                proj_data = project_features( feature_weights, feature_rows, sum_algo )

                                diff = np.absolute( proj_data - c_proj_data )
                                self.assertLess( diff.max(), 1e-11 )

