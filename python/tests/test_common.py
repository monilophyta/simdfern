# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["prime_numbers", "primesfrom2to"]


# ----------------------------------------------------------------------------------

import numpy as np

# ----------------------------------------------------------------------------------

def primesfrom2to(n):
    """ Input n>=6, Returns a array of primes, 2 <= p < n """
    sieb = np.ones( n//3 + (n%6==2), dtype=np.bool)
    for i in range(1,int(n**0.5)//3+1):
        if sieb[i]:
            k=3*i+1|1
            sieb[       k*k//3     ::2*k] = False
            sieb[k*(k-2*(i&1)+4)//3::2*k] = False
    return np.r_[2,3,((3*np.nonzero(sieb)[0][1:]+1)|1)]


# ----------------------------------------------------------------------------------

prime_numbers = primesfrom2to( 2**24 )   # [ 5.761.455 ]

