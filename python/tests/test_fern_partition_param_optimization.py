# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

import unittest

import math
import numpy as np

from ..ctypes_py import get_cpp_event_counts
from ..ctypes_py import create_feature_rows, FeatureRows, WeightedFernLayer
from ..ctypes_py import lsearch_fern_param, line_search_result_t
from ..fern import FernPartitionOptimizer


#from ..tests import IrisTrainingBase
from ..tests import DigitsTrainingBase
from ..clmath import MultiNodeClassStatistics, mutual_counts, normalize_gini
from ..clmath import max_unormed_gini

#-----------------------------------------------------------------------------------

relative_diff = lambda x,y: (x-y) / max(1e-16,max(abs(x),abs(y)))
calc_normalized_penality = lambda s,fern_layer: s * fern_layer.normalized_l1norm
calc_centralized_penality = lambda s,fern_layer: s * fern_layer.centralized_l1norm

# constant penality offset not considered in implementation
calc_centralized_penality_offset = lambda s, fern_layer: -s / math.sqrt(fern_layer.num_features)

# -----------------------------------------------------------------------------------


class FernPartitionParamOptimizationTest( DigitsTrainingBase, unittest.TestCase ):

    seed = 45
    randstate : None

    #num_layers = 1
    num_selected_eigenvectors = 9

    feature_rows : FeatureRows

    pre_node_idx : np.ndarray

    # ------------------------------------------------------------------------------------------------------------


    def __init__(self, *args, **argv ):
        DigitsTrainingBase.__init__(self)
        unittest.TestCase.__init__( self, *args, **argv )

        self.randstate = np.random.RandomState(self.seed)

        # initialize subset of data
        self.init_data(self.num_selected_eigenvectors)

        # classifiy with one already predefined hard-coded layer
        pre_layer = WeightedFernLayer.create( feature_weights=np.array( [0.00110673, -0.47330338, -0.00154508, 0.04084888, 0.00260732, -0.86029226, 0.00113441, 0.01967769, 0.18388484], dtype=np.float32 ), 
                                              feature_split = 0.11307568103075027 )
        self.pre_node_idx = pre_layer.classify( np.zeros( self.num_data, dtype=np.int32 ), self.feature_rows )
        #self.pre_node_idx = np.zeros( self.num_data, dtype=np.int32 )


    def init_data(self, num_features : int ):
        raw_data = self.raw_data - self.raw_data.mean(axis=1)[:,np.newaxis]

        feature_rows = create_feature_rows(0)
        feature_rows.extend( list(raw_data) )

        # Perfrom LDA and use only two dimensions
        mns = MultiNodeClassStatistics( nodeIdx = np.zeros( self.num_data, dtype=np.int32 ),
                                       classIdx = self.labels,
                                   feature_rows = feature_rows,
                                        weights = self.data_weights )
        
        eigenvalue, eigenvector = mns.run_lda()
        assert np.all( eigenvalue[:-1] > eigenvalue[1:] )
        self.raw_data = np.dot( eigenvector[:,:num_features].transpose(), raw_data ).astype(np.float32)

        # initialize with one existing layer
        self.feature_rows = create_feature_rows(0)
        self.feature_rows.extend( list(self.raw_data) )

    # ------------------------------------------------------------------------------------------------------------

    def calc_gini_rating(self, node_idx, do_norm=True):
        mcounts = mutual_counts( X = node_idx, 
                                 Y = self.labels,
                           weights = self.data_weights,
                             num_X = node_idx.max()+1,
                             num_Y = self.labels.max() + 1)

        return mcounts.gini_index( axis = 1, do_norm = do_norm) 

    # ------------------------------------------------------------------------------------------------------------
    
    def get_fern_partition_optimizer(self, regul_ratio : float, feature_rows : FeatureRows ):

        regul_strength = regul_ratio * max_unormed_gini( self.data_magnitude, self.num_labels )

        # initialize fern partition optimizer
        part_optimizer = FernPartitionOptimizer( feature_rows = feature_rows,
                                                    label_idx = self.labels,
                                                     node_idx = self.pre_node_idx,
                                               sample_weights = self.data_weights,
                                                   num_labels = self.num_labels,
                                                    num_leafs = 2,
                                               regul_strength = regul_strength )
        return part_optimizer
    
    
    def init_random_fern_partition(self, num_feature : int, feature_rows : FeatureRows ):

        weights = self.randstate.randn( num_feature ).astype(np.float32)
        feature_split = float(self.randstate.randn(1)[0])

        init_fern_layer = WeightedFernLayer.create( feature_weights = weights, feature_split = feature_split )
        leaf_idx = init_fern_layer.classify( self.pre_node_idx, feature_rows )

        return (init_fern_layer, leaf_idx)


    # ------------------------------------------------------------------------------------------------------------


    def test_fern_partition_coordinate_descent(self):

        # initialize fern partition optimizer
        part_optimizer = self.get_fern_partition_optimizer( regul_ratio = 0.05, feature_rows = self.feature_rows )


        last_total_rating = 0.5 * np.finfo(np.float32).max

        # initialize random layer
        init_fern_layer, leaf_idx = self.init_random_fern_partition( self.num_selected_eigenvectors, self.feature_rows )

        # rating before training
        unormed_pretrain_rating = self.calc_gini_rating( leaf_idx, do_norm=False )
        pretain_penalty = init_fern_layer.centralized_l1norm
        total_pretrain_rating = unormed_pretrain_rating + (part_optimizer.regul_strength * pretain_penalty)

        # initialize n_layer
        n_layer = init_fern_layer

        for i in range(48):

            widx = i%self.num_selected_eigenvectors

            ## optimize weights
            res = part_optimizer.optimize_weight( cidx=widx, fern_layer=n_layer )
            n_layer = res.fern_layer
            n_leaf_idx = n_layer.classify( self.pre_node_idx, self.feature_rows )
            #print(res)

            # rating after training
            unormed_posttrain_rating = res.rating
            posttrain_penalty = res.weight_penalty + calc_centralized_penality_offset(res.regul_strength, n_layer)
            total_posttrain_rating = unormed_posttrain_rating + posttrain_penalty

            # verify penalty
            self.assertAlmostEqual( relative_diff( posttrain_penalty, res.regul_strength * n_layer.centralized_l1norm), 0., places=5 )

            # verify gini rating
            py_gini_rating = self.calc_gini_rating( n_leaf_idx, do_norm=False )
            self.assertEqual( -py_gini_rating, unormed_posttrain_rating )

            # verify total improvement
            #Difficult to garantie since regularisation is only on centers between data samples
            self.assertLessEqual( total_posttrain_rating, (1. - 1e-3) * last_total_rating )

            ## optimize splits
            res = part_optimizer.optimize_split( fern_layer=n_layer )

            # get optimization results
            n_layer = res.fern_layer
            n_leaf_idx = n_layer.classify( self.pre_node_idx, self.feature_rows )

            # verify gini rating
            py_gini_rating = self.calc_gini_rating( n_leaf_idx, do_norm=False )
            self.assertEqual( res.rating, -py_gini_rating )

            # verify improvement
            self.assertLessEqual( res.rating, unormed_posttrain_rating )
            
            last_total_rating = total_posttrain_rating


    def test_fern_partition_line_search(self):
        regul_ratio = 0.01
        regul_strength = regul_ratio * max_unormed_gini( self.data_magnitude, self.num_labels )

        unormed_pretrain_rating = self.calc_gini_rating( self.pre_node_idx, do_norm=False )
        
        for numf in range(2,self.num_selected_eigenvectors+1):

            # initialize with one existing layer
            feature_rows = create_feature_rows(0)
            feature_rows.extend( list(self.raw_data)[:numf] )

            # initialize fern partition optimizer
            part_optimizer = FernPartitionOptimizer( feature_rows = feature_rows,
                                                       label_idx = self.labels,
                                                        node_idx = self.pre_node_idx,
                                                  sample_weights = self.data_weights,
                                                      num_labels = self.num_labels,
                                                       num_leafs = 2,
                                                  regul_strength = regul_strength )


            # initialize a layer with random weights
            feature_weights = self.randstate.randn(numf).astype(np.float32)
            feature_split = float(self.randstate.randn(1)[0])
            fern_layer = WeightedFernLayer.create( feature_weights = feature_weights[:numf], feature_split = feature_split )

            
            last_total_rating = 0.5 * np.finfo(np.float32).max

            # Doing a number of gradient descent steps
            for n_optim_steps in range(5):

                res = part_optimizer.optimize_direction( fern_layer = fern_layer,
                                                 use_regul_gradient = True,
                                                     optimize_split = False )

                #print(numf, n_optim_steps, res["rating"])
                #print(res["regul_strength"])
                n_fern_layer = res.fern_layer

                # the centralized weight is already included in regul_strength
                #n_normalized_penalty = calc_normalized_penality( res["regul_strength"], n_fern_layer )
                n_centralized_penalty = calc_centralized_penality( res.regul_strength, n_fern_layer )
                n_centralized_offset = calc_centralized_penality_offset( res.regul_strength, n_fern_layer )

                # verify correctness of regularization
                #print( res["dir_scale_penalty"], n_normalized_penalty, n_centralized_penalty - n_centralized_offset )
                self.assertAlmostEqual( relative_diff( res.dir_scale_penalty, n_centralized_penalty - n_centralized_offset), 0., places=6 )

                # # get new fern layer
                n_node_idx = n_fern_layer.classify( self.pre_node_idx, feature_rows )

                # # Verfiy correctness of gini rating
                py_gini_rating = self.calc_gini_rating( n_node_idx )
                normed_cpp_gini_rating = normalize_gini( -res.rating, self.data_magnitude, self.num_labels )
                self.assertAlmostEqual( py_gini_rating, normed_cpp_gini_rating )

                # Total Rating before optimization
                total_rating_pre = calc_centralized_penality( res.regul_strength, fern_layer ) - \
                                   calc_centralized_penality_offset( res.regul_strength, fern_layer ) - \
                                   unormed_pretrain_rating
                # Total Rating after optimization
                total_rating_post = float(res.dir_scale_penalty) + float(res.rating)
                #print( "numf=%d  total_rating_pre = %.4f total_rating_post = %.4f" % ( numf, total_rating_pre, total_rating_post ) )
                self.assertLessEqual( total_rating_post, total_rating_pre )
                self.assertLessEqual( total_rating_post, (1. - 1e-4) * last_total_rating )
                
                #print( numf, res["lsearch_dir"] )

                # fern layer for next run
                fern_layer = n_fern_layer
                last_total_rating = total_rating_post
                del n_node_idx


    def test_corner_cases_of_calc_lsearch_candidates(self):
        regul_ratio = 0.01
        regul_strength = regul_ratio * max_unormed_gini( self.data_magnitude, self.num_labels )
        
        # initializ features
        numf = 5
        feature_rows = create_feature_rows(0)
        feature_rows.extend( list(self.raw_data)[:numf] )

        # initialize fern partition optimizer
        part_optimizer = FernPartitionOptimizer( feature_rows = feature_rows,
                                                    label_idx = self.labels,
                                                     node_idx = self.pre_node_idx,
                                               sample_weights = self.data_weights,
                                                   num_labels = self.num_labels,
                                                    num_leafs = 2,
                                               regul_strength = regul_strength )

        # initialize a layer with random weights
        feature_weights = self.randstate.randn(numf).astype(np.float32)
        feature_split = float(self.randstate.randn(1)[0])
        fern_layer = WeightedFernLayer.create( feature_weights = feature_weights[:numf], feature_split = feature_split )
        
        # run a single optimization to get a valid gradient
        init_res = part_optimizer.optimize_direction( fern_layer = fern_layer,
                                              use_regul_gradient = True,
                                                  optimize_split = True )
        
        # -------------------------------------------------------------------------------------------------------
        # next steps are taken from FernPartitionOptimizer.optimize_direction

        # Project all samples onto the plane normal
        plane_normal_projection = part_optimizer.calc_plane_normal_projection( fern_layer.feature_weights, fern_layer.feature_split )   # [ N ]
        
        # Project all samples onto the line search direction
        lsearch_projection = part_optimizer.calc_lsearch_projection( init_res.weight_lsearch_dir, init_res.split_lsearch_dir )  # [ N ]

        # param penalizer for weight regularization
        penalizer = part_optimizer.create_vector_penalizer( offset_weights = fern_layer.feature_weights, 
                                                            scaled_weights = init_res.weight_lsearch_dir )

        # -------------------------------------------------------------------------------------------------------
        # validate some of the data
        
        N = plane_normal_projection.size

        # sample random indices
        randidx = self.randstate.permutation(N)
        inf_idx  = np.sort( randidx[:100] )
        minf_idx = np.sort( randidx[100:200] )
        nan_idx  = np.sort( randidx[200:300] )
        mnan_idx = np.sort( randidx[300:400] )
        
        # create mask of valid samples
        valid_mask = np.ones(N, dtype=np.bool)
        valid_mask[inf_idx] = False
        valid_mask[minf_idx] = False
        valid_mask[nan_idx] = False
        valid_mask[mnan_idx] = False

        # infinite projection
        plane_normal_projection[inf_idx] = np.maximum( np.absolute( plane_normal_projection[inf_idx] ), 1e-8 )
        lsearch_projection[inf_idx] = 0

        # -infinite projection
        plane_normal_projection[minf_idx] = np.minimum( -1 * np.absolute( plane_normal_projection[minf_idx] ), -1e-8 )
        lsearch_projection[minf_idx] = 0

        # nan projection
        plane_normal_projection[nan_idx] = 0
        lsearch_projection[nan_idx] = 0

        # nan projection
        plane_normal_projection[mnan_idx] = 0
        plane_normal_projection[mnan_idx] *= -1
        lsearch_projection[mnan_idx] = 0

        # calculate candate for search direction scaling
        init_leaf_idx, scale_candidates, num_invalid = part_optimizer.calc_lsearch_candidates( 
                                                           factor = lsearch_projection,
                                                           offset = plane_normal_projection )  # [ N ]
        
        self.assertEqual( num_invalid, inf_idx.size + minf_idx.size + nan_idx.size + mnan_idx.size )
        self.assertEqual( np.isfinite( scale_candidates ).sum(), scale_candidates.size - num_invalid )

        # -------------------------------------------------------------------------------------------------------
        # run the actual optimization with changed scale candidates
        res = lsearch_fern_param( param_candidates = scale_candidates,
                                          node_idx = init_leaf_idx,
                                         label_idx = part_optimizer.label_idx,
                                      data_weights = part_optimizer.sample_weights,
                                         num_nodes = part_optimizer.num_nodes << 1,
                                        num_labels = part_optimizer.num_labels,
                                         objective = part_optimizer.objective,
                                         penalizer = penalizer )
        assert isinstance( res, line_search_result_t )

        # create new layer as result of optimization
        n_layer = part_optimizer.create_fern_partition( feature_weights = (res.prmVal * init_res.weight_lsearch_dir) + fern_layer.feature_weights,
                                                          feature_split = (res.prmVal * init_res.split_lsearch_dir)  + fern_layer.feature_split )
        
        n_layer_classification = n_layer.classify( np.zeros_like( init_leaf_idx ), feature_rows )
        
        # -------------------------------------------------------------------------------------------------------
        # calculate effective projection after optimization

        data_projection = (lsearch_projection * float(res.prmVal)) - plane_normal_projection
        projection_classification = (data_projection < 0).astype(np.int32)

        # --------------------------------------------------------------------------------------------------------
        # tests

        # check that classifications of valid samples are equal
        valid_equal = n_layer_classification[valid_mask] == projection_classification[valid_mask]
        self.assertEqual( valid_equal.sum(), valid_equal.size )

        # check correct side for infinite samples
        inf_is_left = projection_classification[inf_idx] == 1
        self.assertEqual( inf_is_left.sum(), inf_is_left.size )

        minf_is_left = projection_classification[minf_idx] == 0
        self.assertEqual( minf_is_left.sum(), minf_is_left.size )

        # check correct side for invalid samples
        nan_is_left = projection_classification[nan_idx] == 0
        self.assertEqual( nan_is_left.sum(), nan_is_left.size )

        mnan_is_left = projection_classification[mnan_idx] == 0
        self.assertEqual( mnan_is_left.sum(), mnan_is_left.size )
