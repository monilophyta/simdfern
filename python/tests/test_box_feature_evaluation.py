# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



import unittest
#import sys
#sys.path.append('../')

from ..image import compute_integral_image, ImageCoords
from ..ctypes_py import BoxFeatureEvaluator
from ..image import ImageBoxes
import numpy as np



class TestBoxFeatureEvaluation( unittest.TestCase ):

    img_size = (641,483)
    #img_size = (5,6)
    img_dtype = np.int32

    img    = None  # [ H x W ]
    intimg = None  # [ H+1 x W+1 ]

    img_coords = None # [ 2 x H+1 * W+1 ]


    def setUp(self):
        # create random image
        self.img = np.random.randint( 0, 1023, size=self.img_size, dtype=self.img_dtype )
        self.intimg = compute_integral_image( self.img )

        # One coordinate for every pixel
        self.img_coords = ImageCoords.from_image_shape( self.img_size )
        #self.img_coords += 1 # should run into an assertion 

    def identity_test( self, do_align ):
        up    = 0
        left  = 0
        down  = up+1
        right = left+1
        box = ImageBoxes( up, left, down, right )

        boxlambda = -0.

        # Test call
        box_eval = BoxFeatureEvaluator( box, box, boxlambda = boxlambda, do_align = do_align )
        idimg = box_eval.process( self.intimg, self.img_coords )

        # reshape results
        self.assertEqual( idimg.size, self.img.size )
        idimg = np.round( idimg ).astype( np.int32 ).reshape( self.img_size )
        #print( self.img, "\n" )
        #print( idimg )

        # check equalness
        self.assertTrue( np.all(self.img == idimg) )


    def test_box_feature_evaluation_identity(self):
        self.identity_test( do_align = False )


    def test_box_feature_evaluation_identity_aligned(self):
        self.identity_test( do_align = True )


    def shift_test( self,  do_align ):
        up    = 3
        left  = 2
        down  = up+1
        right = left+1
        box = ImageBoxes( up, left, down, right )

        boxlambda = -0.

        # Test call
        box_eval = BoxFeatureEvaluator( box, box, boxlambda = boxlambda, do_align = do_align )
        idimg = box_eval.process( self.intimg, self.img_coords )

        # reshape results
        self.assertEqual( idimg.size, self.img.size )
        idimg = np.round( idimg ).astype( np.int32 ).reshape( self.img_size )

        # check equalness
        self.assertTrue( np.all(self.img[up:,left:] == idimg[:-up,:-left]) )

        # check border pattern - should be invalid
        self.assertEqual( (idimg < 0).sum(), idimg.size - idimg[:-up,:-left].size )


    def test_box_feature_evaluation_shift(self):
        self.shift_test( do_align = False )


    def test_box_feature_evaluation_shift_aligned(self):
        self.shift_test( do_align = True )



if __name__ == '__main__':
    unittest.main()

