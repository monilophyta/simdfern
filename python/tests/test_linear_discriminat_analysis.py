# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



import unittest
#import sys
#sys.path.append('../')

from ..clmath import MultiNodeClassStatistics
from ..ctypes_py import calculate_multi_node_lda_covars, FeatureRows, create_feature_rows
from ..fern import select_layers, remove_layer
import numpy as np

import matplotlib.pyplot as plt


matrix_diff = lambda x,y: np.absolute( np.sqrt(np.absolute(x)) - np.sqrt(np.absolute(y)) ).max()

kl_divergence = lambda cov1, cov2: 0.5 * ( np.trace( np.linalg.solve( cov2, cov1 ) ) \
                                           - cov1.shape[0] \
                                           + np.linalg.slogdet( cov2 )[1] - np.linalg.slogdet( cov1 )[1] )
js_divergence = lambda cov1, cov2: 0.5 * ( kl_divergence( cov1, cov2 ) + kl_divergence( cov2, cov1 ) )



class TestLDA( unittest.TestCase ):
    dtype = np.float32
    randseed = 49+1
    num_data = 1000000


    def test_very_simple(self):

        data, label_idx, node_idx, weights, R = \
                self.sample_data( num_labels=2,
                                  num_nodes=1,
                                  class_offset = np.zeros(2,np.int32),
                                  node_offset = np.zeros(1,np.int32),
                                  bgscale=1, bgoffset = 0,
                                  max_weight=1,
                                  rot_angle=0 )  # [ N x 2 ]
        
        data_f32 = data.astype(np.float32)
        test_stat = MultiNodeClassStatistics( node_idx, label_idx, ( data_f32[:,0], data_f32[:,1] ), weights, do_verify=True )
        self.assertTrue( test_stat.has_unseparated_data() )

        _, py_intra_cov, py_inter_cov = test_stat.pythonic_covar_estimation( node_idx, label_idx, ( data[:,0], data[:,1] ), weights  )

        intra_div = js_divergence( test_stat.intra_cov, np.cov(data.transpose()) )
        inter_div = np.exp( np.linalg.slogdet( test_stat.inter_cov )[1] )

        # print( "orig_cov:\n", np.cov(data.transpose()) )
        # print( "inter_cov:\n", test_stat.inter_cov )
        # print( "py_inter_cov:\n", py_inter_cov )
        # print( "intra_cov:\n", test_stat.intra_cov )
        # print( "py_intra_cov:\n", py_intra_cov )

        # print( "intra_div = ", intra_div )
        # print( "inter_div = ", inter_div )

        self.assertAlmostEqual( intra_div, 0, 6 )
        self.assertAlmostEqual( inter_div, 0, 6 )

        # compare with python implementation
        self.assertAlmostEqual( js_divergence( test_stat.intra_cov, py_intra_cov ), 0, 6 )
        #self.assertAlmostEqual( js_divergence( test_stat.inter_cov, py_inter_cov ), 0, 6 )  # likely singular



    def test_multiple_nodes_and_classes(self):

        data, label_idx, node_idx, weights, R = \
                self.sample_data( num_labels=2,
                                  num_nodes=2,
                                  class_offset = np.zeros(2,np.int32),
                                  node_offset = np.zeros(2,np.int32),
                                  bgscale=1, bgoffset = 0,
                                  max_weight=3,
                                  rot_angle= np.pi * 33 / 180. )  # [ N x 2 ]
        
        data_f32 = data.astype(np.float32)
        test_stat = MultiNodeClassStatistics( node_idx, label_idx, ( data_f32[:,0], data_f32[:,1] ), weights, do_verify=True )

        intra_div = js_divergence( test_stat.intra_cov, np.cov(data.transpose()) )
        inter_div = np.exp( np.linalg.slogdet( test_stat.inter_cov )[1] )
        
        _, py_intra_cov, py_inter_cov = test_stat.pythonic_covar_estimation( node_idx, label_idx, ( data[:,0], data[:,1] ), weights  )

        # print( "orig_cov:\n", np.cov(data.transpose()) )
        # print( "inter_cov:\n", test_stat.inter_cov )
        # print( "py_inter_cov:\n", py_inter_cov )
        # print( "intra_cov:\n", test_stat.intra_cov )
        # print( "py_intra_cov:\n", py_intra_cov )

        # print( "intra_div = ", intra_div )
        # print( "inter_div = ", inter_div )

        self.assertAlmostEqual( intra_div, 0, 6 )
        self.assertAlmostEqual( inter_div, 0, 6 )

        # compare with python implementation
        self.assertAlmostEqual( js_divergence( test_stat.intra_cov, py_intra_cov ), 0, 6 )
        self.assertAlmostEqual( js_divergence( test_stat.inter_cov, py_inter_cov ), 0, 2 )


    def test_with_offsets(self):

        data, label_idx, node_idx, weights, R = \
                self.sample_data( num_labels=2,
                                  num_nodes=2,
                                  class_offset = np.array([-4,4],np.int32),
                                  node_offset = np.array([8,-8],np.int32),
                                  bgscale=1, bgoffset = 0,
                                  max_weight=1,
                                  rot_angle=0 )  # [ N x 2 ]
        
        data_f32 = data.astype(np.float32)
        test_stat = MultiNodeClassStatistics( node_idx, label_idx, ( data_f32[:,0], data_f32[:,1] ), weights, do_verify=True )
        _, py_intra_cov, py_inter_cov = test_stat.pythonic_covar_estimation( node_idx, label_idx, ( data[:,0], data[:,1] ), weights )

        # print( "orig_cov:", np.cov(data.transpose()) )
        # print( "inter_cov:\n", test_stat.inter_cov )
        # print( py_inter_cov  )
        # print( "intra_cov:\n", test_stat.intra_cov )
        # print( py_intra_cov )
        #self.plot_data( data, label_idx, node_idx )

        # inter forground (0) dimension should have higher variance than background (1)
        self.assertGreaterEqual( test_stat.inter_cov[0,0], 1 )
        self.assertGreater( test_stat.inter_cov[0,0], test_stat.inter_cov[1,1] )

        # inter noise should still be diagonal
        #inter_div = js_divergence( test_stat.inter_cov, np.diag( np.diag( test_stat.inter_cov ) ) )
        #self.assertAlmostEqual( inter_div, 0, 1 )
        
        # all clusters should be uniform gaussians
        intra_div = js_divergence( test_stat.intra_cov, np.eye(2) )
        self.assertAlmostEqual( intra_div, 0, 5 )

        self.assertAlmostEqual( js_divergence( test_stat.intra_cov, py_intra_cov ), 0, 6 )
        self.assertAlmostEqual( js_divergence( test_stat.inter_cov, py_inter_cov ), 0, 6 )



    def test_with_rotation(self):

        data, label_idx, node_idx, weights, R = \
                self.sample_data( num_labels=2,
                                  num_nodes=2,
                                  class_offset = np.array([-4,4],np.int32),
                                  node_offset = np.array([8,-8],np.int32),
                                  bgscale=1, bgoffset = 0,
                                  max_weight=3,
                                  rot_angle= np.pi * 45 / 180. )  # [ N x 2 ]
        
        data_f32 = data.astype(np.float32)
        test_stat = MultiNodeClassStatistics( node_idx, label_idx, ( data_f32[:,0], data_f32[:,1] ), weights, do_verify=True )
        _, py_intra_cov, py_inter_cov = test_stat.pythonic_covar_estimation( node_idx, label_idx, ( data[:,0], data[:,1] ), weights )

        intra_div = js_divergence( test_stat.intra_cov, py_intra_cov )
        inter_div = js_divergence( test_stat.inter_cov, py_inter_cov )

        # print( "orig_cov:\n", np.cov(data.transpose()) )
        # print( "inter_cov:\n", test_stat.inter_cov )
        # print( "py_inter_cov:\n", py_inter_cov )
        # print( "intra_cov:\n", test_stat.intra_cov )
        # print( "py_intra_cov:\n", py_intra_cov )

        # print( "intra_div = ", intra_div )
        # print( "inter_div = ", inter_div )

        # self.plot_data( data, label_idx, node_idx )

        # all clusters should be uniform gaussians
        intra_div = js_divergence( test_stat.intra_cov, np.eye(2) )
        self.assertAlmostEqual( intra_div, 0, 5 )
        
        self.assertAlmostEqual( intra_div, 0, 5 )
        self.assertAlmostEqual( inter_div, 0, 0 )  # due to higher variance the measure scales up


    def test_lda_with_background(self):

        data, label_idx, node_idx, weights, R = \
                self.sample_data( num_labels=2,
                                  num_nodes=2,
                                  class_offset = np.array([-4,4],np.int32),
                                  node_offset = np.array([8,-8],np.int32),
                                  bgscale=8, bgoffset = 3,
                                  max_weight=1,
                                  rot_angle= np.pi * 33 / 180. )  # [ N x 2 ]
        
        data_f32 = data.astype(np.float32)
        test_stat = MultiNodeClassStatistics( node_idx, label_idx, ( data_f32[:,0], data_f32[:,1] ), weights, do_verify=True )
        _, py_intra_cov, py_inter_cov = test_stat.pythonic_covar_estimation( node_idx, label_idx, ( data[:,0], data[:,1] ), weights )
        
        # compare with python
        self.assertAlmostEqual( js_divergence( test_stat.intra_cov, py_intra_cov ), 0, 4 )
        self.assertAlmostEqual( js_divergence( test_stat.inter_cov, py_inter_cov ), 0, 3 )  


        # Run lda
        eigenvalue, eigenvector = test_stat.run_lda()

        major_vect = eigenvector[:,eigenvalue.argmax()]
        
        # how well has the data carrying dimension been recovered
        major_vect_scalar_diff = np.absolute( np.dot( R.transpose()[0,:], major_vect ) ) 

        # print( R )
        # print( eigenvector )
        # print( "eigenvalue: ", eigenvalue )
        # print( "orig_cov:", np.cov(data.transpose()) )
        # print( "inter_cov: ", test_stat.inter_cov )
        # print( "intra_cov: ", test_stat.intra_cov )
        # print( "diff: ", major_vect_scalar_diff )
        # self.plot_data( data, label_idx, node_idx )
        
        # Compare first rotation component with most  separating lda component
        self.assertAlmostEqual( major_vect_scalar_diff, 1, 6 )


    def test_lda_with_node_hierarchy_change(self):
        """
        Test is for both, node order changes and lda statistc calculation
        """
        num_layers = 5

        data, label_idx, node_idx, weights, R = \
                self.sample_data( num_labels = 6,
                                  num_nodes = 2**(num_layers-1),
                                  class_offset = np.arange( -12, 12, 4, dtype=np.int32 ),
                                  node_offset = np.arange( 15, 63, 3, dtype=np.int32 ),
                                  bgscale=8, bgoffset = 3,
                                  max_weight=99,
                                  rot_angle= np.pi * 11 / 180. )  # [ N x 2 ]

        data_f32 = data.astype(np.float32)
        feature_rows = create_feature_rows()
        feature_rows.extend( ( data_f32[:,0], data_f32[:,1] ) )
        
        compare_stat = MultiNodeClassStatistics( node_idx, label_idx, feature_rows, weights, do_verify=True )

        # alter layer indices
        for lidx in range( 0, num_layers ):
            s_idx = select_layers( node_idx, np.array( [lidx], dtype=np.int32 ), num_layers = num_layers )
            r_idx = remove_layer( node_idx, lidx, num_layers = num_layers )
            self.assertTrue( np.issubdtype( s_idx.dtype, np.int32 ) )
            self.assertTrue( np.issubdtype( r_idx.dtype, np.int32 ) )

            # shift up indices and bring cut out layer in at the lower bit 
            n_node_idx = np.bitwise_or( r_idx << 1, s_idx ).astype(r_idx.dtype)
            self.assertTrue( np.issubdtype( n_node_idx.dtype, np.int32 ) )   # bitwise or might be buggy

            # new note statistics
            n_stat = MultiNodeClassStatistics( n_node_idx, label_idx, feature_rows, weights, do_verify=True )

            self.assertTrue( np.all( np.isclose( compare_stat.intra_cov, n_stat.intra_cov ) ) )
            self.assertTrue( np.all( np.isclose( compare_stat.inter_cov, n_stat.inter_cov ) ) )


    # ---------------------------------------------------------------------------------------------------------------------

    def sample_data(self, num_labels, num_nodes, class_offset, node_offset, bgscale, bgoffset, max_weight=1, rot_angle=0 ):
        randstate = np.random.RandomState( self.randseed )

        labels = randstate.randint( num_labels, size=self.num_data, dtype=np.int32 )
        nodes = randstate.randint( num_nodes, size=self.num_data, dtype=np.int32 )

        # main data dim
        data = randstate.normal( size=self.num_data ).astype( self.dtype ) \
                + class_offset.take( labels, axis=0 ) \
                + node_offset.take( nodes, axis=0 )
        
        # background noise dim
        backgr = (bgscale * randstate.normal( size=self.num_data ) ) + bgoffset

        #combined data
        data = np.stack(( data, backgr )) # [ 2 x N ]

        R = np.array( [ [ np.cos( rot_angle ), -np.sin( rot_angle) ],
                        [ np.sin( rot_angle ),  np.cos( rot_angle) ] ], dtype=self.dtype )
        # weights
        weights = randstate.randint(1,max_weight+1, size=self.num_data, dtype=np.int32 )
        assert( np.all(weights <= max_weight) )

        return ( np.dot(R, data).transpose(), labels, nodes, weights, R ) # [ N x 2 ]
            
    
    @staticmethod
    def plot_data( data, label_idx, node_idx ):
        plt.figure()
        plt.plot( data[label_idx==0,0], data[label_idx==0,1], ",r",
                  data[label_idx==1,0], data[label_idx==1,1], ",b" )
        plt.show()



if __name__ == '__main__':
    unittest.main()

