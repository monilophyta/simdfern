# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["TestPerformanceTracker", "plot_pef_distributions"]


import time
import threading
import os
#import atexit
from pathlib import Path
import pickle
#import typing as tp
import numpy as np

from ..simd import default_simd_vector32_size


# for plotting
#from matplotlib import pyplot as plt



def current_git_branch():
    cmd = "git status -sb 2>/dev/null | grep '##'"
    stream = os.popen(cmd)
    branch = stream.read().strip()
    if len(branch) > 3:
        branch = branch[3:]
    else:
        branch = None
    return branch


def current_git_commit_id():
    cmd = "git log --pretty=format:'%H' -n 1 2>/dev/null"
    stream = os.popen(cmd)
    hashid = stream.read().strip()
    if len(hashid) > 0:
        return hashid
    return None


def get_environment_condidtions() -> dict:
    cond = dict( git_branch = current_git_branch(),
              git_commit_id = current_git_commit_id(),
                  vect_size = default_simd_vector32_size(),
                   tree_lib = None )
    
    ENV_NAME="USE_TREE_LIB"
    if ENV_NAME in os.environ:
        cond["tree_lib"] = os.environ[ENV_NAME].upper()
    return cond

# ------------------------------------------------------------------------------------------------

class TestMeasurement(object):

    time_stamp         : float
    mutual_information : float
    gini_index         : float
    conditions         : dict


    def __init__(self, gini : float, mutual : float, conditions : dict ):
        self.time_stamp = time.time()
        self.mutual_information = float(mutual)
        self.gini_index = float(gini)
        self.conditions = conditions



class TestPerformanceTracker(object):

    # dictionary holding all fname dependent singletons
    __it__ = dict()

    # file name dependent singleton
    instance_creation_look : threading.RLock = threading.RLock()

    @classmethod
    def get_instance(cls, fname : str ):
        with cls.instance_creation_look:
            try:
                it = cls.__it__[fname]
            except KeyError:               
                it = cls.__it__[fname] = cls( fname = fname )
            return it

    def __init__(self, fname : str ):
        with self.instance_creation_look:
            if fname in self.__it__:
                raise RuntimeError( "Second instantiation of singleton")
            
            self.__fname = None
            self.__add_lock = threading.RLock()
            self.__conditions = get_environment_condidtions()
            
            fname = Path(fname)
            if fname.is_file():
                with fname.open("rb") as fin:
                    self.__test_dict = pickle.load(fin)
            else:
                self.__test_dict = {}
            self.__fname = fname

            # make sure dictionary is stored in the end
            #atexit.register( self.store )

    
    # ---------------------------------------------------------------------
    
    __add_lock   : threading.RLock
    __fname      : Path
    __test_dict  : dict
    __conditions : dict
    


    def add( self, test_key : str, gini : float, mutual : float ):
        with self.__add_lock:
            tlist = self.__test_dict.get( test_key, list() )
            tlist.append( TestMeasurement( gini=gini, mutual=mutual, conditions=self.__conditions ) )
            self.__test_dict[test_key] = tlist
            self.store()


    def store(self):
        with self.__add_lock:
            if self.__fname is not None:
                with self.__fname.open("wb") as fout:
                    pickle.dump( self.__test_dict, fout )

# ----------------------------------------------------------------------------------------------------------------------------

def plot_pef_distributions( experiments : dict, ax ):
    #from matplotlib import ticker

    axt = ax.twiny()
    
    data = list(experiments.items())
    data.sort()

    exp_names, data = tuple( zip(*data) )
    
    get_gini_data = lambda mlist: np.array([ m.gini_index for m in mlist ], dtype=np.float32 )
    gini_data = [ get_gini_data( exp_data ) for exp_data in data ]
    get_muinf_data = lambda mlist: np.array([ m.mutual_information for m in mlist ], dtype=np.float32 )
    muinf_data = [ get_muinf_data( exp_data ) for exp_data in data ]

    Y = np.arange( len(exp_names ) ).astype(np.float32)

    for idx, (gini, muinf) in enumerate( zip(gini_data, muinf_data) ):
        ypos = Y[idx]
        yoffset = np.random.rand(len(gini)) * 0.1
        axt.plot( muinf, ypos - yoffset, "rd" )
        ax.plot( gini, ypos + yoffset, "bd" )
        
    
    ax.set_yticks( np.arange(len(exp_names) ) )
    ax.set_yticklabels(exp_names)
    ax.invert_yaxis()
    ax.set_xlabel("gini index")
    axt.set_label("mutual information")
    axt.grid(False)
    ax.grid(True)




