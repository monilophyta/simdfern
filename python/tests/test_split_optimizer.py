# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



import unittest
#import sys
#sys.path.append('../')

import numpy as np
from ..ctypes_py.tree_split_optimization import TreeSplitOptimizer
from ..ctypes_py.fern_split_optimization import FernSplitOptimizer
from ..ctypes_py.tree_ctypes_return_codes import ReturnCode


class TestSplitOptimizer( unittest.TestCase ):

    rand_seed = 69
    
    num_data = 124
    num_nodes = 16
    num_labels = 14

    single_data_weight = 2

    nodeIdx = None
    labelIdx = None
    dataWeight = None
    featureVals1 = None
    featureVals2 = None


    def __init__(self, *aparam, **dparam ):
        super().__init__( *aparam, **dparam )

        np.random.seed( self.rand_seed )

        # create node indices - integers from `low` (inclusive) to `high` (exclusive)
        self.nodeIdx = np.random.randint( 0, self.num_nodes, size=self.num_data, dtype=np.int32 )
        assert( np.unique( self.nodeIdx ).size == self.num_nodes )  # all nodes are used
        assert( np.all( np.unique( self.nodeIdx, return_counts=True )[1] > 2 ) )  # every node has at least two data points

        # create label data - integers from `low` (inclusive) to `high` (exclusive)
        self.labelIdx = np.random.randint( 0, self.num_labels, size=self.num_data, dtype=np.int32 )
        assert( np.unique( self.labelIdx ).size == self.num_labels )  # all labels are used

        self.dataWeight = np.empty( self.num_data, dtype=np.int32 )
        self.dataWeight.fill( self.single_data_weight )

        # create feature values
        self.featureVals1 = np.random.rand( self.num_data ).astype(np.float32)
        self.featureVals2 = np.random.rand( self.num_data ).astype(np.float32)



    def test_tree_parent_statistic_initialization(self):
        np.random.seed( self.rand_seed )

        optim = TreeSplitOptimizer( node_idx = self.nodeIdx,
                                   label_idx = self.labelIdx,
                                data_weights = self.dataWeight, 
                                   num_nodes = self.num_nodes,
                                  num_labels = self.num_labels )
        
        # check pointer initialization
        self.assertTrue( optim.split_optimizer_handle.value > 0 )

    

    def test_tree_feature_processing( self ):
        np.random.seed( self.rand_seed )

        optim = TreeSplitOptimizer( node_idx = self.nodeIdx,
                                   label_idx = self.labelIdx,
                                data_weights = self.dataWeight, 
                                   num_nodes = self.num_nodes,
                                  num_labels = self.num_labels )

        num_improvements = optim.process_feature( self.featureVals1 )
        self.assertGreaterEqual( num_improvements, 0 )


    def test_continous_tree_split_extraction( self ):
        np.random.seed( self.rand_seed )

        optim = TreeSplitOptimizer( node_idx = self.nodeIdx,
                                   label_idx = self.labelIdx,
                                data_weights = self.dataWeight, 
                                   num_nodes = self.num_nodes,
                                  num_labels = self.num_labels )

        # First Optimization
        num_improvements = optim.process_feature( self.featureVals1 )
        self.assertGreaterEqual( num_improvements, 0 )

        tree_layer, ratings = optim.get_optimal_splits( do_align = True )

        # Eval first optimization run
        self.assertEqual( np.issubdtype( ratings.dtype, np.integer),
                          optim.optimization_method.has_gini_rating_flag )
        self.assertEqual( np.issubdtype( ratings.dtype, np.floating),
                          optim.optimization_method.has_entropy_rating_flag )
        
        self.assertEqual( ratings.size, tree_layer.feature_idx.size )
        self.assertEqual( ratings.size, tree_layer.feature_split.size )
        
        self.assertEqual( ratings.max() <= 0, optim.optimization_method.has_gini_rating_flag )
        self.assertEqual( ratings.min() >= 0, optim.optimization_method.has_entropy_rating_flag )

        self.assertTrue( np.all( tree_layer.feature_idx == 0 ) )
        self.assertGreater( tree_layer.feature_split.min(), self.featureVals1.min() )  # actually greater equal
        self.assertLess( tree_layer.feature_split.max(), self.featureVals1.max() )  # actually less equal


        # Eval first optimization run
        n_iterators = 0
        while True:
            n_iterators += 1

            featureVals_n = np.random.rand( self.num_data ).astype(np.float32)

            num_improvements = optim.process_feature( featureVals_n )
            self.assertGreaterEqual( num_improvements, 0 )

            if num_improvements > 0:
                tree_layer_n, ratings_n = optim.get_optimal_splits( do_align = True )
                
                self.assertGreaterEqual( tree_layer_n.feature_idx.min(), 0 )
                self.assertLessEqual( tree_layer_n.feature_idx.max(), n_iterators )

                self.assertTrue( np.all( tree_layer_n.feature_idx >= tree_layer.feature_idx ) )
                self.assertTrue( np.any( tree_layer_n.feature_idx > tree_layer.feature_idx ) )

                self.assertTrue( np.all( ratings_n <= ratings ) )
                self.assertTrue( np.any( ratings_n < ratings ) )

                tree_layer, ratings = tree_layer_n, ratings_n
            else:
                self.assertTrue( np.all( ratings_n == ratings ) )
                self.assertTrue( np.all( tree_layer_n.feature_idx == tree_layer.feature_idx ) )
                self.assertTrue( np.all( tree_layer_n.feature_split == tree_layer.feature_split ) )
                break

        #print( "Num optimization runs: ", n_iterators )
    
    ############################################################################################################
    
    
    def test_fern_parent_statistic_initialization(self):
        np.random.seed( self.rand_seed )

        optim = FernSplitOptimizer( node_idx = self.nodeIdx,
                                   label_idx = self.labelIdx,
                                data_weights = self.dataWeight, 
                                   num_nodes = self.num_nodes,
                                  num_labels = self.num_labels )

        # check pointer initialization
        self.assertTrue( optim.split_optimizer_handle.value > 0 )


    def test_fern_feature_processing( self ):
        np.random.seed( self.rand_seed )

        optim = FernSplitOptimizer( node_idx = self.nodeIdx,
                                   label_idx = self.labelIdx,
                                data_weights = self.dataWeight, 
                                   num_nodes = self.num_nodes,
                                  num_labels = self.num_labels )

        num_improvements = optim.process_feature( self.featureVals1 )
        self.assertGreaterEqual( num_improvements, 0 )
    

    def test_resetting_optimal_split( self ):
        optim = FernSplitOptimizer( node_idx = self.nodeIdx,
                                   label_idx = self.labelIdx,
                                data_weights = self.dataWeight, 
                                   num_nodes = self.num_nodes,
                                  num_labels = self.num_labels )
        
        pre_fern_layer_n, pre_rating_n = optim.get_optimal_split()
        self.assertFalse( np.isfinite( pre_fern_layer_n.feature_split ) )
        self.assertEqual( pre_rating_n , 2147483647 )

        # First Optimization
        num_improvements = optim.process_feature( self.featureVals1 )
        self.assertGreaterEqual( num_improvements, 0 )

        post_fern_layer, post_rating = optim.get_optimal_split()
        self.assertTrue( np.isfinite( post_fern_layer.feature_split ) )
        self.assertLess( post_rating, 2147483647 )

        # call test function
        optim.reset_optimal_split()

        reset_fern_layer, reset_rating = optim.get_optimal_split()
        self.assertFalse( np.isfinite( reset_fern_layer.feature_split ) )
        self.assertEqual( pre_rating_n, reset_rating )

        # optimize again
        num_improvements = optim.process_feature( self.featureVals1 )
        self.assertGreaterEqual( num_improvements, 0 )
        
        reoptim_fern_layer, reoptim_rating = optim.get_optimal_split()
        self.assertEqual( post_rating, reoptim_rating )
        self.assertTrue( np.isfinite( reoptim_fern_layer.feature_split ) )



    def test_continous_fern_split_extraction( self ):
        np.random.seed( self.rand_seed )

        optim = FernSplitOptimizer( node_idx = self.nodeIdx,
                                   label_idx = self.labelIdx,
                                data_weights = self.dataWeight, 
                                   num_nodes = self.num_nodes,
                                  num_labels = self.num_labels )

        # First Optimization
        num_improvements = optim.process_feature( self.featureVals1 )
        self.assertGreaterEqual( num_improvements, 0 )

        fern_layer, rating = optim.get_optimal_split()

        # Eval first optimization run
        self.assertEqual( isinstance( rating, int),
                          optim.optimization_method.has_gini_rating_flag )
        self.assertEqual( isinstance( rating, float),
                          optim.optimization_method.has_entropy_rating_flag )
        
        self.assertEqual( rating <= 0, optim.optimization_method.has_gini_rating_flag )
        self.assertEqual( rating >= 0, optim.optimization_method.has_entropy_rating_flag )
        
        self.assertEqual( fern_layer.feature_idx, 0  )
        self.assertGreater( fern_layer.feature_split, self.featureVals1.min() )  # actually greater equal
        self.assertLess( fern_layer.feature_split, self.featureVals1.max() )  # actually less equal


        # Eval first optimization run
        n_iterators = 0
        while True:
            n_iterators += 1

            featureVals_n = np.random.rand( self.num_data ).astype(np.float32)

            num_improvements = optim.process_feature( featureVals_n )
            self.assertGreaterEqual( num_improvements, 0 )
            
            fern_layer_n, rating_n = optim.get_optimal_split()
            
            self.assertEqual( isinstance( rating_n, int),
                              optim.optimization_method.has_gini_rating_flag )
            self.assertEqual( isinstance( rating_n, float),
                              optim.optimization_method.has_entropy_rating_flag )
            
            self.assertEqual( rating_n <= 0, optim.optimization_method.has_gini_rating_flag )
            self.assertEqual( rating_n >= 0, optim.optimization_method.has_entropy_rating_flag )


            if num_improvements > 0:
                self.assertEqual( fern_layer_n.feature_idx, n_iterators )

                self.assertGreater( fern_layer_n.feature_idx, fern_layer.feature_idx )

                #self.assertLessEqual( rating_n, ratings )
                self.assertLess( rating_n, rating )

                fern_layer, rating = fern_layer_n, rating_n
            else:
                self.assertEqual( rating_n, rating )
                self.assertEqual( fern_layer_n.feature_idx, fern_layer.feature_idx )
                self.assertEqual( fern_layer_n.feature_split, fern_layer.feature_split )
                break

        #print( "Num optimization runs: ", n_iterators )



if __name__ == '__main__':
    unittest.main()
