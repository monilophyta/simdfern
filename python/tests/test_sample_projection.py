# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



import unittest
#import sys
#sys.path.append('../')

import numpy as np
from ..ctypes_py import project_features, FeatureRows, create_feature_rows, ESummationAlgo


class SampleProjectionText( unittest.TestCase ):

    num_data = 1797
    num_features = 7


    @staticmethod
    def generate_data( num_data, num_features ):
        
        feature_weights = np.reciprocal( num_features * np.arange( 1, num_features+1, dtype=np.float32 ) )    # [ F ]

        feature_rows = np.multiply.outer( np.arange( 1, num_features+1, dtype=np.float32 ),
                                          np.arange( num_data, dtype=np.float32 ) )                         # [ F x N ]
        
        reference_proj_data = np.arange( num_data, dtype=np.float32 )

        return (feature_weights, feature_rows, reference_proj_data)


    def test_ndot_branch(self):

        feature_weights, feature_rows, reference_proj_data = self.generate_data( self.num_data, self.num_features )

        ndarray_res = project_features( feature_weights, feature_rows )

        self.assertTrue( np.all( np.isclose( reference_proj_data, ndarray_res ) ) )
    

    def test_sequence_branch(self):

        feature_weights, feature_rows, reference_proj_data = self.generate_data( self.num_data, self.num_features )
        feature_rows = list([ feature_rows[i,:] for i in range( self.num_features ) ])

        list_res = project_features( feature_weights, feature_rows )

        self.assertTrue( np.all( np.isclose( reference_proj_data, list_res ) ) )


    def test_FeatureRow_branch(self):
        
        feature_weights, feature_rows, reference_proj_data = self.generate_data( self.num_data, self.num_features )
        feature_rows = list([ feature_rows[i,:] for i in range( self.num_features ) ])
    
        feature_row_struct = create_feature_rows()
        feature_row_struct.extend( feature_rows )

        for sum_algo in ESummationAlgo:
            frow_res = project_features( feature_weights, feature_row_struct, sum_algo )
            self.assertTrue( np.all( np.isclose( reference_proj_data, frow_res ) ) )


if __name__ == '__main__':
    #unittest.main()
    inst = SampleProjectionText()
    inst.test_FeatureRow_branch()




