# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


import logging
import unittest
#import os
#import sys
#sys.path.append( os.path.dirname(  os.path.dirname( os.path.abspath(__file__) ) ) )
#os.path.join( os.path.dirname( os.path.dirname( os.path.abspath(__file__) ) ), "ctypes_src" )

from .test_box_feature_evaluation import *
from .test_integral_image import *
from .test_bittools import *
from .test_ctypes_information_measures import *

from .test_sample_projection import *
from .test_feature_projection import *

from .test_split_optimizer import *
from .test_fern_partition_param_optimization import *

from .test_classification import *
from .test_image_sequence_subset_sampling import *

from .test_linear_discriminat_analysis import *
from .test_information_measure import *

from .test_image_classification_tree import *

from .test_layer_selection import *

from .test_iris_training import *
from .test_digits_training import *

from .test_binary_conversion import *


if __name__ == '__main__':
    logging.basicConfig( level=logging.DEBUG )

    import cProfile
    pr = cProfile.Profile()
    pr.enable()

    try:
        unittest.main()
    finally:
        pr.disable()
        pr.dump_stats("unit_tests.prof")
