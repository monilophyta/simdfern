# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

import unittest

import numpy as np

from .test_training_test_base import DebugFernTrainingObserver, DebugTreeTrainingObserver
from .test_iris_training_base import IrisTrainingBase

#from ..parameter_registration import default_tree_training_params, default_combined_training_params
from ..parameter_registration import default_fern_training_params

from ..training import TreeTrainer, FernTrainer
from ..training import TreeTrainingContext, FernTrainingContext

#from ..training import CountingTreeTrainingObserver
#from ..training import CountingFernTrainingObserver
#from ..training import ObserverNotificationSource as ons

#from ..clmath import normalize_entropy, MultiNodeClassStatistics
from ..clmath import normalize_gini

# create dummy logger
import logging
dummy_logger = logging.getLogger( "test_iris_training" )
dummy_logger.setLevel( logging.WARNING )

# ------------------------------------------------------------------------------------------

class IrisTrainingTest( IrisTrainingBase, unittest.TestCase ):

    num_tree_layers = 3
    num_fern_layers = 3

    # ------------------------------------------------------------------------------------------

    def __init__(self, *args, **argv ):
        IrisTrainingBase.__init__(self)
        unittest.TestCase.__init__( self, *args, **argv )


    def test_tree_training(self):
        
        tree_features = self.raw_data
        #tree_features = self.ratio_features

        trainer = TreeTrainer( self.labels, self.data_weights, logger=dummy_logger )

        # Debugging Observer
        observer = DebugTreeTrainingObserver( self )
        #observer = None

        # Training Context
        train_context = TreeTrainingContext( tree_features, observer )

        # test call
        trainer.train_tree( train_context, self.num_tree_layers )

        # get history of rating
        node_ratings = train_context.observer.ratings

        self.assertEqual( train_context.num_layers, self.num_tree_layers )
        self.assertEqual( len(node_ratings), self.num_tree_layers )
        self.assertTrue( np.all( np.absolute( train_context.leaf_idx ) < 2**(self.num_tree_layers) ) )

        # check that classification is consistent after training
        self.assertTrue( trainer.verify_classification( train_context, do_throw=False ) )

        # -----------------------------------------------------------------

        mutual_inf_measure = np.array( observer.mutual_inf_measure, dtype=np.float32 )
        self.assertTrue( np.all( mutual_inf_measure[:-1] <= mutual_inf_measure[1:] ) )
        self.assertGreaterEqual( mutual_inf_measure[-1], 1.4808 )
        
        gini_measure = np.array( observer.gini_measure, dtype=np.float32 )
        self.assertTrue( np.all( gini_measure[:-1] <= gini_measure[1:] ) )
        self.assertGreaterEqual( gini_measure[-1], 0.98 )
        
        # -----------------------------------------------------------------
        
        for lidx in range(1,self.num_tree_layers):
            # check for rating improvements
            self.assertTrue( np.all( node_ratings[lidx-1] >= (node_ratings[lidx][::2] + node_ratings[lidx][1::2]) ) )

        for lidx, ratings in enumerate( node_ratings ):
            # checking correctness of rating
            rating_sum = ratings.sum()

            assert rating_sum <= 0  # using gini measure only
            normed_rating = normalize_gini( -rating_sum, self.data_magnitude, self.num_labels )
            self.assertAlmostEqual( normed_rating, gini_measure[lidx], 6 )
            
    # ------------------------------------------------------------------------------------------

    

    def test_fern_training_with_subsequent_optimization(self):
        
        # -----------------------------------------------------------------
        # Test Case 1.1
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "" )
        self.evaluate_fern( trainer, train_context )  # -298

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "N" )
        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context )
        
        # -------------------------------------------------------------------------------------------------
        # Test Case 1.2
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "" )
        self.evaluate_fern( trainer, train_context )

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "LN" )
        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context )
        
        # -------------------------------------------------------------------------------------------------
        # Test Case 1.3
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "" )
        self.evaluate_fern( trainer, train_context )

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "NL" )
        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context )
        
        # -------------------------------------------------------------------------------------------------
        # Test Case 1.4
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "")
        self.evaluate_fern( trainer, train_context )

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "LNL" )
        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context )
        

        # -------------------------------------------------------------------------------------------------
        # Test Case 2
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "" )
        self.evaluate_fern( trainer, train_context )

        # optimizing the layers
        previous_rating = train_context.rating
        optim_node_rating = trainer.optimize_fern( train_context, layer_optimization_steps = "L" )
        self.assertGreaterEqual( previous_rating, optim_node_rating )
        self.evaluate_fern( trainer, train_context )

        # -----------------------------------------------------------------


    def test_fern_training_with_parallel_optimization(self):

        # -------------------------------------------------------------------------------
        # Test Case 1.1
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "N" )
        self.evaluate_fern( trainer, train_context )

        # -------------------------------------------------------------------------------
        # Test Case 1.2
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "LN" )
        self.evaluate_fern( trainer, train_context )

        # -------------------------------------------------------------------------------
        # Test Case 1.3
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "NL" )
        self.evaluate_fern( trainer, train_context )

        # -------------------------------------------------------------------------------
        # Test Case 1.4
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "LNL" )
        self.evaluate_fern( trainer, train_context )

        # -------------------------------------------------------------------------------
        # Test Case 2
        trainer, train_context = self.run_fern_training( layer_optimization_steps = "L" )
        self.evaluate_fern( trainer, train_context )



    def run_fern_training(self, layer_optimization_steps : str ):

        fern_features = self.raw_data

        # configure training parameters
        parameter_registry = default_fern_training_params.copy()

        # creater trainer instance
        trainer = FernTrainer( self.labels, self.data_weights, logger=dummy_logger, default_params=parameter_registry )

        # Debugging Observer
        observer = DebugFernTrainingObserver( self )
        #observer = None

        # Training Context
        train_context = FernTrainingContext( observer )
        train_context.feature_rows = fern_features

        # Training Context
        node_ratings = trainer.train_fern( train_context, 
                                           self.num_fern_layers,
                                           layer_optimization_steps = layer_optimization_steps )
        
        #print( "FernTrainingContext", train_context.observer.calls )

        self.assertEqual( len(node_ratings), self.num_fern_layers )
        self.assertTrue( np.all( node_ratings[:-1] >= node_ratings[1:] ) )

        return (trainer, train_context)

        

    def evaluate_fern(self, trainer, train_context ):
        observer = train_context.observer

        # -----------------------------------------------------------------

        self.assertEqual( train_context.num_layers, self.num_fern_layers )
        self.assertTrue( np.all( train_context.leaf_idx < 2**(self.num_fern_layers) ) )

        # check that classification is consistent after training
        self.assertTrue( trainer.verify_classification( train_context, do_throw=False ) )

        # -----------------------------------------------------------------

        #best_observation = 1.5062
        best_observation = 1.5034
        mutual_inf_measure = np.array( observer.mutual_inf_measure, dtype=np.float32 )
        self.assertTrue( np.all( mutual_inf_measure[:-1] <= mutual_inf_measure[1:] ) )
        #print(mutual_inf_measure)
        self.assertGreaterEqual( mutual_inf_measure[-1], best_observation )
        
        best_observation = 0.986
        gini_measure = np.array( observer.gini_measure, dtype=np.float32 )
        self.assertTrue( np.all( gini_measure[:-1] <= gini_measure[1:] ) )
        #print(gini_measure)
        self.assertGreaterEqual( gini_measure[-1], best_observation )

        # -----------------------------------------------------------------
        # check rating (unormaled negative gini indices)
        
        normed_ratings = normalize_gini( -1 * np.array( observer.ratings, dtype=np.int32 ), self.data_magnitude, self.num_labels )
        self.assertTrue( np.all( np.isclose( normed_ratings, gini_measure ) ) )
