# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



import unittest
import numpy as np 

from ..clmath import mutual_counts



class InformationMeasureTest( unittest.TestCase ):

    rand_seed = 49

    num_data = 10003
    num_labels = 5

    max_weight = 4

    # --------------------------------------------------------------

    labels = None
    data_weights = None

    perfect_nodes = None
    random_nodes = None

    # --------------------------------------------------------------

    def __init__(self, *args, **argv ):
        super().__init__( *args, **argv )

        # build up data
        self.sample_data()
    

    def sample_data(self):

        self.labels = np.random.randint( 0, self.num_labels, size=self.num_data, dtype=np.int32 )

        self.random_nodes = np.random.randint( 0, self.num_labels, size=self.num_data, dtype=np.int32 )
        self.perfect_nodes = (self.labels * self.num_labels) + self.random_nodes


    def test_equal(self):

        mcounts = mutual_counts( self.labels, self.labels )

        self.assertAlmostEqual( 1.0, mcounts.mutual_information(do_norm=True), 5 )
        self.assertAlmostEqual( 1.0, mcounts.gini_index(axis=0), 5 )
        self.assertAlmostEqual( 1.0, mcounts.gini_index(axis=1), 5 )


    def test_perfect(self):

        mcounts = mutual_counts( self.labels, self.perfect_nodes )

        self.assertAlmostEqual( 1.0, mcounts.mutual_information(do_norm=True), 5 )
        self.assertAlmostEqual( 1.0, mcounts.gini_index(axis=0), 5 )
        self.assertGreater( 1.0, mcounts.gini_index(axis=1) )


    def test_random(self):


        mcounts = mutual_counts( self.labels, self.random_nodes )

        # theoretical gini: 
        # https://en.wikipedia.org/wiki/Gini_coefficient#Continuous_probability_distribution
        # Remark: Uniform distribution is actually wrong.It should be Multinomial distribution
        a = mcounts.counts.min()
        b = mcounts.counts.max()
        uniform_theory_gini = float(b - a) / float(3 * (b+a))


        #print("Random: ", uniform_theory_gini)
        #print( mcounts.gini_index(axis=0) )
        ##print( mcounts.gini_index(axis=1) )
        #print( mcounts.mutual_information(do_norm=True) )


        self.assertGreater( 0.001, mcounts.mutual_information(do_norm=True) )
        self.assertAlmostEqual( uniform_theory_gini, mcounts.gini_index(axis=0), 1 )
        #self.assertGreater( 1.0, mcounts.gini_index(axis=1) )






