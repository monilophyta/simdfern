# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = [ "IrisTrainingBase" ]


import functools
import numpy as np

from sklearn.datasets import load_iris


# ------------------------------------------------------------------------------------------

class IrisTrainingBase( object ):

    raw_data    = None    # [ 4 x N ]
    labels      = None    # [ N ]

    feature_names  = None
    label_names = None

    data_weights = None
    
    # ------------------------------------------------------------------------------------------

    @property
    def num_data(self) -> int:
        assert self.data_magnitude == self.labels.size
        return self.labels.size


    @property
    @functools.lru_cache(maxsize=None)
    def data_magnitude(self) -> int:
        assert self.labels.size == self.data_weights.size
        return int(self.data_weights.sum(dtype=int))

    @property
    @functools.lru_cache(maxsize=None)
    def num_labels(self):
        return np.unique(self.labels).size
    
    @property
    def num_dims(self):
        return self.raw_data.shape[0]
    
    # ------------------------------------------------------------------------------------------


    def __init__(self):
        self.load_iris_features()


    def load_iris_features(self):
        iris = load_iris()

        self.raw_data = iris.data.transpose().astype(np.float32)   # [ 4 x N ]
        self.labels = iris.target.astype(np.int32)                 # [ N ]

        self.feature_names = iris.feature_names
        self.label_names = iris.target_names

        self.data_weights = np.ones_like( self.labels )
