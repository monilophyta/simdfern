# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["TestBinaryConversion"]


import unittest
import numpy as np
from ..ctypes_py import calc_binary_cl_weights
from ..clmath import mutual_counts


class TestBinaryConversion( unittest.TestCase ):

    rand_seed     = 69

    num_data      = 2303 # 13243
    num_labels    = 17
    num_layers    = 5
    max_weight    = 123


    leaf_idx      = None
    label_idx     = None
    data_weight   = None


    @property
    def num_leafs(self):
        return (1 << self.num_layers)


    def __init__(self, *args, **kwargs):
        super(TestBinaryConversion, self).__init__(*args, **kwargs)
        self.sample_data()


    def test_calc_binary_cl_entropy_weights(self):
        
        mcounts = mutual_counts( self.leaf_idx, self.label_idx, weights=self.data_weight, num_X=self.num_leafs, num_Y=self.num_labels )
        unormed_entropy = mcounts.conditional_entropy( axis=1, do_norm=False, use_log2=False )

        for layer_bit_idx in range(self.num_layers):

            right_mask = np.bitwise_and( self.leaf_idx, 1 << layer_bit_idx ) == 0
            left_mask = np.logical_not( right_mask )

            # convert to binary problem
            plane_side, weights = calc_binary_cl_weights( \
                                            layer_bit_idx = layer_bit_idx,
                                            leaf_idx      = self.leaf_idx,
                                            label_idx     = self.label_idx,
                                            data_weight   = self.data_weight,
                                            num_leafs     = self.num_leafs,
                                            num_labels    = self.num_labels,
                                            use_gini_index = False ) 
            
            self.assertTrue( np.all( weights >= 0 ) )

            # mask of every data samples brought to left side
            left_map_mask = plane_side == -1
            assert np.all( np.logical_not( left_map_mask ) == (plane_side == 1) )

            # mask indicating if bit flip causes an improvement
            flip_improvement_mask = left_map_mask != left_mask
            
            # verify all positions
            n_leaf_idx = self.leaf_idx.copy()

            for nidx in range(self.num_data):
                
                n_leaf_idx[nidx] = self.swap_bits( n_leaf_idx[nidx], layer_bit_idx )
                n_mcounts = mutual_counts( n_leaf_idx, self.label_idx, weights=self.data_weight, num_X=self.num_leafs, num_Y=self.num_labels )
                n_unormed_entropy = n_mcounts.conditional_entropy( axis=1, do_norm=False, use_log2=False )

                #print( flip_improvement_mask[nidx], unormed_entropy, n_unormed_entropy, weights[nidx] )

                if flip_improvement_mask[nidx]:
                    diff = unormed_entropy - n_unormed_entropy
                else:
                    diff = n_unormed_entropy - unormed_entropy

                delta = max( 1e-3, 0.05 * max(abs(diff),abs(weights[nidx])) )
                self.assertAlmostEqual(diff, weights[nidx], delta=delta )

                # reset to original value
                n_leaf_idx[nidx] = self.leaf_idx[nidx]



    def test_calc_binary_cl_gini_weights(self):
        
        mcounts = mutual_counts( self.leaf_idx, self.label_idx, weights=self.data_weight, num_X=self.num_leafs, num_Y=self.num_labels )
        unormed_gini = mcounts.gini_index( axis=1, do_norm=False )

        for layer_bit_idx in range(self.num_layers):

            right_mask = np.bitwise_and( self.leaf_idx, 1 << layer_bit_idx ) == 0
            left_mask = np.logical_not( right_mask )

            # convert to binary problem
            plane_side, weights = calc_binary_cl_weights( \
                                            layer_bit_idx = layer_bit_idx,
                                            leaf_idx      = self.leaf_idx,
                                            label_idx     = self.label_idx,
                                            data_weight   = self.data_weight,
                                            num_leafs     = self.num_leafs,
                                            num_labels    = self.num_labels,
                                            use_gini_index = True )
            
            self.assertTrue( np.all( weights >= 0 ) )

            # mask of every data samples brought to left side
            left_map_mask = plane_side == -1
            assert np.all( np.logical_not( left_map_mask ) == (plane_side == 1) )

            # mask indicating if bit flip causes an improvement
            flip_improvement_mask = left_map_mask != left_mask
            
            # verify all positions
            n_leaf_idx = self.leaf_idx.copy()

            for nidx in range(self.num_data):
                
                n_leaf_idx[nidx] = self.swap_bits( n_leaf_idx[nidx], layer_bit_idx )
                n_mcounts = mutual_counts( n_leaf_idx, self.label_idx, weights=self.data_weight, num_X=self.num_leafs, num_Y=self.num_labels )
                n_unormed_gini = n_mcounts.gini_index( axis=1, do_norm=False )


                if flip_improvement_mask[nidx]:
                    self.assertLessEqual( unormed_gini, n_unormed_gini )
                    self.assertEqual( n_unormed_gini - unormed_gini, weights[nidx] )
                else:
                    self.assertGreaterEqual( unormed_gini, n_unormed_gini )
                    self.assertEqual( unormed_gini - n_unormed_gini, weights[nidx] )
                
                # reset to original value
                n_leaf_idx[nidx] = self.leaf_idx[nidx]



    def sample_data( self ):
        rand_state = np.random.RandomState( self.rand_seed )

        self.leaf_idx    = rand_state.randint( 0, self.num_leafs, self.num_data, dtype=np.int32 )
        self.label_idx   = rand_state.randint( 0, self.num_labels, self.num_data, dtype=np.int32 )
        #self.data_weight = rand_state.randint( 0, self.max_weight, self.num_data, dtype=np.int32 )
        self.data_weight = np.ones_like( self.leaf_idx )
    

    @staticmethod
    def swap_bits( leaf_idx, bit_idx ):
        # swap selected bits
        leaf_idx = np.bitwise_xor( leaf_idx, 1 << bit_idx )

        return leaf_idx


