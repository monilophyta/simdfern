# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


import unittest
from ..fern import swap_layers, select_layers, select_bits

import numpy as np


class TestLayerSelection( unittest.TestCase ):

    randseed = 49
    rand_state = None

    num_tests = 150
    num_layers = 30


    test_powers = None
    test_in_data = None


    def __init__(self, *args, **argv ):
        super().__init__( *args, **argv )

        self.rand_state = np.random.RandomState( self.randseed )

        self.test_powers = np.arange( self.num_layers, dtype=np.int32 )     # [ N ]
        self.test_in_data = np.left_shift( 1, self.test_powers, dtype=np.int32 )    # [ N ]
    


    def test_select_bits(self):

        order = np.arange( 0, self.num_layers, dtype=np.int32 )  # [ L ]

        for _ in range( self.num_tests ):
            
            # random reordering
            permute_order  = self.rand_state.permutation( order )   # [ L ]

            mask = permute_order[np.newaxis,:] == self.test_powers[:,np.newaxis]   # [ L x N ]
            _, n_powers = np.nonzero( mask )

            # ground comparisons
            n_test_data = np.left_shift( 1, n_powers, dtype=np.int32 )
            
            # Test call
            reordered = select_bits( self.test_in_data, permute_order )  # [ N ]

            check = reordered == n_test_data
            self.assertTrue( np.all( check ) )



    def test_select_layers(self):

        order = np.arange( 0, self.num_layers, dtype=np.int32 )  # [ L ]

        bits = np.eye( self.num_layers ).astype(np.int32)

        for _ in range( self.num_tests ):
                        
            # random reordering
            permute_order  = self.rand_state.permutation( order )   # [ L ]

            n_test_data = np.array([ (self.test_in_data[::-1] * bits[l,permute_order]).sum() for l in range( self.num_layers) ], dtype=np.int32 )

            # test call
            reordered = select_layers( self.test_in_data[::-1], permute_order, self.num_layers )  # [ N ]

            check = reordered == n_test_data
            self.assertTrue( np.all( check ) )

