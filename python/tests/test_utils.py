# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--




from __future__ import print_function

__all__ = ["BoxSampler"]

import numpy as np


class Box(object):

    Left  = None
    Up    = None
    Right = None
    Down  = None 

    def __init__(self, box_pos, box_size ):
        self.Left  = box_pos[:,0]
        self.Up    = box_pos[:,1]
        self.Right = self.Left + box_size[:,0]
        self.Down  = self.Up + box_size[:,1] 



class BoxSampler(object):
    
    def __init__(self, roiSize, minSize, maxSize ):
        self.roiSize = roiSize
        self.minSize = minSize
        self.maxSize = maxSize

    def sample1D(self, N ):
        # Box Size Sampling
        box_size = np.random.randint( self.minSize, self.maxSize, size=(N,), dtype=np.int )
        
        max_box_pos = self.roiSize - box_size  # [ N ]
        assert( np.all( (max_box_pos + box_size) == self.roiSize ) )
        
        box_pos = (max_box_pos + 1) * np.random.rand( N )
        box_pos = box_pos.astype(np.int)
        
        assert( np.all( box_pos >= 0 ) )
        assert( np.all( box_pos <= max_box_pos ) )
        assert( np.all( (box_pos + box_size) <= self.roiSize ) )
    
        return (box_pos, box_size)
    
    
    def sample2D(self, N ):
        
        box_pos1, box_size1 = self.sample1D( N )
        box_pos2, box_size2 = self.sample1D( N )
        
        box_pos = np.vstack( [ box_pos1, box_pos2 ] ).transpose() # [ N x 2]
        box_size = np.vstack( [ box_size1, box_size2 ] ).transpose() # [ N x 2]
        
        return Box(box_pos, box_size)
