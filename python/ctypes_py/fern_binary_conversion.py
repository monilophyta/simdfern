# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["calc_binary_cl_weights"]

# ----------------------------------------------------------------------------------------------

import ctypes as ct
import typing as tp
import numpy as np

from . import tree_types as tt
from .tree_ctypes_library import fern_binary_conversion_interface as fbcv
from .tree_ctypes_return_codes import ReturnCode


# ----------------------------------------------------------------------------------------------


def calc_binary_cl_weights( 
        layer_bit_idx  : int,
        leaf_idx       : np.ndarray, 
        label_idx      : np.ndarray, 
        data_weight    : np.ndarray,
        num_leafs      : int,
        num_labels     : tp.Optional[int] = None,
        use_gini_index : bool = True ):
    
    assert isinstance( leaf_idx, np.ndarray )
    assert isinstance( label_idx, np.ndarray )
    assert isinstance( data_weight, np.ndarray )
    
    assert np.issubdtype( leaf_idx.dtype, np.int32 )
    assert np.issubdtype( label_idx.dtype, np.int32 )
    assert np.issubdtype( data_weight.dtype, np.int32 )
    
    assert leaf_idx.size == label_idx.size
    assert leaf_idx.size == data_weight.size

    layer_bit_idx = int( layer_bit_idx )
    num_data = int( leaf_idx.size )
    num_leafs = int( num_leafs )
    use_gini_index = bool( use_gini_index )

    if num_labels is None:
        num_labels = label_idx.max() + 1
    
    num_labels = int( num_labels )

    in_data = tt.node_label_list_t.from_numpy( node_idx = leaf_idx, 
                                              label_idx = label_idx,
                                           data_weights = data_weight,
                                             num_labels = num_labels,
                                              num_nodes = num_leafs )
    res = tt.binary_mapping_result_data_t.from_numpy( num_data, use_gini_index )

    if use_gini_index is True:
        ret_code = fbcv.calc_gini_based_binary_cl_weights( \
                ct.byref( in_data ),  \
                layer_bit_idx,        \
                ct.byref( res ) )
    else:
        ret_code = fbcv.calc_entropy_based_binary_cl_weights( \
                ct.byref( in_data ),  \
                layer_bit_idx,        \
                ct.byref( res ) )
    
    ret_code = ReturnCode.from_int_return( ret_code )
    ret_code.raise_on_error()

    return (res.PlaneSide, res.BinaryWeight)
