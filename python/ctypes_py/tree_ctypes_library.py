# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

import glob
import os
import os.path
import fnmatch

import ctypes as ct
import numpy as np

from . import tree_types as tt
from . import tree_types_feature_rows as ttfr
from . import tree_types_fern_weight_regularizer as ttp
from .. import simd

from collections import namedtuple


__all__ = ["get_cpp_event_counts",
           "box_eval_interface",
           "information_measure_interface",
           "tree_split_optim_interface",
           "fern_split_optim_interface",
           "fern_line_search_interface",
           "classification_interface",
           "lda_interface",
           "sample_projection_interface",
           "fern_binary_conversion_interface"  ]


# ----------------------------------------------------------------------------------------------

get_cpp_event_counts = None


box_eval_interface_tuple = namedtuple( 
        'box_eval_interface', [ \
            'evaluate_box_features' ] )

box_eval_interface = None


information_measure_interface_tuple = namedtuple( 
        'information_measure_interface', [ \
            'calc_unormed_gini_index',
            'calc_unormed_conditional_entropy' ] )

information_measure_interface = None


tree_split_optim_interface_tuple = namedtuple(
        'tree_split_optim_interface', [ \
            'create_split_optimizer',
            'destruct_split_optimizer',
            'initialize_parent_layer_statistic',
            'process_feature',
            'get_optimal_splits' ] )

tree_split_optim_interface = None


fern_split_optim_interface_tuple = namedtuple(
        'fern_split_optim_interface', [ \
            'create_split_optimizer',
            'reset_optimal_split',
            'destruct_split_optimizer',
            'initialize_parent_layer_statistic',
            'process_feature',
            'get_optimal_split' ] )

fern_split_optim_interface = None


fern_line_search_interface_tuple = namedtuple(
        'fern_line_search_interface', [ \
            'fern_line_search_on_entropy',
            'fern_line_search_on_gini' ] )

fern_line_search_interface = None


classification_interface_tuple = namedtuple(
    'classification_interface', [ 'classify_samples' ] )

classification_interface = None



lda_interface_tuple = namedtuple( 
    'lda_interface', [ 'calculate_lda_covariances' ] )

lda_interface = None


sample_projection_interface_tuple = namedtuple( 
    'sample_projection_interface', [ 'project_samples' ] )

sample_projection_interface = None


fern_binary_conversion_tuple = namedtuple( 
    'fern_binary_conversion_interface', [
        'calc_entropy_based_binary_cl_weights',
        'calc_gini_based_binary_cl_weights'] )

fern_binary_conversion_interface = None


# ----------------------------------------------------------------------------------------------



def find_library( assumed_location, search_dir ):

    assert os.path.isdir( assumed_location )
    assert os.path.isdir( search_dir )

    # Base order
    lib_fnames = [ "libfern.*", "libfernd.*" ]

    ENV_NAME="USE_TREE_LIB"

    # check which variable should be loaded
    if ENV_NAME in os.environ:
        if os.environ[ENV_NAME].upper() == "RELEASE":
            pass # nothing to do
        elif os.environ[ENV_NAME].upper() == "DEBUG":
            lib_fnames = lib_fnames[::-1] # load debug first
        else:
            raise EnvironmentError( "Wrong value of environment variable USE_TREE_LIB=%s" % str(os.environ[ENV_NAME]) )
    
    elif __debug__:
        # load debug library first
        lib_fnames = lib_fnames[::-1] # load debug first

    for lfname in lib_fnames:

        # check aussumed location for
        path_list = glob.glob( os.path.join( assumed_location, lfname ) )
        path_list = [ p for p in path_list if os.path.splitext( p )[1] in {'.so', '.dll'} ]
        if len(path_list) > 0:
            return path_list[0]
        
        # recursively search in search_dir
        for dirpath, dirnames, filenames in os.walk( search_dir ):
            for filename in fnmatch.filter( filenames, lfname ):
                if  os.path.splitext( filename )[1] in {'.so', '.dll'}:
                    return os.path.join( dirpath, filename )
    
    return None


def load_fern_library():

    this_path = os.path.dirname( os.path.abspath(__file__) )
    repo_path = this_path
    for i in range(2):
        repo_path = os.path.dirname( repo_path )

    lib_path = find_library( this_path, repo_path )
    if lib_path is None:
        raise RuntimeError("random fern C++ library binary not found")
    
    print("Loading library %s" % lib_path )
    lib_path, lib_fname = os.path.split( lib_path )
    tree_lib = np.ctypeslib.load_library( lib_fname, lib_path )

    return tree_lib


# ----------------------------------------------------------------------------------------------


def create_interface( tree_lib ):

    # -------------------------------------------------------------------------------------------

    global get_cpp_event_counts
    tree_lib.getEventCounts.restype = tt.EventStat
    get_cpp_event_counts = tree_lib.getEventCounts

    # -------------------------------------------------------------------------------------------

    global box_eval_interface
    tree_lib.evaluate_box_features.restype = ct.c_int32
    tree_lib.evaluate_box_features.argtypes = [ \
        ct.POINTER( tt.intimg_t ), \
        ct.POINTER( tt.box_feature_list_t ), \
        ct.POINTER( tt.img_coord_list_t ), \
        tt.mem_pf32_t ]

    box_eval_interface = box_eval_interface_tuple( tree_lib.evaluate_box_features )

    # -------------------------------------------------------------------------------------------
    
    tree_lib.calc_unormed_gini_index.restype = ct.c_int32
    tree_lib.calc_unormed_gini_index.argtypes = [ \
        ct.POINTER( tt.node_label_list_t ), \
        tt.mem_pi32_t ]

    tree_lib.calc_unormed_conditional_entropy.restype = ct.c_int32
    tree_lib.calc_unormed_conditional_entropy.argtypes = [ \
        ct.POINTER( tt.node_label_list_t ), \
        tt.mem_pf32_t ]
    
    global information_measure_interface
    information_measure_interface = information_measure_interface_tuple( 
                calc_unormed_gini_index = tree_lib.calc_unormed_gini_index,
                calc_unormed_conditional_entropy = tree_lib.calc_unormed_conditional_entropy )
    
    # -------------------------------------------------------------------------------------------

    global tree_split_optim_interface

    tree_lib.tree_create_split_optimizer.restype = ct.c_void_p
    tree_lib.tree_create_split_optimizer.argtypes = [ \
        ct.c_int32, \
        ct.c_int32 ]


    #tree_lib.tree_destruct_split_optimizer.restype = 
    tree_lib.tree_destruct_split_optimizer.argtypes = [ ct.c_void_p ]


    tree_lib.tree_initialize_parent_layer_statistic.restype = ct.c_int32
    tree_lib.tree_initialize_parent_layer_statistic.argtypes = [ \
        ct.c_void_p,
        ct.POINTER( tt.node_label_list_t ) ]


    tree_lib.tree_process_feature.restype = ct.c_int32
    tree_lib.tree_process_feature.argtypes = [ \
        ct.c_void_p,
        ct.POINTER( tt.node_label_data_list_t ) ]


    tree_lib.tree_get_optimal_splits.restype = ct.c_int32
    tree_lib.tree_get_optimal_splits.argtypes = [ \
        ct.c_void_p,
        ct.POINTER( tt.feature_split_list_t ) ]


    tree_split_optim_interface = tree_split_optim_interface_tuple( 
                create_split_optimizer=tree_lib.tree_create_split_optimizer,
                destruct_split_optimizer=tree_lib.tree_destruct_split_optimizer,
                initialize_parent_layer_statistic=tree_lib.tree_initialize_parent_layer_statistic,
                process_feature=tree_lib.tree_process_feature,
                get_optimal_splits=tree_lib.tree_get_optimal_splits )

    # -------------------------------------------------------------------------------------------
    
    global fern_split_optim_interface

    tree_lib.fern_create_split_optimizer.restype = ct.c_void_p
    tree_lib.fern_create_split_optimizer.argtypes = [ \
        ct.c_int32, \
        ct.c_int32 ]


    #tree_lib.fern_reset_optimal_split.restype = ct.c_int32
    tree_lib.fern_reset_optimal_split.argtypes = [ ct.c_void_p ]


    #tree_lib.tree_destruct_split_optimizer.restype = 
    tree_lib.fern_destruct_split_optimizer.argtypes = [ ct.c_void_p ]


    tree_lib.fern_initialize_parent_layer_statistic.restype = ct.c_int32
    tree_lib.fern_initialize_parent_layer_statistic.argtypes = [ \
        ct.c_void_p,
        ct.POINTER( tt.node_label_list_t ) ]


    tree_lib.fern_process_feature.restype = ct.c_int32
    tree_lib.fern_process_feature.argtypes = [ \
        ct.c_void_p,
        ct.POINTER( tt.node_label_data_list_t ) ]


    tree_lib.fern_get_optimal_split.restype = ct.c_int32
    tree_lib.fern_get_optimal_split.argtypes = [ \
        ct.c_void_p,
        ct.POINTER( tt.feature_split_t ) ]


    fern_split_optim_interface = fern_split_optim_interface_tuple( 
                create_split_optimizer=tree_lib.fern_create_split_optimizer,
                reset_optimal_split=tree_lib.fern_reset_optimal_split,
                destruct_split_optimizer=tree_lib.fern_destruct_split_optimizer,
                initialize_parent_layer_statistic=tree_lib.fern_initialize_parent_layer_statistic,
                process_feature=tree_lib.fern_process_feature,
                get_optimal_split=tree_lib.fern_get_optimal_split )

    # -------------------------------------------------------------------------------------------

    global fern_line_search_interface

    tree_lib.fern_line_search_on_entropy.restype = ct.c_int32
    tree_lib.fern_line_search_on_entropy.argtypes = [ \
        ct.POINTER( tt.sample_classification_data_t ),
        tt.mem_pf32_t,
        ct.POINTER( ttp.ParamPenalizer ),
        ct.POINTER( tt.line_search_result_t ) ]


    tree_lib.fern_line_search_on_gini.restype = ct.c_int32
    tree_lib.fern_line_search_on_gini.argtypes = tree_lib.fern_line_search_on_entropy.argtypes

    fern_line_search_interface = fern_line_search_interface_tuple(
                fern_line_search_on_entropy = tree_lib.fern_line_search_on_entropy,
                fern_line_search_on_gini = tree_lib.fern_line_search_on_gini )

    # -------------------------------------------------------------------------------------------

    # num simd vector items
    tree_lib.num_simd_vector_items.restype = ct.c_int32
    SIMD_VECTOR32_SIZE = tree_lib.num_simd_vector_items()
    
    # set vector size in other modules
    simd.set_default_simd_vector32_size( SIMD_VECTOR32_SIZE )
    assert( SIMD_VECTOR32_SIZE == simd.default_simd_vector32_size() )

    # -------------------------------------------------------------------------------------------
    
    global classification_interface

    tree_lib.classify_samples.restype = ct.c_int32
    tree_lib.classify_samples.argtypes = [ \
        ct.POINTER( tt.indexed_tree_layer_t ), \
        ct.POINTER( ttfr.FeatureRows ), \
        tt.mem_pi32_t, \
        tt.mem_pi32_t ]
    
    classification_interface = classification_interface_tuple( 
                classify_samples = tree_lib.classify_samples )
    
    # -------------------------------------------------------------------------------------------

    global lda_interface

    tree_lib.calculate_lda_covariances.restype = ct.c_int32
    tree_lib.calculate_lda_covariances.argtypes = [ \
        ct.POINTER( tt.node_label_list_t ), \
        ct.POINTER( ttfr.FeatureRows ), \
        ct.POINTER( tt.lda_stat_result_data_t ) ]
    
    lda_interface = lda_interface_tuple(
                calculate_lda_covariances = tree_lib.calculate_lda_covariances )

    # -------------------------------------------------------------------------------------------

    global sample_projection_interface

    #tree_lib.project_samples.restype = ct.c_int32
    tree_lib.project_samples.argtypes = [ \
        ct.POINTER( ttfr.FeatureRows ), \
        ct.c_int32,
        tt.mem_pf32_t,
        tt.mem_pf32_t,
        ct.c_int32 ]
    
    sample_projection_interface = sample_projection_interface_tuple(
                project_samples = tree_lib.project_samples )

    # -------------------------------------------------------------------------------------------

    global fern_binary_conversion_interface

    tree_lib.calc_entropy_based_binary_cl_weights.restype = ct.c_int32
    tree_lib.calc_entropy_based_binary_cl_weights.argtypes = [ \
        ct.POINTER( tt.node_label_list_t ), \
        ct.c_uint8, \
        ct.POINTER( tt.binary_mapping_result_data_t ) ]


    tree_lib.calc_gini_based_binary_cl_weights.restype = ct.c_int32
    tree_lib.calc_gini_based_binary_cl_weights.argtypes = [ \
        ct.POINTER( tt.node_label_list_t ), \
        ct.c_uint8, \
        ct.POINTER( tt.binary_mapping_result_data_t ) ]

    fern_binary_conversion_interface = fern_binary_conversion_tuple(
        calc_entropy_based_binary_cl_weights = tree_lib.calc_entropy_based_binary_cl_weights,
        calc_gini_based_binary_cl_weights    = tree_lib.calc_gini_based_binary_cl_weights )
    
# ----------------------------------------------------------------------------------------------


# Library Loading function
create_interface( load_fern_library() )

# ----------------------------------------------------------------------------------------------

