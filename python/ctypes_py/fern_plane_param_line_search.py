# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["lsearch_fern_param"]

# ----------------------------------------------------------------------------------------------

import typing as tp
import ctypes as ct
import numpy as np
from . import tree_types as tt
from . import tree_types_fern_weight_regularizer as ttp

from .tree_ctypes_library import fern_line_search_interface
from .tree_ctypes_return_codes import ReturnCode
from .tree_types_fern_weight_regularizer import EPenalizerCPPType

# ----------------------------------------------------------------------------------------------


def lsearch_fern_param( param_candidates : np.ndarray,
                                node_idx : np.ndarray,
                               label_idx : np.ndarray,
                            data_weights : np.ndarray,
                               num_nodes : tp.Optional[int] = None,
                              num_labels : tp.Optional[int] = None,
                               objective : str = "gini",
                               penalizer : tp.Optional[ttp.ParamPenalizer] = None ):
    """
    Parameters
    ----------
    param_candidates : float32 ndarray [ N ]
        
    node_idx         : int32 ndarray [ N ]
    label_idx        : int32 ndarray [ N ]
    data_weights     : int32 ndarray [ N ]
    num_nodes        : int or None
    num_labels       : int or None
    objective        : str, "gini" or "entropy"
    penalizer        : instance of ParamPenalizer or None
    
    Returns as struct with members:
    -------------------------------
    int32_t     featureIdx;
    float32_t   featureSplit;

    union
    {
        int32_t     nodeGiniRating;
        float32_t   nodeEntropy;
    }
    """
    node_label_list = tt.node_label_list_t.from_numpy( node_idx = node_idx,
                                                      label_idx = label_idx, 
                                                   data_weights = data_weights,
                                                      num_nodes = num_nodes,
                                                     num_labels = num_labels )
    
    param_candidates = np.require( param_candidates, np.float32, [ 'C_CONTIGUOUS' ] )
    assert param_candidates.size == node_label_list.numData

    # create penalizer structure
    if penalizer is None:
        penalizer = ttp.ParamPenalizer( penalizerType = EPenalizerCPPType.NO_PENALIZER )
    assert isinstance( penalizer, ttp.ParamPenalizer )

    # data structure containing results
    res = tt.line_search_result_t()

    # actual call:
    if objective.lower() == "gini":
        expected_optim_retval = ReturnCode.GINI_RATING
        ret_val = fern_line_search_interface.fern_line_search_on_gini( ct.byref( node_label_list ),
                                                                       param_candidates,
                                                                       ct.byref( penalizer ),
                                                                       ct.byref( res ) )
    else:
        assert objective.lower() == "entropy"
        expected_optim_retval = ReturnCode.ENTROPY_RATING
        ret_val = fern_line_search_interface.fern_line_search_on_entropy( ct.byref( node_label_list ),
                                                                          param_candidates,
                                                                          ct.byref( penalizer ),
                                                                          ct.byref( res ) )

    # evaluate return value
    ret_val = ReturnCode.from_int_return( ret_val )
    ret_val.raise_on_error()

    assert ret_val.has_valid_rating_info
    assert bool(expected_optim_retval & ret_val), "Wrong optimization method used"

    return res

