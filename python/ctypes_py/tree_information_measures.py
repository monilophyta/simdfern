# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["calc_unormed_gini_index", "calc_unormed_conditional_entropy"]

# ----------------------------------------------------------------------------------------------

import ctypes as ct
import typing as tp
import numpy as np

from . import tree_types as tt
from .tree_ctypes_library import information_measure_interface as imi
from .tree_ctypes_return_codes import ReturnCode

# ----------------------------------------------------------------------------------------------


def calc_unormed_gini_index( 
        leaf_idx       : np.ndarray, 
        label_idx      : np.ndarray, 
        data_weight    : np.ndarray,
        num_leafs      : int,
        num_labels     : tp.Optional[int] = None ):
    
    assert isinstance( leaf_idx, np.ndarray )
    assert isinstance( label_idx, np.ndarray )
    assert isinstance( data_weight, np.ndarray )
    
    assert np.issubdtype( leaf_idx.dtype, np.int32 )
    assert np.issubdtype( label_idx.dtype, np.int32 )
    assert np.issubdtype( data_weight.dtype, np.int32 )
    
    assert leaf_idx.size == label_idx.size
    assert leaf_idx.size == data_weight.size

    #num_data = int( leaf_idx.size )
    num_leafs = int( num_leafs )

    if num_labels is None:
        num_labels = label_idx.max() + 1
    
    num_labels = int( num_labels )

    in_data = tt.node_label_list_t.from_numpy( node_idx = leaf_idx,
                                              label_idx = label_idx,
                                           data_weights = data_weight,
                                             num_labels = num_labels,
                                              num_nodes = num_leafs )
    unormed_gini = np.empty( num_leafs, dtype=np.int32 )
    
    if __debug__:
        unormed_gini.fill( np.iinfo(unormed_gini.dtype).max )

    ret_code = imi.calc_unormed_gini_index( \
            ct.byref( in_data ),  \
            unormed_gini )
    
    ret_code = ReturnCode.from_int_return( ret_code )
    ret_code.raise_on_error()

    return unormed_gini



def calc_unormed_conditional_entropy( 
        leaf_idx       : np.ndarray, 
        label_idx      : np.ndarray, 
        data_weight    : np.ndarray,
        num_leafs      : int,
        num_labels     : tp.Optional[int] = None ):
    
    assert isinstance( leaf_idx, np.ndarray )
    assert isinstance( label_idx, np.ndarray )
    assert isinstance( data_weight, np.ndarray )
    
    assert np.issubdtype( leaf_idx.dtype, np.int32 )
    assert np.issubdtype( label_idx.dtype, np.int32 )
    assert np.issubdtype( data_weight.dtype, np.int32 )
    
    assert leaf_idx.size == label_idx.size
    assert leaf_idx.size == data_weight.size

    #num_data = int( leaf_idx.size )
    num_leafs = int( num_leafs )

    if num_labels is None:
        num_labels = label_idx.max() + 1
    
    num_labels = int( num_labels )

    in_data = tt.node_label_list_t.from_numpy( node_idx = leaf_idx,
                                              label_idx = label_idx,
                                           data_weights = data_weight,
                                             num_labels = num_labels,
                                              num_nodes = num_leafs )
    unormed_conditional_entropy = np.empty( num_leafs, dtype=np.float32 )

    if __debug__:
        unormed_conditional_entropy.fill( np.nan )

    ret_code = imi.calc_unormed_conditional_entropy( \
            ct.byref( in_data ),  \
            unormed_conditional_entropy )
    
    ret_code = ReturnCode.from_int_return( ret_code )
    ret_code.raise_on_error()

    assert np.all( np.isfinite( unormed_conditional_entropy ) )

    return unormed_conditional_entropy
