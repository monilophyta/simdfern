# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = []

import logging
import atexit
from .tree_ctypes_library import get_cpp_event_counts


# create logger
logger = logging.getLogger('cpp.counts')

if __debug__:
    logger.setLevel(logging.INFO)
else:
    logger.setLevel(logging.WARN)

def dump_count_stats():
    stats = get_cpp_event_counts()
    for field_name, field_type in stats._fields_:
        logger.info("%s=%d", field_name, getattr(stats, field_name))

atexit.register(dump_count_stats)
