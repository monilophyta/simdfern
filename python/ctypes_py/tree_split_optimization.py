# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



# ----------------------------------------------------------------------------------------------

import typing as tp
import ctypes as ct
import numpy as np

from ..simd import aligned_empty

from . import tree_types as tt
from .tree_ctypes_library import tree_split_optim_interface
from .tree_classification_layer import IndexedTreeLayer
from .tree_base_split_optimization import BaseSplitOptimizer
from .tree_ctypes_return_codes import ReturnCode

# ----------------------------------------------------------------------------------------------


class TreeSplitOptimizer( BaseSplitOptimizer ):


    def __init__( self, node_idx : np.ndarray,
                       label_idx : np.ndarray,
                    data_weights : np.ndarray, 
                       num_nodes : tp.Optional[int] = None,
                      num_labels : tp.Optional[int] = None ):
        
        super().__init__( tree_split_optim_interface, \
                          node_idx, \
                          label_idx, \
                          data_weights, \
                          num_nodes, \
                          num_labels )


    def __del__(self):
        self.destruct( tree_split_optim_interface )
    

    def initialize_parent_layer_statistic( self ):
        return super().initialize_parent_layer_statistic( tree_split_optim_interface )


    def process_feature( self, feature_vals ):
        return super().process_feature( tree_split_optim_interface, feature_vals )


    def get_optimal_splits(self, do_align : bool = False ):

        if self.optimization_method.has_gini_rating_flag:
            rating_dtype = np.int32
        else:
            assert self.optimization_method.has_entropy_rating_flag
            rating_dtype = np.float32

        do_align = bool(do_align)
        if do_align is True:
            ratings       = aligned_empty( self.num_nodes, dtype=rating_dtype, do_extend=False )
            featureIdx    = aligned_empty( self.num_nodes, dtype=np.int32, do_extend=False )
            featureSplits = aligned_empty( self.num_nodes, dtype=np.float32, do_extend=False )
        else:
            ratings       = np.empty( self.num_nodes, dtype=rating_dtype )
            featureIdx    = np.empty( self.num_nodes, dtype=np.int32 )
            featureSplits = np.empty( self.num_nodes, dtype=np.float32 )

        if __debug__:
            if np.issubdtype( ratings.dtype, np.integer ):
                ratings.fill( np.iinfo(ratings.dtype).max )
            else:
                ratings.fill( np.nan )
            featureIdx.fill( np.iinfo(featureIdx.dtype).max )
            featureSplits.fill( np.nan )


        assert ratings.flags["C_CONTIGUOUS"]
        assert featureIdx.flags["C_CONTIGUOUS"]
        assert featureSplits.flags["C_CONTIGUOUS"]
        
        param = tt.feature_split_list_t.from_numpy( ratings=ratings, 
                                                    featureIdx=featureIdx,
                                                    featureSplits=featureSplits )
        
        ret_val = tree_split_optim_interface.get_optimal_splits( self.split_optimizer_handle, ct.byref( param ) )
        
        ret_val = ReturnCode.from_int_return( ret_val )
        ret_val.raise_on_error()
 
        assert ret_val.has_valid_rating_info
        assert bool( self.optimization_method & ret_val )
        assert np.issubdtype( ratings.dtype, np.integer ) == ret_val.has_gini_rating_flag
        assert np.issubdtype( ratings.dtype, np.floating ) == ret_val.has_entropy_rating_flag
        assert np.all( np.isfinite( param.FeatureSplits[param.FeatureIdx >= 0] ) )

        return ( IndexedTreeLayer( param.FeatureIdx, param.FeatureSplits ), param.Ratings )

# ----------------------------------------------------------------------------------------------


