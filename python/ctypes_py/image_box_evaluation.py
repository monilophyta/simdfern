# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


import ctypes as ct
import numpy as np

from ..simd import as_aligned_array, aligned_empty, is_array_fully_aligned, default_simd_vector32_size
from ..image import ImageBoxes, ImageCoords

from . import tree_types as tt
from .tree_ctypes_library import box_eval_interface as bei
from .tree_ctypes_return_codes import ReturnCode


__all__ = [ "BoxFeatureEvaluator", "BoxEvaluator" ]



class BoxFeatureEvaluator( object ):
    """!@brief Evaluates box features within an image

    TODO: longer write larger description

    @param do_align Enforce memory alignment also by memory extension, if necessary.
    """

    box_features = None
    box_ratio_feature = True
    
    do_align = False

    # number of boxes without end alignment extension
    num_boxes     = 0

    # number of boxes including end alignment extension
    num_ext_boxes = 0


    def __init__(self, box1, box2, boxlambda = None, do_align=False ):
        
        assert( isinstance( box1, ImageBoxes ) )
        assert( isinstance( box2, ImageBoxes ) )
        assert( box1.size == box2.size )

        if default_simd_vector32_size() == box1.size:
            # Alignment with minor extra costs possible
            do_align = True


        self.num_boxes = self.num_ext_boxes = box1.size

        if boxlambda is not None:
            boxlambda = np.require( boxlambda, np.float32 )
            assert box1.size == boxlambda.size

        if do_align == True:
            box1 = box1.aligned
            box2 = box2.aligned
            self.num_ext_boxes = box1.size
            
            if boxlambda is not None:
                boxlambda = as_aligned_array( boxlambda, do_extend=do_align, overlap_value=0 )

                assert( box1.size == boxlambda.size )
                assert( is_array_fully_aligned( boxlambda ) )  # should have been tested already

        self.box_features = tt.box_feature_list_t.from_numpy( box1, box2, boxlambda )
        self.box_ratio_feature = (boxlambda is None)
        self.do_align  = do_align



    def process( self, intimg : np.ndarray, img_coords : ImageCoords ):
        
        assert( isinstance( img_coords, ImageCoords ) )
        N = img_coords.size

        intimg     = tt.intimg_t.from_numpy( intimg )
        img_coords = tt.img_coord_list_t.from_numpy( img_coords )


        if bool(self.do_align) is True:
            feature_val = aligned_empty( N*self.num_ext_boxes, dtype=np.float32 )
        else:
            feature_val = np.empty( N*self.num_ext_boxes, dtype=np.float32 )

        if __debug__:
            feature_val.fill(np.inf)

        assert bool(feature_val.flags["C_CONTIGUOUS"]) is True

        ret_val = bei.evaluate_box_features( ct.byref(intimg), ct.byref( self.box_features ), ct.byref( img_coords ), 
                                             feature_val )
        
        ret_val = ReturnCode.from_int_return( ret_val )
        ret_val.raise_on_error()
        assert self.check_return_value( ret_val, intimg.integral_image )
        
        # reconstruct output shape + discard extensions
        feature_val = feature_val.reshape( ( N, self.num_ext_boxes ) )
        
        # discard extensions
        if self.num_ext_boxes > self.num_boxes:
            feature_val = feature_val[:,:self.num_boxes]
        
        assert not np.any( np.isinf( feature_val ) )

        return feature_val



    def check_return_value( self, ret_val : ReturnCode, integral_image : np.ndarray ):
        assert ret_val.has_valid_integral_image_processing_info

        assert ret_val.has_float_integral_image_flag == bool(np.issubdtype( integral_image.dtype, np.floating ))
        assert ret_val.has_integer_integral_image_flag == bool(np.issubdtype( integral_image.dtype, np.integer ))

        assert ret_val.has_valid_box_feature_info
        assert ret_val.has_box_ratio_feature_flag == bool(self.box_ratio_feature)

        assert ret_val.has_valid_io_alignment_info
        assert ret_val.has_aligned_io_flag or (bool(self.do_align) is False)  # avx (8*32 bit) alignment is not garantied

        return True

    @property
    def box_union(self):
        return self.box_features.First.Boxes.union.union( self.box_features.Second.Boxes.union )


# ----------------------------------------------------------------------------------------------------------

class BoxEvaluator( BoxFeatureEvaluator ):

    def __init__(self, boxes, scale = None ):
        assert( isinstance( boxes, ImageBoxes ) )

        zero = np.zeros( boxes.size, dtype=np.int32 )
        #one  = np.ones( boxes.size, dtype=np.int32 )
        
        dummy_boxes = ImageBoxes( up = zero, left = zero, down = zero, right = zero )
        assert np.all(dummy_boxes.area == 0)

        dummy_lambda = np.zeros( boxes.size, dtype=np.float32 )

        if scale is not None:
            Lambda = dummy_lambda - scale
            super().__init__( box1=dummy_boxes, box2=boxes, boxlambda=Lambda, do_align=False )
        else:
            super().__init__( box1=boxes, box2=dummy_boxes, boxlambda=dummy_lambda, do_align=False )
    

    @property
    def box_union(self):
        return self.box_features.First.Boxes.union


