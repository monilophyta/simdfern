# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["ReturnCode", "ctypesError"]


from enum import IntFlag



class ctypesError(Exception):
    """Exception indicating an error while ctypes (C/C++) code execution"""

    message = None
    error_code = None

    def __init__(self, message, error_code):
        super(ctypesError,self).__init__()
        self.error_code = error_code
        self.message = message

    def __str__(self): 
        return repr(self.message)



class ReturnCode( IntFlag ):

    @classmethod
    def from_int_return(cls, ret_val):
        ret_val = int(ret_val)
        assert ret_val <= 0, "Return values have to be negative, got %d" %  ret_val
        ret_val = -ret_val
        assert ((ret_val & (~cls.CODE_MASK())) & (~cls.ERROR_MASK()) ) == 0, "Wrong return code flags in %d" %  ret_val
        return cls( ret_val )


    # Code handling
    # ----------------------------------------------------------

    ENTROPY_RATING          =  1 <<  0
    GINI_RATING             =  1 <<  1

    INT32_INT_IMG           =  1 <<  2
    FLOAT32_INT_IMG         =  1 <<  3

    BOX_DIFF_FEATURE        =  1 <<  4
    BOX_RATIO_FEATURE       =  1 <<  5

    NON_ALIGNED_IN_OUT      =  1 <<  6
    ALIGNED_IN_OUT          =  1 <<  7

    PROCESSED_ALIGNED       =  1 <<  8
    PROCESSED_NONALIGNED    =  1 <<  9

    NONMATCHING_ARRAY_SIZES =  1 << 10
    NONALIGNED_INPUT        =  1 << 11
    NONALIGNED_ARRAY_SIZE   =  1 << 12


    @property
    def has_entropy_rating_flag(self):
        return bool(self & self.ENTROPY_RATING)
    
    @property
    def has_gini_rating_flag(self):
        return bool(self & self.GINI_RATING)

    @property
    def has_valid_rating_info(self):
        return (self.has_entropy_rating_flag != self.has_gini_rating_flag)


    @property
    def has_float_integral_image_flag(self):
        return bool(self & self.FLOAT32_INT_IMG)

    @property
    def has_integer_integral_image_flag(self):
        return bool(self & self.INT32_INT_IMG)
    
    @property
    def has_valid_integral_image_processing_info(self):
        return (self.has_float_integral_image_flag != self.has_integer_integral_image_flag)


    @property
    def has_box_diff_feature_flag(self):
        return bool(self & self.BOX_DIFF_FEATURE)
    
    @property
    def has_box_ratio_feature_flag(self):
        return bool(self & self.BOX_RATIO_FEATURE)

    @property
    def has_valid_box_feature_info(self):
        return (self.has_box_diff_feature_flag != self.has_box_ratio_feature_flag)

    
    @property
    def has_aligned_io_flag(self):
        return bool(self & self.ALIGNED_IN_OUT)
    
    @property
    def has_non_aligned_io_flag(self):
        return bool(self & self.NON_ALIGNED_IN_OUT)

    @property
    def has_valid_io_alignment_info(self):
        return (self.has_aligned_io_flag != self.has_non_aligned_io_flag)
    

    @property
    def has_process_aligned_flag(self):
        return bool(self & self.PROCESSED_ALIGNED)
    
    @property
    def has_process_nonaligned_flag(self):
        return bool(self & self.PROCESSED_NONALIGNED)
    
    @property
    def has_valid_processing_info(self):
        return (self.has_process_aligned_flag != self.has_process_nonaligned_flag)


    # Error Handling
    # ----------------------------------------------------------

    NOERROR                 =  0
    
    INVALID_OUTPUT_SIZE     =  1 << 15
    INVALID_POINTERS        =  1 << 16
    UNKNOWN_ERROR           =  1 << 17

    INVALID_INPUT_SIZE      =  1 << 18
    INVALID_INPUT           =  1 << 19


    @staticmethod
    def CODE_MASK():
        return ((1 << 13) - 1)

    @staticmethod
    def ERROR_MASK():
        return ((1 << 20) - 1 - ((1 << 15) - 1))


    def raise_on_error( self ):

        if (self & self.ERROR_MASK()) == 0:
            return True
        
        error_message = self.assemble_error_message()
        raise ctypesError( error_message, self.value )
        

    def assert_on_error( self ):

        if (self & self.ERROR_MASK()) == 0:
            return True
        
        error_message = self.assemble_error_message()
        assert False, error_message
        return False
    

    def assemble_error_message( self ):
        err_val = self.value & self.ERROR_MASK()
        msg = ", ".join( err.name for err in ReturnCode if (err & err_val) )
        msg = "Errors occured in ctypes code execution: %s" % msg
        return msg



