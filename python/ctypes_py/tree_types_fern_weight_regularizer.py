# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["ParamPenalizer", "EPenalizerType" ]


import ctypes as ct
from enum import IntEnum, Enum
import typing as tp
import math
import numpy as np

from ..simd import as_aligned_array
from .tree_types import mem_pf32_t


# -------------------------------------------------------------------------

class EPenalizerCPPType( IntEnum ):
    NO_PENALIZER                 = 0
    L1L2_PENALIZER               = 1
    L1L2_VECT_PENALIZER          = 2
    NORMALIZED_L1_PENALIZER      = 3
    NORMALIZED_L1_VECT_PENALIZER = 4

# -------------------------------------------------------------------------


class Penalizer(ct.Structure):
    """
    float32_t m_regulStrength_f32;
    float32_t m_l1Ratio_f32;
    """
    _fields_=[ ( "regulStrength",   ct.c_float ), \
               ( "l1Ratio",         ct.c_float )  ]



class BaseVectPenalizer(ct.Structure):
    """
    float32_t m_regulStrength_f32;

    const MT* m_offset_pf32;
    const MT* m_offsetEnd_pf32;
    const MT* m_scale_pf32;
    """
    _fields_=[ ( "regulStrength",   ct.c_float ), \
               ( "offsetPtr" ,      mem_pf32_t ), \
               ( "offsetEnd",       ct.POINTER( ct.c_float ) ), \
               ( "scalePtr",        mem_pf32_t ) ]


class VectPenalizer(ct.Structure):
    """
    CBaseVectPenalizer<MT> base;
    float32_t m_l1Ratio_f32;
    """
    _anonymous_ = ("base",)
    _fields_=[ ( "base",     BaseVectPenalizer ), \
               ( "l1Ratio" , ct.c_float ) ]


class NormalizedL1Penalizer(ct.Structure):
    """
    float32_t m_regulStrength_f32;
    float32_t m_l1Offset_f32;
    float32_t m_sqrL2Offset_f32;
    """
    _fields_=[ ( "regulStrength", ct.c_float ), \
               ( "l1Offset",      ct.c_float ), \
               ( "sqrL2Offset",   ct.c_float ) ]
    

class NormalizedL1VectPenalizer(BaseVectPenalizer):
    pass

# -------------------------------------------------------------------------

class ParamPenalizerTypeUnion( ct.Union ):
    _fields_ = [("l1l2Penalizer",             Penalizer ),\
                ("l1l2VectPenalizer",         VectPenalizer),\
                ("normalizedL1Penalizer",     NormalizedL1Penalizer),\
                ("normalizedL1VectPenalizer", NormalizedL1VectPenalizer) ]

# -------------------------------------------------------------------------


class EPenalizerType( Enum ):
    NO_REGULARIZATION  = None
    L1L2               = "L1L2"
    NORMALIZED_L1      = "NORMALIZED_L1"
    CENTRALIZED_L1     = "CENTRALIZED_L1"




class ParamPenalizer(ct.Structure):
    _anonymous_ = ("types",)
    _fields_ = [ ( "penalizerType", ct.c_int ), \
                 ( "types",         ParamPenalizerTypeUnion) ]

    scale  : tp.Optional[np.ndarray] = None
    offset : tp.Optional[np.ndarray] = None


    @classmethod
    def create_single_regularization( cls, 
                               regul_type : EPenalizerType,
                           regul_strength : float,
                             num_features : int = 0,
                                 l1offset : float = 0.0,
                              sqrl2Offset : float = 0.0,
                                  l1ratio : float = 1.0 ):
        """
        Parameters
        ----------
        regul_type : EPenalizerType
            indication which penalizer should be used
        regulation_strength : float
        num_features : int
            only relevant for CENTRALIZED_L1
        l1offset : float
             only relevant for NORMALIZED_L1, CENTRALIZED_L1
        sqrl2Offset : float
             only relevant for NORMALIZED_L1, CENTRALIZED_L1
        l1ratio : float in range [0..1]
        do_align : bool (default False)
            if true: try to align the offset and scale arrays

        Regularization Types
        --------------------

        NO_REGULARIZATION
            disables regularization
        L1L2
            \text{regulation_strength} * ( \text{l1ratio} * \left| x \right|_1 + 
                                       (1-\text{l1ratio}) * \left| x \right|_2 )
        NORMALIZED_L1
            (\text{regulation_strength} * \text{l1ratio}) * 
                    \frac{ \left| x \right|_1 + \text{l1offset} }
                         { \sqrt{ x*x + sqrl2Offset } }
        CENTRALIZED_L1
            \frac{ \text{regulation_strength} * \text{l1ratio} }{ \sqrt{num_features} }
                    \frac{ \left| x \right|_1 + \text{l1offset} }
                         { \sqrt{ x*x + sqrl2Offset } }
        """
        assert isinstance( regul_type, EPenalizerType )
        if not isinstance( regul_type, EPenalizerType ):
            regul_type = EPenalizerType( regul_type )

        regul_strength = float(regul_strength)
        assert regul_strength >= 0
        if regul_strength == 0:
            regul_type = EPenalizerType.NO_REGULARIZATION
        
        if regul_type is EPenalizerType.NO_REGULARIZATION:
            return cls( penalizerType = EPenalizerCPPType.NO_PENALIZER )
        
        # check which penalizer is requested
        if regul_type is EPenalizerType.L1L2:
            penalizer = Penalizer( regulStrength = regul_strength, 
                                             l1Ratio = float(l1ratio) )
            ret = cls( penalizerType = EPenalizerCPPType.L1L2_PENALIZER, 
                       l1l2Penalizer = penalizer )

        else:
            if regul_type is EPenalizerType.NORMALIZED_L1:
                regul_strength = regul_strength * float(l1ratio)
            elif regul_type is EPenalizerType.CENTRALIZED_L1:
                regul_strength = regul_strength * float(l1ratio) / math.sqrt(num_features)
            else:
                assert False, "Invalid regularization"

            penalizer = NormalizedL1Penalizer( regulStrength = regul_strength, 
                                                    l1Offset = float(l1offset),
                                                 sqrL2Offset = float(sqrl2Offset) )
            ret = cls( penalizerType = EPenalizerCPPType.NORMALIZED_L1_PENALIZER, 
                       normalizedL1Penalizer = penalizer )

        return ret



    @classmethod
    def create_vector_regularization( cls, 
                               regul_type : EPenalizerType,
                           regul_strength : float,
                                   offset : np.ndarray,
                                    scale : np.ndarray,
                                  l1ratio : float = 1.0,
                                 do_align : bool = False ):
        """
        Parameters
        ----------
        regul_type : EPenalizerType
            indication which penalizer should be used
        regulation_strength : float
        offset : float32 ndarray [ D ]
        scale : float32 ndarray [ D ]
        l1ratio : float in range [0..1]
        do_align : bool (default False)
            if true: try to align the offset and scale arrays

        Regularization Types
        --------------------

        NO_REGULARIZATION
            disables regularization
        L1L2
            \text{regulation_strength} * ( \text{l1ratio} * \left| \text{scale} * x + \text{offset} \right|_1 + 
                                       (1-\text{l1ratio}) * \left| \text{scale} * x + \text{offset} \right|_2 )
        NORMALIZED_L1
            (\text{regulation_strength} * \text{l1ratio}) * 
                    \frac{\left| \text{scale} * x + \text{offset} \right|_1}
                         {\left| \text{scale} * x + \text{offset} \right|_2}
        CENTRALIZED_L1
            \frac{\text{regulation_strength} * \text{l1ratio}} { \sqrt{len(offset)} }
                    \frac{\left| \text{scale} * x + \text{offset} \right|_1}
                         {\left| \text{scale} * x + \text{offset} \right|_2}
        """
        assert isinstance( regul_type, EPenalizerType )
        if not isinstance( regul_type, EPenalizerType ):
            regul_type = EPenalizerType( regul_type )

        regul_strength = float(regul_strength)
        assert regul_strength >= 0
        if regul_strength == 0:
            regul_type = EPenalizerType.NO_REGULARIZATION
        
        if regul_type is EPenalizerType.NO_REGULARIZATION:
            return cls( penalizerType = EPenalizerCPPType.NO_PENALIZER )
        
        # bring vector parts in correct shape
        if bool(do_align):
            offset = np.asarray( offset, dtype=np.float32 )
            scale = np.asarray( scale, dtype=np.float32 )
            assert offset.size == scale.size
            num_features = offset.size

            offset = as_aligned_array( offset, do_extend=True, overlap_value=0 )
            scale = as_aligned_array( scale, do_extend=True, overlap_value=0 )
        else:
            offset = np.require( offset, dtype=np.float32, requirements="C" )
            scale  = np.require( scale, dtype=np.float32, requirements="C" )
            num_features = offset.size
        
        assert offset.size == scale.size

        # create pointers
        offset_ptr = offset.ctypes.data_as( mem_pf32_t )
        param_dict = dict( scalePtr  = scale.ctypes.data_as( mem_pf32_t ),
                           offsetPtr = offset_ptr,
                           offsetEnd = ct.cast( ct.c_void_p( offset_ptr.value + offset.nbytes ), ct.POINTER(ct.c_float) ) )
        
        # check which penalizer is requested
        if regul_type is EPenalizerType.L1L2:
            penalizer = VectPenalizer( regulStrength = regul_strength, 
                                             l1Ratio = float(l1ratio),
                                        **param_dict )
            ret = cls( penalizerType = EPenalizerCPPType.L1L2_VECT_PENALIZER, 
                       l1l2VectPenalizer = penalizer )

        else:
            if regul_type is EPenalizerType.NORMALIZED_L1:
                regul_strength = regul_strength * float(l1ratio)
            elif regul_type is EPenalizerType.CENTRALIZED_L1:
                regul_strength = regul_strength * float(l1ratio) / math.sqrt(num_features)
            else:
                assert False, "Invalid regularization"

            penalizer = NormalizedL1VectPenalizer( regulStrength = regul_strength, 
                                                   **param_dict )
            ret = cls( penalizerType = EPenalizerCPPType.NORMALIZED_L1_VECT_PENALIZER, 
                       normalizedL1VectPenalizer = penalizer )

        # preserve offset and scale
        ret.scale = scale
        ret.offset = offset

        return ret



