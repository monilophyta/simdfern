# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = [ "calculate_multi_node_lda_covars" ]

import ctypes as ct
#from collections import namedtuple
import numpy as np
#from enum import IntFlag
#from ..simd import is_array_start_aligned, is_array_fully_aligned, aligned_empty_like

from . import tree_types as tt
from .tree_ctypes_library import lda_interface as lda
from .tree_types_feature_rows import FeatureRows



def calculate_multi_node_lda_covars( node_idx, label_idx, weights, feature_rows ):
    """
    Parameters
    ----------
    node_idx : int32 array [ N ]
        node_idx[n] is node index of data sample n,
        >=0 for active nodes, <0 for finalized nodes
    label_idx : int32 array [ N ]
        label_idx[n] is label index of data sample n
    weights : int32 array [ N ]
        weights[n] is weight of data sample n
    feature_rows : float matrix [ F x N ] or f * [ array(N) ]
        features of samples: feature_rows[:,n] f * [ array(N)[n] ]
    
    Returns
    -------
    used_weight_sum : sum of used weights in statistic
    intra_covar : float32 matrix [ F x F ]
    inter_covar : float32 matrix [ F x F ]
    """

    assert isinstance( feature_rows, FeatureRows )

    if __debug__:
        valid_rows = [ np.all( np.isfinite(f) ) for f in feature_rows ]
        assert np.all( valid_rows ), "non-finite values are not supported by lda (yet)"
        assert feature_rows.run_paranoid_ctypes_verifications()

    input_struct = tt.node_label_list_t.from_numpy( node_idx = node_idx,
                                                   label_idx = label_idx,
                                                data_weights = weights )
    output_struct = tt.lda_stat_result_data_t.from_numpy( feature_rows.num_features )

    # ctypes call
    used_weight_sum = lda.calculate_lda_covariances( ct.byref( input_struct ),
                                                     ct.byref( feature_rows ),
                                                     ct.byref( output_struct ) )

    assert used_weight_sum >= 0
    assert output_struct.numFeatures == feature_rows.num_features, "inconsitency in number of features"
    
    intra_covar = output_struct.Intra_Covar
    inter_covar = output_struct.Inter_Covar

    assert np.all( np.isfinite( inter_covar ) )
    assert np.all( np.isfinite( intra_covar ) )
    assert np.all( inter_covar == inter_covar.transpose() )   # symmetrie_check
    assert np.all( intra_covar == intra_covar.transpose() )  # symmetrie check

    return (used_weight_sum, intra_covar, inter_covar )
