# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



from abc import ABC, abstractmethod
from collections import namedtuple, Sequence
#from enum import IntFlag
import ctypes as ct
import numpy as np

from ..simd import is_array_fully_aligned, aligned_empty_like

from . import tree_types as tt
from .tree_ctypes_library import classification_interface as cli
from .tree_ctypes_return_codes import ReturnCode
from .tree_types_feature_rows import FeatureRows, create_feature_rows


__all__ = [ "AbtractLayerBase", "IndexedTreeLayer" ]


# https://numpydoc.readthedocs.io/en/latest/format.html


#--------------------------------------------------------------------------------------------------------

class AbtractLayerBase( ABC ):

    @property
    @abstractmethod
    def max_feature_idx(self):
        return -1

    @abstractmethod
    def classify( self, node_idx, feature_rows ):
        return None


#--------------------------------------------------------------------------------------------------------

IndexedTreeLayerBase = namedtuple('IndexedTreeLayerBase', [ 'feature_idx', 'feature_split' ] )

#--------------------------------------------------------------------------------------------------------


class IndexedTreeLayer( IndexedTreeLayerBase, AbtractLayerBase ):
    

    #def __new__(typ, feature_idx, feature_split ):
    #    obj = IndexedTreeLayerBase.__new__( typ, feature_idx, feature_split )
    #    return obj


    def __init__(self, *args, **argv):
        """
        Parameters
        ----------
        feature_idx : float array [ N ]
            Index of used feature in node n
        feature_split : float array [ N ]
            Value the feature[n] is compared with
        """
        #super().__init__( *args, **argv )
        assert( len(self.feature_idx) == len(self.feature_split) )
        assert( np.all( self.feature_idx > -3 ) ) # including error codes



    def remap_feature_indices(self, idx_map=None ):

        if idx_map is None:
            idx_map = np.arange( -3 - self.feature_idx.max(), 0, dtype=self.feature_idx.dtype )
            
            valid_indices = self.unique_feature_indices
            idx_map[ valid_indices ] = np.arange( valid_indices.size, dtype=self.feature_idx.dtype )
        else:
            assert( idx_map.dtype == self.feature_idx.dtype )

            last_idx = np.array( [-2,-1], dtype=idx_map.dtype )

            if not np.all( idx_map[-2:] == last_idx ):
                idx_map = np.concatenate( (idx_map, last_idx), axis=0 )
        
        
        new_feature_idx = idx_map.take( self.feature_idx )
        assert( new_feature_idx.min() >= -2 )

        return IndexedTreeLayer( new_feature_idx, self.feature_split )


    @property
    def num_branching_nodes(self):
        return self.feature_idx.size

    @property
    def valid_node_mask(self):
        return (self.feature_idx >= 0)

    @property
    def valid_feature_indices(self):
        return self.feature_idx[self.valid_node_mask]

    @property
    def valid_feature_splits(self):
        return self.feature_split[self.valid_node_mask]


    @property
    def unique_feature_indices(self):
        return np.unique( self.valid_feature_indices )   # returns sorted set of array

    @property
    def num_features(self):
        return self.unique_feature_indices.size

    @property
    def max_feature_idx(self):
        return self.feature_idx.max()

    @property
    def ctypes_structure(self):
        return tt.indexed_tree_layer_t.from_numpy( self.feature_idx, self.feature_split )


    def classify( self, node_idx : np.ndarray, feature_rows : Sequence ):
        """
        Parameters
        ----------
        node_idx : int32 array [ N ]
            Node index of sample n,
            >=0 for active nodes, <0 for finalized nodes
        feature_rows : float matrix [ nF x N ] or nf * [ array(N) ]
            features of samples: feature_rows[:,n] nf * [ array(N)[n] ]
        
        Returns
        -------
        int32 array [ N ]
            Propagated node indices.
            >=0 if "input node_idx >= 0" and addressed node is still active,
            <0 otherwise
        """

        assert isinstance( node_idx, np.ndarray )
        assert np.issubdtype( node_idx.dtype, np.int32 )
        assert isinstance( feature_rows, Sequence )

        # convert sequrence to FeatureRows if required
        if not isinstance( feature_rows, FeatureRows ):
            n_feature_rows = create_feature_rows()
            n_feature_rows.extend( feature_rows )
            feature_rows = n_feature_rows

        assert isinstance( feature_rows, FeatureRows )
        assert node_idx.size == feature_rows.num_data
        assert self.num_features <= feature_rows.num_features
        assert self.max_feature_idx < feature_rows.num_features

        # bring input in shape
        node_idx = np.require( node_idx, np.int32, ["C_CONTIGUOUS"] )

        # Output data structure
        if is_array_fully_aligned( node_idx ):
            res_node_idx = aligned_empty_like( node_idx )
        else:
            res_node_idx = np.empty_like( node_idx )
        
        if __debug__:
            res_node_idx.fill( np.iinfo(res_node_idx.dtype).max )
        
        # C library function call
        ctypes_structure = self.ctypes_structure
        ret_code = cli.classify_samples( ct.byref( ctypes_structure ),
                                         ct.byref( feature_rows ),
                                         node_idx,
                                         res_node_idx )

        # Evaluate return codes
        assert ret_code == 0

        return res_node_idx


