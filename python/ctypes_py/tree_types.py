# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



import typing as tp
import math
import numpy as np
import ctypes as ct
from ..image import ImageBoxes, ImageCoords
from ..simd import as_aligned_array, is_array_start_aligned


__all__ = [ "EventStat", "mem_pi32_t", "mem_pf32_t", \
            "intimg_t", "box_list_t", "box_feature_list_t", "img_coord_list_t", \
            "node_label_list_t", "sample_classification_data_t", "lda_sample_data_t", \
            "node_label_data_list_t", \
            "indexed_tree_layer_t", "feature_split_list_t", \
            "feature_split_t", \
            "line_search_result_t", \
            "lda_stat_result_data_t", \
            "binary_mapping_result_data_t" ]

# ----------------------------------------------------------------------------------------------------

def assert_relevant_conditions():
    assert( np.dtype( np.int32 ) < np.dtype( np.int64 ) )
    assert( np.dtype( np.float32 ) < np.dtype( np.float64 ) )

assert_relevant_conditions()

# ----------------------------------------------------------------------------------------------------

class EventStat( ct.Structure ):
    """
    typedef unsigned long long counter_t;

    // tree classification counters
    counter_t m_treeClassificationNumAligned_u;
    counter_t m_treeClassificationNumUnaligned_u;

    // fern_sample_projection counters
    counter_t m_sampleProjectionNumAligned_u;
    counter_t m_sampleProjectionNumUnaligned_u;

    // aligned vs non-aligned line search evaluation counters
    counter_t m_lineSearchEvalNumAligned_u;
    counter_t m_lineSearchEvalNumUnaligned_u;
    """
    _fields_ = [("classification_num_aligned",      ct.c_ulonglong),\
                ("classification_num_unaligned",    ct.c_ulonglong), \
                ("projection_num_aligned",          ct.c_ulonglong), \
                ("projection_num_unaligned",        ct.c_ulonglong), \
                ("line_search_eval_num_aligned",    ct.c_ulonglong), \
                ("line_search_eval_num_unaligned",  ct.c_ulonglong) ]

# ----------------------------------------------------------------------------------------------------


class union_1d_pi32_pf32_t( ct.Union ):
    mem_pf32_t = np.ctypeslib.ndpointer( dtype=np.float32, ndim=1, flags=('C_CONTIGUOUS',) )
    mem_pi32_t = np.ctypeslib.ndpointer( dtype=np.int32,   ndim=1, flags=('C_CONTIGUOUS',) )
    
    _fields_ = [("mem_pf32", mem_pf32_t),\
                ("mem_pi32", mem_pi32_t) ]


class union_2d_pi32_pf32_t( ct.Union ):
    mem_pf32_t = np.ctypeslib.ndpointer( dtype=np.float32, ndim=2 )
    mem_pi32_t = np.ctypeslib.ndpointer( dtype=np.int32,   ndim=2 )
    
    _fields_ = [("mem_pf32", mem_pf32_t),\
                ("mem_pi32", mem_pi32_t) ]


mem_pi8_t    = np.ctypeslib.ndpointer( dtype=np.int8,    ndim=1, flags=('C_CONTIGUOUS',) )
mem_pi32_t   = np.ctypeslib.ndpointer( dtype=np.int32,   ndim=1, flags=('C_CONTIGUOUS',) )
mem_pf32_t   = np.ctypeslib.ndpointer( dtype=np.float32, ndim=1, flags=('C_CONTIGUOUS',) )
mem2d_pi32_t = np.ctypeslib.ndpointer( dtype=np.int32,   ndim=2, flags=('C_CONTIGUOUS',) )
mem2d_pf32_t = np.ctypeslib.ndpointer( dtype=np.float32, ndim=2, flags=('C_CONTIGUOUS',) )


# ----------------------------------------------------------------------------------------------------


class image_descr_t( ct.Structure ):
    """
    int32_t nImgRows;
    int32_t nImgCols;
    
    int32_t rowStride;
    int32_t colStride;
    """
    _fields_=[ ( "nImgRows",  ct.c_int32 ), \
               ( "nImgCols",  ct.c_int32 ), \
               ( "rowStride", ct.c_int32 ), \
               ( "colStride", ct.c_int32 ) ]
    


class intimg_t( ct.Structure ):
    """
    image_descr_t    intimg_descr;

    union
    {
        const float32_t* mem_pf32;
        const int32_t*   mem_pi32;
    };
    
    bool   isFloat32Img;
    """
    _anonymous_ = ("memptr",)
    _fields_=[ ( "intimg_descr",    image_descr_t ), \
               ( "memptr",          union_2d_pi32_pf32_t ), \
               ( "isFloat32Img",    ct.c_bool ), ]

    integral_image = None


    @classmethod
    def from_numpy( cls, integral_image ):
        dtype = integral_image.dtype.type

        if issubclass( dtype, np.integer ) and ( dtype <= np.dtype( np.int32 ) ):
            dtype = np.int32
            isFloat32Img = False
        elif issubclass( dtype, np.floating ) and ( dtype <= np.dtype( np.float32 ) ):
            dtype = np.float32
            isFloat32Img = True
        else:
            raise TypeError( "Unsupported type (%r) of integral image" % dtype )

        integral_image = np.require( integral_image, dtype )

        itemsize = np.dtype( dtype ).itemsize
        assert( (integral_image.strides[0] % itemsize) == 0 )
        assert( (integral_image.strides[1] % itemsize) == 0 )

        # instantiate image descriptor
        intimg_descr = image_descr_t( nImgRows = integral_image.shape[0],
                                      nImgCols = integral_image.shape[1],
                                      rowStride = integral_image.strides[0] // itemsize,
                                      colStride = integral_image.strides[1] // itemsize )

        # instantiate integral image
        ret = cls( intimg_descr    = intimg_descr,
                   isFloat32Img    = isFloat32Img )

        if isFloat32Img is True:
            ret.mem_pf32 = integral_image.ctypes.data_as( union_2d_pi32_pf32_t.mem_pf32_t )
        else:
            ret.mem_pi32 = integral_image.ctypes.data_as( union_2d_pi32_pf32_t.mem_pi32_t )
        
        ret.integral_image = integral_image

        return ret


# ----------------------------------------------------------------------------------------------------

class box_list_t( ct.Structure ):
    """
    int32_t* up_pi32;
    int32_t* left_pi32;
    int32_t* down_pi32;
    int32_t* right_pi32;
    """
    _fields_=[ ( "up_pi32",    mem_pi32_t ), \
               ( "left_pi32",  mem_pi32_t ), \
               ( "down_pi32",  mem_pi32_t ), \
               ( "right_pi32", mem_pi32_t ) ]
    
    Boxes = None

    @classmethod
    def from_numpy( cls, boxes ):
        assert( isinstance(boxes, ImageBoxes) )

        ret = cls(   up_pi32 =    boxes.up.ctypes.data_as( mem_pi32_t ),
                   left_pi32 =  boxes.left.ctypes.data_as( mem_pi32_t ),
                   down_pi32 =  boxes.down.ctypes.data_as( mem_pi32_t ),
                  right_pi32 = boxes.right.ctypes.data_as( mem_pi32_t ) )

        ret.Boxes = boxes

        return ret
    
    def check_size( self, N ):
        return ( N == self.Boxes.size )


# ----------------------------------------------------------------------------------------------------

class box_feature_list_t( ct.Structure ):
    """
    box_list_t   first;
    box_list_t   second;
    float32_t*   lambda;  // NULL if "box ratio" feature instead of "box difference" 
    int32_t      numBoxPairs;
    """
    _fields_=[ ( "first",       box_list_t ), \
               ( "second",      box_list_t ), \
               ( "lambda_pf32", mem_pf32_t ), \
               ( "numBoxPairs", ct.c_int32 ) ]

    First  = None
    Second = None
    Lambda = None

    @classmethod
    def from_numpy( cls, First, Second, Lambda=None ):
        
        N = First.size
        First  = box_list_t.from_numpy( First )
        Second = box_list_t.from_numpy( Second )
        
        assert( First.check_size( N ) )
        assert( Second.check_size( N ) )

        ret = cls( first  =  First,
                   second =  Second,
                   numBoxPairs = N )

        # Preserve original memory
        ret.First = First
        ret.Second = Second

        if Lambda is not None:
            Lambda = np.require( Lambda, np.float32, [ 'C_CONTIGUOUS' ] )
            assert( N == Lambda.size )
            ret.lambda_pf32 = Lambda.ctypes.data_as( mem_pf32_t )
            ret.Lambda = Lambda
        
        return ret


# ----------------------------------------------------------------------------------------------------

class img_coord_list_t( ct.Structure ):
    """
    struct img_coord_t
    {
        int32_t rowIdx;
        int32_t colIdx;
    }
    
    img_coord_t*  coords;
    int32_t       numCoords;
    """

    _fields_=[ ( "coords",     mem2d_pi32_t ), \
               ( "numCoords" , ct.c_int32 ) ]

    img_coords = None


    @classmethod
    def from_indices( cls, rowIdx, colIdx ):
        img_coords = ImageCoords( rowIdx, colIdx )
        return cls.from_numpy( img_coords )


    @classmethod
    def from_numpy( cls, img_coords ):
        assert( isinstance( img_coords, ImageCoords ) )

        N = img_coords.size
        assert( N == img_coords.data.shape[0] )
        assert( 2 == img_coords.data.shape[1] )

        ret = cls( coords    = img_coords.data.ctypes.data_as( mem2d_pi32_t ),
                   numCoords = N )

        ret.img_coords = img_coords

        return ret


# ----------------------------------------------------------------------------------------------------


class sample_classification_data_t( ct.Structure ):
    """
    int32_t                 numData;        // =N
    int32_t                 numNodes;       // =M
    int32_t                 numLabels;      // =L
    
    union  // nodeIdx and leafIdx shall be used as synonyms
    {
        const int32_t*      nodeIdx;     // [ N ]
        const int32_t*      leafIdx;     // [ N ]
    };
    const int32_t*          labelIdx;    // [ N ]
    const int32_t*          dataWeight;  // [ N ]
    """
    _fields_=[ ( "numData",     ct.c_int32 ), \
               ( "numNodes",    ct.c_int32 ), \
               ( "numLabels",   ct.c_int32 ), \
               
               ( "nodeIdx_p" ,    mem_pi32_t ), \
               ( "labelIdx_p" ,   mem_pi32_t ), \
               ( "dataWeight_p",  mem_pi32_t ) ]

    node_idx     : np.ndarray
    label_idx    : np.ndarray
    data_weights : np.ndarray


    @classmethod
    def from_numpy( cls, node_idx     : np.ndarray,
                         label_idx    : np.ndarray,
                         data_weights : np.ndarray, 
                         num_nodes    : tp.Optional[int] = None,
                         num_labels   : tp.Optional[int] = None ):
        # The following requirements are not really functional relevant, but they indicate
        # that a lot of copying to data would be required resulting in a bad performance
        assert np.issubdtype( node_idx.dtype, np.int32 ), "Performance relevant assert violated"
        assert np.issubdtype( label_idx.dtype, np.int32 ), "Performance relevant assert violated"
        assert np.issubdtype( data_weights.dtype, np.int32 ), "Performance relevant assert violated"
        assert node_idx.flags['C_CONTIGUOUS'], "Performance relevant assert violated"
        assert label_idx.flags['C_CONTIGUOUS'], "Performance relevant assert violated"
        assert data_weights.flags['C_CONTIGUOUS'], "Performance relevant assert violated"

        node_idx     = np.require( node_idx,     np.int32, [ 'C_CONTIGUOUS' ] )
        label_idx    = np.require( label_idx,    np.int32, [ 'C_CONTIGUOUS' ] )
        data_weights = np.require( data_weights, np.int32, [ 'C_CONTIGUOUS' ] )

        if num_nodes is None:
            num_nodes = np.absolute( node_idx ).max() + 1
        else:
            assert np.absolute( node_idx ).max() < num_nodes
        
        if num_labels is None:
            num_labels = label_idx.max() + 1
        else:
            assert label_idx.max() < num_labels

        num_data = node_idx.size
        assert( num_data == label_idx.size )
        assert( num_data == data_weights.size )

        ret = cls( numData      = int(num_data),
                   numNodes     = int(num_nodes),
                   numLabels    = int(num_labels),
                   nodeIdx_p    = node_idx.ctypes.data_as( mem_pi32_t ),
                   labelIdx_p   = label_idx.ctypes.data_as( mem_pi32_t ),
                   dataWeight_p = data_weights.ctypes.data_as( mem_pi32_t ) )
        
        ret.node_idx     = node_idx
        ret.label_idx    = label_idx
        ret.data_weights = data_weights

        return ret

# Aliased on sample_classification_data_t
node_label_list_t = sample_classification_data_t
lda_sample_data_t = sample_classification_data_t


# ----------------------------------------------------------------------------------------------------


class node_label_data_list_t( ct.Structure ):
    """
    sample_classification_data_t sampleClData;

    float32_t*                   featureValues;
    int32_t                      featureTypeIdx;
    """

    _fields_=[ ( "sampleClData",    sample_classification_data_t ), \
               ( "featureValues_p", mem_pf32_t ), \
               ( "featureTypeIdx",  ct.c_int32 ), ]

    feature_vals : np.ndarray


    @classmethod
    def from_numpy( cls, sampleClData     : sample_classification_data_t,
                         feature_vals     : np.ndarray,
                         feature_type_idx : int ):

        # The following requirements are not really functional relevant, but they indicate
        # that a lot of copying to data would be required resulting in a bad performance
        assert np.issubdtype( feature_vals.dtype, np.float32 ), "Performance relevant assert violated"
        assert feature_vals.flags['C_CONTIGUOUS'], "Performance relevant assert violated"

        feature_vals = np.require( feature_vals, np.float32, [ 'C_CONTIGUOUS' ] )
        assert sampleClData.numData == feature_vals.size

        ret = cls( sampleClData = sampleClData,
                featureValues_p = feature_vals.ctypes.data_as( mem_pf32_t ),
                 featureTypeIdx = feature_type_idx )

        ret.feature_vals = feature_vals

        return ret


# ----------------------------------------------------------------------------------------------------


class indexed_tree_layer_t( ct.Structure ):
    """
    int32_t numSplits;

    int32_t*     featureIdx;
    float32_t*   featureSplit;
    """

    _fields_=[ ( "numSplits",      ct.c_int32 ), \
               ( "featureIdx" ,    mem_pi32_t ), \
               ( "featureSplits",  mem_pf32_t ) ]

    FeatureIdx    = None
    FeatureSplits = None


    @classmethod
    def from_numpy( cls, featureIdx, featureSplits ):

        featureIdx    = np.require( featureIdx,      np.int32, [ 'C_CONTIGUOUS' ] )
        featureSplits = np.require( featureSplits, np.float32, [ 'C_CONTIGUOUS' ] )

        N = featureIdx.size
        assert( N == featureSplits.size )

        ret = cls( numSplits      = N,
                   featureIdx     = featureIdx.ctypes.data_as( mem_pi32_t ),
                   featureSplits  = featureSplits.ctypes.data_as( mem_pf32_t ) )

        ret.FeatureIdx    = featureIdx
        ret.FeatureSplits = featureSplits

        return ret


# ----------------------------------------------------------------------------------------------------

class feature_split_list_t( ct.Structure ):
    """
    int32_t numSplits;

    int32_t*     featureIdx;
    float32_t*   featureSplit;

    union
    {
        int32_t*     nodeGiniRating;
        float32_t*   nodeEntropy;
    };
    """

    _anonymous_ = ("ratings",)
    _fields_=[ ( "numSplits",      ct.c_int32 ), \
               ( "featureIdx" ,    mem_pi32_t ), \
               ( "featureSplits",  mem_pf32_t ), \
               ( "ratings" ,       union_1d_pi32_pf32_t ) ]

    Ratings       = None
    FeatureIdx    = None
    FeatureSplits = None


    @classmethod
    def from_numpy( cls, ratings, featureIdx, featureSplits ):

        featureIdx    = np.require( featureIdx,      np.int32, [ 'C_CONTIGUOUS' ] )
        featureSplits = np.require( featureSplits, np.float32, [ 'C_CONTIGUOUS' ] )

        N = ratings.size
        assert( N == featureIdx.size )
        assert( N == featureSplits.size )

        ret = cls( numSplits      = N,
                   featureIdx     = featureIdx.ctypes.data_as( mem_pi32_t ),
                   featureSplits  = featureSplits.ctypes.data_as( mem_pf32_t ) )

        # Check ratings types
        if np.issubdtype( ratings.dtype, np.integer ) == True:
            ratings             = np.require( ratings,   np.int32, [ 'C_CONTIGUOUS' ] )
            #ret.nodeGiniRating = ratings.ctypes.data_as( mem_pi32_t )
            ret.mem_pi32        = ratings.ctypes.data_as( union_1d_pi32_pf32_t.mem_pi32_t )
        elif np.issubdtype( ratings.dtype, np.floating ) == True:
            ratings             = np.require( ratings, np.float32, [ 'C_CONTIGUOUS' ] )
            #ret.nodeEntropy     = ratings.ctypes.data_as( mem_pf32_t )
            ret.mem_pf32        = ratings.ctypes.data_as( union_1d_pi32_pf32_t.mem_pf32_t )
        else:
            assert( False )
        
        ret.Ratings       = ratings
        ret.FeatureIdx    = featureIdx
        ret.FeatureSplits = featureSplits

        return ret

# ----------------------------------------------------------------------------------------------------

class rating_union_t( ct.Union ):
    _fields_ = [("nodeEntropy", ct.c_float ),\
                ("nodeGiniRating", ct.c_int32) ]


class feature_split_t( ct.Structure ):
    """
    int32_t     featureIdx;
    float32_t   featureSplit;

    union
    {
        int32_t     nodeGiniRating;
        float32_t   nodeEntropy;
    };
    """

    _anonymous_ = ("rating",)
    _fields_=[ ( "featureIdx" ,    ct.c_int32 ), \
               ( "featureSplit",   ct.c_float ), \
               ( "rating" ,        rating_union_t ) ]


# ----------------------------------------------------------------------------------------------------

class line_search_result_t( ct.Structure ):
    """
    float32_t    prmVal;
    int32_t      numOptimizationSteps;

    union
    {
        int32_t      giniRating;
        float32_t    entropyRating;
    };

    float32_t    prmValPenalty;
    """
    _anonymous_ = ("rating",)
    _fields_=[ ( "prmVal",            ct.c_float ), \
               ( "num_improvements",  ct.c_int32 ), \
               ( "rating" ,           rating_union_t ),
               ( "prmValPenalty",     ct.c_float ) ]

# ----------------------------------------------------------------------------------------------------


class lda_stat_result_data_t( ct.Structure ):
    """
    int32_t                 numFeatures;    // =k

    int32_t                 intra_covar_stride;
    float32_t*              intra_covar;

    int32_t                 inter_covar_stride;
    float32_t*              inter_covar;
    """

    _fields_=[ ( "numFeatures",        ct.c_int32 ), \
               ( "intra_covar_stride", ct.c_int32 ), \
               ( "intra_covar",        mem2d_pf32_t ), \
               ( "inter_covar_stride", ct.c_int32 ),   \
               ( "inter_covar",        mem2d_pf32_t ) ]
    
    Intra_Covar = None
    Inter_Covar = None


    @classmethod
    def from_numpy( cls, num_features ):
        num_features = int(num_features)
        
        intra_covar = np.empty( (num_features, num_features), dtype=np.float32 )
        assert intra_covar.flags['C_CONTIGUOUS'] is True

        inter_covar = np.empty( (num_features, num_features), dtype=np.float32 )
        assert inter_covar.flags['C_CONTIGUOUS'] is True

        if __debug__:
            intra_covar.fill( np.nan )
            inter_covar.fill( np.nan )

        ret = cls( numFeatures        = num_features,
                   intra_covar_stride = num_features,
                   intra_covar        = intra_covar.ctypes.data_as( mem2d_pf32_t ),
                   inter_covar_stride = num_features,
                   inter_covar        = inter_covar.ctypes.data_as( mem2d_pf32_t ) )
               
        ret.Intra_Covar = intra_covar
        ret.Inter_Covar = inter_covar

        return ret


# ----------------------------------------------------------------------------------------------------


class binary_mapping_result_data_t( ct.Structure ):
    """
    int32_t          numData;
    int8_t*          planeSide;
    
    union
    {
        int32_t*     giniWeight;
        float32_t*   entropyWeight;
    };
    """
    _anonymous_ = ("binaryWeight",)
    _fields_=[ ( "numData",       ct.c_int32 ), \
               ( "planeSide",     mem_pi8_t ), \
               ( "binaryWeight",  union_1d_pi32_pf32_t ) ]


    PlaneSide    = None
    BinaryWeight = None


    @classmethod
    def from_numpy( cls, num_data : int, use_gini_index : bool ):
        num_data       = int(num_data)
        use_gini_index = bool( use_gini_index )
        
        plane_side = np.empty( num_data, dtype=np.int8 )
        assert plane_side.flags['C_CONTIGUOUS'] is True

        if __debug__:
            plane_side.fill( np.iinfo(np.int8).max )

        # instance of data structure
        ret = cls( numData = num_data,
                   planeSide = plane_side.ctypes.data_as( mem_pi8_t ) )

        if use_gini_index is True:
            binary_weight = np.empty( num_data, dtype=np.int32 )
            ret.mem_pi32 = binary_weight.ctypes.data_as( union_1d_pi32_pf32_t.mem_pi32_t )

            if __debug__:
                binary_weight.fill( np.iinfo(np.int32).min )
        else:
            binary_weight = np.empty( num_data, dtype=np.float32 )
            ret.mem_pf32 = binary_weight.ctypes.data_as( union_1d_pi32_pf32_t.mem_pf32_t )

            if __debug__:
                binary_weight.fill(np.nan)

        assert binary_weight.flags['C_CONTIGUOUS'] is True
        ret.PlaneSide = plane_side
        ret.BinaryWeight = binary_weight

        return ret
