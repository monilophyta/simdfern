# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



# ----------------------------------------------------------------------------------------------


__all__ = ["BaseSplitOptimizer"]


import typing as tp
from abc import ABC, abstractmethod
import ctypes as ct
import numpy as np

from . import tree_types as tt
from .tree_ctypes_return_codes import ReturnCode


# ----------------------------------------------------------------------------------------------


class BaseSplitOptimizer( ABC ):

    split_optimizer_handle : ct.c_void_p
    last_feature_idx       : tp.Optional[int]= None

    optimization_method    : ReturnCode = ReturnCode.GINI_RATING

    node_label_list        : tt.node_label_list_t


    @property
    def num_data(self) -> int:
        return int(self.node_label_list.numData)

    @property
    def num_nodes(self) -> int:
        return int(self.node_label_list.numNodes)


    def __init__( self, sot, node_idx : np.ndarray,
                            label_idx : np.ndarray,
                         data_weights : np.ndarray,
                            num_nodes : tp.Optional[int] = None,
                           num_labels : tp.Optional[int] = None ):
        
        if num_nodes is None:
            num_nodes = node_idx.max() + 1
        
        if num_labels is None:
            num_labels = label_idx.max() + 1

        num_data = node_idx.size
        assert( num_data == label_idx.size )
        assert( num_data == data_weights.size )
        assert( num_nodes > 0 )

        # Construct optimizer
        handle_int = sot.create_split_optimizer( num_labels, num_nodes )
        assert( handle_int > 0 )
        self.split_optimizer_handle = ct.c_void_p( handle_int )

        # initialize node label meta data
        self.node_label_list = tt.node_label_list_t.from_numpy( node_idx = node_idx,
                                                               label_idx = label_idx, 
                                                            data_weights = data_weights,
                                                               num_nodes = int(num_nodes),
                                                              num_labels = int(num_labels) )
        # Initialize statistic
        self.initialize_parent_layer_statistic()


    def destruct(self, sot):
        if self.split_optimizer_handle is not None:
            sot.destruct_split_optimizer( ct.byref( self.split_optimizer_handle) )
            assert( self.split_optimizer_handle.value is None )

            self.split_optimizer_handle = None
    

    @abstractmethod
    def initialize_parent_layer_statistic( self, sot):

        # fill parent layer statistics
        ret_val = sot.initialize_parent_layer_statistic( self.split_optimizer_handle, 
                                                         ct.byref( self.node_label_list ) )
        ret_val = ReturnCode.from_int_return( ret_val )
        ret_val.raise_on_error()

        assert ret_val.has_valid_rating_info
        assert bool(self.optimization_method & ret_val), "Wrong optimization method used"
        
        self.last_feature_idx = -1


    @abstractmethod
    def process_feature( self, sot, feature_vals ):
        # feature [ N ]
        
        assert( feature_vals.size == self.num_data )
        self.last_feature_idx += 1

        param = tt.node_label_data_list_t.from_numpy( 
                sampleClData = self.node_label_list,
                feature_vals = feature_vals,
            feature_type_idx = self.last_feature_idx )
        
        num_improvements = sot.process_feature( self.split_optimizer_handle, ct.byref( param ) )

        if num_improvements < 0:
            ret_code = ReturnCode.from_int_return( num_improvements )
            ret_code.raise_on_error()

        return num_improvements


# ----------------------------------------------------------------------------------------------


