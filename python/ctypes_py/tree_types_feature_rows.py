# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

from collections.abc import Sequence
import typing as tp
import logging
import ctypes as ct
import atexit
import numpy as np


from .tree_types import mem_pf32_t


__all__ = [ "create_feature_rows", "FeatureRows" ]

# ------------------------------------------------------------------------------------------
# global counting

GLOBAL_COUNTER_feature_row_copy= 0
GLOBAL_COUNTER_feature_row_nocopy = 0


# ------------------------------------------------------------------------------------------
# logging

# create logger
logger = logging.getLogger('tree_types.feature_rows')

if __debug__:
    logger.setLevel(logging.INFO)
else:
    logger.setLevel(logging.WARN)

def dump_count_stats():
    logger.info( "GLOBAL_COUNTER_feature_row_copy=%d", GLOBAL_COUNTER_feature_row_copy)
    logger.info( "GLOBAL_COUNTER_feature_row_nocopy=%d", GLOBAL_COUNTER_feature_row_nocopy )

atexit.register(dump_count_stats)


# ------------------------------------------------------------------------------------------

def create_feature_rows( num_data : int = 0,
                         init_feature_row_capacity : int = 5, 
                         feature_rows : tp.Optional[list] = None, 
                         feature_row_pointer_list : tp.Optional[list] = None ):

    if feature_rows is None:
        feature_rows = list()
    
    if feature_row_pointer_list is None:
        feature_row_pointer_list = list()

    assert isinstance( feature_rows, list )
    assert isinstance( feature_row_pointer_list, list )
    assert len(feature_rows) == len(feature_row_pointer_list)
    assert (len(feature_rows) == 0) or (feature_rows[0].size == num_data)

    init_feature_row_capacity = max( int(init_feature_row_capacity), 
                                        int(FeatureRows.INCREASE_FACTOR * len(feature_row_pointer_list)) )
    
    feature_row_pointer_array = ( mem_pf32_t * init_feature_row_capacity )( *feature_row_pointer_list )

    ret = FeatureRows( numData         = int(num_data),
                       numFeatureRows  = len(feature_row_pointer_list),
                       featureRows     = feature_row_pointer_array )
    
    ret.feature_rows              = feature_rows
    ret.feature_row_pointer_list  = feature_row_pointer_list
    ret.feature_row_pointer_array = feature_row_pointer_array

    return ret


class FeatureRows( ct.Structure ):
    """
    int32_t                 numData;        // =N
    int32_t                 numFeatures;    // =k

    // feature Matrix  [ k x N ]
    const float32_t* const* featureRows;
    """

    _fields_=[ ( "numData",         ct.c_int32 ), \
               ( "numFeatureRows",  ct.c_int32 ), \
               ( "featureRows" ,    ct.POINTER( mem_pf32_t ) ) ]

    # array increase factor in case of exceeded capacity
    INCREASE_FACTOR : float = 1.3

    feature_rows              : list
    feature_row_pointer_list  : list
    feature_row_pointer_array : np.ctypeslib.ndpointer
    
        
    # --- Implement Sequence Protocoll

    def __len__(self):
        return len(self.feature_rows)
    
    def __getitem__(self, idx):
        return self.feature_rows[int(idx)]
    
    def __iter__(self):
        return iter(self.feature_rows)

    def append(self, feature_row):
        self.append_feature_row( feature_row )
    
    
    def extend(self, feature_rows : Sequence):
        assert isinstance( feature_rows, Sequence )
        for nfr in feature_rows:
            self.append_feature_row( nfr )

    # ---- convenient attributes

    @property
    def capacity(self):
        return len(self.feature_row_pointer_array)
    
    @property
    def num_data(self):
        return self.numData
    
    @property
    def num_features(self):
        return len(self.feature_rows)

    # ---- add_feature_row as replacement for append

    def append_feature_row(self, feature_row ):
        assert isinstance( feature_row, np.ndarray )
        assert np.issubdtype( feature_row.dtype, np.float32 ), "expected float32 but got %r" % feature_row.dtype
        assert (self.num_features == 0) or (feature_row.size == self.num_data)

        # bring feature_row in correct shape
        n_feature_row = np.require( feature_row, np.float32, [ 'C_CONTIGUOUS' ] )
        if n_feature_row is feature_row:
            global GLOBAL_COUNTER_feature_row_nocopy
            GLOBAL_COUNTER_feature_row_nocopy += 1
        else:
            global GLOBAL_COUNTER_feature_row_copy
            GLOBAL_COUNTER_feature_row_copy += 1
            feature_row = n_feature_row
        
        feature_row_pointer = feature_row.ctypes.data_as( mem_pf32_t )

        assert len(self.feature_rows) == len(self.feature_row_pointer_list)
        assert len(self.feature_rows) == self.numFeatureRows
        assert len(self.feature_rows) <= len(self.feature_row_pointer_array)
        
        # index of new feature row
        n_row_idx = len(self.feature_row_pointer_list)

        # add feature row to storage lists
        self.feature_rows.append( feature_row )
        self.feature_row_pointer_list.append( feature_row_pointer )

        if n_row_idx == self.capacity:
            # not enough capacity

            # create new pointer array
            n_capacity = max( n_row_idx + 1, int(n_row_idx * self.INCREASE_FACTOR)  )
            n_feature_row_pointer_array = ( mem_pf32_t * n_capacity )( *self.feature_row_pointer_list )
            self.featureRows = n_feature_row_pointer_array
            self.feature_row_pointer_array = n_feature_row_pointer_array

            assert self.capacity == n_capacity
        else:
            # still enough capacity available
            assert self.feature_row_pointer_array[n_row_idx].value is None
            self.feature_row_pointer_array[n_row_idx] = feature_row_pointer
        
        assert feature_row_pointer.value == self.featureRows[n_row_idx].value
        self.numFeatureRows = len(self.feature_rows)
        if self.numData == 0:
            self.numData = feature_row.size

        assert self.capacity >= self.num_features
        assert self.numFeatureRows == self.num_features


    def run_paranoid_ctypes_verifications( self, do_throw=True ):

        # check if feature size attributes are matching
        fsize_check = ( self.numFeatureRows == len(self.feature_rows) ) and \
                      ( self.numFeatureRows == len(self.feature_row_pointer_list) ) and \
                      ( self.numFeatureRows <= len(self.feature_row_pointer_array) )
        assert fsize_check
        
        # check if data sizes of all rows are matching
        dsize_check = all( [ fr.size == self.numData for fr in self.feature_rows ] )
        assert dsize_check
        
        # check that all row pointers are valid
        row_ptr_check = all( [ fr.ctypes.data_as( mem_pf32_t ).value == fr_ptr.value for (fr,fr_ptr) in \
                               zip( self.feature_rows, self.feature_row_pointer_list ) ] )
        assert row_ptr_check
        
        # check that all row pointers are within c-types array
        array_ptr_check = all( [ self.feature_row_pointer_array[idx].value == fr_ptr.value for (idx,fr_ptr) in \
                               enumerate( self.feature_row_pointer_list ) ] )
        assert array_ptr_check

        # check that row pointer attribute is correct

        #array_atr_check = self.featureRows == self.feature_row_pointer_array
        #v1 = self.featureRows
        #v2 = ct.byref( self.feature_row_pointer_array )
        #assert array_atr_check

        # final summary
        final_check = fsize_check and dsize_check and row_ptr_check and array_ptr_check #and array_atr_check
        
        if (not final_check) and bool(do_throw):
            msg = "Inconsistent feature row structure"
            raise AssertionError(msg)
        
        return final_check


# ----------------------------------------------------------------------------------------------------

# https://stackoverflow.com/questions/30813787/how-to-dereference-an-address-into-an-integer-in-python
derefer_first = lambda x: ct.cast( x.ctypes.data_as( mem_pf32_t ).value, ct.POINTER( ct.c_float ) ).contents.value

# ----------------------------------------------------------------------------------------------------


# register as Sequence
assert not issubclass( FeatureRows, Sequence )
Sequence.register( FeatureRows )
assert issubclass( FeatureRows, Sequence )

