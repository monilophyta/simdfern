# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



# ----------------------------------------------------------------------------------------------

import typing as tp
import ctypes as ct
import numpy as np
from . import tree_types as tt
from .tree_ctypes_library import fern_split_optim_interface
from .tree_base_split_optimization import BaseSplitOptimizer
from .fern_classification_layer import IndexedFernLayer
from .tree_ctypes_return_codes import ReturnCode

# ----------------------------------------------------------------------------------------------

class FernSplitOptimizer( BaseSplitOptimizer ):

    def __init__( self, node_idx : np.ndarray,
                       label_idx : np.ndarray,
                    data_weights : np.ndarray, 
                       num_nodes : tp.Optional[int] = None,
                      num_labels : tp.Optional[int] = None ):
        
        super().__init__( fern_split_optim_interface, \
                          node_idx, \
                          label_idx, \
                          data_weights, \
                          num_nodes, \
                          num_labels )

    def __del__(self):
        self.destruct( fern_split_optim_interface )    

    def initialize_parent_layer_statistic( self ):
        return super().initialize_parent_layer_statistic( fern_split_optim_interface )


    def process_feature( self, feature_vals ):
        return super().process_feature( fern_split_optim_interface, feature_vals )

    def reset_optimal_split(self):
        fern_split_optim_interface.reset_optimal_split( self.split_optimizer_handle ) 

    def get_optimal_split(self):

        param = tt.feature_split_t()
        
        ret_val = fern_split_optim_interface.get_optimal_split( self.split_optimizer_handle, ct.byref( param ) )
        ret_val = ReturnCode.from_int_return( ret_val )

        ret_val.raise_on_error()
        assert ret_val.has_valid_rating_info
        assert bool( self.optimization_method & ret_val )

        if ret_val.has_gini_rating_flag:
            rating = param.nodeGiniRating
        else:
            assert ret_val.has_entropy_rating_flag
            rating = param.nodeEntropy

        return ( IndexedFernLayer( param.featureIdx, param.featureSplit ), rating )

# ----------------------------------------------------------------------------------------------
