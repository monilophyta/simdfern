# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = [ "IndexedFernLayer", "WeightedFernLayer", "project_features", "ESummationAlgo" ]


import typing as tp
from collections import namedtuple, Sequence
from enum import IntEnum
from functools import reduce
import ctypes as ct
import math
import numpy as np

from .tree_classification_layer import AbtractLayerBase
from . import tree_types_feature_rows as ttfr
from .tree_ctypes_library import sample_projection_interface as spi


# https://numpydoc.readthedocs.io/en/latest/format.html


class ESummationAlgo(IntEnum):
    NAIVE    = 0
    KAHAN    = 1
    NEUMAIER = 2
    KLEIN    = 3

#--------------------------------------------------------------------------------------------------------

IndexedFernLayerBase = namedtuple('IndexedFernLayerBase', [ 'feature_idx', 'feature_split' ] )

#--------------------------------------------------------------------------------------------------------


class IndexedFernLayer( IndexedFernLayerBase, AbtractLayerBase ):
    

    def __new__(typ, feature_idx, feature_split ):
        obj = IndexedFernLayerBase.__new__( typ, int(feature_idx), float(feature_split) )
        return obj


    def __init__(self, *args, **argv):
        """
        Parameters
        ----------
        feature_idx : float
            Index of used feature in node n
        feature_split : float
            Value the feature is compared with
        """
        pass


    # allows hashing of context - mainly debugging purpose
    def get_hashable_seq(self) -> tuple:
        return (self.feature_idx, self.feature_split)


    @property
    def max_feature_idx(self):
        return self.feature_idx.max()


    def classify( self, nodeIdx, feature_rows, out = None ):
        """
        Parameters
        ----------
        nodeIdx : int32 array [ N ]
            Node index of sample n,
            >=0 for active nodes, <0 for finalized nodes
        feature_rows : float matrix [ nF x N ] or nf * [ array(N) ] or float array [N]
            features of samples: feature_rows[:,n] nf * [ array(N)[n] ]
        
        Returns
        -------
        int32 array [ N ]
            Propagated node indices.
        """

        ## check input

        if not isinstance( feature_rows, np.ndarray ):
            assert isinstance( feature_rows, Sequence )
            # feature_rows is a list of (hopefully) ndarrays
            feature_rows = feature_rows[ self.feature_idx ]
        
        assert isinstance( feature_rows, np.ndarray )
        assert np.issubdtype( feature_rows.dtype, np.floating )

        if feature_rows.ndim == 2:
            # feature_rows is a matrix
            feature_rows = feature_rows[self.feature_idx]
        
        assert feature_rows.ndim == 1
        assert feature_rows.size == nodeIdx.size
        
        assert np.issubdtype( nodeIdx.dtype, np.int32 )

        ## shift up node indices
        n_nodeIdx = np.left_shift( nodeIdx, 1, out=out )
        
        # some bug in some python/numpy versions seems to require this
        n_nodeIdx = n_nodeIdx.astype( np.int32 )
        assert np.issubdtype( n_nodeIdx.dtype, np.int32 )

        ## comparison values
        np.add( n_nodeIdx, (feature_rows < self.feature_split), out=n_nodeIdx )

        return n_nodeIdx

#--------------------------------------------------------------------------------------------------------


WeightedFernLayerBase = namedtuple('WeightedFernLayerBase', [ 'indexed_layer', 'feature_weights' ] )


#--------------------------------------------------------------------------------------------------------



class WeightedFernLayer( WeightedFernLayerBase, AbtractLayerBase ):
    """
    Parameters
    ----------
    feature_weights : ndarray of type float
        Weights for feature projected
    feature_split : float
        Value the feature is compared with on the projected feature
    num_features : int
    """

    @property
    def feature_split(self):
        return self.indexed_layer.feature_split
    
    @property
    def num_features(self):
        return self.feature_weights.size
    
    @property
    def max_feature_idx(self):
        return (self.num_features - 1)


    @classmethod
    def create( typ, feature_weights, feature_split ):
        assert isinstance( feature_weights, np.ndarray )
        assert np.issubdtype( feature_weights.dtype, np.floating )

        indexed_layer = IndexedFernLayer( 0, feature_split )
        
        obj = WeightedFernLayer( indexed_layer, feature_weights )
        return obj

    # allows hashing of context - mainly debugging purpose
    def get_hashable_seq(self) -> tuple:
        return (self.indexed_layer.feature_split, self.feature_weights)


    def project_features( self, feature_rows ):
        """
        Parameters
        ----------
        feature_rows : float matrix [ nF x N ] or nf * [ array(N) ] or instance of FeatureRows
        
        Returns
        -------
        float32 array [ N ]
            Projected features
        """
        return project_features( self.feature_weights, feature_rows, ESummationAlgo.NEUMAIER )


    def classify( self, nodeIdx, feature_rows, out = None ):
        """
        Parameters
        ----------
        nodeIdx : int32 array [ N ]
            Node index of sample n,
            >=0 for active nodes, <0 for finalized nodes
        feature_rows : float matrix [ nF x N ] or nf * [ array(N) ]
            features of samples: feature_rows[:,n] nf * [ array(N)[n] ]
        
        Returns
        -------
        int32 array [ N ]
            Propagated node indices.
        """

        ## check input
        assert np.issubdtype( nodeIdx.dtype, np.int32 )
        
        # Project features
        projected_row = project_features( self.feature_weights, feature_rows, ESummationAlgo.NEUMAIER )
        assert projected_row.size == nodeIdx.size
        
        # run classification
        return self.indexed_layer.classify( nodeIdx, [projected_row], out=out )


    def get_distances( self, feature_rows ):
        """
        Parameters
        ----------
        feature_rows : float matrix [ nF x N ] or nf * [ array(N) ] or instance of FeatureRows
        
        Returns
        -------
        float32 array [ N ]
            sample distances to classification plane
        """
        # Project features
        distances = project_features( self.feature_weights, feature_rows )

        # center the projected row
        distances -= self.feature_split

        return np.absolute( distances )


    def next_stage_features( self, feature_rows ):
        """
        Parameters
        ----------
        feature_rows : float matrix [ nF x N ] or nf * [ array(N) ] or instance of FeatureRows
        
        Returns
        -------
        list of two float32 arrays [ N ]
            Projected features, left and right separated
        """
        # Project features
        projected_row = project_features( self.feature_weights, feature_rows, ESummationAlgo.NEUMAIER )

        # classify
        node_idx = np.zeros( projected_row.size, dtype=np.int32 )
        node_idx = self.indexed_layer.classify( node_idx, [projected_row], out=node_idx )

        # center the projected row
        projected_row -= self.feature_split

        # separate feature rows
        left  = node_idx.choose( ( projected_row, np.float32(0) ) )
        right = node_idx.choose( ( np.float32(0), projected_row ) )
        return np.stack( (left,right), axis=0 )
        

    @property
    def l1norm(self) -> float:
        return float( np.linalg.norm( self.feature_weights, ord=1 ) )
    
    @property
    def l2norm(self) -> float:
        return float( np.linalg.norm( self.feature_weights, ord=2 ) )
    
    @property
    def normalized_l1norm(self) -> float:
        return self.l1norm / max(self.l2norm, np.finfo(self.feature_weights.dtype).min )
    
    @property
    def centralized_l1norm(self) -> float:
        return min( 1.0, max( 0.0, self.normalized_l1norm - 1. ) / math.sqrt( self.num_features ) )

    @property
    def normalized_l1norm_gradient(self) -> np.ndarray:
        l1norm = self.l1norm
        l2norm = max( self.l2norm, np.finfo(float).eps )
        grd = np.sign(self.feature_weights) / l2norm
        grd -= self.feature_weights * (l1norm / (l2norm**3))
        return grd
    
    @property
    def centralized_l1norm_gradient(self) -> np.ndarray:
        return (self.normalized_l1norm_gradient / math.sqrt( self.num_features ))

#--------------------------------------------------------------------------------------------------------



def project_features( feature_weights : np.ndarray, 
                         feature_rows : tp.Sequence,
                             sum_algo : ESummationAlgo = ESummationAlgo.NEUMAIER ):
    """
    Parameters
    ----------
    feature_weights : ndarray of shape [ nF ]
    feature_rows : float matrix [ nF x N ] or nf * [ array(N) ] or instance of FeatureRows
    sum_algo : instance of ESummationAlgo
    
    Returns
    -------
    float array [ N ]
        Projected features with dtype of feature_weights
    """
    assert isinstance( sum_algo, ESummationAlgo )
    assert isinstance( feature_weights, np.ndarray )
    num_features = feature_weights.size
    dtype = feature_weights.dtype

    if isinstance( feature_rows, np.ndarray ):
        assert feature_rows.shape[0] == num_features
        
        # project features
        proj_data = np.dot( feature_weights, feature_rows.astype(dtype) )  # [ nf ] x [ nf x N ]
    elif isinstance( feature_rows, ttfr.FeatureRows ) and np.issubdtype( dtype, np.float32 ):

        assert num_features <= feature_rows.num_features
        assert feature_rows.run_paranoid_ctypes_verifications()

        # TODO: use alignment from proj_feature_creation
        proj_data = np.zeros( feature_rows.num_data, dtype=np.float32 )
        feature_weights = np.require( feature_weights, np.float32, ["C_CONTIGUOUS"] )

        spi.project_samples( ct.byref( feature_rows ),
                             num_features,
                             feature_weights,
                             proj_data,
                             sum_algo )
    else:
        assert isinstance( feature_rows, Sequence )
        assert len(feature_rows) >= num_features

        #project features
        proj_data = reduce( lambda x,idx: x + np.multiply( feature_weights[idx], feature_rows[idx], dtype=dtype ), \
                                          range(num_features), 0  )
    
    return proj_data


