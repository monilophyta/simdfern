# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--




import numpy as np

__all__ = [ "is_array_start_aligned", "is_array_fully_aligned", \
            "set_default_simd_vector32_size", \
            "default_simd_vector32_size", "default_simd_vector32_byte_size" ]


###################################################################

SIMD_VECTOR32_SIZE = None
SIMD_VECTOR32_MASK = None

SIMD_VECTOR32_BYTE_SIZE = None
SIMD_VECTOR32_BYTE_MASK = None



def set_default_simd_vector32_size( simd_item32_num ):
    global SIMD_VECTOR32_SIZE
    global SIMD_VECTOR32_MASK
    global SIMD_VECTOR32_BYTE_SIZE
    global SIMD_VECTOR32_BYTE_MASK
    
    SIMD_VECTOR32_SIZE = int(simd_item32_num)
    SIMD_VECTOR32_MASK = SIMD_VECTOR32_SIZE - 1

    SIMD_VECTOR32_BYTE_SIZE = SIMD_VECTOR32_SIZE * 4
    SIMD_VECTOR32_BYTE_MASK = SIMD_VECTOR32_BYTE_SIZE - 1


def default_simd_vector32_size():
    return SIMD_VECTOR32_SIZE


def default_simd_vector32_byte_size():
    return SIMD_VECTOR32_BYTE_SIZE



###################################################################

def is_array_start_aligned( xarr ) -> bool:
    """!@brief Check if begin of vectors(arrays) and matrices are aligned

    @TODO: Write more documentation
    """

    if xarr.dtype not in [np.dtype(np.int32), np.dtype(np.float32)]:
        raise ValueError( "Only int32 and float32 are supported by SIMD backend")
    
    check = xarr.flags['C_CONTIGUOUS'] and ((xarr.ctypes.data & SIMD_VECTOR32_BYTE_MASK) == 0)

    if xarr.ndim == 1:
        check = check and ( xarr.size >= SIMD_VECTOR32_SIZE )
    elif xarr.ndim == 2:
        check = check and ((xarr.strides[0] & SIMD_VECTOR32_BYTE_MASK ) == 0) and (xarr.shape[1] >= SIMD_VECTOR32_SIZE)
    else:
        check = False

    return check


def is_array_fully_aligned( xarr ) -> bool:
    """!@brief Check is  vectors(arrays) and matrices are aligned

    @TODO: Write more documentation
    """

    check = is_array_start_aligned( xarr )

    if xarr.ndim == 1:
        check = check and (( xarr.size & SIMD_VECTOR32_MASK ) == 0)
    elif xarr.ndim == 2:
        check = check and (( xarr.shape[1] & SIMD_VECTOR32_MASK) == 0)
    else:
        assert( check is False )

    return check
