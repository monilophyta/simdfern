# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = [ "empty_memmap" ]


import numpy as np
import typing as tp
from tempfile import TemporaryFile


def empty_memmap( size: int, dtype: tp.Union[type,str,np.dtype] ):

    tfile = TemporaryFile( mode='w+b', buffering=0 )
    fp = np.memmap( tfile, dtype=dtype, mode='r+', shape=(size,) )
    return fp



