# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["aligned_empty", "aligned_zeros", "aligned_ones", \
           "aligned_empty_like", "aligned_zeros_like", "aligned_ones_like", \
           "as_aligned_array"]



import collections
import numpy as np
import typing as tp
from .alignment import is_array_start_aligned, is_array_fully_aligned, default_simd_vector32_size, default_simd_vector32_byte_size
from .memmap_array import empty_memmap




def aligned_empty( shape      : tp.Union[int,tuple], 
                   dtype      : tp.Union[type,str,np.dtype], \
                   do_extend  : bool = False, \
                   use_memmap : bool = False ):
    
    assert isinstance( use_memmap, bool )
    
    if use_memmap is True:
        generator = empty_memmap
    else:
        generator = np.empty
    
    arr = create_aligned_numpy( shape, dtype, generator, do_extend )
    if __debug__:
        if np.issubdtype( arr.dtype, np.integer ):
            arr.fill( np.iinfo(arr.dtype).max )
        else:
            arr.fill( np.nan )
    return arr


def aligned_zeros( shape, dtype, do_extend=False ):
    return create_aligned_numpy( shape, dtype, np.zeros, do_extend )


def aligned_ones( shape, dtype, do_extend=False ):
    return create_aligned_numpy( shape, dtype, np.ones, do_extend )


def aligned_empty_like( other, do_extend=False ):
    arr = create_aligned_numpy( other.shape or other.size, other.dtype, np.empty, do_extend )
    if __debug__:
        if np.issubdtype( arr.dtype, np.integer ):
            arr.fill( np.iinfo(arr.dtype).max )
        else:
            arr.fill( np.nan )
    return arr


def aligned_zeros_like( other, do_extend=False ):
    return create_aligned_numpy( other.shape or other.size, other.dtype, np.zeros, do_extend )


def aligned_ones_like( other, do_extend=False ):
    return create_aligned_numpy( other.shape or other.size, other.dtype, np.ones, do_extend )


def as_aligned_array( other, do_extend=False, overlap_value=1 ):

    if do_extend:
        alignment_check = is_array_fully_aligned
    else:
        alignment_check = is_array_start_aligned

    if not alignment_check( other ):
        new_array = aligned_empty_like( other, do_extend )
        
        if (other.ndim == 1) or (other.size == 1):
            new_array[:other.size] = other  # copy 

            if other.size < new_array.size:
                new_array[other.size:] = overlap_value  # fill overlap
        
        elif other.ndim == 2:
            new_array[:,:other.shape[1]] = other

            if other.size < new_array.size:
                new_array[:,:other.shape[1]:] = overlap_value
        else:
            assert(False)

        other = new_array
    
    return other



# -----------------------------------------------------------------------------------------


def check_dtype( dtype ):
    # "Only int32 and float32 are supported by SIMD backend"
    assert( dtype in [np.dtype(np.int32), np.dtype(np.float32)] )
    return dtype


def check_shape( shape ):
    assert( (not isinstance( shape, collections.Sequence) ) or (len(shape) in [0,1,2]) )

    if not isinstance( shape, collections.Sequence):
        shape = int(shape)
    elif len(shape) == 1:
        shape = int(shape[0])
    elif len(shape) == 0:
        shape = 0
    
    return shape


def create_aligned_numpy( shape, dtype, generator, do_extend ):
    dtype = check_dtype( dtype )
    shape = check_shape( shape )
    
    if isinstance( shape, int ):
        arr = create_aligned_array( shape, dtype, generator, do_extend )
    elif len(shape) == 2:
        arr = create_aligned_matrix( shape, dtype, generator, do_extend )
    else:
        assert( False )
    
    assert( do_extend or is_array_start_aligned( arr ) )
    assert( (not do_extend) or is_array_fully_aligned( arr ) )

    return arr


def aligned_slice( pointer_val, size ):
    simd_vector32_byte_size = default_simd_vector32_byte_size()

    byte_shift = pointer_val % simd_vector32_byte_size

    if byte_shift > 0:
        # handle unaligned array
        byte_shift = simd_vector32_byte_size - byte_shift
        assert( 0 == (byte_shift % 4) )

        item_shift = byte_shift // 4

        # change array size
        arr_slice = slice(item_shift, item_shift+size)
    else:
        arr_slice = slice(size)
    
    return arr_slice



def create_aligned_array( size, dtype, generator, do_extend ):
    assert( isinstance(size,int) )

    simd_vector32_size = default_simd_vector32_size()

    # number of simd vectors in array
    simd_size = (size + simd_vector32_size - 1) // simd_vector32_size
    simd_size *= simd_vector32_size

    if do_extend:
        size = simd_size

    # create array + addtional bitshift space
    arr = generator( simd_size + simd_vector32_size - 1, dtype )

    # Check alignment and return valid slice
    arr_slice = aligned_slice( arr.ctypes.data, size )
    
    # get aligned sub array
    aligned_arr = arr[arr_slice]
    #if 0 != (aligned_arr.ctypes.data % default_simd_vector32_byte_size()):
    #    print( "Unsucc: ", arr_slice, arr.ctypes.data, aligned_arr.ctypes.data, size, simd_size, aligned_arr.shape )
    assert( 0 == (aligned_arr.ctypes.data % default_simd_vector32_byte_size()) )

    return aligned_arr



def create_aligned_matrix( shape, dtype, generator, extend_col_size ):
    assert( len(shape) == 2 )

    simd_vector32_size = default_simd_vector32_size()

    n_rows, n_cols = shape
    n_ext_cols = n_cols
    
    # correct number of columns
    n_col_rest = n_cols % simd_vector32_size

    if 0 != n_col_rest:
        if not extend_col_size:
            raise ValueError( "No rowise alignemnt with row size %d possible" % n_cols )
        
        n_ext_cols += simd_vector32_size - n_col_rest
    
    # create array + addtional bitshift space
    arr = generator( n_rows * n_ext_cols + simd_vector32_size - 1, dtype )

    # Check alignment and return valid slice
    arr_slice = aligned_slice( arr.ctypes.data, n_rows * n_ext_cols )
    
    # get aligned sub array
    arr = arr[arr_slice]

    # reshape to matrix
    matrix = arr.reshape( (n_rows, n_ext_cols) )
    assert( matrix.ctypes.data == arr.ctypes.data )
    assert( 0 == (matrix.ctypes.data % default_simd_vector32_byte_size()) )

    assert( matrix.shape[0] == shape[0] )
    assert( matrix.shape[1] == n_ext_cols )
    
    return matrix
