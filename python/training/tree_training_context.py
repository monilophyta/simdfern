# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["TreeTrainingContext"]


import typing as tp
import numpy as np

from ..ctypes_py import IndexedTreeLayer
from .base_training_context import BaseTrainingContext
from .tree_training_observation import BaseTreeTrainingObserver




class TreeTrainingContext( BaseTrainingContext ):

    __ratings : tp.Optional[np.ndarray] = None

    @property
    def ratings(self) -> tp.Union[np.ndarray,tp.NoReturn]:
        return self.__ratings


    @property
    def rating(self) -> tp.Union[int,float,tp.NoReturn]:
        if self.__ratings is not None:
            return self.__ratings.sum()
        return None


    def __init__( self, feature_rows, observer=None ):
        assert (observer is None) or isinstance( observer, BaseTreeTrainingObserver )
        super( TreeTrainingContext, self ).__init__(observer)
        
        self.feature_rows = feature_rows


    # allows hashing of context - mainly debugging purpose
    def get_hashable_seq(self) -> tuple:
        return (super().get_hashable_seq() + (self.__ratings,))


    def add_layer( self, layer : IndexedTreeLayer, 
                      leaf_idx : np.ndarray,
                       ratings : np.ndarray ):
        assert isinstance( layer, IndexedTreeLayer )
        assert isinstance( ratings, np.ndarray )

        assert ratings.size == ( 2**self.num_layers ), "%d != %d" % (ratings.size, 2**self.num_layers)
        
        if self.num_layers > 0:
            assert np.all( (ratings[::2] + ratings[1::2]) <= self.__ratings )
            #assert ratings.size == ( 2**self.num_layers ), "%d != %d" % (ratings.size, 2**self.num_layers)

            # absolute since leaf_idx can become negative
            assert np.all( np.right_shift( np.absolute( leaf_idx ), 1 ) ==  np.absolute( self.leaf_idx ) )

        super().add_layer( layer, leaf_idx )
        self.__ratings = ratings


    def set_layers( self, layers   : list,
                          leaf_idx : np.ndarray, 
                          ratings  : np.ndarray ):

        assert isinstance( ratings, np.ndarray )
        assert ratings.size == len(layers)

        super().set_layers( layers, leaf_idx )
        self.__ratings = ratings
