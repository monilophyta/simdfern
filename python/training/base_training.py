# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = [ "AbstractBaseTrainer", "BaseTrainer", "FeaturePrioritizer" ]


from abc import ABC #, abstractmethod
#from collections.abc import Sequence
import typing as tp
import functools
import logging
import numpy as np

from .logging import create_logger
#from ..data_sampling import ItemWeightSampling
#from ..data_io import AbstractBoxFeatures
from ..classification import ClassificationTree
from ..clmath import mutual_counts
from ..parameter_registration import ParameterRegistry



class AbstractBaseTrainer( ABC ):

    logger  =  None

    __param_registry : ParameterRegistry

    def param_registry( self, *args, **argv ):
        return self.__param_registry(*args, **argv)


    labels       : np.ndarray
    data_weights : np.ndarray

    num_labels   : int

    @property
    def num_data(self) -> int:
        assert self.labels.size == self.data_weights.size
        return self.labels.size
    
    @property
    @functools.lru_cache(maxsize=None)
    def data_magnitude(self) -> int:
        assert self.labels.size == self.data_weights.size
        return int(self.data_weights.sum(dtype=int))


    def __init__( self, logger, param_registry : ParameterRegistry ):
        
        assert isinstance( param_registry, ParameterRegistry )
        self.__param_registry = param_registry
        
        if logger is None:
            logger = create_logger('BaseTrain')
        elif isinstance( logger, str ):
            logger = create_logger( logger )
              
        assert isinstance( logger, logging.Logger )
        self.logger = logger
        self.do_log( "initialized: num_data = %d", self.num_data )


    def set_param( self, key : str, value : tp.Union[str,int,float,bool]):
        self.__param_registry.set_param( key, value )


    def do_log(self, *args):
        self.logger.info( *args )

    def do_debug_log(self, *args):
        self.logger.debug( *args )


    def calc_gini_rating( self, leaf_idx : np.ndarray, 
                               num_leafs : tp.Optional[int] = None,
                                 do_norm : bool = False ):
        label_axis = 1
        
        counts = mutual_counts( X       = leaf_idx,
                                Y       = self.labels, 
                                weights = self.data_weights,
                                num_X   = num_leafs,
                                num_Y   = self.num_labels )
        
        return counts.gini_index( axis = label_axis, do_norm = do_norm )


    def calc_conditional_entropy( self, leaf_idx : np.ndarray, 
                                       num_leafs : tp.Optional[int] = None,
                                         do_norm : bool = False,
                                        use_log2 : bool = False ):
        label_axis = 1
        
        counts = mutual_counts( X       = leaf_idx,
                                Y       = self.labels, 
                                weights = self.data_weights,
                                num_X   = num_leafs,
                                num_Y   = self.num_labels )
        
        return counts.conditional_entropy( axis = label_axis, do_norm = do_norm, use_log2 = use_log2 )



# --------------------------------------------------------------------------------------------------

class BaseTrainer( AbstractBaseTrainer ):


    def __init__( self, labels : np.ndarray,
                  data_weights : np.ndarray,
                        logger, 
                param_registry : ParameterRegistry ):
        """
        Parameters
        ----------
        labels : int32 array [ N ]
            labels of sample vectors
        data_weights : int32 array [ N ]
        logger : None or logger instance
            used for output
        param_registry : instance of ParameterRegistry
        """
        
        assert np.issubdtype( labels.dtype, np.int32 )
        
        assert isinstance( data_weights, np.ndarray )
        assert data_weights.size == labels.size
        assert np.issubdtype( data_weights.dtype, np.int32 )

        self.labels       = labels
        self.data_weights = data_weights
        self.num_labels   = labels.max() + 1

        super(BaseTrainer,self).__init__( logger, param_registry )
    

    # --- For debugging -----------------------------------------------------------------

    @staticmethod
    def classify( layers, feature_channels ):
        classifier = ClassificationTree( layers )
        return classifier.classify( feature_channels )


    def verify_classification( self, train_context, do_throw=True ):
        leaf_idx_to_check = self.classify( train_context.layers, train_context.feature_rows )
        assert train_context.leaf_idx.size == leaf_idx_to_check.size

        check_sum = ( train_context.leaf_idx == leaf_idx_to_check ).sum()
        
        if check_sum != leaf_idx_to_check.size:
            
            if bool(do_throw):
                msg = "Node indices not confirmed by classification: %d deviations within %d node indices" % \
                            ( leaf_idx_to_check.size - check_sum,
                                train_context.leaf_idx.size)
                raise AssertionError(msg)
            
            return False
        return True

#------------------------------------------------------------------------------------------------


class FeaturePrioritizer( object ):

    feature_indices = None

    def __init__(self):
        self.feature_indices = np.array([],dtype=np.int32)


    def add_features(self, new_feature_indices ):
        new_feature_indices = np.unique( new_feature_indices )
        new_feature_indices = np.setdiff1d( new_feature_indices, self.feature_indices, assume_unique=True )
        self.feature_indices = np.concatenate( (self.feature_indices, new_feature_indices), axis=0 )
    

    def get_priority_feature_indices( self, index_assortment=None ):
        if index_assortment is None:
            return self.feature_indices
        
        return np.intersect1d( self.feature_indices, np.unique( index_assortment ), assume_unique=True )
    

    def get_subsequent_feature_indices( self, index_assortment ):
        return np.setdiff1d( np.unique(index_assortment), self.feature_indices, assume_unique=True )
    

    def get_feature_indices( self, index_assortment ):
        return np.concatenate( ( self.get_priority_feature_indices( index_assortment ),\
                                 self.get_subsequent_feature_indices( index_assortment ) ),\
                                 axis=0 )
        
#-----------------------------------------------------------------------------------------------------------

