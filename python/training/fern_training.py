# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = [ "FernTrainer", "FLOS", "FernLayerOptimizationSteps" ]

import typing as tp
from enum import Enum, unique
import logging
import warnings
import itertools as it
#import math
import numpy as np

# event counting in python
from ..debugging import evtcnt

from ..ctypes_py import FernSplitOptimizer, WeightedFernLayer, FeatureRows, project_features
from ..clmath import MultiNodeClassStatistics
from ..clmath import max_unormed_entropy, max_unormed_gini

from ..fern import remove_layer
from ..fern import FernPartitionOptimizer, LineSearchResult

from ..parameter_registration import default_fern_training_params
from ..parameter_registration import ParameterRegistry, ParamKey as pk    # default parameter key

from .base_training import BaseTrainer
from .fern_training_context import FernTrainingContext
from .base_training_observation import BaseTrainingObserver


# -------------------------------------------------------------------------------------------------------

@unique
class FernLayerOptimizationSteps(Enum):
    #SAMPLE_RANDOM_LAYER = 'S'
    TRAIN_NEW_LAYER                    = 'N'

    CLEAR_LAYER_CACHE                  = 'C'

    OPTIMIZE_SPLIT                     = 'S'

    WEIGHT_COORDINATE_LSEARCH          = 'W'

    POWELLS_METHOD                     = 'P'

    WEIGHT_GRADIENT_LSEARCH            = 'G'
    WEIGHT_AND_SPLIT_GRADIENT_LSEARCH  = 'A'
    
    LOGISTIC_REGRESSION                = 'L'

# shortcut
FLOS = FernLayerOptimizationSteps

FERN_PART_OPTIM_SET = frozenset( [FLOS.OPTIMIZE_SPLIT,
                                  FLOS.WEIGHT_COORDINATE_LSEARCH,
                                  FLOS.POWELLS_METHOD,
                                  FLOS.WEIGHT_GRADIENT_LSEARCH,
                                  FLOS.WEIGHT_AND_SPLIT_GRADIENT_LSEARCH,
                                  FLOS.LOGISTIC_REGRESSION] )


# -------------------------------------------------------------------------------------------------------

class FernTrainer( BaseTrainer ):

    objective      : str = "gini"
    regul_strength : float
    randstate      : np.random.RandomState


    def __init__(self, labels : np.ndarray, 
                 data_weights : tp.Optional[np.ndarray], 
                       logger : logging.Logger,
               default_params : tp.Optional[ParameterRegistry] = None,
                    rand_seed : float = pk('fern_trainer.rand_seed'),
                  regul_ratio : float = pk('fern_trainer.regularisation_ratio') ):
        assert isinstance(  logger, logging.Logger )
        if default_params is None:
            default_params = default_fern_training_params.copy()
        super().__init__( labels, data_weights, logger.getChild( "fernT" ), default_params )
        
        # initialize random number sampling
        rand_seed = self.param_registry( rand_seed, int )
        self.randstate = np.random.RandomState( rand_seed )

        # initialize regularisation strength
        regul_ratio = self.param_registry( regul_ratio, float )
        self.set_regularization( regul_ratio )
    
    # -------------------------------------------------------------------------------------------------------------------------

    def set_regularization(self, regul_ratio : float ) -> tp.NoReturn:
        """Determs and sets strength of regularization depending on weight ratio between objective
           and regularisation"""
        assert regul_ratio >= 0
        
        if self.objective == "gini":
            regul_weight = max_unormed_gini( self.data_magnitude, self.num_labels )
        elif self.objective == "entropy":
            regul_weight = max_unormed_entropy( self.data_magnitude, self.num_labels )
        else:
            assert( False )
        self.regul_strength = regul_weight * float(regul_ratio)

    # -------------------------------------------------------------------------------------------------------------------------


    def train_fern( self, train_context : FernTrainingContext, 
                             num_layers : int = pk('train_fern.num_layers'),
               layer_optimization_steps : str = pk('train_fern.optimize.layer_optimization_steps') ) -> np.ndarray:
        """
        Parameters
        ----------
        train_context : instance of FernTrainingContext
        num_layers : int
        layer_optimization_steps : str
            composed of N,C,S,W,G,A,L
            or "" (empty string) for no optimization

        Observer calls:
        ---------------
        notify_start_training( self, train_context, num_layers )
        notify_trained_layer( self, train_context )
        notify_finished_training( self, train_context )
        """
        
        assert isinstance( train_context, FernTrainingContext )
        assert train_context.num_data == self.num_data
        
        num_layers = self.param_registry( num_layers, int )
        assert num_layers > 0

        layer_optimization_steps = self.param_registry( layer_optimization_steps, str )
        do_optimize = len(layer_optimization_steps) > 0

        # output data structures
        node_rating = []

        # observer notification
        train_context.observer.notify_start_training( self, train_context, num_layers )

        # Incremental layer learning
        for lidx in range( train_context.num_layers, num_layers ):
            
            # train layer
            fern_layer, rating = self.train_layer( train_context )
            
            # classification
            n_leaf_idx = fern_layer.classify( train_context.leaf_idx, train_context.feature_rows )

            # check results
            assert np.all( n_leaf_idx >= 0), "Negative (disabled) should not appear during fern training"
            
            # add new layer
            train_context.add_layer( fern_layer, n_leaf_idx, rating )

            # observer notification
            train_context.observer.notify_trained_layer( self, train_context )
            
            # logging
            self.do_log( "... trained layer %d/%d; rating=%r", lidx+1, num_layers, rating )

            if do_optimize is True:
                n_rating = self.optimize_fern( train_context = train_context,
                                    layer_optimization_steps = layer_optimization_steps )
                assert( n_rating <= rating )
                rating = n_rating

            # progress for next loop
            node_rating.append( rating )
        
        # Done

        # keep ratings as an array
        node_rating = np.array( node_rating )

        # observer notification
        train_context.observer.notify_finished_training( self, train_context )
        
        return node_rating


    def optimize_fern( self, train_context : FernTrainingContext,
                  layer_optimization_steps : str = pk('optimize_fern.layer_optimization_steps'),
                max_num_layer_optim_cycles : int = pk('optimize_fern.max_num_layer_optim_cycles')):
        """
        Parameters
        ----------
        train_context : instance of FernTrainingContext
        layer_optimization_steps : str, composed of N,C,S,W,G,A,L,P

        Observer calls:
        ---------------
        notify_start_fern_optimization( train_context, self )
        notify_finished_fern_optimization( train_context, self )
        """
        assert isinstance( train_context, FernTrainingContext )

        def optimization_loop( max_num_layer_optim_cycles : int, train_context : FernTrainingContext):
            # initialize counters and indices
            noimprovement_counter = 0
            last_layer_idx = train_context.num_layers

            for optim_cycle_idx in range(max_num_layer_optim_cycles):
                
                # sample new processing order of layers
                while True:
                    layer_optim_order = self.randstate.permutation(train_context.num_layers)
                    if (len(layer_optim_order) == 1) or (layer_optim_order[0] != last_layer_idx):
                        break
                
                for layer_optim_pos in range(train_context.num_layers):
                    layer_idx = int(layer_optim_order[layer_optim_pos])

                    # some optimiation steps use initialization from previous step
                    layer_improved = False

                    # select layer to be optimized
                    n_layer = train_context.layer( layer_idx )

                    # remove current layer from lead_idx
                    r_leaf_idx = remove_layer( train_context.leaf_idx, layer_idx, train_context.num_layers )
                    assert np.issubdtype( r_leaf_idx.dtype, np.int32 )

                    # initialize partition optimization if required
                    if FERN_PART_OPTIM_SET.intersection( layer_optimization_steps ):
                        fern_part_optimizer = self.create_fern_part_optimizer( train_context, r_leaf_idx )

                    # process optimization steps
                    for optim_step in layer_optimization_steps:
                        
                        if optim_step is FLOS.CLEAR_LAYER_CACHE:
                            success = False
                            n_layer = train_context.layer( layer_idx )

                        elif optim_step is FLOS.TRAIN_NEW_LAYER:
                            success, n_layer = self.retrain_layer( train_context = train_context,
                                                                      r_leaf_idx = r_leaf_idx,
                                                                       layer_idx = layer_idx )
                        
                        elif optim_step is FLOS.OPTIMIZE_SPLIT:
                            success, n_layer = self.optimize_split( train_context = train_context, 
                                                                        optimizer = fern_part_optimizer, 
                                                                        layer_idx = layer_idx,
                                                                       fern_layer = n_layer )

                        elif optim_step is FLOS.WEIGHT_COORDINATE_LSEARCH:
                            # optimization using coordinate descent on selected weight
                            success, n_layer = self.optimize_single_weight( train_context = train_context,
                                                                                optimizer = fern_part_optimizer,
                                                                                layer_idx = layer_idx,
                                                                            fern_layer = n_layer )

                        elif optim_step in {FLOS.WEIGHT_GRADIENT_LSEARCH,FLOS.WEIGHT_AND_SPLIT_GRADIENT_LSEARCH}:
                            # optimization using line search along gradient
                            success, n_layer = self.optimize_along_gradient( train_context = train_context,
                                                                                 optimizer = fern_part_optimizer,
                                                                                 layer_idx = layer_idx,
                                                                                fern_layer = n_layer,
                                                                            optimize_split = optim_step is FLOS.WEIGHT_AND_SPLIT_GRADIENT_LSEARCH )

                        elif optim_step is FLOS.POWELLS_METHOD:
                            # optimization using powells method
                            success, n_layer = self.optimize_with_powells_method( train_context = train_context,
                                                                                      optimizer = fern_part_optimizer,
                                                                                      layer_idx = layer_idx,
                                                                                     fern_layer = n_layer )

                        elif optim_step is FLOS.LOGISTIC_REGRESSION:
                            # optimization of logistic regression
                            success, n_layer = self.optimize_logistic_regression( train_context = train_context,
                                                                                      optimizer = fern_part_optimizer,
                                                                                      layer_idx = layer_idx,
                                                                                     init_layer = n_layer )
                        else:
                            assert False, "Invalid layer optimization step"
                        
                        if success:
                            layer_improved = True
                            success = False

                            # optimized layers are always shifted to the last position in layer list
                            layer_optim_order[layer_idx < layer_optim_order] -= 1
                            layer_idx = train_context.num_layers - 1

                    last_layer_idx = layer_idx
                    
                    # Evaluate success
                    if layer_improved:
                        # mark improvements
                        noimprovement_counter = 0
                    else:
                        noimprovement_counter += 1

                        # check for continue
                        if noimprovement_counter == train_context.num_layers:
                            return
                
                # output post cycle performance
                #print( optim_cycle_idx, " regul_rating = ", train_context.regularized_rating(self.regul_strength), " rating = ", train_context.rating )
        # End optimization function

        # coding for optimization steps
        layer_optimization_steps = self.param_registry( layer_optimization_steps, str )
        layer_optimization_steps = parse_optimization_steps( layer_optimization_steps, num_features = train_context.num_features )
        
        max_num_layer_optim_cycles = self.param_registry( max_num_layer_optim_cycles, int )

        # observer notification
        train_context.observer.notify_start_fern_optimization( train_context, self )
        
        # run optimization loop
        optimization_loop(max_num_layer_optim_cycles, train_context)

        # observer notification
        train_context.observer.notify_finished_fern_optimization( self, train_context )
        
        if __debug__:
            assert self.verify_classification( train_context, do_throw=True )
            self.do_log( "... nodeIdx verified" )

        return train_context.rating

    # ----------------------------------------------------------------------------------
    # layer-wise operations:

    def train_layer( self, train_context : FernTrainingContext ):
        """
        Parameters
        ----------
        train_context : instance of FernTrainingContext

        Observer calls:
        ---------------
        notify_start_layer_training( train_context )
        notify_finished_layer_training( self, weighted_fern_layer, rating )
        """

        assert isinstance( train_context, FernTrainingContext )
        assert train_context.num_data == self.num_data

        train_context.observer.notify_start_layer_training( train_context )
        

        weighted_fern_layer, rating = self.__train_layer( train_context.feature_rows,
                                                          train_context.leaf_idx,
                                                          train_context.num_leafs,
                                                          train_context.observer )

        # observer notification
        train_context.observer.notify_finished_layer_training( self,
                                                               train_context,
                                                               weighted_fern_layer,
                                                               rating )
        
        return ( weighted_fern_layer, rating )


    def retrain_layer( self, train_context : FernTrainingContext,
                                r_leaf_idx : np.ndarray,
                                 layer_idx : int ):
        """
        Parameters
        ----------
        train_context : instance of FernTrainingContext

        Observer calls:
        ---------------
        notify_start_layer_retraining( train_context, layer_idx )
        notify_retrain_improvement( self, train_context )
        notify_finished_layer_retraining( self, weighted_fern_layer, rating )
        """

        assert isinstance( train_context, FernTrainingContext )
        assert train_context.num_data == self.num_data

        # notification start of retraining
        train_context.observer.notify_start_layer_retraining( train_context, layer_idx )
        
        # Number of nodes in next to final layer
        num_next2final_nodes = 1 << ( train_context.num_layers - 1 )

        # train new layer
        n_layer, n_rating = self.__train_layer( feature_rows = train_context.feature_rows, 
                                                    leaf_idx = r_leaf_idx, 
                                                   num_leafs = num_next2final_nodes,
                                                    observer = train_context.observer )
        assert np.sign( n_rating ) == np.sign( train_context.rating )


        # check for improvement
        if n_rating < train_context.rating:
            # improvement accomplished

            # logging
            self.do_log( "... layer %d retraining succsessful: %r better than %r", layer_idx+1, n_rating, train_context.rating )

            # classify with new layer
            n_leaf_idx = n_layer.classify( r_leaf_idx, train_context.feature_rows )

            train_context.replace_layer( layer_idx, n_layer, n_leaf_idx, n_rating )

            # observer notification
            train_context.observer.notify_retrain_improvement( self, train_context )

            # mark improvements
            success = True
        else:
            # log 
            self.do_log( "... layer %d retraining without improvement", layer_idx+1 )

            success = False


        # notification of finished training
        train_context.observer.notify_finished_layer_retraining( self,
                                                                 train_context,
                                                                 n_layer,
                                                                 n_rating )
        return (success, n_layer)


    def optimize_logistic_regression( self, 
                             train_context : FernTrainingContext,
                                 optimizer : FernPartitionOptimizer,
                                 layer_idx : int,
                                init_layer : WeightedFernLayer,
                                  l2weight : float = pk('logistic_regression_algo.l2weight'),
                              optim_method : str   = pk('logistic_regression_algo.optim_method'),
                                 optim_tol : float = pk('logistic_regression_algo.optim_tol'),
                                optim_gtol : float = pk('logistic_regression_algo.optim_gtol'),
                             max_num_iters : int   = pk('logistic_regression_algo.max_num_iters') ):
        """
        Parameters
        ----------
        train_context : instance of FernTrainingContext
        layer_idx : int
            index layer which is retrained. This layer should not be addressed in
            r_leaf_idx
        init_layer : None or instance of WeightedFernLayer
            layer for parameter initialization in logistic regression optimization
        max_num_iters : int
            maximum number of iterations

        Observer calls:
        ---------------
        notify_start_logistic_regression( layer_idx )
        notify_logreg_improvement( self, train_context )
        notify_finished_logistic_regression( self, train_context, n_layer, n_rating )
        """
        assert isinstance( train_context, FernTrainingContext )
        assert train_context.num_data == self.num_data
        assert train_context.num_layers > layer_idx
        
        assert isinstance( optimizer, FernPartitionOptimizer )
        assert optimizer.num_data == self.num_data
        assert optimizer.num_features == train_context.num_features
        assert optimizer.node_idx.max() < 2**(train_context.num_layers-1)

        assert isinstance( init_layer, WeightedFernLayer )

        # logistig regression descent algo parameters
        dlrg_params = dict( fern_layer    = init_layer,
                            max_num_iters = self.param_registry( max_num_iters, int ),
                            l2weight      = self.param_registry( l2weight, float ),
                            optim_method  = self.param_registry( optim_method, str ),
                            optim_tol     = self.param_registry( optim_tol, float ),
                            optim_gtol    = self.param_registry( optim_gtol, float ) )
        assert dlrg_params["max_num_iters"] > 0

        # notification start of retraining
        train_context.observer.notify_start_logistic_regression( layer_idx )

        # run the optimization
        optim_res = optimizer.optimize_logistic_regression( **dlrg_params )

        if optim_res["is_converged"]:
            self.do_log( "... layer %d logreg optimization converged after %d %s iterations", layer_idx+1, optim_res["iter_count"], optim_res["optim_method"] )
        else:
            self.do_log( "... layer %d logreg optimization without convergence after %d %s iterations: %s", 
                         layer_idx+1, optim_res["iter_count"], optim_res["optim_method"], optim_res["status_message"] )

        # extract optimized layer
        n_layer = optim_res["fern_layer"]

        # classify with new layer
        n_leaf_idx = n_layer.classify( optimizer.node_idx, train_context.feature_rows )

        # get rating of new layer (gini index)
        n_rating = self.calc_gini_rating( n_leaf_idx, num_leafs = train_context.num_leafs, do_norm=False )
        n_rating *= -1  # rating is aquivalent to unormalized negativ gini index
        assert np.sign( n_rating ) == np.sign( train_context.rating )

         # check for improvement
        if n_rating < train_context.rating:
            # improvement accomplished

            # logging
            self.do_log( "... layer %d logreg succsessful: %r better than %r", layer_idx+1, n_rating, train_context.rating )
            
            train_context.replace_layer( layer_idx, n_layer, n_leaf_idx, n_rating )

            # observer notification
            train_context.observer.notify_logreg_improvement( self, train_context )
            
            success = True
        else:
            # log 
            self.do_log( "... layer %d logreg without improvement", layer_idx+1 )
            success = False

            # if both ratings are equal, we still update it, but do not count it as improvement
            # COMMENTED OUT: Error source not correctly handled outside this function
            # if n_rating == train_context.rating:
            #     train_context.replace_layer( layer_idx, n_layer, n_leaf_idx, n_rating )
            #     self.do_log( "... accepting parameters with equal rating" )

            #     if __debug__:
            #         assert self.verify_classification( train_context, do_throw=True )
            #         self.do_log( "... nodeIdx verified" )
            
        
        # notification of finished training
        train_context.observer.notify_finished_logistic_regression( self,
                                                                    train_context,
                                                                    n_layer,
                                                                    n_rating )
        return (success, n_layer)
        


    def optimize_split(self, train_context : FernTrainingContext,
                                 optimizer : FernPartitionOptimizer,
                                 layer_idx : int,
                                fern_layer : WeightedFernLayer ) -> tp.Tuple[bool,WeightedFernLayer]:
        """
        Parameters
        ----------
        train_context : instance of FernTrainingContext
        optimizer : instance of FernPartitionOptimizer
        layer_idx : int
            index layer which is retrained. This layer should not be addressed in
            optimizer.node_idx
        fern_layer : instance of WeightedFernLayer
            layer for parameter initialization
        
        Returns
        -------
        bool
            True if estimated parameter set show an improvment
        instance of WeightedFernLayer
            Fern Layer with newly estimated parameters

        Observer calls:
        ---------------
        notify_start_split_optimization( layer_idx )
        notify_split_optimization_improvement( self, train_context )
        notify_finish_split_optimization( self, train_context )
        """
        assert isinstance( train_context, FernTrainingContext )
        assert train_context.num_data == self.num_data
        assert train_context.num_layers > layer_idx
        assert isinstance( fern_layer, WeightedFernLayer )
        assert fern_layer.num_features == train_context.num_features
        assert optimizer.node_idx.max() < 2**(train_context.num_layers-1)

        # notification start of retraining
        train_context.observer.notify_start_split_optimization( layer_idx )

        # optimize split parameter
        optim_res = optimizer.optimize_split( fern_layer = fern_layer )

        # the following might not be a bug but could be caused by numerical problems
        if np.all(fern_layer.feature_weights == train_context.layer(layer_idx).feature_weights) and \
               (optim_res.rating > train_context.rating):
            evtcnt("annoying.fern_training.optimize_split.rating_setback")

        # evaluate new layer
        success = self.evaluate_lsearch_result( train_context, optimizer, optim_res, layer_idx, "split optimization" )

        if success:
            # observer notification
            train_context.observer.notify_split_optimization_improvement( self, train_context )
    
        # return new layer
        fern_layer = optim_res.fern_layer

        # notification of finished training
        train_context.observer.notify_finish_split_optimization( self, train_context )
        
        return (success, fern_layer)


    def optimize_single_weight( self, train_context : FernTrainingContext,
                                          optimizer : FernPartitionOptimizer,
                                          layer_idx : int,
                                         fern_layer : WeightedFernLayer ) -> tp.Tuple[bool,WeightedFernLayer]:
        """
        Parameters
        ----------
        train_context : instance of FernTrainingContext
        optimizer : instance of FernPartitionOptimizer
        layer_idx : int
            index layer which is retrained. This layer should not be addressed in
            optimizer.node_idx
        fern_layer : instance of WeightedFernLayer
            layer for parameter initialization
        
        Returns
        -------
        bool
            True if estimated parameter set show an improvment
        instance of WeightedFernLayer
            Fern Layer with newly estimated parameters

        Observer calls:
        ---------------
        notify_start_coordinate_descent( layer_idx )
        notify_coordinate_descent_improvement( self, train_context )
        notify_finish_coordinate_descent( self, train_context )
        """
        assert isinstance( train_context, FernTrainingContext )
        assert train_context.num_data == self.num_data
        assert train_context.num_layers > layer_idx
        assert isinstance( fern_layer, WeightedFernLayer )
        assert fern_layer.num_features == train_context.num_features
        assert optimizer.node_idx.max() < 2**(train_context.num_layers-1)

        # sample next optimization weight
        weight_idx = optimizer.next_weight_optimization_index( self.randstate )

        # notification start of retraining
        train_context.observer.notify_start_coordinate_descent( layer_idx )

        # optimize single weight
        optim_res = optimizer.optimize_weight( fern_layer = fern_layer,
                                                     cidx = weight_idx )

        if optim_res is None:
            success = False
        else:
            assert optim_res.regul_strength == self.regul_strength

            # evaluate new layer
            success = self.evaluate_lsearch_result( train_context, optimizer, optim_res, layer_idx, "weight optimization" )
            if success:
                # observer notification
                train_context.observer.notify_coordinate_descent_improvement( self, train_context )

            # return new layer
            fern_layer = optim_res.fern_layer
        
        # notification of finished training
        train_context.observer.notify_finish_coordinate_descent( self, train_context )
        
        return (success, fern_layer)


    def optimize_along_gradient(self, train_context : FernTrainingContext,
                                          optimizer : FernPartitionOptimizer,
                                          layer_idx : int,
                                         fern_layer : WeightedFernLayer,
                                     optimize_split : bool = False,
                                 use_regul_gradient : bool = pk('along_gradient_optimization_algo.use_regul_gradient') ) -> tp.Tuple[bool,WeightedFernLayer]:
        """
        Parameters
        ----------
        train_context : instance of FernTrainingContext
        optimizer : instance of FernPartitionOptimizer
        layer_idx : int
            index layer which is retrained. This layer should not be addressed in
            optimizer.node_idx
        fern_layer : instance of WeightedFernLayer
            layer for parameter initialization
        optimize_split : bool
            maximum number of iterations
        use_regul_gradient : bool
            True: include gradient of regularisation into the search direction estimation
            False: Only use the logisitic regression gradient for line search
        
        Returns
        -------
        bool
            True if estimated parameter set show an improvment
        instance of WeightedFernLayer
            Fern Layer with newly estimated parameters

        Observer calls:
        ---------------
        notify_start_gradient_line_search( layer_idx )
        notify_gradient_line_search_improvement( self, train_context )
        notify_finish_gradient_line_search( self, train_context )
        """
        assert isinstance( train_context, FernTrainingContext )
        assert train_context.num_data == self.num_data
        assert train_context.num_layers > layer_idx
        assert isinstance( fern_layer, WeightedFernLayer )
        assert fern_layer.num_features == train_context.num_features
        assert optimizer.node_idx.max() < 2**(train_context.num_layers-1)

        use_regul_gradient = self.param_registry( use_regul_gradient, bool )

        # notification start of retraining
        train_context.observer.notify_start_gradient_line_search( layer_idx )

        # optimize search dimension
        optim_res = optimizer.optimize_direction( fern_layer = fern_layer,
                                          use_regul_gradient = use_regul_gradient,
                                              optimize_split = optimize_split )
        
        if optim_res is None:
            success = False
        else:
            assert optim_res.regul_strength == self.regul_strength

            # evaluate new layer
            success = self.evaluate_lsearch_result( train_context, optimizer, optim_res, layer_idx, "gradient line search" )
            if success:
                # observer notification
                train_context.observer.notify_gradient_line_search_improvement( self, train_context )
        
            # return new layer
            fern_layer = optim_res.fern_layer

        # notification of finished training
        train_context.observer.notify_finish_gradient_line_search( self, train_context )
        
        return (success, fern_layer)


    def optimize_with_powells_method(self, train_context : FernTrainingContext,
                                               optimizer : FernPartitionOptimizer,
                                               layer_idx : int,
                                              fern_layer : WeightedFernLayer,
                                           max_num_iters : int   = pk('powells_method.max_num_iters'),
                                              optim_dtol : float = pk('powells_method.optim_dtol'),
                                         reorthogonalize : bool  = pk('powells_method.reorthogonalize')) -> tp.Tuple[bool,WeightedFernLayer]:
        """
        Parameters
        ----------
        train_context : instance of FernTrainingContext
        optimizer : instance of FernPartitionOptimizer
        layer_idx : int
            index layer which is retrained. This layer should not be addressed in
            optimizer.node_idx
        fern_layer : instance of WeightedFernLayer
            layer for parameter initialization
        max_num_iters : int
            maximum number of iterations
        optim_dtol : float
            threshold for comparison of subsequent direction estimates. If difference falls below
            this threshold the optimization terminates
        reorthogonalize : bool
            True: reorthogonalize direction set before optimization start
            False: old (previously estimated) direction set might be reused
        
        Returns
        -------
        bool
            True if estimated parameter set show an improvment
        instance of WeightedFernLayer
            Fern Layer with newly estimated parameters

        Observer calls:
        ---------------
        notify_start_powells_method( layer_idx )
        notify_powells_method_improvement( self, train_context )
        notify_finish_powells_method( self, train_context )
        """
        assert isinstance( train_context, FernTrainingContext )
        assert train_context.num_data == self.num_data
        assert train_context.num_layers > layer_idx
        assert isinstance( fern_layer, WeightedFernLayer )
        assert fern_layer.num_features == train_context.num_features
        assert optimizer.node_idx.max() < 2**(train_context.num_layers-1)

        max_num_iters = self.param_registry( max_num_iters, int )
        optim_dtol = self.param_registry( optim_dtol, float )
        reorthogonalize = self.param_registry( reorthogonalize, bool )

        # notification start of retraining
        train_context.observer.notify_start_powells_method( layer_idx )

        # optimize search dimension
        optim_res = optimizer.powells_method( fern_layer = fern_layer,
                                           max_num_iters = max_num_iters,
                                              optim_dtol = optim_dtol,
                             enforce_reorthogonalization = reorthogonalize )
        
        assert optim_res.regul_strength == self.regul_strength

        # evaluate new layer
        success = self.evaluate_lsearch_result( train_context, optimizer, optim_res, layer_idx, "powells method" )
        if success:
            # observer notification
            train_context.observer.notify_powells_method_improvement( self, train_context )
    
        # return new layer
        fern_layer = optim_res.fern_layer

        # notification of finished training
        train_context.observer.notify_finish_powells_method( self, train_context )
        
        return (success, fern_layer)

    # ----------------------------------------------------------------------------------
    # Internal:

    def __train_layer( self, feature_rows : FeatureRows,
                             leaf_idx     : np.ndarray,
                             num_leafs    : int,
                             observer ):
        """
        Parameters
        ----------
        feature_rows : instance of ctypes_py.FeatureRows
        leaf_idx     : int32 nd.array
        num_leafs    : integer
        observer     : child of BaseTrainingObserver

        Observer calls:
        ---------------
        notify_lda_result( self, lda_stat, eigenvalues, eigenvectors )
        notify_processed_feature( self, num_improvements, eigen_vector_idx=eidx )
        """
        assert isinstance( feature_rows, FeatureRows )
        num_features = feature_rows.num_features
        num_data     = feature_rows.num_data
        assert isinstance( observer, BaseTrainingObserver)
        assert isinstance( leaf_idx, np.ndarray )
        assert leaf_idx.size == num_data
        assert np.issubdtype( leaf_idx.dtype, np.int32 )
        

        # initialize split optimization
        optimizer = FernSplitOptimizer( node_idx = leaf_idx,
                                       label_idx = self.labels,
                                    data_weights = self.data_weights,
                                       num_nodes = num_leafs,
                                      num_labels = self.num_labels )

        # run lda 
        lda_stat = MultiNodeClassStatistics( nodeIdx = leaf_idx,
                                            classIdx = self.labels,
                                        feature_rows = feature_rows,
                                             weights = self.data_weights,
                                           do_verify = False )
        eigenvalues, eigenvectors = lda_stat.run_lda()
        eigenvalues = eigenvalues.astype(np.float32)
        eigenvectors = eigenvectors.astype(np.float32)
        assert eigenvalues.size <= num_features
        assert eigenvectors.shape[1] == eigenvalues.size     # number of eigenvectors
        assert eigenvectors.shape[0] == num_features         # number of features

        # observer notification
        observer.notify_lda_result( self, lda_stat, eigenvalues, eigenvectors )

        # check for sorted eigenvalues
        self.do_log( "projection estimation on %d lda dimensions", eigenvalues.size )
        self.do_debug_log( "... eigenvalues: %s", eigenvalues )
        assert np.all( eigenvalues[1:] <= eigenvalues[:-1] ), "Order of eigenvalues: %s" % repr(eigenvalues)

        for eidx in range(len(eigenvalues)):
            evect = eigenvectors[:,eidx]
            assert np.isclose( np.linalg.norm(evect), 1.0 )

            # project features on eigenvector
            feature_vals = project_features( evect, feature_rows )
            assert feature_vals.size == self.num_data

            # process feature
            num_improvements = optimizer.process_feature( feature_vals )

            # observer notification
            observer.notify_processed_feature( self, num_improvements, eigen_vector_idx=eidx )

        # Final Result
        indexed_fern_layer, rating = optimizer.get_optimal_split()

        # optimal features index
        optimal_feature_idx = indexed_fern_layer.feature_idx
        self.do_log( "... selected eigenvector %d with rating %d", indexed_fern_layer.feature_idx, rating )
        
        # Optimal split axis
        optimal_weights = eigenvectors[:,optimal_feature_idx]
        
        # Bringing weight with and feature splits together
        weighted_fern_layer = WeightedFernLayer.create( optimal_weights, indexed_fern_layer.feature_split )
        
        return ( weighted_fern_layer, rating )



    def create_fern_part_optimizer( self, train_context : FernTrainingContext,
                                             r_leaf_idx : np.ndarray,
                                           logreg_dtype : str   = pk('logistic_regression_algo.dtype'),
                                           weight_scale : float = pk('logistic_regression_algo.weight_scale'),
                                                    eps : float = pk('logistic_regression_algo.eps') ) -> FernPartitionOptimizer:
        
        assert isinstance( train_context, FernTrainingContext )
        assert train_context.num_data == self.num_data
        assert np.all( r_leaf_idx < (train_context.num_leafs - 1) )
        assert np.all( r_leaf_idx >= 0 )

        logreg_dtype = np.dtype( self.param_registry( logreg_dtype, str ) )
        weight_scale = self.param_registry( weight_scale, float )
        eps          = self.param_registry( eps, float )

        assert np.issubdtype( logreg_dtype, np.floating )
        assert weight_scale > 0
        assert eps > 0

        # initialize fern partition optimizer
        part_optimizer = FernPartitionOptimizer( feature_rows = train_context.feature_rows,
                                                    label_idx = self.labels,
                                                     node_idx = r_leaf_idx,
                                               sample_weights = self.data_weights,
                                                   num_labels = self.num_labels,
                                                    num_leafs = 2**(train_context.num_layers - 1),
                                               regul_strength = self.regul_strength )
        return part_optimizer



    def evaluate_lsearch_result( self, train_context : FernTrainingContext,
                                           optimizer : FernPartitionOptimizer,
                                           optim_res : LineSearchResult,
                                           layer_idx : int,
                                        optim_method : str ):
        
        if __debug__:
            # check that old layer classification is correct
            c_leaf_idx = train_context.layer(layer_idx).classify( optimizer.node_idx, train_context.feature_rows )
            c_rating = -1 * self.calc_gini_rating( c_leaf_idx, num_leafs = train_context.num_leafs, do_norm=False )
            assert c_rating == train_context.rating, "Failed rating check: %r != %r" % (c_rating, train_context.rating)

            # check that new layer classification is correct
            c_leaf_idx = optim_res.fern_layer.classify( optimizer.node_idx, train_context.feature_rows )
            c_rating = -1 * self.calc_gini_rating( c_leaf_idx, num_leafs = train_context.num_leafs, do_norm=False )
            check = (optim_res.num_invalid_candidates > 0) or (c_rating == optim_res.rating)
            if not check:
                evtcnt("annoying.fern_training.evaluate_lsearch_result.debug.rating_unequalness")

        # initialize with no improvement
        success = False

        # check if improvements where made
        if optim_res.regularized_rating < train_context.regularized_layer_rating(layer_idx, self.regul_strength):
            
            # mark improvement
            success = True

            # classify with new layer
            if __debug__:
                n_leaf_idx = c_leaf_idx
            else:
                n_leaf_idx = optim_res.fern_layer.classify( optimizer.node_idx, train_context.feature_rows )

            if (optim_res.num_invalid_candidates > 0) or __debug__:
                # In case of invalid candidates the optim_res rating might not be trustable
                n_rating = -1 * self.calc_gini_rating( n_leaf_idx, num_leafs = train_context.num_leafs, do_norm=False )

                if n_rating != optim_res.rating:
                    # Wrong rating due to invalid line search candidates or numeric resolution isues
                    evtcnt("annoying.fern_training.evaluate_lsearch_result.rating_unqualness")
                    optim_res.rating = n_rating

                # check if optimization is still valid
                if optim_res.regularized_rating >= train_context.regularized_layer_rating(layer_idx, self.regul_strength):
                    # no optimization anymore
                    success = False

        if success:
            self.do_log(  "... %s improved layer %d", optim_method, layer_idx+1 )

            # replace with new layer
            train_context.replace_layer( layer_idx, optim_res.fern_layer, n_leaf_idx, optim_res.rating )
        else:
            self.do_log(  "... %s no improvement for layer %d", optim_method, layer_idx+1 )
        
        return success


# -----------------------------------------------------------------------------------------------------------------------

COORDINATE_OPTIM_SET = {FLOS.WEIGHT_AND_SPLIT_GRADIENT_LSEARCH,FLOS.WEIGHT_COORDINATE_LSEARCH,FLOS.OPTIMIZE_SPLIT}


def parse_optimization_steps( optim_steps : str, num_features : int ) -> list:
    optim_enum_list = []
    optim_iter_num = []
    n_iters = 0

    optim_steps = optim_steps.upper()

    idx = -1
    while True:
        idx += 1
        if idx >= len(optim_steps):
            break
        s = optim_steps[idx]

        if s.isalpha():
            optim_enum_list.append( [FLOS(s)] )
            optim_iter_num.append( max(n_iters,1) )
            n_iters = 0
        elif s == '[':
            cidx = optim_steps.find("]", idx+1)
            if cidx > idx:
                optim_enum_list.append( parse_optimization_steps( optim_steps[idx:cidx], num_features ) )
                optim_iter_num.append( max(n_iters,1) )
                n_iters = 0
                idx = cidx
    
        elif s.isdigit():
            n_iters *= 10
            n_iters += int(s)
        elif s == '*':
            assert all([ True for x in optim_enum_list[-1] if x in COORDINATE_OPTIM_SET ])
            n_iters = num_features
        else:
            assert False
        
    optim_iter_num.append(max(n_iters,1))
    optim_iter_num = optim_iter_num[1:]

    assert len(optim_enum_list) == len(optim_iter_num)

    optim_enum_list = sum( [ n * s for n,s in zip(optim_iter_num, optim_enum_list) ], [] )
    #print( "optim_list: ", "".join( [x.value for x in optim_enum_list] ) )

    return optim_enum_list



