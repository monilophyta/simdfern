# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["SampleFeatureContext"]

# ---------------------------------------------------------------------------------------------------------------

import typing as tp
import logging
from collections.abc import Sequence
import numpy as np

from ..ctypes_py import FeatureRows, create_feature_rows
from ..data_io import AbstractBoxFeatures


# ------------------------------------------------------------------------------------------
# logging

# create logger
logger = logging.getLogger('training.base_data_context')

if __debug__:
    logger.setLevel(logging.INFO)
else:
    logger.setLevel(logging.WARN)


# ---------------------------------------------------------------------------------------------------------------


class SampleFeatureContext( object ):
    # ----------------------------------------
    # Data sample features

    __feature_rows : FeatureRows


    def __init__( self ):
        self.__feature_rows = create_feature_rows()


    def __get_feature_rows(self) -> FeatureRows:
        return self.__feature_rows
    
    
    def __set_feature_rows(self, feature_rows : tp.Union[FeatureRows,list,np.ndarray]):
        if isinstance( feature_rows, FeatureRows ):
            self.__feature_rows = feature_rows
        else:
            feature_rows = self.check_feature_rows( feature_rows )
            assert isinstance( feature_rows, list )
            self.__feature_rows = create_feature_rows( len(feature_rows[0]), init_feature_row_capacity=len(feature_rows) )
            self.__feature_rows.extend( feature_rows )
    

    feature_rows = property(__get_feature_rows, __set_feature_rows)

    @property
    def num_features(self) ->  int:
        return self.__feature_rows.num_features
    

    @property
    def num_data(self) -> int:
        return self.__feature_rows.num_data

    
    @staticmethod
    def check_feature_rows( feature_rows : tp.Union[FeatureRows,list,np.ndarray] ) -> list:
        """
        Parameters
        ----------
        feature_rows : float matrix [ nF x N ] or nf * [ array(N) ]
            features of samples: feature_rows[:,n] nf * [ array(N)[n] ]
        """

        if isinstance( feature_rows, np.ndarray ):
            assert feature_rows.ndim == 2
            assert np.issubdtype( feature_rows.dtype, np.float32 ) is True
            
            # Decompose into rows
            feature_rows = [ feature_rows[i,:] for i in range( feature_rows.shape[0] ) ]
        elif isinstance( feature_rows, (AbstractBoxFeatures,Sequence) ):
            feature_rows = list(feature_rows)
            
            if len(feature_rows) > 0:
                num_data = len(feature_rows[0])
                assert np.all( np.array([ x.size for x in feature_rows ]) == num_data )
        else:
            assert False, "Unknown type"

        return feature_rows

    # allows hashing of context
    def get_hashable_seq(self) -> tuple:
        return tuple( self.feature_rows.feature_rows )
    
    # --- prevent undefined behavior caused by pickling
    def __getstate__(self):
        # No pickling of this instance
        if __debug__:
            print("SampleFeatureContext not pickled")
            logger.warning( "SampleFeatureContext not pickled" )
        return False
    
    def __setstate__(self, state):
        pass

# ---------------------------------------------------------------------------------------------------------------

