# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



__all__ = [ "DummyTreeTrainingObserver", \
            "BaseCountingTreeTrainingObserver", \
            "CountingTreeTrainingObserver", "VerboseTreeTrainingObserver" ]


import numpy as np

from .base_training_observation import BaseTreeTrainingObserver, ONS
from .base_training_observation import BaseCountingTrainingObserver

# ----------------------------------------------------------------------------------------


class DummyTreeTrainingObserver( BaseTreeTrainingObserver ):
    pass


# ----------------------------------------------------------------------------------------


class BaseCountingTreeTrainingObserver( BaseTreeTrainingObserver, BaseCountingTrainingObserver ):


    def notify_start_training( self, trainer, train_context, num_layers ):
        super().notify_start_training( trainer, train_context, num_layers )
        self._initialize( record_gini_index = self.is_recording_gini_index, 
                  record_mutual_information = self.is_recording_mutual_information )


    def notify_layer_classification( self, trainer, train_context, feature_priotizer ):
        super().notify_layer_classification( trainer, train_context, feature_priotizer )

        self._record_time()
        self.measure_source.append( ONS.POST_TREE_LAYER_TRAINING )

        self._num_layers.append( train_context.num_layers )
        self.ratings.append( train_context.ratings )
        self._update_informations_measures( trainer, train_context )



class CountingTreeTrainingObserver( BaseCountingTreeTrainingObserver ):

    def __init__(self, record_gini_index : bool = False,
               record_mutual_information : bool = False ):
        
        self._initialize( record_gini_index = record_gini_index, 
                  record_mutual_information = record_mutual_information )


    def notify_layer_classification( self, trainer, train_context, feature_priotizer ):
        super().notify_layer_classification( trainer, train_context, feature_priotizer )

        if __debug__ and (len(self.measure_source) > 1):
            improved_measure =     (self.mutual_inf_measure[-1] >= self.mutual_inf_measure[-2]) \
                                or (self.gini_measure[-1] >=  self.gini_measure[-2])
            assert improved_measure

#---------------------------------------------------------------------------------------------------

class VerboseTreeTrainingObserver( CountingTreeTrainingObserver ):

    out = None


    def __init__(self, out, record_gini_index : bool = False,
                    record_mutual_information : bool = False ):
        super().__init__( record_gini_index, record_mutual_information )
        self.out = out


    def notify_start_layer_training( self, trainer, train_context ):
        super().notify_start_layer_training( trainer, train_context )
        self.out.write( "Tree layer training: ")


    def notify_processed_feature( self, trainer, train_context, optimizer, num_improvements, feature_idx ):
        super().notify_processed_feature( trainer, train_context, optimizer, num_improvements, feature_idx )
        if num_improvements > 0:
            self.out.write("+(%d:%d)" % (feature_idx, num_improvements) )
        else:
            self.out.write(".")
        self.out.flush()


    def notify_finished_layer_training( self, trainer, layer, rating ):
        super().notify_finished_layer_training( trainer, layer, rating )
        self.out.write("\n")


    def notify_layer_classification( self, trainer, train_context, feature_priotizer ):
        super().notify_layer_classification( trainer, train_context, feature_priotizer )
        self.out.write("Number of used features: %d\n" % len( feature_priotizer.get_priority_feature_indices() ) )





