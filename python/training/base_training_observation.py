# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = [ "BaseTrainingObserver", "DummyTrainingObserver", 
            "BaseTreeTrainingObserver", "BaseFernTrainingObserver",
            "TestingObserver",
            "BaseCombinedTrainingObserver",
            "BaseCountingTrainingObserver",
            "ObserverNotificationSource", "ONS"]

# -----------------------------------------------------------------------------------------------------------------------



#from abc import ABC
from enum import IntEnum, auto
import typing as tp
import time

import numpy as np

#from ..ctypes_py import FernSplitOptimizer
from ..clmath import mutual_counts
from ..ctypes_py import FeatureRows

from .base_data_context import SampleFeatureContext


# -----------------------------------------------------------------------------------------------------------------------


class ObserverNotificationSource(IntEnum):

    # ----Tree Training Source ----------------------
    POST_TREE_LAYER_TRAINING            = auto()

    # ----Fern Training Source ----------------------
    FERN_RETRAIN_IMPROVEMENT            = auto()
    FERN_LOGREG_IMPROVEMENT             = auto()
    POST_FERN_LAYER_TRAINING            = auto()
    #POST_FERN_LAYER_OPTIMIZATION        = auto()

    # ----Combined Training Source ----------------------
    INITIALLY_TRAINED_FERN              = auto()
    IMPROVED_FERN                       = auto()
    CANDIDATE_LAYER_TRAINING            = auto()
    CANDIDATE_FERN_OPTIMIZATION         = auto()

    ###### Later Added ##############################
    # ----Fern Training Source ----------------------
    FERN_WEIGHT_SPLIT_OPTIMIZATION_IMPROVEMENT = auto()
    FERN_WEIGHT_COORDINATE_LSEARCH_IMPROVEMENT = auto()
    FERN_WEIGHT_GRADIENT_LSEARCH_IMPROVEMENT   = auto()
    FERN_POWELLS_METHOD_IMPROVEMENT            = auto()


# Shortcut
ONS = ObserverNotificationSource

# -----------------------------------------------------------------------------------------------------------------------


class BaseTrainingObserver( object ):

    def notify_start_training( self, *args, **argv ):
        pass
    def notify_finished_training( self, *args, **argv ):
        pass
    def notify_start_layer_training( self, *args, **argv ):
        pass
    def notify_finished_layer_training( self, *args, **argv ):
        pass
    def notify_processed_feature( self, *args, **argv ):
        pass
    
# -----------------------------------------------------------------------------------------------------------------------


class DummyTrainingObserver( BaseTrainingObserver ):

    __dummy_notification = lambda *args, **argv: None

    calls : set

    def __init__(self):
        self.calls = set()

    def __getattr__(self, name):
        prefix = "notify"

        if (len(name) >= len(prefix)) and (name[:len(prefix)] == prefix):
            #print( "Called observing: ", name)
            self.calls.add(name)
            return self.__dummy_notification

# ----------------------------------------------------------------------------------------


class BaseTreeTrainingObserver( BaseTrainingObserver ):

    def notify_layer_classification( self, *args, **argv ):
        pass


# ----------------------------------------------------------------------------------------



class BaseFernTrainingObserver( BaseTrainingObserver ):

    # fern_training.train_fern
    def notify_trained_layer( self, *args, **argv ):
        pass

    # fern_training.__train_layer
    def notify_lda_result( self, *args, **argv ):
        pass

    # fern_training.retrain_layer
    def notify_start_layer_retraining( self, *args, **argv ):
        pass
    def notify_retrain_improvement( self, *args, **argv ):
        pass
    def notify_finished_layer_retraining( self, *args, **argv ):
        pass
    
    # fern_training.optimize_fern
    def notify_start_fern_optimization( self, *args, **argv ):
        pass
    def notify_finished_fern_optimization( self, *args, **argv ):
        pass

    # descend_layer_gradient
    def notify_start_logistic_regression( self, *args, **argv ):
        pass
    def notify_logreg_improvement( self, *args, **argv ):
        pass
    def notify_finished_logistic_regression( self, *args, **argv ):
        pass


    # optimize_split
    def notify_start_split_optimization( self, *args, **argv ):         #( layer_idx ):
        pass
    def notify_split_optimization_improvement( self, *args, **argv ):   #( self, train_context ):
        pass
    def notify_finish_split_optimization( self, *args, **argv ):        #( self, train_context ):
        pass

    # optimize_single_weight
    def notify_start_coordinate_descent( self, *args, **argv ):        #( layer_idx ):
        pass
    def notify_coordinate_descent_improvement( self, *args, **argv ):  #( self, train_context ):
        pass
    def notify_finish_coordinate_descent( self, *args, **argv ):       #( self, train_context ):
        pass

    # optimize_along_gradient
    def notify_start_gradient_line_search( self, *args, **argv ):       # ( layer_idx ):
        pass
    def notify_gradient_line_search_improvement( self, *args, **argv ): #( self, train_context ):
        pass
    def notify_finish_gradient_line_search( self, *args, **argv ):      #( self, train_context ):
        pass

    # optimize with powells method
    def notify_start_powells_method( self, *args, **argv ):       # ( layer_idx ):
        pass
    def notify_powells_method_improvement( self, *args, **argv ): #( self, train_context ):
        pass
    def notify_finish_powells_method( self, *args, **argv ):      #( self, train_context ):
        pass

# ----------------------------------------------------------------------------------------


class BaseCombinedTrainingObserver( BaseFernTrainingObserver ):

    def notify_start_combined_training( self, *args, **argv ):
        pass
    def notify_selected_features( self, *args, **argv ):
        pass
    def notify_initially_trained_fern( self, *args, **argv ):
        pass
    def notify_candidate_feature_selection( self, *args, **argv ):
        pass
    def notify_candidate_layer_training( self, *args, **argv ):
        pass
    def notify_candidate_fern_optimization( self, *args, **argv ):
        pass
    def notify_improved_fern( self, *args, **argv ):
        pass
    def notify_finished_combined_training( self, *args, **argv ):
        pass



# -------------------------------------------------------------------------------------------------

class BaseAdvancedObserver( object ):

    _mutual_inf_measure  : tp.Optional[list]
    _gini_measure        : tp.Optional[list]


    @property
    def mutual_inf_measure(self):
        return np.array( self._mutual_inf_measure )

    @property
    def gini_measure(self):
        return np.array( self._gini_measure )

    # ---------------------------------------------------------------------------------

    @property
    def is_recording_gini_index(self):
        has_gini_index = hasattr(self, '_gini_measure')
        return (has_gini_index and ( self._gini_measure is not None) )

    @property
    def is_recording_mutual_information(self):
        has_mutual_inf_measure = hasattr(self, '_mutual_inf_measure')
        return (has_mutual_inf_measure and (self._mutual_inf_measure is not None) )

    @property
    def num_measures(self) -> int:
        if self.is_recording_gini_index:
            return len(self._gini_measure)
        if self.is_recording_mutual_information:
            return len(self._mutual_inf_measure)
        return 0
    
    # ---------------------------------------------------------------------------------

    def _initialize(self, record_gini_index : bool,
                  record_mutual_information : bool ):

        self._mutual_inf_measure = None
        self._gini_measure = None

        if bool(record_gini_index):
            self._gini_measure = list()
        
        if bool(record_mutual_information):
            self._mutual_inf_measure = list()
    

    def _initialize_with( self, other, shallow_copy : bool = False ):
        assert isinstance( other, BaseAdvancedObserver )
        
        if bool(shallow_copy):
            copy = lambda x: x
        else:
            copy = lambda x: ((x is not None) and list(x)) or x
        
        self._mutual_inf_measure = copy( other._mutual_inf_measure )
        self._gini_measure       = copy( other._gini_measure )

    
    # ---------------------------------------------------------------------------------

    def _update_informations_measures( self, labels, leaf_idx, data_weights ):

        mcounts = mutual_counts( labels, np.absolute( leaf_idx ), weights=data_weights )
        
        if self.is_recording_mutual_information:
            self._mutual_inf_measure.append( mcounts.mutual_information(do_norm=False) )
        
        if self.is_recording_gini_index:
            self._gini_measure.append( mcounts.gini_index( axis=0, do_norm=True ) )

# -------------------------------------------------------------------------------------------------


class BaseCountingTrainingObserver( BaseAdvancedObserver ):

    _num_layers         : list
    ratings             : list
    time_ticks          : list
    measure_source      : list

    @property
    def num_layers(self):
        return np.array( self._num_layers, dtype=np.int32 )
    
    # ---------------------------------------------------------------------------------

    def _initialize(self, record_gini_index : bool,
                  record_mutual_information : bool ):

        super()._initialize( record_gini_index, record_mutual_information )

        self._num_layers = list()
        self.ratings = list()
        self.time_ticks = list()
        self.measure_source = list()
    

    def _initialize_with( self, other, shallow_copy : bool = False ):
        assert isinstance( other, BaseCountingTrainingObserver )
        super()._initialize( other, shallow_copy )
        
        if bool(shallow_copy):
            copy = lambda x: x
        else:
            copy = lambda x: ((x is not None) and list(x)) or x
        
        self._num_layers         = copy( other.num_layers )
        self.ratings             = copy( other.ratings )
        self.time_ticks          = copy( other.time_ticks )
        self.measure_source      = copy( other.measure_source )
        
    # ---------------------------------------------------------------------------------

    def _update_informations_measures( self, trainer, train_context ):
        
        if self.is_recording_gini_index or self.is_recording_mutual_information:
            super()._update_informations_measures( trainer.labels,
                                                   train_context.leaf_idx,
                                                   trainer.data_weights )

    def _record_time(self):
        self.time_ticks.append( time.process_time() )


# -------------------------------------------------------------------------------------------------

class TestingObserver( BaseAdvancedObserver, SampleFeatureContext ):

    labels       : np.ndarray
    data_weights : tp.Optional[np.ndarray] = None

    num_labels   : int

    # ---------------------------------------------------------------------------------

    def __init__(self):
        super().__init__()

    # allows hashing of context
    def get_hashable_seq(self) -> tuple:
        return (super().get_hashable_seq() + ( self.labels, self.data_weights, self.num_labels ))

    # ---------------------------------------------------------------------------------

    def initialize(self, feature_rows : tp.Union[FeatureRows,list,np.ndarray],
                               labels : np.ndarray,
                         data_weights : tp.Optional[np.ndarray],
                    record_gini_index : bool,
            record_mutual_information : bool ):
        
        super()._initialize( record_gini_index, record_mutual_information )
        assert bool(record_gini_index) == self.is_recording_gini_index
        assert bool(record_mutual_information) == self.is_recording_mutual_information
        
        self.feature_rows = feature_rows

        assert isinstance( labels, np.ndarray )
        assert np.issubdtype( labels.dtype, np.integer )
        assert self.num_data == labels.size
        self.labels = labels.astype(np.int32)
        self.num_labels = self.labels.max() + 1

        if data_weights is not None:
            assert isinstance( data_weights, np.ndarray )
            assert self.num_data == data_weights.size
            assert np.issubdtype( data_weights.dtype, np.int32 )
            self.data_weights = data_weights


    def initialize_with( self, other, shallow_copy : bool = False ):
        assert isinstance( other, TestingObserver )
        super()._initialize( other, shallow_copy )

        # no copy on test data
        self.feature_rows = other.feature_rows
        self.labels       = other.labels
        self.data_weights = other.data_weights
        self.num_labels   = other.num_labels
    
    
    # ---------------------------------------------------------------------------------

    def update_informations_measures( self, train_context ):
        
        if self.is_recording_gini_index or self.is_recording_mutual_information:

            # classifiy test data
            t_leaf_idx = np.zeros( self.num_data, dtype=np.int32 )

            for layer in train_context.layers:
                t_leaf_idx = layer.classify( t_leaf_idx, self.feature_rows )

            # update statistics
            super()._update_informations_measures( self.labels, t_leaf_idx, self.data_weights )

    # ---------------------------------------------------------------------------------
