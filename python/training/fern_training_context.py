# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["FernTrainingContext"]


import typing as tp
import numpy as np
from ..ctypes_py import WeightedFernLayer #, IndexedFernLayer
from .base_training_context import BaseTrainingContext
from .base_training_observation import BaseFernTrainingObserver




class FernTrainingContext( BaseTrainingContext ):

    __rating : tp.Union[int,float] = None


    @property
    def rating(self) -> tp.Union[int,float,tp.NoReturn]:
        return self.__rating
    
    @property
    def centralized_l1norm(self) -> float:
        return float(np.array( [ layer.centralized_l1norm for layer in self.layers ], dtype=np.float32 ).sum())


    def __init__( self, observer=None ):
        assert (observer is None) or isinstance( observer, BaseFernTrainingObserver )
        super( FernTrainingContext, self ).__init__(observer)


    def regularized_rating(self, regul_strength : float) -> float:
        return (float(self.rating) + (regul_strength * self.centralized_l1norm))

    def regularized_layer_rating(self, layer_idx : int, 
                                  regul_strength : float) -> float:
        return (float(self.rating) + (regul_strength * self.layers[layer_idx].centralized_l1norm))

    # allows hashing of context - mainly debugging purpose
    def get_hashable_seq(self) -> tuple:
        return (super().get_hashable_seq() + (self.__rating,))

    # -----------------------------------------
    # # node idx and Classifier Layers

    def add_layer( self, layer : WeightedFernLayer, 
                      leaf_idx : np.ndarray,
                        rating : tp.Union[int,float] ):

        assert isinstance( layer, WeightedFernLayer )
        assert np.all( leaf_idx >= 0 ), "fern layers should not be used with disabled (negative) nodes"   # reason: if data gets disables, the classification task gets easier

        if self.num_layers > 0:
            assert rating <= self.__rating
            assert np.all( np.right_shift( leaf_idx, 1 ) == self.leaf_idx )

        super().add_layer( layer, leaf_idx )
        self.__rating = rating


    def replace_layer( self, layer_idx : int,
                               n_layer : WeightedFernLayer,
                            n_leaf_idx : np.ndarray,
                              n_rating : tp.Union[int,float] ):
        
        assert isinstance( n_layer, WeightedFernLayer )
        
        assert layer_idx >= 0
        assert layer_idx < self.num_layers

        n_layers = self.layers[:layer_idx] + self.layers[(layer_idx+1):]
        n_layers.append( n_layer )
        
        self.set_layers( n_layers, n_leaf_idx, n_rating )


    def set_layers( self, layers   : list,
                          leaf_idx : np.ndarray, 
                          rating   : tp.Union[int,float] ):

        assert np.all( self.leaf_idx >= 0 ), "fern layers should not be used with disabled (negative) nodes"   # reason: if data gets disables, the classification task gets easier
        
        super().set_layers( layers, leaf_idx )
        self.__rating = rating
    
    # ----------------------------------------------------------------------------------------------------------------------

