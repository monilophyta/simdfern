
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



__all__ = [ "DummyFernTrainingObserver", \
            "BaseCountingFernTrainingObserver", \
            "CountingFernTrainingObserver", "VerboseFernTrainingObserver" ]

import typing as tp
from abc import ABCMeta #, abstractmethod
import numpy as np

from ..ctypes_py import FeatureRows

from .fern_training import FernTrainer
#from ..clmath import mutual_counts
from .base_training_observation import BaseFernTrainingObserver, ONS
from .base_training_observation import BaseCountingTrainingObserver
from .base_training_observation import TestingObserver


# ----------------------------------------------------------------------------------------


class DummyFernTrainingObserver( BaseFernTrainingObserver ):
    pass

# ----------------------------------------------------------------------------------------


class BaseCountingFernTrainingObserver( BaseFernTrainingObserver, BaseCountingTrainingObserver, metaclass=ABCMeta ):

    regularization   : list
    testing_observer : tp.Optional[TestingObserver] = None

    def total_rating(self, idx : int):
        assert len(self.regularization) == len(self.ratings)
        return (float(self.ratings[idx]) + self.regularization[idx])
    
    @property
    def total_ratings(self):
        assert len(self.regularization) == len(self.ratings)
        return (np.array(self.ratings, dtype=np.float32) + np.array( self.regularization, dtype=np.float32))


    def _initialize(self, record_gini_index : bool,
                  record_mutual_information : bool ):
        super()._initialize( record_gini_index, record_mutual_information )
        self.regularization = list()
    

    def _initialize_with( self, other, shallow_copy : bool = False ):
        super()._initialize_with( other, shallow_copy )
        
        if bool(shallow_copy):
            copy = lambda x: x
        else:
            copy = lambda x: ((x is not None) and list(x)) or x
        
        self.regularization = copy( other.regularization )


    def set_test_data(self, feature_rows : tp.Union[FeatureRows,list,np.ndarray],
                                  labels : np.ndarray,
                            data_weights : tp.Optional[np.ndarray] = None ):
        
        self.testing_observer = TestingObserver()
        self.testing_observer.initialize( feature_rows = feature_rows,
                                                labels = labels,
                                          data_weights = data_weights,
                                     record_gini_index = self.is_recording_gini_index,
                             record_mutual_information = self.is_recording_mutual_information )
    

    def generic_notification( self, trainer, train_context, notify_source ):
        self._record_time()
        self.measure_source.append( notify_source )

        self._num_layers.append( train_context.num_layers )
        self.ratings.append( train_context.rating )
        self.regularization.append( trainer.regul_strength * train_context.centralized_l1norm )
        self._update_informations_measures( trainer, train_context )

        if self.testing_observer is not None:
            self.testing_observer.update_informations_measures( train_context )


    def notify_trained_layer( self, trainer, train_context ):
        super().notify_trained_layer( trainer, train_context )
        self.generic_notification( trainer, train_context, ONS.POST_FERN_LAYER_TRAINING )


    def notify_retrain_improvement( self, trainer, train_context ):
        super().notify_retrain_improvement( trainer, train_context )
        self.generic_notification( trainer, train_context, ONS.FERN_RETRAIN_IMPROVEMENT )


    def notify_logreg_improvement( self, trainer, train_context ):
        super().notify_logreg_improvement( trainer, train_context )
        self.generic_notification( trainer, train_context, ONS.FERN_LOGREG_IMPROVEMENT )


    def notify_split_optimization_improvement( self, trainer, train_context ):
        super().notify_split_optimization_improvement( trainer, train_context )
        self.generic_notification( trainer, train_context, ONS.FERN_WEIGHT_SPLIT_OPTIMIZATION_IMPROVEMENT )

    def notify_coordinate_descent_improvement( self, trainer, train_context ):
        super().notify_coordinate_descent_improvement( trainer, train_context )
        self.generic_notification( trainer, train_context, ONS.FERN_WEIGHT_COORDINATE_LSEARCH_IMPROVEMENT )

    def notify_gradient_line_search_improvement( self, trainer, train_context ):
        super().notify_gradient_line_search_improvement( trainer, train_context )
        self.generic_notification( trainer, train_context, ONS.FERN_WEIGHT_GRADIENT_LSEARCH_IMPROVEMENT )

    def notify_powells_method_improvement( self, trainer, train_context ):
        super().notify_powells_method_improvement( trainer, train_context )
        self.generic_notification( trainer, train_context, ONS.FERN_POWELLS_METHOD_IMPROVEMENT )

    def get_observations(self):

        stats = { "num_layers" : self.num_layers,
                  "time_ticks" : np.asarray( self.time_ticks ),
              "measure_source" : np.asarray( self.measure_source, dtype=np.int32 ),
                "train_rating" : np.asarray( self.ratings )  }
        
        if self.is_recording_gini_index:
            stats["train_gini_index"] = self.gini_measure
        
        if self.is_recording_mutual_information:
            stats["train_mutual_inf"] = self.mutual_inf_measure
        
        if self.testing_observer is not None:
            
            if self.testing_observer.is_recording_gini_index:
                stats["test_gini_index"] = self.testing_observer.gini_measure
            
            if self.testing_observer.is_recording_mutual_information:
                stats["test_mutual_inf"] = self.testing_observer.mutual_inf_measure
        
        return stats


# --------------------------------------------------------------------------------------


class CountingFernTrainingObserver( BaseCountingFernTrainingObserver ):

    def __init__(self, record_gini_index : bool = False,
               record_mutual_information : bool = False ):
        
        self._initialize( record_gini_index = record_gini_index, 
                  record_mutual_information = record_mutual_information )

    # -------------------------------------------------------------------------------------------------------

    def debug_check(self):
        improved_rating = (len(self.ratings) < 2) or (self.total_rating(-1) <= self.total_rating(-2))
        improved_total_rating = (len(self.ratings) < 2) or (self.ratings[-1] <= self.ratings[-2])
        assert improved_rating or improved_total_rating

        if __debug__ and (self.num_measures > 1) and (self.regularization[-2] == self.regularization[-1]):
            improved_measure =      (self.mutual_inf_measure[-1] >= self.mutual_inf_measure[-2]) \
                                or  (self.gini_measure[-1] >=  self.gini_measure[-2])
            assert improved_measure
        return True
    
    # -------------------------------------------------------------------------------------------------------

    def notify_trained_layer( self, trainer, train_context ):
        super().notify_trained_layer( trainer, train_context )
        assert self.debug_check()

    def notify_retrain_improvement( self, trainer, train_context ):
        super().notify_retrain_improvement( trainer, train_context )
        assert self.debug_check()

    def notify_logreg_improvement( self, trainer, train_context ):
        super().notify_logreg_improvement( trainer, train_context )
        assert self.debug_check()

    def notify_split_optimization_improvement( self, trainer, train_context ):
        super().notify_split_optimization_improvement( trainer, train_context )
        assert self.debug_check()

    def notify_coordinate_descent_improvement( self, trainer, train_context ):
        super().notify_coordinate_descent_improvement( trainer, train_context )
        assert self.debug_check()

    def notify_gradient_line_search_improvement( self, trainer, train_context ):
        super().notify_gradient_line_search_improvement( trainer, train_context )
        assert self.debug_check()

    def notify_powells_method_improvement( self, trainer, train_context ):
        super().notify_powells_method_improvement( trainer, train_context )
        assert self.debug_check()

#------------------------------------------------------------------------------------------------------------

class VerboseFernTrainingObserver( CountingFernTrainingObserver ):

    out = None

    def __init__(self, out):
        self.out = out


    def notify_start_layer_training( self, train_context ):
        super().notify_start_layer_training( train_context )
        self.out.write( "Fern layer training: ")


    def notify_finished_layer_training( self, trainer, train_context, weighted_fern_layer, rating ):
        super().notify_finished_layer_training( trainer, train_context, weighted_fern_layer, rating )
        self.out.write("\n")


    def notify_finished_fern_optimization( self, trainer, train_context ):
        super().notify_finished_fern_optimization( trainer, train_context )
        self.out.write( train_context.num_layers * "," )
        self.out.write( "#" )
        self.out.flush()


    def notify_processed_feature( self, trainer, num_improvements, eigen_vector_idx ):
        super().notify_processed_feature( trainer, num_improvements, eigen_vector_idx )
        
        if num_improvements > 0:
            self.out.write("*(%d:%d)" % (eigen_vector_idx, num_improvements) )
        else:
            self.out.write(">")
        self.out.flush()
