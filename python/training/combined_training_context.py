# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["CombinedTrainingContext"]


import typing as tp
import numpy as np


#from ..ctypes_py import WeightedFernLayer #, IndexedFernLayer
from .base_training_context import AbstractLayerTrainingContext
from .tree_training_context import TreeTrainingContext
from .fern_training_context import FernTrainingContext
#from .base_training_context import BaseTrainingContext

#from .base_training_observation import BaseFernTrainingObserver, BaseTreeTrainingObserver
from .base_training_observation import BaseCombinedTrainingObserver
from .combined_training_observation import CountingCombinedTreeTrainingObserver

from ..fern import remove_layer




class TreeLayerTrainingContext( AbstractLayerTrainingContext ):

    __num_layers : int

    @property
    def num_layers(self) -> int:
        return self.__num_layers

    def __init__(self, feature_rows : tp.Sequence,
                           leaf_idx : np.ndarray,
                         num_layers : int,
                           observer : tp.Optional[BaseCombinedTrainingObserver] ):
        super().__init__( observer )
        assert isinstance( leaf_idx, np.ndarray )
        assert len(feature_rows[0]) == leaf_idx.size
        
        self.feature_rows = feature_rows
        self.__num_layers = int(num_layers)

        assert np.all( leaf_idx < self.num_leafs )
        self.set_leaf_idx( leaf_idx )


    # allows hashing of context - mainly debugging purpose
    def get_hashable_seq(self) -> tuple:
        return (super().get_hashable_seq() + (self.__num_layers,))

# -------------------------------------------------------------------------------------------------

class CombinedTrainingContext( FernTrainingContext ):

    __tree_feature_rows      : tp.Sequence


    fern_feature_getter : tp.Callable

    feature_indices     : tp.Sequence
    feature_splits      : tp.Sequence


    @property
    def num_distinct_features( self ) -> int:
        assert len( self.feature_indices ) == self.num_features
        return np.unique( self.feature_indices ).size


    @property
    def tree_feature_rows(self) -> tp.Sequence:
        return self.__tree_feature_rows
    
    @property
    def num_tree_features(self) -> int:
        return len(self.tree_feature_rows)


    def __init__(self, tree_feature_rows : tp.Sequence, 
                     fern_feature_getter : tp.Callable,
                                observer : tp.Optional[BaseCombinedTrainingObserver] = None ):
        """
        Parameters
        ----------
        tree_feature_rows : sequence
            Sequence of arrays of shape [ N ] containing the features of all data
            for tree training  (e.q. difference features)
        fern_feature_getter : callable
            fern_feature_getter( indices, splits ) shall return the corresponding
            fern features (e.g. self silimilaty featurs) to the indices (shape = [ I ])
            by using the coresponding splits (shape = [ I ]).
            Return value shall be a float32 array of shape [ I x N ]
        observer : None or derived from BaseCombinedTrainingObserver
        """
        assert callable( fern_feature_getter )
        assert (observer is None) or isinstance( observer, BaseCombinedTrainingObserver )

        super().__init__( observer )
        
        self.fern_feature_getter = fern_feature_getter
        
        self.feature_indices = list()
        self.feature_splits  = list()

        self.__tree_feature_rows = self.check_feature_rows( tree_feature_rows )


    # allows hashing of context - mainly debugging purpose
    def get_hashable_seq(self) -> tuple:
        hashable = tuple(self.feature_indices) # + tuple(self.feature_splits)  - float types are not good for hashing
        return (super().get_hashable_seq() + hashable)


    def copy( self ):
        n_inst = self.__copy_without_layers()

        if self.num_layers > 0:
            n_inst.set_layers( layers = list( self.layers ),
                             leaf_idx = self.leaf_idx.copy(), 
                               rating = self.rating )

        return n_inst


    def remove_layer(self, layer_idx : int,
                   rating_correction : tp.Optional[tp.Union[int,float]] = None ):
        
        layer_idx = int(layer_idx)
        assert layer_idx < self.num_layers

        # remove layer with specified index
        n_layers = [ fl for (idx, fl) in enumerate( self.layers ) if idx != layer_idx ]
        n_leaf_idx = remove_layer( self.leaf_idx, layer_idx, num_layers=self.num_layers )
        n_rating = self.rating

        if rating_correction is not None:
            n_rating -= rating_correction
            assert (n_rating + rating_correction) == self.rating

        # copy this training context
        n_inst = self.__copy_without_layers()

        if len(n_layers) > 0:
            n_inst.set_layers( layers = n_layers,
                             leaf_idx = n_leaf_idx, 
                               rating = n_rating )
        
        assert ( n_inst.num_layers + 1 ) == self.num_layers
        assert np.absolute( n_inst.leaf_idx ).max() < (1 << (self.num_layers-1))

        return n_inst


    def __copy_without_layers(self):
        n_inst = CombinedTrainingContext( tree_feature_rows = self.tree_feature_rows, 
                                        fern_feature_getter = self.fern_feature_getter,
                                                   observer = self.observer )
        
        n_inst.feature_rows = list(self.feature_rows)
        
        if self.num_features > 0:
            n_inst.feature_indices = self.feature_indices.copy()
            n_inst.feature_splits = self.feature_splits.copy()

        return n_inst



    def add_features(self, feature_indices : np.ndarray, 
                            feature_splits : np.ndarray ):
        
        if isinstance( self.feature_indices, np.ndarray ):
            indices_dtype = self.feature_indices.dtype
        else:
            indices_dtype = feature_indices.dtype
        
        if isinstance( self.feature_splits, np.ndarray ):
            split_dtype = self.feature_splits.dtype
        else:
            split_dtype = feature_splits.dtype

        feature_indices = np.asarray( feature_indices, dtype=indices_dtype )
        feature_splits = np.asarray( feature_splits, dtype=split_dtype )

        n_feature_rows = self.fern_feature_getter( feature_indices, feature_splits )
        n_feature_rows = self.check_feature_rows( n_feature_rows )

        self.feature_rows.extend( n_feature_rows )
        self.feature_indices = np.concatenate( ( np.asarray( self.feature_indices, dtype=indices_dtype ), 
                                                 feature_indices), axis=0 )
        self.feature_splits  = np.concatenate( ( np.asarray( self.feature_splits, dtype=split_dtype ), 
                                                 feature_splits), axis=0 )
        
        assert len( self.feature_rows ) == self.num_features
        assert len( self.feature_rows ) == self.feature_indices.size
        assert len( self.feature_rows ) == self.feature_splits.size
    

    def _as_dict( self ):
        res = { "fern_layers"     : self.layers,
                "node_idx"        : self.leaf_idx,   # for compatibility
                "leaf_idx"        : self.leaf_idx,
                "fern_rating"     : self.rating,
                "feature_indices" : self.feature_indices,
                "feature_lambdas" : self.feature_splits }
        
        return res
    

    def create_tree_training_context( self ):
        tree_training_observer = CountingCombinedTreeTrainingObserver( self.observer )
        t_train_context = TreeTrainingContext( feature_rows = self.__tree_feature_rows,
                                                   observer = tree_training_observer )
        
        return t_train_context


    def create_tree_layer_training_context(self):
        tree_training_observer = CountingCombinedTreeTrainingObserver( self.observer )
        layer_context = TreeLayerTrainingContext( feature_rows = self.__tree_feature_rows,
                                                      leaf_idx = self.leaf_idx,
                                                    num_layers = self.num_layers,
                                                      observer = tree_training_observer )
        return layer_context

# -------------------------------------------------------------------------------------------------
