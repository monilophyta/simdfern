
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



__all__ = [ "CountingCombinedTrainingObserver", "CountingCombinedTreeTrainingObserver",
            "VerboseCombinedTrainingObserver" ]



#import typing as tp
#import numpy as np

from .base_training_observation import BaseCombinedTrainingObserver, BaseCountingTrainingObserver
from .base_training_observation import ONS
from .tree_training_observation import BaseCountingTreeTrainingObserver


# ------------------------------------------------------------------------------------------------

class CountingCombinedTreeTrainingObserver( BaseCountingTreeTrainingObserver ):

    def __init__(self, main_observer : BaseCountingTrainingObserver ):
        assert isinstance( main_observer, BaseCountingTrainingObserver )
        self._initialize_with( main_observer, shallow_copy=True )


# ------------------------------------------------------------------------------------------------


class CountingCombinedTrainingObserver( BaseCombinedTrainingObserver, BaseCountingTrainingObserver ):


    def __init__(self, record_gini_index : bool = False,
               record_mutual_information : bool = False ):
        
        self._initialize( record_gini_index = record_gini_index, 
                  record_mutual_information = record_mutual_information )

    # --------------------------------------------------------------------------------

    def generic_notification( self, trainer, train_context, notify_source ):
        self._record_time()
        self.measure_source.append( notify_source )

        self._num_layers.append( train_context.num_layers )
        self.ratings.append( train_context.rating )
        self._update_informations_measures( trainer, train_context )
    

    # ----- CombinedTraining Notification ---------------------------------------------

    def notify_start_combined_training( self, trainer, train_context, num_layers ):
        super().notify_start_combined_training( trainer, train_context, num_layers )

        self._initialize( record_gini_index = self.is_recording_gini_index, 
                  record_mutual_information = self.is_recording_mutual_information )
    
    # ---------------------------------------------------------------------------------------------------------

    def notify_initially_trained_fern( self, trainer, train_context ):
        super().notify_initially_trained_fern( trainer, train_context )
        self.generic_notification( trainer, train_context, ONS.INITIALLY_TRAINED_FERN )

    def notify_improved_fern( self, trainer, train_context ):
        super().notify_initially_trained_fern( trainer, train_context )
        self.generic_notification( trainer, train_context, ONS.IMPROVED_FERN )

    # -------- Candidate Layer training -------------------------------------------------------------------------

    def notify_candidate_layer_training( self, trainer, train_context ):
        super().notify_candidate_layer_training( trainer, train_context )
        self.generic_notification( trainer, train_context, ONS.CANDIDATE_LAYER_TRAINING )


    def notify_candidate_fern_optimization( self, trainer, train_context ):
        super().notify_candidate_fern_optimization( trainer, train_context )
        self.generic_notification( trainer, train_context, ONS.CANDIDATE_FERN_OPTIMIZATION )


    # ----- FernTraining Notification --------------------------------------------------------------------------

    def notify_trained_layer( self, trainer, train_context ):
        super().notify_trained_layer( trainer, train_context )
        self.generic_notification( trainer, train_context, ONS.POST_FERN_LAYER_TRAINING )


    def notify_retrain_improvement( self, trainer, train_context ):
        super().notify_retrain_improvement( trainer, train_context )
        self.generic_notification( trainer, train_context, ONS.FERN_RETRAIN_IMPROVEMENT )


    def notify_logreg_improvement( self, trainer, train_context ):
        super().notify_logreg_improvement( trainer, train_context )
        self.generic_notification( trainer, train_context, ONS.FERN_LOGREG_IMPROVEMENT )


    def notify_split_optimization_improvement( self, trainer, train_context ):
        super().notify_split_optimization_improvement( trainer, train_context )
        self.generic_notification( trainer, train_context, ONS.FERN_WEIGHT_SPLIT_OPTIMIZATION_IMPROVEMENT )

    def notify_coordinate_descent_improvement( self, trainer, train_context ):
        super().notify_coordinate_descent_improvement( trainer, train_context )
        self.generic_notification( trainer, train_context, ONS.FERN_WEIGHT_COORDINATE_LSEARCH_IMPROVEMENT )

    def notify_gradient_line_search_improvement( self, trainer, train_context ):
        super().notify_gradient_line_search_improvement( trainer, train_context )
        self.generic_notification( trainer, train_context, ONS.FERN_WEIGHT_GRADIENT_LSEARCH_IMPROVEMENT )


#---------------------------------------------------------------------------------------------------

class VerboseCombinedTrainingObserver( CountingCombinedTrainingObserver ):

    out = None


    def __init__(self, out):
        super().__init__(         record_gini_index = True,
                          record_mutual_information = True)
        self.out = out

    # ----- CombinedTraining Notification ---------------------------------------------

    #def notify_start_combined_training( self, trainer, train_context, num_layers ):
    #    super().notify_start_combined_training( trainer, train_context, num_layers )
    

    def notify_selected_features( self, trainer, train_context ):
        super().notify_selected_features( trainer, train_context )
        self.out.write( "Selected %d features for initial fern training.\n" % train_context.num_features )


    def notify_initially_trained_fern( self, trainer, train_context ):
        super().notify_initially_trained_fern( trainer, train_context )
        self.out.write( "Initially trained fern with:\n\t%d layers\n\t%d features\n\trating = %r.\n" % 
                          ( train_context.num_layers, train_context.num_features, train_context.rating ) )


    def notify_candidate_feature_selection( self, trainer, train_context ):
        super().notify_candidate_feature_selection( trainer, train_context )
        self.out.write( "Features selection for layer candidate training with %d features.\n" % train_context.num_features )


    def notify_candidate_layer_training( self, trainer, train_context ):
        super().notify_candidate_layer_training( trainer, train_context )
        self.out.write( "Trained candidate layer with rating = %r.\n" % train_context.rating )


    def notify_candidate_fern_optimization( self, trainer, train_context ):
        super().notify_candidate_fern_optimization( trainer, train_context )
        self.out.write( "Optimized candidate layer with rating = %r.\n" % train_context.rating )


    def notify_improved_fern( self, trainer, train_context ):
        super().notify_improved_fern( trainer, train_context )
        self.out.write( "Improved fern with rating = %r.\n" % train_context.rating )


    def notify_finished_combined_training( self, trainer, train_context ):
        super().notify_finished_combined_training( trainer, train_context )
        self.out.write( "Finished fern training with rating = %r.\n" % train_context.rating )


