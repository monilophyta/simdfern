# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["AbstractLayerTrainingContext", "BaseTrainingContext"]

# -------------------------------------------------------------------------------------------------


import typing as tp
from abc import ABCMeta, abstractmethod
#import math
import numpy as np

from ..ctypes_py import AbtractLayerBase

from .base_training_observation import BaseTrainingObserver, DummyTrainingObserver
from .base_data_context import SampleFeatureContext


# ---------------------------------------------------------------------------------------------------------------

class AbstractLayerTrainingContext( SampleFeatureContext, metaclass=ABCMeta ):

    def __init__( self, observer=None ):
        super().__init__()
        assert (observer is None) or isinstance( observer, BaseTrainingObserver )
        
        self.__leaf_idx = None

        if observer is None:
            observer = DummyTrainingObserver()
        self.__observer = observer


    # allows hashing of context
    def get_hashable_seq(self) -> tuple:
        return (super().get_hashable_seq() + ( self.leaf_idx, ))

    # ----------------------------------------
    # observer

    __observer : BaseTrainingObserver
    @property
    def observer(self):
        return self.__observer

    # -----------------------------------------
    # # node idx
    __leaf_idx  : tp.Optional[np.ndarray]

    @property
    def leaf_idx(self):
        if self.__leaf_idx is None:
            return np.zeros( self.num_data, dtype=np.int32 )
        return self.__leaf_idx
    
    def set_leaf_idx( self, leaf_idx ):
        if __debug__ and (self.num_features > 0):
            assert leaf_idx.size == self.num_data
        
        assert np.issubdtype( leaf_idx.dtype, np.int32 )
        self.__leaf_idx = leaf_idx
    
    
    @property
    def num_leafs(self) -> int:
        return int(1 << self.num_layers)
    
    @property
    @abstractmethod
    def num_layers(self) -> int:
        pass

# -------------------------------------------------------------------------------------------------



class BaseTrainingContext( AbstractLayerTrainingContext ):

    def __init__( self, observer=None ):
        super().__init__( observer )
        self.__layers = []

    # -----------------------------------------
    # # node idx and Classifier Layers

    __layers    : list
    
    @property
    def layers(self) -> list:
        return self.__layers
    
    @property
    def num_layers(self) -> int:
        return len(self.layers)

    def layer(self,idx):
        return self.__layers[idx]
   

    def add_layer( self, layer : AbtractLayerBase, leaf_idx : np.ndarray ):
        assert isinstance( layer, AbtractLayerBase )
        assert np.issubdtype( leaf_idx.dtype, np.int32 )
        assert self.num_data == leaf_idx.size
        assert leaf_idx.max() < (1 << (self.num_layers + 1))
        
        if self.num_layers > 0:
            assert self.leaf_idx.size == leaf_idx.size
            assert np.all( np.right_shift( np.absolute( leaf_idx ), 1 ) == np.absolute( self.leaf_idx ) )
        
        self.__layers.append( layer )
        self.set_leaf_idx( leaf_idx )


    def set_layers( self, layers   : list,
                          leaf_idx : np.ndarray ):
        assert isinstance( layers, list )
        assert np.issubdtype( leaf_idx.dtype, np.int32 )
        assert self.num_data == leaf_idx.size
        assert leaf_idx.max() < (1 << len(layers))

        self.__layers = list(layers)
        self.set_leaf_idx( leaf_idx )

