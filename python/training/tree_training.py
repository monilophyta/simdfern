# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



__all__ = [ "TreeTrainer" ]


import logging
import typing as tp
import numpy as np

from ..parameter_registration import default_tree_training_params
from ..parameter_registration import ParameterRegistry, ParamKey as pk    # default parameter key

from .base_training import BaseTrainer, FeaturePrioritizer
from .base_training_context import AbstractLayerTrainingContext
from .tree_training_context import TreeTrainingContext

from ..ctypes_py import TreeSplitOptimizer, IndexedTreeLayer



class TreeTrainer( BaseTrainer ):


    def __init__(self, labels : np.ndarray,
                 data_weights : np.ndarray,
                       logger : logging.Logger,
               default_params : tp.Optional[ParameterRegistry] = None ):
        assert isinstance(  logger, logging.Logger )
        if default_params is None:
            default_params = default_tree_training_params.copy()
        super().__init__( labels, data_weights, logger.getChild( "treeT" ), default_params )



    def train_tree( self, train_context : TreeTrainingContext, 
                             num_layers : int = pk('train_tree.num_layers')  ) -> np.ndarray:
        """
        Parameters
        ----------
        train_context : instance of TreeTrainingContext
        num_layers    : integer

        Observer calls:
        ---------------
        notify_start_training( self, train_context, num_layers )
        notify_layer_classification( self, train_context, feature_priotizer )
        layers, node_ratings, nodeIdx = assess_finished_training( ons.TREE_TRAINING, self, layers, node_ratings, nodeIdx )
        """

        num_layers = self.param_registry( num_layers, int )
        assert num_layers > 0

        assert isinstance( train_context, TreeTrainingContext )
        assert train_context.num_data == self.num_data

        self.do_log( "train_tree: num_features available for tree training: %d", train_context.num_features )

        # output data structures
        ratings = np.array([])

        # feature priotizer
        priotizer = FeaturePrioritizer()

        # observer notification
        train_context.observer.notify_start_training( self, train_context, num_layers )


        for lidx in range( num_layers ):
            
            feature_indices = priotizer.get_feature_indices( np.arange( train_context.num_features, dtype=np.int32 ) )

            # train new layer
            tree_layer, ratings = self.train_layer( train_context, feature_indices )

            # put new features into priotizer
            priotizer.add_features( tree_layer.unique_feature_indices )

            # selected feature rows
            selected_feature_rows = [ train_context.feature_rows[fidx] for fidx in tree_layer.unique_feature_indices ]

            # remapped layer indices
            remapped_tree_layer = tree_layer.remap_feature_indices()

            # classification
            n_leaf_idx = remapped_tree_layer.classify( train_context.leaf_idx, selected_feature_rows )

            # logging
            self.do_log( "... trained layer %d/%d; sum(ratings)=%r", lidx+1, num_layers, ratings.sum() )

            # check results
            assert( np.all( np.equal( np.logical_or( train_context.leaf_idx < 0, n_leaf_idx < 0 ), n_leaf_idx < 0 ) ) )
            assert( np.all( np.equal( np.logical_and( train_context.leaf_idx < 0, n_leaf_idx < 0 ), train_context.leaf_idx < 0 ) ) )
            assert( np.all( (-n_leaf_idx[ n_leaf_idx < 0 ] % 2) == 0 ) )
            assert( np.all( tree_layer.feature_idx[ -n_leaf_idx[ n_leaf_idx < 0 ] // 2 ] < 0 ) )
            
            # add new layer to the context
            train_context.add_layer( tree_layer, n_leaf_idx, ratings )

            # observer notification
            train_context.observer.notify_layer_classification( self, train_context, feature_priotizer=priotizer )
        
        # observer notification
        train_context.observer.notify_finished_training( self, train_context )

        # Done
        return ratings



    #def train_layer( self, feature_rows, nodeIdx, feature_indices=None, num_nodes=None, observer=None ):
    def train_layer( self, train_context : AbstractLayerTrainingContext, 
                         feature_indices : np.ndarray ) -> tp.Tuple[IndexedTreeLayer,np.ndarray]:
        """
        Parameters
        ----------
        train_context   : instance of AbstractLayerTrainingContext
        feature_indices : integer ndarray
            range 0..self.num_features if not provided
        
        Returns
        -------
        tree_layer      : instance of IndexedTreeLayer
        ratings         : ndarray [float or integer]

        Observer calls:
        ---------------
        notify_start_layer_training( trainer, train_context )
        notify_processed_feature( trainer, train_context, optimizer, num_improvements, feature_idx )
        notify_finished_layer_training( trainer, tree_layer, ratings )
        """

        assert isinstance( train_context, AbstractLayerTrainingContext )
        assert train_context.num_data == self.num_data

        if feature_indices is None:
            feature_iter = enumerate(train_context.feature_rows)
        else:
            assert isinstance( feature_indices, np.ndarray )
            assert np.issubdtype( feature_indices.dtype, np.integer )
            feature_iter = ( (idx, train_context.feature_rows[idx]) for idx in feature_indices )

        # initialize split optimization
        optimizer = TreeSplitOptimizer( node_idx = train_context.leaf_idx,
                                       label_idx = self.labels,
                                    data_weights = self.data_weights,
                                       num_nodes = train_context.num_leafs,
                                      num_labels = self.num_labels )

        # observer notification
        train_context.observer.notify_start_layer_training( self, train_context )

        # loop over all features
        for fidx, feature_vals in feature_iter:

            assert train_context.num_data == feature_vals.size

            # process feature
            num_improvements = optimizer.process_feature( feature_vals )

            # observer notification
            train_context.observer.notify_processed_feature( self, train_context, optimizer, num_improvements, feature_idx=fidx  )
        
        tree_layer, ratings = optimizer.get_optimal_splits()

        # check if remapping of features might be required
        if feature_indices is not None:
            tree_layer = tree_layer.remap_feature_indices( feature_indices )

        # observer notification
        train_context.observer.notify_finished_layer_training( self, tree_layer, ratings )

        return (tree_layer, ratings)

