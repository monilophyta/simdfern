# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



__all__ = [ "CombinedTrainer" ]


import logging
import typing as tp
import numpy as np

from ..parameter_registration import default_combined_training_params, default_tree_training_params, default_fern_training_params
from ..parameter_registration import ParameterRegistry, ParamKey as pk    # default parameter key

#from .base_training_context import LayerTrainingContext
from .fern_training_context import FernTrainingContext
from .combined_training_context import CombinedTrainingContext

from .base_training import AbstractBaseTrainer, FeaturePrioritizer
from .tree_training import TreeTrainer
from .fern_training import FernTrainer


#from .base_training_observation import ObserverNotificationSource as ons

from ..fern import evaluate_layer_contributions
from ..clmath import mutual_counts

#from ..ctypes_py import TreeSplitOptimizer



class CombinedTrainer( AbstractBaseTrainer ):

    tree_trainer : TreeTrainer
    fern_trainer : FernTrainer

    @property
    def labels(self) -> np.ndarray:
        return self.tree_trainer.labels

    @property
    def data_weights(self) -> np.ndarray:
        return self.tree_trainer.data_weights

    @property
    def num_labels(self) -> int:
        return self.tree_trainer.num_labels
    
    @property
    def num_data(self) -> int:
        return self.tree_trainer.num_data

    

    def __init__(self, labels : np.ndarray,
                 data_weights : np.ndarray,
                       logger : logging.Logger,
          tree_trainer_params : tp.Optional[ParameterRegistry] = None,
          fern_trainer_params : tp.Optional[ParameterRegistry] = None,
      combined_trainer_params : tp.Optional[ParameterRegistry] = None ):
        
        assert isinstance(  logger, logging.Logger )
        logger = logger.getChild( "combiT" )

        if combined_trainer_params is None:
            combined_trainer_params = default_combined_training_params.copy()
        
        # -----------------------------------------------------------------------------------------------------

        if tree_trainer_params is None:
            if combined_trainer_params.has_subtree( "tree_training" ):
                tree_trainer_params = combined_trainer_params.subtree( "tree_training" )
            else:
                tree_trainer_params = default_tree_training_params.copy()

        self.tree_trainer = TreeTrainer( labels, data_weights, logger = logger, default_params = tree_trainer_params )

        # -----------------------------------------------------------------------------------------------------

        if fern_trainer_params is None:
            if combined_trainer_params.has_subtree( "fern_training" ):
                fern_trainer_params = combined_trainer_params.subtree( "fern_training" )
            else:
                fern_trainer_params = default_fern_training_params.copy()

        self.fern_trainer = FernTrainer( labels, data_weights, logger = logger, default_params = fern_trainer_params )

        # -----------------------------------------------------------------------------------------------------
        # calling base constructor
        super( CombinedTrainer, self ).__init__(logger, combined_trainer_params)


    def train_fern( self, train_context : CombinedTrainingContext,
                        num_fern_layers : int  = pk('train_fern.num_fern_layers'),
                  max_num_fern_features : int  = pk('train_fern.max_num_fern_features'),
              max_num_feature_extension : int  = pk('train_fern.max_num_feature_extension'),
                   num_init_tree_layers : int  = pk('train_fern.num_init_tree_layers'),
                  retrain_weakest_layer : bool = pk('train_fern.retrain_weakest_layer') ):
        """
        Using:

        N = num_data

        Parameters
        ----------
        num_fern_layers       : int
            number of fern layers to be trained
        max_num_fern_features : int
            maximum number of features used in this this training
        max_num_feature_extension : int
        num_init_tree_layers  : int >= 1
            size of initially traind decission tree for initial fern feature acquisition.
            The number of initial features is then: 2**(num_init_fern_layers-1)
        retrain_weakest_layer : bool (default True)
            If True, after every fern optimiation the weakest (lowest contribution) fern layer
            removed. Then a further decission tree layer is trainined for additional feature 
            acquisition. These (plus already acquired) features are used for a training of a new 
            replacing fern layer.
            If False, the procedure is the same as with True, except that a further tree layer is trained
            without removing a weak layer. This results in potentially twice more features in next fern training
        """
        num_fern_layers           = self.param_registry( num_fern_layers, int )
        num_init_tree_layers      = self.param_registry( num_init_tree_layers, int )
        max_num_fern_features     = self.param_registry( max_num_fern_features, int )
        max_num_feature_extension = self.param_registry( max_num_feature_extension, int )
        retrain_weakest_layer     = self.param_registry( retrain_weakest_layer, bool )

        assert num_init_tree_layers >= 1
        assert max_num_feature_extension >= 1
        assert max_num_feature_extension <= max_num_fern_features
        assert max_num_fern_features >= ((1 << num_init_tree_layers) - 1)

        # Number of nodes in next to final layer
        num_next2final_nodes = 1 << (num_fern_layers-1)

        # tree feature priotizer
        tree_feature_priotizer = FeaturePrioritizer()

        # observer notification
        train_context.observer.notify_start_combined_training( self, train_context, num_fern_layers )

        # Get initial feature set
        tree_feature_indices, tree_feature_splits = self.select_intial_fern_features( train_context, num_init_tree_layers )
        self.do_log( "number initialy selected features: %d (distinct: %d)", tree_feature_indices.size, np.unique(tree_feature_indices).size )

        # Add Features to priotizer and fern feature manager
        tree_feature_priotizer.add_features( tree_feature_indices )
        train_context.add_features( tree_feature_indices, tree_feature_splits )

        # observer notification
        train_context.observer.notify_selected_features( self, train_context )

        # Train Initial Fern
        self.train_initial_fern( train_context, num_fern_layers )

        # observer notification
        train_context.observer.notify_initially_trained_fern( self, train_context )

        # Optimization loops
        continue_optimization = True
        while continue_optimization:
            
            # determ maximum number of next features
            max_num_next_features = max_num_fern_features - train_context.num_distinct_features
            max_num_next_features = min( max_num_feature_extension, max_num_next_features )
            if max_num_next_features <= 0:
                self.do_log( "reached %d distinct features. Stopping.", train_context.num_distinct_features )
                break
            
            # configure fern to be updated
            if retrain_weakest_layer:
                # remove weakest fern layer
                n_train_context = self.remove_weakest_layer( train_context )
                assert np.all( n_train_context.leaf_idx < num_next2final_nodes )
            else:
                n_train_context = train_context.copy()

            # select next fern features
            next_feature_indices, next_feature_splits = \
                        self.select_next_fern_features( n_train_context, \
                                                        tree_feature_priotizer = tree_feature_priotizer, \
                                                        max_num_features = max_num_next_features, \
                                                        num_nodes = n_train_context.num_leafs )
            
            # observer notification
            train_context.observer.notify_candidate_feature_selection( self, n_train_context )

            # logging results
            self.do_log( "next num. selected features: %d (distinct: %d)", next_feature_indices.size, np.unique(next_feature_indices).size )

            # extend fern features
            n_train_context.add_features( next_feature_indices, next_feature_splits )
            self.do_log( "... total num features: %d (distinct: %d)", n_train_context.num_features,
                                                                      n_train_context.num_distinct_features )
            
            if retrain_weakest_layer:
                # Train new layer
                self.train_next_fern_layer( n_train_context )
                train_context.observer.notify_candidate_layer_training( self, n_train_context )

            # Optimizing entire fern with new features
            self.optimize_fern( n_train_context )
            train_context.observer.notify_candidate_fern_optimization( self, n_train_context )
            
            # check for improvements
            if n_train_context.rating < train_context.rating:
                self.do_log( "... improved fern from rating %r to %r", train_context.rating, n_train_context.rating )
                
                # take-over of new training context
                assert id(train_context) != id(n_train_context)
                train_context = n_train_context
                train_context.observer.notify_improved_fern( self, n_train_context )

                # extend tree feature priotizer
                tree_feature_priotizer.add_features( next_feature_indices )
            else:
                self.do_log( "... no improvement in fern rating from %r to %r. Training stopped", train_context.rating, n_train_context.rating )
                continue_optimization = False
            

            if __debug__:
                assert self.fern_trainer.verify_classification( train_context, do_throw=True )
                self.do_log( "... nodeIdx verified" )
        
        train_context.observer.notify_finished_combined_training( self, train_context )
        return train_context


    # ------------------------------------------------------------------------------------------------------------------------------------

    def select_intial_fern_features( self, train_context : CombinedTrainingContext,
                                           num_layers : int ):
        
        # create training context for trees
        tree_training_context = train_context.create_tree_training_context()
        
        # train initial tree
        self.tree_trainer.train_tree( tree_training_context, num_layers )

        # extract features from trained tree layers
        tree_layers = tree_training_context.layers
        tree_feature_indices, tree_feature_splits = zip( *[ (x.valid_feature_indices, x.valid_feature_splits) for x in tree_layers ] )

        tree_feature_indices = np.concatenate( tree_feature_indices, axis=0 )
        tree_feature_splits  = np.concatenate( tree_feature_splits, axis=0 )

        return (tree_feature_indices, tree_feature_splits)

    
    def select_next_fern_features( self, train_context          : CombinedTrainingContext,
                                         tree_feature_priotizer : np.ndarray,
                                         max_num_features       : int,
                                         num_nodes              : int  ):

        # create tree training context
        layer_train_context = train_context.create_tree_layer_training_context()
        assert layer_train_context.num_leafs <= num_nodes
        assert np.all( layer_train_context.leaf_idx == train_context.leaf_idx )

        # get features in the order of their priority
        tree_feature_indices = tree_feature_priotizer.get_feature_indices( np.arange( layer_train_context.num_features, dtype=np.int32 ) )

        # train next tree layer
        next_tree_layer, next_tree_layer_ratings = \
            self.tree_trainer.train_layer( layer_train_context, feature_indices = tree_feature_indices )
        self.do_log( "tree layer trained for feature acquisition: %d features with sum(ratings)=%d", 
                     next_tree_layer_ratings.size, next_tree_layer_ratings.sum() )
        assert np.all( next_tree_layer_ratings <= 0 ), "Gini node rating are expected to be smaller than zero"


        # get features of next training
        n_feature_indices = next_tree_layer.valid_feature_indices
        n_feature_splits  = next_tree_layer.valid_feature_splits

        # select features subset for further training
        if n_feature_indices.size > max_num_features:

            # Node rating of previous fern for comparison
            node_ratings = mutual_counts( layer_train_context.leaf_idx, self.labels, 
                                          weights = self.data_weights, 
                                          num_X=num_nodes, num_Y=self.num_labels ).unormed_node_gini_index( axis=1 )
            
            #assert node_ratings.size == next_tree_layer_ratings.size
            assert node_ratings.size == next_tree_layer_ratings.size
            assert node_ratings.size == next_tree_layer.num_branching_nodes
            assert np.all( node_ratings <= (-1 * next_tree_layer_ratings) )

            # get improvements for every node -> the smaller the better
            node_improvements = node_ratings + next_tree_layer_ratings
            assert np.all( node_improvements <= 0  )

            # discard invalid (empty) nodes
            node_improvements = node_improvements[next_tree_layer.valid_node_mask]
            assert node_improvements.size == n_feature_indices.size

            # Remove redundancy in feature list
            unique_feature_indices = np.unique( n_feature_indices )
            if unique_feature_indices.size < n_feature_indices.size:
                # limit feature size to maximum available
                max_num_features = min( unique_feature_indices.size, max_num_features )
                
                # get maximum improvement for every features
                relevant_feature_mask = np.zeros( n_feature_indices.size, dtype=np.bool )

                for uf in unique_feature_indices:
                    uf_idx = np.flatnonzero( n_feature_indices == uf )
                    uf_idx = uf_idx[ node_improvements[ uf_idx ].argmin() ] # feature index with lowest(best) improvement
                    relevant_feature_mask[ uf_idx ] = True
                
                # select relevant features
                n_feature_indices = n_feature_indices[ relevant_feature_mask ]
                n_feature_splits  = n_feature_splits[ relevant_feature_mask ]
                node_improvements = node_improvements[ relevant_feature_mask ]

            # for debugging
            assert np.unique( n_feature_indices ).size == n_feature_indices.size, \
                    "Expected unique feature, but found redundancy"
                
            # best indices 
            best_idx = np.argsort( node_improvements )[:max_num_features]
            self.do_log( "... selected %d features with sum(improvements)=%d", max_num_features, node_improvements[best_idx].sum() )

            n_feature_indices = n_feature_indices[best_idx]
            n_feature_splits  = n_feature_splits[best_idx]
        else:
            self.do_log( "... selected all of them" )

        return (n_feature_indices, n_feature_splits)
    

    def train_initial_fern( self, train_context : CombinedTrainingContext,
                                     num_layers : int,
                       incremental_optimization : bool = pk('train_initial_fern.incremental_optimization'),
                       layer_optimization_steps : str  = pk('train_initial_fern.layer_optimization_steps') ):

        incremental_optimization = self.param_registry( incremental_optimization, bool )
        layer_optimization_steps = self.param_registry( layer_optimization_steps, str )

        # train ferns
        self.fern_trainer.train_fern( train_context, num_layers,
                           layer_optimization_steps = (incremental_optimization and layer_optimization_steps) or "" )
        assert train_context.num_layers == num_layers
        assert np.absolute( train_context.leaf_idx ).max() < (1 << num_layers)

        # optimization
        if not incremental_optimization:
            self.fern_trainer.optimize_fern( train_context )
            assert train_context.num_layers == num_layers
            assert np.absolute( train_context.leaf_idx ).max() < (1 << num_layers)

    
    def train_next_fern_layer( self, train_context : FernTrainingContext ):

        num_nodes = 1 << train_context.num_layers
        assert np.absolute( train_context.leaf_idx ).max() < num_nodes

        # train next fern layer
        n_last_fern_layer, n_last_fern_layer_rating = \
                self.fern_trainer.train_layer( train_context )
        
        # classification
        n_leaf_idx = n_last_fern_layer.classify( train_context.leaf_idx, train_context.feature_rows )

        # attach new layer
        train_context.add_layer( n_last_fern_layer, n_leaf_idx, n_last_fern_layer_rating )

        return train_context
    

    def optimize_fern( self, train_context : FernTrainingContext ):

        assert np.absolute( train_context.leaf_idx ).max() < (1 << train_context.num_layers)

        before_rating = train_context.rating

        # Optimize fern layers
        self.fern_trainer.optimize_fern( train_context )

        assert before_rating >= train_context.rating
        return train_context

    
    def remove_weakest_layer( self, train_context : CombinedTrainingContext,
                       layer_contribution_measure : str = pk('remove_weakest_layer.layer_contribution_measure') ) -> CombinedTrainingContext:
        assert isinstance( train_context, CombinedTrainingContext )
        
        # evaluate layer contributions
        layer_contribution_measure = self.param_registry( layer_contribution_measure, str )
        layer_contributions = evaluate_layer_contributions( labels       = self.labels, 
                                                            nodeIdx      = train_context.leaf_idx,
                                                            data_weights = self.data_weights,
                                                            num_labels   = self.num_labels,
                                                            num_layers   = train_context.num_layers,
                                                            measure      = layer_contribution_measure )

        assert np.all( layer_contributions <= -train_context.rating ), "Layer contribution shall be smaller than (negative) rating (which is gini)"
        self.do_log( "fern layer contributions are: %r ", layer_contributions )

        # Identify weakest layer
        weakest_layer_idx = layer_contributions.argmin()
        weakest_layer_contribution = layer_contributions[weakest_layer_idx]

        # remove weakest layer
        n_train_context = train_context.remove_layer( layer_idx = weakest_layer_idx, 
                                              rating_correction = -weakest_layer_contribution )
        
        if __debug__:
            assert -n_train_context.rating == self.calc_gini_rating( leaf_idx = n_train_context.leaf_idx, 
                                                                    num_leafs = n_train_context.num_leafs )

        self.do_log( "removed weakest layer %d for retraining ", weakest_layer_idx )

        return n_train_context

#---------------------------------------------------------------------------------------------------

