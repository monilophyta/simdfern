/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <tree.hpp>
#include "ctypes_return_codes.hpp"
#include "ctypes_os.hpp"
#include "ctypes_types.hpp"



// ----------------------------------------------------------------------------------------
// Generic function

extern "C"
DLL_PUBLIC 
int32_t num_simd_vector_items();

extern "C"
DLL_PUBLIC 
tree::EventStat getEventCounts();

// ----------------------------------------------------------------------------------------


extern "C"
DLL_PUBLIC 
int32_t 
evaluate_box_features( const intimg_t*           f_intImg_p,
                       const box_feature_list_t* f_boxFeatures_p,
                       const img_coord_list_t*   f_imgCoords_p,
                       float32_t*                f_featuresOut_p );


// ----------------------------------------------------------------------------------------

extern "C"
DLL_PUBLIC 
int32_t 
classify_samples( const indexed_tree_layer_t*  f_treeLayer_p,
                  const feature_rows_t*        f_featureRows_p,
                  const int32_t*               f_inNodeIdx_p,
                        int32_t*               f_outNodeIdx_p );



// ----------------------------------------------------------------------------------------


namespace ctypes
{
    struct CFernSplitOptimizer;
    typedef CFernSplitOptimizer  fern_split_optimizer_t;
}


// ----------------------------------------------------------------------------------------

enum class ESummationAlgo : int32_t
{
    NAIVE    = 0,
    KAHAN    = 1,
    NEUMAIER = 2,
    KLEIN    = 3
};


extern "C"
DLL_PUBLIC 
void 
project_samples( const feature_rows_t* f_featureRows_p,
                       int32_t         f_numFeatures_u,
                 const float32_t*      f_weights_p,
                       float32_t*      f_projectedData_p,
                       ESummationAlgo  f_sumAlgo_e );

// ----------------------------------------------------------------------------------------
