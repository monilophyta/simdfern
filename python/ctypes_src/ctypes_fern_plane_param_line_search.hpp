/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <tree.hpp>
#include <type_traits>
#include "ctypes_tree.hpp"
#include "ctypes_return_codes.hpp"



namespace ctypes
{


enum class EPenalizerType : int
{
    NO_PENALIZER                 = 0,
    
    L1L2_PENALIZER               = 1,
    L1L2_VECT_PENALIZER          = 2,

    NORMALIZED_L1_PENALIZER      = 3,
    NORMALIZED_L1_VECT_PENALIZER = 4
};


struct param_penalizer_t
{
    EPenalizerType  m_penalizerType_e;

    // anonymous union
    union
    {
        tree::CPenalizer                                                   m_l1l2Penalizer;
        
        union
        {
            tree::CVectPenalizer<float32_t>    m_unaligned;
            tree::CVectPenalizer<smd::vf32_t>  m_aligned;
        }                                                                  m_l1l2VectPenalizer;


        tree::CNormalizedL1Penalizer                                       m_normalizedL1Penalizer;
        union
        {
            tree::CNormalizedL1VectPenalizer<float32_t>    m_unaligned;
            tree::CNormalizedL1VectPenalizer<smd::vf32_t>  m_aligned;
        }                                                                  m_normalizedL1VectPenalizer;
    };
};
static_assert( std::is_standard_layout<param_penalizer_t>::value );


struct line_search_result_t
{
    float32_t    prmVal;
    int32_t      numOptimizationSteps;

    union
    {
        int32_t      giniRating;
        float32_t    entropyRating;
    };

    float32_t    prmValPenalty;
};
static_assert( std::is_standard_layout<line_search_result_t>::value );

}  // namespace ctypes


extern "C"
DLL_PUBLIC
int32_t 
fern_line_search_on_entropy( const sample_classification_data_t*   f_sampleClData_p,
                             const float32_t*                      f_prmValCandidates_p,
                             const ctypes::param_penalizer_t*      f_paramPenalizer_p,
                             ctypes::line_search_result_t*         f_optimRes_p );


extern "C"
DLL_PUBLIC
int32_t 
fern_line_search_on_gini( const sample_classification_data_t*  f_sampleClData_p,
                          const float32_t*                     f_prmValCandidates_p,
                          const ctypes::param_penalizer_t*     f_paramPenalizer_p,
                          ctypes::line_search_result_t*        f_optimRes_p );


