/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include <cassert>
#include <tree.hpp>
#include "ctypes_fern_split_optimization.hpp"
#include "ctypes_base_split_optimization.inl"
#include "ctypes_return_codes.hpp"



extern "C"
ctypes::fern_split_optimizer_t* fern_create_split_optimizer( int32_t f_numLabels_i32, int32_t f_numNodes_i32 )
{
    return ctypes::base_create_split_optimizer<ctypes::fern_split_optimizer_t>( f_numLabels_i32, f_numNodes_i32 );
}


extern "C"
void fern_reset_optimal_split( ctypes::fern_split_optimizer_t* f_soptim_p )
{
    f_soptim_p->reset_optimal_split();
}


extern "C"
void fern_destruct_split_optimizer( ctypes::fern_split_optimizer_t** f_soptim_p )
{
    return ctypes::base_destruct_split_optimizer( f_soptim_p );
}


extern "C"
int32_t fern_initialize_parent_layer_statistic( ctypes::fern_split_optimizer_t* f_soptim_p,
                                                const node_label_list_t*        f_data_p )
{
    return ctypes::base_initialize_parent_layer_statistic( f_soptim_p, f_data_p );
}


extern "C"
int32_t fern_process_feature( ctypes::fern_split_optimizer_t* f_soptim_p,
                              const node_label_data_list_t*   f_data_p )
{
    return ctypes::base_process_feature( f_soptim_p, f_data_p );
}


extern "C"
int32_t fern_get_optimal_split( const ctypes::fern_split_optimizer_t* f_soptim_p,
                                feature_split_t*                      f_split )
{
    if ( (nullptr == f_soptim_p) || (nullptr == f_split) )
    {
        return ctypes::ret_flags_t( ctypes::ReturnCode::INVALID_POINTERS );
    }

    return f_soptim_p->get_optimal_split( f_split );
}



//----------------------------------------------------------------------------------------------------------------


namespace ctypes
{


void CFernSplitOptimizer::reset_optimal_split()
{
    switch ( m_optimMethod_e )
    {
        case ESplitOptimMethod::ENTROPY_OPTIMIZATION:
            m_entropyOptim_p->m_optimizer.reset_optimal_split();
            break;
        case ESplitOptimMethod::GINI_OPTIMIZATION:
            m_giniOptim_p->m_optimizer.reset_optimal_split();
            break;
        default:
        {
            assert(false);
        }
    }
}


ret_flags_t CFernSplitOptimizer::get_optimal_split( feature_split_t* f_split ) const
{
    ret_flags_t ret_flags;

    switch ( m_optimMethod_e )
    {
        case ESplitOptimMethod::ENTROPY_OPTIMIZATION:
            {
                f_split->featureIdx   = m_entropyOptim_p->m_optimizer.getOptimalFeatureSplit().m_featureIdx_i32;
                f_split->featureSplit = m_entropyOptim_p->m_optimizer.getOptimalFeatureSplit().m_featureSplit_f32;
                f_split->nodeEntropy  = m_entropyOptim_p->m_optimizer.getOptimalFeatureSplit().m_nodeRating;

                ret_flags << ReturnCode::ENTROPY_RATING;
            }
            break;
        case ESplitOptimMethod::GINI_OPTIMIZATION:
            {
                f_split->featureIdx      = m_giniOptim_p->m_optimizer.getOptimalFeatureSplit().m_featureIdx_i32;
                f_split->featureSplit    = m_giniOptim_p->m_optimizer.getOptimalFeatureSplit().m_featureSplit_f32;
                f_split->nodeGiniRating  = m_giniOptim_p->m_optimizer.getOptimalFeatureSplit().m_nodeRating;
                
                ret_flags << ReturnCode::GINI_RATING;
            }
            break;
        default:
        {
            assert(false);
            return ret_flags_t( ReturnCode::UNKNOWN_ERROR );
        }
    }

    return ret_flags;
}


} // namespace ctypes


