/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "ctypes_tree.hpp"
#include "ctypes_types_base.hpp"



struct lda_stat_result_data_t
{
    int32_t                 numFeatures;    // =k

    int32_t                 intra_covar_stride;
    float32_t*              intra_covar;

    int32_t                 inter_covar_stride;
    float32_t*              inter_covar;
};
static_assert( std::is_standard_layout<lda_stat_result_data_t>::value );



extern "C"
DLL_PUBLIC 
int32_t
calculate_lda_covariances( const node_label_list_t*      f_ldaInput_p,
                           const feature_rows_t*         f_featureRows_p,
                                 lda_stat_result_data_t* f_ldaOutput_p );

