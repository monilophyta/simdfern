/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

// example: see https://github.com/nlgranger/CPPyModule/blob/master/example-distutils/flipping_wrapper.h


#include "ctypes_types_base.hpp"


struct intimg_t
{
    image_descr_t    intimg_descr;

    union
    {
        const float32_t* mem_pf32;
        const int32_t*   mem_pi32;
    };
    
    bool   isFloat32Img;
};
static_assert( std::is_standard_layout<intimg_t>::value );



struct box_feature_list_t
{
    box_list_t   first;
    box_list_t   second;
    float32_t*   lambda;  // NULL if "box ratio" feature instead of "box difference" 

    int32_t      numBoxPairs;
};
static_assert( std::is_standard_layout<box_feature_list_t>::value );



struct img_coord_list_t
{
    img_coord_t*  coords;
    int32_t       numCoords;
};
static_assert( std::is_standard_layout<img_coord_list_t>::value );


//------------------------------------------------------------------------------------


struct indexed_tree_layer_t
{
    int32_t     numSplits;

    int32_t*    featureIdx;     // [K]
    float32_t*  featureSplit;  // [K]
};
static_assert( std::is_standard_layout<indexed_tree_layer_t>::value );



struct feature_split_list_t
{
    int32_t      numSplits;

    int32_t*     featureIdx;
    float32_t*   featureSplit;

    union
    {
        int32_t*     nodeGiniRating;
        float32_t*   nodeEntropy;
    };
};
static_assert( std::is_standard_layout<feature_split_list_t>::value );


//------------------------------------------------------------------------------------

struct feature_split_t
{
    int32_t      featureIdx;
    float32_t    featureSplit;

    union
    {
        int32_t      nodeGiniRating;
        float32_t    nodeEntropy;
    };
};
static_assert( std::is_standard_layout<feature_split_list_t>::value );


//------------------------------------------------------------------------------------

struct classification_result_data_t
{
    //int32_t       storageSize;

    int32_t       numData;
    int32_t*      nodeIdx;   // aligned
};
static_assert( std::is_standard_layout<classification_result_data_t>::value );

