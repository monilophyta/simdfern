/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include <cassert>
#include <tree.hpp>

#include "ctypes_types.hpp"
#include "ctypes_return_codes.hpp"

#include "ctypes_fern_plane_param_line_search.hpp"


namespace ctypes
{
namespace 
{

struct CLineSearchBase
{
protected:

    const sample_classification_data_t*  m_sampleClData_p;
    const float32_t*                     m_prmValCandidates_p;
    const ctypes::param_penalizer_t*     m_paramPenalizer_p;
    ctypes::line_search_result_t*        m_optimRes_p;

public:

    ret_flags_t m_retflag;

    inline CLineSearchBase( const sample_classification_data_t*  f_sampleClData_p,
                            const float32_t*                     f_prmValCandidates_p,
                            const ctypes::param_penalizer_t*     f_paramPenalizer_p,
                            ctypes::line_search_result_t*        f_optimRes_p )
        : m_sampleClData_p( f_sampleClData_p )
        , m_prmValCandidates_p( f_prmValCandidates_p )
        , m_paramPenalizer_p( f_paramPenalizer_p )
        , m_optimRes_p( f_optimRes_p )
    {
        m_retflag = check_fern_plane_param_line_search_input();
    }

    inline
    bool has_error() { return m_retflag.has_error(); }
    

    inline constexpr
    size_t num_data() const { return (m_sampleClData_p->numData); }

    // ----------------------------------------------------------------------------------

private:

    inline ret_flags_t
    check_fern_plane_param_line_search_input() const;

protected:

    static inline bool is_aligned(const tree::CBaseVectPenalizer<float32_t>& f_paramPenalizer_r);

    static inline ret_flags_t check_vect_penalizer(const tree::CBaseVectPenalizer<float32_t>& f_paramPenalizer_r);


    inline constexpr 
    const tree::nodeIdx_t* node_begin() const { 
        return tree::nodeIdx_t::cast_from( m_sampleClData_p->nodeIdx ); }
    
    inline constexpr
    const tree::nodeIdx_t* node_end() const { 
        return (node_begin() + num_data()); }

    inline constexpr 
    const float32_t* candidate_begin() const { return m_prmValCandidates_p; }
    inline constexpr
    const float32_t* candidate_end() const { return (candidate_begin() + num_data()); }

    inline constexpr
    const int32_t* data_weights_begin() const { return m_sampleClData_p->dataWeight; }
    inline constexpr
    const int32_t* data_weights_end() const { return (data_weights_begin() + num_data()); }
    
    inline constexpr
    const int32_t* label_idx_begin() const { return m_sampleClData_p->labelIdx; }
    inline constexpr
    const int32_t* label_idx_end() const { return (label_idx_begin() + num_data()); }
};


inline bool
CLineSearchBase::is_aligned(const tree::CBaseVectPenalizer<float32_t>& f_paramPenalizer_r)
{
    bool l_isAligned_b = (nullptr != f_paramPenalizer_r.m_offset_pf32) &&
                         smd::is_memory_aligned( f_paramPenalizer_r.m_offset_pf32 ) &&
                         smd::is_memory_aligned( f_paramPenalizer_r.m_offsetEnd_pf32 ) &&
                         smd::is_memory_aligned( f_paramPenalizer_r.m_scale_pf32 );
    return l_isAligned_b;
}


inline ret_flags_t CLineSearchBase::check_fern_plane_param_line_search_input() const
{
    ret_flags_t retflag;
    
    bool l_invalidInput_b = (m_sampleClData_p == nullptr) ||
                            (m_prmValCandidates_p == nullptr) ||
                            (m_paramPenalizer_p == nullptr) ||
                            (m_optimRes_p == nullptr) ||
                            (node_begin() == nullptr) ||
                            (label_idx_begin() == nullptr) ||
                            (data_weights_begin() == nullptr);
    if (l_invalidInput_b)
    {
        retflag << ReturnCode::INVALID_POINTERS;
    }

    return retflag;
}


inline ret_flags_t
CLineSearchBase::check_vect_penalizer(const tree::CBaseVectPenalizer<float32_t>& f_paramPenalizer_r)
{
    ret_flags_t retflag;

    bool l_invalidInput_b = (f_paramPenalizer_r.m_offset_pf32 > f_paramPenalizer_r.m_offsetEnd_pf32) ||
                            ( bool(f_paramPenalizer_r.m_offsetEnd_pf32 == nullptr) != 
                              bool(f_paramPenalizer_r.m_scale_pf32 == nullptr) );
    if (l_invalidInput_b)
    {
        retflag << ReturnCode::INVALID_POINTERS;
    }
    else
    {
        l_invalidInput_b = (!std::isfinite(f_paramPenalizer_r.m_regulStrength_f32)) ||
                           (f_paramPenalizer_r.m_regulStrength_f32 < 0.f);
        
        if (l_invalidInput_b)
        {
            retflag << ReturnCode::INVALID_INPUT;
        }
    }

    return retflag;
}


//--------------------------------------------------------------------------------------------------------



template <typename split_stat_T>
class CLineSearch : protected CLineSearchBase
{
public:

    // Inherit from base class
    using CLineSearchBase::CLineSearchBase;
    using CLineSearchBase::has_error;
    using CLineSearchBase::m_retflag;

    ret_flags_t run();

private:

    template <typename penalizer_T>
    inline void co_line_search( const penalizer_T& f_paramPenalizer_r );

    template <typename penalizer_T>
    inline ret_flags_t vector_line_search( const penalizer_T& f_paramPenalizer_r );
};



template <typename split_stat_T>
template <typename penalizer_T>
inline void
CLineSearch<split_stat_T>::co_line_search( const penalizer_T& f_paramPenalizer_r )
{
   
    typedef tree::CPlaneParamLineSearch<penalizer_T,split_stat_T> optimizer_t;
    optimizer_t l_optimizer( m_sampleClData_p->numLabels, m_sampleClData_p->numNodes, f_paramPenalizer_r );

    const int32_t* l_labelIterEnd = l_optimizer.initialize_from( this->node_begin(),
                                                                 this->node_end(),
                                                                 this->label_idx_begin(),
                                                                 this->data_weights_begin() );
    tree_bounds_check( l_labelIterEnd == this->label_idx_end() );
    

    /// run the optimization
    int32_t num_optimizations = l_optimizer.process_unsorted_feature( this->candidate_begin(),
                                                                      this->candidate_end(),
                                                                      this->label_idx_begin(),
                                                                      this->node_begin(),
                                                                      this->data_weights_begin() );
    
    /// copy the output
    m_optimRes_p->prmVal = l_optimizer.getOptimal().m_prmVal_f32;
    m_optimRes_p->numOptimizationSteps = num_optimizations;
    m_optimRes_p->prmValPenalty = l_optimizer.getOptimal().prmValPenalty();

    if ( true == std::is_same<split_stat_T,tree::CTreeSplitEntropyStatistics>::value )
    {
        m_optimRes_p->entropyRating = l_optimizer.getOptimal().splitRating();
    }
    else if ( true == std::is_same<split_stat_T,tree::CTreeSplitGiniStatistics>::value )
    {
        m_optimRes_p->giniRating = l_optimizer.getOptimal().splitRating();
    }
    else
    {
        assert( false );
    }
}


template <typename split_stat_T>
template <typename penalizer_T>
inline ret_flags_t
CLineSearch<split_stat_T>::vector_line_search( const penalizer_T& f_paramPenalizer_r )
{
    ret_flags_t retflag = m_retflag;

    retflag << this->check_vect_penalizer( f_paramPenalizer_r.m_unaligned );
    if (retflag.has_error())
        return retflag;
    
    if (false == this->is_aligned(f_paramPenalizer_r.m_unaligned))
    {
        /// vec{b} and vec{c} are not aligned
        this->co_line_search( f_paramPenalizer_r.m_unaligned );
        retflag << ReturnCode::PROCESSED_NONALIGNED;

        // update event counts
        tree::evtcnt().increment_line_search_eval_num_unaligned( f_paramPenalizer_r.m_unaligned.num_features() * 
                                                                 this->num_data());
    }
    else
    {
        /// vec{b} and vec{c} are aligned
        this->co_line_search( f_paramPenalizer_r.m_aligned );
        retflag << ReturnCode::PROCESSED_ALIGNED;

        // update event counts
        tree::evtcnt().increment_line_search_eval_num_aligned( f_paramPenalizer_r.m_aligned.num_features() * 
                                                               this->num_data() );
    }

    return retflag;
}



template <typename split_stat_T>
inline ret_flags_t
CLineSearch<split_stat_T>::run()
{
    ret_flags_t retflag = m_retflag;
    tree_assert( false == retflag.has_error(), "called run on errornous configuraiton" );


    switch (m_paramPenalizer_p->m_penalizerType_e)
    {
        case EPenalizerType::NO_PENALIZER:
            this->co_line_search( tree::CNoPenalizer() );
            break;
        case EPenalizerType::L1L2_PENALIZER:
            this->co_line_search( m_paramPenalizer_p->m_l1l2Penalizer );
            break;
        case EPenalizerType::L1L2_VECT_PENALIZER:
            this->vector_line_search( m_paramPenalizer_p->m_l1l2VectPenalizer );
            break;
        case EPenalizerType::NORMALIZED_L1_PENALIZER:
            this->co_line_search( m_paramPenalizer_p->m_normalizedL1Penalizer );
            break;
        case EPenalizerType::NORMALIZED_L1_VECT_PENALIZER:
            this->vector_line_search( m_paramPenalizer_p->m_normalizedL1VectPenalizer );
            break;
        default:
            retflag << ReturnCode::INVALID_INPUT;
            break;
    }

    return retflag;
}


}  // anonymous namespace
}  // namespace ctypes


extern "C"
int32_t
fern_line_search_on_entropy( const sample_classification_data_t*  f_sampleClData_p,
                             const float32_t*                     f_prmValCandidates_p,
                             const ctypes::param_penalizer_t*     f_paramPenalizer_p,
                             ctypes::line_search_result_t*        f_optimRes_p )
{
    using namespace ctypes;
    
    CLineSearch<tree::CTreeSplitEntropyStatistics> l_lsearchAlgo( f_sampleClData_p,
                                                                  f_prmValCandidates_p,
                                                                  f_paramPenalizer_p,
                                                                  f_optimRes_p );
    
    if (l_lsearchAlgo.has_error())
        return l_lsearchAlgo.m_retflag;
    
    l_lsearchAlgo.run();

    return static_cast<int32_t>( l_lsearchAlgo.m_retflag << ReturnCode::ENTROPY_RATING );
}



extern "C"
int32_t
fern_line_search_on_gini( const sample_classification_data_t*  f_sampleClData_p,
                          const float32_t*                     f_prmValCandidates_p,
                          const ctypes::param_penalizer_t*     f_paramPenalizer_p,   
                          ctypes::line_search_result_t*        f_optimRes_p )
{
    using namespace ctypes;
    
    CLineSearch<tree::CTreeSplitGiniStatistics> l_lsearchAlgo( f_sampleClData_p,
                                                               f_prmValCandidates_p,
                                                               f_paramPenalizer_p,
                                                               f_optimRes_p );
    
    if (l_lsearchAlgo.has_error())
        return l_lsearchAlgo.m_retflag;
    
    l_lsearchAlgo.run();

    return static_cast<int32_t>( l_lsearchAlgo.m_retflag << ReturnCode::GINI_RATING );
}


