/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "ctypes_base_split_optimization.hpp"
#include <cassert>
#include <new>
#include <tree.hpp>


namespace ctypes
{

inline constexpr ReturnCode split_optim_code( ESplitOptimMethod f_val ) 
{ 
    switch (f_val)
    {
        case ESplitOptimMethod::ENTROPY_OPTIMIZATION:
            return  ReturnCode::ENTROPY_RATING;

        case ESplitOptimMethod::GINI_OPTIMIZATION:
            return  ReturnCode::GINI_RATING;

        default:
            tree_assert( false, "This should never happen" );
            return ReturnCode::UNKNOWN_ERROR;
    }
}


// ------------------------------------------------------------------------------------------------------------------------



template <typename split_optimizer_T>
inline split_optimizer_T* base_create_split_optimizer( int32_t f_numLabels_i32, int32_t f_numNodes_i32 )
{
    split_optimizer_T* l_optimizer_p = 
            new (std::nothrow) split_optimizer_T( ctypes::ESplitOptimMethod::GINI_OPTIMIZATION,
                                                  f_numLabels_i32,
                                                  f_numNodes_i32 );

    if (nullptr == l_optimizer_p)
        return NULL;
    
    if ( false == l_optimizer_p->validPointer() )
    {
        delete l_optimizer_p;
        return NULL;
    }

    return l_optimizer_p;
}



template <typename split_optimizer_T>
inline void base_destruct_split_optimizer( split_optimizer_T** f_soptim_p )
{
    if (*f_soptim_p != NULL)
    {
        delete *f_soptim_p;
        *f_soptim_p = NULL;
    }
}



template <typename split_optimizer_T>
inline ret_flags_t base_initialize_parent_layer_statistic( split_optimizer_T*         f_soptim_p,
                                                           const node_label_list_t*   f_data_p )
{
    if ( ( nullptr == f_soptim_p) || (nullptr == f_data_p) || 
         ( nullptr == f_data_p->labelIdx ) || 
         ( nullptr == f_data_p->nodeIdx ) || 
         ( nullptr == f_data_p->dataWeight ) )
    {
        return ReturnCode::INVALID_POINTERS;
    }
    
    f_soptim_p->initialize_parent_layer_statistic( f_data_p );

    return split_optim_code( f_soptim_p->m_optimMethod_e );
}



template <typename split_optimizer_T>
inline int32_t base_process_feature( split_optimizer_T*             f_soptim_p,
                                     const node_label_data_list_t*  f_data_p )
{
    if ( ( nullptr == f_soptim_p) || (nullptr == f_data_p) || 
         ( nullptr == f_data_p->sampleClData.labelIdx ) || 
         ( nullptr == f_data_p->featureValues ) || 
         ( nullptr == f_data_p->sampleClData.nodeIdx ) || 
         ( nullptr == f_data_p->sampleClData.dataWeight ) )
    {
        return ret_flags_t( ReturnCode::INVALID_POINTERS );
    }

    return f_soptim_p->process_feature( f_data_p );
}



// -------------------------------------------------------------------------------------------------------------------------



template <typename giniOptim_T, typename entropyOptim_T>
inline
CBaseSplitOptimizer<giniOptim_T,entropyOptim_T>::CBaseSplitOptimizer( ESplitOptimMethod f_optim_e, int32_t f_numLabels_i32, int32_t f_numNodes_i32 )
    : m_giniOptim_p( nullptr )
    , m_optimMethod_e( f_optim_e )
{
    switch ( m_optimMethod_e )
    {
        case ESplitOptimMethod::ENTROPY_OPTIMIZATION:
            m_entropyOptim_p = new (std::nothrow) entropyOptim_t( f_numLabels_i32, f_numNodes_i32 );
            break;
        case ESplitOptimMethod::GINI_OPTIMIZATION:
            m_giniOptim_p = new (std::nothrow) giniOptim_t( f_numLabels_i32, f_numNodes_i32 );
            break;
        default:
        {
            assert(false);
        }
    }
}


template <typename giniOptim_T, typename entropyOptim_T>
inline
CBaseSplitOptimizer<giniOptim_T,entropyOptim_T>::~CBaseSplitOptimizer()
{
    switch ( m_optimMethod_e )
    {
        case ESplitOptimMethod::ENTROPY_OPTIMIZATION:
            delete m_entropyOptim_p;
            m_entropyOptim_p = nullptr;
            break;
        case ESplitOptimMethod::GINI_OPTIMIZATION:
            delete m_giniOptim_p;
            m_giniOptim_p = nullptr;
            break;
        default:
        {
            assert(false);
        }
    }
}


template <typename giniOptim_T, typename entropyOptim_T>
inline
void CBaseSplitOptimizer<giniOptim_T,entropyOptim_T>::initialize_parent_layer_statistic( const node_label_list_t* f_data_p )
{
    static_assert( sizeof(int32_t) == sizeof(tree::nodeIdx_t) );
    const tree::nodeIdx_t* l_nodeIter     = tree::nodeIdx_t::cast_from( f_data_p->nodeIdx );
    const int32_t*         l_labelIdxIter = f_data_p->labelIdx;
    const int32_t*         l_weightIter   = f_data_p->dataWeight;

    const int32_t* l_labelIdxIterFinal = nullptr;

    switch ( m_optimMethod_e )
    {
        case ESplitOptimMethod::ENTROPY_OPTIMIZATION:
            l_labelIdxIterFinal = m_entropyOptim_p->m_parentStatistic.initialize_from(
                                                        l_nodeIter,
                                                        std::next( l_nodeIter, f_data_p->numData ),
                                                        l_labelIdxIter,
                                                        l_weightIter );
            break;
        case ESplitOptimMethod::GINI_OPTIMIZATION:
            l_labelIdxIterFinal = m_giniOptim_p->m_parentStatistic.initialize_from(
                                                        l_nodeIter,
                                                        std::next( l_nodeIter, f_data_p->numData ),
                                                        l_labelIdxIter,
                                                        l_weightIter );
            break;
        default:
        {
            assert(false);
        }
    }

    tree_assert( l_labelIdxIterFinal == std::next( l_labelIdxIter, f_data_p->numData ),
                 "Inconsistent label iterator ending" );
}



template <typename giniOptim_T, typename entropyOptim_T>
inline int32_t
CBaseSplitOptimizer<giniOptim_T,entropyOptim_T>::process_feature( const node_label_data_list_t* f_data_p )
{
    const float32_t*       l_featureIter  = f_data_p->featureValues;
    const tree::nodeIdx_t* l_nodeIter     = tree::nodeIdx_t::cast_from( f_data_p->sampleClData.nodeIdx );
    const int32_t*         l_labelIdxIter = f_data_p->sampleClData.labelIdx;
    const int32_t*         l_weightIter   = f_data_p->sampleClData.dataWeight;


    int32_t num_improvements = -1;

    switch ( m_optimMethod_e )
    {
        case ESplitOptimMethod::ENTROPY_OPTIMIZATION:
            
            // Initialize Feature with Parent Statistic
            m_entropyOptim_p->m_optimizer.initialize_feature_run(
                    f_data_p->featureTypeIdx, 
                    m_entropyOptim_p->m_parentStatistic );
            
            num_improvements = m_entropyOptim_p->m_optimizer.process_unsorted_feature( 
                                        l_featureIter, std::next( l_featureIter, f_data_p->sampleClData.numData ),
                                        l_labelIdxIter, l_nodeIter, l_weightIter );
            break;

        case ESplitOptimMethod::GINI_OPTIMIZATION:
            
            // Initialize Feature with Parent Statistic
            m_giniOptim_p->m_optimizer.initialize_feature_run(
                    f_data_p->featureTypeIdx, 
                    m_giniOptim_p->m_parentStatistic );
            
            num_improvements = m_giniOptim_p->m_optimizer.process_unsorted_feature( 
                                        l_featureIter, std::next( l_featureIter, f_data_p->sampleClData.numData ),
                                        l_labelIdxIter, l_nodeIter, l_weightIter );
            break;

        default:
        {
            assert(false);
        }
    }

    return num_improvements;
}




template <typename giniOptim_T, typename entropyOptim_T>
inline
size_t CBaseSplitOptimizer<giniOptim_T,entropyOptim_T>::n_nodes() const
{
    switch ( m_optimMethod_e )
    {
        case ESplitOptimMethod::ENTROPY_OPTIMIZATION:
            return m_entropyOptim_p->m_optimizer.n_nodes();
            break;
        case ESplitOptimMethod::GINI_OPTIMIZATION:
            return m_giniOptim_p->m_optimizer.n_nodes();
            break;
        default:
        {
            assert(false);
            return 0;
        }
    }
}


template <typename giniOptim_T, typename entropyOptim_T>
inline
bool CBaseSplitOptimizer<giniOptim_T,entropyOptim_T>::validPointer() const
{
    return ( (m_entropyOptim_p != nullptr) || (m_giniOptim_p != nullptr) );
}


} // namespace ctypes


