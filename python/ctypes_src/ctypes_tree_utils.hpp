/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "ctypes_types.hpp"
#include <simd.hpp>


namespace ctypesutil
{

// -------------------------------------------------------------------------------


inline constexpr
bool isBeginAligned( const box_list_t& f_boxList )
{
    
    return (    smd::is_memory_aligned( f_boxList.left_pi32 ) 
             && smd::is_memory_aligned( f_boxList.up_pi32 )
             && smd::is_memory_aligned( f_boxList.right_pi32 )
             && smd::is_memory_aligned( f_boxList.down_pi32 ) );
}


// -------------------------------------------------------------------------------

inline constexpr
bool isBeginAligned( const box_feature_list_t& f_boxList )
{
    return (    isBeginAligned( f_boxList.first ) 
             && isBeginAligned( f_boxList.second )
             && ( ( NULL == f_boxList.lambda ) || smd::is_memory_aligned( f_boxList.lambda ) ) );
}


inline constexpr
bool isAligned( const box_feature_list_t& f_boxList )
{
    return (    isBeginAligned( f_boxList )
             && smd::is_memory_aligned( f_boxList.first.left_pi32 + f_boxList.numBoxPairs )  // if one index array is endAligned, the other are also
             && ( ( NULL == f_boxList.lambda ) || smd::is_memory_aligned( f_boxList.lambda +  f_boxList.numBoxPairs ) ) );
}



} // namespace ctypesutil


