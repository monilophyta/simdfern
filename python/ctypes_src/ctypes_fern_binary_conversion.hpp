/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <type_traits>
#include "ctypes_tree.hpp"
#include "ctypes_types_base.hpp"




struct bmap_result_data_t
{
    int32_t      numData;
    int8_t*      planeSide;
    
    union
    {
        int32_t*     giniWeight;
        float32_t*   entropyWeight;
    };
};
static_assert( std::is_standard_layout<bmap_result_data_t>::value );



extern "C"
DLL_PUBLIC 
int32_t
calc_entropy_based_binary_cl_weights(
        const node_label_list_t*  f_bmapInput_p,
        const uint8_t             f_layerBitIdx_u8,
              bmap_result_data_t* f_bmapOutput_p );


extern "C"
DLL_PUBLIC 
int32_t
calc_gini_based_binary_cl_weights(
        const node_label_list_t*  f_bmapInput_p,
        const uint8_t             f_layerBitIdx_u8,
              bmap_result_data_t* f_bmapOutput_p );

