/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <type_traits>
#include <algorithm>
#include <tree.hpp>
#include "ctypes_tree.hpp"


static inline
smd::array_view_ci32_t 
get_feature_idx_array( const indexed_tree_layer_t& f_treeLayer_r );


static inline
smd::array_view_cf32_t 
get_split_val_array( const indexed_tree_layer_t& f_treeLayer_r );


static inline smd::bundle_iterator_cf32_t
make_iterator_bundle( const feature_rows_t& f_featureRows_r );



// --------------------------------------------------------------------------------------------


extern "C"
int32_t 
classify_samples( const indexed_tree_layer_t*  f_treeLayer_p,
                  const feature_rows_t*        f_featureRows_p,
                  const int32_t*               f_inNodeIdx_p,
                        int32_t*               f_outNodeIdx_p )
{
    //typedef smd::bundle_iterator_cf32_t    bundle_iterator_t;
    typedef tree::nodeIdx_t                          index_t;
    using namespace ctypes;

    // -------------------------------------------------------------------------------------
    // Input check

    // -------------------------------------------------------------------------------------
    // Processing

    const tree::CIndexedTreeLayer l_indexedTreeLayer( get_feature_idx_array( *f_treeLayer_p ),
                                                      get_split_val_array( *f_treeLayer_p ) );
    

    l_indexedTreeLayer.classify_data_sequence( index_t::cast_from( f_inNodeIdx_p ),
                                               index_t::cast_from( f_inNodeIdx_p ) + f_featureRows_p->numData,
                                               make_iterator_bundle( *f_featureRows_p ),
                                               index_t::cast_from( f_outNodeIdx_p ) ); 
    
    return 0;
}

// --------------------------------------------------------------------------------------------
// static functions



static inline
smd::array_view_ci32_t  
get_feature_idx_array( const indexed_tree_layer_t& f_treeLayer_r )
{
    return smd::wrap_array<const int32_t>( f_treeLayer_r.featureIdx,
                                           f_treeLayer_r.numSplits );   // [ K ]
}


static inline
smd::array_view_cf32_t
get_split_val_array( const indexed_tree_layer_t& f_treeLayer_r )
{
    return smd::wrap_array<const float32_t>( f_treeLayer_r.featureSplit,
                                             f_treeLayer_r.numSplits );   // [ K ]
}


static inline smd::bundle_iterator_cf32_t
make_iterator_bundle( const feature_rows_t& f_featureRows_r )
{
    typedef smd::bundle_iterator_cf32_t bundle_iterator_t;
    
    std::function<const float32_t*(size_t)> row_gen_kernel = 
        [&f_featureRows_r] ( size_t rIdx ) -> const float32_t*
        {
            return const_cast<const float32_t*>( f_featureRows_r.featureRows[ rIdx ] );
        };
    
    bundle_iterator_t it = smd::make_iterator_bundle( f_featureRows_r.numFeatures, row_gen_kernel, 0 );
    
    return it;
}
