/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <cstdint>


namespace ctypes
{

enum class ReturnCode : std::int32_t
{
    ENTROPY_RATING          =  1 <<  0,
    GINI_RATING             =  1 <<  1,

    INT32_INT_IMG           =  1 <<  2,
    FLOAT32_INT_IMG         =  1 <<  3,

    BOX_DIFF_FEATURE        =  1 <<  4,
    BOX_RATIO_FEATURE       =  1 <<  5,

    NON_ALIGNED_IN_OUT      =  1 <<  6,
    ALIGNED_IN_OUT          =  1 <<  7,

    PROCESSED_ALIGNED       =  1 <<  8,
    PROCESSED_NONALIGNED    =  1 <<  9,

    NONMATCHING_ARRAY_SIZES =  1 << 10,
    NONALIGNED_INPUT        =  1 << 11,
    NONALIGNED_ARRAY_SIZE   =  1 << 12,


    CODE_MASK               = (1 << 13) - 1,

    //-----------------------------------

    NOERROR                 =  0,
    
    INVALID_OUTPUT_SIZE     =  1 << 15,
    INVALID_POINTERS        =  1 << 16,
    UNKNOWN_ERROR           =  1 << 17,

    INVALID_INPUT_SIZE      =  1 << 18,
    INVALID_INPUT           =  1 << 19,

    ERROR_MASK              = (1 << 20) - 1 - ((1 << 15) - 1)   //0b011111000000000   // Bits used for errors
};



class CReturnFlags
{

private:

    std::int32_t  m_code_i32;

public:


    inline CReturnFlags()
        : m_code_i32(0)
    {}

    inline CReturnFlags( ReturnCode f_rcode_e )
        : m_code_i32( static_cast<std::int32_t>( f_rcode_e  ) )
    {}

    
    inline operator std::int32_t()
    {
        return (-m_code_i32);  // return Code are always negativ
    }


    inline CReturnFlags& operator<<( CReturnFlags f_rvalue )
    {
        m_code_i32 = m_code_i32 | f_rvalue.m_code_i32;
        return (*this);
    }

    inline CReturnFlags& operator<<( ReturnCode f_rcode_e )
    {
        m_code_i32 = m_code_i32 | static_cast<std::int32_t>(f_rcode_e);
        return (*this);
    }

    inline bool has_error() const
    {
        return ( 0 != (m_code_i32 & static_cast<std::int32_t>(ReturnCode::ERROR_MASK)) );
    }
};

typedef CReturnFlags  ret_flags_t;



}  // namespace ctypes
