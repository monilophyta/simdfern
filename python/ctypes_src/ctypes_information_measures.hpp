/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "ctypes_tree.hpp"
#include "ctypes_types_base.hpp"


extern "C"
DLL_PUBLIC
int32_t
calc_unormed_conditional_entropy( 
        const node_label_list_t*  f_bmapInput_p,
              float32_t*          f_unormedCondEntropy_p );


extern "C"
DLL_PUBLIC
int32_t
calc_unormed_gini_index( 
        const node_label_list_t*  f_bmapInput_p,
              int32_t*            f_unormedGini_p );
