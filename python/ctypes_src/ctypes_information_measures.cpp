/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include <tree.hpp>
#include "ctypes_information_measures.hpp"
#include "ctypes_tree.hpp"
#include "ctypes_return_codes.hpp"



// ------------------------------------------------------------------------
// forward declarations


template <typename OUT_T>
static ctypes::ret_flags_t
check_input(         
        const node_label_list_t*  f_bmapInput_p,
                          OUT_T*  f_output_p );

// ------------------------------------------------------------------------------------


extern "C"
int32_t
calc_unormed_gini_index( 
        const node_label_list_t*  f_bmapInput_p,
              int32_t*            f_unormedGini_p )
{
    ctypes::ret_flags_t ret_flags;
    ret_flags << check_input( f_bmapInput_p,
                              f_unormedGini_p );
    
    if ( ret_flags.has_error() )
        return ret_flags;
    
    /// calculate gini index statistics
    tree::CTreeSplitGiniStatistics l_giniStat( f_bmapInput_p->numLabels, f_bmapInput_p->numLeafs );
    {
        auto labelItEnd = l_giniStat.initialize_from(
                                      tree::nodeIdx_t::cast_from( f_bmapInput_p->nodeIdx ), 
                                      tree::nodeIdx_t::cast_from( f_bmapInput_p->nodeIdx ) + f_bmapInput_p->numData,
                                      f_bmapInput_p->labelIdx,
                                      f_bmapInput_p->dataWeight );
        tree_bounds_check( labelItEnd == (f_bmapInput_p->labelIdx + f_bmapInput_p->numData) );
    }

    /// fill output
    for (int32_t lidx=0; lidx < f_bmapInput_p->numLeafs; ++lidx )
    {
        f_unormedGini_p[lidx] = l_giniStat.unormed_gini_coefficient( lidx );
    }

    return ret_flags;
}



extern "C"
int32_t
calc_unormed_conditional_entropy( 
        const node_label_list_t*  f_bmapInput_p,
              float32_t*          f_unormedCondEntropy_p )
{
    ctypes::ret_flags_t ret_flags;
    ret_flags << check_input( f_bmapInput_p,
                              f_unormedCondEntropy_p );
    
    if ( ret_flags.has_error() )
        return ret_flags;
    
    /// calculate gini index statistics
    tree::CTreeSplitEntropyStatistics l_entropyStat( f_bmapInput_p->numLabels, f_bmapInput_p->numLeafs );
    {
        auto labelItEnd = l_entropyStat.initialize_from(
                                      tree::nodeIdx_t::cast_from( f_bmapInput_p->nodeIdx ), 
                                      tree::nodeIdx_t::cast_from( f_bmapInput_p->nodeIdx ) + f_bmapInput_p->numData,
                                      f_bmapInput_p->labelIdx,
                                      f_bmapInput_p->dataWeight );
        tree_bounds_check( labelItEnd == (f_bmapInput_p->labelIdx + f_bmapInput_p->numData) );
    }

    /// fill output
    for (int32_t lidx=0; lidx < f_bmapInput_p->numLeafs; ++lidx )
    {
        f_unormedCondEntropy_p[lidx] = l_entropyStat.node_rating( lidx );
    }

    return ret_flags;
}


// -------------------------------------------------------------------------------------


template <typename OUT_T>
static ctypes::ret_flags_t
check_input(         
        const node_label_list_t*  f_bmapInput_p,
                          OUT_T*  f_output_p )
{
    using namespace ctypes;


    if ( (nullptr == f_bmapInput_p) || 
         (nullptr == f_output_p) || 
         (nullptr == f_bmapInput_p->nodeIdx) || 
         (nullptr == f_bmapInput_p->labelIdx) ||
         (nullptr == f_bmapInput_p->dataWeight) )
    {
        return ctypes::ret_flags_t( ctypes::ReturnCode::INVALID_POINTERS );
    }

    ctypes::ret_flags_t ret_flags;

    if ( ( 0 > f_bmapInput_p->numLabels ) || ( 0 > f_bmapInput_p->numLeafs ) )
    {
        ret_flags << ctypes::ReturnCode::INVALID_INPUT;
    }

    if ( 0 > f_bmapInput_p->numData )
    {
        ret_flags << ctypes::ReturnCode::INVALID_INPUT_SIZE;
    }

    return ret_flags;
}
