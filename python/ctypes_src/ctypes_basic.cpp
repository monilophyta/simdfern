/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "ctypes_tree.hpp"
#include <simd.hpp>
#include <simd_cfg.inl>



extern "C"
int32_t num_simd_vector_items()
{
    static_assert( smd::vf32_t::num_items() == smd::vi32_t::num_items() );
    static_assert( smd::vb32_t::num_items() == smd::vi32_t::num_items() );

    return static_cast<int32_t>( smd::vf32_t::num_items() );
}

