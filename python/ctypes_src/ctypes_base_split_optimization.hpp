/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdint>
#include <type_traits>
#include "ctypes_types_base.hpp"
#include "ctypes_types.hpp"
#include "ctypes_return_codes.hpp"



//------------------------------------------------------------------------------------


struct node_label_data_list_t
{
    sample_classification_data_t   sampleClData;
    float32_t*                     featureValues;
    int32_t                        featureTypeIdx;
};
static_assert( std::is_standard_layout<node_label_data_list_t>::value );



//------------------------------------------------------------------------------------


namespace ctypes
{



enum class ESplitOptimMethod : bool
{
    ENTROPY_OPTIMIZATION = false,
       GINI_OPTIMIZATION = true
};



inline constexpr ReturnCode split_optim_code( ESplitOptimMethod f_val );


// -----------------------------------------------------------------------------------------------------


template <typename split_optimizer_T>
inline split_optimizer_T* base_create_split_optimizer( int32_t f_numLabels_i32, int32_t f_numNodes_i32 );


template <typename split_optimizer_T>
inline void base_destruct_split_optimizer( split_optimizer_T** f_soptim_p );


template <typename split_optimizer_T>
inline ret_flags_t base_initialize_parent_layer_statistic( split_optimizer_T*         f_soptim_p,
                                                           const node_label_list_t*   f_data_p );


template <typename split_optimizer_T>
inline int32_t base_process_feature( split_optimizer_T*             f_soptim_p,
                                     const node_label_data_list_t*  f_data_p );



// -----------------------------------------------------------------------------------------------------


template <typename optim_T, typename stat_T>
struct CSplitOptimizerTuple
{
    optim_T       m_optimizer;
    stat_T        m_parentStatistic;

    inline
    CSplitOptimizerTuple( int32_t f_numLabels_i32, int32_t f_numNodes_i32 )
        : m_optimizer( f_numLabels_i32, f_numNodes_i32 )
        , m_parentStatistic( f_numLabels_i32, f_numNodes_i32 )
    {}
};




template <typename giniOptim_T, typename entropyOptim_T>
struct CBaseSplitOptimizer
{
    typedef giniOptim_T          giniOptim_t;
    typedef entropyOptim_T    entropyOptim_t;


    union 
    {
        giniOptim_t*    m_giniOptim_p;
        entropyOptim_t* m_entropyOptim_p;
    };
    
    const ESplitOptimMethod  m_optimMethod_e;


    inline CBaseSplitOptimizer( ESplitOptimMethod f_optimMethod_e, int32_t f_numLabels_i32, int32_t f_numNodes_i32 );
    inline ~CBaseSplitOptimizer();

    inline void initialize_parent_layer_statistic( const node_label_list_t* f_data_p );
    
    inline int32_t process_feature( const node_label_data_list_t* f_data_p );

    inline size_t n_nodes() const;
    inline bool validPointer() const;
};



}  // namespace ctypes

