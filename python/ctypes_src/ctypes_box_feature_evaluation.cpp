/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include "ctypes_box_feature_evaluation.hpp"
#include "ctypes_tree_utils.hpp"
#include "ctypes_return_codes.hpp"
#include <iterator>




//----------------------------------------------------------------------------------------------------------------


extern "C"
int32_t
evaluate_box_features( const intimg_t*           f_intImg_p,
                       const box_feature_list_t* f_boxFeatures_p,
                       const img_coord_list_t*   f_imgCoords_p,
                       float32_t*                f_featuresOut_p )
{
    /// check for memory alignments
    const bool l_boxesAligned_b = ctypesutil::isAligned( *f_boxFeatures_p );  // TODO: modify to that boxes do not really need to be aligned!
    const bool l_outAligned_b   = smd::is_memory_aligned( f_featuresOut_p );
    const bool l_isAligned_b    = l_boxesAligned_b && l_outAligned_b;

    
    /// cast box coordinates
    ctypes::img_coords_t l_imgCoords;
    {
        static_assert( sizeof( img::image_coord_i32_t ) == sizeof( img_coord_t ) );
        const img::image_coord_i32_t* l_coordBegin_p = 
                                reinterpret_cast<img::image_coord_i32_t*>( f_imgCoords_p->coords );
        
        l_imgCoords = std::make_pair( l_coordBegin_p, l_coordBegin_p + f_imgCoords_p->numCoords ) ;
    }
    

    ctypes::ret_flags_t retflags;

    if ( true == l_isAligned_b )
    {   // Aligned: Vectorized Computation
        
        /// Build Output Data Structure
        smd::vf32_t* f_featuresOut_vp = reinterpret_cast<smd::vf32_t*>( f_featuresOut_p );
        typedef ctypes::CBoxFeatureEvaluator< smd::vf32_t, tree::EUseSIMD::USE_SIMD_FLOAT32>  boxfeature_eval_t;
        
        retflags << boxfeature_eval_t( l_imgCoords, f_intImg_p->intimg_descr, f_featuresOut_vp ).evaluate( *f_intImg_p, *f_boxFeatures_p );
        retflags << ctypes::ReturnCode::ALIGNED_IN_OUT;
    }
    else
    {   // Nonaligned => element-wise computation
        typedef ctypes::CBoxFeatureEvaluator< float32_t, tree::EUseSIMD::USE_ELEM_FLOAT32 >  boxfeature_eval_t;

        retflags << boxfeature_eval_t( l_imgCoords, f_intImg_p->intimg_descr, f_featuresOut_p ).evaluate( *f_intImg_p, *f_boxFeatures_p );
        retflags << ctypes::ReturnCode::NON_ALIGNED_IN_OUT;
    }

    return retflags;
}



//----------------------------------------------------------------------------------------------------------------

namespace ctypes
{


template <typename OUT_T, tree::EUseSIMD USE_SIMD>
inline
CBoxFeatureEvaluator<OUT_T,USE_SIMD>::CBoxFeatureEvaluator( const img_coords_t&      f_imgCoords,
                                                            const image_descr_t&     f_intImgDescr_r,
                                                            OUT_T*                   f_featuresOut_p )
    : m_imgCoords_r( f_imgCoords )
    , m_imageFrame( f_intImgDescr_r.nImgRows,
                    f_intImgDescr_r.nImgCols,
                    0,
                    f_intImgDescr_r.rowStride,
                    f_intImgDescr_r.colStride )
    , m_featuresOut_p( f_featuresOut_p )
{}


template <typename OUT_T, tree::EUseSIMD USE_SIMD>
inline
ret_flags_t CBoxFeatureEvaluator<OUT_T,USE_SIMD>::evaluate( const intimg_t&               f_intImg_r,
                                                            const box_feature_list_t&     f_boxFeatures_r )
{
    ret_flags_t retflags;

    if (true == f_intImg_r.isFloat32Img)
    {
        const img::image_cf32_ptr_t l_imagePointer = 
                        m_imageFrame.initialize_memory_pointer( f_intImg_r.mem_pf32 );
        
        retflags << select_feature_type( l_imagePointer, f_boxFeatures_r );
        retflags << ReturnCode::FLOAT32_INT_IMG;
    }
    else
    {
        const img::image_ci32_ptr_t l_imagePointer = 
                        m_imageFrame.initialize_memory_pointer( f_intImg_r.mem_pi32 );
        
        retflags << select_feature_type( l_imagePointer, f_boxFeatures_r );
        retflags << ReturnCode::INT32_INT_IMG;
    }

    return retflags;
}



template <typename OUT_T, tree::EUseSIMD USE_SIMD>
template <typename IMG_T>
inline
ret_flags_t CBoxFeatureEvaluator<OUT_T,USE_SIMD>::select_feature_type( const img::CImagePointer<IMG_T>&  f_imagePointer_r,
                                                                       const box_feature_list_t&         f_boxFeatures_r )
{
    tree_assert( (USE_SIMD != tree::EUseSIMD::USE_SIMD_FLOAT32) || 
                 ( 0 == ( f_boxFeatures_r.numBoxPairs & (smd::vf32_t::num_items() - 1) ) ),
                 "Number of Box items not vecotorizabe but vetorization is happening" );
    
    
    ctypes::ret_flags_t retflags;
    OUT_T* l_featureEnd = NULL;

    /// for feature index type
    typedef typename std::conditional<USE_SIMD == tree::EUseSIMD::USE_SIMD_FLOAT32, 
                                         smd::vi32_t, 
                                         int32_t>::type idx_t;

    /// differentiate between box difference and ratio features
    const bool l_useRatioFeature_b = (NULL == f_boxFeatures_r.lambda);

    /// number of box pair items - number of items is smaller when they are vectorized
    const size_t l_numBoxPairItems = (USE_SIMD == tree::EUseSIMD::USE_SIMD_FLOAT32) 
                                       ? f_boxFeatures_r.numBoxPairs / smd::vf32_t::num_items()
                                       : f_boxFeatures_r.numBoxPairs;

    if (true ==l_useRatioFeature_b)
    {
        tree::boxratio_eval_t<IMG_T,USE_SIMD> l_boxEvaluator( m_imageFrame, 
                                                              f_imagePointer_r,
                                                              l_numBoxPairItems );
        
        // fill integral boxes into evaluater
        emplace_box_features( l_boxEvaluator, f_boxFeatures_r, idx_t(), tree::dummy_param_t<0>() );

        // Evaluate Features
        l_featureEnd = l_boxEvaluator.evaluate_locations( m_imgCoords_r.first, m_imgCoords_r.second, m_featuresOut_p );

        retflags << ctypes::ReturnCode::BOX_RATIO_FEATURE;
    }
    else
    {
        /// decide for vectorized float or simple float typeof parameter
        typedef typename std::conditional< USE_SIMD == tree::EUseSIMD::USE_SIMD_FLOAT32, 
                                           smd::vf32_t, float32_t>::type            param_t;
        
        tree::boxdiff_eval_t<IMG_T,USE_SIMD> l_boxEvaluator( m_imageFrame, 
                                                             f_imagePointer_r,
                                                             l_numBoxPairItems );
        
        // fill integral boxes into evaluater
        emplace_box_features( l_boxEvaluator, f_boxFeatures_r, idx_t(), param_t() );
        
        // Evaluate Features
        l_featureEnd = l_boxEvaluator.evaluate_locations( m_imgCoords_r.first, m_imgCoords_r.second, m_featuresOut_p );
        
        retflags << ctypes::ReturnCode::BOX_DIFF_FEATURE;
    }

    tree_assert( static_cast<std::ptrdiff_t>( ( std::distance(m_featuresOut_p, l_featureEnd) * sizeof(OUT_T) ) / sizeof(float32_t) ) == 
                 static_cast<std::ptrdiff_t>( f_boxFeatures_r.numBoxPairs * std::distance(m_imgCoords_r.first, m_imgCoords_r.second) ),
                 "Inconsistent number of features in output" );

    return retflags;
}



//----------------------------------------------------------------------------------------------------------------



template <typename Ts, typename BoxEval_T, typename IDX_T, typename PARAM_T>
static inline
void emplace_box_features( tree::CBoxFeatureEvaluatorBase<Ts, BoxEval_T>& f_boxEval,
                           const box_feature_list_t&                      f_boxFeatures_r,
                           IDX_T, PARAM_T )
{
    // Cast to correct evaluation type
    BoxEval_T& l_boxEval = static_cast<BoxEval_T&>( f_boxEval );

    static const size_t l_idxStep = sizeof( IDX_T ) / sizeof( int32_t );
    static_assert( 0 ==  ( sizeof( IDX_T ) % sizeof( int32_t ) ), "Integral and vector types are incompatible" );

    static const size_t l_ParamStep = sizeof( PARAM_T ) / sizeof( float32_t );
    static_assert( (0 == l_ParamStep) || (0 == ( sizeof( PARAM_T ) % sizeof( float32_t ) )),
                  "Invalid feature parameter" );
    
    tree_assert( ( 0 == l_ParamStep) == ( NULL == f_boxFeatures_r.lambda ), 
                   "Inconsistent feature parameter array initialization" );

    /// decide between vectorized and fundamental box type
    typedef typename std::conditional< smd::is_simd<IDX_T>::value,
                                       img::box_vi32_cpt,            
                                       img::box_i32_cpt
                                     >::type  box_t;

    /// initialize iterator for feature parameters (lambda)
    static const float32_t l_dummy_float = std::numeric_limits<float32_t>::signaling_NaN();
    const float32_t* lambdaIter = ( 0 == l_ParamStep ) 
                                    ? &l_dummy_float               // bloody hack!
                                    : f_boxFeatures_r.lambda;
    
    /// loop over all box corners
    const int32_t* left1    = f_boxFeatures_r.first.left_pi32;
    const int32_t* left1End = std::next( left1, f_boxFeatures_r.numBoxPairs );
    for ( const int32_t    *up1 = f_boxFeatures_r.first.up_pi32,
                        *right1 = f_boxFeatures_r.first.right_pi32,
                         *down1 = f_boxFeatures_r.first.down_pi32,
                         
                         *left2 = f_boxFeatures_r.second.left_pi32,
                           *up2 = f_boxFeatures_r.second.up_pi32,
                        *right2 = f_boxFeatures_r.second.right_pi32,
                         *down2 = f_boxFeatures_r.second.down_pi32;
          
          // Continue while
          left1 < left1End;
          
           std::advance( left1, l_idxStep ),   std::advance( up1, l_idxStep ),
          std::advance( right1, l_idxStep ), std::advance( down1, l_idxStep ),
          
          std::advance(  left2, l_idxStep ),   std::advance( up2, l_idxStep ),
          std::advance( right2, l_idxStep ), std::advance( down2, l_idxStep ),
          
          std::advance( lambdaIter, l_ParamStep ) )
    {
        const box_t box1( reinterpret_cast<const IDX_T*>(    up1 ),
                          reinterpret_cast<const IDX_T*>(  left1 ),
                          reinterpret_cast<const IDX_T*>(  down1 ),
                          reinterpret_cast<const IDX_T*>( right1 ) );
        tree_assert( box1.verify_correctness(), "Inconsistent Box1" );
        
        const box_t box2( reinterpret_cast<const IDX_T*>(    up2 ),
                          reinterpret_cast<const IDX_T*>(  left2 ),
                          reinterpret_cast<const IDX_T*>(  down2 ),
                          reinterpret_cast<const IDX_T*>( right2 ) );
        tree_assert( box2.verify_correctness(), "Inconsistent Box2" );
        
        l_boxEval.emplace_boxpair( box1, box2, *reinterpret_cast<const PARAM_T*>( lambdaIter ) );
    }

    tree_assert( left1 == left1End, "Array Ends are not alligned" );
}


}  // namespace ctypes