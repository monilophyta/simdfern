/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <iterator>

#include "ctypes_tree.hpp"
#include "ctypes_types.hpp"
#include <tree.hpp>



extern "C"
void 
project_samples( const feature_rows_t* f_featureRows_p,
                       int32_t         f_numFeatures_u,
                 const float32_t*      f_weights_p,
                       float32_t*      f_projectedData_p,
                       ESummationAlgo  f_sumAlgo_e )
{
    tree_assert( f_numFeatures_u <= f_featureRows_p->numFeatures, "Inconsistent number of features" );

    const int32_t l_numFeatures_u = std::min( f_numFeatures_u, f_featureRows_p->numFeatures );
    
    float32_t* const l_projectedEnd_p = std::next( f_projectedData_p, f_featureRows_p->numData );
    const float32_t* l_weightsEnd_p = std::next( f_weights_p, l_numFeatures_u );
    
    
    switch (f_sumAlgo_e)
    {
        case ESummationAlgo::NAIVE:
        {
            tree::CSimpleProjection( f_projectedData_p, l_projectedEnd_p ).map( f_weights_p, l_weightsEnd_p, f_featureRows_p->featureRows );
            break;
        }
        case ESummationAlgo::KAHAN:
        {
            tree::CKahanSampleProjection( f_projectedData_p, l_projectedEnd_p ).map( f_weights_p, l_weightsEnd_p, f_featureRows_p->featureRows );
            break;
        }
        case ESummationAlgo::NEUMAIER:
        {
            tree::CNeumaierSampleProjection( f_projectedData_p, l_projectedEnd_p ).map( f_weights_p, l_weightsEnd_p, f_featureRows_p->featureRows );
            break;
        }
        case ESummationAlgo::KLEIN:
        {
            tree::CKleinSampleProjection( f_projectedData_p, l_projectedEnd_p ).map( f_weights_p, l_weightsEnd_p, f_featureRows_p->featureRows );
            break;
        }
        default:
        {
            // this should never happen
            tree_assert( false, "This should never happen" );
            std::fill( f_projectedData_p, l_projectedEnd_p, std::numeric_limits<float32_t>::signaling_NaN() );
            break;
        }
    }
}

