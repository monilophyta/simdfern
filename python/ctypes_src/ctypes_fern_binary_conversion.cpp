/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include <tree.hpp>
#include "ctypes_fern_binary_conversion.hpp"
#include "ctypes_tree.hpp"
#include "ctypes_return_codes.hpp"

// ------------------------------------------------------------------------
// forward declarations


static
ctypes::ret_flags_t
check_input( const node_label_list_t*  f_bmapInput_p,
             const uint8_t             f_layerBitIdx_u8,
                   bmap_result_data_t* f_bmapOutput_p );


template <typename converter_T, typename out_T>
static
ctypes::ret_flags_t
calc_binary_cl_weights(
        const node_label_list_t& f_bmapInput_r,
        const uint8_t            f_layerBitIdx_u8,
              int8_t*            f_planeSide_pi8,
              out_T*             f_weight_p );

// ------------------------------------------------------------------------


extern "C"
int32_t
calc_entropy_based_binary_cl_weights(
        const node_label_list_t*  f_bmapInput_p,
        const uint8_t             f_layerBitIdx_u8,
              bmap_result_data_t* f_bmapOutput_p )
{
    ctypes::ret_flags_t ret_flags;
    ret_flags << check_input( f_bmapInput_p,
                              f_layerBitIdx_u8,
                              f_bmapOutput_p );
    
    if ( ret_flags.has_error() )
        return ret_flags;
    
    return calc_binary_cl_weights<tree::multicl_to_binary_entropy_conv_t, float32_t>( 
                // Inputs
                *f_bmapInput_p,
                f_layerBitIdx_u8,
                // Outputs
                f_bmapOutput_p->planeSide,
                f_bmapOutput_p->entropyWeight );
}


extern "C"
int32_t
calc_gini_based_binary_cl_weights(
        const node_label_list_t*  f_bmapInput_p,
        const uint8_t             f_layerBitIdx_u8,
              bmap_result_data_t* f_bmapOutput_p )
{
    ctypes::ret_flags_t ret_flags;
    ret_flags << check_input( f_bmapInput_p,
                              f_layerBitIdx_u8,
                              f_bmapOutput_p );
    
    if ( ret_flags.has_error() )
        return ret_flags;
    
    return calc_binary_cl_weights<tree::multicl_to_binary_gini_conv_t, int32_t>( 
                // Inputs
                *f_bmapInput_p,
                f_layerBitIdx_u8,
                // Outputs
                f_bmapOutput_p->planeSide,
                f_bmapOutput_p->giniWeight );
}

// ------------------------------------------------------------------------


template <typename converter_T, typename out_T>
static
ctypes::ret_flags_t
calc_binary_cl_weights(
        const node_label_list_t& f_bmapInput_r,
        const uint8_t            f_layerBitIdx_u8,
              int8_t*            f_planeSide_pi8,
              out_T*             f_weight_p  )
{    
    /// Initialize leaf node iterator ending
    const int32_t* const l_leafIterEnd = f_bmapInput_r.nodeIdx + f_bmapInput_r.numData;
    
    /// Initialize converter
    converter_T l_conv( f_bmapInput_r.numLabels, f_bmapInput_r.numLeafs, f_layerBitIdx_u8 );
    {
        const int32_t* labelEnd = 
                l_conv.initialize_statistics( f_bmapInput_r.nodeIdx,
                                              l_leafIterEnd,
                                              f_bmapInput_r.labelIdx,
                                              f_bmapInput_r.dataWeight );
        
        tree_assert( labelEnd == (f_bmapInput_r.labelIdx + f_bmapInput_r.numData), 
                     "Inconsistent array ending" );
    }

    /// create binary mapping
    {
        /// Input iterators
        const int32_t* leafIter  = f_bmapInput_r.nodeIdx;
        const int32_t* labelIter = f_bmapInput_r.labelIdx;
        const int32_t* datwIter  = f_bmapInput_r.dataWeight;

        /// Output Iterators
        int8_t*  psideIter = f_planeSide_pi8;
        auto*    binwIter  = f_weight_p;

        for (; leafIter != l_leafIterEnd;   ++leafIter, ++labelIter, ++datwIter,
                                            ++psideIter, ++binwIter )
        {
            typedef typename converter_T::binary_mapping_t binary_mapping_t;
            
            /// do the actual mapping
            const binary_mapping_t bmap = l_conv.map_sample( *leafIter, *labelIter, *datwIter );

            /// fill the output
            *psideIter = static_cast<int8_t>( bmap.m_planeSide );
            *binwIter  = bmap.m_weightRating;
        }
    }

    return ctypes::ret_flags_t();
}



static
ctypes::ret_flags_t
check_input( const node_label_list_t*  f_bmapInput_p,
             const uint8_t             f_layerBitIdx_u8,
                   bmap_result_data_t* f_bmapOutput_p )
{
    using namespace ctypes;

    if ( (nullptr == f_bmapInput_p) || 
         (nullptr == f_bmapOutput_p) || 
         (nullptr == f_bmapInput_p->nodeIdx) || 
         (nullptr == f_bmapInput_p->labelIdx) ||
         (nullptr == f_bmapInput_p->dataWeight) ||
         (nullptr == f_bmapOutput_p->planeSide) ||
         (nullptr == f_bmapOutput_p->giniWeight) ||
         (nullptr == f_bmapOutput_p->entropyWeight) )
    {
        return ctypes::ret_flags_t( ctypes::ReturnCode::INVALID_POINTERS );
    }

    ctypes::ret_flags_t ret_flags;

    if ( ( 0 > f_bmapInput_p->numLabels ) || 
         ( 0 > f_bmapInput_p->numLeafs ) || 
         ( f_bmapInput_p->numLeafs < (1 << f_layerBitIdx_u8) ) )
    {
        ret_flags << ctypes::ReturnCode::INVALID_INPUT;
    }

    if ( 0 > f_bmapInput_p->numData )
    {
        ret_flags << ctypes::ReturnCode::INVALID_INPUT_SIZE;
    }

    if ( ( 0 > f_bmapOutput_p->numData) ||
         ( f_bmapInput_p->numData > f_bmapOutput_p->numData ) )
    {
        ret_flags << ctypes::ReturnCode::INVALID_OUTPUT_SIZE;
    }

    return ret_flags;
}
