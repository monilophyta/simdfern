/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <tree.hpp>
#include "ctypes_lda_cov_calculation.hpp"
#include "ctypes_tree.hpp"




static inline smd::bundle_iterator_cf32_t
make_iterator_bundle( const feature_rows_t& f_sampleData_r );


// --------------------------------------------------------------------------------------------------


extern "C" 
int32_t
calculate_lda_covariances( const node_label_list_t*      f_ldaInput_p,
                           const feature_rows_t*         f_featureRows_p,
                                 lda_stat_result_data_t* f_ldaOutput_p )
{
    using namespace tree;
    typedef smd::bundle_iterator_cf32_t    bundle_iterator_t;

    // Some debug checks
    tree_assert( f_ldaInput_p->numData == f_featureRows_p->numData, "Inconsistent number of data" );
    tree_assert( f_featureRows_p->numFeatures == f_ldaOutput_p->numFeatures, "Inconsistent number of features" );

    // handling in case of un-raised inconsistencies
    const int32_t l_numData     = std::min( f_ldaInput_p->numData, f_featureRows_p->numData );
    const int32_t l_numFeatures = std::min( f_featureRows_p->numFeatures, f_ldaOutput_p->numFeatures );

    // Create input data iterators
    bundle_iterator_t l_dataColIter = make_iterator_bundle( *f_featureRows_p );

    /// Wrap node idx
    static_assert( sizeof(int32_t) == sizeof(tree::nodeIdx_t) );
    const nodeIdx_t* l_nodeIter     = nodeIdx_t::cast_from( f_ldaInput_p->nodeIdx );

    /// other iterators
    const int32_t*   l_labelIdxIter = f_ldaInput_p->labelIdx;
    const int32_t*   l_weightIter   = f_ldaInput_p->dataWeight;

    /// set output data structure
    smd::matrix_view_f32_t l_intra_cov_af32 = smd::wrap_matrix( f_ldaOutput_p->intra_covar,
                                                                f_ldaOutput_p->numFeatures, f_ldaOutput_p->numFeatures,
                                                                f_ldaOutput_p->intra_covar_stride );


    /// set output data structure
    smd::matrix_view_f32_t l_inter_cov_af32 = smd::wrap_matrix( f_ldaOutput_p->inter_covar,
                                                                f_ldaOutput_p->numFeatures, f_ldaOutput_p->numFeatures,
                                                                f_ldaOutput_p->inter_covar_stride );


    /// statistic instance
    CMultiNodeClassStatistics multi_node_class_cov_stat( f_ldaInput_p->numLabels, 
                                                         f_ldaInput_p->numNodes,
                                                         l_numFeatures );
    
    /// process input data
    multi_node_class_cov_stat.process( l_nodeIter, l_nodeIter + l_numData,
                                       l_labelIdxIter,
                                       l_weightIter,
                                       l_dataColIter );
    
    /// fill output
    int32_t l_intraNodeWeightSum_i32 = multi_node_class_cov_stat.get_intra_covariance( l_intra_cov_af32 );
    int32_t l_interNodeWeightSum_i32 = multi_node_class_cov_stat.get_inter_covariance( l_inter_cov_af32 );
    tree_assert( l_intraNodeWeightSum_i32 == l_interNodeWeightSum_i32,
                 "Inconsistent weight statistics" );


    return l_interNodeWeightSum_i32;
}


// --------------------------------------------------------------------------------------------------



static inline smd::bundle_iterator_cf32_t
make_iterator_bundle( const feature_rows_t& f_sampleData_r )
{
    typedef smd::bundle_iterator_cf32_t bundle_iterator_t;
    
    std::function<const float32_t*(size_t)> row_gen_kernel = 
        [&f_sampleData_r] ( size_t rIdx ) -> const float32_t*
        {
            return const_cast<const float32_t*>( f_sampleData_r.featureRows[ rIdx ] );
        };
    
    bundle_iterator_t it = smd::make_iterator_bundle( f_sampleData_r.numFeatures, row_gen_kernel, 0 );
    
    return it;
}
