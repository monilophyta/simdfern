/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <tree.hpp>
#include <image.hpp>
#include "ctypes_tree.hpp"



namespace ctypes
{



typedef std::pair<const img::image_coord_i32_t*, const img::image_coord_i32_t*>  img_coords_t;



template <typename FOUT_T, tree::EUseSIMD USE_SIMD>
struct CBoxFeatureEvaluator
{

public:

    inline
    CBoxFeatureEvaluator( const img_coords_t&           f_imgCoords,
                          const image_descr_t&          f_intImgDescr_r,
                          FOUT_T*                       f_featuresOut_p );
    

    inline
    ret_flags_t evaluate( const intimg_t&               f_intImg_r,
                          const box_feature_list_t&     f_boxFeatures_r );

private:

    template <typename IMG_T>
    inline
    ret_flags_t select_feature_type( const img::CImagePointer<IMG_T>& f_imagePointer,
                                     const box_feature_list_t&        f_boxFeatures_r );
    



    const img_coords_t&           m_imgCoords_r;
    const img::image_frame_t      m_imageFrame;
    FOUT_T*                       m_featuresOut_p;
};


template <typename Ts, typename BoxEval_T, typename IDX_T, typename PARAM_T>
static inline
void emplace_box_features( tree::CBoxFeatureEvaluatorBase<Ts, BoxEval_T>& f_boxEval,
                           const box_feature_list_t&                      f_boxFeatures_r,
                           IDX_T, PARAM_T );



} // namespace ctypes