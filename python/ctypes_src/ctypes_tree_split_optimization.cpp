/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */



#include "ctypes_tree_split_optimization.hpp"
#include "ctypes_base_split_optimization.inl"
#include <cassert>



extern "C"
ctypes::tree_split_optimizer_t* tree_create_split_optimizer( int32_t f_numLabels_i32, int32_t f_numNodes_i32 )
{
    return ctypes::base_create_split_optimizer<ctypes::tree_split_optimizer_t>( f_numLabels_i32, f_numNodes_i32 );
}


extern "C"
void tree_destruct_split_optimizer( ctypes::tree_split_optimizer_t** f_soptim_p )
{
    return ctypes::base_destruct_split_optimizer( f_soptim_p );
}


extern "C"
int32_t tree_initialize_parent_layer_statistic( ctypes::tree_split_optimizer_t* f_soptim_p,
                                                const node_label_list_t*        f_data )
{
    return ctypes::base_initialize_parent_layer_statistic( f_soptim_p, f_data );
}


extern "C"
int32_t tree_process_feature( ctypes::tree_split_optimizer_t* f_soptim_p,
                              const node_label_data_list_t*   f_data )
{
    return ctypes::base_process_feature( f_soptim_p, f_data );
}


extern "C"
int32_t tree_get_optimal_splits( const ctypes::tree_split_optimizer_t* f_soptim_p,
                                 feature_split_list_t*                 f_splits )
{
    if ( (nullptr == f_soptim_p) || (nullptr == f_splits) || 
         ( nullptr == f_splits->featureIdx ) || ( nullptr == f_splits->featureSplit ) )
    {
        return ctypes::ret_flags_t( ctypes::ReturnCode::INVALID_POINTERS );
    }
    
    if ( (nullptr == f_splits->nodeGiniRating) || (nullptr == f_splits->nodeEntropy) )
    {
        return ctypes::ret_flags_t( ctypes::ReturnCode::INVALID_POINTERS );
    }

    return f_soptim_p->get_optimal_splits( f_splits );
}



//----------------------------------------------------------------------------------------------------------------


namespace ctypes
{


void CTreeSplitOptimizer::initialize_parent_layer_statistic( const node_label_list_t* f_data_p )
{
    // calling base method
    base_t::initialize_parent_layer_statistic( f_data_p );

    switch ( m_optimMethod_e )
    {
        case ESplitOptimMethod::ENTROPY_OPTIMIZATION:
            
            // Initialize node ratings with parent statistic
            m_entropyOptim_p->m_optimizer.initialize_node_ratings( m_entropyOptim_p->m_parentStatistic );
            break;

        case ESplitOptimMethod::GINI_OPTIMIZATION:
            
            // Initialize Feature with Parent Statistic
            m_giniOptim_p->m_optimizer.initialize_node_ratings( m_giniOptim_p->m_parentStatistic );
            break;

        default:
        {
            assert(false);
        }
    }
}


ret_flags_t CTreeSplitOptimizer::get_optimal_splits( feature_split_list_t* f_splits ) const
{
    if ( static_cast<int32_t>(n_nodes()) != f_splits->numSplits )
    {
        return ret_flags_t( ReturnCode::INVALID_OUTPUT_SIZE );
    }

    ret_flags_t ret_flags;
    int32_t*   l_featureIdx_p    = f_splits->featureIdx;
    float32_t* l_featureSplit_p  = f_splits->featureSplit;

    switch ( m_optimMethod_e )
    {
        case ESplitOptimMethod::ENTROPY_OPTIMIZATION:
            {
                float32_t* l_featureRating_p = f_splits->nodeEntropy;
                m_entropyOptim_p->m_optimizer.getOptimalFeatureSplits(
                                                l_featureIdx_p,
                                                l_featureSplit_p,
                                                l_featureRating_p );
                
                tree_assert( std::next(f_splits->nodeEntropy, f_splits->numSplits) == l_featureRating_p,
                             "Inconsistent array size" );

                ret_flags << ReturnCode::ENTROPY_RATING;
            }
            break;
        case ESplitOptimMethod::GINI_OPTIMIZATION:
            {
                int32_t* l_featureRating_p = f_splits->nodeGiniRating;
                m_giniOptim_p->m_optimizer.getOptimalFeatureSplits(
                                                l_featureIdx_p,
                                                l_featureSplit_p,
                                                l_featureRating_p );
                
                tree_assert( std::next(f_splits->nodeGiniRating, f_splits->numSplits) == l_featureRating_p,
                             "Inconsistent array size" );
                
                ret_flags << ReturnCode::GINI_RATING;
            }
            break;
        default:
        {
            assert(false);
            return ret_flags_t( ReturnCode::UNKNOWN_ERROR );
        }
    }

    tree_assert( std::next(f_splits->featureIdx,   f_splits->numSplits) == l_featureIdx_p,   "Inconsistent array size" );
    tree_assert( std::next(f_splits->featureSplit, f_splits->numSplits) == l_featureSplit_p, "Inconsistent array size" );

    return ret_flags;
}


} // namespace ctypes


