/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <type_traits>
#include <tree.hpp>
#include "ctypes_tree.hpp"
#include "ctypes_return_codes.hpp"
#include "ctypes_base_split_optimization.hpp"



namespace ctypes
{

    struct CTreeSplitOptimizer;
    typedef CTreeSplitOptimizer  tree_split_optimizer_t;


    struct CTreeSplitOptimizer 
        : public CBaseSplitOptimizer< CSplitOptimizerTuple< tree::split_gini_optimizer_t,
                                                            tree::CTreeSplitGiniStatistics>,
                                    CSplitOptimizerTuple< tree::split_entropy_optimizer_t,
                                                            tree::CTreeSplitEntropyStatistics> >
    {
    private:

        typedef CBaseSplitOptimizer< CSplitOptimizerTuple< tree::split_gini_optimizer_t,
                                                        tree::CTreeSplitGiniStatistics>,
                                    CSplitOptimizerTuple< tree::split_entropy_optimizer_t,
                                                        tree::CTreeSplitEntropyStatistics> >  base_t;


    public:

        using typename base_t::giniOptim_t;
        using typename base_t::entropyOptim_t;

        // Inherit constructor
        using base_t::base_t;

        // overriding base method
        void initialize_parent_layer_statistic( const node_label_list_t* f_data_p );

        ret_flags_t get_optimal_splits( feature_split_list_t* f_splits ) const;
    };


}  // namespace ctypes


extern "C"
{

DLL_PUBLIC 
ctypes::tree_split_optimizer_t* tree_create_split_optimizer( int32_t f_numLabels_i32, int32_t f_numNodes_i32 );

DLL_PUBLIC 
void tree_destruct_split_optimizer( ctypes::tree_split_optimizer_t** f_soptim_p );

DLL_PUBLIC 
int32_t tree_initialize_parent_layer_statistic( ctypes::tree_split_optimizer_t* f_soptim_p,
                                                const node_label_list_t*        f_data );

DLL_PUBLIC 
int32_t tree_process_feature( ctypes::tree_split_optimizer_t* f_soptim_p,
                              const node_label_data_list_t*   f_data );

DLL_PUBLIC 
int32_t tree_get_optimal_splits( const ctypes::tree_split_optimizer_t* f_soptim_p,
                                 feature_split_list_t*                 f_splits );


}  // extern C

