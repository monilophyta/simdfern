/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <stdint.h>
#include <cstddef>
#include <type_traits>
typedef float float32_t;


struct image_descr_t
{
    int32_t nImgRows;
    int32_t nImgCols;
    
    int32_t rowStride;
    int32_t colStride;
};
static_assert( std::is_standard_layout<image_descr_t>::value );


struct box_list_t
{
    int32_t* up_pi32;
    int32_t* left_pi32;
    int32_t* down_pi32;
    int32_t* right_pi32;
};
static_assert( std::is_standard_layout<box_list_t>::value );


struct img_coord_t
{
    int32_t rowIdx;
    int32_t colIdx;
};
static_assert( std::is_standard_layout<img_coord_t>::value );



struct matrix_descr_t
{
    size_t     m_nRows;
    size_t     m_stride;
    size_t     m_nCols;
};
static_assert( std::is_standard_layout<matrix_descr_t>::value );


struct feature_rows_t
{
    int32_t                 numData;        // =N
    int32_t                 numFeatures;    // =k

    // feature Matrix  [ k x N ]
    const float32_t* const* featureRows;
};
static_assert( std::is_standard_layout<feature_rows_t>::value );



struct sample_classification_data_t
{
    int32_t                 numData;        // =N

    union
    {
        int32_t             numNodes;       // =M
        int32_t             numLeafs;
    };
    
    int32_t                 numLabels;      // =L
    
    union
    {
        const int32_t*      nodeIdx;
        const int32_t*      leafIdx;
    };
    const int32_t*          labelIdx;
    const int32_t*          dataWeight;
};
static_assert( std::is_standard_layout<sample_classification_data_t>::value );

typedef sample_classification_data_t node_label_list_t;

