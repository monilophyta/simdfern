# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["ParamKey", "ParameterRegistry", "ParameterException"]


import json
import typing as tp
import collections
import copy


class ParamKey(object):

    key : str

    def __init__(self, key : str ):
        self.key = key


class ParameterException(Exception):
    """Exception indicating an error in paramter handling"""

    message = None

    def __init__(self, message):
        super(ParameterException,self).__init__()
        self.message = message

    def __str__(self): 
        return repr(self.message)



is_comment = lambda key: "_comment" in key



# --------------------------------------------------------------------------------------

class ParameterRegistryBase(collections.abc.Container):

    __ptree : dict
    

    def __init__(self, ptree : dict):
        self.__ptree = ptree
        

    def keys(self):
        def key_gen( prefix, dct ) -> list:
            key_list = list()
            for key in dct.keys():
                if isinstance( dct[key], dict ):
                    key_list.extend( key_gen( "%s%s." % (prefix,key), dct[key] ))
                else:
                    key_list.append("%s%s" % (prefix,key))
            sum([ key_gen( "%s%s." % (prefix,key), dct[key] ) for key in dct.keys() ], [] )
        return key_gen( "", self.__ptree )


    def items(self):
        keys = self.keys()
        get_leaf = lambda k,node: node[k]
        return { k : get_leaf( *self._get_leaf(k )) for k in keys }


    def __contains__(self, key : str) -> bool:
        key_seq = key.split(".")
        node = self.__ptree

        for k in key_seq:
            if (not isinstance(node, dict)) or (k not in node):
                return False
            node = node[k]
        return isinstance( node, (int,float,bool,str,type(None) ) )


    def has_subtree(self, key_prefix ) -> bool:
        key_seq = key_prefix.split(".")
        node = self.__ptree

        for k in key_seq:
            if (not isinstance(node, dict)) or (k not in node):
                return False
            node = node[k]
        return isinstance( node, dict )


    def _get_node(self, key : str) -> tp.Tuple[str,dict]:
        key_seq = key.split(".")
        node = self.__ptree

        for k in key_seq[:-1]:
            if k not in node:
                raise ParameterException( "Calling for not registered paramter '%s'" % key )
            node = node[k]

            if not isinstance(node, dict):
                raise ParameterException( "Parameter key '%s' has terminal node (%r) on its path" % (key,k) )
        
        k = key_seq[-1]
        if k not in node:
            raise ParameterException( "Calling for not registered paramter '%s'" % key )
        return (k, node)


    def _get_leaf(self, key : str) -> dict:
        k, node = self._get_node(key)

        if not isinstance( node[k], (int,float,bool,str,type(None) ) ):
            raise ParameterException( "Key '%s' is not a terminal node" % key )
        return (k, node)


    def _create_path(self, key : str) -> tp.Tuple[str,dict]:
        key_seq = key.split(".")
        node = self.__ptree

        for k in key_seq[:-1]:
            if k not in node:
                node[k] = {}
            node = node[k]

            if not isinstance(node, dict):
                raise ParameterException( "Parameter key '%s' has terminal node (%r) on its path" % (repr(key),k) )
        
        k = key_seq[-1]
        if k in node:
            raise ParameterException( "Second registration of key '%s'" % repr(key) )
        return (k,node)


# --------------------------------------------------------------------------------------


class ParameterRegistry(ParameterRegistryBase):

    def __init__(self, param_dict : dict ):
        super().__init__(param_dict)


    @classmethod
    def load_json(cls, file_path : str,
                   skip_comments : bool = True ):
        
        with open(file_path, "r") as fin:
            json_data = json.load( fin )
        
        if skip_comments:
            node_stack = [json_data]
            while len(node_stack) > 0:
                node = node_stack.pop()
                node_keys = tuple( node.keys() )
                for key in node_keys:
                    snode = node[key]
                    if is_comment(key):
                        del node[key]
                    elif isinstance( snode, dict ):
                        node_stack.append(snode)
        
        return cls(json_data)
    
    
    def copy(self):
        return copy.deepcopy(self)

    def __setitem__(self, key : str, value : tp.Union[str,int,float,bool]):
        self.set_param( key, value )


    def __getitem__(self, key : str) -> tp.Union[int,float,bool,str,type(None)]:
        assert isinstance( key, str )
        key, node = self._get_leaf( key )
        return node[key]
    

    def save_json(self, file_path):
        raise ParameterException("Not yet implemented")


    def register_param(self, key : str, value : tp.Union[str,int,float,bool,type(None)]):
        """Register new (paramter,value) pair"""
        assert isinstance(key,str)
        assert isinstance(value, (float,int,bool,str,type(None)))
        k, node = self._create_path( key )
        node[k] = value
        

    def set_param(self, key : str, value : tp.Union[str,int,float,bool]):
        """Set new value for existing paramter"""
        assert isinstance( value, (int,float,bool,str,type(None) ) )
        assert isinstance( key, str )
        key, node = self._get_leaf( key )
        node[key] = value
    

    def set_params(self, n_params ):
        assert isinstance( n_params, (dict, ParameterRegistry) )

        for key, value in n_params.items():
            self.set_param( key, value )
    

    def subtree(self, key):
        k, node = self._get_node(key)
        node = node[k]
        if not isinstance( node, dict ):
            raise ParameterException( "Key '%s' is a terminal node" % key )
        return ParameterRegistry( copy.deepcopy(node) )


    def __call__(self, item : tp.Union[ParamKey,float,int,bool,str],
                       Type : type ) -> tp.Union[float,int,bool,str]:
        
        assert isinstance( item, (ParamKey,float,int,bool,str) )
        assert isinstance( Type, type )

        if not isinstance( item, ParamKey ):
            value = Type(item)
        else:
            k, node = self._get_leaf( item.key )
            value = node[k]
            
            if value is not None:
                value = Type(value)

        return value


# -------------------------------------------------------------------------------

#pr = ParameterRegistry()
#pr.load_json( "fern_training_default.json" )
#pr.load_json( "combined_training_default.json" )
