# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["default_tree_training_params",
           "default_fern_training_params",
           "default_combined_training_params"]


import os.path

from .parameter_registry import ParameterRegistry

dirpath = os.path.dirname( os.path.abspath(__file__) )

default_tree_training_params     = ParameterRegistry.load_json( os.path.join( dirpath, "tree_training_default.json" ) )
default_fern_training_params     = ParameterRegistry.load_json( os.path.join( dirpath, "fern_training_default.json" ) )
default_combined_training_params = ParameterRegistry.load_json( os.path.join( dirpath, "combined_training_default.json" ) )
