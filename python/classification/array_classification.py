# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["ClassificationTree"]


from collections.abc import Sequence
import numpy as np

from ..ctypes_py import IndexedTreeLayer, WeightedFernLayer
from ..simd import aligned_zeros


class ClassificationTree( object ):
    
    tree_layers = None


    @property
    def num_features(self):
        return ( max([ l.max_feature_idx for l in self.tree_layers ] ) )

    @property
    def num_layers(self):
        return len(self.tree_layers)


    def __init__(self, tree_layers ):
        """
        Parameters
        ----------
        tree_layers : sequence of L times IndexedTreeLayer or WeightedFernLayer
        """

        assert isinstance( tree_layers, Sequence )
        assert np.all( [ isinstance( x, (WeightedFernLayer, IndexedTreeLayer) ) for x in tree_layers ])

        self.tree_layers = tree_layers
    
    
    def classify(self, feature_channels, initial_nodeIdx = None ):

        assert isinstance( feature_channels, Sequence )
        assert self.num_features <= len(feature_channels)

        if initial_nodeIdx is None:
            num_data = len(feature_channels[0])
            initial_nodeIdx = aligned_zeros( (num_data,), dtype=np.int32, do_extend=False ) # [ B+e ]
        else:
            num_data = initial_nodeIdx.size
            assert( num_data <= len(feature_channels[0]))
        
        # run classification
        node_idx = initial_nodeIdx
        for layer in self.tree_layers:
            node_idx = layer.classify( node_idx, feature_channels )
        
        return node_idx

    
    def convert_ratio_to_diff( self ):
        """
        Assuming box ratio features it converts the classifcation tree for box diff features
        """

        old_feature_idx = list()
        feature_lambda = list()

        n_tree_layers = list()

        cur_feature_idx = 0

        for l in self.tree_layers:
            
            valid_feature_idx = np.flatnonzero( l.feature_idx >= 0 )

            # preserve mapping of old to new features
            old_feature_idx.append( l.feature_idx[ valid_feature_idx ] )
            feature_lambda.append( l.feature_split[ valid_feature_idx ] )

            n_feature_idx = l.feature_idx.copy()
            n_feature_idx[ valid_feature_idx ] = np.arange( cur_feature_idx, cur_feature_idx+valid_feature_idx.size, dtype=np.int32 )
            
            # create new layer with splits at zero
            n_layer = IndexedTreeLayer( n_feature_idx, np.zeros_like( l.feature_split ) )
            n_tree_layers.append( n_layer )

            # update feature counter
            cur_feature_idx += valid_feature_idx.size
        
        old_feature_idx = np.concatenate( old_feature_idx, axis=0 )
        feature_lambda = np.concatenate( feature_lambda )

        diff_tree = ClassificationTree( n_tree_layers )

        return (diff_tree, old_feature_idx, feature_lambda)


        


            
