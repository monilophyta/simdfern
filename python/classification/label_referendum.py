# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

import numpy as np



class LabelPoll( object ):

    counts = None  # [ num_data x num_labels ]
    __dataIdx = None  # [ num_data ] -  0..(num_data-1) range

    @property
    def num_data(self):
        return self.counts.shape[0]


    @property
    def num_labels(self):
        return self.counts.shape[-1]


    def __init__(self, num_data, num_labels, dtype=np.float32 ):
        #assert np.issubdtype( dtype, np.floating )
        self.counts = np.zeros( (num_data, num_labels), dtype=dtype )

        # plain data index
        self.__dataIdx = np.arange( self.num_data, dtype=np.int )
    

    def get_majority_decisions( self ):
        return self.counts.argmax( axis=1 )
    
    def get_label_probability( self, label_idx, pseudo_label_count = 0, pseudo_rest_count = 0.5 ):
        assert self.num_data == label_idx.size
        
        conf = self.counts[self.__dataIdx,label_idx] + pseudo_label_count
        conf = conf / ( self.counts.sum( axis=1 ) + pseudo_rest_count )
        return conf


    def add_votes( self, label_idx ):
        assert self.num_data == label_idx.size

        # add label_counts
        self.counts[self.__dataIdx,label_idx] += 1


    def add_vote_histograms( self, hists ):
        assert hists.shape[-1] == self.num_labels
        #assert np.issubdtype( hists.dtype, np.integer )

        self.counts += hists
    

    def add_sparse_vote_histograms( self, label_idx, label_counts, rest_counts ):
        assert self.num_data == label_idx.size
        assert self.num_data == label_counts.size
        assert self.num_data == rest_counts.size

        if not np.issubdtype( self.counts.dtype, np.floating ):
            self.counts = self.counts.astype(np.float)

        # add rest counts to all items
        rest_counts = np.multiply( 1. / (self.num_labels-1), rest_counts, dtype=self.counts.dtype )
        self.counts += rest_counts[np.newaxis,:]

        # add label_counts minus rest_counts
        self.counts[self.__dataIdx,label_idx] += label_counts - rest_counts




