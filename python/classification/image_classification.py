# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = [ "ImageClassificationTree" ]


from collections.abc import Sequence
from functools import reduce
import itertools as it

import numpy as np

from ..ctypes_py import BoxFeatureEvaluator
from ..image import ImageCoords
from ..simd import aligned_zeros, as_aligned_array
from .array_classification import ClassificationTree


class ImageClassificationTree( ClassificationTree ):

    box_feature_channels = None

    classifier_roi = None   # Instance of Rectangle

    INVALID_NODE_IDX = np.iinfo( np.int32 ).min


    @property
    def num_features(self):
        return sum( [ x.num_boxes for x in self.box_feature_channels ] )

    @property
    def num_channels(self):
        return len( self.box_feature_channels )


    def __init__(self, tree_layers, box_feature_channels, channel_feature_mapping=None ):
        """
        Parameters
        ----------
        tree_layers : sequence of L times IndexedTreeLayer or WeightedFernLayer
        box_feature_channels : sequence of L times BoxFeatureEvaluator
        channel_feature_mapping : None or int array 
            all calculated features using the BoxFeatureEvaluators in box_feature_channels
            become concatenated.
            channel_feature_mapping[i] serves for rordering the concatenated array X,
            i.e channel_feature_mapping[i] is the index of feature i within X
        """
        super().__init__( tree_layers )

        assert isinstance( box_feature_channels, Sequence )
        assert np.all( [ isinstance( x, BoxFeatureEvaluator ) for x in box_feature_channels ])

        self.box_feature_channels = box_feature_channels

        assert( super().num_features <= self.num_features )

        # set classifier ROI
        classifier_roi = [ x.box_union for x in self.box_feature_channels ]

        if self.num_channels > 1:
            self.classifier_roi = reduce( lambda x,y: x.union(y), classifier_roi )
        else:
            self.classifier_roi = classifier_roi[0]
        
        if channel_feature_mapping is not None:
            assert channel_feature_mapping.size == self.num_features

        self.channel_feature_mapping = channel_feature_mapping
    

    def classify( self, channel_integrals, validity_mask=None, buffer_size=8192 ):

        ## Check input
        if self.num_channels > 1:

            if isinstance( channel_integrals, np.ndarray ):
                assert channel_integrals.ndim == 3
                channel_dim = np.argmin( channel_integrals.shape[::2] ) / 2
                assert channel_integrals.shape[channel_dim] == self.num_channels

                if channel_dim > 0:
                    channel_integrals = np.swapaxes( channel_integrals, channel_dim, 0 )
                    channel_dim = 0

                channel_integrals = [ channel_integrals[i,:,:] for i in range(self.num_channels) ]
            elif isinstance( channel_integrals, Sequence ):
                assert len(channel_integrals) == self.num_channels
                assert channel_integrals[0].ndim == 2
                assert np.all( [ channel_integrals[0].shape == x.shape for x in channel_integrals[1:] ] )
            else:
                assert False
        else:
            if isinstance( channel_integrals, Sequence ):
                assert len(channel_integrals) == 1
                channel_integrals = channel_integrals[0]
            
            assert isinstance( channel_integrals, np.ndarray )
            assert channel_integrals.ndim == 2

            channel_integrals = (channel_integrals,)
        
        int_image_shape = channel_integrals[0].shape
        img_shape = (int_image_shape[0]-1, int_image_shape[1]-1)

        if validity_mask is not None:
            assert isinstance( validity_mask, np.ndarray )
            assert validity_mask.size == 2
            assert validity_mask.shape == img_shape
            assert validity_mask.flags


        ## Allocate output memory
        node_idx_image = np.empty( img_shape, dtype=np.int32 )
        node_idx_image.fill( self.INVALID_NODE_IDX )   # fill with default value for invalid items
        flat_node_idx_image = node_idx_image.ravel()  # for later classificaton

        ## Determ Indices for Evaluation
        u_range = np.arange( start=max( 0, -self.classifier_roi.up ), \
                             stop=img_shape[0] - max( 0, self.classifier_roi.down ), \
                             dtype=np.int32 )
        v_range = np.arange( start=max( 0, -self.classifier_roi.left ), \
                             stop=img_shape[1] - max( 0, self.classifier_roi.right ), \
                             dtype=np.int32 )

        u_grid, v_grid = np.meshgrid( u_range, v_range, indexing='ij' )  # ij: matrix indexing
        u_grid = u_grid.ravel()
        v_grid = v_grid.ravel()

        ## filter invalid indices
        if validity_mask is not None:
            validity_mask = validity_mask[u_grid,v_grid]
            u_grid = u_grid[validity_mask]
            v_grid = v_grid[validity_mask]
        
        ## Set buffer size, if not set already
        if buffer_size is None:
            buffer_size = u_grid.size
        
        uv_indices = np.ravel_multi_index( (u_grid, v_grid), img_shape )  # [ N ]
        u_grid = as_aligned_array( u_grid, do_extend=False )
        v_grid = as_aligned_array( v_grid, do_extend=False )
        
        self.__run_classification( flat_node_idx_image, channel_integrals, buffer_size, (u_grid,v_grid), uv_indices )
        return node_idx_image


    def __run_classification( self, flat_node_idx_image, channel_integrals, buffer_size, grid, uv_indices ):
        u_grid, v_grid = grid

        ## allocate node index buffer
        initial_node_idx = aligned_zeros( (buffer_size,), dtype=np.int32, do_extend=False ) # [ B+e ]

        ## run the classification
        for idx in range( 0, uv_indices.size, buffer_size ):
            islice = slice(idx, idx+buffer_size)

            # extract features
            img_coords = ImageCoords( rowIdx = u_grid[islice], colIdx = v_grid[islice] )
            num_locations = img_coords.size

            # calculate feature channels
            feature_channels = [ bf.process( intimg, img_coords ) for bf, intimg in zip( self.box_feature_channels, channel_integrals ) ]  # C * [ N x F ]
            # convert to list if lists
            feature_channels = [ list( f.transpose() ) for f in feature_channels ]  # C * [ F * [N] ]
            # TODO: This array is essentially copied due to traonsposition
            # Proposed Change: Features should into an array of desired (transposed) shape

            # flatten lists if lists
            feature_channels = tuple( it.chain( *feature_channels ) )   # C * F * [N]

            if self.channel_feature_mapping is not None:
                feature_channels = [ feature_channels[idx] for idx in self.channel_feature_mapping ]

            # run over the layers
            node_idx = super().classify( feature_channels, initial_node_idx[:num_locations] )
            
            # transfer node indices into output
            flat_node_idx_image[ uv_indices[islice] ] = node_idx


