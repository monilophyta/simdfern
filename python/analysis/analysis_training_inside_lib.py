# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["project_splitting_plane"]


import numpy as np

def project_splitting_plane( u, v, svect, soffset ):
    """
    Parameters
    ----------
    u : float32 ndarray [ len=D ]
        combonent of plane to project on
    v : float32 ndarray [ len=D ]
        combonent of plane to project on
    svect : float32 ndarray [ len=D ]
        normal vector of plane for projection
    soffset : float
        offset of plane for projection
    
    Alogrithm follows:
    https://de.wikipedia.org/wiki/Schnittgerade#Schnitt_einer_Ebene_in_Normalenform_mit_einer_Ebene_in_Parameterform

    Returns
    -------
    pc : array [ len=2 ]
        one reference point of projection line
    pvect : array [ len=2 ]
        normalized direction vector of projection line
    """

    nu = np.dot( u, svect )
    nv = np.dot( v, svect )

    # projection matrix
    proj = np.stack( (u,v) )

    if abs(nu) < abs(nv):
        u,v = v,u
        nu,nv = nv,nu
        
    pc = (soffset / nu) * u
    pvect = v - ( (nv/nu) * u )

    # project pc and pvect
    pc    = np.dot( proj, pc )  # [ 2 ]
    pvect = np.dot( proj, pvect ) # [ 2 ]

    # normalization
    pnorm = np.linalg.norm(pvect)
    pvect = pvect / pnorm

    return (pc, pvect)


