# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


from ..clmath import Crosstab
import numpy as np
import matplotlib.pyplot as plt


num_data = 1000
num_selected = 400
N = 100
num_chosen = np.arange(0,N+1)

toarray = np.zeros_like( num_chosen )

cr = Crosstab( num_chosen, N+toarray, num_selected+toarray, num_data+toarray )

chi2_errprob, X = cr.chi2_type_one_error_prob()
gtest_errprob, G = cr.gtest_type_one_error_prob()
hyp_errprob = cr.hypergeom_type_one_error_prob()


fig, (ax1,ax2) = plt.subplots(2,1)
ax1.plot( num_chosen, X, "b-", num_chosen, G, 'r-' )
ax2.plot( num_chosen, chi2_errprob, "b-", num_chosen, gtest_errprob, 'r-', num_chosen, hyp_errprob, 'g-', )
plt.show()
