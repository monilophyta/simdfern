# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

import math
import typing as tp
import numpy as np
import matplotlib.pyplot as plt

import pylineclip

from ..ctypes_py import lsearch_fern_param, FernSplitOptimizer, create_feature_rows, WeightedFernLayer
from ..fern import LevelWeightCoordinateDescent

from ..tests import IrisTrainingBase
from ..training import FernTrainer
from ..clmath import MultiNodeClassStatistics, mutual_counts


class IrisTrainingViz( IrisTrainingBase ):

    #num_layers = 1

    weights        : list
    feature_splits : list
    pygini  : list
    cppgini : list


    pre_layer : tp.Optional[WeightedFernLayer]
    pre_node_idx : np.ndarray

    co_descent : LevelWeightCoordinateDescent


    def __init__(self):
        super().__init__()

        self.weights = [ np.array([0.1, 1.0], dtype=np.float32) ]
        self.feature_splits = [ 1.2 ]

        raw_data = self.raw_data - self.raw_data.mean(axis=1)[:,np.newaxis]
        print(raw_data.shape)

        feature_rows = create_feature_rows(0)
        feature_rows.extend( list(raw_data) )

        # Perfrom LDA and use only two dimensions
        mns = MultiNodeClassStatistics( nodeIdx = np.zeros( self.num_data, dtype=np.int32 ),
                                       classIdx = self.labels,
                                   feature_rows = feature_rows,
                                        weights = self.data_weights )
        
        eigenvalue, eigenvector = mns.run_lda()
        assert np.all( eigenvalue[:-1] > eigenvalue[1:] )
        self.raw_data = np.dot( eigenvector[:,:2].transpose(), raw_data ).astype(np.float32)

        # initialize with one existing layer
        feature_rows = create_feature_rows(0)
        feature_rows.extend( list(self.raw_data) )
        self.pre_layer = WeightedFernLayer.create( feature_weights=np.array( [-0.99986649,  0.01633971], dtype=np.float32 ), 
                                                    feature_split = -0.076731087359765998 )
        self.pre_node_idx = self.pre_layer.classify( np.zeros( self.num_data, dtype=np.int32 ), feature_rows )
        #self.pre_node_idx = np.left_shift( self.pre_node_idx, 1 )

        # initialize coordinate descent
        self.co_descent = LevelWeightCoordinateDescent( init_fern_layer = WeightedFernLayer.create( feature_weights = self.weights[-1],
                                                                                                      feature_split = self.feature_splits[-1] ),
                                                        feature_rows = feature_rows,
                                                           label_idx = self.labels,
                                                            leaf_idx = self.pre_node_idx,
                                                      sample_weights = self.data_weights,
                                                          num_labels = self.num_labels,
                                                           num_leafs = 2,
                                                         regul_ratio = 0.05 )

        self.pygini = [ self.calc_gini_rating( self.pre_node_idx ) ]
        self.cppgini = list()


    def run_weight_optimization(self):
        
        for i in range(5):
            self.optimize_weight(i%2)
            self.optimize_split()



    def optimize_weight( self, widx : int ):
        assert int(widx) in {0,1}

        res = self.co_descent.optimize_weight( widx )

        self.weights.append( self.co_descent.feature_weights.copy() )
        self.feature_splits.append( self.co_descent.feature_split )

        # verify gini rating
        n_node_idx = self.pre_node_idx + (np.dot( self.weights[-1], self.raw_data ) < self.feature_splits[-1]).astype(np.int32)
        post_gini = self.calc_gini_rating( n_node_idx )

        self.cppgini.append( res["rating"] )

        #print(min_weight, res.lower_weight_margin, res.upper_weight_margin )
        print( "Gini rating %d, penalty=%.2f after %d improvements: %r" % ( res["rating"],
                                                                             res["prm_val_penalty"],
                                                                             res["num_optim_steps"],
                                                                             [ self.weights[-1], self.feature_splits[-1] ] ) )
        print( "\t pre=%.3f  post=%.3f" % (self.pygini[-1],post_gini) )
        self.pygini.append( post_gini )



    def optimize_split(self):

        res = self.co_descent.optimize_split()
        if res is None:
            print("No impromenent during split optimization")
        else:
            self.weights.append( self.co_descent.feature_weights.copy() )
            self.feature_splits.append( self.co_descent.feature_split )
            print( "Split optimization with rating = %d" % res["rating"] )


    def calc_gini_rating(self, node_idx):
        mcounts = mutual_counts( X = node_idx, 
                                 Y = self.labels,
                           weights = self.data_weights,
                             num_X = node_idx.max()+1,
                             num_Y = self.labels.max() + 1)

        return mcounts.gini_index( axis = 1, do_norm = True ) 


    def plot_data(self):

        fig = plt.figure()
        ax = plt.gca()

        for label_idx in range(self.num_labels):
            
            lmask = self.labels == label_idx
            assert np.any(lmask)

            ax.plot( self.raw_data[0,lmask], self.raw_data[1,lmask], '+', label=self.label_names[label_idx] )

        xlim = (-3,3)
        ylim = (-3,3)
        xlim = ax.set_xlim(xlim)
        ylim = ax.set_ylim(ylim)

        for idx,(weights,split) in enumerate(zip(self.weights,self.feature_splits)):
            alpha = min( 0.1 + (float(idx+1) * 0.9) / len(self.feature_splits), 1.0 )
            self.plot_planes( ax, weights,split, idx, xlim, ylim, alpha=alpha )


        ax.legend()
        plt.grid()
        plt.show()


    def plot_planes(self, ax, weights, split, idx, xlim, ylim, alpha=1.0 ):

        norm = 1. / np.linalg.norm(weights)
        pc = split * norm
        pn = weights * norm
        #pn = pn[::-1]

        pv = pn[::-1].copy()
        pv[0] *= -1

        pM = pn * pc
        p1 = pM - 1000*pv
        p2 = pM + 1000*pv

        # clipping
        res = pylineclip.cohensutherland( xlim[0], ylim[1], xlim[1], ylim[0], p1[0], p1[1], p2[0], p2[1] )
        if res[0] is not None:
            p1 = res[::2]
            p2 = res[1::2]
            ax.text( p1[0], p2[0], "%02d" % idx, alpha=alpha )
            ax.plot( p1, p2, "k-", alpha=alpha )


    

# -------------------------------------------------------------------------------------

if __name__ == '__main__':
    itv = IrisTrainingViz()
    itv.run_weight_optimization()
    itv.plot_data()
