# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


import os.path
import numpy as np
from scipy.linalg import eigh as scipy_eigh, eig as scipy_eig, qr as scipy_qr
import scipy.linalg


import matplotlib.pyplot as plt

# ---------------------------------------------------------------------------------

def singular_eig1( inter_cov, intra_cov ):
    Z, residues, rank, Sval = scipy.linalg.lstsq( intra_cov, inter_cov )
    eigenvalue, eigenvector = scipy_eig( Z )

    eigenvalue = np.real( eigenvalue )
    eigenvector = np.real( eigenvector )

    return (eigenvalue, eigenvector)


def singular_eig2( inter_cov, intra_cov ):
    # ges: T^-1 S = X     T : intra_cov, S : inter_cov
    # => X' = S' (T^-1)' = S' T'^-1
    # QR:
    # T' = Q R    with    Q' == Q^-1
    # => X' = S' (Q R)^-1 = S R^-1 Q'

    Q,R = scipy_qr(intra_cov)
    Y, residues, rank, Sval = scipy.linalg.lstsq( R, Q.transpose() )
    X = np.dot( inter_cov, Y )

    eigenvalue, eigenvector = scipy_eig( X )
    eigenvalue = np.real( eigenvalue )
    eigenvector = np.real( eigenvector )

    return (eigenvalue, eigenvector)


def robust_eigh( A, B ):
    """
    Solves A v = \lambda B v 
    for symmetric - positive definite/semidefinite matrices A and B

    Problem is equivalent to: 
        X v = \lambda v
    with
        X = B^-1 A
    """

    # Problem is equivalent to: X v = \lambda v

    # check if B is not singular
    is_singular = False
    try:
        C, is_lower = scipy.linalg.cho_factor( B )
    except np.linalg.LinAlgError as err:
        is_singular = True
    
    if not is_singular:
        # solve for not singular B
        X = scipy.linalg.cho_solve( (C, is_lower), A, overwrite_b=False, check_finite=True )

        try:
            eigenvalue, eigenvector = scipy_eigh( X, turbo=False, eigvals_only=False )
        except np.linalg.LinAlgError as err:
            is_singular = True
            warnings.warn( repr( err ) )
    
    if is_singular:
        # inversion with QR decomposition
        Q,R = scipy_qr(B)
        Y, residues, rank, Sval = scipy.linalg.lstsq( R, Q.transpose() )
        X = np.dot( A, Y )

        eigenvalue, eigenvector = scipy_eig( X )
        eigenvalue = np.real( eigenvalue )
        eigenvector = np.real( eigenvector )

    return (eigenvalue, eigenvector)


def shape_eigenvalues( eigenvalues, eigenvectors ):
    idx = np.argsort( eigenvalues )[::-1]
    eigenvalues = eigenvalues[idx]
    eigenvectors = eigenvectors[:,idx]

    # scipy.linalg.eigh does not normalize eigenvvalues
    eignorm = np.reciprocal( np.linalg.norm( eigenvectors, axis=0 ) )
    eigenvectors *= eignorm[np.newaxis,:]

    return (eigenvalues, eigenvectors)



def test_robust_eigh( n_dim, repeats, dtype=np.float32 ):

    rand = np.random.RandomState()

    for r in range(repeats):

        N = rand.randint( n_dim+1, 1000 * n_dim )
        A = np.random.randn( N, n_dim ).astype(dtype)
        A = np.dot( A.transpose(), A )

        N = rand.randint( n_dim+1, 1000 * n_dim )
        B = np.random.randn( N, n_dim ).astype(dtype)
        B = np.dot( A.transpose(), A )

        eigvals1, eigvects1 = scipy_eigh( A, b=B, type=1, turbo=False, eigvals_only=False )
        eigvals1, eigvects1 = shape_eigenvalues( eigvals1, eigvects1 )

        eigvals2, eigvects2 = robust_eigh( A, B )
        eigvals2, eigvects2 = shape_eigenvalues( eigvals2, eigvects2 )

        assert np.all( np.isclose( eigvals1, eigvals2, atol=1e-03, rtol=1e-03  ) )
        C1 = np.absolute( eigvects1 )
        C2 = np.absolute( eigvects2 )
        M = np.absolute(C1 - C2)
        #print( M.max() )
        #absolute(`a` - `b`) <= (`atol` + `rtol` * absolute(`b`))
        assert np.all( np.isclose( np.absolute( eigvects1 ), np.absolute( eigvects2 ), atol=1e-03, rtol=1e-03 ) ), "%r\n%r" % (eigvects1, eigvects2)

    

def analyse( **covm ):

    inter_cov = covm["inter_cov"]
    intra_cov = covm["intra_cov"]

    inter_cov = inter_cov.astype(np.float32) # S
    intra_cov = intra_cov.astype(np.float32) # T


    eigenvalue, eigenvector = singular_eig1( inter_cov, intra_cov )
    print(eigenvalue, eigenvalue.dtype )

    eigenvalue, eigenvector = singular_eig2( inter_cov, intra_cov )
    print(eigenvalue, eigenvalue.dtype)


    eigenvalue, eigenvector = robust_eigh( inter_cov, intra_cov )
    print(eigenvalue)

    inter_cov = covm["inter_cov"].astype(np.float64) # S
    intra_cov = covm["intra_cov"].astype(np.float64) # T
    eigenvalue, eigenvector = scipy_eigh( inter_cov, b=intra_cov, type=1, turbo=False, eigvals_only=False )
    print(eigenvalue, eigenvalue.dtype)




    # plt.figure()

    # for i, covname in enumerate( covm.keys() ):

    #     plt.subplot(221+i)
    #     covmatrix = covm[covname]
    #     #print( covmatrix )

    #     plt.imshow( covmatrix )
    #     plt.title( covname )
    #     plt.colorbar()
    

# ---------------------------------------------------------------------------------

# dirpath = os.path.dirname( os.path.abspath(__file__) )
# dirpath = os.path.dirname( dirpath ) # ../
# dirpath = os.path.dirname( dirpath ) # ../

# eigh_failed = np.load( os.path.join( dirpath, "scipy_eigh_failed.npz") )
# eig_failed = np.load( os.path.join( dirpath, "scipy_eig_failed.npz") )

# print( eigh_failed.keys() )
# analyse( **eig_failed )
# #analyse( **eigh_failed )
# plt.show()


test_robust_eigh( 6, 1000, dtype=np.float32 )
