# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

import typing as tp
import numpy as np

from matplotlib import pyplot as plt
from matplotlib import cm
import matplotlib._color_data as mcd

from ..ctypes_py import create_feature_rows, FeatureRows, WeightedFernLayer
from ..ctypes_py import project_features, FernSplitOptimizer, ESummationAlgo
from ..ctypes_py import line_search_result_t
from ..tests import DigitsTrainingBase

from ..clmath import PowellsMethod
from ..clmath import MultiNodeClassStatistics, mutual_counts, normalize_gini
from ..clmath import max_unormed_gini

from ..fern import FernPartitionOptimizer
from ..training import FLOS



class FernPartitionParamOptimizationAnalysis( DigitsTrainingBase ):

    seed = 45
    randstate : None

    #num_layers = 1
    num_selected_eigenvectors = 9

    feature_rows : FeatureRows

    pre_node_idx : np.ndarray

    # -------------------------------------------
    
    regul_ratio    : float
    part_optimizer : FernPartitionOptimizer

    # -------------------------------------------

    init_fern_layer : WeightedFernLayer
    init_leaf_idx   : np.ndarray
    
    @property
    def init_penality(self):
        return (self.part_optimizer.regul_strength * self.init_fern_layer.centralized_l1norm)

    @property
    def init_rating(self):
        return -self.calc_gini_rating( self.init_leaf_idx, do_norm=False )

    # ------------------------------------------------------------------------------------------------------------
    

    def __init__(self, regul_ratio=0.1 ):
        DigitsTrainingBase.__init__(self)

        self.randstate = np.random.RandomState(self.seed)

        # initialize subset of data
        self.init_data(self.num_selected_eigenvectors)

        # classifiy with one already predefined hard-coded layer
        pre_layer = WeightedFernLayer.create( feature_weights=np.array( [0.00110673, -0.47330338, -0.00154508, 0.04084888, 0.00260732, -0.86029226, 0.00113441, 0.01967769, 0.18388484][:self.num_selected_eigenvectors], dtype=np.float32 ), 
                                              feature_split = 0.11307568103075027 )
        self.pre_node_idx = pre_layer.classify( np.zeros( self.num_data, dtype=np.int32 ), self.feature_rows )
        #self.pre_node_idx = np.zeros( self.num_data, dtype=np.int32 )

        # initialize fern partition optimizer for optimization tests
        self.regul_ratio = regul_ratio
        self.part_optimizer = self.get_fern_partition_optimizer( regul_ratio=regul_ratio )

        # initialize fern layer for later optimization
        #self.init_random_fern_partition()
        self.init_lda_fern_partition()



    def init_data(self, num_features : int ):
        raw_data = self.raw_data - self.raw_data.mean(axis=1)[:,np.newaxis]

        feature_rows = create_feature_rows(0)
        feature_rows.extend( list(raw_data) )

        # Perfrom LDA and use only two dimensions
        mns = MultiNodeClassStatistics( nodeIdx = np.zeros( self.num_data, dtype=np.int32 ),
                                       classIdx = self.labels,
                                   feature_rows = feature_rows,
                                        weights = self.data_weights )
        
        eigenvalue, eigenvector = mns.run_lda()
        assert np.all( eigenvalue[:-1] > eigenvalue[1:] )
        self.raw_data = np.dot( eigenvector[:,:num_features].transpose(), raw_data ).astype(np.float32)[::-1,:]

        # initialize with one existing layer
        self.feature_rows = create_feature_rows(0)
        self.feature_rows.extend( list(self.raw_data) )



    def get_fern_partition_optimizer(self, regul_ratio : float ):

        regul_strength = regul_ratio * max_unormed_gini( self.data_magnitude, self.num_labels )

        # initialize fern partition optimizer
        part_optimizer = FernPartitionOptimizer( feature_rows = self.feature_rows,
                                                    label_idx = self.labels,
                                                     node_idx = self.pre_node_idx,
                                               sample_weights = self.data_weights,
                                                   num_labels = self.num_labels,
                                                    num_leafs = 2,
                                               regul_strength = regul_strength,
                                                      grd_eps = 0.1 )
        
        return part_optimizer



    def init_random_fern_partition(self):
        """Initialize optimization partition randomly"""

        weights = self.randstate.randn( self.num_selected_eigenvectors ).astype(np.float32)
        feature_split = float(self.randstate.randn(1)[0])

        init_fern_layer = WeightedFernLayer.create( feature_weights = weights, feature_split = feature_split )
        leaf_idx = init_fern_layer.classify( self.pre_node_idx, self.feature_rows )

        self.init_fern_layer = init_fern_layer
        self.init_leaf_idx = leaf_idx
    

    def init_lda_fern_partition(self):
        """Initialize optimization partition with LDA"""

        # run lda 
        lda_stat = MultiNodeClassStatistics( nodeIdx = self.pre_node_idx,
                                            classIdx = self.labels,
                                        feature_rows = self.feature_rows,
                                             weights = self.data_weights,
                                           do_verify = False )
        eigenvalues, eigenvectors = lda_stat.run_lda()
        eigenvalues = eigenvalues.astype(np.float32)
        eigenvectors = eigenvectors.astype(np.float32)

        self.part_optimizer.split_optimizer.reset_optimal_split()

        for eidx in range(len(eigenvalues)):
            evect = eigenvectors[:,eidx]
            # project features on eigenvector
            feature_vals = project_features( evect, self.feature_rows )
            # process feature
            self.part_optimizer.split_optimizer.process_feature( feature_vals )

        # Final Result
        indexed_fern_layer, rating = self.part_optimizer.split_optimizer.get_optimal_split()
        self.part_optimizer.split_optimizer.reset_optimal_split()

        # optimal features index
        optimal_feature_idx = indexed_fern_layer.feature_idx

        # Optimal split axis
        optimal_weights = eigenvectors[:,optimal_feature_idx]
        
        # Bringing weight and feature splits together
        init_fern_layer = WeightedFernLayer.create( optimal_weights, indexed_fern_layer.feature_split )
        
        # classify data
        leaf_idx = init_fern_layer.classify( self.pre_node_idx, self.feature_rows )
        
        self.init_fern_layer = init_fern_layer
        self.init_leaf_idx = leaf_idx



    # ------------------------------------------------------------------------------------------------------------
    

    def calc_gini_rating(self, node_idx, do_norm=True):
        mcounts = mutual_counts( X = node_idx, 
                                 Y = self.labels,
                           weights = self.data_weights,
                             num_X = node_idx.max()+1,
                             num_Y = self.labels.max() + 1)

        return mcounts.gini_index( axis = 1, do_norm = do_norm) 

    # ------------------------------------------------------------------------------------------------------------
    
    def optimize_logistic_regression(self, max_num_iters : int ):
        
        # initialize layer
        n_layer = self.init_fern_layer

        res = self.part_optimizer.optimize_logistic_regression( fern_layer = n_layer,
                                                             max_num_iters = max_num_iters,
                                                                  l2weight = 0.01,
                                                                optim_gtol = 1e-2,
                                                              optim_method = "BFGS" )
        fern_layer = res["fern_layer"]

        # classify data
        leaf_idx = fern_layer.classify( self.pre_node_idx, self.feature_rows )

        penality = self.part_optimizer.regul_strength * fern_layer.centralized_l1norm
        rating = -self.calc_gini_rating( leaf_idx, do_norm=False )

        rating_history  = np.array( [self.init_rating, rating], dtype=np.float32 )
        penalty_history = np.array( [self.init_penality, penality], dtype=np.float32 )
        optim_history   = np.array( [None], dtype=np.string_)
        
        return (rating_history,penalty_history,optim_history)


    # ------------------------------------------------------------------------------------------------------------


    def optimize_weights(self, num_repeats : int, reset_split : bool ):
        """Optimize individual weights"""


        rating_history  = [self.init_rating]
        penalty_history = [self.init_penality]
        optim_history   = [None]

        # initialize layer
        n_layer = self.init_fern_layer

        for iidx in range(num_repeats):
            for widx in range(n_layer.num_features):
                ## optimize weights
                res = self.part_optimizer.optimize_weight( cidx=widx, fern_layer=n_layer )
                penalty_history.append( res.penalty )
                
                if reset_split:
                    res = self.part_optimizer.optimize_split( fern_layer = res.fern_layer )

                n_layer = res.fern_layer

                rating_history.append( res.rating )
                optim_history.append( FLOS.WEIGHT_COORDINATE_LSEARCH.value )
        
        rating_history = np.array( rating_history, dtype = np.float32 )
        penalty_history = np.array( penalty_history, dtype=np.float32 )
        optim_history = np.array( optim_history, dtype=np.string_)

        return (rating_history,penalty_history,optim_history)


    def optimize_axis_directions(self, num_repeats : int, reset_split : bool ):
        """Optimize in the direction of individual weight"""

        rating_history  = [self.init_rating]
        penalty_history = [self.init_penality]
        optim_history   = [None]

        # initialize layer
        n_layer = self.init_fern_layer

        directions = np.eye(n_layer.num_features, dtype=np.float32)


        for iidx in range(num_repeats):
            for widx in range(n_layer.num_features):

                ## optimize weights
                res = self.part_optimizer.optimize_direction( fern_layer = n_layer,
                                                      use_regul_gradient = False,
                                                          optimize_split = False,
                                                      weight_lsearch_dir = directions[widx,:],
                                                       split_lsearch_dir = None )
                
                penalty_history.append( res.penalty )
                
                if reset_split:
                    res = self.part_optimizer.optimize_split( fern_layer = res.fern_layer )

                n_layer = res.fern_layer

                rating_history.append( res.rating )
                optim_history.append( FLOS.WEIGHT_COORDINATE_LSEARCH.value )
        
        rating_history = np.array( rating_history, dtype = np.float32 )
        penalty_history = np.array( penalty_history, dtype=np.float32 )
        optim_history = np.array( optim_history, dtype=np.string_)

        return (rating_history,penalty_history,optim_history)
    

    def optimize_gradient_directions(self, num_repeats : int, use_regul_gradient : bool, optimize_split : bool, reset_split : bool ):
        """Optimize in the direction of the gradient"""

        rating_history  = [self.init_rating]
        penalty_history = [self.init_penality]
        optim_history   = [None]

        # initialize layer
        n_layer = self.init_fern_layer

        for iidx in range(num_repeats):

            ## optimize weights
            res = self.part_optimizer.optimize_direction( fern_layer = n_layer,
                                                  use_regul_gradient = use_regul_gradient,
                                                      optimize_split = optimize_split,
                                                  weight_lsearch_dir = None,
                                                   split_lsearch_dir = None )
            penalty_history.append( res.penalty )
            
            if reset_split:
                res = self.part_optimizer.optimize_split( fern_layer = res.fern_layer )

            n_layer = res.fern_layer

            rating_history.append( res.rating )
            optim_history.append( FLOS.WEIGHT_GRADIENT_LSEARCH.value )
        
        rating_history = np.array( rating_history, dtype = np.float32 )
        penalty_history = np.array( penalty_history, dtype=np.float32 )
        optim_history = np.array( optim_history, dtype=np.string_)

        return (rating_history,penalty_history,optim_history)


    def optimize_cg_gradient_directions(self, num_repeats : int, use_regul_gradient : bool, optimize_split : bool, reset_split : bool ):
        """Optimize in the direction of the gradient"""

        rating_history  = [self.init_rating]
        penalty_history = [self.init_penality]
        optim_history   = [None]

        # part_optimizer
        part_optimizer = self.get_fern_partition_optimizer( self.regul_ratio )

        # initialize layer
        n_layer = self.init_fern_layer

        for iidx in range(num_repeats):

            ## optimize weights
            res = part_optimizer.optimize_direction( fern_layer = n_layer,
                                             use_regul_gradient = use_regul_gradient,
                                                 optimize_split = optimize_split,
                                             weight_lsearch_dir = None,
                                              split_lsearch_dir = None )
            penalty_history.append( res.penalty )
            
            if reset_split:
                res = self.part_optimizer.optimize_split( fern_layer = res.fern_layer )

            n_layer = res.fern_layer

            rating_history.append( res.rating )
            optim_history.append( FLOS.WEIGHT_GRADIENT_LSEARCH.value )
        
        rating_history = np.array( rating_history, dtype = np.float32 )
        penalty_history = np.array( penalty_history, dtype=np.float32 )
        optim_history = np.array( optim_history, dtype=np.string_)

        return (rating_history,penalty_history,optim_history)


    def optimize_powells_method(self, num_repeats : int, finalize_split : bool ):
        """Optimize using powells method"""

        rating_history  = [self.init_rating]
        penalty_history = [self.init_penality]
        optim_history   = [None]

        # initialize layer
        n_layer = self.init_fern_layer
        powell_optimizer = None

        # optimization
        for idx in range(num_repeats):

            optim_res = self.part_optimizer.powells_method( fern_layer = n_layer,
                                                         max_num_iters = 1,
                                                            optim_dtol = 1e-7,
                                                      powell_optimizer = powell_optimizer )

            n_layer = optim_res.fern_layer
            #powell_optimizer = optim_res.powell_optimizer

            # store results               
            penalty_history.append( optim_res.penalty )
            rating_history.append( optim_res.rating )
            optim_history.append( "P" )


        if finalize_split:
            res = self.part_optimizer.optimize_split( fern_layer = n_layer )
            penalty_history.append( res.penalty )
            rating_history.append( res.rating )
            optim_history.append( "S" )

        rating_history = np.array( rating_history, dtype = np.float32 )
        penalty_history = np.array( penalty_history, dtype=np.float32 )
        optim_history = np.array( optim_history, dtype=np.string_)

        return (rating_history,penalty_history,optim_history)



# ----------------------------------------------------------------------------------------------------------------

def plot_optimzation_progress( fig, histories, labels ):
    #colors = list(mcd.TABLEAU_COLORS.values())
    #colors = list(mcd.CSS4_COLORS.values())
    colors = np.linspace(0, 1, len(histories))
    colors = [cm.nipy_spectral(x) for x in colors]

    axes = fig.subplots( nrows = 3,
                         ncols = 1,
                        sharex = True, 
                        sharey = False,
                       squeeze = False,
                    subplot_kw = {},
                   gridspec_kw = {} )
    axes = tuple(axes[:,0])
    
    ax_min = np.zeros(3,dtype=np.float32) + np.finfo(np.float32).max
    ax_max = np.zeros(3,dtype=np.float32) - np.finfo(np.float32).max


    for pidx, (rating_history, penalty_history, optim_history) in enumerate( histories ): 

        steps = np.arange( rating_history.size )

        axes[0].plot( steps, rating_history, linestyle='-', color=colors[pidx], label=labels[pidx] )
        axes[1].plot( steps, penalty_history, linestyle='-', color=colors[pidx] )
        axes[2].plot( steps, rating_history+penalty_history, linestyle='-', color=colors[pidx] )

        ax_max[0] = max( ax_max[0], rating_history.max() )
        ax_min[0] = min( ax_min[0], rating_history.min() )
        
        ax_max[1] = max( ax_max[1], penalty_history.max() )
        ax_min[1] = min( ax_min[1], penalty_history.min() )

    #ax.set_ylim( (ax_min, ax_max))
    #axt.set_ylim( (axt_min, axt_max) )

    axes[2].set_xlabel( "optimization steps" )
    axes[0].set_ylabel( "rating" )
    axes[1].set_ylabel( "penalty" )
    
    for ax in axes:
        ax.grid(True)
    axes[0].legend()


def plot_optimzation_comparison( fig, histories, labels ):

    colors = np.linspace(0, 1, len(histories))
    colors = [cm.nipy_spectral(x) for x in colors]

    ax = fig.subplots(1,1)

    for pidx, (rating_history, penalty_history, optim_history) in enumerate( histories ):

        total_rating = rating_history[-1] + penalty_history[-1]
        ax.plot( penalty_history[-1], total_rating, marker='o', color=colors[pidx], label=labels[pidx] )

    ax.grid(True)
    ax.legend()



# ---------------------------------------------------------------------------------------------------


if __name__ == '__main__':
    fppoa = FernPartitionParamOptimizationAnalysis( regul_ratio=0.1 )
    #fppoa.optimize_weights()

    num_iters = 4

    pwm = fppoa.optimize_powells_method(num_iters, finalize_split=True)
    
    optim_histories = [fppoa.optimize_weights( num_iters, reset_split=False ),
                       fppoa.optimize_axis_directions( num_iters, reset_split=False ),

                       fppoa.optimize_weights( num_iters, reset_split=True ),
                       fppoa.optimize_axis_directions( num_iters, reset_split=True ),

                    #    fppoa.optimize_gradient_directions( num_iters*fppoa.num_selected_eigenvectors, use_regul_gradient=False, optimize_split=False, reset_split=False ),
                    #    fppoa.optimize_gradient_directions( num_iters*fppoa.num_selected_eigenvectors, use_regul_gradient=True, optimize_split=False, reset_split=False ),
                    #    fppoa.optimize_gradient_directions( num_iters*fppoa.num_selected_eigenvectors, use_regul_gradient=False, optimize_split=True, reset_split=False ),
                    #    fppoa.optimize_gradient_directions( num_iters*fppoa.num_selected_eigenvectors, use_regul_gradient=True, optimize_split=True, reset_split=False ), 
                       
                    #    fppoa.optimize_gradient_directions( num_iters*fppoa.num_selected_eigenvectors, use_regul_gradient=False, optimize_split=False, reset_split=True ),
                    #    fppoa.optimize_gradient_directions( num_iters*fppoa.num_selected_eigenvectors, use_regul_gradient=True, optimize_split=False, reset_split=True ),
                    #    fppoa.optimize_gradient_directions( num_iters*fppoa.num_selected_eigenvectors, use_regul_gradient=False, optimize_split=True, reset_split=True ),
                    #    fppoa.optimize_gradient_directions( num_iters*fppoa.num_selected_eigenvectors, use_regul_gradient=True, optimize_split=True, reset_split=True ),
                       
                    #    fppoa.optimize_cg_gradient_directions( num_iters*fppoa.num_selected_eigenvectors, use_regul_gradient=False, optimize_split=False, reset_split=False ),
                    #    fppoa.optimize_cg_gradient_directions( num_iters*fppoa.num_selected_eigenvectors, use_regul_gradient=True, optimize_split=False, reset_split=False ),
                    #    fppoa.optimize_cg_gradient_directions( num_iters*fppoa.num_selected_eigenvectors, use_regul_gradient=False, optimize_split=True, reset_split=False ),
                    #    fppoa.optimize_cg_gradient_directions( num_iters*fppoa.num_selected_eigenvectors, use_regul_gradient=True, optimize_split=True, reset_split=False ), 
                       
                    #    fppoa.optimize_cg_gradient_directions( num_iters*fppoa.num_selected_eigenvectors, use_regul_gradient=False, optimize_split=False, reset_split=True ),
                    #    fppoa.optimize_cg_gradient_directions( num_iters*fppoa.num_selected_eigenvectors, use_regul_gradient=True, optimize_split=False, reset_split=True ),
                    #    fppoa.optimize_cg_gradient_directions( num_iters*fppoa.num_selected_eigenvectors, use_regul_gradient=False, optimize_split=True, reset_split=True ),
                    #    fppoa.optimize_cg_gradient_directions( num_iters*fppoa.num_selected_eigenvectors, use_regul_gradient=True, optimize_split=True, reset_split=True ),
                       
                        pwm ]

    labels = ["weight optim",
              "axis optim",

              "weight optim+split_reset",
              "axis optim+split_reset",
              
              # "along weight gradient", 
              # "along weight+regul gradient",
              # "along weight+split gradient", 
              # "along weight+split+regul gradient",
            
              # "along weight gradient + reset", 
              # "along weight+regul gradient + reset",
              # "along weight+split gradient + reset", 
              # "along weight+split+regul gradient + reset",
              
              "powells" ]


    fig = plt.figure()
    #ax = plt.subplot(111)
    plot_optimzation_progress( fig, optim_histories, labels )
    plt.tight_layout()

    optim_histories.append( fppoa.optimize_logistic_regression( max_num_iters = 100 ) )
    labels.append( "logistic regression (40)" )

    fig = plt.figure()
    #ax = plt.subplot(111)
    plot_optimzation_comparison( fig, optim_histories, labels )
    plt.tight_layout()

    plt.show()
