# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


import numpy as np
import logging
#import matplotlib.pyplot as plt

#from ..tests import IrisTrainingBase
from ..tests import DigitsTrainingBase
from ..training import FernTrainer
from ..fern import remove_layer, select_layers
from ..clmath import mutual_counts

from .fern_logistic_regression_coodinate_descent import LogisticFernRegressionCoordinateDescent
from ..ctypes_py import WeightedFernLayer

from scipy.optimize import minimize

#from ..clmath import MultiNodeClassStatistics
#from .test_training_inside_lib import project_splitting_plane

import matplotlib.pyplot as plt




def create_logger( prefix ):
    logger = logging.getLogger( prefix )
    logger.setLevel( logging.DEBUG )

    #formatter = logging.Formatter( "%(asctime)s;%(levelname)s;%(message)s", "%Y-%m-%d %H:%M:%S")
    
    formatter = logging.Formatter( "%(name)s:%(asctime)s: %(message)s","%z" )
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger


class LogisticRegressionAnalyser( DigitsTrainingBase ):

    dtype = np.float32
    #dtype = np.float128

    num_fern_layers = 3
    random_seed     = 59
    #max_iter        = 5

    layers = None
    leaf_idx = None
    node_rating = None


    estimator = None
    lidx = None


    def fern_training(self):
        feature_rows = self.raw_data
        print( feature_rows.shape )
        logger = create_logger( "lsvm_analysis" )
        trainer = FernTrainer( self.labels, self.data_weights, logger=logger )

        #observer = DebugFernTrainingObserver( self )
        observer = None

        layers, node_ratings, leaf_idx = trainer.train_fern( feature_rows, self.num_fern_layers, observer=observer )

        self.layers = layers
        self.node_ratings = node_ratings
        self.leaf_idx = leaf_idx
    

    def initialize_estimator( self ):
        feature_rows = list( self.raw_data )

        estimator = LogisticFernRegressionCoordinateDescent( feature_rows   = feature_rows, \
                                                             init_layers    = self.layers, \
                                                             label_idx      = self.labels, \
                                                             leaf_idx       = self.leaf_idx, \
                                                             sample_weights = None, \
                                                             num_labels     = self.num_labels, \
                                                             dtype          = self.dtype )

        self.estimator = estimator


    def run_coordinate_descent( self ):

        #self.estimator.gradient_comparison(1)

        gini_index = self.estimator.get_gini_index()
        mutual_inf = self.estimator.get_mutual_information()
        print( "Initially gini=%.4f, mutualinf=%.4f" % (gini_index, mutual_inf))
        
        for cdstep in self.estimator.coordinate_descent():

            gini_index = self.estimator.get_gini_index()
            mutual_inf = self.estimator.get_mutual_information()
            
            param_norm = np.linalg.norm( cdstep.x[:-1] )
            print( "cycle_idx=%d, layer=%d, fx=%.2f, |v|=%.5f, num_iters=%d, gini=%.4f, mutualinf=%.4f" % 
                    (cdstep.cycle_idx, cdstep.layer_idx, cdstep.fx, param_norm, cdstep.num_iters, gini_index, mutual_inf) )



    def verify_lg_problem( self ):
        mcounts = mutual_counts( self.leaf_idx,
                                 self.labels,
                                 num_X = 1 << self.num_fern_layers,
                                 num_Y = self.num_labels )
        
        orig_gini = mcounts.gini_index( 1, do_norm=False )
        orig_entropy = mcounts.conditional_entropy( axis=1, do_norm=False, use_log2=False )

        print( "orig_gini = ", orig_gini )
        print( "orig_entropy = ", orig_entropy )

        fweights = np.concatenate( [ self.layers[self.lidx].feature_weights, [-self.layers[self.lidx].feature_split] ], axis=0 )
        print( "fweights.shape: ", fweights.shape )
        #e_optim = self.eval( fweights )
        #print( "e_optim: ", e_optim )
    

    def eval_gini( self, fweights ):
        e_gini = self.estimator.gini_split_eval( fweights )
        #print( "e_gini: ", e_gini )
        return -e_gini
        

    def eval_conditional_entropy( self, fweights ):
        e_centropy = self.estimator.conditional_entropy_split_eval( fweights )
        #print( "e_centropy: ", e_centropy )
        return e_centropy


    def optimize( self, use_gini ):
        method = [ "Nelder-Mead", "Powell", "CG", "BFGS", "Newton-CG", "trust-ncg" ] #, "trust-krylov", "trust-exact" ]

        iter_count = 0


        mcounts = mutual_counts( self.leaf_idx,
                                 self.labels,
                                 num_X = 1 << self.num_fern_layers,
                                 num_Y = self.num_labels )

        if use_gini is True:
            cur_eval = mcounts.gini_index( 1, do_norm=True )
            print( "cur_eval (gini): %.3f" % cur_eval )
        else:
            cur_eval = mcounts.conditional_entropy( 1, do_norm=True )
            print( "cur_eval (conditional entropy): %.3f" % cur_eval )
        
        while True:

            for lidx in range(self.num_fern_layers):
                
                r_leaf_idx = self.initialize_estimator( lidx )
                init_weights = np.concatenate( [ self.layers[lidx].feature_weights, [-self.layers[lidx].feature_split] ], axis=0 )

                if use_gini is True:
                    res = minimize( self.eval_gini, 
                                    x0=init_weights,
                                    method = method[1],
                                    jac = False,
                                    #hess = "3-point",
                                    callback=self.optimization_callback,
                                    options={"maxiter": 1, "disp" : False} )
                else:
                    res = minimize( self.eval_conditional_entropy, 
                                    x0=init_weights,
                                    method = "BFGS",
                                    jac = False,
                                    #hess = "3-point",
                                    callback=self.optimization_callback,
                                    options={"maxiter": 1, "disp" : False} )

                feature_weights = res.x[:-1]
                #norm = 1. / np.linalg.norm( feature_weights )
                #feature_weights *= norm
                #feature_split = float(-res.x[-1] * norm)
                feature_split = float(-res.x[-1])

                n_layer = WeightedFernLayer.create( feature_weights.astype(np.float32), feature_split )
                n_leaf_idx = n_layer.classify( r_leaf_idx, self.raw_data )


                mcounts = mutual_counts( n_leaf_idx,
                                         self.labels,
                                         num_X = 1 << self.num_fern_layers,
                                         num_Y = self.num_labels )

                if use_gini is True:
                    n_eval = mcounts.gini_index( 1, do_norm=True )
                    improvement = n_eval > cur_eval
                else:
                    n_eval = mcounts.conditional_entropy( 1, do_norm=True )
                    improvement = bool(n_eval < cur_eval)
                

                if improvement is True:
                    self.layers[lidx] = n_layer

                    # delete layer
                    leaf_idx = np.bitwise_and( self.leaf_idx, np.bitwise_not( np.int32(1 << lidx) ) )

                    # isolate new layer
                    n_layer_idx = np.bitwise_and( n_leaf_idx, np.int32(1) )
                    n_layer_idx = np.left_shift( n_layer_idx, np.int32(lidx) )

                    # bring together
                    self.leaf_idx = np.bitwise_or( leaf_idx, n_layer_idx )
                    
                    print( "New Eval: %.3f" % n_eval, " , internal: %.3f" % res.fun )

                    cur_eval = n_eval
                else:
                    print( "No improvement with new eval = %.3f (old eval = %.3f)" % ( n_eval, cur_eval ) )


# -----------------------------------------------------------------------------------------------------------


if __name__ == '__main__':
    #unittest.main()

    inst = LogisticRegressionAnalyser()
    inst.fern_training()
    inst.initialize_estimator()
    inst.run_coordinate_descent()

    #inst.verify_lg_problem()
    #inst.optimize( use_gini=False )
    #print(res)


