# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


import numpy as np
import numpy.ma as ma
import pickle
import sys

import matplotlib.colors
import matplotlib.pyplot as plt
from ..tests import DigitsTrainingBase
from ..training import FernTrainer, VerboseFernTrainingObserver
from ..fern import FernLeafHistograms, select_layers

from ..tree import TreeAnalyser
from ..visualization import TreePlotter

from ..clmath import MultiNodeClassStatistics, mutual_counts

from .analysis_training_inside_lib import project_splitting_plane



class DigitsPruningViz( DigitsTrainingBase ):

    num_fern_layers = 6


    def fern_training(self):
        fern_features = self.raw_data
        trainer = FernTrainer( self.labels, self.data_weights )

        observer = VerboseFernTrainingObserver( out=sys.stdout )
        #observer = None

        layers, node_ratings, nodeIdx = trainer.train_fern( fern_features, self.num_fern_layers, do_optimize=True, observer=observer )
        print( node_ratings )
        return (layers, nodeIdx)
    

    def get_leaf_histogram( self, nodeIdx ):
        lf_hist = FernLeafHistograms.build_from_indices( nodeIdx, \
                                                          self.labels, \
                                                          self.data_weights, \
                                                          num_layers = self.num_fern_layers, \
                                                          num_labels = self.labels.max()+1 )
        return lf_hist


    def reorder( self, nodeIdx, lf_hist=None, **optim_args ):
        if lf_hist is None:
            lf_hist = self.get_leaf_histogram( nodeIdx )

        n_layer_idx = lf_hist.optimize_layer_order( **optim_args )

        n_nodeIdx  = select_layers( nodeIdx, n_layer_idx, num_layers=self.num_fern_layers )
        n_lf_hist = lf_hist.reorder_histograms( n_layer_idx )

        return (n_nodeIdx, n_lf_hist)



    def prune( self, nodeIdx, lf_hist ):

        thinned_out_leaf_idx = lf_hist.calc_thinnedout_leaf_map( same_label_pruning=True, reduced_significance_pruning=False )
        pruned_nodeIdx = thinned_out_leaf_idx[ nodeIdx ]

        assert np.unique( nodeIdx ).size >= np.unique( pruned_nodeIdx ).size

        return pruned_nodeIdx



# -----------------------------------------------------------------------------------------------------------


def plot_confusion_matrix( ax, nodeIdx, labels, data_weights, 
                           fontsize='large',
                           cmap=plt.cm.cool,
                           lmap=plt.cm.nipy_spectral,
                           **plot_args ):

    lf_hist = FernLeafHistograms.build_from_indices( nodeIdx, labels, data_weights )
    
    # create label ranges
    labels = np.arange( lf_hist.num_labels, dtype=np.int32 )
    label_range = np.linspace( 0, 1, lf_hist.num_labels, dtype=np.float32 )
    label_color = plt.cm.nipy_spectral( label_range )[:,:3]

    # normalize confusion_matrix
    confusion_matrix = lf_hist.expected_confusion_matrix()
    confusion_matrix = confusion_matrix.astype( np.float32 ) * np.reciprocal( confusion_matrix.sum(axis=-1).astype(np.float32) )

    # extending confusion matrix with labels colors
    confusion_matrix = np.concatenate( ( label_range[:,np.newaxis], confusion_matrix ), axis=1 )
    label_range = np.concatenate( ([0], label_range), axis=0 )
    confusion_matrix = np.concatenate( ( label_range[np.newaxis,:], confusion_matrix ), axis=0 )

    # crate masked array
    confusion_mask = np.zeros( confusion_matrix.shape, dtype=np.bool )
    confusion_mask[:,0] = True
    confusion_mask[0,:] = True
    confusion_matrix_c = ma.masked_array( confusion_matrix, confusion_mask )
    label_mask = np.logical_not( confusion_mask )
    label_mask[0,0] = True
    confusion_matrix_l = ma.masked_array( confusion_matrix, label_mask )


    mh_l = ax.matshow( confusion_matrix_l, cmap=lmap )
    mh_c = ax.matshow( confusion_matrix_c, cmap=cmap )
    
    #ax.set_xticks( labels )
    #ax.set_xticklabels( labels )
    #ax.set_yticks( labels )
    #ax.set_yticklabels( labels )

    #xticklabels = ax.get_xticklabels()
    #yticklabels = ax.get_yticklabels()
    #label_color = plt.cm.nipy_spectral( np.linspace( 0, 1, lf_hist.num_labels ) )[:,:3]
    diag_color = "black"

    color_vals = matplotlib.colors.rgb_to_hsv( label_color )
    wb_select = np.logical_or( color_vals[:,0] < 0.35, color_vals[:,0] > 0.9 )
    wb_select = np.logical_and( wb_select, color_vals[:,2] > 0.5 )
    
    fg_colors = ["white","black"]
    for idx in range( lf_hist.num_labels):
        ax.text( 0, idx+1, idx, horizontalalignment="center", verticalalignment="center",
                 color=fg_colors[wb_select[idx]], fontsize=fontsize, fontweight='bold' )
        ax.text( idx+1, 0, idx, horizontalalignment="center", verticalalignment="center",
                 color=fg_colors[wb_select[idx]], fontsize=fontsize, fontweight='bold' )
        ax.text( idx+1, idx+1, ".%02d" % int( np.round( confusion_matrix[idx+1,idx+1] * 100 ) ), 
                 horizontalalignment="center", verticalalignment="center",
                 fontweight='bold', fontsize=fontsize, color=diag_color )

    ax.set_axis_off()
    #ax.grid( which="minor", color="black", linestyle='-', linewidth=3)
    #cbb = plt.colorbar( mh_c, shrink=0.25 )

    ax.set_title( "Precission = %.2f" % lf_hist.expected_precision() )

    return mh_c



def plot_tree( ax, nodeIdx, labels, data_weights, title=None, **plot_args ):

    tanal = TreeAnalyser( nodeIdx, labels, data_weights )

    nodes_toplot_idx = np.arange( tanal.num_nodes, dtype=np.int32 )
    node_hists       = tanal.node_weight_hists
    edge_weights     = tanal.node_supports


    if True:
        # remove_single_branch:
        valid_node_idx = tanal.collapse_unbranched_paths()

        nodes_toplot_idx = nodes_toplot_idx[ valid_node_idx ]
        node_hists       = node_hists[valid_node_idx,:]
        edge_weights     = edge_weights[valid_node_idx]


    tplot = TreePlotter( node_idx = nodes_toplot_idx, 
                         node_hists = node_hists,
                         edge_weights = edge_weights )
    tplot.plot( ax=ax, linewidth=7, node_cmap=plt.cm.nipy_spectral, **plot_args  )
    
    title_appendix = "%d leafs" % nodes_toplot_idx.size
    if title is None:
        title = title_appendix
    else:
        title = "%s\n%s" % (title, title_appendix)
    
    ax.set_title( title )

    ax.set_axis_off()
    #plt.tight_layout()
    #ax.grid( True )



# -----------------------------------------------------------------------------------------------------------


if __name__ == '__main__':
    #unittest.main()

    #inst = DigitsPruningViz()
    #fern_layers, nodeIdx = inst.fern_training()
    
    #with open("fern6_dump.pickle", "wb") as fout:
    #    pickle.dump( (inst, fern_layers, nodeIdx), fout )
    
    with open("fern6_dump.pickle", "rb") as fin:
        inst, fern_layers, nodeIdx = pickle.load( fin )

    n_layers = len( fern_layers )
    circular_tree = True

    # Plot Original Tree
    fig, ax = plt.subplots( 2,3 )

    ## Plot original Tree
    plot_tree( ax[0,0], nodeIdx, inst.labels, inst.data_weights, circular_tree = circular_tree, title="Original Tree" )
    
    pruned_nodeIdx = inst.prune( nodeIdx, inst.get_leaf_histogram( nodeIdx ) )
    plot_tree( ax[1,0], pruned_nodeIdx, inst.labels, inst.data_weights, circular_tree = circular_tree, title="Original Tree - Pruned" )


    ## Plot smallest contribution last
    nf_nodeIdx, nf_hist = inst.reorder( nodeIdx, smallest_contribution_first=False )
    plot_tree( ax[0,1], nf_nodeIdx, inst.labels, inst.data_weights, circular_tree = circular_tree, title="Smallest contrib last" )

    nf_pruned_nodeIdx = inst.prune( nf_nodeIdx, nf_hist )
    plot_tree( ax[1,1], nf_pruned_nodeIdx, inst.labels, inst.data_weights, circular_tree = circular_tree, title="Smallest contrib last - Pruned" )

    ## Plot largest contribution last
    nt_nodeIdx, nt_hist = inst.reorder( nodeIdx, smallest_contribution_first=True )
    plot_tree( ax[0,2], nt_nodeIdx, inst.labels, inst.data_weights, circular_tree = circular_tree, title="Smallest contrib first"  )

    nt_pruned_nodeIdx = inst.prune( nt_nodeIdx, nt_hist )
    plot_tree( ax[1,2], nt_pruned_nodeIdx, inst.labels, inst.data_weights, circular_tree = circular_tree, title="Smallest contrib first - Pruned" )


    fig, ax = plt.subplots( 2,2 )
    plot_confusion_matrix( ax[0,0], nodeIdx, inst.labels, inst.data_weights )
    plot_confusion_matrix( ax[0,1], pruned_nodeIdx, inst.labels, inst.data_weights )
    plot_confusion_matrix( ax[1,0], nf_pruned_nodeIdx, inst.labels, inst.data_weights )
    mh = plot_confusion_matrix( ax[1,1], nt_pruned_nodeIdx, inst.labels, inst.data_weights )


    #ax4.set_axis_off()
    #fig.tight_layout()
    plt.show()


