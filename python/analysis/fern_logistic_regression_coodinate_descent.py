# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["LogisticFernRegressionCoordinateDescent", "cd_result_tuple"]


#import sys
import typing as tp
from collections import Sequence, namedtuple
import numpy as np
#import scipy

from ..ctypes_py import WeightedFernLayer, FeatureRows

from ..clmath import mutual_counts

#from .fern_logistic_regression_counts import LogisticRegressionStatCounter
#from .fern_logistic_regression_layer import LogisticRegressionFernLayer
#from .fern_logistic_regression_optimization import EntropyOptimization
from ..fern import LogisticRegressionEvaluator
from ..fern import SLSQPContext

from .fern_layer_reordering import remove_layer, select_layers


cd_result_tuple = namedtuple("coordinate_descent_result", [ "cycle_idx", "layer_idx", "x", "fx", "num_iters"] )


class LogisticFernRegressionCoordinateDescent( object ):

    default_dtype  = np.float32

    # optimization tolerance for convergence definition
    optim_ftol     : float = 1e-3

    # pseudo count in entropy statistics
    eps            : float = 0.5

    # scale of weight vector
    scale          : float = 1.0

    dtype          = None

    # -------------------------------------------------------------------------------

    feature_rows   : FeatureRows

    leaf_idx       : np.ndarray
    label_idx      : np.ndarray
    sample_weights : tp.Optional[np.ndarray]

    num_labels     : int

    # -------------------------------------------------------------------------------

    regression_layers  : np.ndarray    # [ num_layers x num_weights ]

    # -------------------------------------------------------------------------------

    @property
    def num_layers(self):
        return (self.regression_layers.shape[0])

    @property
    def num_leafs(self):
        return ( 1 << self.num_layers )
    
    @property
    def num_data(self):
        return int(self.leaf_idx.size)
    
    # -------------------------------------------------------------------------------


    def __init__(self, feature_rows   : FeatureRows,
                       init_layers    : list,
                       label_idx      : np.ndarray,
                       leaf_idx       : np.ndarray,
                       sample_weights : tp.Optional[np.ndarray] = None,
                       num_labels     : int = None,
                       dtype          = default_dtype ):

        assert isinstance( init_layers, Sequence )
        assert np.all( [ isinstance( layer, WeightedFernLayer ) for layer in init_layers ] )
        num_layers = len(init_layers)
        num_leafs = 1 << num_layers

        assert isinstance( label_idx, np.ndarray )
        assert np.issubdtype( label_idx.dtype, np.int32 )

        assert isinstance( leaf_idx, np.ndarray )
        assert np.issubdtype( leaf_idx.dtype, np.int32 )
        assert label_idx.size == leaf_idx.size
        assert np.all( leaf_idx >= 0 )
        assert np.all( leaf_idx < num_leafs )

        if num_labels is None:
            num_labels = label_idx.max() + 1
        else:
            assert num_labels > 0

        assert isinstance( feature_rows, FeatureRows )
        assert np.all( leaf_idx.size == np.array( [ feature.size for feature in feature_rows ] ) )
        
        self.dtype = np.dtype( dtype )

        if sample_weights is not None:
            assert isinstance( sample_weights, np.ndarray )
            assert sample_weights.size == leaf_idx.size

        # ------------------------------------------------------------------------------

        self.feature_rows   = feature_rows
        self.leaf_idx       = leaf_idx
        self.label_idx      = label_idx
        self.sample_weights = sample_weights
        self.num_labels     = num_labels

        # initial values for the regression layers
        reg_weights = np.stack( [ layer.feature_weights.astype(dtype) for layer in init_layers ], axis=0 ) # [ num_layers x num_features ]
        reg_offsets = np.array( [ layer.feature_split for layer in init_layers ], dtype=self.dtype )       # [ num_layers ]
        self.regression_layers = np.concatenate( (reg_weights, reg_offsets[:,np.newaxis] ), axis=1 )       # [ num_layers x num_features+1 ]
        #self.lagrangians = np.zeros( len(init_layers), dtype=self.dtype )

    
    def get_fern_layer( self, layer_idx ) -> WeightedFernLayer:
        weights = self.regression_layers[layer_idx,:-1]
        offset = self.regression_layers[layer_idx,-1]

        # normalization of weights 
        param_norm = 1 / np.linalg.norm( weights )
        weights = weights * param_norm
        offset  = offset * param_norm
        
        # create fern layer
        fern_layer = WeightedFernLayer.create( weights.astype(np.float32), -float(offset) )
        return fern_layer


    def update_layer( self, layer_idx : int,
                             n_params : np.ndarray ):
        
        # store new parameter set
        self.regression_layers[layer_idx,:] = n_params

        # generate fern layer
        fern_layer = self.get_fern_layer( layer_idx )

        # classify for new leaf indices
        n_leaf_idx = np.zeros( self.num_data, dtype=np.int32 )
        n_leaf_idx = fern_layer.classify( n_leaf_idx, self.feature_rows, out = n_leaf_idx )
        assert np.all( n_leaf_idx < 2 )

        # shift layer bit to correct position
        n_bit_shift = self.num_layers - layer_idx - 1
        n_leaf_idx = np.left_shift(n_leaf_idx, n_bit_shift).astype(np.int32)
        
        # erase old layer
        r_leaf_idx = select_layers( self.leaf_idx, [layer_idx], num_layers=self.num_layers )  # [ num_data ]
        assert np.all( r_leaf_idx < 2 )
        rmask = np.bitwise_not( np.int32(1 << n_bit_shift) ).astype(np.int32)
        leaf_idx = np.bitwise_and( self.leaf_idx, rmask ).astype(np.int32)
        assert np.all( leaf_idx >= 0 )
        assert np.all( leaf_idx < self.num_leafs )
        assert np.all( 0 == select_layers(leaf_idx, [layer_idx], num_layers=self.num_layers) ), "Check for correct layer removal failed"

        # integrate n_leaf_idx
        assert np.all( (np.left_shift(r_leaf_idx, n_bit_shift).astype(np.int32) + leaf_idx) == self.leaf_idx ), "Check for correct layer removal failed"
        leaf_idx += n_leaf_idx
        assert np.all( leaf_idx < self.num_leafs )
        #print( "max indices: ", self.leaf_idx.max(), leaf_idx.max() )
        self.leaf_idx = leaf_idx


    def get_mutual_information( self ) -> float:
        mcounts = mutual_counts(     X = self.leaf_idx,
                                     Y = self.label_idx, 
                               weights = self.sample_weights,
                                 num_X = self.num_leafs,
                                 num_Y = self.num_labels )
        return mcounts.mutual_information( do_norm = False )
    

    def get_gini_index( self, do_norm : bool = True ) -> float:
        mcounts = mutual_counts(     X = self.leaf_idx,
                                     Y = self.label_idx, 
                               weights = self.sample_weights,
                                 num_X = self.num_leafs,
                                 num_Y = self.num_labels )
        return mcounts.gini_index( axis=1, do_norm = bool(do_norm) )



    def optimize_layer( self, layer_idx : int,
                              num_iters : int = 500,
                          slsqp_context : tp.Optional[SLSQPContext] = None ) -> SLSQPContext:
        
        if slsqp_context is None:
            slsqp_context = SLSQPContext( x0 = self.regression_layers[layer_idx,:],
                                          num_eq_constr = 1 )
        else:
            slsqp_context.reinitialize()

        # gather layer evaluator
        logreg_eval = self.logreg_layer_evaluator( layer_idx )

        # run one optimization step
        do_continue = slsqp_context.next_iteration( func = logreg_eval.eval,
                                               jac = logreg_eval.gradient,
                                  constraint_funcs = [ logreg_eval.constraint_eval ],
                                    constraint_jac = [ logreg_eval.constraint_gradient ],
                                         num_iters = int(num_iters),
                                              ftol = self.optim_ftol )
        
        # update fern layer
        self.update_layer( layer_idx, slsqp_context.x )

        return slsqp_context



    def coordinate_descent(self, max_num_cycles : int = 100,
                                      num_iters : int = 500 ):
        
        slsqp_contexts = [ SLSQPContext( x0 = self.regression_layers[layer_idx,:],
                                         num_eq_constr = 1 ) for layer_idx in range(self.num_layers) ]

        layer_optimization_turn = np.zeros( self.num_layers, dtype=np.int32 )

        # sample optimization order
        layer_indices = np.random.permutation( self.num_layers )

        for cycle_idx in range( max_num_cycles ):
            for layer_idx in layer_indices:
                #for layer_idx in [1]:
                layer_idx = int(layer_idx)

                slsqp_c = self.optimize_layer( layer_idx, num_iters, slsqp_context = slsqp_contexts[layer_idx] )

                if slsqp_c.majiter > 1:
                    layer_optimization_turn[layer_idx] = self.num_layers

                ret = cd_result_tuple( cycle_idx = cycle_idx,
                                       layer_idx = layer_idx,
                                               x = slsqp_c.x,
                                              fx = slsqp_c.fx,
                                       num_iters = slsqp_c.majiter )

                yield ret

                layer_optimization_turn -=  1
                if not np.any( layer_optimization_turn > 0 ):
                    return



    def logreg_layer_evaluator( self, layer_idx : int ) -> LogisticRegressionEvaluator:
        # remove layer from leaf indices
        r_leaf_idx = remove_layer( self.leaf_idx, layer_idx, num_layers=self.num_layers )  # [ num_data ]
        
        logreg_layer_eval = \
            LogisticRegressionEvaluator( feature_rows = self.feature_rows,
                                            label_idx = self.label_idx,
                                             leaf_idx = r_leaf_idx,
                                       sample_weights = self.sample_weights,
                                           num_labels = self.num_labels,
                                            num_leafs = 1 << (self.num_layers-1),
                                                scale = self.scale,
                                                  eps = self.eps,
                                        eval_gradient = False,
                                        eval_jacobian = False,
                                       max_cache_size = 1,
                                                dtype = self.dtype )
        
        return logreg_layer_eval


    def gradient_comparison( self, layer_idx : int ):
        """For debugging purpose"""
        
        x = self.regression_layers[layer_idx,:]

        # gather layer evaluator
        logreg_eval = self.logreg_layer_evaluator( layer_idx )
        approx_gradient, approx_hessian = logreg_eval.approximate_derivatives( x )

        optim_val = logreg_eval.eval( x )
        gradient = logreg_eval.gradient( x )
        hessian = logreg_eval.hessian( x )

        #print( gradient )
        #print( approx_gradient )
        norm = np.minimum( np.absolute(gradient), np.absolute(approx_gradient) )
        diff = np.absolute( gradient - approx_gradient ) / (norm + 1e-8)
        print( "gradient diff: ", diff.min(), diff.mean(), diff.max() )

        norm = np.minimum( np.absolute(hessian), np.absolute(approx_hessian) )
        diff = np.absolute( hessian - approx_hessian ) / (norm + 1e-3)
        print( "hessian diff: ", diff.min(), np.median( diff ), diff.max() )

