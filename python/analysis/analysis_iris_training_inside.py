# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


import numpy as np

import matplotlib.pyplot as plt
import pylineclip


from .test_iris_training_base import IrisTrainingBase
from ..training import TreeTrainer, FernTrainer

from ..clmath import MultiNodeClassStatistics

from .test_training_inside_lib import project_splitting_plane



class IrisTrainingViz( IrisTrainingBase ):

    num_tree_layers = 4
    num_fern_layers = 5


    def tree_training(self):
        tree_features = self.raw_data
        #tree_features = self.ratio_features

        trainer = TreeTrainer( self.labels, self.data_weights )

        observer = None

        # train
        layers, node_ratings, nodeIdx = trainer.train_tree( tree_features, self.num_tree_layers, observer=observer )



    def fern_training(self):
        fern_features = self.raw_data
        trainer = FernTrainer( self.labels, self.data_weights )

        #observer = DebugFernTrainingObserver( self )
        observer = None

        layers, node_ratings, nodeIdx = trainer.train_fern( fern_features, self.num_fern_layers, observer=observer )
        return (layers, nodeIdx)
    

    def visualize_data_with_labels( self, fern_layers=None ):
        self.visualize_raw_data( self.labels, self.label_names, fern_layers )


    def visualize_proj_with_labels( self, fern_layers ):
        proj_vects = [ f.feature_weights for f in fern_layers ]    # [ L ]
        proj_data = np.dot( np.stack( proj_vects ), self.raw_data )   # [ L x N ]

        self.visualize_proj_data( self.labels, self.label_names, proj_vects, proj_data, fern_layers, "Layer %02d" )


    def visualize_data_with_nodes( self, nodeIdx, fern_layers=None ):
        if nodeIdx.min() < 0:
            nodeIdx = np.absolute(nodeIdx)
        
        node_labels = list( [ "n%02d" % n for n in range(nodeIdx.max()+1) ] )
        self.visualize_raw_data( nodeIdx, node_labels, fern_layers )


    def visualize_proj_with_nodes( self, nodeIdx, fern_layers ):
        proj_vects = [ f.feature_weights for f in fern_layers ]    # [ L ]
        proj_data = np.dot( np.stack( proj_vects ), self.raw_data )   # [ L x N ]
        
        if nodeIdx.min() < 0:
            nodeIdx = np.absolute(nodeIdx)
        
        node_labels = list( [ "n%02d" % n for n in range(nodeIdx.max()+1) ] )
        self.visualize_proj_data( nodeIdx, node_labels, proj_vects, proj_data, fern_layers, "Layer %02d" )




    def visualize_raw_data( self, group_idx, group_names, fern_layers=None ):
        fig = self.create_figure()
        fig.suptitle('Iris Data')  # Add a title so we know which it is

        num_groups = len(group_names)

        aidx = 0
        for i in range(1, self.num_dims):
            for j in range(i):
                ax = plt.subplot( 2, 3, aidx+1 )

                for l in range(num_groups):
                    mask = group_idx == l
                    if mask.sum() == 0:
                        continue
                    
                    plt.plot( self.raw_data[i,mask], self.raw_data[j,mask], '.', label=group_names[l] )
                
                plt.xlabel( self.feature_names[i] )
                plt.ylabel( self.feature_names[j] )

                if fern_layers is not None:
                    # perpare clipping
                    dmin = self.raw_data[[i,j],:].min(axis=1)
                    dmax = self.raw_data[[i,j],:].max(axis=1)

                    for lidx, layer in enumerate(fern_layers):
                        # get line parameters
                        nvect = layer.feature_weights
                        offset = layer.feature_split

                        D = nvect.size
                        ivect = np.eye(D,dtype=nvect.dtype)[i,:]
                        jvect = np.eye(D,dtype=nvect.dtype)[j,:]

                        # get intersection line
                        pc, pvect = project_splitting_plane( ivect, jvect, nvect, offset )

                        # estimate scale
                        scale = np.linalg.norm(dmax - dmin)

                        # two points on line
                        p1 = pc + scale * pvect   # [ 2 ]
                        p2 = pc - scale * pvect   # [ 2 ]

                        # clipping
                        res = pylineclip.cohensutherland( dmin[0], dmax[1], dmax[0], dmin[1], p1[0], p1[1], p2[0], p2[1] )
                        if None not in res:
                            p1 = np.array( [res[0],res[2]], dtype=np.float32 )
                            p2 = np.array( [res[1],res[3]], dtype=np.float32 )
                        else:
                            continue

                        #print(p1,p2)
                        plt.plot( p1, p2, 'k:' )
                        plt.text( p1.mean(), p2.mean(), "L%02d" % (lidx+1), fontsize=6)
                    
                ax.yaxis.set_major_formatter(plt.NullFormatter())
                ax.xaxis.set_major_formatter(plt.NullFormatter())

                aidx += 1
        
        plt.legend()
        


    def visualize_proj_data( self, group_idx, group_names, proj_vects, proj_data, fern_layers, ax_name_pattern ):
        """
        Parameters
        ----------
        group_idx : int32 ndarray [ len=N ]
        proj_vects : P x [ float32 ndarray, len=D ]
        proj_data : [ D x N ]
        """

        fig = self.create_figure()
        fig.suptitle('Iris Data')  # Add a title so we know which it is

        num_groups = len(group_names)
        num_proj   = len(proj_vects)

        for i in range( 1, num_proj ):
            ivect = proj_vects[i]
            idata = proj_data[i,:]

            for j in range(i):
                jvect = proj_vects[j]
                jdata = proj_data[j,:]

                aidx = (num_proj-1) * (i-1) + j
                ax = plt.subplot( num_proj-1, num_proj-1, aidx+1 )

                for l in range(num_groups):
                    mask = group_idx == l
                    if mask.sum() == 0:
                        continue
                    
                    plt.plot( idata[mask], jdata[mask], '.', label=group_names[l] )
                
                plt.xlabel( ax_name_pattern % (i+1) )
                plt.ylabel( ax_name_pattern % (j+1) )


                # perpare clipping
                dmin = np.array( [idata.min(), jdata.min()], dtype=np.float32 )
                dmax = np.array( [idata.max(), jdata.max()], dtype=np.float32 )

                for lidx, layer in enumerate( fern_layers ):

                    # get line parameters
                    nvect = layer.feature_weights
                    offset = layer.feature_split

                    # get intersection line
                    pc, pvect = project_splitting_plane( ivect, jvect, nvect, offset )

                    # estimate scale
                    scale = max( np.linalg.norm(pc - dmin), np.linalg.norm(dmax - pc) )

                    # two points on line
                    p1 = pc + scale * pvect   # [ 2 ]
                    p2 = pc - scale * pvect   # [ 2 ]

                    # clipping
                    res = pylineclip.cohensutherland( dmin[0], dmax[1], dmax[0], dmin[1], p1[0], p1[1], p2[0], p2[1] )
                    if None not in res:
                        p1 = np.array( [res[0],res[2]], dtype=np.float32 )
                        p2 = np.array( [res[1],res[3]], dtype=np.float32 )
                    else:
                        continue

                    #print(p1,p2)
                    if lidx in (i,j):
                        style = "k-"
                    else:
                        style = "k:"
                    plt.plot( p1, p2, style )
                    plt.text( p1.mean(), p2.mean(), "L%02d" % (lidx+1), fontsize=6)
                
                ax.yaxis.set_major_formatter(plt.NullFormatter())
                ax.xaxis.set_major_formatter(plt.NullFormatter())
        
        #plt.legend()


    def plot_lda_data( self, group_idx, group_names, fern_layers ):
        
        #self.create_figure()
        #num_groups = len(group_names)

        lda_inst = MultiNodeClassStatistics( np.zeros_like( self.labels ), 
                                             self.labels,
                                             list([ self.raw_data[i,:] for i in range(self.raw_data.shape[0]) ]),
                                             self.data_weights )
        
        eigenvalue, eigenvector = lda_inst.run_lda()

        order = np.argsort( eigenvalue )[::-1]
        eigenvector = eigenvector[:,order]
        eigenvalue = eigenvalue[order]
        num_dims = len(eigenvalue)

        proj_data = np.dot( np.transpose( eigenvector ), self.raw_data )  # [ D x N ]
        proj_vects = [ eigenvector[:,i] for i in range(num_dims) ]

        self.visualize_proj_data( group_idx, group_names, proj_vects, proj_data, fern_layers, "Comp. %02d" )


    @staticmethod
    def create_figure():
        plt.rcParams.update({'font.size': 7, 'lines.linewidth' : 0.5})
        fig = plt.figure(dpi=110, figsize=(13, 8))
        plt.tight_layout() # Unit: fraction of font size
        #plt.tight_layout(pad=1.0, w_pad=2.0, h_pad=2.0) # Unit: fraction of font size
        plt.subplots_adjust( left=0.05, right=0.99, bottom=0.05, top=0.95, wspace=0.25, hspace=0.3 )
        return fig

# -----------------------------------------------------------------------------------------------------------


if __name__ == '__main__':
    #unittest.main()

    inst = IrisTrainingViz()
    fern_layers, nodeIdx = inst.fern_training()
    
    inst.visualize_data_with_labels( fern_layers )
    inst.visualize_data_with_nodes( nodeIdx, fern_layers )
    inst.visualize_proj_with_labels( fern_layers )
    inst.visualize_proj_with_nodes( nodeIdx, fern_layers )
    
    inst.plot_lda_data( inst.labels, inst.label_names, fern_layers )
    plt.show()

