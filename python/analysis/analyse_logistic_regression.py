# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


from collections.abc import Sequence

import numpy as np
import logging
logging.basicConfig(level=logging.DEBUG)
#import matplotlib.pyplot as plt

#import sys
#sys.path.append("..") # Adds higher directory to python modules path.

#from ..tests import IrisTrainingBase
from ..tests import DigitsTrainingBase
from ..training import FernTrainer, FernTrainingContext, CountingFernTrainingObserver
from ..training import ObserverNotificationSource

from ..fern import remove_layer, select_layers
from ..fern import LogisticFernRegressionAlgo
#from ..fern import LogisticRegressionStatCounter
#from ..fern import LogisticRegressionFernLayer
#from ..fern import EntropyOptimization
#from ..fern import LogisticRegressionEvaluator
#from ..fern import MinimizationContext


from ..clmath import mutual_counts
#from ..clmath import normalize_entropy, normalize_gini, MultiNodeClassStatistics
from ..parameter_registration import default_fern_training_params



from ..ctypes_py import WeightedFernLayer

from scipy.optimize import minimize

#from ..clmath import MultiNodeClassStatistics
#from .test_training_inside_lib import project_splitting_plane

import matplotlib.pyplot as plt




def create_logger( prefix ):
    logger = logging.getLogger( prefix )
    logger.setLevel( logging.DEBUG )

    #formatter = logging.Formatter( "%(asctime)s;%(levelname)s;%(message)s", "%Y-%m-%d %H:%M:%S")
    
    #formatter = logging.Formatter( "%(name)s:%(asctime)s: %(message)s","%z" )
    #ch = logging.StreamHandler()
    #ch.setFormatter(formatter)
    #logger.addHandler(ch)
    return logger


class LogisticRegressionAnalyser( DigitsTrainingBase ):

    dtype = np.float32
    #dtype = np.float128

    feature_rows : Sequence

    random_seed     = 59
    #max_iter        = 5

    layers = None
    leaf_idx = None
    node_rating = None


    estimator = None
    #lidx = None

    w_serie : list


    def __init__(self):
        super().__init__()
        self.feature_rows = list( self.raw_data )
        self.w_serie = list()

        self.run_fern_training()


    def run_fern_training(self):

        logger = create_logger( "logreg_analysis" )

        # configure training parameters
        parameter_registry = default_fern_training_params.copy()
        parameter_registry.set_param( 'train_fern.optimize.do_retrain_layers', True )
        parameter_registry.set_param( 'train_fern.optimize.do_logreg_before_retraining', False )
        parameter_registry.set_param( 'train_fern.optimize.do_logreg_after_retraining', False )

        trainer = FernTrainer( self.labels, self.data_weights, logger=logger )

        #observer = DebugFernTrainingObserver( self )
        observer = CountingFernTrainingObserver(True,True)
        train_context = FernTrainingContext( observer )
        train_context.feature_rows = self.feature_rows
        self.feature_rows = train_context.feature_rows

        # running the training
        # Training Context
        node_rating = trainer.train_fern( train_context, num_layers=1 )

        self.layers = train_context.layers
        self.node_rating = node_rating
        self.leaf_idx = train_context.leaf_idx

        print( "LDA-Layer with Node Rating: ", node_rating, "\t", self.evalute_layer(train_context.layers[0]) )
        print( "Mutual information after LDA: ", observer.mutual_inf_measure )
        print( "Gini Index after LDA: ", observer.gini_measure )
    

    def initialize_estimator( self ):
        estimator = LogisticFernRegressionAlgo( feature_rows   = self.feature_rows, \
                                                label_idx      = self.labels, \
                                                leaf_idx       = self.leaf_idx, \
                                                sample_weights = None, \
                                                num_labels     = self.num_labels, \
                                                dtype          = self.dtype )
        self.estimator = estimator
        self.estimator.l2weight = 0.01  # l2norm regularisation

    
    def debug_callback(self, x_arg ):
        w_abs = np.linalg.norm( x_arg[:-1] )
        w_norm = 1. / w_abs

        weights = x_arg * w_norm
        test_layer = WeightedFernLayer.create( weights[:-1].astype(np.float32), -float(weights[-1]) )

        print( "|w| = %.3f\t" % w_abs, self.evalute_layer(test_layer), "\t", self.evaluate_weights(weights) )
        self.w_serie.append(weights)


    def evalute_layer(self, layer ):
        n_leaf_idx = layer.classify( np.zeros(self.num_data,dtype=np.int32), self.feature_rows )
        
        mcounts = mutual_counts( self.labels, n_leaf_idx )
        mutual_inf = mcounts.mutual_information( do_norm = False)
        gini_index = mcounts.gini_index( axis=0, do_norm=True )
        cond_entropy = mcounts.conditional_entropy( axis=0, do_norm = True, use_log2 = True )


        res_str = "muinf=%.3f  gini=%.4f  cond_ent=%.3f" % (mutual_inf, gini_index, cond_entropy )
        return res_str

    def evaluate_weights(self, n_weights):
        
        # calculate lot points of planes
        w0 = self.w_serie[0][:-1] * self.w_serie[0][-1]
        neww = n_weights[:-1] * n_weights[-1]
        lastw = self.w_serie[-1][:-1] * self.w_serie[-1][-1]

        res_str = "|w0-wk|=%.4f  |wkm1 - wk|=%.8f" % ( np.linalg.norm(w0-neww), np.linalg.norm(lastw-neww) )
        return res_str


    def run_logreg_training(self):
        
        min_context = self.estimator.get_optimization_context( self.layers[0] )
        
        #min_context.optim_method = "CG"          # using only first derivative
        #min_context.optim_method = "BFGS"        # using first only and aproximating second

        #min_context.optim_method = "Newton-CG"   # truncated Newton, using hessian
        #min_context.optim_method = "trust-ncg"   # Newton conjugate gradient trust-region
        #min_context.optim_method = "trust-exact" # only available in newer scipy
        #min_context.optim_method = "trust-krylov"
        
        self.w_serie.append(min_context.x0)
        self.estimator.optimize_layer( min_context, num_iters = 500, callback = self.debug_callback )

        n_layer = self.estimator.get_fern_layer( min_context )



# -----------------------------------------------------------------------------------------------------------


if __name__ == '__main__':
    #unittest.main()

    inst = LogisticRegressionAnalyser()
    #inst.run_fern_training()
    inst.initialize_estimator()
    inst.run_logreg_training()



