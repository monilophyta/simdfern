# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


import numpy as np
import scipy
from ..clmath import MultiNodeClassStatistics, js_divergence



class TestLDA( object ):
    dtype = np.float32
    randseed = 49+1
    num_data = 1000000


    def analyse_multiple_nodes_and_classes(self):

        data, label_idx, node_idx, weights, R = \
                self.sample_data( num_labels=2,
                                  num_nodes=2,
                                  class_offset = np.array([-4,4],np.int32),
                                  node_offset = np.array([8,-8],np.int32),
                                  bgscale=3, bgoffset = 0,
                                  max_weight=1,
                                  rot_angle= 0 )  # [ N x 2 ]
        
        test_stat = MultiNodeClassStatistics( node_idx, label_idx, ( data[:,0], data[:,1] ), weights, do_verify=True )
        print( test_stat.intra_cov )
        print( test_stat.inter_cov )

        _, py_intra_cov, py_inter_cov = test_stat.pythonic_covar_estimation( node_idx, label_idx, ( data[:,0], data[:,1] ), weights  )
        print( py_intra_cov )
        print( py_inter_cov  )

        print( "R" )
        print( R )

        diff_cov = test_stat.inter_cov - test_stat.intra_cov
        eigenvalue, eigenvector = np.linalg.eigh( diff_cov )
        print( "eigenvalues 1: ", eigenvalue )
        print( eigenvector )

        diff_cov = test_stat.intra_cov - test_stat.inter_cov
        eigenvalue, eigenvector = np.linalg.eigh( diff_cov )
        print( "eigenvalues 2: ", eigenvalue )
        print( eigenvector )
        
        eigenvalue, eigenvector = scipy.linalg.eigh( test_stat.inter_cov, b=test_stat.intra_cov, type = 1, turbo=False, eigvals_only=False )
        print( "eigenvalues 3: ", eigenvalue )
        print( eigenvector )

        eigenvalue, eigenvector = scipy.linalg.eigh( test_stat.intra_cov, b=test_stat.inter_cov, type = 1, turbo=False, eigvals_only=False )
        print( "eigenvalues 4: ", eigenvalue )
        print( eigenvector )

    # ---------------------------------------------------------------------------------------------------------------------

    def sample_data(self, num_labels, num_nodes, class_offset, node_offset, bgscale, bgoffset, max_weight=1, rot_angle=0 ):
        
        randstate = np.random.RandomState( self.randseed )

        labels = randstate.randint( num_labels, size=self.num_data, dtype=np.int32 )
        nodes = randstate.randint( num_nodes, size=self.num_data, dtype=np.int32 )

        # main data dim
        data = randstate.normal( size=self.num_data ).astype( self.dtype ) \
                + class_offset.take( labels, axis=0 ) \
                + node_offset.take( nodes, axis=0 )
        
        # background noise dim
        backgr = (bgscale * randstate.normal( size=self.num_data ) ) + bgoffset

        #combined data
        data = np.stack(( data, backgr )) # [ 2 x N ]

        R = np.array( [ [ np.cos( rot_angle ), -np.sin( rot_angle) ],
                        [ np.sin( rot_angle ),  np.cos( rot_angle) ] ], dtype=self.dtype )
        # weights
        weights = randstate.randint(1,max_weight+1, size=self.num_data, dtype=np.int32 )
        assert( np.all(weights <= max_weight) )

        return ( np.dot(R, data).transpose(), labels, nodes, weights, R ) # [ N x 2 ]

# ------------------------------------------------------------------------------------------------


TestLDA().analyse_multiple_nodes_and_classes()
