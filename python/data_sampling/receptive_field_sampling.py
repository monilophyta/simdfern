# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



__all__= ["ReceptiveFieldSampler"]

import numpy as np
from .receptive_field import ReceptiveField


class ReceptiveFieldSampler( object ):

    height = None
    width  = None


    def __init__( self, height, width  ):
        self.height = height
        self.width  = width
    

    def draw_uniform_sample(self, rand_state=None ) -> ReceptiveField:

        if not isinstance( rand_state, np.random.RandomState ):
            rand_state = np.random.RandomState( rand_state )
        
        row_pos = rand_state.randint( 0, self.height, 1, np.int32 )[0]
        col_pos = rand_state.randint( 0, self.width, 1, np.int32 )[0]

        return ReceptiveField( row_pos, col_pos, self.height, self.width )



