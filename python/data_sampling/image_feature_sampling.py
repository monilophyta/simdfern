# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["ImageFeatureSampler", "calc_number_of_feature_boxes"]


import numpy as np
import math as m
import typing as tp

from .receptive_field_sampling import ReceptiveFieldSampler
from .receptive_field import ReceptiveField
from ..image import compute_integral_image, ImageCoords, ImageBoxes
from ..ctypes_py import BoxEvaluator



# ---------------------------------------------------------------------------------------------
# --- Inner Moduls Utils ----------------------------------------------------------------------


class SingleChannelBoxExtractor( object ):

    box_eval = None

    def __init__(self, box_eval ):
        self.box_eval = box_eval
    

    def __call__(self, positions, image ):
        assert image.ndim == 2
        
        # integral image in this channel
        img_int = compute_integral_image( image )
        
        # box integrals
        box_int = self.box_eval.process( img_int, positions )

        return box_int


class MultiChannelBoxExtractor( object ):

    box_evals = None

    @property
    def num_channels(self):
        return len(self.box_evals)
    
    @property
    def num_boxes(self):
        return sum( [bv.num_boxes for bv in self.box_evals ] )


    def __init__(self, box_evals ):
        self.box_evals = box_evals


    def __call__(self, positions, image ):
        assert image.ndim == 3
        assert image.shape[0] == self.num_channels
        
        num_sel_data = positions.size
        box_int = list()

        for ch_idx, box_eval in enumerate( self.box_evals ):
            if not isinstance( box_eval, BoxEvaluator ):
                # no features in this channel
                assert box_eval is False
                continue
                
            assert isinstance( box_eval, BoxEvaluator )
            
            # integral image in this channel
            channel_int = compute_integral_image( image[ch_idx,:] )
            
            # box integrals
            box_int_vals = box_eval.process( channel_int, positions )
            assert box_int_vals.shape[0] == num_sel_data
            box_int.append( box_int_vals )  # [ P x Fc ]  - Fc number of featurs in channel c
        
        
        # concatenate features
        box_int = np.concatenate( box_int, axis=1 )  # [ P x F ]
        assert box_int.shape[0] == num_sel_data
        assert box_int.shape[1] == self.num_boxes
        
        return box_int


# ---------------------------------------------------------------------------------------------



class ImageFeatureSampler( object ):
    """

    Attributes
    ----------
    data_images, \
    num_features               : int
    rand_state                 = None
    max_rfield_height          : int = 101
    max_rfield_width           : int = 101
    min_box_size               : int = 2
    max_box_size               : int = 99
    use_inter_channel_features : bool = False
    channel_weights
    
    #------------------------------------------------------------------------------------------------------------------
    # Usage Variant 1 (faster):
    # Condition: The label images are independent of receptive field shape (no invalid areas in label images)

    pos_sampler = ImagePositionSampler( label_img_sequence, **otherl_params )

    with ImageFeatureSampler( data_img_sequence, num_features, rand_state, **otheri_params ) as imgf_sampler:
        
        for img_idx, positions, labels in pos_sampler.position_iterator( num_data, imgf_sampler.receptive_field, rand_state ):

            data = imgf_sampler( img_idx, positions )

            # ....
            # .... store/use (data,labels)
    
    #------------------------------------------------------------------------------------------------------------------
    # Usage Variant 2 (slower):
    # Condition: The label images are independent of receptive field shape (no invalid areas in label images)
    
    with ImageFeatureSampler( data_img_sequence, num_features, rand_state, **otheri_params ) as imgf_sampler:

        label_img_sequence = get_label_sequence( imgf_sampler.receptive_field )
        pos_sampler = ImagePositionSampler( label_img_sequence, **otherl_params )

        for img_idx, positions, labels in pos_sampler.position_iterator( num_data, imgf_sampler.receptive_field, rand_state ):

            data = imgf_sampler( img_idx, positions )

            # ....
            # .... store/use (data,labels)
    """

    ## permanent Variables
    data_images                = None
    num_features               = None

    receptive_field_sampler    = None

    use_inter_channel_features = False
    channel_weights            = None

    rand_state                 = None

    # values for debugging
    max_image_value            = None


    @property
    def num_data(self):
        return len(self.data_images)

    @property
    def num_channels(self):
        return self.data_images.num_channels

    @property
    def image_height(self):
        return self.data_images.height
    
    @property
    def image_width(self):
        return self.data_images.width
    

    ## state variables
    box_extractor      = None
    rfield             = None
    boxes              = None
    box_scaling        = None
    channel_indices    = None
    channel_box_counts = None

    @property
    def receptive_field(self):
        if self.box_extractor is None:
            return None
        return self.rfield


    def __init__(self, data_images, \
                       max_image_value,
                       num_features               : int, \
                       max_rfield_height          : int = 101, \
                       max_rfield_width           : int = 101, \
                       min_box_size               : int = 2, \
                       max_box_size               : int = 99, \
                       use_inter_channel_features : bool = False, \
                       channel_weights             = None,
                       rand_state                 : tp.Optional[np.random.RandomState] = None ):
        """
        Attributes
        ----------
        data_images : sequences of imag
            For single channel images the shape should be [ W x H ].
            For multi channel images the shape should be [ C x W x H ]
        max_image_value : int or float
            maximal possible image pixel value.
            Used in box feature evaluation.
        rand_state : instance of numpy.random.RandomState
        """
        max_rfield_height = int( max_rfield_height )
        max_rfield_width  = int( max_rfield_width )
        min_box_size      = int( min_box_size )
        max_box_size      = int( max_box_size )
        num_features      = int( num_features )

        assert max_rfield_width > 0
        assert max_rfield_height > 0
        assert min_box_size > 0
        assert max_box_size >= min_box_size
        assert max_rfield_width >= max_box_size
        assert max_rfield_height >= max_box_size
        
        assert num_features > 0
        assert data_images.num_channels > 0

        self.data_images = data_images
        self.num_features = num_features

        if not isinstance( rand_state, np.random.RandomState ):
            rand_state = np.random.RandomState( rand_state )
        self.rand_state = rand_state

        # instance for sampling of receptive fields
        self.receptive_field_sampler = ReceptiveFieldSampler( max_rfield_height, max_rfield_width )
        self.min_box_size = min_box_size
        self.max_box_size = max_box_size

        if self.num_channels > 1:
            self.use_inter_channel_features = bool( use_inter_channel_features )

            # weight for channel sampling
            if channel_weights is None:
                channel_weights = np.array( [ 1./ self.num_channels ], dtype=np.float32 ).repeat( self.num_channels )
            else:
                channel_weights = np.require( channel_weights, dtype=np.float32 )
            assert channel_weights.size == self.num_channels

            self.channel_weights = channel_weights
        
        self.max_image_value = float( max_image_value )


    def __enter__(self):
        assert self.box_extractor is None
        assert self.boxes is None
        assert self.rfield is None
        assert self.channel_indices is None
        assert self.channel_box_counts is None

        self.box_extractor, \
        self.boxes, \
        self.box_scaling, \
        self.rfield, \
        self.channel_indices, \
        self.channel_box_counts =  self.__prepare_entrance()

        return self


    def __exit__(self, exception_type, exception_value, traceback):
        self.box_extractor      = None
        self.boxes              = None
        self.box_scaling        = None
        self.rfield             = None
        self.channel_indices    = None
        self.channel_box_counts = None


    def __call__(self, img_idx, positions ):
        assert isinstance( img_idx, int )
        assert isinstance( positions, ImageCoords )
        
        img = self.data_images[ img_idx ]
        data = self.box_extractor( positions, img )  # [ nN x F ]

        # check for resonable values
        assert np.all( np.isfinite( data ) )
        assert np.all( data >= 0 ), "Invalid data normalization"
        assert np.all( data <= 1 ), "Invalid data normalization"

        return data


    def __prepare_entrance( self ) -> tp.Tuple[ tp.Union[SingleChannelBoxExtractor,MultiChannelBoxExtractor], \
                                                ImageBoxes, \
                                                ReceptiveField, \
                                                np.ndarray ]:
        
        channel_box_counts = self.__get_channel_box_assigments()
        assert channel_box_counts.size == self.num_channels
        
        # how many boxes have to be samples
        num_boxes = channel_box_counts.sum()

        # sample receptive field
        receptive_field = self.receptive_field_sampler.draw_uniform_sample( self.rand_state )

        # sample boxes
        boxes = receptive_field.draw_uniform_box_samples( num_boxes, \
                                                          self.min_box_size, \
                                                          self.max_box_size, \
                                                          self.rand_state )

        # normalization of box areas
        box_scaling = np.reciprocal( boxes.area.astype(np.float32) * self.max_image_value )
        assert np.all( np.isfinite( box_scaling ) )

        # prepare channel handling
        if self.num_channels == 1:
            channel_indices = np.zeros( num_boxes, dtype=np.int16 )

            # construct on ctypes box evaluation for all boxes
            box_eval = BoxEvaluator( boxes, scale=box_scaling )
            box_extractor = SingleChannelBoxExtractor( box_eval )
        else:
            channel_indices = np.arange( self.num_channels, dtype=np.int16 )
            channel_indices = channel_indices.repeat( channel_box_counts )

            cb_end   = np.add.accumulate( channel_box_counts )
            cb_start = np.concatenate( ([0], cb_end[:-1]) )

            box_evals = [ BoxEvaluator( boxes.select( slice(s,e) ), \
                                        scale= box_scaling[s:e] ) \
                          for s,e in zip( cb_start, cb_end ) if (s < e) ]
            
            box_extractor = MultiChannelBoxExtractor( box_evals )

        return (box_extractor, boxes, box_scaling, receptive_field, channel_indices, channel_box_counts )



    def __get_channel_box_assigments(self):

        if self.num_channels == 1:
            return np.array( [ calc_number_of_feature_boxes( self.num_features ) ], dtype=np.int32 )
        
        if self.use_inter_channel_features is True:
            # all boxes can be used in all features
            num_boxes = calc_number_of_feature_boxes( self.num_features )

            channel_box_counts = self.rand_state.multinomial(       n = num_boxes, \
                                                                pvals = self.channel_weights, \
                                                                 size = 1 ).ravel()
        else:
            # only channel specific feature shall be used
            assert (self.num_channels > 1) and (self.use_inter_channel_features is False)

            channel_feature_counts = self.rand_state.multinomial(     n = self.num_features, \
                                                                  pvals = self.channel_weights, \
                                                                   size = 1 ).ravel()

            channel_box_counts = np.array( [ calc_number_of_feature_boxes( cfc ) for cfc in channel_feature_counts ], \
                                           dtype=np.int32 )
        
        return channel_box_counts


# ---------------------------------------------------------------------------------------------


def calc_number_of_feature_boxes( num_features : int ) -> int:

    num_features = int(num_features)
    assert num_features >= 0
    if num_features < 1:
        return 0

    numb_from_numf = lambda numf: (1 + m.sqrt( 1 + (8*numf) )) / 2
    # mainly for debugging
    numf_from_numb = lambda numb: (numb * (numb - 1)) // 2

    num_boxes = numb_from_numf( num_features )
    assert num_features >= numf_from_numb( int(num_boxes) )
    
    num_boxes = int(m.ceil(num_boxes))
    assert num_features <= numf_from_numb( num_boxes )

    return num_boxes
