# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



__all__ = [ "SequentialPixelSelector" ]

import numpy as np
from ..image import ImageCoords


class SequentialPixelSelector( object ):
    """
    
    Annotation: 
    nC: number of different classes/labels
    Pc: number of pixel position available for sampling

    Attributes
    ----------
    rand_state : instance of numpy.random.RandomState
    pixel_pos_idx : [ nC x [ Pc x 2 ] ]
    pixel_sample_counts : list of integer arrays - [ nC x [ Pc ] ]
        number of items to be drawn at this position
    pixel_class_counts : list of integer arrays - [ nC x [ Pc ] ]
        number of available pixel of desired class at pixel position
    """

    rand_state       = None  # instance of np.random.RandomState

    pixel_pos_idx    = None  # nC x [ Pc x 2 ]
    
    pixel_sample_counts = None  # nC x [ Pc ]
    pixel_class_counts  = None  # nC x [ Pc ]



    def __init__(self, rand_state,
                       pixel_pos_idx,
                       pixel_sample_counts,
                       pixel_class_counts ):
        
        self.rand_state          = rand_state

        self.pixel_pos_idx       = pixel_pos_idx        # nC x [ Pc x 2 ]

        self.pixel_sample_counts = pixel_sample_counts  # nC x [ Pc ]
        self.pixel_class_counts  = pixel_class_counts   # nC x [ Pc ]


    @property
    def num_classes(self):
        return len( self.pixel_pos_idx )


    def sample_data( self, label_img, frame_weight : float = 1 ):
        """
        Parameters
        ----------
        label_img : numpy integer array [ H x W ]
        frame_weight : float  >= 0
            weight > 1 over-represents data in this frame
            weight < 1 under-represents data in this frame
        """
        
        # label_img:  [ H x W ]

        assert( label_img.max() < self.num_classes )
        frame_weight = float( frame_weight )
        assert frame_weight > 0

        selected_positions = []
        selecled_labels = []
        selected_weights = []


        for class_idx in range( self.num_classes ):

            class_counts = self.pixel_class_counts[ class_idx ]

            if class_counts is None:
                assert( self.pixel_pos_idx[ class_idx ] is None )
                assert( self.pixel_sample_counts[ class_idx ] is None )
                continue
            
            sample_counts = self.pixel_sample_counts[ class_idx ]
            assert( issubclass(sample_counts.dtype.type, np.integer) )
            pos_idx       = self.pixel_pos_idx[ class_idx ]

            # sample pixel indices of class "class_idx"
            selected_pos_idx, selected_counts, remaining_stats = self.draw_class_samples( label_img, frame_weight, class_idx, pos_idx, class_counts, sample_counts )

            if selected_pos_idx is not None:
                # selected_pos_idx:  [ Nc x 2 ]
                Nc = selected_pos_idx.shape[0]
                selected_positions.append( selected_pos_idx )
                selecled_labels.append( np.asarray( class_idx, dtype=label_img.dtype ).repeat( Nc ) )
                selected_weights.append( selected_counts.astype(np.int32) )    # selected counts can serve as weights in laters training

                # set new stats
                pos_idx, class_counts, sample_counts = remaining_stats

                assert( self.pixel_pos_idx[ class_idx ].shape[0] >= pos_idx.shape[0] )
                self.pixel_pos_idx[ class_idx ]       = pos_idx                 # [ Pc ]

                assert( self.pixel_class_counts[ class_idx ].size >= class_counts.size )
                self.pixel_class_counts[ class_idx ]  = class_counts            # [ Pc ]
                
                assert( self.pixel_sample_counts[ class_idx ].size >= sample_counts.size )
                self.pixel_sample_counts[ class_idx ] = sample_counts           # [ Pc ]
        
        #############
        assert( len(selected_positions) == len(selecled_labels) )

        if len(selected_positions) > 0:
            selected_positions = np.concatenate( selected_positions, axis=0 )
            selecled_labels    = np.concatenate( selecled_labels, axis=0 )
            selected_weights   = np.concatenate( selected_weights, axis=0 )
        else:
            selected_positions = np.empty( (0,2), dtype=np.int32 )
            selecled_labels    = np.empty( 0, dtype=label_img.dtype )
            selected_weights   = np.empty( 0, dtype=np.int32 )
        
        selected_positions = ImageCoords( selected_positions[:,0], selected_positions[:,1] )

        return (selected_positions, selecled_labels, selected_weights)


    def draw_class_samples( self, label_img,
                               frame_weight : float,
                                  class_idx : int,
                                    pos_idx,
                               class_counts,
                              sample_counts ):
        """
        Parameters
        ----------
        label_img : 
        frame_weight : float
        class_idx : integer;  0 <= x < self.num_classes
        pos_idx : integer matrix [ Pc x 2 ]
        class_counts : integer array [ Pc ]
            number of availabe labels at specified position in this and future frames
        sample_counts : integer array [ Pc ]
            number of labeled items to be sampled at specified position
        """
        assert frame_weight > 0

        # label at pixel positions
        pixel_label = label_img[ pos_idx[:,0], pos_idx[:,1] ]   # [ Pc ]

        # indices of pixels with correct labels
        candidate_label_idx = np.flatnonzero( pixel_label == class_idx )    # [ pcAux <= Pc ]

        # no relevant pixels
        if candidate_label_idx.size == 0:
            return (None,None,None) # (pos_idx, class_counts, sample_counts)
        
        # filter out candidates on positions where no sampling has to happen anymore
        candidate_sample_counts = sample_counts[ candidate_label_idx ]  # [ pcAux ]
        candidate_valid_mask    = candidate_sample_counts > 0    # Masking indicating valid positions for sampling
        candidate_label_idx     = candidate_label_idx.compress( candidate_valid_mask )     # [ pc <= pcAux ]
        candidate_sample_counts = candidate_sample_counts.compress( candidate_valid_mask ) # [ pc ]
        del candidate_valid_mask

        # grab number of pixels still available for sampling
        candidate_class_counts = class_counts[ candidate_label_idx ]  # [ pc ]
        assert( np.all( candidate_class_counts > 0 ) )  # "zero" should have been removed already

        ## Sampling  ##################################################################

        # calculate binomial sampling frequencies
        candidate_freq = np.reciprocal( candidate_class_counts, dtype=np.float32 )  # [ pc ]

        # apply frame weight
        candidate_freq = np.power( candidate_freq, 1. / frame_weight )

        # Binomial sampling
        selected_counts = self.rand_state.binomial( candidate_sample_counts, candidate_freq )  # [ pc ]

        # mask of selected pixels
        selected_mask = selected_counts > 0     # [ pc ]
        
        # select sampled pixel positions
        selected_pos_idx = pos_idx[ candidate_label_idx[ selected_mask ], :]  # [ sum(selected_mask) x 2 ]
        assert( np.all( label_img[ selected_pos_idx[:,0], selected_pos_idx[:,1] ] == class_idx ) )

        ## Clean up  ##################################################################

        # decrease available counts
        class_counts[ candidate_label_idx ] -= 1

        # decrease items to be sampled
        assert( np.all( selected_counts <= sample_counts[ candidate_label_idx ] ) )
        sample_counts[ candidate_label_idx ] -= selected_counts

        # remove non sampleable pixel positions
        still_sampleable_idx = np.flatnonzero( sample_counts > 0 )
        if still_sampleable_idx.size < sample_counts.size:
            sample_counts = sample_counts[ still_sampleable_idx ]
            class_counts  = class_counts[ still_sampleable_idx ]
            pos_idx       = pos_idx[ still_sampleable_idx, : ]

        ### Done
        return (selected_pos_idx, selected_counts[selected_mask], (pos_idx, class_counts, sample_counts))

