# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



__all__ = ["ItemWeightSampling", "sample_data_weights"]


import numpy as np
import typing as tp


#-----------------------------------------------------------------------------------------------------------


def sample_data_weights( labels : np.ndarray, \
                         prior_sample_weights : tp.Optional[np.ndarray] = None, \
                         eq_distr_fading :float = 1., \
                         rands = None ) -> np.ndarray:
    """
    Parameters
    ----------
    labels : int32 array [ N ]
        labels of sample vectors
    prior_sample_weights : none or int32 array [ N ]
        prior sample of data weights
    eq_distr_fading : float
        fading value between 0 (original distribution) and 1 (uniform distribution)
    rands : None or integer or np.random.RandomState
        random seed or instance of RandomState for sampling of data weights
    """

    assert np.issubdtype( labels.dtype, np.integer )
    assert eq_distr_fading >= 0
    assert eq_distr_fading <= 1
    assert (prior_sample_weights is None) or (prior_sample_weights.size == labels.size)
    assert (prior_sample_weights is None) or (np.issubdtype( prior_sample_weights.dtype, np.integer ) )

    if not isinstance( rands, np.random.RandomState ):
        rands = np.random.RandomState( rands )

    if eq_distr_fading > 0:

        # calculate label frequencies
        label_freqs = np.bincount( labels, weights=prior_sample_weights )
        label_freqs = label_freqs.astype( np.float32 ) / float( label_freqs.sum() )

        if not isinstance( rands, np.random.RandomState ):
            rands = np.random.RandomState( rands )
        
        data_weight_sampler = ItemWeightSampling( label_freqs, target_freq="uniform", target_fading=eq_distr_fading )
        data_weights = data_weight_sampler.sample_weights( labels, rand_state = rands )

        if prior_sample_weights is not None:
            assert (int(data_weights.max()) * int(prior_sample_weights.max())) <= np.iinfo( data_weights.dtype ).max
            data_weights *= prior_sample_weights
    
    elif prior_sample_weights is not None:
        data_weights = prior_sample_weights
    else:
        data_weights = np.ones( labels.size, dtype=np.int32 )

    return data_weights


#-----------------------------------------------------------------------------------------------------------


class ItemWeightSampling( object ):
    """

    Attributes
    ----------
    binomial_n : integer array [ nC ]
        Parameter of binomial distributions.
        binomial_n[i] is the "number of trials" paramter for class i
    binomial_p : list of float arrays - [ nC ]
        Parameter of binomial distributions.
        binomial_p[i] is the "success probability for each trial" paramter 
        for class i
    weight_offset : integer
        weight offset to each samples weight
    """

    binomial_p = None  # [ nC ]  nC: num classes 

    weight_offset = None # integer


    def __init__( self, source_freq,
                        target_freq="uniform",
                        target_fading=1.0,
                        min_weight = 1 ):
        """
        Parameters
        ----------
        source_freq : float32 array of size nC
        target_freq : float32 array of size nC or string "uniform"
        target_fading : float between 0 and 1
            final sample frequencies fade between source_freq (fading=0) and
            target_freq (fading=1)
        min_weight : integer >= 0
            smalles possibly sampled weight 
        """

        assert( min_weight == int(min_weight) )
        min_weight = int( min_weight )
        assert( min_weight >= 0 )

        source_freq = np.asarray( source_freq, dtype=np.float32 )
        assert( np.isclose( source_freq.sum(), 1. ) )

        if isinstance( target_freq, str ) and (target_freq.lower() == "uniform"):
            target_freq = 1. / float( source_freq.size )
            target_freq = np.array( target_freq, dtype=np.float32 ).repeat( source_freq.size )

        target_freq = np.asarray( target_freq, dtype=np.float32 )
        assert( target_freq.size == source_freq.size )
        
        # fading between source and target frequencies
        if target_fading < 1.0:
            target_freq *= target_fading
            target_freq += (1 - target_fading) * source_freq

        assert( np.isclose( target_freq.sum(), 1. ) )

        # normalzations
        Zc = (min_weight * source_freq) / target_freq

        # minimal required normalization
        Z = Zc.max()

        # determ source scaling
        qc = ((Z * target_freq) / source_freq) - min_weight  # [ nC ]
        assert( np.all(qc > -0.001) )
        qc = np.maximum( qc, 0 )
        assert( np.all( np.isclose( target_freq, (min_weight + qc) * source_freq / Z ) ) )

        # parameters of binomial distribution
        weight_offset = np.floor( qc )    # [ nC ]
        self.weight_offset = weight_offset.astype( np.int32 ) + int( min_weight )

        self.binomial_p = qc - weight_offset
        assert( np.all( self.binomial_p >= 0 ) )
        assert( np.all( self.binomial_p <= 1 ) )
        
    


    def sample_weights( self, label_indices, rand_state=None ):
        """
        Sampling weights from distribution

        Parameters
        ----------
        label_indices : integer array
        rand_state : RandomState instance or None 
        """

        label_indices = np.asarray( label_indices, dtype=np.int32 )

        if rand_state is None:
            rand_state = np.random.RandomState()
        
        # sample weights
        weights = rand_state.binomial( 1, self.binomial_p.take( label_indices ) )
        
        # add offsets
        weights = np.add( self.weight_offset.take( label_indices ), weights, dtype=np.int32 )

        return weights

