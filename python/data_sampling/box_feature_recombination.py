# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = [ "recombine_boxes" ]


import numpy as np
import typing as tp




def recombine_boxes( num_boxes : int, 
                     num_features : tp.Optional[int] = None,
                     rand_state : tp.Optional[np.random.RandomState] = None ) -> np.ndarray:
    """
    Recombines 'num_boxes' boxes for the composition of 'num_features' box features

    Parameters
    ----------
    num_boxes : int
        number of available features boxes
    num_features : int or None
        number of different box features (two boxes) to be formed via
        recombination
        if None, num_features is set to the maximum value: 
            'num_boxes * (num_boxes-1) / 2'
    rand_state : instance of np.mtrand.RandomState

    Returns
    -------
    box_combinations: numpy.int32 array of shape [ num_features x 2 ]
        Containes tuples of box indices combination.
        All indices are < num_boxes
    """
    num_boxes = int(num_boxes)
    assert num_boxes > 0
    max_num_features = (num_boxes * (num_boxes-1)) // 2

    if num_features is None:
        num_features = max_num_features
    else:
        num_features = int(num_features)
        assert num_features > 0
        assert num_features <= max_num_features
        num_features = min( num_features, max_num_features )

    if num_boxes >= (2*num_features):
        return np.stack( ( np.arange(0, 2*num_features, 2, dtype=np.int32), \
                           np.arange(1, 2*num_features, 2, dtype=np.int32) ), axis=1 )
    
    # build full indices
    bidx = np.arange( 0, num_boxes, dtype=np.int32 )
    idx1, idx2 = np.meshgrid( bidx, bidx )
    mask = idx1 > idx2
    idx1 = idx1[mask]
    idx2 = idx2[mask]

    assert idx1.size == max_num_features
    assert np.all( idx1 > idx2 )
    box_combinations = np.stack( (idx1,idx2), axis=1 ) # [ F x 2 ]
    
    if num_features < max_num_features:
        if rand_state is None:
            return box_combinations[:num_features,:].copy()
        else:
            selection = rand_state.permutation( np.arange(max_num_features) )[:num_features]
            return box_combinations[selection,:]
    
    return box_combinations


# -----------------------------------------------------------------------------------------------


