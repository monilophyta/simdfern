# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["ImageSequenceSubsetSampler"]


import numpy as np

from ..image import compute_integral_image, Rectangle

from .sequentiel_pixel_selection import SequentialPixelSelector



class ImageSequenceSubsetSampler( object ):

    class_frequency_image = None      # [ H x W x nlabels ]
    class_frequency_int_image = None  # [ H+1 x W+1 x nlabels ]


    def __init__(self, class_frequency_image ):
        assert( class_frequency_image.ndim == 3 )
        assert( np.issubdtype( class_frequency_image.dtype, np.integer ) == True )
        self.class_frequency_image = class_frequency_image

        # compute integrals of label frequencies -> short cut for sub window frequence calcutations
        self.compute_integral_image()


    ###########################################################################################

    @property
    def total_num_data(self):
        return int(self.class_frequency_int_image[-1,-1,:].sum(dtype=np.int64))

    @property
    def height(self):
        return self.class_frequency_image.shape[0]
    
    @property
    def width(self):
        return self.class_frequency_image.shape[1]
    
    @property
    def num_classes(self):
        return self.class_frequency_image.shape[2]
    

    @property
    def image_frame(self):
        rect = Rectangle( up = 0,
                          left = 0,
                          down = self.height,
                          right = self.width )
        
        assert( rect.height == self.height )
        assert( rect.width == self.width )
        return rect

    ###########################################################################################

    def create_frame_data_sampler(self, frame_section,
                                         sample_num,
                                         class_weights = None,
                                         weight_frequency_fading = 1,
                                         rand_state = None ):
        """
        Parameters
        ----------
        frame_section : Rectangle
            rectangular section of frame, where data are going to be sampled
        sample_num : integer
            number of data (pixels) to be sampled
        class_weights : floating array of size [ num_classes ]
            priority weights for data sampling for each class
        weight_frequency_fading : float between 0 and 1
            fading value between (normalized) class frequences within and frame section
            and weight in class_weights: 
                final_weight = class_weight * fading + frequency * (1-fading)
        rand_state : instance of numpy.random.RandomState
        """
        ## Check input
        assert( isinstance( frame_section, Rectangle ) == True )
        assert( frame_section.is_inside( self.image_frame ) )
        assert( (class_weights is None) or ( \
                      np.issubdtype( class_weights.dtype, np.floating ) and \
                    ( class_weights.size == self.num_classes ) and \
                      np.all( class_weights >= 0 ) and \
                      np.all( class_weights <= 1 ) and \
                      np.isclose( class_weights.sum(), 1.0 ) ) )
        assert( weight_frequency_fading <= 1.0 )
        assert( weight_frequency_fading >= 0. )

        if not isinstance( rand_state, np.random.RandomState ):
            rand_state = np.random.RandomState( rand_state )

        ## determ class frequencies within frame section
        class_counts = frame_section.compute_integral( self.class_frequency_int_image )  # [ nC ]
        num_data = class_counts.sum()
        
        class_freq = class_counts.astype( np.float32 ) / float( num_data )   # [ nC ]

        ## determ sampling frequencies
        if class_weights is None:
            sample_weights = class_freq   # [ nC ]
        else:
            sample_weights = (class_weights * weight_frequency_fading) + (class_freq * (1. - weight_frequency_fading) )   # [ nC ]

            if np.any(class_counts == 0):
                # Renormalization
                sample_weights[class_counts==0] = 0
                sample_weights *= 1. / float(sample_weights.sum())   # [ nC ]
        
        assert( np.isclose( sample_weights.sum(), 1.0 ) )

        ## absolute number of class samples to be drawn
        sample_counts = round_counts( sample_num, sample_weights )          # [ nC ]
        assert( sample_counts.sum() == sample_num )

        ## determ sample_counts on pixel level
        class_freq_subimage = frame_section.sub_image( self.class_frequency_image )  # [ sH x sW x nC ], sH < height, sw < width
        
        # arrays holding sample resuts
        pixel_pos_idx_array       = self.num_classes * [None]
        pixel_sample_counts_array = self.num_classes * [None]
        pixel_class_counts_array  = self.num_classes * [None]

        ## loop over all classes for sampling of pixel-wise frequencies 
        for class_idx, counts in enumerate( sample_counts ):
            if counts == 0:
                continue
            
            # indices of drawable pixel positions
            pixel_pos_idx = list(np.nonzero( class_freq_subimage[:,:,class_idx] ))  # [ 2 x [Pc] ]
            assert( pixel_pos_idx[0].size > 0 )

            # change reference of pixel_pos_idx from class_freq_subimage to self.class_frequency_image
            pixel_pos_idx[0] += frame_section.up      # [ Pc ]
            pixel_pos_idx[1] += frame_section.left    # [ Pc ]
            pixel_pos_idx = tuple( pixel_pos_idx )
            assert( np.all( self.class_frequency_image[ pixel_pos_idx + (class_idx,) ] > 0 ) )

            # frequencies of drawable pixel positions
            pixel_class_counts = self.class_frequency_image[ pixel_pos_idx + (class_idx,) ]   # [ Pc ]
            pixel_class_freq = pixel_class_counts.astype( np.float32 ) / float( class_counts[class_idx] )  # [ Pc ]
            assert( np.isclose( pixel_class_freq.sum(), 1.0 ) )

            # sample counts for relevant pixel positions
            pixel_sample_counts = rand_state.multinomial( counts, pixel_class_freq )  # [ Pc ]
            assert( pixel_sample_counts.sum() == counts )

            # storage
            pixel_pos_idx_array[ class_idx ]       = np.transpose( pixel_pos_idx )  # [ Pc x 2 ]
            pixel_sample_counts_array[ class_idx ] = pixel_sample_counts
            pixel_class_counts_array[ class_idx ]  = pixel_class_counts

        ######################################################################

        return SequentialPixelSelector( rand_state,
                                        pixel_pos_idx_array,
                                        pixel_sample_counts_array,
                                        pixel_class_counts_array )




    ###########################################################################################


    def compute_integral_image(self):
        max_freq = int( self.class_frequency_image.max() )
        max_integral = max_freq * self.height * self.width
        
        int_dtype = None
        for test_dtype in ( np.uint8, np.uint16, np.uint32, np.uint64 ):
            if max_integral <= np.iinfo( test_dtype ).max:
                int_dtype = test_dtype
                break
        
        assert( int_dtype is not None )

        self.class_frequency_int_image = compute_integral_image( self.class_frequency_image, int_dtype )


#############################################################################################################
### Utils


def round_counts( num, weights ):
    """  Rounding counts = num*weights to integer
    Ensures that counts.sum() == num
    """

    ## absolute number of class samples to be drawn
    counts = np.floor( float(num) * weights ).astype(np.int)   # [ nC ]
    rest_n = num - counts.sum()
    assert( rest_n >= 0 )
    
    # Random treatment for rounding errors
    if rest_n > 0:
        rest_p = ( float(num) * weights ) - counts.astype( np.float32 )
        assert( np.all( rest_p < 1. ) )

        add_one_idx = np.argsort( rest_p )[-rest_n:]

        counts[add_one_idx] += 1

    assert( counts.sum() == num )

    return counts



