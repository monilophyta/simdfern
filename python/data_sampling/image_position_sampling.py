# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


import numpy as np
from .image_sequence_subset_sampling import ImageSequenceSubsetSampler
from ..image import ImageCoords


class ImagePositionSampler( object ):

    label_images               = None
    data_position_sampler      = None
    weight_frequency_fading    = None


    @property
    def num_frames(self):
        return len(self.label_images)

    @property
    def image_height(self):
        return self.label_images.height
    
    @property
    def image_width(self):
        return self.label_images.width

    @property
    def num_labels(self):
        return self.label_images.num_labels
    
    @property
    def total_num_data(self):
        return self.data_position_sampler.total_num_data
    


    def __init__(self, label_images, \
                       weight_frequency_fading : float = 0 ):
        """
        Parameters
        ----------
        label_images : sequences of label images
        weight_frequency_fading : float between 0 and 1
            fading value between (normalized) class frequences within and frame section
            and weight in class_weights: 
                final_weight = class_weight * fading + frequency * (1-fading)
        """
        assert weight_frequency_fading >= 0
        assert weight_frequency_fading <= 1
        self.weight_frequency_fading = weight_frequency_fading
        
        assert label_images.num_labels > 1

        self.label_images = label_images

        # instance for sampling of data positions
        label_frequency_image = create_label_distribution( label_images, label_images.num_labels )
        self.data_position_sampler   = ImageSequenceSubsetSampler( label_frequency_image )


    def position_iterator( self, num_data : int, 
                                 receptive_field, 
                                 frame_weight_skew : float = 1,
                                 rand_state = None ):
        """
        Parameters
        ----------
        num_data : 
        receptive_field : 
        frame_weight_skew : float - positive (<0)
            == 1: every frame is qually treated during sampling
             < 1: later frames get higher sampling weights 
             > 1: earlier frames get higher sampling weights
        rand_state :
        """

        num_data = int( num_data )
        assert num_data > 0
        frame_weight_skew = float( frame_weight_skew )
        
        if not isinstance( rand_state, np.random.RandomState ):
            rand_state = np.random.RandomState( rand_state )
        

        # default is a uniform distribution over all classes
        uniform_class_distribution = np.array( [1. / self.num_labels], dtype=np.float32 ).repeat( self.num_labels )
        
        # determ section available for sampling
        frame_section = receptive_field.frame_section( self.image_height, self.image_width )

        # Pixel position sampling
        sequential_pixel_selector = \
            self.data_position_sampler.create_frame_data_sampler( frame_section,
                                                                  num_data,
                                                                  class_weights = uniform_class_distribution,  # default is natural class distribution
                                                                  weight_frequency_fading = self.weight_frequency_fading,
                                                                  rand_state = rand_state )

        # sampling the data
        data_idx = 0

        # permute image indices
        image_indices = rand_state.permutation( self.num_frames )

        # create sampling frames for every frame
        frame_weights = frame_weight_skew / np.linspace( 1, frame_weight_skew, self.num_frames, dtype=np.float32 )

        # doing the real work
        for img_idx, frame_weight in zip( image_indices, frame_weights ):
            
            # acquire label image
            label_img = self.label_images[img_idx]
            
            # sample image coordinates
            positions, labels, pos_weights = sequential_pixel_selector.sample_data( label_img, frame_weight = frame_weight )
            assert isinstance( positions, ImageCoords )
            assert labels.size == pos_weights.size
            assert positions.size == pos_weights.size
            
            num_sel_data = len(labels)  # = P

            if num_sel_data == 0:
                # nothing to be done
                continue
            
            data_idx += pos_weights.sum()
            
            yield (int(img_idx), positions, labels, pos_weights)
            
        
        # check if target size is met
        assert num_data == data_idx




# ---------------------------------------------------------------------------------------------
# --- Inner Moduls Utils -----------------------------------------------------------------------------------


def create_label_distribution( label_images, num_labels ):
    """
    Parameters
    ----------
    label_images: iterable sequence of label images
        All images require the same dimension
        Negative labels are considered as invalid
    """

    if len(label_images) < np.iinfo( np.uint8 ).max:
        dtype = np.uint8
    elif len(label_images) < np.iinfo( np.uint16 ).max:
        dtype = np.uint16
    elif len(label_images) < np.iinfo( np.uint32 ).max:
        dtype = np.uint32
    else:
        dtype = np.uint64

    label_shape = label_images[0].shape[:2]
    label_counts = np.zeros( label_shape + (num_labels,), dtype=dtype )

    rowIdx, colIdx = np.mgrid[:label_shape[0],:label_shape[1]]
    rowIdx = rowIdx.ravel()
    colIdx = colIdx.ravel()

    for img in label_images:
        img = img.ravel()
        valid_idx = np.flatnonzero( img >= 0 )

        label_counts[ rowIdx[valid_idx], colIdx[valid_idx], img[valid_idx] ] += 1

    return label_counts

