# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



__all__ = ["ReceptiveField"]

import collections
import numpy as np

from ..image import ImageBoxes, Rectangle



class ReceptiveField( object ):

    row_pos = None
    col_pos = None
    
    height = None
    width  = None


    def __init__(self, row_pos : int, col_pos : int, height : int, width : int ):

        assert( row_pos >= 0 )
        assert( row_pos < height )
        assert( col_pos >= 0 )
        assert( col_pos < width )

        self.row_pos = row_pos
        self.col_pos = col_pos

        self.height = height
        self.width = width


    def frame_section( self, image_height : int, image_width : int ) -> Rectangle:
        rect = Rectangle( up = self.row_pos,
                          left = self.col_pos,
                          down = image_height - self.height + self.row_pos + 1,
                          right = image_width - self.width + self.col_pos + 1 )

        assert( rect.height == (image_height - self.height + 1) )
        assert( rect.width == (image_width - self.width + 1) )
        return rect
    

    def draw_uniform_box_samples( self, Num : int, min_box_size : int = 1, max_box_size=None, rand_state=None ) -> ImageBoxes:
        if not isinstance( rand_state, np.random.RandomState ):
            rand_state = np.random.RandomState( rand_state )
        
        if max_box_size is None:
            max_box_size = (self.height, self.width)
        elif not isinstance( max_box_size, collections.Sequence ):
            max_box_size = (max_box_size, max_box_size)
        
        if not isinstance( min_box_size, collections.Sequence ):
            min_box_size = (min_box_size, min_box_size)
        
        box_rpos, box_rsize = self.draw_uniform_1d_box_samples( rand_state, Num, min_box_size[0], max_box_size[0], self.height )
        box_cpos, box_csize = self.draw_uniform_1d_box_samples( rand_state, Num, min_box_size[1], max_box_size[1], self.width )
        
        box_rpos -= self.row_pos
        box_cpos -= self.col_pos

        boxes = ImageBoxes( up = box_rpos, left=box_cpos,\
                            down=box_rpos+box_rsize, right=box_cpos+box_csize )

        return boxes
        
    
    @staticmethod
    def draw_uniform_1d_box_samples( rand_state, Num : int, min_size : int, max_size : int, field_size : int ):
        max_size = min( max_size, field_size )

        assert( min_size <= max_size )
        assert( min_size > 0 )

        # Box Size Sampling
        box_size = rand_state.randint( min_size, max_size+1, size=(Num,), dtype=np.int32 )

        # one above max box
        max_box_pos = field_size - box_size  # [ N ]
        assert( np.all( (max_box_pos + box_size) == field_size ) )
        
        #box_pos = rand_state.randint( 0, max_box_pos, size=(Num,), dtype=np.int32 )
        box_pos = np.minimum( np.floor( max_box_pos * rand_state.rand( Num ) ).astype(np.int32), max_box_pos - 1 )
        
        assert( np.all( box_pos >= 0 ) )
        assert( np.all( box_pos <= max_box_pos ) )
        assert( np.all( (box_pos + box_size) <= field_size ) )

        return (box_pos, box_size)

        
    


