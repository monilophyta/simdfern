# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


from abc import ABC, abstractmethod
from collections import namedtuple
from collections.abc import Sequence
import numpy as np


from ..data_sampling import ReceptiveFieldSampler, DataPositionSampler, SequentialPixelSelector
from ..ctypes_py import BoxFeatureEvaluator
from tree_training import TreeTrainer


BaseImageDataSamplingParams = namedtuple('BaseImageDataSamplingParams', \
                                         ["num_data", "num_features", \
                                          "max_rfield_height", "max_rfield_width", \
                                          "min_box_size", "max_box_size", \
                                          "channel_weights" ] )

class ImageDataSamplingParams( BaseImageDataSamplingParams ):
    
    #def __new__(typ, *args, **argv ):
    #    obj = BaseImageDataSamplingParams.__new__(typ, *args, **argv )
    #    return obj
        
    def __init__(self, *args, **argv ):
        super(ImageDataSamplingParams,self).__init__()
        # TODO: check parameter values
        
        if self.channel_weights is not None:
            assert np.isclose( self.channel_weights.sum(), 1 )



class TrainingResults( object ):

    rand_seed = None

    receptive_field = None

    training_data_positions = None
    training_data_image_idx = None
    training_data_labels    = None

    sampled_feature_boxes   = None

    data_weights            = None

    tree_layers         = None
    node_ratings        = None
    tree_node_indices   = None

    fern_feature_boxes  = None
    fern_feature_params = None

    fern_layers         = None
    fern_node_indices   = None



class AbstractImageClassifierTrainer( ABC ):

    image_data_sampling_params = None

    rand_state = None

    receptive_field_sampler = None
    data_position_sampler   = None

    #----------------------------------------------------------------------------

    @property
    def image_height(self):
        return self.data_position_sampler.height
    
    @property
    def image_width(self):
        return self.data_position_sampler.width

    @property
    def num_classes(self):
        return self.data_position_sampler.num_classes

    #----------------------------------------------------------------------------------------------------------------

    @abstractmethod
    def initialize_training_data_sampling( self, num_data, num_features ):
        pass

    @abstractmethod
    def add_training_data( self, labels, feature_matrix ):
        pass
    
    @abstractmethod
    def finalize_training_data_sampling( self ):
        pass
    
    @abstractmethod
    def get_training_feature_rows( self ):
        pass
    
    @abstractmethod
    def get_training_data_iterator(self):
        pass

    #----------------------------------------------------------------------------------------------------------------

    def __init__(self, image_data_sampling_params, num_image_channels, label_frequency_image, rand=None ):

        assert isinstance( image_data_sampling_params, ImageDataSamplingParams )
        assert isinstance( label_frequency_image, np.ndarray )
        assert np.issubdtype( label_frequency_image.dtype, np.integer )
        
        self.image_data_sampling_params = image_data_sampling_params

        if not isinstance( rand, np.random.RandomState ):
            rand = np.random.RandomState( rand )
        
        self.rand_state = rand

        # members relevant for data sampling
        self.receptive_field_sampler = ReceptiveFieldSampler( image_data_sampling_params.max_rfield_height, \
                                                              image_data_sampling_params.max_rfield_width )
        
        self.data_position_sampler = DataPositionSampler( label_frequency_image )


    def run_training(self):
        
        res = TrainingResults()
        res.rand_seed = self.rand_state.get_state()

        ## Sampling of Training data
        res.receptive_field = self.receptive_field_sampler.draw_uniform_sample( self.rand_state )

        tree_feature_evaluators = self.sample_training_features( receptive_field )
        res.sampled_feature_boxes = [ ( x.box_features.First, \
                                        x.box_features.Second, \
                                        x.box_features.Lambda )   for x in tree_feature_evaluators ]
        
        sequential_pixel_selector = self.create_sequential_pixel_selector( receptive_field )

        # extract tree training data
        res.training_data_positions, \
        res.training_data_image_idx, \
        res.training_data_labels     = self.generate_training_data( tree_feature_evaluators, sequential_pixel_selector )
        assert np.issubdtype( res.training_data_labels.dtype, np.integer )
        del sequential_pixel_selector

        ## Training a decission tree
        # Instantiate Tree Trainer
        tree_trainer = TreeTrainer( res.training_data_labels.astype(np.int32), self.get_training_feature_rows() )
        
        # Run the tree training
        res.tree_layers,
        res.node_ratings,
        res.tree_node_indices,
        res.data_weights       = tree_trainer.train_tree( num_tree_layers, eq_distr_fading, rands=self.rand_state )
        del tree_trainer

        ## Training a Fern

        # select fern feature evaluators
        res.fern_feature_indices = np.unique( np.concatenate( [ l.feature_idx for l in tree_layers ], axis = 0 ) )
        fern_feature_evaluators = self.select_fern_features( tree_feature_evaluators, res.fern_feature_indices )
        res.fern_feature_params = np.concatenate( [ x.box_features.Lambda for x in fern_feature_evaluators ], axis=0 )

        # Extract fern features
        fern_training_feature_rows = self.extract_training_data( fern_feature_evaluators, res.training_data_positions, res.training_data_image_idx )  # F x [ N ]
        
        # Instantiate the fern trainer
        fern_trainer = FernTrainer( res.training_data_labels, fern_training_feature_rows, res.data_weights )

        # Run The Fern Training
        res.fern_layers, res.fern_node_indices = fern_trainer.train_fern( num_fern_layers )

        #fern_layers, fern_node_indices = fern_trainer.optimize_fern( fern_layers, fern_node_indices )


    #----------------------------------------------------------------------------------------------------------------
    # Sampling Training Data


    def sample_training_features( self, receptive_field )
        
        # sample receptive field
        rfield = self.receptive_field_sampler.draw_uniform_sample( self.rand_state )

        # sample box features
        boxes = rfield.draw_uniform_box_samples( 2 * self.image_data_sampling_params.num_features, \
                                                 self.image_data_sampling_params.min_box_size, \
                                                 self.image_data_sampling_params.max_box_size, \
                                                 self.rand_state )
        box1 = boxes.select( slice(0, self.image_data_sampling_params.num_features) )
        box2 = boxes.select( slice(self.image_data_sampling_params.num_features, None) )
        
        # sample channels
        if self.num_image_channels == 1:
            ## Simple case with only one image channel

            # one box evaluator for all features
            feature_evaluators = (BoxFeatureEvaluator( box1, box2 ), )
        else:
            ## Complex case with multiple image channels

            if self.image_data_sampling_params.channel_weights is None:
                channel_weights = np.ones( self.num_image_channels, dtype=np.float32 ) / self.num_image_channels
            else:
                channel_weights = self.image_data_sampling_params.channel_weights
                assert channel_weights.size == self.num_image_channels
            
            # sample channel belonging for all feature boxe evaluators
            channel_counts = self.rand_state.multinomial( self.image_data_sampling_params.num_features, channel_weights )
            
            channel_end_idx = np.add.accumulate( channel_counts )
            assert( channel_end_idx[-1] == self.image_data_sampling_params.num_features )
            channel_start_idx = np.concatenate( ([0], channel_end_idx[:-1]), axis=0 )
            
            channel_slices = [ slice(s,e) for (s,e) in zip(channel_start_idx, channel_end_idx) ]
            feature_evaluators = [ BoxFeatureEvaluator( box1.select(s), box2.select(s) ) for s in channel_slices ]

        return feature_evaluators


    def create_sequential_pixel_selector( self, receptive_field ):

        # Create pixel selectors
        frame_section = rfield.frame_section( self.image_height, self.image_width )

        # Pixel position sampling
        equal_class_distribution = np.array( [1. / self.num_classes], dtype=np.float32 ).repeat( self.num_classes )
        sequential_pixel_selector = self.data_position_sampler.create_frame_data_sampler( frame_section,
                                                                                          self.image_data_sampling_params.num_data,
                                                                                          class_weights = equal_class_distribution,  # default is natural class distribution
                                                                                          weight_frequency_fading = self.image_data_sampling_params.weight_frequency_fading,
                                                                                          rand_state=self.rand_state )
        return sequential_pixel_selector


    def generate_training_data( self, feature_evaluators, sequential_pixel_selector ):
        
        assert isinstance( feature_evaluators, Sequence )
        assert len( feature_evaluators ) == self.num_image_channels
        assert isinstance( sequential_pixel_selector, SequentialPixelSelector )

        training_data_iter = self.get_training_data_iterator()

        self.initialize_training_data_sampling( self.image_data_sampling_params.num_data,
                                                self.image_data_sampling_params.num_features )

        training_data_positions = list()
        training_data_image_idx = list()
        training_data_labels = list()

        for image_idx, label_img, img_channels in training_data_iter:
            # image_idx: int
            # label_img: [ H x W ]
            # img_channels: D x [ H x W ]
            
            # sample image coordinates
            positions, labels = sequential_pixel_selector.sample_data( label_img )

            if len(labels) > 0:
                
                feature_values = [ fe.process( channel, positions ) for (fe,channel) in zip( feature_evaluators, img_channels ) ] # C x [ N x Fc ]
                feature_values = np.concatenate( feature_values, axis = 1 )   # [ N x F ]
                assert feature_values.shape[0] == positions.size
                assert feature_values.shape[1] == self.image_data_sampling_params.num_features

                self.add_training_data( labels, feature_val.transpose() )

                training_data_positions.append( positions )
                training_data_labels.append( labels )
                training_data_image_idx( image_idx )

        self.finalize_training_data_sampling( self.image_data_sampling_params.num_data, 
                                              self.image_data_sampling_params.num_features )
        
        training_data_labels = np.concatenate( training_data_labels, axis = 0)
        training_data_image_idx = np.array( training_data_image_idx, dtype=np.int32 )

        return (training_data_positions, training_data_image_idx, training_data_labels )



