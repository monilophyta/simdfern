# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["PowellsMethod"]


import typing as tp
import numpy as np
from scipy.stats import ortho_group


class PowellsMethod(object):

    xpos              : np.ndarray  # [ D ]
    direction_set     : np.ndarray  # [ N x D ]
    direction_weights : np.ndarray  # [ N ]

    rand_state        : np.random.RandomState

    re_orthogonalization_counter : int


    @property
    def num_dirs(self) -> int:
        return self.direction_weights.size


    def __init__(self, xinit : np.ndarray, 
                  rand_state : tp.Optional[tp.Union[int,np.random.RandomState]] = None,
           rand_dir_set_init : bool = False ):
        xinit = np.asarray( xinit )
        assert np.issubdtype( xinit.dtype, np.floating )

        if isinstance( rand_state, np.random.RandomState ):
            self.rand_state = rand_state
        else:
            self.rand_state = np.random.RandomState(seed=rand_state)

        self.xpos = xinit

        # sample random orthogonal matrix
        if bool(rand_dir_set_init):
            self.direction_set = ortho_group.rvs( dim=xinit.size, random_state=self.rand_state ).astype( dtype=xinit.dtype )
        else:
            self.direction_set = np.eye( xinit.size, dtype=xinit.dtype )
        self.re_orthogonalization_counter = xinit.size
        self.direction_weights = np.zeros( xinit.size, dtype=xinit.dtype )




    def get_current_pos(self, idx : int) -> np.ndarray:
        return self.xpos + np.dot( self.direction_weights[:idx], self.direction_set[:idx,:] )

    def reset_xpos(self, xnew : np.ndarray, reothogonalize : bool = False ):
        assert self.xpos.size == xnew.size
        self.xpos = xnew
        self.direction_weights.fill(0)

        if bool(reothogonalize) and (self.re_orthogonalization_counter < self.num_dirs):
            self.reorthogonalize()


    def get_search_vector(self, idx : int) -> np.ndarray:
        return self.direction_set[idx,:]


    def update_search_weight(self, idx : int, weight : float ):
        self.direction_weights[idx] = weight


    def update_direction_set(self) -> np.ndarray:
        # summerize all search directions
        n_direction = np.dot( self.direction_weights, self.direction_set )
        assert n_direction.size == self.xpos.size

        # update central position
        n_xpos = self.xpos + n_direction
        assert np.all( n_xpos == self.get_current_pos(self.num_dirs) )
        self.xpos = n_xpos

        # normalize search direction
        n_direction *= 1. / max( np.linalg.norm( n_direction ), 1e-16 )

        # get index of most successful search direction
        n_idx = np.argmax( np.absolute( self.direction_weights ) )

        # remove most successful search direction
        self.direction_set[n_idx:-1,:] = self.direction_set[(n_idx+1):,:]

        # shuffle the first N-1 directions
        shuffle_idx = self.rand_state.permutation(self.num_dirs-1)
        self.direction_set[:-1,:] = self.direction_set[shuffle_idx,:]

        # add new search direction at the end
        self.direction_set[-1,:] = n_direction

        # set weights of all search directions to zero
        self.direction_weights.fill(0)

        return n_direction


    def reorthogonalize(self):
        """re-orthogonalize the search directions set by using a SVD
        """
        u, s, vh = np.linalg.svd(self.direction_set, full_matrices=True)
        self.direction_set = np.dot(u, vh)
        self.re_orthogonalization_counter = self.num_dirs

    def optimization_step(self, lsearch_func : tp.Callable[[np.ndarray,np.ndarray],tp.Tuple[float,tp.Any]]) -> tp.Tuple[np.ndarray,tp.Any]:
        
        if self.re_orthogonalization_counter == 0:
            # reorthogonalize after D iterations
            self.reorthogonalize()
        self.re_orthogonalization_counter -= 1

        for idx in range(self.num_dirs):
            n_weight,_ = lsearch_func( self.get_current_pos(idx), self.direction_set[idx,:] )

            # update weights
            self.direction_weights[idx] = n_weight
        
        # update new direction set
        n_direction = self.update_direction_set()
        
        # further search along optimal direction
        n_weight, res = lsearch_func( self.xpos, n_direction )

        # update search direction
        n_direction *= n_weight
        self.xpos = self.xpos + n_direction

        # return result
        return (n_direction, res)


