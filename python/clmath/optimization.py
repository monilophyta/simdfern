# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["GradientConjugator", "CG_METHOD"]


from enum import Enum
#import functools

import numpy as np
import typing as tp



class CG_METHOD(Enum):
    NONE             = 'None'
    FLETCHER_REEVES  = "Fletcher–Reeves"
    POLAK_RIBIERE    = "Polak–Ribière"
    HESTENES_STIEFEL = "Hestenes-Stiefel"
    DAI_YUAN         = "Dai–Yuan"


def fletcher_reeves( x : np.ndarray, xm1 : np.ndarray, cg_m1 : np.ndarray, eps : float = 1e-8 ) -> float:
    return float( np.square( x ).sum() / max( eps, np.square( xm1 ).sum() ) )

def polak_ribiere( x : np.ndarray, xm1 : np.ndarray, cg_m1 : np.ndarray, eps : float = 1e-8 ) -> float:
    return float( np.dot( x, x - xm1 ) / max( eps, np.square( xm1 ).sum() ) )

def hestenes_stiefel( x : np.ndarray, xm1 : np.ndarray, cg_m1 : np.ndarray, eps : float = 1e-8 ) -> float:
    diff = x - xm1
    divisor = np.dot( cg_m1, diff )
    divisor = np.sign( divisor ) * max( eps, abs(divisor) )
    return float( np.dot( x, diff ) / divisor )

def dai_yuan( x : np.ndarray, xm1 : np.ndarray, cg_m1 : np.ndarray, eps : float = 1e-8 ) -> float:
    diff = x - xm1
    divisor = np.dot( cg_m1, diff )
    divisor = np.sign( divisor ) * max( eps, abs(divisor) )
    return float( np.square( x ).sum() / divisor )


CG_METHOD_DICT = { (CG_METHOD.FLETCHER_REEVES,False) : fletcher_reeves,
                   (CG_METHOD.FLETCHER_REEVES,True)  : lambda *args, **argv: max(0., fletcher_reeves(*args,**argv)),

                     (CG_METHOD.POLAK_RIBIERE,False) : polak_ribiere,
                     (CG_METHOD.POLAK_RIBIERE,True)  : lambda *args, **argv: max(0., polak_ribiere(*args,**argv)),

                  (CG_METHOD.HESTENES_STIEFEL,False) : hestenes_stiefel,
                  (CG_METHOD.HESTENES_STIEFEL,True)  : lambda *args, **argv: max(0., hestenes_stiefel(*args,**argv)),

                          (CG_METHOD.DAI_YUAN,False) : dai_yuan,
                          (CG_METHOD.DAI_YUAN,True)  : lambda *args, **argv: max(0., dai_yuan(*args,**argv)) }



class GradientConjugator(object):
    """
    Class for gradient conjugation

    mathematical background from here:
    https://en.wikipedia.org/w/index.php?title=Nonlinear_conjugate_gradient_method&oldid=992680925
    """

    last_gradient           : tp.Optional[np.ndarray]
    last_cg_direction       : np.ndarray

    cg_method_call          : tp.Callable[[np.ndarray,np.ndarray,np.ndarray,tp.Optional[float]],float]
    orthogonality_threshold : float


    def __init__(self,       method : CG_METHOD = CG_METHOD.POLAK_RIBIERE,
        restart_on_direction_change : bool = True,
            orthogonality_threshold : float = 0.1 ):
        """
        Parameters
        ----------
        method : instance of CG_METHOD
        restart_on_direction_change : bool
            if True CG restarts (beta:=0) when beta becomes negative
        orthogonality_threshold : float (default 0.1)
            When non-orthogonality between succeeding gradient exceeds this threshold CG
            restarts (beta:=0)
        """
        
        self.cg_method_call = CG_METHOD_DICT[ (CG_METHOD(method),bool(restart_on_direction_change)) ]
        self.last_gradient = None
        self.orthogonality_threshold = orthogonality_threshold
    

    def next_conjugate_gradient(self, grd : np.ndarray):
        if self.last_gradient is None:
            self.last_gradient = grd
            self.last_cg_direction = grd
            return grd
        
        orthogonality_value = abs( np.dot( grd, self.last_gradient ) ) / np.square(grd).sum()

        if orthogonality_value < self.orthogonality_threshold:
            beta = self.cg_method_call( grd, self.last_gradient, self.last_cg_direction )
        else:
            # restart
            beta = 0.
        
        n_cg_direction = grd + beta * self.last_cg_direction

        self.last_gradient = grd
        self.last_cg_direction = n_cg_direction
        return n_cg_direction
    

    def __call__(self, grd : np.ndarray):
        return self.next_conjugate_gradient(grd)







