# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["normalize_entropy", "normalize_gini", "max_unormed_entropy", "max_unormed_gini"]


import math
import numpy as np



def normalize_entropy( unormed_entropy, num_data, num_labels=None ):
    normed_entropy = unormed_entropy * (1. / num_data)
    assert np.all( normed_entropy >= 0 )

    if num_labels is not None:
        # assertainment check
        assert np.all( normed_entropy <= (np.log(num_labels) * float(num_labels)) )
    
    return normed_entropy


def normalize_gini( unormed_gini, num_data, num_labels ):
    assert np.all( unormed_gini >= 0 )
    
    max_gini = num_data * (num_labels-1)
    assert np.all( unormed_gini <= max_gini )

    normed_gini = unormed_gini * (1. / max_gini)  #  Why minus 1??

    return normed_gini


def max_unormed_entropy( num_data, num_labels ) -> float:
    return (float( num_data ) * math.log( num_labels ))


def max_unormed_gini( num_data, num_labels ) -> int:
    max_abs_optimum = num_data * (num_labels-1)
    assert normalize_gini( max_abs_optimum, num_data, num_labels ) == 1.0
    return max_abs_optimum


