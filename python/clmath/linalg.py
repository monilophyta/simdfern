# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["robust_eigh", "qr_eig",
           "GLOBAL_COUNTER_b_singl" ]


#import warnings
import logging
import atexit
import numpy as np
import scipy.linalg


# ------------------------------------------------------------------------------------------
# global counting

GLOBAL_COUNTER_ab_posdef= 0
GLOBAL_COUNTER_b_singl = 0


# ------------------------------------------------------------------------------------------
# logging

# create logger
logger = logging.getLogger('clmath.linalg')

if __debug__:
    logger.setLevel(logging.INFO)
else:
    logger.setLevel(logging.WARN)

def dump_count_stats():
    logger.info( "GLOBAL_COUNTER_ab_posdef=%d", GLOBAL_COUNTER_ab_posdef)
    logger.info( "GLOBAL_COUNTER_b_singl=%d", GLOBAL_COUNTER_b_singl )

atexit.register(dump_count_stats)


# --------------------------------------------------------------------------------------------
# real stuff


def qr_eig( A, B ):
    # inversion with QR decomposition
    Q,R = scipy.linalg.qr(B)
    Y, residues, rank, Sval = scipy.linalg.lstsq( R, Q.transpose() )
    X = np.dot( A, Y )

    eigenvalue, eigenvector = scipy.linalg.eig( X )
    eigenvalue = np.real( eigenvalue )
    eigenvector = np.real( eigenvector )

    return (eigenvalue, eigenvector)



def robust_eigh( A, B, use_qr = False ):
    """
    Solves A v = \lambda B v 
    for symmetric - positive definite/semidefinite matrices A and B

    Problem is equivalent to: 
        X v = \lambda v
    with
        X = B^-1 A
    """

    # for debugging
    global GLOBAL_COUNTER_b_singl
    global GLOBAL_COUNTER_ab_posdef


    try:
        eigenvalue, eigenvector = scipy.linalg.eigh( A, b=B, type=1, turbo=False, eigvals_only=False )
        GLOBAL_COUNTER_ab_posdef+= 1
    except np.linalg.LinAlgError as err:
        #warnings.warn( repr( err ) )
        GLOBAL_COUNTER_b_singl += 1
        use_qr = True

        #np.savez( "scipy_eigh_failed.npz", A = A, B = B )

    
    if use_qr:
        eigenvalue, eigenvector = qr_eig( A, B )

    return (eigenvalue, eigenvector)

