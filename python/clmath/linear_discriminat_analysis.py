# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



__all__ = [ "MultiNodeClassStatistics", "js_divergence", "kl_divergence" ]


import typing as tp
from collections import Sequence
#import warnings
import numpy as np
#import scipy.linalg
from .linalg import robust_eigh
from ..ctypes_py import calculate_multi_node_lda_covars, FeatureRows, create_feature_rows



class MultiNodeClassStatistics(object):

    unseparated_weight_sum : int
    intra_cov              : np.ndarray
    inter_cov              : np.ndarray

    @property
    def num_features(self) -> int:
        return self.inter_cov.shape[0]

    # for debugging
    py_inter_cov : tp.Optional[np.ndarray]
    py_intra_cov : tp.Optional[np.ndarray]    



    def __init__(self, nodeIdx, classIdx, feature_rows, weights, do_verify=False ):
        """
        Parameters
        ----------
        nodeIdx : int32 array [ N ]
            Node index of sample n,
            >=0 for active nodes, <0 for finalized nodes
        classIdx : int32 array [ N ]
        feature_rows : list of float matrices: nf * [ array(N) ]
        weights : int32 array [ N ]
        do_verify : bool (default False)
            if true, run a verification with pure python based covar estimations
        Returns
        -------
        """

        if not isinstance( feature_rows, FeatureRows ):
            n_feature_rows = create_feature_rows()
            n_feature_rows.extend( feature_rows )
            feature_rows = n_feature_rows
        
        assert isinstance( feature_rows, FeatureRows )
        assert feature_rows.num_data == nodeIdx.size
        assert feature_rows.num_data == classIdx.size
        assert feature_rows.num_data == weights.size

        used_weight_sum, intra_cov, inter_cov = calculate_multi_node_lda_covars( nodeIdx, classIdx, weights, feature_rows )
        assert used_weight_sum >= 0
        assert used_weight_sum <= weights.sum(dtype=np.int64)
        assert np.isfinite( inter_cov ).all()
        assert np.isfinite( intra_cov ).all()
        
        self.unseparated_weight_sum = used_weight_sum
        self.inter_cov = inter_cov
        self.intra_cov = intra_cov

        self.py_intra_cov = None
        self.py_inter_cov = None

        if do_verify:
            py_used_weight_sum, self.py_intra_cov, self.py_inter_cov = self.pythonic_covar_estimation( nodeIdx, classIdx, feature_rows.feature_rows, weights  )
            assert py_used_weight_sum == self.unseparated_weight_sum
            # assert js_divergence( self.intra_cov, py_intra_cov ) < 1e-4
            # assert js_divergence( self.inter_cov, py_inter_cov ) < 1e-3
    

    def has_unseparated_data(self):
        return (self.unseparated_weight_sum > 0)


    def run_lda(self):
        inter_cov = self.inter_cov
        intra_cov = self.intra_cov

        # remove zeros on diagonal
        intra_variances = np.diag( intra_cov )
        inter_variances = np.diag( inter_cov )
        valid_mask = np.logical_and( intra_variances > 0, inter_variances > 0 )
        shrinked_matrices = not valid_mask.all()
        if shrinked_matrices:
            inter_cov = inter_cov.compress( valid_mask, axis=0 ).compress( valid_mask, axis=1 )
            intra_cov = intra_cov.compress( valid_mask, axis=0 ).compress( valid_mask, axis=1 )

        # generalized eigenvalue decomposition
        eigenvalue, eigenvector = robust_eigh( inter_cov, intra_cov )
        #eigenvalue, eigenvector = scipy_eigh( inter_cov, b=intra_cov, type=1, turbo=False, eigvals_only=False )
        assert np.all( np.isfinite( eigenvector ) ) 

        # scipy.linalg.eigh does not normalize eigenvvalues
        eignorm = np.reciprocal( np.linalg.norm( eigenvector, axis=0 ) )
        eigenvector *= eignorm[np.newaxis,:]

        # sort eigenvalue according to size
        sorted_eigenvalue_idx = np.argsort( eigenvalue )[::-1]
        eigenvalue = eigenvalue[sorted_eigenvalue_idx]
        eigenvector = eigenvector[:,sorted_eigenvalue_idx]

        #diff_cov = self.inter_cov - self.intra_cov
        #eigenvalue, eigenvector = np.linalg.eigh( diff_cov )


        # blow up shrinked matrices
        if shrinked_matrices:
            n_shape = (self.inter_cov.shape[0], eigenvalue.size)
            n_eigenvector = np.zeros( n_shape, dtype=eigenvector.dtype )
            n_eigenvector[valid_mask,:] = eigenvector
            eigenvector = n_eigenvector

        return (eigenvalue, eigenvector)


    # -------------------------------------------------------------------------------------------------
    # pure python based covar estimations - to following is UNUSED CODE


    def pythonic_covar_estimation(self, nodeIdx, classIdx, feature_rows, weights ):
        if __debug__:
            valid_rows = [ np.all( np.isfinite(f) ) for f in feature_rows ]
            assert np.all( valid_rows ), "non-finite values are not supported by lda (yet)"
        
        nodeidx_set, classidx_set = self.generate_index_sets( nodeIdx, classIdx )

        #num_classes = classidx_set.size

        # covariance matrices
        inter_cov, intra_cov, used_weight_sum = self.calculate_sufficient_statistics( nodeidx_set, nodeIdx, \
                                                                                      classidx_set, classIdx, \
                                                                                      feature_rows, weights )
        assert used_weight_sum >= 0
        assert used_weight_sum <= weights.sum(dtype=np.int64)
        assert np.isfinite( inter_cov ).all()
        assert np.isfinite( intra_cov ).all()
        assert True == np.all( np.triu( intra_cov, k=1 ).ravel() == 0 )
        
        if used_weight_sum > 0:
            cov_norm = 1. / float( used_weight_sum )
            inter_cov = cov_norm * inter_cov
            intra_cov = cov_norm * (intra_cov + np.tril( intra_cov, k=-1 ).transpose())

            assert np.all( (intra_cov == intra_cov.transpose()).ravel() )
        else:
            inter_cov = np.zeros( (self.num_features, self.num_features), dtype=np.float32 )
            intra_cov = np.zeros( (self.num_features, self.num_features), dtype=np.float32 )

        return (used_weight_sum, intra_cov, inter_cov)



    @staticmethod
    def generate_index_sets( nodeIdx, classIdx ):
        # build node index set and counts
        nodeidx_set, nodeidx_counts = np.unique( nodeIdx, return_counts=True )  #  [ L ]
        
        # filter for valid indices
        node_mask = nodeidx_set >= 0
        nodeidx_set = nodeidx_set[node_mask] # discard invalid node indices
        nodeidx_counts = nodeidx_counts[node_mask]
        
        # sort node index list according to size (improving numerical behaviour)
        nodeidx_set = nodeidx_set[ np.argsort( nodeidx_counts ) ]
        
        classidx_set = np.unique( classIdx ) # [ C ]

        return (nodeidx_set, classidx_set)
    


    def calculate_sufficient_statistics( self, nodeidx_set, nodeIdx, classidx_set, classIdx, feature_rows, weights ):
        
        intra_cov = np.zeros( (self.num_features, self.num_features), dtype=np.float64 )
        inter_cov = np.zeros( (self.num_features, self.num_features), dtype=np.float64 )
        
        # initialize number of uesed weighted data
        used_weight_sum = 0

        for nidx in nodeidx_set:
            node_mask = nodeIdx == nidx   # [ N ]

            if not np.any( node_mask ):
                continue
            
            # calculate global mean
            node_mean, node_weights = np.average( np.stack(feature_rows)[:,node_mask], \
                                                    axis=1, \
                                                    weights=weights[node_mask], \
                                                    returned=True )
            #print( node_weights )
            node_weights = int(node_weights.mean() + 0.5)

            # data indices for each class in this node
            data_indices = [ np.flatnonzero( np.logical_and( node_mask, classIdx == cidx ) ) for cidx in classidx_set ]  # [ nC ]

            # filter for valid data indices
            data_indices = [ data_idx for data_idx in data_indices if data_idx.size > 0 ]

            # check if at least two classes are present in this node
            if len(data_indices) < 2:
                continue  # nothing to be done for less than two valid classes

            # initialize Size of Node
            node_size = 0

            # Container for node specific inter class covariance
            node_inter_cov = np.zeros_like( inter_cov )

            # Container for node and class specific means
            #node_class_means = np.empty( (len(data_indices), self.num_features), dtype=np.float64 )  # [ nC x nF ]
            #node_class_means.fill( np.nan )


            for valid_idx, data_idx in enumerate( data_indices ):
                assert data_idx.size > 0

                data_weights = weights[data_idx] # [ N_cl ]
                node_class_size = data_weights.sum( dtype=np.int64 )
                node_size += node_class_size

                #node_class_means[valid_idx,:] = self.calc_intra_class_covariance( feature_rows, data_idx, data_weights, intra_cov )
                node_class_mean = self.calc_intra_class_covariance( feature_rows, data_idx, data_weights, intra_cov )
                mean_diff = node_mean - node_class_mean
                node_inter_cov += node_class_size * np.multiply.outer( mean_diff, mean_diff )

                # loop over classes to build up node_inter_cov
                #for other_idx in range(valid_idx):
                #    assert valid_idx > other_idx 
                #    assert np.isfinite( node_class_means[other_idx,:] ).all()
                #    assert np.isfinite( node_class_means[valid_idx,:] ).all()
                #    mean_diff = node_class_means[valid_idx,:] - node_class_means[other_idx,:]
                # 
                #    # update node specific inter class covariance
                #    node_inter_cov += np.multiply.outer( mean_diff, mean_diff )
            
            # add node specific inter class covariance to global inter class covariance
            #inter_cov += node_size * node_inter_cov
            inter_cov += node_inter_cov
            used_weight_sum += node_size

            assert node_weights == node_size

        return (inter_cov, intra_cov, used_weight_sum)



    def calc_intra_class_covariance( self, feature_rows, data_idx, data_weights, intra_cov ):

        node_class_mean = np.empty( self.num_features, dtype=np.float64 )

        if __debug__:
            node_class_mean.fill(np.nan)

        for x_idx in range(self.num_features):
            Xf = feature_rows[x_idx][data_idx]   # [ N_cl ]
            
            # calculate mean
            node_class_mean[ x_idx ] = np.average( Xf, weights=data_weights )

            # substract class mean
            Xf = Xf - node_class_mean[ x_idx ]   # [ N_cl ]

            # calculate intra variance on diagonal
            intra_cov[x_idx,x_idx] += np.dot( np.square( Xf ), data_weights )

            for y_idx in range( 0, x_idx ):
                # Feature with substracted mean
                Yf = feature_rows[ y_idx ][data_idx] - node_class_mean[ y_idx ]   # [ N_cl ]

                # Update correlation matrix
                corr_xy = Xf * Yf
                intra_cov[x_idx,y_idx] += np.dot(corr_xy, data_weights)

        assert np.all( np.isfinite( node_class_mean ) )

        return node_class_mean


# ---------------------------------------------------------------------------------------------------

# tools debugging
kl_divergence = lambda cov1, cov2: 0.5 * ( np.trace( np.linalg.solve( cov2, cov1 ) ) \
                                           - cov1.shape[0] \
                                           + np.linalg.slogdet( cov2 )[1] - np.linalg.slogdet( cov1 )[1] )
js_divergence = lambda cov1, cov2: 0.5 * ( kl_divergence( cov1, cov2 ) + kl_divergence( cov2, cov1 ) )
