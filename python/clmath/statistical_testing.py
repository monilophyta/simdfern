# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["Crosstab"]


import numpy as np
from scipy.stats import hypergeom, chi2


safe_log = lambda x, dtype=np.float32: np.log( np.maximum( x, np.finfo( dtype ).tiny, dtype=dtype ) )



class Crosstab( object ):


    num_chosen   = None   # number drawn selected
    num_data     = None
    num_selected = None
    N            = None   # number drawn samples


    def __init__( self, num_chosen, N, num_selected, num_data ):
        assert num_chosen.size   == N.size
        assert num_selected.size == N.size
        
        self.num_chosen   = num_chosen.astype(np.int64)
        self.num_data     = num_data
        self.num_selected = num_selected.astype(np.int64)
        self.N            = N.astype(np.int64)
    

    @classmethod
    def from_histograms( cls, class_freqs, histograms, num_data=None ):
        """
        Parameters
        ----------
        class_freqs : int32 array [ num_classes ]
            absolute number of samples each class 
        histograms : int32 matrix [ num_nodes x num_classes ]
            coocurence matrix of classes and leaf indices
        num_data : int or None
            if given shall be equal class_freqs.sum()
        """

        assert class_freqs.size == histograms.shape[1]

        # total number of data
        if num_data is None:
            num_data = class_freqs.sum()
        else:
            assert num_data == class_freqs.sum()

        num_nodes = histograms.shape[0]
        maxIdx = histograms.argmax(axis=1)   # [ num_nodes ]

        # grabbing local chosen set parameters
        N = histograms.sum(axis=1)   # [ num_nodes ]  - number of drawn samples
        num_chosen = histograms[ (np.arange(num_nodes, dtype=np.int32), maxIdx) ]  # [ num_nodes ]

        # grabbing global set parameters
        num_selected = class_freqs[ maxIdx ]  # total number of Type I objects (n)
        assert np.all( num_selected <= num_data )

        assert np.all( num_chosen <= num_selected )
        assert np.all( num_chosen <= N )

        return cls( num_chosen, N, num_selected, num_data )


    def hypergeom_type_one_error_prob( self ):
        """
        Performs fishers exact test on the frequency distribution of the selected elements.
        The result show significanc if the frequencies are unusual low or high

        Returns
        -------
        right side of (hypergeometric) cumulative distribution 
        function for each (selected) node
            the smaller this value, the better (more significant)
        """
        
        # left side excluding the obervation
        k = np.maximum( self.num_chosen - 1, 0 )

        # getting left side sum up until (exclusive) num_chosen
        hypergeom_logcdf = np.vectorize( hypergeom.logcdf )
        left_logcdf = hypergeom_logcdf( k, self.num_data, self.num_selected, self.N )

        # converting to right cdf
        right_cdf = -np.expm1( left_logcdf, dtype=np.float64 )
        right_cdf[self.num_chosen==0] = 1.
        
        return right_cdf
    

    def chi2_type_one_error_prob( self ):
        """
        Performs a Chi² test on the frequency distribution of the selected elements.
        The result show significanc if the frequencies are unusual low or high

        Returns
        -------
        right side of (chi²) cumulative distribution 
        function for each (selected) node
            the smaller this value, the better (more significant)
        """
        
        #               | this node  |     not this node                | 
        # --------------|------------|----------------------------------|
        #     selected: |   k        |  num_selected - k                |
        # --------------|-------    -|----------------------------------|
        # not-selected: |  N-k       |  num_data - num_selected - N + k |

        #  https://de.wikipedia.org/wiki/Chi-Quadrat-Test#Vierfeldertest

        num_degrees_of_freedom = 1

        # left side excluding the obervation
        k = np.maximum( self.num_chosen - 1, 0 )

        # left side including the obervation
        #k = self.num_chosen

        # Observations [ 4 x num_nodes ]
        O = np.stack( (                                              k, 
                        self.num_selected                          - k,
                        self.N                                     - k,
                        self.num_data - self.num_selected - self.N + k ), axis=0 )
        
        assert np.all( O.sum(axis=0) == self.num_data )
        
        log_enum = 2 * safe_log( np.absolute( (O[0,:] * O[3,:]) - (O[1,:] * O[2,:]) )) + safe_log( self.num_data )
        log_denum = safe_log( self.num_selected ) + \
                    safe_log( self.N ) + \
                    safe_log( self.num_data - self.N ) + \
                    safe_log( self.num_data - self.num_selected )

        # Pruefgrosse
        X = np.exp( log_enum - log_denum )

        # getting left side sum up until (exclusive) num_chosen
        chi2_logcdf = chi2.logcdf( X, num_degrees_of_freedom )

        # converting to right cdf
        right_cdf = -np.expm1( chi2_logcdf, dtype=np.float64 )
        right_cdf[self.num_chosen==0] = 1.
        
        return (right_cdf, X)

    

    def gtest_type_one_error_prob( self ):
        """
        Performs a G-test on the frequency distribution of the selected elements.
        The result show significanc if the frequencies are unusual low or high

        Returns
        -------
        right side of (chi²) cumulative distribution 
        function for each (selected) node
            the smaller this value, the better (more significant)
        """
        
        #               | this node  |     not this node                | 
        # --------------|------------|----------------------------------|
        #     selected: |   k        |  num_selected - k                |
        # --------------|-------    -|----------------------------------|
        # not-selected: |  N-k       |  num_data - num_selected - N + k |

        # According to: 
        # https://en.wikipedia.org/wiki/G-test
        # https://de.wikipedia.org/wiki/G-Test

        num_degrees_of_freedom = 1

        # left side excluding the obervation
        k = np.maximum( self.num_chosen - 1, 0 )

        # left side including the obervation
        #k = self.num_chosen

        # Observations [ 2 x num_nodes ]
        O = np.stack( (          k,
                        self.N - k ), axis=0 )
        O = O.astype( np.float32 )

        # The expected counts under the null hypothesis ( Means of hypergeometric distributions )
        Ek  = self.N * np.divide( self.num_selected, self.num_data, dtype=np.float32 )
        
        # Expectations [ 2 x num_nodes ]
        E = np.stack( (                             Ek,
                        self.N.astype(np.float32) - Ek ), axis=0 )

        # The G value
        G =  2 * (O * ( safe_log( O, dtype=np.float32 ) - safe_log( E ) )).sum( axis=0 )  # [ num_nodes ]

        # getting left side sum up until (exclusive) num_chosen
        chi2_logcdf = chi2.logcdf( G, num_degrees_of_freedom )

        # converting to right cdf
        right_cdf = -np.expm1( chi2_logcdf, dtype=np.float64 )
        right_cdf[self.num_chosen==0] = 1.
        
        return (right_cdf, G)

