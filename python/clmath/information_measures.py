# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["hist_counts", "mutual_counts"]


import typing as tp
import numpy as np



eps32 = np.finfo( np.float32 ).eps

# -------------------------------------------------------------------------------------------------------


class hist_counts( object ):

    bincounts : np.ndarray # [ n ]

    def __init__(self, X : np.ndarray, 
                 weights : tp.Optional[np.ndarray] = None,
                     num : tp.Optional[int] = None ):
        
        assert (weights is None) or (weights.size == X.size)

        if num is None:
            num = X.max() + 1
        
        bincounts = np.bincount( X, weights=weights, minlength=num )
        self.bincounts = bincounts.astype(np.int64)


    def entropy( self, do_norm : bool = True, 
                      use_log2 : bool = True ):
        
        if bool(use_log2):
            log = np.log2
        else:
            log = np.log

        entropy = np.dot( log( np.maximum( self.bincounts, eps32, dtype=np.float32 ) ),
                          self.bincounts.astype(np.float32) )

        if bool(do_norm):
            N = self.bincounts.sum()
            entropy = log(N) - ( entropy / N )

        return entropy


# -------------------------------------------------------------------------------------------------------


class mutual_counts( object ):

    counts = None  # [ nX x nY ]


    def __init__(self, X, Y, weights=None, num_X=None, num_Y=None ):

        assert X.size == Y.size
        assert (weights is None) or (weights.size == X.size)

        if num_X is None:
            num_X = X.max() + 1
        
        if num_Y is None:
            num_Y = Y.max() + 1
        
        jointXY = np.ravel_multi_index( (X,Y), (num_X,num_Y) )  # [ N ]

        counts = np.bincount( jointXY, weights=weights, minlength= (num_X * num_Y) )  # [nX * nY]
        self.counts = counts.astype( np.int64 ).reshape( (num_X,num_Y) )



    def conditional_entropy(self, axis : int,
                                  do_norm : bool = True,
                                  use_log2 : bool = True ):
        """
        Parameters
        ----------
        axis: integer
            The axis of labels, NOT the axis of nodes
        do_norm : bool (default=True)
            Normalize with number of data (sum of weights)
        use_log2 : bool (default=False)
            Use base 2 logarithm instead of natural logarithm
        Returns
        -------
        array with unormed conditional entropy
        """

        if bool(use_log2):
            log = np.log2
        else:
            log = np.log

        node_sizes = self.counts.sum( axis=axis, dtype=np.int64 ) # [ nN ]
        max_entropy = np.dot( log( np.maximum( node_sizes, eps32, dtype=np.float32 ) ), \
                              node_sizes.astype(np.float32) )

        entropy = np.dot( log( np.maximum( self.counts, eps32, dtype=np.float32 ) ).ravel(), \
                          self.counts.ravel().astype(np.float32) )
        
        assert entropy <= max_entropy
        centropy = max_entropy - entropy

        if bool(do_norm) is True:
            centropy *= 1. / max( float(node_sizes.sum()), eps32 )
        
        return float(centropy)



    def mutual_information(self, do_norm : bool = False):
        """
        Parameters
        ----------
        do_norm : bool (default=False)
            Norm mutual information between zero and one
        """
        Nx = self.counts.sum( axis=1, dtype=np.int64 )  # [ nX ]
        Ny = self.counts.sum( axis=0, dtype=np.int64 )  # [ nY ]
        N = Ny.sum()

        muinf = np.dot( np.log2( np.maximum( self.counts, eps32 ) ).ravel(), \
                        self.counts.ravel() )
        
        Hx = -np.dot( Nx, np.log2( np.maximum( Nx, eps32 ) ) )
        Hy = -np.dot( Ny, np.log2( np.maximum( Ny, eps32 ) ) )
        
        muinf += Hx + Hy

        log2_N = np.log2( N )

        # required normalization
        muinf = (muinf / float(N)) + log2_N

        if do_norm:
            # TODO: Fix required! Replace min with entropy of labels via axis parameters
            nval = min( Hx, Hy ) / float(N)
            nval += log2_N
            muinf = muinf / nval

        return float(muinf)
    

    def unormed_node_gini_index( self, axis : int ):
        """
        Parameters
        ----------
        axis: integer
            The axis of labels, NOT the axis of nodes
        
        Returns
        -------
        array with unormed gini index for every node
        """
        axis    = int(axis)
        if axis < 0: axis = 2 - axis
        assert axis in (0,1)

        # counts: [ L x N ]

        if axis == 0:
            dim = lambda i: (i,slice(None)) 
        else:
            dim = lambda i: (slice(None),i)
        
        nL = self.counts.shape[axis]
        nN = self.counts.shape[1-axis]

        node_gini = np.zeros( nN, dtype=self.counts.dtype )  # [ nN ]

        for i in range(1,nL):
            x = self.counts[dim(i)]  # [ 1 x nN ]

            for j in range(i):
                node_gini += np.absolute( x - self.counts[dim(j)] )
        
        return node_gini


    def gini_index(self, axis : int, do_norm : bool = True):
        """
        Parameters
        ----------
        axis: integer
            The axis of labels, NOT the axis of nodes
        do_norm : bool (default=True)
            If False, gini index is not normalized with the number of data
            The gini index then is not restricted to interval between 0 and 1 but
            scales with the number of data.
            The advantage could be, that it could be added to other unormalized gini
            index values resulting in the overall gini index after a final normalization
        
        Returns
        -------
        normed (float) or unormed (int64) gini index
        """
        do_norm = bool(do_norm)

        gini = int( self.unormed_node_gini_index( axis ).astype(np.int64).sum() )

        if do_norm is True:
            N  = int(self.counts.sum( dtype=np.int64 ))
            nL = int(self.counts.shape[axis])
            gini = float(gini) * (1 / float( N * (nL-1) )) # Hä!? Why minus 1???
            # TODO: Find out, why I need -1 for correct results.
            # https://de.wikipedia.org/wiki/Gini-Koeffizient says without -1 !
            
        return gini


# -------------------------------------------------------------------------------------------------------
