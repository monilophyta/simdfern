/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <utility>
#include <simd.hpp>
#include <memory>
#include "image_rack.hpp"
#include "image_anker.hpp"
#include "image_margin.hpp"
#include "image_ptr.hpp"
#include "image_sliding_window.hpp"
#include "image_extent.hpp"



namespace img
{

struct CImageFrame;
typedef CImageFrame  image_frame_t;



struct CImageFrame : public image_rack_i32_t
{
    typedef image_rack_i32_t          base_t;
    typedef CImageFrame               this_t;
    typedef CMargin                 margin_t;
    typedef CWindowIterator sliding_window_t;

private:

    int32_t m_nRows;
    int32_t m_nCols;


public:


    inline CImageFrame()
        : base_t( 0, 0, 0 )
        , m_nRows(0)
        , m_nCols(0)
    {}

    inline CImageFrame( int32_t f_nRows,
                        int32_t f_nCols )
        : base_t( 0, f_nCols, 1 )
        , m_nRows( f_nRows )
        , m_nCols( f_nCols )
    {}

    inline CImageFrame( int32_t f_nRows,
                        int32_t f_nCols,
                        int32_t f_offset,
                        int32_t f_rowStride,
                        int32_t f_colStride )
        : base_t( f_offset, f_rowStride, f_colStride )
        , m_nRows( f_nRows )
        , m_nCols( f_nCols )
    {}


    inline void destruct() { *this = CImageFrame(); }  // set everything like in default constructor

    inline int32_t nRows() const { return m_nRows; }
    inline int32_t nCols() const { return m_nCols; }


    this_t mirror_horizontally() const;    // To be implemented
    this_t mirror_vertically() const;  // To be implemented

    this_t window( const margin_t& f_margin ) const;
    
    // treats described frame as integral image
    this_t convert_to_integral_image_frame() const;
    
    //this_t window( const box_i32_t& f_windowBox ) const;    // To be implemented
    
    // ------------------------------------------------------------------------------------------

    inline int32_t anker_offset( const image_coord_i32_t& f_idx ) const { return flat_index( f_idx ); }
    image_anker_i32_t anker( const image_coord_i32_t& f_idx ) const;

    // ------------------------------------------------------------------------------------------


    template <typename IT>
    typename smd::select<IT,smd::bool32_t>::type
    is_inside( const CImageFrameCoordinate<IT>&   f_idx ) const;


    template <typename ET>
    typename smd::comparison<typename ET::value_t>::type
    is_feature_inside( const CFeatureExtent<ET>& f_extent, const image_coord_i32_t& f_idx ) const;


    // ------------------------------------------------------------------------------------------

    /// Sliding window iterators
    sliding_window_t sliding_window_begin( int32_t f_rowStep=1, int32_t f_colStep=1 ) const;
    
    sliding_window_t sliding_window_end( int32_t f_rowStep=1, int32_t f_colStep=1  ) const;

    std::pair<sliding_window_t,sliding_window_t>
    sliding_window( int32_t f_rowStep=1, int32_t f_colStep=1 ) const;
    
    // ------------------------------------------------------------------------------------------

    template <typename T>
    inline CImagePointer<T> initialize_memory_pointer( T* f_memptr ) const;

    template <typename T = int32_t>
    inline CImagePointer<T> allocate_memory() const;

};


} // namespace img

