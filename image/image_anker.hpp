/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <simd.hpp>
#include <type_traits>
#include "image_base_types.hpp"
#include "image_rack.hpp"
#include "image_sliding_window.hpp"


namespace img
{



typedef image_rack_i32_t   image_anker_i32_t;
typedef image_rack_vi32_t  image_anker_vi32_t;



image_anker_i32_t
create_image_anker( const sliding_window_t& f_winIter,
                    const image_frame_t& f_imageFrame );


image_anker_vi32_t
create_image_anker(       sliding_window_t& f_winIter, 
                    const sliding_window_t& f_winEnd,
                       const image_frame_t& f_imageFrame );




} // img
