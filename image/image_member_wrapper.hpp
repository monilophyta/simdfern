/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <type_traits>
#include <simd.hpp>


namespace img
{
//----------------------------------------------------------------------------------


template <typename T>
struct fundamental_wrapper;

typedef fundamental_wrapper<int32_t>            funwrap_i32_t;


template <typename T>
struct simd_wrapper;

typedef simd_wrapper<const smd::vi32_t&>     simdwrap_cvi32_t;


template <typename PTR_T>
struct pointer_wrapper;

typedef pointer_wrapper<const int32_t>         ptrwrap_ci32_t;
typedef pointer_wrapper<int32_t>                ptrwrap_i32_t;
typedef pointer_wrapper<const smd::vi32_t>    ptrwrap_cvi32_t;
typedef pointer_wrapper<smd::vi32_t>           ptrwrap_vi32_t;


//----------------------------------------------------------------------------------



template <typename T>
struct fundamental_wrapper
{
    typedef T constr_t;
    typedef T   init_t;
    
    typedef typename std::remove_const<
            typename std::remove_reference<T>::type>::type value_t;
    
    
    static_assert( std::is_fundamental<value_t>::value );
    
    value_t m_value;

    inline fundamental_wrapper() {}
    inline fundamental_wrapper( constr_t f_value ) : m_value( f_value ) {}


    inline value_t& operator() () { return m_value; }
    inline value_t operator() () const { return m_value; }

    inline operator value_t() const { return m_value; }
};


//----------------------------------------------------------------------------------



template <typename T>
struct simd_wrapper
{
    typedef T       constr_t;


    typedef typename std::remove_const<
            typename std::remove_reference<T>::type>::type value_t;
    
    typedef value_t   init_t;

    static_assert( true == smd::is_simd<value_t>::value );
    static_assert( false == std::is_reference<value_t>::value );
    static_assert( false == std::is_const<value_t>::value );

    value_t m_value;

    inline simd_wrapper( constr_t f_value ) : m_value( f_value ) {}

    inline simd_wrapper& operator=( const simd_wrapper& other ) { m_value = other.m_value; return (*this); }
    inline simd_wrapper& operator=( const value_t& f_val ) { m_value = f_val; return (*this); }

    inline value_t& operator() () { return m_value; }
    inline const value_t& operator() () const { return m_value; }

    inline operator const value_t&() const { return m_value; }
};


//----------------------------------------------------------------------------------



template <typename PTR_T>
struct pointer_wrapper
{
    typedef PTR_T* constr_t; 
    typedef PTR_T*   init_t; 

    typedef PTR_T                                             ref_t;
    typedef typename std::remove_const<ref_t>::type         value_t;
    

    static_assert( !std::is_pointer<ref_t>::value );
    static_assert( !std::is_const<value_t>::value );
    static_assert( !std::is_pointer<PTR_T>::value );
    static_assert( std::is_fundamental<value_t>::value ||  smd::is_simd<value_t>::value );


    PTR_T* m_ptr;

    inline pointer_wrapper( constr_t f_ptr ) : m_ptr( f_ptr ) {}

    inline pointer_wrapper& operator=( const pointer_wrapper& other ) { *m_ptr = *other.m_ptr; return (*this); }
    inline pointer_wrapper& operator=( const value_t& f_val ) { *m_ptr = f_val; return (*this); }

    inline ref_t& operator() () { return *m_ptr; }
    inline const ref_t& operator() () const { return *m_ptr; }

    inline operator const value_t&() const { return *m_ptr; }
};


//----------------------------------------------------------------------------------


} // namespace img
