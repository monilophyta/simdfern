/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "image_frame_coordinate.hpp"
#include "image_type_names.hpp"
#include <sstream>


namespace img
{


inline
std::string CImageFrameCoordinate<int32_t>::to_string() const
{
    std::ostringstream out;

    out << TypeName< CImageFrameCoordinate<int32_t> >()
        << "( rowIdx = " << rowIdx()
        << ", colIdx = " << colIdx()
        << ",  rcIdx = " << rcIdx()
        << " )";

    return out.str();
}


inline
std::string CImageFrameCoordinate<smd::vi32_t>::to_string() const
{
    std::ostringstream out;

    out << TypeName< CImageFrameCoordinate<smd::vi32_t> >()
        << "( rowIdx = " << rowIdx()
        << ", colIdx = " << colIdx()
        << " )";

    return out.str();
}



} // namespace img

