/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "image_box.hpp"
#include "image_type_names.hpp"
#include <sstream>
#include <simd.hpp>




namespace img
{

template <typename IT>
template <typename OT>
inline 
auto CBox<IT>::operator=( const CBox<OT>& other ) -> const this_t&
{
        m_Up() =    other.m_Up();
        m_Down() =  other.m_Down();
        m_Left() =  other.m_Left();
    m_Right() = other.m_Right();
    return *this;
}


template <typename IT>
inline bool CBox<IT>::verify_correctness() const
{
    bool check = (    smd::all( m_Left() <= m_Right() )
                   && smd::all( m_Up() <= m_Down() ) );

    if ( false == check )
    {
        std::cerr << "Inconsistent Box: " << to_string() << std::endl;
    }
    
    return check;
}


template <typename IT>
std::string CBox<IT>::to_string() const
{
    std::ostringstream out;

    out << TypeName< CBox<IT> >()
        << "( Up = " << m_Up()
        << ", Left = " << m_Left()
        << ", Down = " << m_Down()
        << ", Right = " << m_Right()
        << " )";

    return out.str();
}


} // namespace img

