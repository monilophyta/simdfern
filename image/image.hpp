/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "image_cfg.hpp"
#include "image_debug.hpp"

#include "image_base_types.hpp"

#include "image_type_names.hpp"

#include "image_ptr.hpp"

#include "image_frame_coordinate.hpp"
#include "image_sliding_window.hpp"

#include "image_margin.hpp"
#include "image_rack.hpp"
#include "image_anker.hpp"
#include "image_box.hpp"
#include "image_extent.hpp"

#include "image_frame.hpp"

#include "image_integral_box.hpp"


#include "image_type_names.inl"
#include "image_ptr.inl"
#include "image_frame_coordinate.inl"
#include "image_box.inl"
#include "image_frame.inl"





