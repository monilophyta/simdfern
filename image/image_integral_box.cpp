/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "image_integral_box.hpp"
#include "image.hpp"
#include <sstream>


namespace img
{


//----------------------------------------------------------------------------------------------


template <typename IT>
template <typename intval_T, typename ptr_T>
inline intval_T
CIntBox<IT>::evaluate_generic( const CImagePointer<ptr_T>& f_imgPtr, IT f_offset ) const
{
    static_assert( sizeof( CIntBox<IT> ) == (4 * sizeof(idx_t)) );

    return (   f_imgPtr[ f_offset + m_RightDown ]
             - f_imgPtr[ f_offset + m_LeftDown ]
             + f_imgPtr[ f_offset + m_LeftUp ]
             - f_imgPtr[ f_offset + m_RightUp ] );
}

//----------------------------------------------------------------------------------------------


template <typename IT>
auto
CIntBox<IT>::evaluate( const image_ci32_ptr_t& f_imgPtr, IT f_offset ) const -> intval_i32_t
{
    return evaluate_generic<intval_i32_t, const int32_t> ( f_imgPtr, f_offset );
}


template <typename IT>
auto
CIntBox<IT>::evaluate( const image_cf32_ptr_t& f_imgPtr, IT f_offset ) const -> intval_f32_t
{
    return evaluate_generic<intval_f32_t, const float32_t> ( f_imgPtr, f_offset );
}


template <typename IT>
std::string CIntBox<IT>::to_string() const
{
    std::ostringstream out;

    out << TypeName< CIntBox<IT> >()
        << "( LeftUp = " << m_LeftUp
        << ", RightDown = " << m_RightDown
        << ", LeftDown = " << m_LeftDown
        << ", RightUp = " << m_RightUp
        << " )";

    return out.str();
}


// explicit instantiation
template
struct CIntBox<int32_t>;

template
struct CIntBox<const smd::vi32_t&>;


} // namespace img

