/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once



namespace img
{


struct CMargin;
typedef CMargin margin_t;


struct CMargin
{
    int32_t m_Up;
    int32_t m_Down;

    int32_t m_Left;
    int32_t m_Right;


    inline CMargin()
        : m_Up(0)
        , m_Down(0)
        , m_Left(0)
        , m_Right(0)
    {}


    inline CMargin( int32_t f_margin )
        : m_Up( f_margin )
        , m_Down( f_margin )
        , m_Left( f_margin )
        , m_Right( f_margin )
    {}

    inline CMargin( int32_t f_Up,
                    int32_t f_Down,
                    int32_t f_Left,
                    int32_t f_Right )
        : m_Up( f_Up )
        , m_Down( f_Down )
        , m_Left( f_Left )
        , m_Right( f_Right )
    {}   
};


} // namespace img

