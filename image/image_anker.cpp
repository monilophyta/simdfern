/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "image_anker.hpp"
#include "image_debug.hpp"
#include "image_frame.hpp"


namespace img
{

//--------------------------------------------------------------------------------------------------


image_anker_i32_t
create_image_anker( const sliding_window_t& f_winIter,
                    const image_frame_t& f_imageFrame )
{
    return image_anker_i32_t( 
              f_imageFrame.flat_index( f_winIter.rowIdx(), f_winIter.colIdx() ),   // f_offset
              f_imageFrame.rowStride(),                                            // f_rowStride
              f_imageFrame.colStride() );                                          // f_colStride
}


image_anker_vi32_t
create_image_anker(       sliding_window_t& f_winIter, 
                    const sliding_window_t& f_winEnd,
                       const image_frame_t& f_imageFrame )
{
    image_bounds_check( f_winIter != f_winEnd );
    
    smd::vi32_t l_offset;

    if ( f_winIter != f_winEnd )
    {
        auto oIt = l_offset.begin();
        
        for (; (oIt != l_offset.end()) && ( f_winIter != f_winEnd );
               ++oIt, ++f_winIter )
        {
            *oIt = f_imageFrame.flat_index( f_winIter.rowIdx(), f_winIter.colIdx() );
        }

        // if not enough items are availableanymore in f_winIter, 
        // the last elemt is copied for rest
        for (; oIt != l_offset.end(); ++oIt )
        {
            *oIt = *(oIt - 1);
        }
    }
    else
    {
        l_offset.fill(0);
    }

    return image_anker_vi32_t( 
              l_offset,                                                          // f_offset
              smd::vi32_t( f_imageFrame.rowStride() ),                           // f_rowStride
              smd::vi32_t( f_imageFrame.colStride() ) );                         // f_colStride
}


//--------------------------------------------------------------------------------------------------


} // namespace img

