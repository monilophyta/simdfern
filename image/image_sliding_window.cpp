/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "image.hpp"
#include "image_sliding_window.hpp"
#include "image_arithmetics.hpp"


namespace img
{


auto CWindowIterator::operator++() -> this_t&     // prefix
{
    m_colIdx += m_colStep;
    if ( m_colIdx >= m_colEnd )
    {
        m_rowIdx += m_rowStep;
        m_colIdx = m_colOffset;
    }

    return *this;
}


auto CWindowIterator::operator--() -> this_t&     // prefix
{
    m_colIdx -= m_colStep;
    if ( m_colIdx < m_colOffset )
    {
        m_rowIdx -= m_rowStep;

        const int32_t l_colLength = img::remove_remainder(m_colEnd - m_colOffset, m_colStep);
        image_assert( l_colLength > 0, "number of cols should be larger than zero");

        m_colIdx = m_colOffset + l_colLength - 1;
    }

    return *this;
}


std::string CWindowIterator::to_string() const
{
    std::ostringstream out;

    out << TypeName< this_t >()
        << "( rowIdx = " << rowIdx()
        << ", colIdx = " << colIdx()
        << ",  rcIdx = " << rcIdx()
        << ", rowStep = " << m_rowStep
        << ", colStep = " << m_colStep
        << ", colOffset = " << m_colOffset
        << ", colEnd = " << m_colEnd
        << " )";

    return out.str();
}


} // namespace img
