/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include "image_member_wrapper.hpp"



namespace img
{

//----------------------------------------------------------------------------------

template <typename IT>
struct CBox;


typedef CBox< funwrap_i32_t >          box_i32_t;
typedef CBox< simdwrap_cvi32_t >       box_vi32_t;

typedef CBox< ptrwrap_ci32_t >         box_i32_cpt;
typedef CBox< ptrwrap_i32_t >          box_i32_pt;

typedef CBox< ptrwrap_cvi32_t >        box_vi32_cpt;
typedef CBox< ptrwrap_vi32_t >         box_vi32_pt;


//----------------------------------------------------------------------------------


template <typename IT>
struct CBox
{
    typedef CBox<IT>                this_t;
    typedef IT                   wrapper_t;
    typedef typename IT::constr_t constr_t;
    typedef typename IT::value_t     idx_t;
    typedef typename IT::init_t     init_t;


    wrapper_t m_Up;    // inside box
    wrapper_t m_Left;  // inside box

    wrapper_t m_Down;   // outside box
    wrapper_t m_Right;  // outside box


    inline CBox()
      : m_Up( init_t( 0 ) )
      , m_Left( init_t( 0 ) )
      , m_Down( init_t( 0 ) )
      , m_Right( init_t( 0 ) )
    {}

    inline CBox( constr_t f_Up,
                 constr_t f_Left,
                 constr_t f_Down,
                 constr_t f_Right )
      : m_Up( f_Up )
      , m_Left( f_Left )
      , m_Down( f_Down )
      , m_Right( f_Right )
    {}



    template <typename OT>
    inline explicit CBox( const CBox<OT>& other )
    {
        (*this) = other;
    }


    template <typename OT>
    inline 
    const this_t& operator=( const CBox<OT>& other );


    inline idx_t area() const;

    inline bool verify_correctness() const;


    inline std::string to_string() const;
};


//-------------------------------------------------------------------------------------


} // namespace img
