/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <iterator>
#include <simd.hpp>
#include "image_frame_coordinate.hpp"




namespace img
{


struct CWindowIterator;
typedef CWindowIterator sliding_window_t;


struct CWindowIterator 
    : public image_coord_i32_t
    , public std::iterator< std::bidirectional_iterator_tag,
                            image_coord_i32_t >
{
private:

    typedef image_coord_i32_t base_t;
    typedef CWindowIterator   this_t;


    int32_t          m_rowStep;
    int32_t          m_colStep;

    int32_t          m_colOffset;
    int32_t          m_colEnd;      // Valid End of Col + 1

public:

    CWindowIterator()
        : base_t()
        , m_rowStep(0)
        , m_colStep(0)
        , m_colOffset(0)
        , m_colEnd(0)
    {}


    CWindowIterator( int32_t f_rowIdx,
                     int32_t f_colIdx,
                     int32_t f_rowStep,
                     int32_t f_colStep,
                     int32_t f_colOffset,
                     int32_t f_colEnd )
        : base_t( f_rowIdx, f_colIdx )
        , m_rowStep( f_rowStep )
        , m_colStep( f_colStep )
        , m_colOffset( f_colOffset )
        , m_colEnd( f_colEnd )
    {}


    inline image_coord_i32_t& operator*() { return static_cast<image_coord_i32_t&>(*this); }

    this_t& operator++();
    this_t& operator--();

    inline this_t operator++(int) { this_t retval = *this; ++(*this); return retval;}  // postfix
    inline this_t operator--(int) { this_t retval = *this; --(*this); return retval;}  // postfix

    std::string to_string() const;

    //anker_i32_t    single_anker() const;
    //anker_vi32_t multiple_anker() const;

};



} // namespace img
