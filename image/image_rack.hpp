/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <type_traits>
#include "image_base_types.hpp"
#include "image_frame_coordinate.hpp"
#include "image_box.hpp"
#include "image_integral_box.hpp"



namespace img
{

//------------------------------------------------------------------------------------------------------------

template <typename IT>
struct CImageRack;


typedef CImageRack<int32_t>                image_rack_i32_t;
typedef CImageRack< const smd::vi32_t& >   image_rack_vi32_t;


//------------------------------------------------------------------------------------------------------------


template <typename IT>
struct CImageRack
{
protected:

    typedef typename std::remove_const<
                 typename std::remove_reference<IT>::type>::type elem_t;
    
    typedef typename std::conditional< 
                std::is_integral<elem_t>::value,
                elem_t,
                const elem_t& >::type                            member_acc_t;
    
    typedef typename std::conditional< 
                std::is_integral<elem_t>::value,
                intbox_i32_t,
                intbox_vi32_t >::type                            elem_intbox_t;

    static_assert( std::is_integral<elem_t>::value == std::is_same<elem_t,int32_t>::value,
                   "for integral types currently only int32_t is supported" );


    elem_t m_offset;
    elem_t m_rowStride;
    elem_t m_colStride;

public:

    inline CImageRack()
        : m_offset( 0 )
        , m_rowStride( 0 )
        , m_colStride( 0 )
    {}

    inline CImageRack( IT f_offset, IT f_rowStride, IT f_colStride )
        : m_offset( f_offset )
        , m_rowStride( f_rowStride )
        , m_colStride( f_colStride )
    {}


    inline member_acc_t offset() const { return m_offset; }
    inline member_acc_t rowStride() const { return m_rowStride; }
    inline member_acc_t colStride() const { return m_colStride; }


    elem_t      flat_index( const  image_coord_i32_t& f_idx ) const;
    smd::vi32_t flat_index( const image_coord_vi32_t& f_idx ) const;

    template <typename CT>
    inline auto
    flat_index( const CT& f_rowIdx, const CT& f_colIdx ) const
    {
        return flat_index( make_image_coordinate( f_rowIdx, f_colIdx ) );
    }


    elem_intbox_t intbox( const box_i32_t& f_box ) const;
    intbox_vi32_t intbox( const box_vi32_t& f_box ) const;
    
    elem_intbox_t intbox( const box_i32_cpt& f_box ) const;
    elem_intbox_t intbox( const box_i32_pt& f_box ) const;

    intbox_vi32_t intbox( const box_vi32_cpt& f_box ) const;
    intbox_vi32_t intbox( const box_vi32_pt& f_box ) const;

    elem_intbox_t offset_free_intbox( const box_i32_t& f_box ) const;
    intbox_vi32_t offset_free_intbox( const box_vi32_t& f_box ) const;
    
    elem_intbox_t offset_free_intbox( const box_i32_cpt& f_box ) const;
    elem_intbox_t offset_free_intbox( const box_i32_pt& f_box ) const;

    intbox_vi32_t offset_free_intbox( const box_vi32_cpt& f_box ) const;
    intbox_vi32_t offset_free_intbox( const box_vi32_pt& f_box ) const;


private:

    template <typename FLAT_IDX_T, typename IDX_T>
    inline FLAT_IDX_T 
    flat_index_generic( IDX_T rowIdx, IDX_T colIdx, IT offset ) const;

    template <typename INTBOX_T, typename BOX_T>
    inline INTBOX_T
    intbox_generic( const BOX_T& f_box, IT offset ) const;

};


//------------------------------------------------------------------------------------------------------------

// Explicit instantiaten
extern template struct CImageRack< int32_t >;
extern template struct CImageRack< const smd::vi32_t& >;

//------------------------------------------------------------------------------------------------------------



}  // namespace img
