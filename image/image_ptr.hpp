/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <type_traits>
#include "image_base_types.hpp"
#include "image_debug.hpp"



namespace img
{


template <typename T>
struct CImagePointer;


typedef CImagePointer<const int32_t>    image_ci32_ptr_t;
typedef CImagePointer<const float32_t>  image_cf32_ptr_t;

typedef CImagePointer<int32_t>          image_i32_ptr_t;
typedef CImagePointer<float32_t>        image_f32_ptr_t;



template <typename T>
struct CImagePointer
{
public:

    typedef typename std::remove_const<T>::type        elem_t;

    typedef typename smd::select_simd<elem_t>::type    simd_elem_t;

    template<typename U>
    friend struct CImagePointer;


private:

    T*              m_imgPtr;

    std::ptrdiff_t  m_minIdx;
    std::ptrdiff_t  m_maxIdx;

public:


    inline
    CImagePointer()
        : m_imgPtr( NULL )
        , m_minIdx(  (std::numeric_limits< decltype(m_minIdx) >::max() >> 2)  )
        , m_maxIdx( -(std::numeric_limits< decltype(m_maxIdx) >::max() >> 2)  )
    {}


    inline
    CImagePointer( const CImagePointer<elem_t> &other )
        : m_imgPtr( other.m_imgPtr )
        , m_minIdx( other.m_minIdx )
        , m_maxIdx( other.m_maxIdx )
    {}


    inline
    CImagePointer( T* f_memptr, std::ptrdiff_t f_minIdx, std::ptrdiff_t f_maxIdx )
        : m_imgPtr( f_memptr )
        , m_minIdx( f_minIdx )
        , m_maxIdx( f_maxIdx )
    {}

    inline CImagePointer& operator=( const CImagePointer& ) = default;


    inline void destruct()   // set everything like in default constructor
    {
        *this = CImagePointer<T>();
    }


    inline void deallocate_memory();

    inline void set_zero();

    inline elem_t operator[] ( std::ptrdiff_t f_idx ) const;

    inline simd_elem_t operator[] ( const smd::vi32_t& f_idx ) const;

    inline elem_t& operator[] ( std::ptrdiff_t f_idx );

};



} // namespace img
