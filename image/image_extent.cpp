/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "image_extent.hpp"
#include "image.hpp"
#include <algorithm>
#include <simd.hpp>
#include <sstream>



namespace img
{


//----------------------------------------------------------------------------------


template <typename IT>
bool CFeatureExtent<IT>::verify_correctness() const
{
    bool check = (    smd::all( m_Left() <= m_Right() )
                   && smd::all( m_Up() <= m_Down() ) );

    if ( false == check )
    {
        std::cerr << "Inconsistent Extent: " << to_string() << std::endl;
    }
    
    return check;
}


template <typename IT>
std::string CFeatureExtent<IT>::to_string() const
{
    std::ostringstream out;

    out << TypeName< CFeatureExtent<IT> >()
        << "( Up = " << m_Up()
        << ", Left = " << m_Left()
        << ", Down = " << m_Down()
        << ", Right = " << m_Right()
        << " )";

    return out.str();
}



//----------------------------------------------------------------------------------


template <typename IT>
template <typename CT>
auto CFeatureExtent<IT>::create_feature_extent( const CBox<CT>& f_boxFirst,
                                                const CBox<CT>& f_boxSecond ) -> this_t
{
    image_expect( f_boxFirst.verify_correctness(), "Inconsistent image box" );
    image_expect( f_boxSecond.verify_correctness(), "Inconsistent image box" );
    
    this_t l_extent( std::min( f_boxFirst.m_Up(),    f_boxSecond.m_Up() ),
                     std::max( f_boxFirst.m_Down(),  f_boxSecond.m_Down() ),
                     std::min( f_boxFirst.m_Left(),  f_boxSecond.m_Left() ),
                     std::max( f_boxFirst.m_Right(), f_boxSecond.m_Right() ) );
    
    image_expect( l_extent.verify_correctness(), "Inconsistent feature extent" );
    
    return l_extent;
}


//----------------------------------------------------------------------------------


#define CREATE_FEATURE_EXTENT_INSTANTIATION( RET_T, BOX_T, TEMP ) \
template RET_T \
RET_T::create_feature_extent< TEMP >( const BOX_T& f_boxFirst, const BOX_T& f_boxSecond );


CREATE_FEATURE_EXTENT_INSTANTIATION( feature_extent_i32_t,   box_i32_t,    funwrap_i32_t )
CREATE_FEATURE_EXTENT_INSTANTIATION( feature_extent_i32_t, box_i32_cpt,   ptrwrap_ci32_t )
CREATE_FEATURE_EXTENT_INSTANTIATION( feature_extent_i32_t, box_i32_pt,     ptrwrap_i32_t )


CREATE_FEATURE_EXTENT_INSTANTIATION( feature_extent_vi32_t, box_vi32_t,  simdwrap_cvi32_t )
CREATE_FEATURE_EXTENT_INSTANTIATION( feature_extent_vi32_t, box_vi32_cpt, ptrwrap_cvi32_t )
CREATE_FEATURE_EXTENT_INSTANTIATION( feature_extent_vi32_t, box_vi32_pt,   ptrwrap_vi32_t )
#undef CREATE_FEATURE_EXTENT_INSTANTIATION



//----------------------------------------------------------------------------------


// Explicit instantiations
template struct CFeatureExtent< funwrap_i32_t >;
template struct CFeatureExtent< ptrwrap_ci32_t >;
template struct CFeatureExtent< ptrwrap_i32_t >;
template struct CFeatureExtent< simdwrap_cvi32_t >;
template struct CFeatureExtent< ptrwrap_cvi32_t >;
template struct CFeatureExtent< ptrwrap_vi32_t >;


} // namespace img
