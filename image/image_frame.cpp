/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "image_frame.hpp"
#include "image.hpp"
#include "image_arithmetics.hpp"



namespace img
{


auto CImageFrame::window( const margin_t& f_margin ) const -> this_t
{
    const bool l_validMargin = ( m_nRows > (f_margin.m_Up + f_margin.m_Down) ) &&
                               ( m_nCols > (f_margin.m_Left + f_margin.m_Right) );
    
    if (false == l_validMargin)
        return this_t();
    
    return this_t( m_nRows - f_margin.m_Up   - f_margin.m_Down,
                   m_nCols - f_margin.m_Left - f_margin.m_Right, 
                   flat_index( f_margin.m_Up,               // rowIdx
                               f_margin.m_Left ),           // colIdx
                   m_rowStride, m_colStride );
}


auto CImageFrame::convert_to_integral_image_frame() const -> this_t
{
    return this_t( m_nRows - 1,
                   m_nCols - 1,
                   m_offset,
                   m_rowStride, m_colStride );
}


image_anker_i32_t
CImageFrame::anker( const image_coord_i32_t& f_idx ) const
{
    return image_anker_i32_t( flat_index( f_idx ),
                              rowStride(),
                              colStride() );
}

//--------------------------------------------------------------------------------------------------------



template <typename IT>
typename smd::select<IT,smd::bool32_t>::type
CImageFrame::is_inside( const CImageFrameCoordinate<IT>&   f_idx ) const
{
    typedef typename smd::select<IT,smd::bool32_t>::type    bool_t;
    
    bool_t check = ( f_idx.rowIdx() >= 0 ) &&
                   ( f_idx.colIdx() >= 0 ) &&
                   ( f_idx.rowIdx() < nRows() ) &&
                   ( f_idx.colIdx() < nCols() );
    
    return check;
}


//Explicit instantiation
template smd::select<int32_t,smd::bool32_t>::type
CImageFrame::is_inside( const CImageFrameCoordinate<int32_t>& f_idx ) const;

template typename smd::select<smd::vi32_t,smd::bool32_t>::type
CImageFrame::is_inside( const CImageFrameCoordinate<smd::vi32_t>& f_idx ) const;


//--------------------------------------------------------------------------------------------------------


template <typename ET>
typename smd::comparison<typename ET::value_t>::type
CImageFrame::is_feature_inside( const CFeatureExtent<ET>& f_extent, const image_coord_i32_t& f_idx ) const
{
    image_expect( f_extent.verify_correctness(), "Inconsistent feature extent" );
    
    typedef typename CFeatureExtent<ET>::value_t     value_t;
    typedef typename smd::comparison<value_t>::type   bool_t;
    

    bool_t l_check =    ((f_extent.m_Up()    + f_idx.rowIdx() ) >= value_t(0))
                     && ((f_extent.m_Down()  + f_idx.rowIdx() )  < value_t(m_nRows) )
                     && ((f_extent.m_Left()  + f_idx.colIdx() ) >= value_t(0) )
                     && ((f_extent.m_Right() + f_idx.colIdx() )  < value_t(m_nCols) );
    return l_check;
}


//Explicit instantiation
#define IS_FEATURE_INSIDE( TEMPP ) \
template typename smd::comparison<typename TEMPP::value_t>::type \
CImageFrame::is_feature_inside<TEMPP>( const CFeatureExtent<TEMPP>& f_extent, const image_coord_i32_t& f_idx ) const;

IS_FEATURE_INSIDE( funwrap_i32_t )
IS_FEATURE_INSIDE( ptrwrap_ci32_t )
IS_FEATURE_INSIDE( ptrwrap_i32_t )

IS_FEATURE_INSIDE( simdwrap_cvi32_t )
IS_FEATURE_INSIDE( ptrwrap_cvi32_t )
IS_FEATURE_INSIDE( ptrwrap_vi32_t )

#undef IS_FEATURE_INSIDE


//--------------------------------------------------------------------------------------------------------


auto CImageFrame::sliding_window_begin( int32_t f_rowStep, int32_t f_colStep ) const -> sliding_window_t
{
    image_assert( f_rowStep > 0, "windows steps need to be larger than zero");
    image_assert( f_colStep > 0, "windows steps need to be larger than zero");
    
    const bool l_validSlide = ( m_nRows > 0 ) && ( m_nCols > 0 );
    
    if (false == l_validSlide)
        return sliding_window_t();
    

    sliding_window_t resIter( 0,              // rowIdx
                              0,              // colIdx
                              f_rowStep,
                              f_colStep,
                              0,              // colOffset
                              m_nCols );      // colEnd
    
    return resIter;
}



auto CImageFrame::sliding_window_end( int32_t f_rowStep, int32_t f_colStep ) const -> sliding_window_t
{
    image_assert( f_rowStep > 0, "windows steps need to be larger than zero");
    image_assert( f_colStep > 0, "windows steps need to be larger than zero");
    
    const bool l_validSlide = ( m_nRows > 0 ) && ( m_nCols > 0 );
    
    if (false == l_validSlide)
        return sliding_window_t();
    
    const int32_t l_finalNumRows = img::remove_remainder( m_nRows + f_rowStep - 1, 
                                                          f_rowStep );
    
    image_assert( l_finalNumRows > 0, "number of rows should be larger than zero");

    sliding_window_t resIter( l_finalNumRows,     // final rowIdx
                              0,                  // final colIdx
                              f_rowStep,
                              f_colStep,
                              0,                  // colOffset
                              m_nCols );          // colEnd
    
    return resIter;
}




auto CImageFrame::sliding_window( int32_t f_rowStep, int32_t f_colStep ) const
-> std::pair<sliding_window_t,sliding_window_t>
{
    return std::make_pair( sliding_window_begin( f_rowStep, f_colStep),
                           sliding_window_end(   f_rowStep, f_colStep ) );
}


} // namespace img
