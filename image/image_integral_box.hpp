/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "image_ptr.hpp"
#include <string>


namespace img
{

//----------------------------------------------------------------------------------

template <typename IT>
struct CIntBox;

typedef CIntBox<int32_t>             intbox_i32_t;
typedef CIntBox<const smd::vi32_t&> intbox_vi32_t;


//----------------------------------------------------------------------------------


template <typename IT>
struct CIntBox
{

    typedef typename std::remove_const<
            typename std::remove_reference<IT>::type>::type idx_t;
    

    // evaluation type for int32 integral images
    typedef typename std::conditional< 
                std::is_integral<idx_t>::value,
                int32_t,
                smd::vi32_t >::type                         intval_i32_t;

    // evaluation type for float32 integral images
    typedef typename std::conditional< 
                std::is_integral<idx_t>::value,
                float32_t,
                smd::vf32_t >::type                         intval_f32_t;
    

    idx_t m_LeftUp;     // left up corner (outside)
    idx_t m_RightDown;  // right down corner (inside)
    
    idx_t m_LeftDown;   // left down corner (outside)
    idx_t m_RightUp;    // right up corner (outside)


    inline CIntBox()
      : m_LeftUp( 0 )
      , m_RightDown( 0 )
      , m_LeftDown( 0 )
      , m_RightUp( 0 )
    {}

    inline CIntBox( 
        IT f_LeftUp_r,
        IT f_RightDown_r,
        IT f_LeftDown_r,
        IT f_RightUp_r )
      : m_LeftUp(    f_LeftUp_r )
      , m_RightDown( f_RightDown_r )
      , m_LeftDown(  f_LeftDown_r )
      , m_RightUp(   f_RightUp_r )
    {}


    intval_i32_t
    evaluate( const image_ci32_ptr_t& f_imgPtr, IT f_offset = idx_t(0) ) const;


    intval_f32_t
    evaluate( const image_cf32_ptr_t& f_imgPtr, IT f_offset = idx_t(0) ) const;


    std::string to_string() const;

private:


    template <typename intval_T, typename ptr_T>
    inline intval_T
    evaluate_generic( const CImagePointer<ptr_T>& f_imgPtr, IT f_offset ) const;
};

//-------------------------------------------------------------------------------------


// explicit instantiation
extern template
struct CIntBox<int32_t>;

extern template
struct CIntBox<const smd::vi32_t&>;

//-------------------------------------------------------------------------------------


} // namespace img
