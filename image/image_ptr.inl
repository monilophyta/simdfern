/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "image_ptr.hpp"
#include <cstdlib>


namespace img
{



template <typename T>
inline void 
CImagePointer<T>::deallocate_memory()
{
    if ( NULL != m_imgPtr )
    {
        std::free( m_imgPtr );
    }
    
    *this = CImagePointer();
}



template <typename T>
inline void CImagePointer<T>::set_zero()
{
    std::memset( m_imgPtr + m_minIdx, int(0), 1 + m_maxIdx - m_minIdx );
}



template <typename T>
inline auto
CImagePointer<T>::operator[] ( std::ptrdiff_t f_idx ) const -> elem_t
{
    image_bounds_check( (m_minIdx <= f_idx) && (m_maxIdx >= f_idx) );
    return m_imgPtr[ f_idx ];
}



template <typename T>
inline auto
CImagePointer<T>::operator[] ( const smd::vi32_t& f_idx ) const -> simd_elem_t
{
    image_bounds_check( smd::all( ( f_idx >= smd::vi32_t( static_cast<int32_t>(m_minIdx) ) ) && 
                                  ( f_idx <= smd::vi32_t( static_cast<int32_t>(m_maxIdx) ) ) ) );
    return f_idx.gather( m_imgPtr );
}



template <typename T>
inline auto
CImagePointer<T>::operator[] ( std::ptrdiff_t f_idx ) -> elem_t&
{
    image_bounds_check( (m_minIdx <= f_idx) && (m_maxIdx >= f_idx) );
    return m_imgPtr[ f_idx ];
}


} // namespace img

