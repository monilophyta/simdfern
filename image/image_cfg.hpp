/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#ifndef __BYTE_ORDER__
    static_assert( false, "Unable to determ byte order. Makro __BYTE_ORDER__ ist not set" );
#else

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__

    #define IMAGE_USE_LITTLE_ENDIAN
    //#pragma message("Using Little Endian") 

#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__

    #undef IMAGE_USE_LITTLE_ENDIAN
    //#pragma message("Using Big Endian") 

#else
    static_assert( false, "Unknown byte order __BYTE_ORDER__" );
#endif  // #if

#endif // #ifndef
