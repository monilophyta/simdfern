/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "image_frame.hpp"
//#include <limit>
#include <algorithm>



namespace img
{

#define IS_FEATURE_INSIDE( TEMPP ) \
extern template typename smd::comparison<typename TEMPP::value_t>::type \
CImageFrame::is_feature_inside<TEMPP>( const CFeatureExtent<TEMPP>& f_extent, const image_coord_i32_t& f_idx ) const;

IS_FEATURE_INSIDE( funwrap_i32_t )
IS_FEATURE_INSIDE( ptrwrap_ci32_t )
IS_FEATURE_INSIDE( ptrwrap_i32_t )

IS_FEATURE_INSIDE( simdwrap_cvi32_t )
IS_FEATURE_INSIDE( ptrwrap_cvi32_t )
IS_FEATURE_INSIDE( ptrwrap_vi32_t )

#undef IS_FEATURE_INSIDE




template <typename T>
inline CImagePointer<T> 
CImageFrame::initialize_memory_pointer( T* f_memptr ) const
{
    std::ptrdiff_t l_minIdx =  std::numeric_limits< std::ptrdiff_t >::max();
    std::ptrdiff_t l_maxIdx = -std::numeric_limits< std::ptrdiff_t >::max();

    std::tie( l_minIdx, l_maxIdx ) = 
            std::minmax( offset(), 
                         flat_index( nRows() - 1, nCols() - 1 ) );

    image_assert( l_minIdx <= l_maxIdx, "make_pair did not work as expected" );

    return CImagePointer<T>( f_memptr, l_minIdx, l_maxIdx );
}



template <typename T>
inline CImagePointer<T>
CImageFrame::allocate_memory() const
{
    //int32_t l_minIdx = std::numeric_limits<int32_t>::max();
    //int32_t l_maxIdx = -std::numeric_limits<int32_t>::max();

    auto l_frameCorners = { flat_index(0,0), 
                            flat_index(m_nRows,0),
                            flat_index(0,m_nCols) };
    
    // minimum and maximum idx within frame
    std::pair<int32_t,int32_t> l_minmaxIdx = std::minmax( l_frameCorners );

    image_assert( l_minmaxIdx.first <= l_minmaxIdx.second, "Minimum not smaller than Maximum" );
    image_assert( l_minmaxIdx.first >= 0 , "negative image indices" );
    image_assert( l_minmaxIdx.second >= 0 , "negative image indices" );

    // Final amount of memory for allocation
    size_t l_memSize = l_minmaxIdx.second - std::min( l_minmaxIdx.first, int32_t(0) );

    // memory points
    T* l_memptr = reinterpret_cast<T*>( std::malloc( sizeof(int32_t) * l_memSize ) );

    if (l_minmaxIdx.first < 0)
    {
        /* This should actually never happen since it acutally means, that indices point
           before an image points!
           However this function handles also this case in memory allocating for prevention of
           untracable segmentation faults.
           In Debug mode an assertion shall be raised */
        
        // move pointer forward
        l_memptr -= l_minmaxIdx.first;

        // reset remaining memSize
        l_memSize += l_minmaxIdx.first;
    }
    
    return CImagePointer<T>( l_memptr, 0, l_memSize - 1 );
}


} // namespace img