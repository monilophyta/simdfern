/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <type_traits>
#include <simd.hpp>
#include "image_cfg.hpp"


namespace img
{

template <typename IT>
struct CImageFrameCoordinate;


typedef CImageFrameCoordinate<int32_t>        image_coord_i32_t;
typedef CImageFrameCoordinate<smd::vi32_t>    image_coord_vi32_t;

//------------------------------------------------------------------------------------

template <typename IT>
inline CImageFrameCoordinate<IT>
make_image_coordinate( const IT& f_rowIdx, const IT& f_colIdx )
{
    return CImageFrameCoordinate<IT>( f_rowIdx, f_colIdx );
}


//------------------------------------------------------------------------------------


template <typename IT>
struct CImageFrameCoordinate {};


//------------------------------------------------------------------------------------


template<>
struct CImageFrameCoordinate<int32_t>
{
private:

    typedef CImageFrameCoordinate<int32_t> this_t;

protected:

    union 
    {
        int64_t      m_rcIdx;
        struct
        {
        #ifdef IMAGE_USE_LITTLE_ENDIAN
            int32_t  m_colIdx;
            int32_t  m_rowIdx;
        #else
            int32_t  m_rowIdx;
            int32_t  m_colIdx;
        #endif // IMAGE_USE_LITTLE_ENDIAN
        };
    };

public:

    CImageFrameCoordinate()
        : m_rcIdx(0)
    {}


    CImageFrameCoordinate( int32_t f_rowIdx,
                           int32_t f_colIdx )
        : 
        #ifdef IMAGE_USE_LITTLE_ENDIAN
            m_colIdx( f_colIdx ),
            m_rowIdx( f_rowIdx )
        #else
            m_rowIdx( f_rowIdx ),
            m_colIdx( f_colIdx )
        #endif // IMAGE_USE_LITTLE_ENDIAN
    {}

    int32_t rowIdx() const { return m_rowIdx; }
    int32_t colIdx() const { return m_colIdx; }
    int64_t rcIdx() const { return m_rcIdx; }


    inline bool operator==( const this_t& other) const { return (rcIdx() == other.rcIdx() ); }
    inline bool operator!=( const this_t& other) const { return (rcIdx() != other.rcIdx() ); }

    inline std::string to_string() const;
};


//------------------------------------------------------------------------------------


template<>
struct CImageFrameCoordinate<smd::vi32_t>
{
private:

    typedef CImageFrameCoordinate<smd::vi32_t> this_t;

protected:

    smd::vi32_t m_rowIdx;
    smd::vi32_t m_colIdx; 

public:

    CImageFrameCoordinate()
        : m_rowIdx(0)
        , m_colIdx(0)
    {}


    CImageFrameCoordinate( const smd::vi32_t& f_rowIdx,
                           const smd::vi32_t& f_colIdx )
        : m_rowIdx( f_rowIdx )
        , m_colIdx( f_colIdx )
    {}

    const smd::vi32_t& rowIdx() const { return m_rowIdx; }
    const smd::vi32_t& colIdx() const { return m_colIdx; }

    inline std::string to_string() const;
};




}  // namespace img