/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "image_member_wrapper.hpp"
#include "image_box.hpp"
#include <simd.hpp>
#include <type_traits>
#include <string>


namespace img
{


//----------------------------------------------------------------------------------

template <typename IT>
struct CFeatureExtent;


typedef CFeatureExtent< funwrap_i32_t >            feature_extent_i32_t;
typedef CFeatureExtent< simdwrap_cvi32_t >        feature_extent_vi32_t;

typedef CFeatureExtent< ptrwrap_ci32_t >         feature_extent_i32_cpt;
typedef CFeatureExtent< ptrwrap_i32_t >           feature_extent_i32_pt;

typedef CFeatureExtent< ptrwrap_cvi32_t >       feature_extent_vi32_cpt;
typedef CFeatureExtent< ptrwrap_cvi32_t >        feature_extent_vi32_pt;


//----------------------------------------------------------------------------------



template <typename IT>
struct CFeatureExtent
{
    typedef CFeatureExtent<IT>      this_t;
    
    typedef IT                   wrapper_t;
    typedef typename IT::constr_t constr_t;
    typedef typename IT::value_t   value_t;
    typedef typename IT::init_t     init_t;

    wrapper_t m_Up;
    wrapper_t m_Down;

    wrapper_t m_Left;
    wrapper_t m_Right;


    inline CFeatureExtent()
        : m_Up( init_t(0) )
        , m_Down( init_t(0) )
        , m_Left( init_t(0) )
        , m_Right( init_t(0) )
    {}


    inline CFeatureExtent( constr_t f_Up,
                           constr_t f_Down,
                           constr_t f_Left,
                           constr_t f_Right )
        : m_Up( f_Up )
        , m_Down( f_Down )
        , m_Left( f_Left )
        , m_Right( f_Right )
    {}


    //----------------------------------------------------------------------------------


    template <typename CT>
    static this_t create_feature_extent( const CBox<CT>& f_boxFirst,
                                         const CBox<CT>& f_boxSecond );
    
    //----------------------------------------------------------------------------------

    template <typename OT>
    inline explicit CFeatureExtent( const CFeatureExtent<OT>& other )
    {
        (*this) = other;
    }


    template <typename OT>
    inline void union_update( const CFeatureExtent<OT>& other )
    {
        m_Up    = std::min( m_Up(),    smd::min( other.m_Up()   ) );
        m_Down  = std::max( m_Down(),  smd::max( other.m_Down() ) );
        m_Left  = std::min( m_Left(),  smd::min( other.m_Left() ) );
        m_Right = std::max( m_Right(), smd::max( other.m_Right() ) );
    }


    template <typename OT>
    inline typename
    std::enable_if<    std::is_fundamental<typename OT::value_t>::value 
                    == std::is_fundamental<typename IT::value_t>::value,
                   const this_t&>::type 
    operator=( const CFeatureExtent<OT>& other )
    {
           m_Up() =    other.m_Up();
         m_Down() =  other.m_Down();
         m_Left() =  other.m_Left();
        m_Right() = other.m_Right();
        return *this;
    }

    bool verify_correctness() const;

    std::string to_string() const;
};




extern template struct CFeatureExtent< funwrap_i32_t >;
extern template struct CFeatureExtent< ptrwrap_ci32_t >;
extern template struct CFeatureExtent< ptrwrap_i32_t >;
extern template struct CFeatureExtent< simdwrap_cvi32_t >;
extern template struct CFeatureExtent< ptrwrap_cvi32_t >;
extern template struct CFeatureExtent< ptrwrap_vi32_t >;


} // nnamespace img

