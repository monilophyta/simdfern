/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


namespace img
{

template <typename T>
constexpr 
T remove_remainder( T f_dividend, T f_divisor )
{
    return (f_dividend - (f_dividend % f_divisor));
}




} // namespace img

