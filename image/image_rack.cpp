/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "image_rack.hpp"




//---------------------------------------------------------------------------------------------------

namespace img
{

//---------------------------------------------------------------------------------------------------


template <typename IT>
template <typename FLAT_IDX_T, typename IDX_T>
FLAT_IDX_T  
CImageRack<IT>::flat_index_generic( IDX_T f_rowIdx, IDX_T f_colIdx, IT f_offset ) const
{
    return ( ( (f_rowIdx * m_rowStride) + 
               (f_colIdx * m_colStride) ) + 
               f_offset ); 
}


//---------------------------------------------------------------------------------------------------

template <typename IT>
auto
CImageRack<IT>::flat_index( const image_coord_i32_t& f_idx ) const -> elem_t
{
    return flat_index_generic<elem_t, int32_t>( f_idx.rowIdx(), f_idx.colIdx(), m_offset );
}



template <typename IT>
auto 
CImageRack<IT>::flat_index( const image_coord_vi32_t& f_idx ) const -> smd::vi32_t
{
    return flat_index_generic< smd::vi32_t, const smd::vi32_t& >( f_idx.rowIdx(), f_idx.colIdx(), m_offset );
}


//---------------------------------------------------------------------------------------------------


/** Nature of integral image makes these operations obsolet
IT l_left  = f_cbox.m_Left  - 1;  // puts index outside
IT l_Up    = f_cbox.m_Up  - 1;   // puts index outside
IT l_Down  = f_cbox.m_Down - 1;  // puts index inside
IT l_right = f_cbox.m_Right - 1;   // puts index inside
**/

template <typename IT>
template <typename INTBOX_T, typename BOX_T>
inline INTBOX_T
CImageRack<IT>::intbox_generic( const BOX_T& f_box, IT f_offset ) const
{
    typedef typename INTBOX_T::idx_t        intbox_idx_t;
    typedef typename    BOX_T::idx_t       aux_box_idx_t;

    typedef typename std::conditional<
            std::is_integral<aux_box_idx_t>::value,
            aux_box_idx_t,
            const aux_box_idx_t&>::type        box_idx_t;
    
    static_assert( std::is_integral<aux_box_idx_t>::value == std::is_same<aux_box_idx_t,int32_t>::value,
                   "for integral types currently only int32_t is supported" );
    
    static_assert( std::is_integral<aux_box_idx_t>::value != std::is_reference<box_idx_t>::value,
                   "integral types must not be a reference" );

    
    INTBOX_T ibox( flat_index_generic<intbox_idx_t, box_idx_t>( f_box.m_Up,   f_box.m_Left,  f_offset ),    // left up corner (outside)
                   flat_index_generic<intbox_idx_t, box_idx_t>( f_box.m_Down, f_box.m_Right, f_offset ),    // right down corner (inside)
                   flat_index_generic<intbox_idx_t, box_idx_t>( f_box.m_Down, f_box.m_Left,  f_offset ),    // left down corner (outside)
                   flat_index_generic<intbox_idx_t, box_idx_t>( f_box.m_Up,   f_box.m_Right, f_offset ) );  // right up corner (outside)
    
    return ibox;
}


//---------------------------------------------------------------------------------------------------


template <typename IT>
auto
CImageRack<IT>::intbox( const box_i32_t& f_box ) const -> elem_intbox_t
{
    return intbox_generic<elem_intbox_t, box_i32_t>( f_box, m_offset );
}



template <typename IT>
auto
CImageRack<IT>::intbox( const box_vi32_t& f_box ) const -> intbox_vi32_t
{
    return intbox_generic<intbox_vi32_t, box_vi32_t>( f_box, m_offset );
}



template <typename IT>
auto
CImageRack<IT>::intbox( const box_i32_cpt& f_box ) const -> elem_intbox_t
{
    return intbox_generic<elem_intbox_t, box_i32_cpt>( f_box, m_offset );
}


template <typename IT>
auto
CImageRack<IT>::intbox( const box_i32_pt& f_box ) const -> elem_intbox_t
{
    return intbox_generic<elem_intbox_t, box_i32_pt>( f_box, m_offset );
}


template <typename IT>
auto
CImageRack<IT>::intbox( const box_vi32_cpt& f_box ) const -> intbox_vi32_t
{
    return intbox_generic<intbox_vi32_t, box_vi32_cpt>( f_box, m_offset );
}


template <typename IT>
auto
CImageRack<IT>::intbox( const box_vi32_pt& f_box ) const -> intbox_vi32_t
{
    return intbox_generic<intbox_vi32_t, box_vi32_pt>( f_box, m_offset );
}


template <typename IT>
auto
CImageRack<IT>::offset_free_intbox( const box_i32_t& f_box ) const -> elem_intbox_t
{
    return intbox_generic<elem_intbox_t, box_i32_t>( f_box, elem_t(0) );
}


template <typename IT>
auto
CImageRack<IT>::offset_free_intbox( const box_vi32_t& f_box ) const -> intbox_vi32_t
{
    return intbox_generic<intbox_vi32_t, box_vi32_t>( f_box, elem_t(0) );
}


template <typename IT>
auto
CImageRack<IT>::offset_free_intbox( const box_i32_cpt& f_box ) const -> elem_intbox_t
{
    return intbox_generic<elem_intbox_t, box_i32_cpt>( f_box, elem_t(0) );
}


template <typename IT>
auto
CImageRack<IT>::offset_free_intbox( const box_i32_pt& f_box ) const -> elem_intbox_t
{
    return intbox_generic<elem_intbox_t, box_i32_pt>( f_box, elem_t(0) );
}



template <typename IT>
auto
CImageRack<IT>::offset_free_intbox( const box_vi32_cpt& f_box ) const -> intbox_vi32_t
{
    return intbox_generic<intbox_vi32_t, box_vi32_cpt>( f_box, elem_t(0) );
}



template <typename IT>
auto
CImageRack<IT>::offset_free_intbox( const box_vi32_pt& f_box ) const -> intbox_vi32_t
{
    return intbox_generic<intbox_vi32_t, box_vi32_pt>( f_box, elem_t(0) );
}


//---------------------------------------------------------------------------------------------------


// Explicit instantiaten
template struct CImageRack< int32_t >;
template struct CImageRack< const smd::vi32_t& >;


} // namespace img

