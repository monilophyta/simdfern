/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <cpp_debug.hpp>


/// Assert that EXPRESSION evaluates to true, otherwise raise AssertionFailureException with associated MESSAGE 
/// (which may use C++ stream-style message formatting)
#define image_assert(EXPRESSION, MESSAGE) cpp_assert(EXPRESSION, MESSAGE)

#define image_expect(EXPRESSION, MESSAGE) cpp_assert(EXPRESSION, MESSAGE)

#define image_bounds_check(EXPRESSION) image_assert(EXPRESSION, "Out of bounds access")
#define image_dims_check(EXPRESSION) image_assert(EXPRESSION, "Array/Matrix/Image dimensions do not match")
