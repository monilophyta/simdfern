/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "image_type_names.hpp"


//------------------------------------------------------------------------------
// TypeName Specialization


namespace img
{



template <typename T>
inline std::string TypeName()
{
    typedef typename std::remove_const<
            typename std::remove_reference<T>::type>::type plainType_t;
    return TypeInfo<plainType_t>::Name(); 
}



template <>
struct TypeInfo<intbox_i32_t>
{
    static inline std::string Name()
    {
        return std::string("intbox_i32");
    }
};


template <>
struct TypeInfo<intbox_vi32_t>
{
    static inline std::string Name()
    {
        return std::string("intbox_vi32");
    }
};


template <>
struct TypeInfo<sliding_window_t>
{
    static inline std::string Name()
    {
        return std::string("sliding_window");
    }
};

} // namespace img


