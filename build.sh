#!/bin/sh

# https://stackoverflow.com/questions/45933732/how-to-specify-a-compiler-in-cmake

#export CXX=/usr/bin/g++-8
#export CXX=/usr/bin/clang++-7


# build debug
mkdir -p build/debug
cd build/debug
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ../../
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ../../
cmake --build . -- -j4
cd ../..
cp build/debug/libfernd.{so,dll} ./ 2>/dev/null

# build release
mkdir -p build/release
cd build/release
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ../../
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ../../
cmake --build . -- -j4
cd ../..
cp build/release/libfern.{so,dll} ./ 2>/dev/null
