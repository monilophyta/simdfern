# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



import numpy as np
import matplotlib.pyplot as plt

num_trials = 1
num_classes = 12

min_weight = 10

rstate = np.random.RandomState(59)
width = 0.25  # the width of the bars


#####################################################
# sampling

hists = rstate.rand( num_classes, 2 ).astype( dtype=np.float32 )
hists *= 1. / hists.sum(axis=0)

assert( np.all( hists.ravel() > 0 ) )

alpha = hists[:,0]
assert( np.isclose( 1, alpha.sum() ) )
beta = hists[:,1]
assert( np.isclose( 1, beta.sum() ) )

Z = (min_weight * alpha) / beta # [ N ]
Z_max = Z.max()
#Z_max = max(1,Z.max())
print("Z_max: ", Z_max )

q = ((Z_max * beta) / alpha) - min_weight
assert( np.all( q >= 0 ) )
#print(Z)
print(q)

beta_approx = (min_weight + q) * alpha
beta_approx *= 1 / beta_approx.sum()
#assert( np.all( np.isclose( beta_check, beta ) ) )

print( beta_approx-beta )

# Plot Result
ind = np.arange(num_classes)

fig, ax = plt.subplots()
rects1 = ax.bar(ind - width, alpha, width, label='alpha')
rects2 = ax.bar(ind,         beta, width, label='beta')
rects3 = ax.bar(ind + width, beta_approx, width, label='beta approx')
plt.legend()

plt.show()


