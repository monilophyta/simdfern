# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



NumFeatures = 10000
roiSize = 41
minBoxSize = 1
maxBoxSize = 41

extraStd = 1.5
seed = None


# iterative sampling
num_initial_sampling = 100

################################################################################

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

################################################################################


class BoxSampler(object):
    
    def __init__(self, roiSize, minSize, maxSize ):
        self.roiSize = roiSize
        self.minSize = minSize
        self.maxSize = maxSize

    def sample1D(self, N ):
        # Box Size Sampling
        box_size = np.random.randint( self.minSize, self.maxSize, size=(N,), dtype=np.int )
        
        max_box_pos = self.roiSize - box_size  # [ N ]
        assert( np.all( (max_box_pos + box_size) == self.roiSize ) )
        
        box_pos = (max_box_pos + 1) * np.random.rand( N )
        box_pos = box_pos.astype(np.int)
        
        assert( np.all( box_pos >= 0 ) )
        assert( np.all( box_pos <= max_box_pos ) )
        assert( np.all( (box_pos + box_size) <= self.roiSize ) )
    
        return (box_pos, box_size)
    
    
    def sample2D(self, N ):
        
        box_pos1, box_size1 = self.sample1D( N )
        box_pos2, box_size2 = self.sample1D( N )
        
        box_pos = np.vstack( [ box_pos1, box_pos2 ] ).transpose() # [ N x 2]
        box_size = np.vstack( [ box_size1, box_size2 ] ).transpose() # [ N x 2]
        
        return (box_pos, box_size)
        
###########################################################################

class Box(object):
    
    BoxPos = None   # [ 2 ]
    BoxSize = None  # [ 2 ]
    
    def __init__( self, box_pos, box_size ):
        self.BoxPos = box_pos
        self.BoxSize = box_size
        
    @property
    def pos(self):
        return self.BoxPos

    @property
    def size(self):
        return self.BoxSize
    
    def plot(self, axis, color):
        axis.add_patch( Rectangle( self.pos, self.size[0], self.size[1], alpha=0.3, facecolor=color ) )


class Feature(object):
    
    Box1 = None
    Box2 = None
    
    def __init__(self, box1, box2 ):
        self.Box1 = box1
        self.Box2 = box2
    
    @property
    def first(self):
        return self.Box1
    
    @property
    def second(self):
        return self.Box2
    
    def plot(self, axis, color ):
        self.first.plot(axis,color)
        self.second.plot(axis,color)


class BoxArray(object):
    
    BoxPos = None   # [ N x 2 ]
    BoxSize = None  # [ N x 2 ]
    
    def __init__( self, box_pos, box_size ):
        self.BoxPos = box_pos
        self.BoxSize = box_size
    
    
    @property
    def pos(self):
        return self.BoxPos
    
    
    @property
    def size(self):
        return self.BoxSize
    
    
    def get(self, idx ):
        return Box( self.BoxPos[idx,:], self.BoxSize[idx,:] )
    
    @property
    def num(self):
        return self.BoxPos.shape[0]
    
    def sub(self, fslice):
        return BoxArray( self.BoxPos[fslice,:], self.BoxSize[fslice,:] )
    
    def add(self, box):
        self.BoxPos = np.concatenate( (self.BoxPos[:,:], box.BoxPos[np.new_axis,:]), axis=0 )
        self.BoxSize = np.concatenate( (self.BoxSize[:,:], box.BoxSize[np.new_axis,:]), axis=0 )
    
    def plot(self, idx, axis, color ):
        self.get(idx).plot( axis, color )
        #self.get(idx).plot( axis, color )


class FeatureArray(object):
    
    bArray1 = None
    bArray2 = None
    
    
    def __init__(self, bArray1, bArray2 ):
        self.bArray1 = bArray1
        self.bArray2 = bArray2
    
    @classmethod
    def sample( cls, Num, boxSampler ):
        box1_pos, box1_size = boxSampler.sample2D( Num )
        box2_pos, box2_size = boxSampler.sample2D( Num )
        
        return cls( BoxArray(box1_pos, box1_size),
                    BoxArray(box2_pos, box2_size) )
    
    @property
    def first(self):
        return self.bArray1
    
    @property
    def second(self):
        return self.bArray2
    
    @property
    def num(self):
        return min( self.bArray1.num, self.bArray2.num )
    
    def get(self, idx ):
        return Feature( self.bArray1.get(idx), self.bArray2.get(idx) )
    
    def sub(self, fslice ):
        return FeatureArray( self.bArray1.sub(fslice), self.bArray2.sub(fslice) )
    
    def add(self, feature):
        self.bArray1.add( feature.first )
        self.bArray2.add( feature.second )
    

########################################################################################

def box2gaussian( leftUp, rightDown, extraStd ):
    
    # leftUp:    [ N x 2 ]
    # rightDown: [ N x 2 ]
    
    mu = 0.5 * (leftUp + rightDown).astype( np.float32 )
    std = 0.5 * np.absolute(leftUp - rightDown).astype( np.float32 )
    std += extraStd
    
    return (mu, std )



def jsh_boxgauss_divergence( muA, stdA, muB, stdB ):
    
    # mu:    [ N x 2 ]
    # std:   [ N x 2 ]
    
    varA = np.square( stdA )
    varB = np.square( stdB )
    
    divergence = ( varB / (2*varA) ) + ( varA / (2*varB) ) - 1 + \
                 np.square( muA - muB ) * ( 0.5 * ( np.reciprocal( varA ) + np.reciprocal( varB ) ) )
    assert( np.all( divergence.ravel() >= 0 ) )
    
    return divergence


def box_divergense( boxA, boxB ):
    muA, stdA = box2gaussian( boxA.pos, boxA.pos + boxA.size - 1, extraStd )
    muB, stdB = box2gaussian( boxB.pos, boxB.pos + boxB.size - 1, extraStd )
    
    divergence = 0.5 * jsh_boxgauss_divergence( muA, stdA, muB, stdB )
    return divergence.sum(axis=-1)


def jsh_boxgauss_crossentropy( muA, stdA, muB, stdB ):
    
    # mu:    [ N x 2 ]
    # std:   [ N x 2 ]
    
    varA = np.square( stdA )
    varB = np.square( stdB )
    
    divergence = np.log( 2 * np.pi * stdA * stdB ) + \
                 ( varB / (2*varA) ) + ( varA / (2*varB) ) + \
                 np.square( muA - muB ) * ( 0.5 * ( np.reciprocal( varA ) + np.reciprocal( varB ) ) )
    assert( np.all( divergence.ravel() >= 0 ) )
    
    return divergence


def box_crossentropy( boxA, boxB ):
    muA, stdA = box2gaussian( boxA.pos, boxA.pos + boxA.size - 1, extraStd )
    muB, stdB = box2gaussian( boxB.pos, boxB.pos + boxB.size - 1, extraStd )
    
    crossentropy = 0.5 * jsh_boxgauss_crossentropy( muA, stdA, muB, stdB ).sum(axis=-1)
    return crossentropy


########################################################################################

def feature_divergence( f1Array, f2Array, measure ):
    
    cmp1 = measure( f1Array.first, f2Array.first ) + measure( f1Array.second, f2Array.second )
    
    cmp2 = measure( f1Array.first, f2Array.second ) + measure( f1Array.second, f2Array.first )
                               
    return np.minimum( cmp1, cmp2 )


def pairwise_feature_divergence( f1Array, f2Array, measure ):
        
    div_matrix = np.zeros( (f1Array.num, f1Array.num), dtype=np.float32 )
    
    for i in range(f1Array.num):
        div_matrix[i,:] = feature_divergence( f1Array.get(i), f2Array, measure )

    return div_matrix


def sym_pairwise_feature_divergence( fArray, measure ):
    
    div_matrix = np.zeros( (fArray.num, fArray.num), dtype=np.float32 )
    
    min_div = np.empty( fArray.num, dtype=np.float32 )
    min_div.fill( np.nan )
    
    for i in range(1, fArray.num-1):
        divergences = feature_divergence( fArray.get(i), fArray.sub( slice(i,None,None) ), measure )
        div_matrix[i-1,i:] = divergences
        
        min_div[i-1] = min( min_div[i-1], divergences.min() )
        min_div[i:] = np.minimum( min_div[i:], divergences )
    
    div_matrix = div_matrix + div_matrix.transpose()

    return (div_matrix, min_div)


########################################################################################


def iterative_sampling():
    boxSampler = BoxSampler( roiSize, minBoxSize, maxBoxSize )
    
    # first sampling
    fArray = FeatureArray.sample( num_initial_sampling, boxSampler )
    div_matrix, min_divergences = sym_pairwise_feature_divergence( fArray, box_crossentropy )
    
    maxIdx = min_divergences.argmax()
    
    # first element in container
    featureContainer = FeatureArray.sub( slice(maxIdx,maxIdx+1) )
    
    


def main():
    boxSampler = BoxSampler( roiSize, minBoxSize, maxBoxSize )
    
    f1Array = FeatureArray.sample( NumFeatures, boxSampler )
    #f2Array = FeatureArray.sample( NumFeatures, boxSampler )
    
    fullDiv = pairwise_feature_divergence( f1Array, box_crossentropy )
    fullDiv_triu = np.triu( fullDiv )
    fullDiv_triu_ravel = fullDiv.ravel()[ fullDiv_triu.ravel() > 0 ]
    #measure = feature_divergence( f1Array, f2Array, box_divergense )
    #measure = feature_divergence( f1Array, f2Array, box_crossentropy )
    
    plt.figure()
    plt.hist( fullDiv_triu_ravel, 100 )
    plt.yscale('log')
    
    maxRowIdx = fullDiv_triu.max(axis=1).argmax()
    maxColIdx = fullDiv_triu.max(axis=0).argmax()
    print( fullDiv_triu[ maxRowIdx, maxColIdx ] )
    
    plt.figure()
    ax = plt.gca()
    f1Array.get(maxRowIdx).plot( plt.gca(), "r" )
    f1Array.get(maxColIdx).plot( plt.gca(), "g" )
    plt.xlim([0,roiSize-1])
    plt.ylim([0,roiSize-1])
    ax.set_aspect('equal')
    
    plt.figure()
    plt.imshow( fullDiv )
    plt.colorbar()


########################################################################################

np.random.seed( seed )
main()







