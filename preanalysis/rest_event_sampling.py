# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--



import numpy as np
import matplotlib.pyplot as plt


num_events = 2**24
#num_events = 100
num_samples = 1600
num_trials = 1000


def eval_counts( sampler ):
    improvement_counts = np.zeros( num_samples-1, dtype=np.int )

    for i in range(num_trials):

        samples = sampler( num_events, num_samples )

        past_best = np.maximum.accumulate( samples )
        future_best = np.maximum.accumulate( samples[::-1] )[::-1]

        # check wenn improvements in the future have been possible
        best_mask = past_best[:-1] < future_best[1:]
        improvement_counts += best_mask
    
    improvement_freq = improvement_counts.astype( np.float32 ) / num_trials

    return improvement_freq


def theory_counts():
    N = np.arange( num_samples-1 )
    M = num_samples - 1 - N

    N = N.astype(np.float32)
    M = M.astype(np.float32)

    improvement_freq = (1. / (N + 1)) - (1 / (N+M+1))

    return improvement_freq


###################################################

fig, ax = plt.subplots()

# uniform sampling
sampler = lambda num_events, num_samples: np.random.randint( 0, num_events, num_samples )
plt.plot( eval_counts( sampler ), label="uniform int" )

# uniform sampling
sampler = lambda num_events, num_samples: np.random.rand( num_samples )
plt.plot( eval_counts( sampler ), label="uniform float" )

# # biased sampling
# sampler = lambda num_events, num_samples: np.random.binomial( num_events, 0.5, num_samples )
# plt.plot( eval_counts( sampler ), label="binomial p=0.5" )

# sampler = lambda num_events, num_samples: np.random.binomial( num_events, 0.01, num_samples )
# plt.plot( eval_counts( sampler ), label="binomial p=0.01" )

# sampler = lambda num_events, num_samples: np.random.binomial( num_events, 0.9, num_samples )
# plt.plot( eval_counts( sampler ), label="binomial p=0.9" )

plt.plot( theory_counts(), label="theory" )

#plt.yscale('log')
plt.legend()
plt.show()