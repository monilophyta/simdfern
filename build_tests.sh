#!/bin/bash


# https://stackoverflow.com/questions/45933732/how-to-specify-a-compiler-in-cmake

#export CXX=/usr/bin/g++-8
#export CXX=/usr/bin/clang++-7


mkdir -p test_build
cd test_build
cmake -G "Unix Makefiles" ../tests
cmake -G "Unix Makefiles" ../tests
cmake --build . -- -j4
cd ../

