/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <image.hpp>


namespace test
{

class CIntegralImageTestBase : public ::testing::Test
{

protected:

    const int32_t m_imgRows = 479;
    const int32_t m_imgCols = 637;

    unsigned int     m_randomSeed_u = 42;


    virtual void SetUp();

    virtual void TearDown();


protected:

    // randomly sampled image
    img::image_i32_ptr_t            m_img_p;
    img::image_frame_t              m_imgFrame;


    img::image_i32_ptr_t            m_intImg_p;
    img::image_frame_t              m_intImgFrame;


    void allocate_memory();
    void deallocate_memory();
    
    void initialize_image();
    void compute_integral_image();
};


} // namespace test

