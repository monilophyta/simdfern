/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace test
{
namespace util
{



template <typename IT>
inline void compute_integral_image( 
            const img::image_frame_t&        f_imgFrame,
            const img::CImagePointer<IT>&    f_img_p,
            const img::image_frame_t&        f_intImgFrame,
            img::CImagePointer<IT>&          f_intImg_p );





} // namespace util
} // namespace test



