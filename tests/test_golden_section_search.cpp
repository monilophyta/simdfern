/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <random>
#include <utility>
#include <limits>
#include <gtest/gtest.h>
#include <tree.hpp>


using namespace tree;

static const unsigned int RANDOM_SEED = 69;



TEST( tree_golden_section_search, tree_golden_section_searcH_test )
{
    static const unsigned int NUM_REPEATS = 111;
    static const float32_t    FTOL        = 1e-4;
    static const float32_t    RADIUS      = 42.f;
    
    std::default_random_engine     l_rand( RANDOM_SEED );
    std::uniform_real_distribution<float32_t> l_uniformDistr( -RADIUS, RADIUS );

    for (unsigned int n = 0; n < NUM_REPEATS; ++n)
    {
        const float32_t u = l_uniformDistr( l_rand );
        const float32_t v = l_uniformDistr( l_rand );

        auto func = [=]( float32_t X ) { return ((X-u)*(X-u) + v); };

        float32_t l_left_f32  = std::numeric_limits<float32_t>::signaling_NaN();
        float32_t l_right_f32 = std::numeric_limits<float32_t>::signaling_NaN();
        
        std::tie(l_left_f32, l_right_f32) = golden_section_search( func, -2.f * RADIUS, 2.f * RADIUS, FTOL );
        
        EXPECT_LE( l_left_f32, l_right_f32 );

        float32_t l_estimate_f32 = (0.5f * l_left_f32) + (0.5f * l_right_f32);

        EXPECT_NEAR( double(l_estimate_f32), double(u), 0.01 );
    }
}

