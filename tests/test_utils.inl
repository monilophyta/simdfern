/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "test_utils.hpp"



namespace test
{
namespace util
{


template <typename IT>
inline void compute_integral_image( 
            const img::image_frame_t&        f_imgFrame,
            const img::CImagePointer<IT>&    f_img_p,
            const img::image_frame_t&        f_intImgFrame,
            img::CImagePointer<IT>&          f_intImg_p )
{
    const int32_t l_imgRows = f_imgFrame.nRows();
    const int32_t l_imgCols = f_imgFrame.nCols();
    
    // Add Horizontal
    for ( int32_t rIdx = 0; rIdx < l_imgRows; ++rIdx )
    {
        for ( int32_t cIdx = 0; cIdx < l_imgCols; ++cIdx )
        {
            const int32_t     l_flatImgIdx = f_imgFrame.flat_index( rIdx, cIdx );
            
            const int32_t l_flatIntPrevIdx = f_intImgFrame.flat_index( rIdx+1, cIdx );
            const int32_t     l_flatIntIdx = f_intImgFrame.flat_index( rIdx+1, cIdx+1 );
            
            ASSERT_EQ( 0, f_intImg_p[ l_flatIntIdx ] );

            f_intImg_p[ l_flatIntIdx ] =   f_intImg_p[ l_flatIntPrevIdx ]
                                         + f_img_p[ l_flatImgIdx ];
        }
    }

    // Add Vertically
    for ( int32_t rIdx = 0; rIdx < l_imgRows; ++rIdx )
    {
        for ( int32_t cIdx = 0; cIdx < l_imgCols; ++cIdx )
        {
            const int32_t l_flatIntPrevIdx = f_intImgFrame.flat_index( rIdx,   cIdx+1 );
            const int32_t     l_flatIntIdx = f_intImgFrame.flat_index( rIdx+1, cIdx+1 );

            f_intImg_p[ l_flatIntIdx ] =   f_intImg_p[ l_flatIntPrevIdx ] 
                                         + f_intImg_p[ l_flatIntIdx ];
        }
    }
}



} // namespace util
} // namespace test

