/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include <gtest/gtest.h>
#include <random>


//#define private public
//#define protected public
//#define class struct

#include <simd.hpp>
#include <tree.hpp>

//#undef private
//#undef protected
//#undef class



using namespace tree;
//using namespace smd;


template <int TREEDEPTH, int NUM_LABELS>
class CTreeStatisticsTest : public ::testing::Test
{
protected:

    const unsigned int m_randomSeed_u = 69;

    const int32_t m_maxWeight_i32 = 179;

    // already reached depth of trained tree
    const int32_t m_treeDepth_i32 = TREEDEPTH;

    const int32_t m_numLabels_i32 = NUM_LABELS;
    const int32_t m_numLeafs_i32  = int32_t(1) << int32_t( m_treeDepth_i32 );

    const int32_t m_numData_i32   = 1677; //100001;

    // -----------------------------------------------------------------------

private:

    std::default_random_engine m_rand;

protected:

    // -----------------------------------------------------------------------

    typedef smd::val_array<int32_t>      node_array_t;
    typedef smd::val_array<int32_t>     index_array_t;
    typedef smd::val_array<int32_t>    weight_array_t;

    struct CDataSet
    {
        index_array_t        m_labelIdx_ai32;
        node_array_t          m_leafIdx_ai32;
        weight_array_t        m_weights_ai32;
        
        inline CDataSet( int32_t f_numData_i32 )
            : m_labelIdx_ai32( f_numData_i32 )
            , m_leafIdx_ai32( f_numData_i32 )
            , m_weights_ai32( f_numData_i32 )
        {}
    };

    CDataSet                    m_data;

    CTreeSplitEntropyStatistics m_entropyStatistics;
    CTreeSplitGiniStatistics    m_giniStatistics;

    // -----------------------------------------------------------------------


    virtual void SetUp();


    template <typename split_stat_T>
    void increase_decrease_test( const split_stat_T& f_stat ) const;


private:

    void generate_sample_dataset( CDataSet& dset );

    void generate_statistics( const CDataSet& dset );

public:

    CTreeStatisticsTest()
        : m_rand( m_randomSeed_u )
        , m_data( m_numData_i32 )
        , m_entropyStatistics( m_numLabels_i32, m_numLeafs_i32 )
        , m_giniStatistics( m_numLabels_i32, m_numLeafs_i32 )
    {
        m_entropyStatistics.initialize_zero();
        m_giniStatistics.initialize_zero();
    }

};

// ----------------------------------------------------------------------------------------------------

typedef CTreeStatisticsTest<1,2>  CTreeStatisticsTest_treedepth1_nlabels2;
typedef CTreeStatisticsTest<2,2>  CTreeStatisticsTest_treedepth2_nlabels2;
typedef CTreeStatisticsTest<7,2>  CTreeStatisticsTest_treedepth7_nlabels2;
typedef CTreeStatisticsTest<5,11> CTreeStatisticsTest_treedepth5_nlabels11;

// ----------------------------------------------------------------------------------------------------

TEST_F( CTreeStatisticsTest_treedepth1_nlabels2, EntropyStatIncreaseDecreaseTest )
{
    increase_decrease_test<CTreeSplitEntropyStatistics>( m_entropyStatistics );
}

TEST_F( CTreeStatisticsTest_treedepth1_nlabels2, GiniStatIncreaseDecreaseTest )
{
    increase_decrease_test<CTreeSplitGiniStatistics>( m_giniStatistics );
}

// ----------------------------------------------------------------------------------------------------

TEST_F( CTreeStatisticsTest_treedepth2_nlabels2, EntropyStatIncreaseDecreaseTest )
{
    increase_decrease_test<CTreeSplitEntropyStatistics>( m_entropyStatistics );
}

TEST_F( CTreeStatisticsTest_treedepth2_nlabels2, GiniStatIncreaseDecreaseTest )
{
    increase_decrease_test<CTreeSplitGiniStatistics>( m_giniStatistics );
}

// ----------------------------------------------------------------------------


TEST_F( CTreeStatisticsTest_treedepth7_nlabels2, EntropyStatIncreaseDecreaseTest )
{
    increase_decrease_test<CTreeSplitEntropyStatistics>( m_entropyStatistics );
}

TEST_F( CTreeStatisticsTest_treedepth7_nlabels2, GiniStatIncreaseDecreaseTest )
{
    increase_decrease_test<CTreeSplitGiniStatistics>( m_giniStatistics );
}

// ----------------------------------------------------------------------------


TEST_F( CTreeStatisticsTest_treedepth5_nlabels11, EntropyStatIncreaseDecreaseTest )
{
    increase_decrease_test<CTreeSplitEntropyStatistics>( m_entropyStatistics );
}

TEST_F( CTreeStatisticsTest_treedepth5_nlabels11, GiniStatIncreaseDecreaseTest )
{
    increase_decrease_test<CTreeSplitGiniStatistics>( m_giniStatistics );
}


// ----------------------------------------------------------------------------------------------------

template <int TREEDEPTH, int NUM_LABELS>
void CTreeStatisticsTest<TREEDEPTH,NUM_LABELS>::SetUp()
{      
    generate_sample_dataset( m_data );
    generate_statistics( m_data );
}


// ----------------------------------------------------------------------------------------------------

template <int TREEDEPTH, int NUM_LABELS>
template <typename split_stat_T>
void CTreeStatisticsTest<TREEDEPTH,NUM_LABELS>::increase_decrease_test( const split_stat_T& f_stat ) const
{
    /// initialize empty left statistic
    split_stat_T l_leftStat( m_numLabels_i32, m_numLeafs_i32 );
    l_leftStat.initialize_zero();
    ASSERT_TRUE( l_leftStat.assert_correctness() );

    /// initialize full right statistic
    split_stat_T l_rightStat( m_numLabels_i32, m_numLeafs_i32 );
    l_rightStat.initialize_from( f_stat );
    ASSERT_TRUE( l_rightStat.assert_correctness() );

    /// Test Case 1: Initialize node statistic values:
    {
        /// Empty statistics should have zero rating
        EXPECT_EQ( 0, l_leftStat.total_rating() );

        /// Expect same rating for orginial and copied statistic
        EXPECT_EQ( f_stat.total_rating(), l_rightStat.total_rating() );

        // Comparisons on node level
        for (int32_t nIdx = 0; nIdx < m_numLeafs_i32; ++nIdx)
        {
            /// Empty statistics should have zero rating
            EXPECT_EQ( 0, l_leftStat.node_rating( nIdx ) );

            /// Expect same rating for orginial and copied statistic
            EXPECT_EQ( f_stat.node_rating( nIdx ), l_rightStat.node_rating( nIdx ) );
        }
    }

    /// Test Case 2: Increase / Decrease
    {
        auto leafIt = m_data.m_leafIdx_ai32.begin();
        auto labelIt = m_data.m_labelIdx_ai32.begin();
        auto weightIt = m_data.m_weights_ai32.begin();

        for ( ; leafIt != m_data.m_leafIdx_ai32.end(); 
                ++leafIt, ++labelIt, ++weightIt)
        {
            const auto l_leftIncreaseDiff = l_leftStat.calc_increase_rating( *leafIt, *labelIt, *weightIt );
            const auto l_rightDecreaseDiff = l_rightStat.calc_decrease_rating( *leafIt, *labelIt, *weightIt );
            
            /// statistic ratings BEFORE increase/decrease
            const auto l_leftPreIncreaseRating = l_leftStat.total_rating();
            const auto l_rightPreDecreaseRating = l_rightStat.total_rating();

            /// actual increase/decrease
            l_leftStat.increase_counts( *leafIt, *labelIt, *weightIt );
            ASSERT_TRUE( l_leftStat.assert_correctness() );
            l_rightStat.decrease_counts( *leafIt, *labelIt, *weightIt );
            ASSERT_TRUE( l_rightStat.assert_correctness() );

            /// statistic ratings AFTER increase/decrease
            const auto l_leftPostIncreaseRating = l_leftStat.total_rating();
            const auto l_rightPostDecreaseRating = l_rightStat.total_rating();

            /// tests
            if ( std::is_floating_point<decltype(l_leftPostIncreaseRating)>::value )
            {
                {
                    //EXPECT_FLOAT_EQ( l_leftPreIncreaseRating + l_leftIncreaseDiff, l_leftPostIncreaseRating );
                    auto l_leftError = std::abs( l_leftPreIncreaseRating + l_leftIncreaseDiff - l_leftPostIncreaseRating );
                    auto l_leftTol   = std::max( std::abs(l_leftPreIncreaseRating + l_leftIncreaseDiff), 
                                                 std::abs(l_leftPostIncreaseRating) );
                    
                    if (l_leftTol > 1e-2f)
                    {
                        EXPECT_LE( l_leftError, 1e-5f * l_leftTol );
                    }
                }

                {
                    //EXPECT_FLOAT_EQ( l_rightPreDecreaseRating + l_rightDecreaseDiff, l_rightPostDecreaseRating );
                    auto l_rightError = std::abs( l_rightPreDecreaseRating + l_rightDecreaseDiff - l_rightPostDecreaseRating );
                    auto l_rightTol   = std::max( std::abs(l_rightPreDecreaseRating + l_rightDecreaseDiff), 
                                                  std::abs(l_rightPostDecreaseRating) );
                    
                    if (l_rightTol > 1e-2f)
                    {
                        EXPECT_LE( l_rightError, 1e-5f * l_rightTol );
                    }
                }
            }
            else
            {
                EXPECT_EQ( l_leftPreIncreaseRating + l_leftIncreaseDiff, l_leftPostIncreaseRating );
                EXPECT_EQ( l_rightPreDecreaseRating + l_rightDecreaseDiff, l_rightPostDecreaseRating );
            }
        }
    }

    /// Test Case 3: Final node statistic values
    {
        /// Empty statistics should have zero rating
        ASSERT_TRUE( l_rightStat.assert_correctness() );
        EXPECT_EQ( 0, l_rightStat.total_rating() );

        /// Expect same rating for orginial and copied statistic
        ASSERT_TRUE( l_leftStat.assert_correctness() );
        if ( std::is_floating_point<decltype(l_leftStat.total_rating())>::value )
        {
            EXPECT_FLOAT_EQ( f_stat.total_rating(), l_leftStat.total_rating() );
        }
        else
        {
            EXPECT_EQ( f_stat.total_rating(), l_leftStat.total_rating() );
        }

        // Comparisons on node level
        for (int32_t nIdx = 0; nIdx < m_numLeafs_i32; ++nIdx)
        {
            /// Empty statistics should have zero rating
            EXPECT_EQ( 0, l_rightStat.node_rating( nIdx ) );

            /// Expect same rating for orginial and copied statistic
            if ( std::is_floating_point<decltype(l_leftStat.node_rating( nIdx ))>::value )
            {
                const auto l_statVal = f_stat.node_rating( nIdx );
                const auto l_leftVal = l_leftStat.node_rating( nIdx );
                const auto l_error = std::abs( l_statVal - l_leftVal );
                const auto l_tol = std::max( std::abs(l_statVal), std::abs( l_leftVal) ); 
                EXPECT_LE( l_error, 1e-4f * l_tol );
            }
            else
            {
                EXPECT_EQ( f_stat.node_rating( nIdx ), l_leftStat.node_rating( nIdx ) );
            }
        }
    }
}


// ----------------------------------------------------------------------------------------------------


template <int TREEDEPTH, int NUM_LABELS>
void CTreeStatisticsTest<TREEDEPTH,NUM_LABELS>::generate_sample_dataset( CDataSet& dset )
{
    ASSERT_EQ( dset.m_leafIdx_ai32.size(), dset.m_weights_ai32.size() );
    ASSERT_EQ( dset.m_labelIdx_ai32.size(), dset.m_weights_ai32.size() );
    
    // Leaf Distribution
    std::uniform_int_distribution<int32_t> l_leafDistr(0, m_numLeafs_i32 - 1 );
    // Label Distribution
    std::uniform_int_distribution<int32_t> l_labelDistr(0, m_numLabels_i32 - 1 );
    // Weight Distribution
    std::uniform_int_distribution<int32_t> l_weightDistr(0, m_maxWeight_i32 - 1 );
    
    auto leafIt = dset.m_leafIdx_ai32.begin();
    auto labelIt = dset.m_labelIdx_ai32.begin();
    auto weightIt = dset.m_weights_ai32.begin();

    for ( ; leafIt != dset.m_leafIdx_ai32.end(); 
            ++leafIt, ++labelIt, ++weightIt)
    {
        *leafIt  = l_leafDistr( m_rand );
        *labelIt = l_labelDistr( m_rand );
        *weightIt = l_weightDistr( m_rand );
    }
}


template <int TREEDEPTH, int NUM_LABELS>
void CTreeStatisticsTest<TREEDEPTH,NUM_LABELS>::generate_statistics( const CDataSet& dset )
{
    ASSERT_EQ( dset.m_leafIdx_ai32.size(), dset.m_weights_ai32.size() );
    ASSERT_EQ( dset.m_labelIdx_ai32.size(), dset.m_weights_ai32.size() );
    
    /// generate entropy statistics
    {
        const auto labelEnd = m_entropyStatistics.initialize_from(
                            nodeIdx_t::cast_from( dset.m_leafIdx_ai32.begin() ), 
                            nodeIdx_t::cast_from( dset.m_leafIdx_ai32.end() ),
                            dset.m_labelIdx_ai32.begin(),
                            dset.m_weights_ai32.begin() );
        ASSERT_EQ( labelEnd, dset.m_labelIdx_ai32.end() );
    }

    /// generate gini statistics
    {
        const auto labelEnd = m_giniStatistics.initialize_from(
                            nodeIdx_t::cast_from( dset.m_leafIdx_ai32.begin() ), 
                            nodeIdx_t::cast_from( dset.m_leafIdx_ai32.end() ),
                            dset.m_labelIdx_ai32.begin(),
                            dset.m_weights_ai32.begin() );
        ASSERT_EQ( labelEnd, dset.m_labelIdx_ai32.end() );
    }
}

// ----------------------------------------------------------------------------------------------------



