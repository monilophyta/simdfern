/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>
#include "test_integral_image_base.hpp"

#include <image.hpp>


using namespace img;




class CIntegralImageTest : public test::CIntegralImageTestBase
{};




TEST_F( CIntegralImageTest, TestIntegalDecomposition )
{
    static const box_i32_t l_centerBox( 0,0,1,1 );
    
    const image_frame_t l_intImgFrame = m_intImgFrame.convert_to_integral_image_frame();

    sliding_window_t imgIt, intIt;
    sliding_window_t imgEnd, intEnd;

    std::tie( imgIt, imgEnd ) = m_imgFrame.sliding_window();
    std::tie( intIt, intEnd ) = l_intImgFrame.sliding_window();
    
    const intbox_i32_t l_intBox = l_intImgFrame.offset_free_intbox( l_centerBox );
    //std::cout << "intBox: " << l_intBox.to_string() << std::endl;

    for (; (imgIt != imgEnd) && (intIt != intEnd);
            ++imgIt, ++intIt )
    {
        //std::cout << "sliding img: " << imgIt.to_string() << std::endl;
        //std::cout << "sliding int: " << intIt.to_string() << std::endl;
        
        ASSERT_EQ( imgIt.colIdx(), intIt.colIdx() );
        ASSERT_EQ( imgIt.rowIdx(), intIt.rowIdx() );
        
        int32_t l_intOffset = l_intImgFrame.anker_offset( intIt );
        //std::cout << "l_intOffset = " << l_intOffset << std::endl;
        int32_t l_intVal = l_intBox.evaluate( m_intImg_p, l_intOffset );
        ASSERT_GE( l_intVal, 0 );

        int32_t l_imgOffset = m_imgFrame.anker_offset( imgIt );
        int32_t l_imgVal = m_img_p[ l_imgOffset ];

        // image pixel value should have been reconstructed
        ASSERT_EQ( l_imgVal, l_intVal );
    }

    ASSERT_EQ( intIt, intEnd );
    ASSERT_EQ( imgIt, imgEnd );
}


