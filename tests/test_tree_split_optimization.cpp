/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include <gtest/gtest.h>
#include <random>
#include <cmath>
#include <array>
#include <numeric>
#include <algorithm>
#include <unordered_set>

#include <simd.hpp>
#include <image.hpp>

#define private public
#define protected public
#define class struct

#include <tree.hpp>

#undef private
#undef protected
#undef class


// * TODO: evaluate split results
//    * label sets of siblings should be disjunct
// * TODO: leave one node out of distribution => should not case any negativ impact


using namespace tree;
using namespace smd;


template <int TREEDEPTH, int NUM_LABELS>
class CSplitOptimizationTest : public ::testing::Test
{
protected:

    const unsigned int m_randomSeed_u = 69;

    const int32_t m_maxWeight_i32 = 179;
    
    // standard deviation of sampling from class clusters
    const float32_t m_sampleStd_f32 = 1.f;

    // already reached depth of trained tree
    static constexpr int32_t m_treeDepth_i32 = TREEDEPTH;

    static constexpr int32_t m_numLabels_i32 = NUM_LABELS;
    
    const int32_t m_numLeafs_i32 = std::pow( 2, m_treeDepth_i32-1 );
    const int32_t m_numData_i32    = 1677; //100001;
    const int32_t m_numInfData_i32 = 17;
    const int32_t m_numNaNData_i32 = 13;

    // -----------------------------------------------------------------------

private:

    std::default_random_engine m_rand;

    std::array<float32_t,m_numLabels_i32>  m_centroids_af32;

protected:

    // -----------------------------------------------------------------------

    typedef std::vector<float32_t> feature_array_t;
    typedef std::vector<int32_t>   index_array_t;
    typedef std::vector<nodeIdx_t> node_index_array_t;
    typedef std::vector<int32_t>   weight_array_t;

    struct CDataSet
    {
        feature_array_t      m_features_af32;
        feature_array_t      m_rounded_features_af32;
        index_array_t        m_labelIdx_ai32;
        node_index_array_t   m_nodeIdx_a;
        weight_array_t       m_weights_ai32;
        

        inline void resize( int32_t f_numData_i32 )
        {
            m_features_af32.resize( f_numData_i32 );
            m_labelIdx_ai32.resize( f_numData_i32 );
            m_rounded_features_af32.resize( f_numData_i32 );
            m_nodeIdx_a.resize( f_numData_i32 );
            m_weights_ai32.resize( f_numData_i32 );
        }

        inline void clear()
        {
            m_features_af32.clear();
            m_rounded_features_af32.clear();
            m_labelIdx_ai32.clear();
            m_nodeIdx_a.clear();
            m_weights_ai32.clear();
        }
    };

    CDataSet                    m_usorted;
    CDataSet                    m_sorted;

    CTreeSplitEntropyStatistics m_parentEntropyStatistics;
    CTreeSplitGiniStatistics    m_parentGiniStatistics;

    // -----------------------------------------------------------------------


    virtual void SetUp();


    virtual void TearDown()
    {
        m_usorted.clear();
        m_sorted.clear();
    }


    void gini_vs_entropy_comparison_test( const CDataSet& dset, bool sorted ) const;
    
    template <typename optimizer_T>
    void sorted_vs_usorted_comparison_test() const;


    template <typename optimizer_T>
    void sorted_vs_usorted_comparison_test( const typename optimizer_T::split_stat_t& f_parentStatistics ) const;

private:

    void generate_centroids();
    void generate_sample_dataset( CDataSet& dset );
    void generate_sample_nodeIdx( CDataSet& dset );

    void sort_dataset( CDataSet& tdset, const CDataSet& sdset );

    void generate_parent_statistics( const CDataSet& dset );

public:

    CSplitOptimizationTest()
        : m_rand( m_randomSeed_u )
        , m_parentEntropyStatistics( m_numLabels_i32, m_numLeafs_i32 )
        , m_parentGiniStatistics( m_numLabels_i32, m_numLeafs_i32 )
    {
        m_parentEntropyStatistics.initialize_zero();
        m_parentGiniStatistics.initialize_zero();
    }

};

// ----------------------------------------------------------------------------------------------------

typedef CSplitOptimizationTest<1,2>  CSplitOptimizationTest_treedepth1_nlabels2;
typedef CSplitOptimizationTest<2,2>  CSplitOptimizationTest_treedepth2_nlabels2;
typedef CSplitOptimizationTest<7,2>  CSplitOptimizationTest_treedepth7_nlabels2;
typedef CSplitOptimizationTest<5,11> CSplitOptimizationTest_treedepth5_nlabels11;

// ----------------------------------------------------------------------------------------------------



TEST_F( CSplitOptimizationTest_treedepth1_nlabels2, SortedGiniVsEntropyComparison )
{
    gini_vs_entropy_comparison_test( m_sorted, true );
}


TEST_F( CSplitOptimizationTest_treedepth1_nlabels2, UnsortedGiniVsEntropyComparison )
{
    gini_vs_entropy_comparison_test( m_usorted, false );
}


TEST_F( CSplitOptimizationTest_treedepth1_nlabels2, SortedVsUnsortedEntropyProcessingComparison )
{
    sorted_vs_usorted_comparison_test<split_entropy_optimizer_t>( m_parentEntropyStatistics );
}

TEST_F( CSplitOptimizationTest_treedepth1_nlabels2, SortedVsUnsortedGiniProcessingComparison )
{
    sorted_vs_usorted_comparison_test<split_gini_optimizer_t>( m_parentGiniStatistics );
}

//---------------------------------------------------------------------------------------------


TEST_F( CSplitOptimizationTest_treedepth2_nlabels2, SortedGiniVsEntropyComparison )
{
    gini_vs_entropy_comparison_test( m_sorted, true );
}


TEST_F( CSplitOptimizationTest_treedepth2_nlabels2, UnsortedGiniVsEntropyComparison )
{
    gini_vs_entropy_comparison_test( m_usorted, false );
}


TEST_F( CSplitOptimizationTest_treedepth2_nlabels2, SortedVsUnsortedEntropyProcessingComparison )
{
    sorted_vs_usorted_comparison_test<split_entropy_optimizer_t>( m_parentEntropyStatistics );
}

TEST_F( CSplitOptimizationTest_treedepth2_nlabels2, SortedVsUnsortedGiniProcessingComparison )
{
    sorted_vs_usorted_comparison_test<split_gini_optimizer_t>( m_parentGiniStatistics );
}

//---------------------------------------------------------------------------------------------

TEST_F( CSplitOptimizationTest_treedepth7_nlabels2, SortedGiniVsEntropyComparison )
{
    gini_vs_entropy_comparison_test( m_sorted, true );
}


TEST_F( CSplitOptimizationTest_treedepth7_nlabels2, UnsortedGiniVsEntropyComparison )
{
    gini_vs_entropy_comparison_test( m_usorted, false );
}


TEST_F( CSplitOptimizationTest_treedepth7_nlabels2, SortedVsUnsortedEntropyProcessingComparison )
{
    sorted_vs_usorted_comparison_test<split_entropy_optimizer_t>( m_parentEntropyStatistics );
}


TEST_F( CSplitOptimizationTest_treedepth7_nlabels2, SortedVsUnsortedGiniProcessingComparison )
{
    sorted_vs_usorted_comparison_test<split_gini_optimizer_t>( m_parentGiniStatistics );
}

// ----------------------------------------------------------------------------------------------------



TEST_F( CSplitOptimizationTest_treedepth5_nlabels11, SortedVsUnsortedEntropyProcessingComparison )
{
    sorted_vs_usorted_comparison_test<split_entropy_optimizer_t>( m_parentEntropyStatistics );
}


TEST_F( CSplitOptimizationTest_treedepth5_nlabels11, SortedVsUnsortedGiniProcessingComparison )
{
    sorted_vs_usorted_comparison_test<split_gini_optimizer_t>( m_parentGiniStatistics );
}

// ----------------------------------------------------------------------------------------------------




template <int TREEDEPTH, int NUM_LABELS>
void CSplitOptimizationTest<TREEDEPTH,NUM_LABELS>::gini_vs_entropy_comparison_test( const CDataSet& dset, bool sorted ) const
{
    ASSERT_EQ( 2, NUM_LABELS );  // This Test only garanties correct results for num_labels == 2
    
    // testobject
    split_entropy_optimizer_t entropySplitOptimizer( m_numLabels_i32, m_numLeafs_i32 );
    split_gini_optimizer_t    giniSplitOptimizer( m_numLabels_i32, m_numLeafs_i32 );

    // Initialize node ratings
    entropySplitOptimizer.initialize_node_ratings( m_parentEntropyStatistics );
    giniSplitOptimizer.initialize_node_ratings( m_parentGiniStatistics );
    
    const int32_t l_featureIdx1_i32 = 0;

    ASSERT_EQ( static_cast<int32_t>( dset.m_features_af32.size()), m_numData_i32 );
    ASSERT_EQ( static_cast<int32_t>( dset.m_labelIdx_ai32.size()), m_numData_i32 );
    ASSERT_EQ( static_cast<int32_t>( dset.m_nodeIdx_a.size()),  m_numData_i32 );
    ASSERT_EQ( static_cast<int32_t>( dset.m_weights_ai32.size()),  m_numData_i32 );

    /// Initialize parent-statistc
    {
        
        entropySplitOptimizer.initialize_feature_run( l_featureIdx1_i32, m_parentEntropyStatistics );

        ASSERT_TRUE( entropySplitOptimizer.m_leftStat.assert_correctness() );
        ASSERT_TRUE( entropySplitOptimizer.m_rightStat.assert_correctness() );

        giniSplitOptimizer.initialize_feature_run( l_featureIdx1_i32, m_parentGiniStatistics );

        ASSERT_TRUE( giniSplitOptimizer.m_leftStat.assert_correctness() );
        ASSERT_TRUE( giniSplitOptimizer.m_rightStat.assert_correctness() );
    }

    /// process feature 
    {
        if (true == sorted)
        {
            entropySplitOptimizer.process_sorted_feature( dset.m_features_af32.data(),
                                                          dset.m_features_af32.data() + dset.m_features_af32.size(),
                                                          dset.m_labelIdx_ai32.data(),
                                                          dset.m_nodeIdx_a.data(),
                                                          dset.m_weights_ai32.data() );
        
            giniSplitOptimizer.process_sorted_feature( dset.m_features_af32.data(),
                                                       dset.m_features_af32.data() + dset.m_features_af32.size(),
                                                       dset.m_labelIdx_ai32.data(),
                                                       dset.m_nodeIdx_a.data(),
                                                       dset.m_weights_ai32.data() );
        }
        else
        {
            entropySplitOptimizer.process_unsorted_feature( dset.m_features_af32.data(),
                                                            dset.m_features_af32.data() + dset.m_features_af32.size(),
                                                            dset.m_labelIdx_ai32.data(),
                                                            dset.m_nodeIdx_a.data(),
                                                            dset.m_weights_ai32.data() );
        
            giniSplitOptimizer.process_unsorted_feature( dset.m_features_af32.data(),
                                                         dset.m_features_af32.data() + dset.m_features_af32.size(),
                                                         dset.m_labelIdx_ai32.data(),
                                                         dset.m_nodeIdx_a.data(),
                                                         dset.m_weights_ai32.data() );
        }
        
        ASSERT_TRUE( entropySplitOptimizer.m_leftStat.assert_correctness() );
        ASSERT_TRUE( entropySplitOptimizer.m_rightStat.assert_correctness() );
        
        ASSERT_TRUE( giniSplitOptimizer.m_leftStat.assert_correctness() );
        ASSERT_TRUE( giniSplitOptimizer.m_rightStat.assert_correctness() );
    }

    /// Compare Splits
    {
        auto giniIt = giniSplitOptimizer.m_nodeStates.cbegin();
        auto entrIt = entropySplitOptimizer.m_nodeStates.cbegin();
        
        for (; (giniIt != giniSplitOptimizer.m_nodeStates.cend()) && (entrIt != entropySplitOptimizer.m_nodeStates.cend());
               ++giniIt, ++entrIt )
        {
            ASSERT_NE( giniIt, giniSplitOptimizer.m_nodeStates.cend() );
            ASSERT_NE( entrIt, entropySplitOptimizer.m_nodeStates.cend() );

            if ( false == std::isfinite(giniIt->optimal().m_featureSplit_f32) )
            {
                ASSERT_FALSE( std::isfinite(entrIt->optimal().m_featureSplit_f32) );
            }
            else
            {
                EXPECT_EQ( giniIt->optimal().m_featureSplit_f32, entrIt->optimal().m_featureSplit_f32 );
            }
        }

        ASSERT_EQ( giniIt,    giniSplitOptimizer.m_nodeStates.cend() );
        ASSERT_EQ( entrIt, entropySplitOptimizer.m_nodeStates.cend() );
    }
}



template <int TREEDEPTH, int NUM_LABELS>
template <typename optimizer_T>
void 
CSplitOptimizationTest<TREEDEPTH,NUM_LABELS>::sorted_vs_usorted_comparison_test(
                            const typename optimizer_T::split_stat_t& f_parentStatistics ) const
{
    optimizer_T l_sortedOptimizer( m_numLabels_i32, m_numLeafs_i32 );
    optimizer_T l_usortedOptimizer( m_numLabels_i32, m_numLeafs_i32 );

    // Initialize node ratings
    l_sortedOptimizer.initialize_node_ratings( f_parentStatistics );
    l_usortedOptimizer.initialize_node_ratings( f_parentStatistics );


    const int32_t l_featureIdx1_i32 = 0;

    ASSERT_EQ( static_cast<int32_t>( m_sorted.m_features_af32.size()), m_numData_i32 );
    ASSERT_EQ( static_cast<int32_t>( m_sorted.m_labelIdx_ai32.size()), m_numData_i32 );
    ASSERT_EQ( static_cast<int32_t>( m_sorted.m_nodeIdx_a.size()),     m_numData_i32 );
    ASSERT_EQ( static_cast<int32_t>( m_sorted.m_weights_ai32.size()),  m_numData_i32 );

    ASSERT_EQ( static_cast<int32_t>( m_usorted.m_features_af32.size()), m_numData_i32 );
    ASSERT_EQ( static_cast<int32_t>( m_usorted.m_labelIdx_ai32.size()), m_numData_i32 );
    ASSERT_EQ( static_cast<int32_t>( m_usorted.m_nodeIdx_a.size()),     m_numData_i32 );
    ASSERT_EQ( static_cast<int32_t>( m_usorted.m_weights_ai32.size()),  m_numData_i32 );

    /// Initialize parent-statistc
    {
        ASSERT_TRUE( f_parentStatistics.assert_correctness() );
        
        l_sortedOptimizer.initialize_feature_run( l_featureIdx1_i32, f_parentStatistics );

        ASSERT_TRUE( l_sortedOptimizer.m_leftStat.assert_correctness() );
        ASSERT_TRUE( l_sortedOptimizer.m_rightStat.assert_correctness() );

        l_usortedOptimizer.initialize_feature_run( l_featureIdx1_i32, f_parentStatistics );

        ASSERT_TRUE( l_usortedOptimizer.m_leftStat.assert_correctness() );
        ASSERT_TRUE( l_usortedOptimizer.m_rightStat.assert_correctness() );
    }

    /// process feature 
    {
        l_sortedOptimizer.process_sorted_feature( 
                    m_sorted.m_features_af32.data(),
                    m_sorted.m_features_af32.data() + m_sorted.m_features_af32.size(),
                    m_sorted.m_labelIdx_ai32.data(),
                    m_sorted.m_nodeIdx_a.data(),
                    m_sorted.m_weights_ai32.data() );
    
        l_usortedOptimizer.process_unsorted_feature(
                    m_usorted.m_features_af32.data(),
                    m_usorted.m_features_af32.data() + m_usorted.m_features_af32.size(),
                    m_usorted.m_labelIdx_ai32.data(),
                    m_usorted.m_nodeIdx_a.data(),
                    m_usorted.m_weights_ai32.data() );
        
        ASSERT_TRUE( l_sortedOptimizer.m_leftStat.assert_correctness() );
        ASSERT_TRUE( l_sortedOptimizer.m_rightStat.assert_correctness() );
        
        ASSERT_TRUE( l_usortedOptimizer.m_leftStat.assert_correctness() );
        ASSERT_TRUE( l_usortedOptimizer.m_rightStat.assert_correctness() );
    }

    /// Compare Splits
    {
        auto sortIt  = l_sortedOptimizer.m_nodeStates.cbegin();
        auto usortIt = l_usortedOptimizer.m_nodeStates.cbegin();
        
        for (; (sortIt != l_sortedOptimizer.m_nodeStates.cend()) && (usortIt != l_usortedOptimizer.m_nodeStates.cend());
               ++sortIt, ++usortIt )
        {
            ASSERT_NE( sortIt,   l_sortedOptimizer.m_nodeStates.cend() );
            ASSERT_NE( usortIt, l_usortedOptimizer.m_nodeStates.cend() );

            if ( false == std::isfinite( sortIt->optimal().m_featureSplit_f32 ) )
            {
                ASSERT_FALSE( std::isfinite( usortIt->optimal().m_featureSplit_f32 ) );
            }
            else
            {
                EXPECT_EQ( sortIt->optimal().m_featureSplit_f32, usortIt->optimal().m_featureSplit_f32 );
            }
        }

        ASSERT_EQ(  sortIt,  l_sortedOptimizer.m_nodeStates.cend() );
        ASSERT_EQ( usortIt, l_usortedOptimizer.m_nodeStates.cend() );
    }
}


// ----------------------------------------------------------------------------------------------------

template <int TREEDEPTH, int NUM_LABELS>
void CSplitOptimizationTest<TREEDEPTH,NUM_LABELS>::SetUp()
{      
    m_usorted.resize( m_numData_i32 );
    m_sorted.resize( m_numData_i32 );

    generate_centroids();
    
    generate_sample_dataset( m_usorted );
    generate_sample_nodeIdx( m_usorted );

    sort_dataset( m_sorted, m_usorted );

    generate_parent_statistics( m_usorted );

    // fill rounded features
    std::transform( m_usorted.m_features_af32.cbegin(), m_usorted.m_features_af32.cend(),
                    m_usorted.m_rounded_features_af32.begin(),
                    std::roundf );
    
    std::transform( m_sorted.m_features_af32.cbegin(), m_sorted.m_features_af32.cend(),
                    m_sorted.m_rounded_features_af32.begin(),
                    std::roundf );
}


// ----------------------------------------------------------------------------------------------------



template <int TREEDEPTH, int NUM_LABELS>
void CSplitOptimizationTest<TREEDEPTH,NUM_LABELS>::generate_centroids()
{
    const float32_t l_stdprogress_f32 = 100;
    float32_t l_Centroid_f32 = 0.f;

    for ( auto it = m_centroids_af32.begin();
               it != m_centroids_af32.end(); 
               ++it )
    {
        *it = l_Centroid_f32;

        l_Centroid_f32 += l_stdprogress_f32 * m_sampleStd_f32;
    }
}



template <int TREEDEPTH, int NUM_LABELS>
void CSplitOptimizationTest<TREEDEPTH,NUM_LABELS>::generate_sample_dataset( CDataSet& dset )
{
    // Label Distribution
    std::uniform_int_distribution<int32_t> l_labelDistr(0, m_numLabels_i32 - 1 );

    // Weight Distribution
    std::uniform_int_distribution<int32_t> l_weightDistr( 1, m_maxWeight_i32 );

    // class distributions
    typedef std::normal_distribution<float32_t> cdistr_t;
    std::vector<cdistr_t> l_classDistr_a( m_numLabels_i32 );
    {
        auto cit = m_centroids_af32.begin();
        for ( auto dit = l_classDistr_a.begin();
                dit != l_classDistr_a.end();
                ++dit, ++cit )
        {
            *dit = cdistr_t( *cit, m_sampleStd_f32 );
        }
    }

    // data sampling
    dset.m_weights_ai32.resize( m_numData_i32 );
    auto weight_sampler_func = [&]() { return l_weightDistr( m_rand ); };
    std::generate( dset.m_weights_ai32.begin(), dset.m_weights_ai32.end(), weight_sampler_func );

    dset.m_features_af32.resize( m_numData_i32 );
    dset.m_labelIdx_ai32.resize( m_numData_i32 );
    {
        auto labelIt = dset.m_labelIdx_ai32.begin();

        // initialize counters for NaN and inf
        int32_t l_numInfData_i32 = m_numInfData_i32;
        int32_t l_numNaNData_i32 = m_numNaNData_i32;

        for ( auto fIt  = dset.m_features_af32.begin();
                   fIt != dset.m_features_af32.end();
              ++fIt, ++labelIt )
        {
            // label sampling
            *labelIt = l_labelDistr( m_rand );

            // select class distribution
            cdistr_t & l_cldistr_r = l_classDistr_a.at(*labelIt);

            // feature sampling
            float32_t l_sampledValue = l_cldistr_r( m_rand );

            // check if value should be set to inf
            const bool l_setInf_b = (l_numInfData_i32 > 0) && 
                                     std::bernoulli_distribution( double(l_numInfData_i32) / 
                                           double(dset.m_features_af32.end() - fIt) )(m_rand);

            if (true == l_setInf_b)
            {
                l_numInfData_i32--;
                l_sampledValue = std::numeric_limits<float32_t>::infinity();
            }
            else
            {
                
                const bool l_setNaN_b = (l_numNaNData_i32 > 0) && 
                                           std::bernoulli_distribution( double(l_numNaNData_i32) / 
                                           double(dset.m_features_af32.end() - fIt) )(m_rand);

                if (true == l_setNaN_b)
                {
                    l_numNaNData_i32--;
                    l_sampledValue = std::numeric_limits<float32_t>::quiet_NaN();
                }
            }
            
            *fIt = l_sampledValue;
        }

        ASSERT_EQ( 0, l_numNaNData_i32 );
        ASSERT_EQ( 0, l_numNaNData_i32 );
    }
}



template <int TREEDEPTH, int NUM_LABELS>
void CSplitOptimizationTest<TREEDEPTH,NUM_LABELS>::generate_sample_nodeIdx( CDataSet& dset )
{

    if (m_numLeafs_i32 == 1)
    {
        std::fill( dset.m_nodeIdx_a.begin(), dset.m_nodeIdx_a.end(), nodeIdx_t( 0 ) );
    }
    else
    {
        // Leaf Distribution
        std::uniform_int_distribution<int32_t> l_nodeDistr(0, m_numLeafs_i32 - 1 );

        // node sampling
        for ( auto lIt = dset.m_nodeIdx_a.begin();
                lIt != dset.m_nodeIdx_a.end();
                ++lIt )
        {
            // node sampling
            *lIt = nodeIdx_t( l_nodeDistr( m_rand ) );
        }
    }
}


// ----------------------------------------------------------------------------------------------------


template <int TREEDEPTH, int NUM_LABELS>
void CSplitOptimizationTest<TREEDEPTH,NUM_LABELS>::sort_dataset( CDataSet& tdset, const CDataSet& sdset )
{
    const int32_t l_numData_i32 = sdset.m_features_af32.size();
    
    // sorting features
    index_array_t l_sortedDataIdx_ai32( l_numData_i32 );
    {
        std::iota( l_sortedDataIdx_ai32.begin(), l_sortedDataIdx_ai32.end(), 0 );
        
        // more complicate comparison kernel also copes with NaN values
        // These value are accumlated at the end of the sequence
        // Comparisons using >= or <= resultet in segmentation faults
        auto sortKernel = [&sdset]( int32_t i1, int32_t i2) -> bool {
            return ( (sdset.m_features_af32[i1] < sdset.m_features_af32[i2]) ||
                     (std::isnan(sdset.m_features_af32[i2]) && (!std::isnan(sdset.m_features_af32[i1]))) ); };

        std::sort( l_sortedDataIdx_ai32.begin(), l_sortedDataIdx_ai32.end(), sortKernel );  
    }

    // apply sorting
    tdset.m_features_af32.resize( l_numData_i32 );
    tdset.m_labelIdx_ai32.resize( l_numData_i32 );
    tdset.m_nodeIdx_a.resize( l_numData_i32 );
    tdset.m_weights_ai32.resize( l_numData_i32 );
    
    std::transform( l_sortedDataIdx_ai32.cbegin(), l_sortedDataIdx_ai32.cend(), 
                    tdset.m_features_af32.begin(),
                    [&sdset]( int32_t idx ) -> float32_t { return sdset.m_features_af32[idx]; } );
    
    std::transform( l_sortedDataIdx_ai32.cbegin(), l_sortedDataIdx_ai32.cend(), 
                    tdset.m_labelIdx_ai32.begin(),
                    [&sdset]( int32_t idx ) -> int32_t { return sdset.m_labelIdx_ai32[idx]; } );

    std::transform( l_sortedDataIdx_ai32.cbegin(), l_sortedDataIdx_ai32.cend(), 
                    tdset.m_nodeIdx_a.begin(),
                    [&sdset]( int32_t idx ) -> nodeIdx_t { return sdset.m_nodeIdx_a[idx]; } );
    
    std::transform( l_sortedDataIdx_ai32.cbegin(), l_sortedDataIdx_ai32.cend(), 
                    tdset.m_weights_ai32.begin(),
                    [&sdset]( int32_t idx ) -> int32_t { return sdset.m_weights_ai32[idx]; } );
}


// ----------------------------------------------------------------------------------------------------


template <int TREEDEPTH, int NUM_LABELS>
void CSplitOptimizationTest<TREEDEPTH,NUM_LABELS>::generate_parent_statistics( const CDataSet& dset )
{
    /// instantiate auxiliary statistic holder
    CTreeSplitGiniStatistics l_auxStat( m_numLabels_i32, m_numLeafs_i32 );
    l_auxStat.initialize_zero();

    auto    labIt = dset.m_labelIdx_ai32.cbegin();
    auto weightIt = dset.m_weights_ai32.cbegin();

    for (auto nodeIt = dset.m_nodeIdx_a.cbegin();
              nodeIt != dset.m_nodeIdx_a.cend();
              ++nodeIt, ++labIt, ++weightIt )
    {
        l_auxStat.m_CorrelationCounts_ai32.elem( nodeIt->idx(), *labIt ) += *weightIt;
                   l_auxStat.m_Counts_ai32.elem( nodeIt->idx() )         += *weightIt;
    }


    // initialize fixture statistics
    m_parentEntropyStatistics.initialize_from( static_cast<CTreeSplitStatisticsBase&>( l_auxStat ) );
    ASSERT_TRUE( m_parentEntropyStatistics.assert_correctness() );
    m_parentGiniStatistics.initialize_from( static_cast<CTreeSplitStatisticsBase&>( l_auxStat ) );
    ASSERT_TRUE( m_parentGiniStatistics.assert_correctness() );
}

