/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>
#include <tree.hpp>
#include <random>
#include "test_integral_image_base.hpp"
#include <vector>
#include <algorithm>
#include <cmath>
#include <cassert>



typedef float float32_t;



class CBoxEvaluationTest : public test::CIntegralImageTestBase
{

protected:

    static constexpr int32_t num_v32boxes = 2;
    static constexpr int32_t num_i32Boxes = 3 + ( smd::vf32_t::size() * num_v32boxes );
    

    const int32_t m_patchSize = 21;

    
    virtual void SetUp()
    {
        test::CIntegralImageTestBase::SetUp();

        sampleI32Boxes();
        m_randomSeed_u = sampleV32Boxes();

        createEvaluator( m_i32BoxRatioEvaluator, m_i32Boxes, 1.f );
        createEvaluator( m_v32BoxRatioEvaluator, m_v32Boxes, 1.f );

        createEvaluator( m_i32BoxDiffEvaluator, m_i32Boxes, 1.f );
        createEvaluator( m_v32BoxDiffEvaluator, m_v32Boxes, 1.f );
    }

    virtual void TearDown()
    {
        test::CIntegralImageTestBase::TearDown();
    }


    // -----------------------------------------------

protected:

    std::vector<img::box_i32_t>    m_i32Boxes;
    std::vector<img::box_vi32_t>   m_v32Boxes;

    tree::boxratio_eval_i32_t      m_i32BoxRatioEvaluator;
    tree::boxratio_eval_vi32_t     m_v32BoxRatioEvaluator;

    tree::boxdiff_eval_i32_t       m_i32BoxDiffEvaluator;
    tree::boxdiff_eval_vi32_t      m_v32BoxDiffEvaluator;


    int sampleI32Boxes();
    int sampleV32Boxes();

    template <typename ET, typename BT>
    void createEvaluator( ET& f_BoxEvaluator, const BT& f_Boxes, float32_t f_featureParam );

    template <typename I32_EVAL_T, typename V32_EVAL_T>
    inline
    void evaluateConsistency( const I32_EVAL_T& f_i32BoxFeatureEvaluator, 
                              const V32_EVAL_T& f_v32BoxFeatureEvaluator,
                              const float32_t f_validFeatureValue_f32 );
};




TEST_F( CBoxEvaluationTest, TestBoxRatioEvaluationConsistency )
{
    evaluateConsistency( m_i32BoxRatioEvaluator, m_v32BoxRatioEvaluator, 1.f );
}

TEST_F( CBoxEvaluationTest, TestBoxDiffEvaluationConsistency )
{
    evaluateConsistency( m_i32BoxDiffEvaluator, m_v32BoxDiffEvaluator, 0.f );
}



template <typename I32_EVAL_T, typename V32_EVAL_T>
inline
void CBoxEvaluationTest::evaluateConsistency( const I32_EVAL_T& f_i32BoxFeatureEvaluator, 
                                              const V32_EVAL_T& f_v32BoxFeatureEvaluator,
                                              const float32_t f_validFeatureValue_f32 )
{
    const img::image_frame_t l_intImgFrame = m_intImgFrame.convert_to_integral_image_frame();

    img::sliding_window_t intIt, intEnd;
    std::tie( intIt, intEnd ) = l_intImgFrame.sliding_window( 5, 3 );
    
    // declare 
    std::vector<float32_t> l_i32FeatureOut( m_i32Boxes.size(), float32_t(0) );
    smd::array_vf32_t      l_v32FeatureOut( smd::vf32_t::size() * num_v32boxes );
    l_v32FeatureOut.set_zero();

    for (; (intIt != intEnd); ++intIt )
    {
        //std::cout << intIt.to_string() << std::endl;

        // initialize out arrays
        std::fill( l_i32FeatureOut.begin(), l_i32FeatureOut.end(), f_validFeatureValue_f32 - 1.f );
        l_v32FeatureOut.elem().fill( f_validFeatureValue_f32 - 1.f );
        
        // cacluate fundamental features
        {
            const auto endIter = f_i32BoxFeatureEvaluator.evaluate_location( *intIt, l_i32FeatureOut.begin() );
            ASSERT_EQ( endIter, l_i32FeatureOut.end() );
        }

        // cacluate simd features
        {
            const auto endIter = f_v32BoxFeatureEvaluator.evaluate_location( *intIt, l_v32FeatureOut.begin() );
            ASSERT_EQ( endIter, l_v32FeatureOut.end() );
        }

        // Evaluate i32 results
        for (auto it = l_i32FeatureOut.cbegin();
                  it != l_i32FeatureOut.cend();
                  ++it )
        {
            const bool isCorrectRatio = (*it == f_validFeatureValue_f32);
            const bool isInValid = (false == std::isfinite(*it));
            ASSERT_NE( isCorrectRatio, isInValid );
        }

        // Evaluate vi32 results
        auto iti32 = l_i32FeatureOut.cbegin();
        for (auto itv32 = l_v32FeatureOut.elem().cbegin();
                  itv32 != l_v32FeatureOut.elem().cend();
                  ++iti32, ++itv32 )
        {
            ASSERT_LE( iti32, l_i32FeatureOut.cend() );
            
            const bool isCorrectRatio = (*itv32 == f_validFeatureValue_f32);
            const bool isInValid = (false == std::isfinite(*itv32));
            //std::cout << "value: " << *itv32 << std::endl;
            if ( isCorrectRatio == isInValid )
            {
                // something went wrong!
                
                const size_t featureIdx = itv32 - l_v32FeatureOut.elem().cbegin();
                std::cout << "Test Value at " << intIt.rowIdx() << ";" << intIt.colIdx() << " feature = " << featureIdx << " : " << *itv32 << std::endl;
                std::cout << "Boxi32: " << m_i32Boxes[featureIdx].to_string() << std::endl;
                std::cout << "Boxv32: " << m_v32Boxes[ featureIdx  / smd::vi32_t::size() ].to_string() << std::endl;
                //std::cout << "Image Value: " << 
            }
            EXPECT_NE( isCorrectRatio, isInValid );

            if (false == isInValid)
            {
                ASSERT_EQ( *iti32, *itv32 );
            }
        }
    }
}



int CBoxEvaluationTest::sampleI32Boxes()
{
    const int32_t minPos = -(m_patchSize >> 1);
    const int32_t maxPos = (m_patchSize-1) >> 1;
    assert( (maxPos - minPos + 1) == m_patchSize );
    
    std::default_random_engine             l_rand( m_randomSeed_u );
    std::uniform_int_distribution<int32_t> l_equalDistr( minPos, maxPos );
    
    m_i32Boxes.reserve( num_i32Boxes );

    for (int i = 0; i < num_i32Boxes; ++i)
    {
        int32_t rPos = l_equalDistr( l_rand );
        int32_t cPos = l_equalDistr( l_rand );

        m_i32Boxes.emplace_back( cPos, rPos, cPos+1, rPos+1 );
    }

    return static_cast<int>( l_equalDistr( l_rand ) );
}



int CBoxEvaluationTest::sampleV32Boxes()
{
    const int32_t minPos = -(m_patchSize >> 1);
    const int32_t maxPos = (m_patchSize-1) >> 1;
    assert( (maxPos - minPos + 1) == m_patchSize );
    
    std::default_random_engine             l_rand( m_randomSeed_u );
    std::uniform_int_distribution<int32_t> l_equalDistr( minPos, maxPos );
    
    m_v32Boxes.reserve( num_v32boxes );

    for (int i = 0; i < num_v32boxes; ++i)
    {
        smd::vi32_t cPos;
        smd::vi32_t rPos; 


        for ( auto rp = rPos.begin(), cp = cPos.begin();
                   rp != rPos.end();
              ++rp, ++cp )
        {
            *rp = l_equalDistr( l_rand );
            *cp = l_equalDistr( l_rand );
        }

        m_v32Boxes.emplace_back( cPos, rPos, cPos+1, rPos+1 );
    }

    return static_cast<int>( l_equalDistr( l_rand ) );
}



template <typename ET, typename BT>
void CBoxEvaluationTest::createEvaluator( ET& f_BoxEvaluator, const BT& f_Boxes, float32_t f_featureParam )
{

    f_BoxEvaluator = std::move( ET( m_intImgFrame, m_intImg_p, f_Boxes.size() ) );
    typename ET::feature_param_t l_featureParam( f_featureParam );

    for ( auto l_box_r = f_Boxes.cbegin();
               l_box_r != f_Boxes.cend();
          ++l_box_r )
    {
        f_BoxEvaluator.emplace_boxpair( *l_box_r, *l_box_r, l_featureParam );
    }
}


