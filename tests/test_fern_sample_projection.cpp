/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */



#include <gtest/gtest.h>
#include <tree.hpp>
#include <simd.hpp>
#include <random>


using namespace tree;


template <int NUM_DATA>
class CFernSampleProjectionTest : public ::testing::Test
{
protected:
    static const unsigned int m_numFeatures_u = 13;
    static const unsigned int m_numData_u = NUM_DATA;
 
    typedef const float32_t* feature_ptr_array_t;


    smd::matrix_f32_t  m_feature_rows;
    smd::array_f32_t   m_weights;

    feature_ptr_array_t m_ptrArray[m_numFeatures_u];
 
    void gnerate_data();


    smd::array_f32_t traditional_projection_reference() const;

    template <typename PROJ_T>
    void projectionTest() const;


    virtual void SetUp()
    {
        gnerate_data();
    }
    //virtual void TearDown()
    //{
    //}



};  // CFernSampleProjectionTest


// ----------------------------------------------------------------------------

typedef CFernSampleProjectionTest<1249> CFernSampleProjectionTest_mixedAlignment;
typedef CFernSampleProjectionTest<512> CFernSampleProjectionTest_fullAlignment;


TEST_F( CFernSampleProjectionTest_mixedAlignment, TestSimpleProjection )
{
    projectionTest<CSimpleProjection>();
}
TEST_F( CFernSampleProjectionTest_fullAlignment, TestSimpleProjection )
{
    projectionTest<CSimpleProjection>();
}

TEST_F( CFernSampleProjectionTest_mixedAlignment, TestKahanProjection )
{
    projectionTest<CKahanSampleProjection>();
}
TEST_F( CFernSampleProjectionTest_fullAlignment, TestKahanProjection )
{
    projectionTest<CKahanSampleProjection>();
}

TEST_F( CFernSampleProjectionTest_mixedAlignment, TestNeumaierProjection )
{
    projectionTest<CNeumaierSampleProjection>();
}
TEST_F( CFernSampleProjectionTest_fullAlignment, TestNeumaierProjection )
{
    projectionTest<CNeumaierSampleProjection>();
}

TEST_F( CFernSampleProjectionTest_mixedAlignment, TestKleinProjection )
{
    projectionTest<CKleinSampleProjection>();
}
TEST_F( CFernSampleProjectionTest_fullAlignment, TestKleinProjection )
{
    projectionTest<CKleinSampleProjection>();
}

// ----------------------------------------------------------------------------

template <int NUM_DATA>
template <typename PROJ_T>
void CFernSampleProjectionTest<NUM_DATA>::projectionTest() const
{
    smd::array_f32_t l_proj( m_numData_u );
    l_proj.set_zero();
    
    // [ N ] = [ F ] x [ F x N ]

    PROJ_T sample_projection( l_proj.begin(), l_proj.end() );

    sample_projection.map( m_weights.cbegin(), m_weights.cend(),
                           std::begin(m_ptrArray) );
    
    //smd::array_f32_t l_projRef = std::move( traditional_projection_reference() );

    // Run the comparison
    for (unsigned int l_nIdx=0; l_nIdx < m_numData_u; ++l_nIdx)
    {
        EXPECT_FLOAT_EQ( l_proj(l_nIdx), static_cast<float32_t>( l_nIdx ) );
    }
}


// ----------------------------------------------------------------------------

template <int NUM_DATA>
void CFernSampleProjectionTest<NUM_DATA>::gnerate_data()
{
    m_feature_rows = std::move( smd::matrix_f32_t( m_numFeatures_u, m_numData_u ) );
    m_weights = std::move( smd::array_f32_t( m_numFeatures_u ) );
    
    // fill with values
    for (unsigned int l_fIdx=0; l_fIdx < m_numFeatures_u; ++l_fIdx )
    {
        m_weights(l_fIdx) = 1 / static_cast<float32_t>( (l_fIdx+1) * m_numFeatures_u );

        // initialize pointer array with row starts
        m_ptrArray[l_fIdx] = m_feature_rows.begin( l_fIdx );
        
        for (unsigned int l_nIdx=0; l_nIdx < m_numData_u; ++ l_nIdx )
        {
            m_feature_rows(l_fIdx,l_nIdx) = static_cast<float32_t>( (l_fIdx+1) * l_nIdx );
        }
    }
}


template <int NUM_DATA>
smd::array_f32_t CFernSampleProjectionTest<NUM_DATA>::traditional_projection_reference() const
{
    smd::array_f32_t l_proj( m_numData_u );
    l_proj.set_zero();

    for (unsigned int l_nIdx=0; l_nIdx < m_numData_u; ++l_nIdx )
    {
        for (unsigned int l_fIdx=0; l_fIdx < m_numFeatures_u; ++l_fIdx )
        {
            l_proj(l_nIdx) += m_weights(l_fIdx) * m_feature_rows(l_fIdx,l_nIdx);
        }
    }

    return l_proj;
}

