/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include <gtest/gtest.h>
#include "test_integral_image_base.hpp"
#include <random>
#include "test_utils.hpp"
#include "test_utils.inl"


using namespace img;


namespace test
{


void CIntegralImageTestBase::SetUp()
{
    allocate_memory();
    initialize_image();
    compute_integral_image();

}


void CIntegralImageTestBase::TearDown()
{
    deallocate_memory();
}


void CIntegralImageTestBase::allocate_memory()
{

    m_imgFrame    = image_frame_t( m_imgRows,     m_imgCols );
    m_img_p       = m_imgFrame.allocate_memory<int32_t>();
    m_img_p.set_zero();

    m_intImgFrame = image_frame_t( m_imgRows + 1, m_imgCols + 1 );
    m_intImg_p    = m_intImgFrame.allocate_memory<int32_t>();
    m_intImg_p.set_zero();
}



void CIntegralImageTestBase::deallocate_memory()
{
    m_imgFrame.destruct();
    m_intImgFrame.destruct();

    m_img_p.deallocate_memory();
    m_intImg_p.deallocate_memory();
}



void CIntegralImageTestBase::initialize_image()
{
    std::default_random_engine             l_rand( m_randomSeed_u );
    std::uniform_int_distribution<int32_t> l_equalDistr(127, 255 );

    for ( int32_t rIdx = 0; rIdx < m_imgRows; ++rIdx )
    {
        for ( int32_t cIdx = 0; cIdx < m_imgCols; ++cIdx )
        {
            const int32_t l_flatIndex = m_imgFrame.flat_index( rIdx, cIdx );
            
            ASSERT_EQ( int32_t(0), m_img_p[ l_flatIndex ] );
            
            m_img_p[ l_flatIndex ] = l_equalDistr( l_rand );
        }
    }

    m_randomSeed_u = static_cast<int>( l_equalDistr( l_rand ) );
}


void CIntegralImageTestBase::compute_integral_image()
{
    test::util::compute_integral_image( m_imgFrame, m_img_p, m_intImgFrame, m_intImg_p );
}


} // namespace test

